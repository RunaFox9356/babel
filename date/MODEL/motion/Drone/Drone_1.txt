#==============================================================================
#
# 『モーションビューワ』スクリプトファイル [motion_weapon.txt]
# Author : AKIRA TANAKA
#
#==============================================================================
SCRIPT			# この行は絶対消さないこと！

#------------------------------------------------------------------------------
# モデル数
#------------------------------------------------------------------------------
NUM_MODEL = 17

#------------------------------------------------------------------------------
# モデルファイル名
#------------------------------------------------------------------------------
MODEL_FILENAME = data/MODEL/doron/hontai.x		# [0]ドローン本体
MODEL_FILENAME = data/MODEL/doron/hanebou_01.x		# [1]羽_1
MODEL_FILENAME = data/MODEL/doron/hanebou_02.x		# [2]羽_2
MODEL_FILENAME = data/MODEL/doron/hanebou_03.x		# [3]羽_3
MODEL_FILENAME = data/MODEL/doron/hanebou_04.x		# [4]羽_4
MODEL_FILENAME = data/MODEL/doron/hanebou_05.x		# [5]羽_5
MODEL_FILENAME = data/MODEL/doron/hanebou_06.x		# [6]羽_6
MODEL_FILENAME = data/MODEL/doron/hanebou_07.x		# [7]羽_7
MODEL_FILENAME = data/MODEL/doron/hanebou_08.x		# [8]羽_8
MODEL_FILENAME = data/MODEL/doron/hanebou_09.x		# [9]羽_9
MODEL_FILENAME = data/MODEL/doron/hanebou_10.x		# [10]羽_10
MODEL_FILENAME = data/MODEL/doron/hanebou_11.x		# [11]羽_11
MODEL_FILENAME = data/MODEL/doron/hanebou_12.x		# [12]羽_12
MODEL_FILENAME = data/MODEL/doron/hanebou_13.x		# [13]羽_13
MODEL_FILENAME = data/MODEL/doron/hanebou_14.x		# [14]羽_14
MODEL_FILENAME = data/MODEL/doron/hanebou_15.x		# [15]羽_15
MODEL_FILENAME = data/MODEL/doron/hanebou_16.x		# [16]羽_16

#------------------------------------------------------------------------------
# キャラクター情報
#------------------------------------------------------------------------------
CHARACTERSET
	NUM_PARTS = 17					# パーツ数
	MOVE = 3.0						# 移動量
	JUMP = 10.25					# ジャンプ量
	RADIUS = 20.0					# 半径
	HEIGHT = 40.0					# 高さ

	PARTSSET
		INDEX = 0					# ドローン本体
		PARENT = -1					# (親)NULL
		POS = 0.0 0.0 0.0
		ROT = 0.0 0.0 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 1					# 羽_1(右奥 縦)
		PARENT = 0
		POS = 39.0 60.5 39.0
		ROT = 0.0 0.0 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 2					# 羽_2(右奥 左上から右下)
		PARENT = 0
		POS = 39.0 60.5 38.0
		ROT = 0.0 -0.8 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 3					# 羽_3(右奥 横)
		PARENT = 0
		POS = 40.0 60.5 39.0
		ROT = 0.0 -1.55 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 4					# 羽_4(右奥 上から左下)
		PARENT = 0
		POS = 39.0 60.5 38.0
		ROT = 0.0 0.8 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 5					# 羽_5(右前 縦)
		PARENT = 0
		POS = 39.0 60.0 -38.5
		ROT = 0.0 0.0 0.0
	END_PARTSSET

	PARTSSET
		INDEX = 6					# 羽_6(右前 左上から右下)
		PARENT = 0
		POS = 39.0 60.0 -38.5
		ROT = 0.0 -0.8 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 7					# 羽_7(右前 横)
		PARENT = 0
		POS = 39.0 60.0 -38.5
		ROT = 0.0 -1.55 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 8					# 羽_8(右前 右上から左下)
		PARENT = 0
		POS = 39.0 60.0 -38.5
		ROT = 0.0 0.8 0.0
	END_PARTSSET
	
		PARTSSET
		INDEX = 9					# 羽_9(左奥 縦)
		PARENT = 0
		POS = -39.0 60.5 39.0
		ROT = 0.0 0.0 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 10					# 羽_10(左奥 左上から右下)
		PARENT = 0
		POS = -39.0 60.5 39.0
		ROT = 0.0 -0.8 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 11					# 羽_11(左奥 横)
		PARENT = 0
		POS = -39.0 60.5 39.0
		ROT = 0.0 -1.55 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 12					# 羽_12(左奥 右上から左下)
		PARENT = 0
		POS = -39.0 60.5 39.0
		ROT = 0.0 0.8 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 13					# 羽_13(左前 縦)
		PARENT = 0
		POS = -39.0 60.0 -38.5
		ROT = 0.0 0.0 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 14					# 羽_14(左前 左上から右下)
		PARENT = 0
		POS = -39.0 60.0 -38.5
		ROT = 0.0 -0.8 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 15					# 羽_15(左前 横)
		PARENT = 0
		POS = -39.0 60.0 -38.5
		ROT = 0.0 -1.55 0.0
	END_PARTSSET
	
	PARTSSET
		INDEX = 16					# 羽_16(左前 右上から左下)
		PARENT = 0
		POS = -39.0 60.0 -38.5
		ROT = 0.0 0.8 0.0
	END_PARTSSET
	
END_CHARACTERSET

#------------------------------------------------------------------------------
# モーション情報
#------------------------------------------------------------------------------
#---------------------------------------
# [0] ニュートラルモーション
#---------------------------------------
MOTIONSET
	LOOP = 1		# ループするかどうか[0:ループしない / 1:ループする]
	NUM_KEY = 2		# キー数

	KEYSET			# --- << KEY : 0 / 2 >> ---
		FRAME = 45
		KEY	# ----- [ 0 ] -----
			POS = 0.00 10.00 0.00
			ROT = 0.00 0.00 0.00
		END_KEY

		KEY	# ----- [ 1 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 2 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 3 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 4 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 5 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 6 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 7 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 8 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 9 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 10 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 11 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 12 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 13 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 14 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 15 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 16 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
	END_KEYSET

	KEYSET			# --- << KEY : 1 / 2 >> ---
		FRAME = 50
		KEY	# ----- [ 0 ] -----
			POS = 0.00 10.00 0.00
			ROT = 0.00 0.00 0.00
		END_KEY

		KEY	# ----- [ 1 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 2 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 3 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 4 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 5 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 6 ] -----
			POS = 0.00 0.50 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 7 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 8 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 9 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 10 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 11 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 12 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 13 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 14 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 15 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 16 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
	END_KEYSET
END_MOTIONSET

#---------------------------------------
# [1] 移動モーション
#---------------------------------------
MOTIONSET
	LOOP = 1		# ループするかどうか[0:ループしない / 1:ループする]
	NUM_KEY = 2		# キー数

	KEYSET			# --- << KEY : 0 / 2 >> ---
		FRAME = 45
		KEY	# ----- [ 0 ] -----
			POS = 0.00 10.00 0.00
			ROT = 0.00 0.00 0.00
		END_KEY

		KEY	# ----- [ 1 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 2 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 3 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 4 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 5 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 6 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 7 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 8 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 9 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY

		KEY	# ----- [ 10 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 11 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 12 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 13 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 14 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 15 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
		KEY	# ----- [ 16 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 12.5 0.00
		END_KEY
		
	END_KEYSET

	KEYSET			# --- << KEY : 1 / 2 >> ---
		FRAME = 50
		KEY	# ----- [ 0 ] -----
			POS = 0.00 10.00 0.00
			ROT = 0.00 0.00 0.00
		END_KEY

		KEY	# ----- [ 1 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 2 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 3 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 4 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 5 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 6 ] -----
			POS = 0.00 0.50 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 7 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 8 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 9 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY

		KEY	# ----- [ 10 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 11 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 12 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 13 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 14 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 15 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
		KEY	# ----- [ 16 ] -----
			POS = 0.00 0.00 0.00
			ROT = 0.00 -12.5 0.00
		END_KEY
		
	END_KEYSET
END_MOTIONSET

END_SCRIPT		# この行は絶対消さないこと！
