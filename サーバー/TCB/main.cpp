//============================
//
// メイン設定
// Author:hamada ryuuga
//
//============================
#include "main.h"
#include "tcp_listener.h"

#include "listener.h"
#include "client.h"

#include "model_data.h"

#include "udp_client.h"
#include <thread>

using namespace std;


int g_nRoomCount;

std::string EndAccess = "exit";
namespace nl = nlohmann;
const char* pathToJSON = "data/input.json";
CListener*pListenr = nullptr;
std::string m_IPaddress;
std::string m_IPoot;
CModelData::SPlayerData player[MAX_P];
int m_hostPlayer;

void ThreadA(CClient*pClient)
{
	char aSendData[256] = {};

	int Data2 = 0;
	char aRecvData[50] = {};
	char aRecvData2[50] = {};

	pClient->Recv(&aRecvData[0], sizeof(int), CClient::TYPE_TCP);

	memcpy(&m_hostPlayer, &aRecvData[0], (int)sizeof(int));

	CClient*pClient2;

	while (1)
	{
		pClient2 = pListenr->Accept();
		
		pClient2->Recv(&aRecvData2[0], sizeof(int), CClient::TYPE_TCP);

		memcpy(&Data2, &aRecvData2[0], (int)sizeof(int));

		//if (Data2 != m_hostPlayer)
		{
			break;
		}
	}

	printf("二つつながった\n");

	bool bMapConnect = true;
	memcpy(&aSendData[0], &bMapConnect, sizeof(bool));
	pClient->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);

	// プレイヤー2にsend
	memcpy(&aSendData[0], &bMapConnect, sizeof(bool));
	pClient2->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);
	
	MapSend(pClient, pClient2, pListenr, m_hostPlayer, false);

	// スレッド生成
	thread th(CommuniCationClient, pClient, pClient2, pListenr);

	// スレッドを切り離す
	th.join();

}

void main(void)
{

	g_nRoomCount = 0;
	WSADATA wasData;
	m_hostPlayer = 0;
	int nErr = WSAStartup(WINSOCK_VERSION, &wasData);

	if (nErr != 0)
	{
		printf("was　error");
	}
	
	pListenr = new CListener;
	LoodIp();
	int poot = atoi(m_IPoot.c_str());
	if (pListenr->Init(INADDR_ANY, 22233))
	{
		while (1)
		{
			CClient*pClient = pListenr->Accept();

			std::thread th(ThreadA, pClient);
			g_nRoomCount++;
			// 部屋を作り終えるまでブロック
			th.join();
			pListenr->UninitOn();
		}
		
	}

	pListenr->UninitOn();
	WSACleanup();

}

//--------------------------
// 1部屋事のマルチスレッド
//--------------------------
void CommuniCationClient(CClient *pSendRecvP1, CClient *pSendRecvP2, CListener *pServer)
{
	int nRecv = 1;
	int nUdpRecv = 0;
	int nSendTimer = 0;
	fd_set fds, readfds;
	SOCKET maxfd, sock[MAX_P];
	char aRecvData[2048];
	bool IsMapSelect;
	int score = 0;
	int Timer = 10000;
	player[0].SetPlayerClear();
	player[1].SetPlayerClear();
	CReceiveData::SReceiveList sendData;
	sendData.Init();
	player[0].Player.m_MyId = P1;
	player[1].Player.m_MyId = P2;

	FD_ZERO(&readfds);
	cout << "部屋数 : " << g_nRoomCount << "\n" << endl;

	// ソケットの入手
	sock[0] = pSendRecvP1->GetUdp()->GetSock();
	sock[1] = pSendRecvP2->GetUdp()->GetSock();

	bool SetInit[2];
	SetInit[0] = false;
	SetInit[1] = false;

	player[0].Player.m_IsGame = true;
	player[1].Player.m_IsGame = true;

	player[0].Player.m_PlayData.m_Life = 1000;
	player[1].Player.m_PlayData.m_Life = 1000;

	for (int nCnt = 0; nCnt < MAX_P; nCnt++)
	{
		// 監視ソケットの登録
		FD_SET(sock[nCnt], &readfds);
	}
	// 最大ソケットの判定
	if (sock[0] > sock[1])
	{
		maxfd = sock[0];
	}
	else if (sock[1] > sock[0])
	{
		maxfd = sock[1];
	}


	while (1)
	{
		memcpy(&fds, &readfds, sizeof(fd_set));
		// ソケットの監視
		select(maxfd + 1, &fds, NULL, NULL, NULL);
		// プレイヤー1にsendされていたら
		if (FD_ISSET(sock[0], &fds)&& player[0].Player.m_IsGame)
		{
			nRecv = pSendRecvP1->Recv(&aRecvData[0], sizeof(CModelData::SSendPack), CClient::TYPE_UDP);
		
			if (nRecv == sizeof(CModelData::SSendPack))
			{
				CModelData::SSendPack Data;
				memcpy(&Data, &aRecvData[0], (int)sizeof(CModelData::SSendPack));

				player[0].SetPlayer(Data);

				score += Data.m_addscore;
				Data.m_score = score;

				/*CModelData::SSendPack Data;
				memcpy(&Data, &aRecvData[0], (int)sizeof(CModelData::SSendPack));

				score += Data.m_addscore;
				Data.m_score = score;

				int id = 0;
				for (int i = 0; i < MAX_P; i++)
				{
					if (player[i].Player.m_MyId == Data.m_MyId)
					{
						SetInit[i] = true;
						player[i].SetPlayer(Data);
						sendData.SetListPlayr(player[i].Player.m_PlayData, i);
						sendData.SetListEnemy(&player[i].Player.m_isPopEnemy[0]);
						sendData.SetListGimmick(&player[i].Player.m_isPopGimmick[0]);
						sendData.m_score = score;
					}
				}

				score += Data.m_addscore;
				Data.m_score = score;*/
				pSendRecvP2->Send((const char*)&Data, sizeof(CModelData::SSendPack), CClient::TYPE_UDP);
			}


		}
		// プレイヤー2にsendされていたら
		if (FD_ISSET(sock[1], &fds)&& player[1].Player.m_IsGame)
		{

			nRecv = pSendRecvP2->Recv(&aRecvData[0], sizeof(CModelData::SSendPack), CClient::TYPE_UDP);
			if (nRecv == sizeof(CModelData::SSendPack))
			{
				CModelData::SSendPack Data;
				memcpy(&Data, &aRecvData[0], (int)sizeof(CModelData::SSendPack));

				player[1].SetPlayer(Data);

				score += Data.m_addscore;
				Data.m_score = score;
				pSendRecvP1->Send((const char*)&Data, sizeof(CModelData::SSendPack), CClient::TYPE_UDP);
				/*CModelData::SSendPack Data;
				memcpy(&Data, &aRecvData[0], (int)sizeof(CModelData::SSendPack));

				score += Data.m_addscore;
				Data.m_score = score;

				int id = 0;
				for (int i = 0; i < MAX_P; i++)
				{

					if (player[i].Player.m_MyId == Data.m_MyId)
					{	SetInit[i] = true;
						player[i].SetPlayer(Data);
						sendData.SetListPlayr(player[i].Player.m_PlayData,i);
						sendData.SetListEnemy(&player[i].Player.m_isPopEnemy[0]);
						sendData.SetListGimmick(&player[i].Player.m_isPopGimmick[0]);
						sendData.m_score = score;

					}
				}	*/
			
			}
		}
		
		if (!player[0].Player.m_IsGame && !player[1].Player.m_IsGame)
		{
			MapSend(pSendRecvP1, pSendRecvP2, pServer, m_hostPlayer,true);
			score = 0;
			player[0].Player.m_PlayData.m_Life = 1000;
			player[1].Player.m_PlayData.m_Life = 1000;
			player[0].Player.m_IsGame = true;
			player[1].Player.m_IsGame = true;
		}

	}

	pSendRecvP1->Uninit();
	pSendRecvP2->Uninit();
	delete pSendRecvP1;
	delete pSendRecvP2;

	g_nRoomCount--;
	cout << "部屋が解散されました。\n" << endl;
	cout << "部屋数 : [" << g_nRoomCount <<"]\n"<< endl;
}

void MapSend(CClient *pSendRecvP1, CClient *pSendRecvP2, CListener *pServer,int ID,bool isresult)
{
	int nRecv = 1;
	int nSendTimer = 0;
	fd_set fds, readfds;
	SOCKET maxfd, sock[MAX_P];
	char aRecvData[1024];
	bool IsMapSelect;
	int score = 0;
	FD_ZERO(&readfds);
	SSendMap map;
	map.isMap = false;
	map.sendMap = 0;
	char aSendData[256] = {};


	
	cout << "マップえらんでます :部屋の数[" << g_nRoomCount<< "]\n" << endl;

	// ソケットの入手
	sock[0] = pSendRecvP1->GetTcp()->GetSock();
	sock[1] = pSendRecvP2->GetTcp()->GetSock();

	for (int nCnt = 0; nCnt < MAX_P; nCnt++)
	{
		// 監視ソケットの登録
		FD_SET(sock[nCnt], &readfds);
	}
	// 最大ソケットの判定
	if (sock[0] > sock[1])
	{
		maxfd = sock[0];
	}
	else if (sock[1] > sock[0])
	{
		maxfd = sock[1];
	}
	if (isresult)
	{
		bool re = true;
		memcpy(&aSendData[0], &re, sizeof(bool));
		pSendRecvP1->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);

		// プレイヤー2にsend
		memcpy(&aSendData[0], &re, sizeof(bool));
		pSendRecvP2->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);
	}
	while (!map.isMap)
	{

		if (ID == 0)
		{
			printf("ホストは「最初に繋いだPC」です\n");
			pSendRecvP1->Recv(&aRecvData[0], sizeof(SSendMap), CClient::TYPE_TCP);
			memcpy(&map, &aRecvData, sizeof(SSendMap));
			pSendRecvP2->Send(&aRecvData[0], sizeof(SSendMap), CClient::TYPE_TCP);
		}

		if (ID == 1)
		{
			printf("ホストは「二番目に繋いだPC」です\n");
			pSendRecvP2->Recv(&aRecvData[0], sizeof(SSendMap), CClient::TYPE_TCP);
			memcpy(&map, &aRecvData, sizeof(SSendMap));
			pSendRecvP1->Send(&aRecvData[0], sizeof(SSendMap), CClient::TYPE_TCP);
		}
	}

	printf("[MAP]確認送信[%d]番目マップ\n", map.sendMap);

	bool bMapConnect = true;
	memcpy(&aSendData[0], &bMapConnect, sizeof(bool));
	pSendRecvP1->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);

	// プレイヤー2にsend
	memcpy(&aSendData[0], &bMapConnect, sizeof(bool));
	pSendRecvP2->Send(&aSendData[0], sizeof(bool), CClient::TYPE_TCP);

	player[0].Player.m_IsGame = true;
	player[1].Player.m_IsGame = true;

}

std::string GetConfigString(const std::string& filePath, const char* pSectionName, const char* pKeyName)
{
	if (filePath.empty())
	{
		return "";
	}
	std::array<char, MAX_PATH> buf = {};
	GetPrivateProfileStringA(pSectionName, pKeyName, "", &buf.front(), static_cast<DWORD>(buf.size()), filePath.c_str());
	return &buf.front();
}


void LoodIp()
{
	std::string filePath = "data\\system.ini";
	auto WindowText = GetConfigString(filePath, "System", "IPADDRESS");
	auto WindowWidth = GetConfigString(filePath, "System", "PORT_NUM");
	auto WindowHeight = GetConfigString(filePath, "System", "NAME");

	m_IPaddress = WindowText;
	m_IPoot = WindowWidth;
}