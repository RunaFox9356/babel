//====================================================
//
// TCP
// Author : 浜田
//
//====================================================
#ifndef _TCP_LISTENER_H_
#define _TCP_LISTENER_H_

//-------------------------------
// インクルード
//-------------------------------

class CTcp_client;
//-------------------------------
// Classの定義
//-------------------------------
class CTcp_Listener
{
public:
	CTcp_Listener();	// デフォルトコンストラクタ
	~CTcp_Listener();	// デフォルトデストラクタ
	bool Init(void);	// 初期化
	CTcp_client* Accept(void);	// 接続待ち
	void Uninit(void);	// 終了
private:
	SOCKET m_sock;	// ソケット
};

#endif // _TCP_CLIENT_H_