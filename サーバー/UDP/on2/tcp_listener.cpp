﻿//====================================================
//
// 接続
// Author : 浜田
//
//====================================================
//-------------------------------
// インクルード
//-------------------------------
#include "tcp_listener.h"
#include "tcp_client.h"

//-------------------------------
// デフォルトコンストラクタ
//-------------------------------
CTcp_Listener::CTcp_Listener()
{
	m_sockServer = INVALID_SOCKET;
}

//-------------------------------
// デフォルトデストラクタ
//-------------------------------
CTcp_Listener::~CTcp_Listener()
{

}

//-------------------------------
// 初期化
//-------------------------------
bool CTcp_Listener::Init()
{
	FILE *pFile;
	char aFile[64];
	int nPort;
	int nWait;


	fclose(pFile);

	//------------------------
	// ソケット作成
	//------------------------
	m_sock = socket(AF_INET, SOCK_STREAM, 0);

	if (m_sock == INVALID_SOCKET)
	{
		printf("ソケット失敗\n");
		return false;
	}

	//------------------------
	// 受付準備
	//------------------------
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;	// どの通信か
	addr.sin_port = htons(nPort);	// ポート番号
	addr.sin_addr.S_un.S_addr = INADDR_ANY;	// 誰でもアクセスできる

	bind(m_sock,
		(struct sockaddr*)&addr,
		sizeof(addr));

	listen(m_sockServer, nWait);	// 最大待機数

	return true;
}

//-------------------------------
// 接続受付
//-------------------------------
CTcp_client * CTcp_Listener::Accept()
{
	CTcp_client *pSendRecv = new CTcp_client;

	//------------------------
	// 接続待ち
	//------------------------
	struct sockaddr_in clientAddr;
	int nLength = sizeof(clientAddr);

	SOCKET sock = accept(m_sockServer,
		(struct sockaddr*)&clientAddr,
		&nLength);

	printf("接続出来ました。\n");
	if (pSendRecv != NULL)
	{
		pSendRecv->Init(sock);
	}

	return pSendRecv;
}

//-------------------------------
// 終了
//-------------------------------
void CTcp_Listener::Uninit()
{
	if (m_sockServer == INVALID_SOCKET)
	{
		return;
	}

	//------------------------
	// 接続切断
	//------------------------
	printf("接続を切断します。\n");
	closesocket(m_sockServer);	// 接続受付用ソケット
	m_sockServer = INVALID_SOCKET;
}