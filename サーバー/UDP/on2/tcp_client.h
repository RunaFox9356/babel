//============================
//
// client設定
// Author:hamada ryuuga
//
//============================

#ifndef _TCP_CKIENT_H_
#define _TCP_CKIENT_H_

//構造体

class  CTcp_client
{
public:
	CTcp_client();
	~CTcp_client();
	bool Init(const char*plPAddress,int nPortNum); bool Init(SOCKET sock);
	int Send(char*pSendData, int nSendDataSize);
	int Recv(char*pRecvData, int nRecvDataSize);
	void Uninit(void);
	SOCKET GetSock() { return m_sock; };
	
private:
	SOCKET m_sock;
};

#endif