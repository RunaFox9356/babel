#ifndef _MAIN_H_
#define _MAIN_H_
//------------------------
// インクルード
//------------------------
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <thread>


//------------------------
// 前方宣言
//------------------------
class CTcp_client;
class CTcp_Listener;

//------------------------
// マクロ定義
//------------------------
#define MAX_IP_NUM (16)
#define MAX_PLAYER (2)
#define MAX_COMMUDATA (524)

#include <iostream>
#include <winsock2.h>
#include <thread>
#include <iostream>

#pragma comment(lib, "ws2_32.lib")

//------------------------
// プロトタイプ宣言
//------------------------
void CommuniCationClient(CTcp_client *pSendRecvP1, CTcp_client *pSendRecvP2, CTcp_Listener *pServer);
void RoomCreate(CTcp_client *pSendRecv, CTcp_Listener *pServer);
void KeyWait(void);

#endif