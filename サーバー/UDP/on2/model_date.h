//============================
//
// 座標があるモデル
// Author:hamada ryuuga
//
//============================

#ifndef _MODEL_DATE_H_
#define _MODEL_DATE_H_

#include "protocol.h"

class CModelData
{

	struct SSendData
	{
		enum EDATA
		{
			ESENDDATA_ENEMY,
		};


		SSendData(int id) :mId(id) {}
		int mId;
	};
	struct SSendEnemy : public SSendData
	{
		int mNumEnemy;
		D3DXVECTOR3 m_pos;
		D3DXVECTOR3 m_rot;
		int m_motion;
		SSendEnemy() : SSendData(ESENDDATA_ENEMY) {}

		int GetSize() const
		{
			int Size = 0;
			Size += mNumEnemy;
			Size += sizeof(m_pos);
			Size += sizeof(m_rot);
			Size += sizeof(m_motion);
			return Size;
		}
		std::string GetList()
		{
			std::string Size;
			Size += m_pos.x;
			Size += m_pos.y;
			Size += m_pos.z;
			Size += m_rot.x;
			Size += m_rot.y;
			Size += m_rot.z;
			Size += m_motion;

			return Size;
		}

	};
	struct SPlayerDate
	{
		SSendEnemy Player;
		bool IsUse = false;
	};


	CModelData();
	~CModelData();

	SPlayerDate GetCommu() { return m_CommuData; };
	void SetCommu(SPlayerDate Set) {  m_CommuData = Set; };

	SPlayerDate m_CommuData;
};


#endif