//=============================================================================
//
// タイトルクラス(title.cpp)
// Author : 浜田琉雅
// 概要 : タイトルクラスの管理を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "matching.h"

#include "application.h"
#include "input.h"
#include "sound.h"
#include "client.h"
#include "debug_proc.h"
#include "camera.h"
#include "text.h"

//=============================================================================
// コンストラクタ
// Author : 浜田琉雅
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CMatching::CMatching()
{

}

//=============================================================================
// デストラクタ
// Author : 浜田琉雅
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CMatching::~CMatching()
{

}

//=============================================================================
// 初期化
// Author : 浜田琉雅
// 概要 :メンバ変数の初期値を設定
//=============================================================================
HRESULT CMatching::Init()
{
	CText::Create(D3DXVECTOR3(0.0f, 100.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "マッチング画面");
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM000);

	m_Connect.myConnect = false;
	m_Connect.enemyConnect = false;
	m_isCommand = false;
	m_textPopOk = false;
	selectText = nullptr;

	m_ConnectMode = 0;
	return S_OK;
}

//=============================================================================
// 終了
// Author : 浜田琉雅
// 概要 : 自身の破棄
//=============================================================================
void CMatching::Uninit()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 浜田琉雅
// 概要 : 更新を行う
//=============================================================================
void CMatching::Update()
{
	
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(DIK_SPACE)&& m_isCommand)
	{
		std::thread ConnectOn([&] {m_Connect = CApplication::GetInstance()->ConnectTh(m_ConnectMode); });

		// スレッドをきりはなす
		ConnectOn.detach();
	}

	if (m_Connect.myConnect&& m_Connect.enemyConnect &&!m_textPopOk)
	{
		if (m_ConnectMode == 0)
		{
			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_AGENT);
		}
		else if (m_ConnectMode == 1)
		{
			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_HACKER);
		}
		m_textPopOk = true;
		CText::Create(D3DXVECTOR3(0.0f, 300.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "マッチングOK");
	}



#ifdef _DEBUG
	// デバック表示
	CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル| F7 デバック表示削除\n");


	if (pInput->Trigger(DIK_F1))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		if (selectText != nullptr)
		{
			selectText->Uninit();
			selectText = nullptr;
		}
		selectText = CText::Create(D3DXVECTOR3(0.0f, 200.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "エージェントです！");
		m_isCommand = true;
		m_ConnectMode = 0;
	}
	if (pInput->Trigger(DIK_F3))
	{
		if (selectText != nullptr)
		{
			selectText->Uninit();
			selectText = nullptr;
		}
		selectText = CText::Create(D3DXVECTOR3(0.0f, 200.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "ハッカーです！");
		m_isCommand = true;
		m_ConnectMode = 1;
	}
	if (pInput->Trigger(DIK_F4))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TITLE);
	}
#endif // _DEBUG


	//m_socket->Update();
}
