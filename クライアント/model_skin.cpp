//=============================================================================
//
// 3Dモデルクラス(model3D.h)
// Author : 浜田琉雅
// 概要 : 3Dモデル生成を行う
//
//=============================================================================

#include "model_skin.h"
#include "application.h"
#include "renderer.h"
#include "shader_manager.h"
#include "camera.h"
#include "light.h"
#include "debug_proc.h"

namespace
{
	const std::string shaderLabel = "Effect";
}

//=============================================================================
// インスタンス生成
// Author : 浜田琉雅
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CSkinMesh * CSkinMesh::Create(std::string Name)
{
	// オブジェクトインスタンス
	CSkinMesh *pSkinMesh = nullptr;

	// メモリの解放
	pSkinMesh = new CSkinMesh;

	// メモリの確保ができなかった
	assert(pSkinMesh != nullptr);

	// 数値の初期化
	pSkinMesh->Load(Name);

	// インスタンスを返す
	return pSkinMesh;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// フレーム作成処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT MY_HIERARCHY::CreateFrame(LPCSTR Name, LPD3DXFRAME *ppNewFrame)
{
	//新しいフレームアドレス格納用変数を初期化
	*ppNewFrame = nullptr;

	//フレームの領域確保
	MYFRAME* pFrame = new MYFRAME;
	//領域確保の失敗時の処理
	if (pFrame == nullptr)
	{
		return E_OUTOFMEMORY;
	}

	//フレーム名格納用領域確保
	pFrame->Name = new TCHAR[lstrlen(Name) + 1];
	//領域確保の失敗時の処理
	if (!pFrame->Name)
	{
		return E_FAIL;
	}
	
	//フレーム名格納
	strcpy(pFrame->Name, Name);

	//行列の初期化
	D3DXMatrixIdentity(&pFrame->TransformationMatrix);
	D3DXMatrixIdentity(&pFrame->CombinedTransformationMatrix);

	//追加：オフセット関係初期化
	//pFrame->OffsetID = 0xFFFFFFFF;
	//D3DXMatrixIdentity(&(pFrame->OffsetMat));

	// 新規フレームのメッシュコンテナ初期化
	pFrame->pMeshContainer = nullptr;

	// 新規フレームの兄弟フレームアドレス格納用変数初期化
	pFrame->pFrameSibling = nullptr;

	// 新規フレームの子フレームアドレス格納用変数初期化
	pFrame->pFrameFirstChild = nullptr;

	//外部の新規フレームアドレス格納用変数に、作成したフレームのアドレスを格納
	*ppNewFrame = pFrame;
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// HRESULT MY_HIERARCHY::CreateMeshContainer
// メッシュコンテナーを作成する
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT MY_HIERARCHY::CreateMeshContainer(LPCSTR Name, CONST D3DXMESHDATA* pMeshData,
	CONST D3DXMATERIAL* pMaterials, CONST D3DXEFFECTINSTANCE* pEffectInstances,
	DWORD NumMaterials, CONST DWORD *pAdjacency, LPD3DXSKININFO pSkinInfo,
	LPD3DXMESHCONTAINER *ppMeshContainer)
{
	// ローカル生成用
	MYMESHCONTAINER *pMeshContainer = nullptr;

	// 一時的なDirectXデバイス取得用
	LPDIRECT3DDEVICE9 pDevice = nullptr;

	// 一時的なメッシュデータ格納用
	LPD3DXMESH pMesh = nullptr;

	// メッシュコンテナ格納用変数初期化
	*ppMeshContainer = nullptr;

	// pMeshに"外部引数の"メッシュアドレスを格納
	pMesh = pMeshData->pMesh;

	// メッシュコンテナ領域の動的確保
	pMeshContainer = new MYMESHCONTAINER;
	if (pMeshContainer == nullptr)
	{	// 領域確保失敗
		return E_OUTOFMEMORY;
	}

	// メッシュコンテナを初期化
	ZeroMemory(pMeshContainer, sizeof(MYMESHCONTAINER));

	// メッシュコンテナの名前格納用領域を動的確保
	pMeshContainer->Name = new TCHAR[lstrlen(Name) + 1];
	if (!pMeshContainer->Name)
	{	// 領域確保失敗
		return E_OUTOFMEMORY;
	}

	// 確保した領域にメッシュコンテナ名を格納
	strcpy(pMeshContainer->Name, Name);

	// DirectXデバイス取得
	pMesh->GetDevice(&pDevice);

	// メッシュの面の数を取得
	int facesAmount = pMesh->GetNumFaces();

	// メッシュのマテリアル数を格納(最大で1つ)
	pMeshContainer->NumMaterials = max(1, NumMaterials);

	// メッシュコンテナの、マテリアルデータ格納領域を動的確保
	pMeshContainer->pMaterials = new D3DXMATERIAL[pMeshContainer->NumMaterials];

	// メッシュコンテナの、テクスチャデータ格納領域を動的確保
	pMeshContainer->ppTextures = new LPDIRECT3DTEXTURE9[pMeshContainer->NumMaterials];

	// メッシュコンテナの、面ごとに持つ3つの隣接性情報が格納されたDWORD型のアドレス格納用(ポインタ)変数
	pMeshContainer->pAdjacency = new DWORD[facesAmount * 3];

	if ((pMeshContainer->pAdjacency == nullptr) || (pMeshContainer->pMaterials == nullptr))
	{	// 領域確保失敗
		return E_OUTOFMEMORY;
	}

	// 外部引数の隣接性情報をメッシュコンテナに格納
	memcpy(pMeshContainer->pAdjacency, pAdjacency, sizeof(DWORD) * facesAmount * 3);

	// テクスチャデータ格納用領域を初期化(memsetを使用して0で中身を埋める)
	memset(pMeshContainer->ppTextures, 0, sizeof(LPDIRECT3DTEXTURE9) * pMeshContainer->NumMaterials);

	if (NumMaterials > 0)
	{
		// 外部引数のマテリアルデータアドレスをメッシュコンテナに格納
		memcpy(pMeshContainer->pMaterials, pMaterials, sizeof(D3DXMATERIAL) * NumMaterials);

		// マテリアル数分ループさせる
		for (DWORD numMat = 0; numMat < NumMaterials; numMat++)
		{
			if (pMeshContainer->pMaterials[numMat].pTextureFilename == nullptr)
			{
				continue;
			}

			// テクスチャのファイルパス保存用変数
			TCHAR strTexturePath[MAX_PATH];

			// テクスチャのファイルパスを保存(再読み込み時に必要)
			strcpy_s(strTexturePath, lstrlen(pMeshContainer->pMaterials[numMat].pTextureFilename) + 1, pMeshContainer->pMaterials[numMat].pTextureFilename);

			// テクスチャ情報の読み込み
			if (FAILED(D3DXCreateTextureFromFile(pDevice, strTexturePath, &pMeshContainer->ppTextures[numMat])))
			{ // 読み込み失敗時
				// テクスチャファイル名格納用
				CHAR TexMeshPass[255];

				// もしなければ、Graphフォルダを調べる
				// 注）ファイル名の結合時に、必ず両方にファイル名がある事を確認してから
				// strcpy_sとstrcat_sを使うようにする(この場合は、上にある 
				// テクスチャのファイルがあり、さらにそのファイル名の長さが0でなければ の所のif文)。
				// TexMeshPassに、Xファイルがある場所と同じディレクトリと、テクスチャのファイル名を
				// 結合したものを格納
				// strcpy_s( TexMeshPass, sizeof( TexMeshPass ) , "./../Source/Graph/" );
				strcpy_s(TexMeshPass, sizeof(TexMeshPass), "./Graph/");
				strcat_s(TexMeshPass, sizeof(TexMeshPass) - strlen(TexMeshPass) - strlen(strTexturePath) - 1, strTexturePath);

				// テクスチャ情報の読み込み
				if (FAILED(D3DXCreateTextureFromFile(pDevice, TexMeshPass, &pMeshContainer->ppTextures[numMat])))
				{
					pMeshContainer->ppTextures[numMat] = nullptr;
				}
				pMeshContainer->pMaterials[numMat].pTextureFilename = nullptr;
			}
		}
	}
	else
	{	// マテリアルがない場合
		// テクスチャファイル名をNULLに
		pMeshContainer->pMaterials[0].pTextureFilename = nullptr;

		// マテリアルデータ初期化
		memset(&pMeshContainer->pMaterials[0].MatD3D, 0, sizeof(D3DMATERIAL9));
		pMeshContainer->pMaterials[0].MatD3D.Diffuse = { 0.5f,0.5f,0.5f,1.0f };
		pMeshContainer->pMaterials[0].MatD3D.Specular = pMeshContainer->pMaterials[0].MatD3D.Diffuse;
	}

	// メッシュ情報を格納(今回は通常メッシュと完全に分けているためすべてスキンメッシュ情報となる)
	pMeshContainer->pSkinInfo = pSkinInfo;

	// 参照カウンタ
	pSkinInfo->AddRef();

	// ボーンの数を取得
	DWORD dwBoneNum = pSkinInfo->GetNumBones();

	// フレーム(ボーン)単位でのワールド行列格納用領域の動的確保
	pMeshContainer->pBoneOffsetMatrices = new D3DXMATRIX[dwBoneNum];

	// ボーンの数だけループさせる
	for (DWORD i = 0; i < dwBoneNum; i++)
	{
		// 角フレーム(ボーン)のオフセット行列を取得して格納
		memcpy(&pMeshContainer->pBoneOffsetMatrices[i], pMeshContainer->pSkinInfo->GetBoneOffsetMatrix(i), sizeof(D3DMATRIX));
	}

	for (int i = 0; i < dwBoneNum; i++)
	{
		DWORD numVertices = pMesh->GetNumVertices();

		// 影響を受ける頂点の個数
		DWORD numInfluence = pSkinInfo->GetNumBoneInfluences(i);
		if (numInfluence > 0)
		{
			std::vector<DWORD> indices(numInfluence);
			std::vector<float> weights(numInfluence);
			pSkinInfo->GetBoneInfluence(i, indices.data(), weights.data());
		}
	}

	// メッシュコンテナにオリジナルのpMesh情報を格納
	D3DVERTEXELEMENT9 Decl[MAX_FVF_DECL_SIZE];
	pMesh->GetDeclaration(&Decl[0]);

	//CShaderManager* pShManager = CApplication::GetInstance()->GetShaderManager();
	//D3DVERTEXELEMENT9* decl = pShManager->GetDeclaration("SkinMesh");
	pMesh->CloneMesh(pMesh->GetOptions(), Decl, pDevice, &pMeshContainer->pOriMesh);

	// メッシュのタイプを定義
	pMeshContainer->MeshData.Type = D3DXMESHTYPE_MESH;

	// シェーダで描画する場合は別途変換が必要
	// 頂点単位でのブレンドの重みとボーンの組み合わせテーブルを適応した新しいメッシュを返す。
	if (FAILED(pMeshContainer->pSkinInfo->ConvertToBlendedMesh(
		pMeshContainer->pOriMesh,				// 元のメッシュデータアドレス
		NULL,									// オプション(現在は使われていないためNULLでいい)
		pMeshContainer->pAdjacency,				// 元のメッシュの隣接性情報
		nullptr,								// 出力メッシュの隣接性情報
		nullptr,								// 各面の新しいインデックス値格納用変数のアドレス
		nullptr,								// 角頂点の新しいインデックス値格納用変数のアドレス
		&pMeshContainer->dwWeight,				// ボーンの影響の一面当たりの最大数格納用変数のアドレス
		&pMeshContainer->dwBoneNum,				// ボーンの組み合わせテーブルに含まれるボーン数格納用変数のアドレス
		&pMeshContainer->pBoneBuffer,			// ボーンの組み合わせテーブルへのポインタ
		&pMeshContainer->MeshData.pMesh )))		// 出力されるメッシュアドレス格納用変数のアドレス(固定パイプライン用)
	{
		return E_FAIL;
	}

	// ローカルに生成したメッシュコンテナーを呼び出し側にコピーする
	*ppMeshContainer = pMeshContainer;

	// 参照カウンタを増やしたので減らす
	SAFE_RELEASE(pDevice);
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// HRESULT MY_HIERARCHY::DestroyFrame(LPD3DXFRAME pFrameToFree) 
// フレーム破棄処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT MY_HIERARCHY::DestroyFrame(LPD3DXFRAME pFrameToFree)
{
	// 二重解放防止
	// if (pFrameToFree == NULL)return S_FALSE;
	SAFE_DELETE_ARRAY(pFrameToFree->Name);
	if (pFrameToFree->pFrameFirstChild)
	{
		DestroyFrame(pFrameToFree->pFrameFirstChild);
	}
	if (pFrameToFree->pFrameSibling)
	{
		DestroyFrame(pFrameToFree->pFrameSibling);
	}
	SAFE_DELETE(pFrameToFree);
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// HRESULT MY_HIERARCHY::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerBase)
// メッシュコンテナーを破棄
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT MY_HIERARCHY::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerBase)
{
	MYMESHCONTAINER* pMeshContainer = static_cast<MYMESHCONTAINER*>(pMeshContainerBase);

	SAFE_DELETE_ARRAY(pMeshContainer->Name);
	SAFE_RELEASE(pMeshContainer->pSkinInfo);
	SAFE_DELETE_ARRAY(pMeshContainer->pAdjacency);
	SAFE_DELETE_ARRAY(pMeshContainer->pMaterials);
	SAFE_DELETE_ARRAY(pMeshContainer->ppBoneMatrix);

	if (pMeshContainer->ppTextures != nullptr)
	{	// テクスチャ解放処理
		for (DWORD numMat = 0; numMat < pMeshContainer->NumMaterials; numMat++)
		{
			SAFE_RELEASE(pMeshContainer->ppTextures[numMat]);
		}

		SAFE_DELETE_ARRAY(pMeshContainer->ppTextures);
	}

	SAFE_RELEASE(pMeshContainer->MeshData.pMesh);
	SAFE_RELEASE(pMeshContainer->pOriMesh);

	if (pMeshContainer->pBoneBuffer != nullptr)
	{
		SAFE_RELEASE(pMeshContainer->pBoneBuffer);
		SAFE_DELETE_ARRAY(pMeshContainer->pBoneOffsetMatrices);
	}

	SAFE_DELETE(pMeshContainer);
	pMeshContainerBase = nullptr;

	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュのコンストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CSkinMesh::CSkinMesh() : m_MaterialFlg(FALSE),					// マテリアル変更フラグ
	m_renderMode(Render_Default),								// 描画モード
	m_nRenderTime(0),											// 描画時間
	m_AnimeTime(0),												// アニメーション時間
	m_AnimSpeed(SKIN_ANIME_SPEED),								// アニメーションスピード
	m_CurrentTrack(0),											// アニメーショントラック番号
	m_CurrentTrackDesc({ D3DXPRIORITY_LOW, 1, 1, 0, TRUE }),	// アニメーショントラックデータ
	m_bStopMotion(false)
{
	memset(&m_Material, 0, sizeof(D3DMATERIAL9));

	// シェーダーのハンドル初期設定
	CShaderManager* pEffect = CApplication::GetInstance()->GetShaderManager();
	m_handler.Technique = pEffect->GetTechniqueCache(shaderLabel, "Skin");
	m_handler.Texture = pEffect->GetParameterCache(shaderLabel, "Tex");
	m_handler.vLightDir = pEffect->GetParameterCache(shaderLabel, "vLightDir");
	m_handler.vDiffuse = pEffect->GetParameterCache(shaderLabel, "vDiffuse");
	m_handler.vAmbient = pEffect->GetParameterCache(shaderLabel, "vAmbient");
	m_handler.WVP = pEffect->GetParameterCache(shaderLabel, "wvp");
	m_handler.vCameraPos = pEffect->GetParameterCache(shaderLabel, "vCameraPos");
	m_handler.time = pEffect->GetParameterCache(shaderLabel, "renderTime");
	m_handler.numBlendMatrix = pEffect->GetParameterCache(shaderLabel, "numBlendMatrix");
	m_handler.isUseTexture = pEffect->GetParameterCache(shaderLabel, "isUseTex");
	m_handler.boneMatrix = pEffect->GetParameterCache(shaderLabel, "skinMatrix");
	m_handler.boneOffsetMatrix = pEffect->GetParameterCache(shaderLabel, "skinOffsetMatrix");
	m_handler.boneIndex = pEffect->GetParameterCache(shaderLabel, "skinIndex");
}

//***************************************************************
//
// スキンメッシュクラスの初期化関連
//
//***************************************************************

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュクラスの初期化処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT CSkinMesh::Load(std::string pMeshPath)
{
	m_pos = { 0.0f,0.0f,0.0f };
	m_rot = { 0.0f,0.0f,0.0f };
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// Xファイルからアニメーションメッシュを読み込み作成する
	if (FAILED(D3DXLoadMeshHierarchyFromX(pMeshPath.c_str(),
		D3DXMESH_MANAGED,
		pDevice,
		&m_cHierarchy,
		nullptr,
		&m_pFrameRoot,
		&m_pAnimController)))
	{
		MessageBox(NULL, "Xファイルの読み込みに失敗しました", pMeshPath.c_str(), MB_OK);
		return E_FAIL;
	}

	// ボーン行列初期化
	AllocateAllBoneMatrices(m_pFrameRoot, m_pFrameRoot);

	// アニメーショントラックの取得
	for (DWORD i = 0; i<m_pAnimController->GetNumAnimationSets(); i++)
	{
		// アニメーション取得
		m_pAnimController->GetAnimationSet(i, &(m_pAnimSet[i]));
	}
	// すべてのフレーム参照変数の生成
	m_FrameArray.clear();
	m_IntoMeshFrameArray.clear();
	CreateFrameArray(m_pFrameRoot);

	// フレーム配列にオフセット情報作成
	for (auto& frame : m_IntoMeshFrameArray)
	{
		MYMESHCONTAINER* pMyMeshContainer = (MYMESHCONTAINER*)frame->pMeshContainer;
		while (pMyMeshContainer) 
		{
			// スキン情報
			if (pMyMeshContainer->pSkinInfo) 
			{
				DWORD cBones = pMyMeshContainer->pSkinInfo->GetNumBones();
				for (DWORD iBone = 0; iBone<cBones; iBone++) 
				{
					// フレーム内から同じ名前のフレームを検索
					for (DWORD Idx = 0; Idx<m_FrameArray.size(); Idx++) 
					{
						if (strcmp(pMyMeshContainer->pSkinInfo->GetBoneName(iBone), m_FrameArray[Idx]->Name) == 0) 
						{
							
							//Offset行列
							m_FrameArray[Idx]->OffsetMat = *(pMyMeshContainer->pSkinInfo->GetBoneOffsetMatrix(iBone));
							m_FrameArray[Idx]->OffsetID = Idx;
							break;
						}
					}
				}
			}
			pMyMeshContainer = (MYMESHCONTAINER *)pMyMeshContainer->pNextMeshContainer;
		}
	}
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// HRESULT AllocateAllBoneMatrices( THING* pThing,LPD3DXFRAME pFrame )
// ボーン行列の初期化処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT CSkinMesh::AllocateAllBoneMatrices(LPD3DXFRAME pFrameRoot, LPD3DXFRAME pFrame)
{
	if (pFrame->pMeshContainer != nullptr)
	{	// 階層の走査(メモリを確保したメッシュコンテナ領域を探す処理)
		// ボーン行列の初期化処理
		if (FAILED(AllocateBoneMatrix(pFrameRoot, pFrame->pMeshContainer)))
		{
			return E_FAIL;
		}
	}
	if (pFrame->pFrameSibling != nullptr)
	{	// 再起判断処理
		if (FAILED(AllocateAllBoneMatrices(pFrameRoot, pFrame->pFrameSibling)))
		{
			return E_FAIL;
		}
	}
	if (pFrame->pFrameFirstChild != nullptr)
	{
		if (FAILED(AllocateAllBoneMatrices(pFrameRoot, pFrame->pFrameFirstChild)))
		{
			return E_FAIL;
		}
	}
	return S_OK;
}


//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 全てのフレームポインタ格納処理関数
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::CreateFrameArray(LPD3DXFRAME _pFrame)
{
	if (_pFrame == nullptr)
	{
		return;
	}

	// フレームアドレス格納
	MYFRAME* pMyFrame = (MYFRAME*)_pFrame;
	m_FrameArray.push_back(pMyFrame);

	// メッシュコンテナがある場合はIntoMeshFrameArrayにアドレスを格納
	if (pMyFrame->pMeshContainer != nullptr)
	{
		m_IntoMeshFrameArray.push_back(pMyFrame);
	}
	
	if (pMyFrame->pFrameFirstChild != nullptr)
	{	// 子フレーム検索
		CreateFrameArray(pMyFrame->pFrameFirstChild);
	}
	if (pMyFrame->pFrameSibling != nullptr)
	{	// 兄弟フレーム検索
		CreateFrameArray(pMyFrame->pFrameSibling);
	}
}

//***************************************************************
//
// スキンメッシュクラスの破棄関連
//
//***************************************************************

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュクラスの破棄
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::Release()
{
	if (m_pFrameRoot != nullptr)
	{
		// ボーンフレーム関係解放
		FreeAnim(m_pFrameRoot);
		// その他情報(テクスチャの参照データなど)の解放
		m_cHierarchy.DestroyFrame(m_pFrameRoot);
		m_pFrameRoot = nullptr;
	}

	// アニメーションコントローラー解放
	SAFE_RELEASE(m_pAnimController);

	// すべてのフレーム参照変数の要素を削除
	m_FrameArray.clear();
	m_FrameArray.shrink_to_fit();

	// メッシュコンテナありのフレーム参照変数の要素を削除
	m_IntoMeshFrameArray.clear();
	m_IntoMeshFrameArray.shrink_to_fit();
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ボーンのワールド行列格納処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT CSkinMesh::AllocateBoneMatrix(LPD3DXFRAME pFrameRoot, LPD3DXMESHCONTAINER pMeshContainerBase)
{
	MYFRAME *pFrame = nullptr;

	// メッシュコンテナの型をオリジナルの型として扱う
	// (メッシュコンテナ生成時にオリジナルの型として作っているので問題はないが、
	// 基本ダウンキャストは危険なので多用は避けるべき)
	MYMESHCONTAINER *pMeshContainer = (MYMESHCONTAINER*)pMeshContainerBase;

	// スキンメッシュでなければ
	if (pMeshContainer->pSkinInfo == nullptr)
	{
		return S_OK;
	}

	// ボーンの数取得
	DWORD dwBoneNum = pMeshContainer->pSkinInfo->GetNumBones();

	// 各ボーンのワールド行列格納用領域を確保
	SAFE_DELETE(pMeshContainer->ppBoneMatrix);
	pMeshContainer->ppBoneMatrix = new D3DXMATRIX*[dwBoneNum];

	// ボーンの数だけループ
	for (DWORD i = 0; i< dwBoneNum; i++)
	{
		// 子フレーム(ボーン)のアドレスを検索してpFrameに格納
		pFrame = static_cast<MYFRAME*>(D3DXFrameFind(pFrameRoot, pMeshContainer->pSkinInfo->GetBoneName(i)));

		// 子フレームがなければ処理を終了する
		if (pFrame == nullptr)
		{
			return E_FAIL;
		}

		// 各ボーンのワールド行列格納用変数に最終行列を格納
		pMeshContainer->ppBoneMatrix[i] = &pFrame->CombinedTransformationMatrix;
	}
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 全ての階層フレームを再帰的に解放
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::FreeAnim(LPD3DXFRAME pFrame)
{
	if (pFrame->pMeshContainer != nullptr)
	{	// メッシュコンテナが存在する場合
		m_cHierarchy.DestroyMeshContainer(pFrame->pMeshContainer);
		pFrame->pMeshContainer = nullptr;
	}

	if (pFrame->pFrameSibling != nullptr)
	{	// 兄弟フレームが存在する場合
		FreeAnim(pFrame->pFrameSibling);
	}
	if (pFrame->pFrameFirstChild != nullptr)
	{	// 子フレームが存在する場合
		FreeAnim(pFrame->pFrameFirstChild);
	}
}

//***************************************************************
//
// スキンメッシュクラスの更新関連
//
//***************************************************************

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュクラスの更新処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::Update()
{
	// 押しっぱなしによる連続切り替え防止
	static bool PushFlg = false; // ここでは仮でフラグを使用するが、本来はメンバ変数などにする



	if (!m_bStopMotion)
	{// アニメーション時間を更新
		m_AnimeTime++;
	}
}

//***************************************************************
//
// スキンメッシュクラスの描画関連
//
//***************************************************************

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュの描画
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::Draw() 
{
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);	// 行列初期化関数

	// 向きの反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);			// 行列回転関数
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);						// 行列掛け算関数 

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);				// 行列移動関数
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);					// 行列掛け算関数

	// 現在のアニメーション番号を適応
	m_pAnimController->SetTrackAnimationSet(0, m_pAnimSet[m_CurrentTrack]);

	// 0(再生中の)トラックからトラックデスクをセットする

	//-----------------------------------------------
	//ここでモーションの速度＆重さ＆優先順位を設定してます
	//----------------------------------------------
	m_pAnimController->SetTrackDesc(0, &(m_CurrentTrackDesc));

	if (!m_bStopMotion)
	{// アニメーション時間データの更新
		m_pAnimController->AdvanceTime(m_AnimSpeed, NULL);
	}

	// アニメーションデータを更新
	UpdateFrameMatrices(m_pFrameRoot, &m_mtxWorld);

	// アニメーション描画
	DrawFrame(m_pFrameRoot);

	// 0(再生中の)トラックから更新したトラックデスクを取得する
	m_pAnimController->GetTrackDesc(0, &m_CurrentTrackDesc);

	D3DXEVENTHANDLE event = m_pAnimController->GetCurrentPriorityBlend();
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// void UpdateFrameMatrices(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix)
// フレーム内のメッシュ毎にワールド変換行列を更新する
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::UpdateFrameMatrices(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix)
{
	MYFRAME *pFrame = static_cast<MYFRAME*>(pFrameBase);
	if (pParentMatrix != nullptr)
	{	// CombinedTransformationMatrixに最終行列を格納
		D3DXMatrixMultiply(&pFrame->CombinedTransformationMatrix, &pFrame->TransformationMatrix, pParentMatrix);
	}
	else
	{	// CombinedTransformationMatrixに最終行列を格納
		pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix;
	}

	if (pFrame->pFrameSibling != nullptr)
	{
		UpdateFrameMatrices(pFrame->pFrameSibling, pParentMatrix);
	}
	if (pFrame->pFrameFirstChild != nullptr)
	{
		UpdateFrameMatrices(pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// void DrawFrame(LPDIRECT3DDEVICE9 pDevice,LPD3DXFRAME pFrameBase)
// フレームをレンダリング
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::DrawFrame(LPD3DXFRAME pFrameBase)
{
	MYFRAME* pFrame = static_cast<MYFRAME*>(pFrameBase);
	MYMESHCONTAINER* pMeshContainer = (MYMESHCONTAINER*)pFrame->pMeshContainer;
	while (pMeshContainer != nullptr)
	{
		//SHADER_KIND a = GetpShader()->GetShaderKind();
		////シェーダを使用しているのなら専用の描画関数に飛ばす
		//if( GetpShader() != NULL && GetpShader()->GetShaderKind() == SHADER_KIND_LAMBERT ){
		DrawMaterial(pMeshContainer);
		//}
		//else{
		//RenderMeshContainer(pMeshContainer, pFrame);
		// }
		//次のメッシュコンテナへアクティブを移す
		pMeshContainer = (MYMESHCONTAINER*)pMeshContainer->pNextMeshContainer;
	}
	if (pFrame->pFrameSibling != nullptr)
	{
		DrawFrame(pFrame->pFrameSibling);
	}
	if (pFrame->pFrameFirstChild != nullptr)
	{
		DrawFrame(pFrame->pFrameFirstChild);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// void RenderMeshContainer(LPDIRECT3DDEVICE9 pDevice,MYMESHCONTAINER* pMeshContainer, MYFRAME* pFrame)
// フレーム内のそれぞれのメッシュをレンダリング
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::RenderMeshContainer(MYMESHCONTAINER* pMeshContainer, MYFRAME* pFrame)
{
	if (pMeshContainer->pSkinInfo == nullptr)
	{
		MessageBox(NULL, "スキンメッシュXファイルの描画に失敗しました。", NULL, MB_OK);
		exit(EOF);
	}

	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();
	DWORD i, k;
	UINT iMatrixIndex;
	D3DXMATRIX mStack;

	// ボーンテーブルからバッファの先頭アドレスを取得
	LPD3DXBONECOMBINATION pBoneCombination = reinterpret_cast<LPD3DXBONECOMBINATION>(pMeshContainer->pBoneBuffer->GetBufferPointer());

	//dwPrevBoneIDにUINT_MAXの値(0xffffffff)を格納
	DWORD dwPrevBoneID = UINT_MAX;

	// スキニング計算
	for (i = 0; i < pMeshContainer->dwBoneNum; i++)
	{
		DWORD dwBlendMatrixNum = 0;

		// 影響している行列数取得
		for (k = 0; k < pMeshContainer->dwWeight; k++)
		{
			if (pBoneCombination[i].BoneId[k] != UINT_MAX)
			{	// 現在影響を受けているボーンの数
				dwBlendMatrixNum = k;
			}
		}

		// ジオメトリブレンディングを使用するために行列の個数を指定
		pDevice->SetRenderState(D3DRS_VERTEXBLEND, dwBlendMatrixNum);

		// 影響している行列の検索
		for (k = 0; k < pMeshContainer->dwWeight; k++)
		{
			// iMatrixIndexに1度の呼び出しで描画出来る各ボーンを識別する値を格納
			// ( このBoneID配列の長さはメッシュの種類によって異なる
			// ( インデックスなしであれば　=　頂点ごとの重み であり
			// インデックスありであれば　=　ボーン行列パレットのエントリ数)

			// 現在のボーン(i番目)からみてk番目のボーンid
			iMatrixIndex = pBoneCombination[i].BoneId[k];

			// 行列の情報があれば
			if (iMatrixIndex != UINT_MAX)
			{
				// mStackにオフセット行列*ボーン行列を格納
				mStack = pMeshContainer->pBoneOffsetMatrices[iMatrixIndex] * (*pMeshContainer->ppBoneMatrix[iMatrixIndex]);

				// 行列スタックに格納
				pDevice->SetTransform(D3DTS_WORLDMATRIX(k), &mStack);
			}
		}

		D3DMATERIAL9 TmpMat = pMeshContainer->pMaterials[pBoneCombination[i].AttribId].MatD3D;
		TmpMat.Emissive.a = TmpMat.Diffuse.a = TmpMat.Ambient.a = 1.0f;
		pDevice->SetMaterial(&TmpMat);
		pDevice->SetTexture(0, pMeshContainer->ppTextures[pBoneCombination[i].AttribId]);

		// dwPrevBoneIDに属性テーブルの識別子を格納
		dwPrevBoneID = pBoneCombination[i].AttribId;

		// メッシュの描画
		pMeshContainer->MeshData.pMesh->DrawSubset(i);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーによる描画
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::DrawMaterial(MYMESHCONTAINER *pMeshContainer)
{
	CShaderManager* pShaderManager = CApplication::GetInstance()->GetShaderManager();
	LPD3DXEFFECT pEffect = pShaderManager->GetEffect(shaderLabel);
	if (pEffect == nullptr)
	{
		assert(false);
		return;
	}

	// カメラ情報
	CCamera* pCamera = CApplication::GetInstance()->GetCamera(); /*ここで取得*/

	D3DXMATRIX viewMatrix = pCamera->GetMtxView();
	D3DXMATRIX projMatrix = pCamera->GetMtxProj();

	//-------------------------------------------------
	// シェーダの設定
	//-------------------------------------------------
	pEffect->SetTechnique(m_handler.Technique);
	pEffect->Begin(NULL, 0);

	m_wvp = { m_mtxWorld, viewMatrix, projMatrix };
	pEffect->SetVector(m_handler.vCameraPos, &static_cast<D3DXVECTOR4>(pCamera->GetPosV()));

	// ライト情報
	CLight* lightClass = CApplication::GetInstance()->GetLight();

	// ライトの方向
	D3DXVECTOR3 vecLight = lightClass->GetLightVec(); // ライトの取得
	D3DXVECTOR4 lightDir = D3DXVECTOR4(vecLight.x, vecLight.y, vecLight.z, 0.0f);
	pEffect->SetVector(m_handler.vLightDir, &lightDir);

	pEffect->SetInt(m_handler.time, m_nRenderTime);

	switch (m_renderMode)
	{
	case ERenderMode::Render_Default:
		RenderDefault(pMeshContainer, pEffect);
		break;
	case ERenderMode::Render_Highlight:
		//RenderHighLight(pMeshContainer, pEffect);
		break;
	case ERenderMode::Render_Hologram:
		//RenderDefault(pMeshContainer, pEffect, 2);
		break;
	default:
		assert(false);
		break;
	}

	pEffect->End();	// デバイスの取得
	m_nRenderTime++;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 通常の描画
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::RenderDefault(MYMESHCONTAINER *pMeshContainer, LPD3DXEFFECT& effect, UINT pass)
{
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();
	LPD3DXBONECOMBINATION pBoneCombination;
	DWORD i, k;
	DWORD dwPrevBoneID;
	D3DXMATRIX mStack;

	//D3DXMATRIX mtxTrans;
	//D3DXMatrixIdentity(&mtxTrans);
	//D3DXMatrixTranslation(&mtxTrans, 800.0f,0.0f,20.0f);
	//D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	if (pMeshContainer->pSkinInfo != nullptr)
	{
		// ボーンテーブルからバッファの先頭アドレスを取得
		pBoneCombination = reinterpret_cast<LPD3DXBONECOMBINATION>(pMeshContainer->pBoneBuffer->GetBufferPointer());

		// dwPrevBoneIDにUINT_MAXの値(0xffffffff)を格納
		dwPrevBoneID = UINT_MAX;

		// スキニング計算
		for (i = 0; i < pMeshContainer->dwBoneNum; i++)
		{
			DWORD dwBlendMatrixNum = 0;
			std::vector<D3DXMATRIX> mtxStack(16, D3DXMATRIX());
			std::vector<int> mtxIndex(16, 0);

			for (k = 0; k < pMeshContainer->dwWeight; k++)
			{
				// 現在影響を受けているボーンの数
				dwBlendMatrixNum = k;
			}

			// ジオメトリブレンディングを使用するために行列の個数を指定
			pDevice->SetRenderState(D3DRS_VERTEXBLEND, dwBlendMatrixNum);

			// 影響している行列の検索
			for (k = 0; k < pMeshContainer->dwWeight; k++)
			{
				// iMatrixIndexに1度の呼び出しで描画出来る各ボーンを識別する値を格納
				// ( このBoneID配列の長さはメッシュの種類によって異なる
				// ( インデックスなしであれば　=　頂点ごとの重み であり
				// インデックスありであれば　=　ボーン行列パレットのエントリ数)
				// 現在のボーン(i番目)からみてk番目のボーンid
				UINT iMatrixIndex = pBoneCombination[i].BoneId[k];

				// 行列の情報があれば
				if (iMatrixIndex != UINT_MAX)
				{
					// mStackにオフセット行列*ボーン行列を格納
					mStack = pMeshContainer->pBoneOffsetMatrices[iMatrixIndex] * (*pMeshContainer->ppBoneMatrix[iMatrixIndex]);

					// 行列スタックに格納
					pDevice->SetTransform(D3DTS_WORLDMATRIX(k), &mStack);

					/*mtxStack[k] = mStack;
					mtxIndex[k] = k;*/
				}
			}

			// ダイイングメッセージ : シェーダーで描画するならこの関数やそれ以外の関数のコードを
			// 大幅に変える（生成時点のものも）
			/*effect->SetInt(m_handler.numBlendMatrix, static_cast<INT>(dwBlendMatrixNum));
			effect->SetIntArray(m_handler.boneIndex, mtxIndex.data(), mtxIndex.size());
			effect->SetMatrixArray(m_handler.boneMatrix, mtxStack.data(), mtxStack.size());*/
			 
			D3DMATERIAL9 TmpMat = pMeshContainer->pMaterials[pBoneCombination[i].AttribId].MatD3D;
			TmpMat.Emissive.a = TmpMat.Diffuse.a = TmpMat.Ambient.a = 1.0f;
			pDevice->SetMaterial(&TmpMat);

			pDevice->SetTexture(0, pMeshContainer->ppTextures[pBoneCombination[i].AttribId]);
			effect->SetBool(m_handler.isUseTexture, (pMeshContainer->ppTextures[pBoneCombination[i].AttribId] != nullptr));

			//dwPrevBoneIDに属性テーブルの識別子を格納
			dwPrevBoneID = pBoneCombination[i].AttribId;

			// モデルの色の設定 
			effect->SetValue(m_handler.vDiffuse, &pMeshContainer->pMaterials[dwPrevBoneID].MatD3D.Diffuse, sizeof(D3DCOLORVALUE));
			effect->SetValue(m_handler.vAmbient, &pMeshContainer->pMaterials[dwPrevBoneID].MatD3D.Ambient, sizeof(D3DCOLORVALUE));

			m_wvp.world = m_mtxWorld;
			effect->SetValue(m_handler.WVP, &m_wvp, sizeof(CShaderManager::WVP));

			effect->BeginPass(0);
			pMeshContainer->MeshData.pMesh->DrawSubset(i);	// モデルパーツの描画
			effect->EndPass();
		}
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// アウトライン付き描画
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::RenderHighLight(MYMESHCONTAINER *pMeshContainer, LPD3DXEFFECT& effect)
{
	/* 強調表示する場合はオブジェクトをリストの末尾に移動 */

	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

	// ステンシルテストの設定
	pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);
	pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);
	pDevice->SetRenderState(D3DRS_STENCILWRITEMASK, 0xff);
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);

	pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);
	pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
	pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_REPLACE);

	RenderDefault(pMeshContainer, effect);

	pDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_GREATER);

	for (int nCntMat = 0; nCntMat < (int)pMeshContainer->NumMaterials; nCntMat++)
	{
		effect->BeginPass(0);
		pMeshContainer->MeshData.pMesh->DrawSubset(nCntMat);	//モデルパーツの描画
		effect->EndPass();
	}

	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
}

//***************************************************************
//
// スキンメッシュクラスの検索や便利関数
//
//***************************************************************

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 対象のボーンを検索
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
MYFRAME* CSkinMesh::SearchBoneFrame(std::string _BoneName, D3DXFRAME* _pFrame)
{
	MYFRAME* pFrame = static_cast<MYFRAME*>(_pFrame);
	if (strcmp(pFrame->Name, _BoneName.c_str()) == 0)
	{
		return pFrame;
	}

	if (_pFrame->pFrameSibling != nullptr)
	{	// 兄弟フレームを検索
		pFrame = SearchBoneFrame(_BoneName, _pFrame->pFrameSibling);
		if (pFrame != nullptr)
		{
			return pFrame;
		}
	}
	if (_pFrame->pFrameFirstChild != nullptr)
	{	// 子フレームを検索
		pFrame = SearchBoneFrame(_BoneName, _pFrame->pFrameFirstChild);
		if (pFrame != nullptr)
		{
			return pFrame;
		}
	}
	return nullptr;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// モーションブレンドの設定
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::BlendAnimations(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix)
{
	MYFRAME *pFrame = static_cast<MYFRAME*>(pFrameBase);
	if (pParentMatrix != nullptr)
	{	// CombinedTransformationMatrixに最終行列を格納
		D3DXMatrixMultiply(&pFrame->CombinedTransformationMatrix, &pFrame->TransformationMatrix, pParentMatrix);
	}
	else
	{	// CombinedTransformationMatrixに最終行列を格納
		pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix;
	}

	if (pFrame->pFrameSibling != nullptr)
	{
		UpdateFrameMatrices(pFrame->pFrameSibling, pParentMatrix);
	}
	if (pFrame->pFrameFirstChild != nullptr)
	{
		UpdateFrameMatrices(pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ボーンのマトリックス取得( ボーンの名前 )
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
D3DXMATRIX CSkinMesh::GetBoneMatrix(std::string _BoneName)
{
	MYFRAME* pFrame = SearchBoneFrame(_BoneName, m_pFrameRoot);
	if (pFrame != nullptr) 
	{	// ボーンが見つかった場合
		// ボーン行列を返す
		return pFrame->CombinedTransformationMatrix;
	}
	else 
	{	// ボーンが見つからなかった場合
		// 単位行列を返す
		D3DXMATRIX TmpMatrix;
		D3DXMatrixIdentity(&TmpMatrix);
		return TmpMatrix;
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ボーンのマトリックスポインタ取得( ボーンの名前 )
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
D3DXMATRIX* CSkinMesh::GetpBoneMatrix(std::string _BoneName)
{
	//注意）RokDeBone用に設定(対象ボーンの一つ先の行列をとってくる)
	MYFRAME* pFrame = SearchBoneFrame(_BoneName, m_pFrameRoot);
	if (pFrame != nullptr) 
	{	// ボーンが見つかった場合
		return &pFrame->CombinedTransformationMatrix;
	}
	else 
	{
		return nullptr;
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// オブジェクトのアニメーション変更( 変更するアニメーション番号 )
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CSkinMesh::ChangeAnim(DWORD _NewAnimNum)
{
	if (m_CurrentTrack != _NewAnimNum)
	{
		// 新規アニメーションに変更
		m_CurrentTrack = _NewAnimNum;

		// アニメーションタイムを初期化
		m_AnimeTime = 0;

		// アニメーションを最初の位置から再生させる
		m_pAnimController->GetTrackDesc(0, &m_CurrentTrackDesc);
		m_CurrentTrackDesc.Position = 0;
		m_pAnimController->SetTrackDesc(0, &m_CurrentTrackDesc);
	}
}