//=============================================================================
// インクルードファイル
//=============================================================================
#include <assert.h>
#include "gimmick.h"
#include "move.h"
#include "agent.h"
#include"gimmicdoor.h"
#include"collision_rectangle3D.h"
#include"model_obj.h"
#include"model3D.h"
#include "game.h"
#include "application.h"
#include "model_data.h"

//=============================================================================
//								静的変数の初期化
//=============================================================================
const float CGimmicDoor::DEFAULT_OPEN_SPEED = 5.0f;		//ディフォルトの開くアニメーションのスピード
const float CGimmicDoor::DEFAULT_OPEN_DISTANCE = 50.0f;	//ディフォルトの開くときの距離


CGimmicDoor * CGimmicDoor::Create(const bool bOpenInXDirection)
{
	// オブジェクトインスタンス
	CGimmicDoor *pDoorObj = nullptr;

	// メモリの解放
	pDoorObj = new CGimmicDoor;

	// メモリの確保ができなかった
	assert(pDoorObj != nullptr);

	// 数値の初期化
	pDoorObj->Init();

	pDoorObj->IsOpeningInXDirection(bOpenInXDirection);

	// インスタンスを返す
	return pDoorObj;
}

CGimmicDoor::CGimmicDoor() : m_bOpenInXDir(true),
m_rot(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
{
}

CGimmicDoor::~CGimmicDoor()
{
}

HRESULT CGimmicDoor::Init()
{
	door = CModelObj::Create();
	assert(door != nullptr);
	door->SetType(49);
	door->SetPos(D3DXVECTOR3(150.0f, 0.f, 100.0f));
	door->SetObjType(CObject::OBJTYPE_3DMODEL);
	door->SetShadowDraw(false);
	D3DXVECTOR3 modelDoorGimmick = door->GetModel()->GetMyMaterial().size;
	pCollisionDoorGimmick = door->GetCollision();
	pCollisionDoorGimmick->SetSize(modelDoorGimmick);
	pCollisionDoorGimmick->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y * 0.5f, 0.0f));
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_move.x = 5.0f;
	m_hitModel= CModelObj::Create();
	m_IsLeft = false;
	SetPosDefault(D3DXVECTOR3(150.0f, 0.f, 100.0f));
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));

	pCollisionDoor = m_hitModel->GetCollision();
	
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 50.f, 0.0f);
	D3DXVECTOR3 size = D3DXVECTOR3(100.0f, 100.f, 100.0f);

	if (m_hitModel)
	{
		m_hitModel->SetPos(D3DXVECTOR3(150.0f, 0.f, 100.0f));
		m_hitModel->SetObjType(CObject::EObjectType::OBJETYPE_NON_SOLID_GIMMICK);
	}

	if (pCollisionDoor)
	{//nullチェック
		pCollisionDoor->SetPos(pos);					//コリジョンのサイズの位置の設定
		pCollisionDoor->SetSize(size);				//コリジョンのサイズの設定
	}
	//// 移動クラスのメモリ確保
	//pMove = new CMove;
	//assert(pMove != nullptr);
	//pMove->SetMoving(1.2f, 50.0f, 1.0f, 0.1f);

	//ドア開閉時のタイプ設定　大事！！
	bSetType(Type::Type_Player);

	return S_OK;
}

void CGimmicDoor::Update()
{
	switch (m_type)
	{
	case CGimmicDoor::Type_Player:
		//あたってたらデータを保存
		SetAnimation(pCollisionDoor->Collision(CObject::OBJETYPE_PLAYER, false));
		break;
	case CGimmicDoor::Type_Enemy:
		//あたってたらデータを保存
		SetAnimation(pCollisionDoor->Collision(CObject::OBJETYPE_ENEMY, false));
		break;
	case CGimmicDoor::Type_None:
		//あたってたらデータを保存

		break;
	default:
		break;
	}
	
	//開く/閉まるアニメーション
	Animation();
}

// 位置のセッター
void CGimmicDoor::SetPos(const D3DXVECTOR3 & pos)
{
	if (door)
	{
		door->SetPos(pos);
	}
}

// 位置の設定
void CGimmicDoor::SetPosDefault(D3DXVECTOR3 pos)
{
	m_posDefault = pos;

	if (m_hitModel)
	{
		m_hitModel->SetPos(pos);
	}
	if (door)
	{
		door->SetPos(pos);
	}
}

//エフェクトのインデックスの設定
void CGimmicDoor::SetRenderMode(int mode)
{
	if (door)
		door->SetRenderMode(mode);
}

// モデルの種類の設定
void CGimmicDoor::SetModelType(int nIdx, const bool bRot)
{
	if (!door)
		return;

	if (nIdx < 0 || nIdx >= CModel3D::GetMaxModel())
		nIdx = 49;

	door->SetType(nIdx);

	CCollision_Rectangle3D* pCollision = door->GetCollision();
	CModel3D* pModel = door->GetModel();

	if (pCollision && pModel)
	{
		D3DXVECTOR3 size = pModel->GetMyMaterial().size, pos = D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f);

		if (!bRot)
		{
			pCollision->SetSize(size);

			if (pCollisionDoor)
			{
				pCollisionDoor->SetSize(D3DXVECTOR3(size.x, size.y, size.z + 100.0f));
			}
		}
		else
		{
			size = D3DXVECTOR3(size.z, size.y, size.x);
			pCollision->SetSize(size);

			if (pCollisionDoor)
			{
				pCollisionDoor->SetSize(D3DXVECTOR3(size.x + 100.0f, size.y, size.z));
			}
		}

		pCollision->SetPos(pos);
	}
}

//X方向に開くかどうかの設定処理
void CGimmicDoor::IsOpeningInXDirection(const bool bOpenInXDir)
{
	m_bOpenInXDir = bOpenInXDir;		//開く方向の設定

	if (bOpenInXDir)
	{//X方向に開く場合

		m_move = D3DXVECTOR3(0.0f, 0.0f, DEFAULT_OPEN_SPEED);	//アニメーションの速度の設定

		if (door)
		{//nullチェック

			door->SetRot(D3DXVECTOR3(0.0f, D3DX_PI * 0.5f, 0.0f));			//モデルの回転角度の設定

			D3DXVECTOR3 modelDoorGimmick = door->GetModel()->GetMyMaterial().size;	//モデルサイズの設定
			CCollision_Rectangle3D* pCollision = door->GetCollision();				//当たり判定の取得

			if (pCollision)
			{//nullチェック
				pCollision->SetSize(modelDoorGimmick);								//当たり判定のサイズの設定
			}

			if (pCollisionDoor)
			{
				pCollisionDoor->SetSize(D3DXVECTOR3(100.0f, modelDoorGimmick.y, modelDoorGimmick.z));
			}
		}
	}
	else
	{	//Z方向に開く場合
		m_move = D3DXVECTOR3(DEFAULT_OPEN_SPEED, 0.0f, 0.0f);		//アニメーションの速度の設定

		if (door)
		{	//nullチェック
			
			D3DXVECTOR3 modelDoorGimmick = door->GetModel()->GetMyMaterial().size;							//モデルサイズの取得
			D3DXVECTOR3 size = D3DXVECTOR3(modelDoorGimmick.z, modelDoorGimmick.y, modelDoorGimmick.x);		//モデルサイズの設定

			CCollision_Rectangle3D* pCollision = door->GetCollision();				//当たり判定の取得

			if (pCollision)
			{//nullチェック

				pCollision->SetSize(size);											//当たり判定のサイズの設定
			}

			if (pCollisionDoor)
			{
				pCollisionDoor->SetSize(D3DXVECTOR3(size.x, modelDoorGimmick.y, 100.0f));
			}
		}
	}
}

//開く/閉まるアニメーション
void CGimmicDoor::Animation()
{
	D3DXVECTOR3 pos = GetPos(), size = door->GetModel()->GetMyMaterial().size;

	if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_log >= 0 && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.mId >= 0)
	{
		if (GetAnimation() || CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
		{	//オンラインのデータかローカルのデータがtrueだったら動く

			if (m_bOpenInXDir)
			{
				if ((pos.z >= m_posDefault.z - size.x))
				{
					pos.x += -m_move.x;
					pos.z += -m_move.z;
					SetPos(pos);
				}
			}
			else
			{
				if ((pos.x >= m_posDefault.x - size.x))
				{
					pos.x += -m_move.x;
					pos.z += -m_move.z;
					SetPos(pos);
				}
			}
		}
		else if (!GetAnimation())
		{
			if (m_bOpenInXDir)
			{
				if (pos.z <= m_posDefault.z)
				{
					pos.x += m_move.x;
					pos.z += m_move.z;
					SetPos(pos);
				}
			}
			else if (!m_bOpenInXDir)
			{
				if (pos.x <= m_posDefault.x)
				{
					pos.x += m_move.x;
					pos.z += m_move.z;
					SetPos(pos);
				}
			}
		}
	}	
}

D3DXVECTOR3 CGimmicDoor::Move()
{
	// 移動情報の計算
	pMove->Moving(m_move);
	m_move = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 移動情報の取得
	D3DXVECTOR3 moveing = pMove->GetMove();
	return moveing;
}



