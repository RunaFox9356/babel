//=============================================================================
//
// ポリゴンクラス(polygon.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================
#ifndef _POLYGON_H_		// このマクロ定義がされてなかったら
#define _POLYGON_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "object.h"
#include "texture.h"
#include <vector>

//=============================================================================
// ポリゴンクラス
// Author : 唐�ｱ結斗
// 概要 : ポリゴン生成を行うクラス
//=============================================================================
class CPolygon : public CObject
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CPolygon(int nPriority = PRIORITY_LEVEL0);
	~CPolygon();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;																		// 初期化
	void Uninit() override;																			// 終了
	void Update() override;																			// 更新
	void Draw() override;																			// 描画
	void SetPos(const D3DXVECTOR3 &pos) override;													// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &posOld) override { m_posOld = posOld; }						// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;													// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size) override;													// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return m_pos; }													// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return m_posOld; }											// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return m_rot; }												// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return m_size; }												// 大きさのゲッター
	std::vector<int> GetTexData() { return m_nNumTex; }
	virtual void SetColor(const D3DXCOLOR &color);													// 色の設定
	D3DXCOLOR GetColor() { return m_color; }														// 色の取得
	virtual void SetVtx() = 0;																		// 頂点座標などの設定
	virtual void SetTex(int nTex, const D3DXVECTOR2 &minTex, const D3DXVECTOR2 &maxTex) = 0;		// テクスチャ座標の設定
	void LoadTex(const std::vector<int> nNumTex) { m_nNumTex = nNumTex; }							// テクスチャの設定
	void LoadTex(const int nNumTex) { m_nNumTex.clear();  m_nNumTex.push_back(nNumTex); }			// テクスチャの設定
	void LoadTex(const int nNumTex1, const int nNumTex2);											// テクスチャの設定
	void SetTexture(std::vector<LPDIRECT3DTEXTURE9> tex) { m_pTexture = tex; }
	void SetTexture(LPDIRECT3DTEXTURE9 tex) { m_pTexture.clear();  m_pTexture.push_back(tex); }
	void SetTexture(LPDIRECT3DTEXTURE9 tex1 , LPDIRECT3DTEXTURE9 tex2);

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<LPDIRECT3DTEXTURE9>		m_pTexture;			// テクスチャポインタ
	std::vector<int>					m_nNumTex;			// テクスチャの種別
	D3DXVECTOR3							m_pos;				// 位置
	D3DXVECTOR3							m_posOld;			// 過去位置
	D3DXVECTOR3							m_rot;				// 向き
	D3DXVECTOR3							m_size;				// 大きさ
	D3DXCOLOR							m_color;			// カラー
};

#endif



