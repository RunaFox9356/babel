//=============================================================================
//
// SaveDataManager.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "mapDataManager.h"
#include "mesh.h"
#include "model_obj.h"
#include "application.h"
#include "gimmick.h"



//コンストラクタ
CMapDataManager::CMapDataManager()
{
	//ベクトルをクリアする
	m_mapData.clear();
	m_mapData.shrink_to_fit();
}

//デストラクタ
CMapDataManager::~CMapDataManager()
{

}


//初期化
HRESULT CMapDataManager::Init()
{
	return S_OK;
}

//終了
void CMapDataManager::Uninit()
{
	//ベクトルをクリアする
	m_mapData.clear();
	m_mapData.shrink_to_fit();
}

//データの入力
void CMapDataManager::LoadData(void)
{
	//ストリンクのベクトルの宣言
	std::vector<std::string> vPath;

	LoadFilePaths(vPath);				//マップのファイルパスの読み込み

	for (int nCnt = 0; nCnt < (int)vPath.size(); nCnt++)
	{
		//マップのデータを読み込む
		LoadMapData(vPath.data()[nCnt]);
		ClearAllPointers(nCnt);
	}

	//読み込んだマップ数の設定
	m_nMaxMap = m_mapData.size();
}

//マップデータの取得
const CMapDataManager::MAP_DATA CMapDataManager::GetMapData(const int nIdx)
{
	int idx = nIdx;

	//マップインデックスの確認
	if (nIdx < 0 || nIdx >= (int)m_mapData.size())
		idx = 0;

	return m_mapData.data()[idx];		//マップデータを返す
}

//生成したメッシュフィールドへのポインタの保存処理
void CMapDataManager::SetPointerToField(const int nMapIdx, const int nCreatedIdx, CMesh3D * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx > (int)m_mapData.data()[nMapIdx].vMeshData.size())
		return;

	m_mapData.data()[nMapIdx].vMeshData.data()[nCreatedIdx].pMesh = pObj;		//メッシュフィールドのポインタを保存する
}

//生成したモデルへのポインタの保存処理
void CMapDataManager::SetPointerToModel(const int nMapIdx, const int nCreatedIdx, CModelObj * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx >(int)m_mapData.data()[nMapIdx].vModelData.size())
		return;

	m_mapData.data()[nMapIdx].vModelData.data()[nCreatedIdx].pModel = pObj;		//モデルのポインタを保存する
}

//生成したエネミーへのポインタの保存処理
void CMapDataManager::SetPointerToEnemy(const int nMapIdx, const int nCreatedIdx, CEnemy * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx >(int)m_mapData.data()[nMapIdx].vEnemyData.size())
		return;

	m_mapData.data()[nMapIdx].vEnemyData.data()[nCreatedIdx].pEnemy = pObj;		//エネミーのポインタを保存する
}

//生成したギミックへのポインタの保存処理
void CMapDataManager::SetPointerToGimmick(const int nMapIdx, const int nCreatedIdx, CGimmick * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx >(int)m_mapData.data()[nMapIdx].vGimmickData.size())
		return;

	m_mapData.data()[nMapIdx].vGimmickData.data()[nCreatedIdx].pGimmick = pObj;		//ギミックのポインタを保存する
}

//生成した壁へのポインタの保存処理
void CMapDataManager::SetPointerToWall(const int nMapIdx, const int nCreatedIdx, CWall * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx >(int)m_mapData.data()[nMapIdx].vWallData.size())
		return;

	m_mapData.data()[nMapIdx].vWallData.data()[nCreatedIdx].pWall = pObj;		//ギミックのポインタを保存する
}

//生成した壁へのポインタの保存処理
void CMapDataManager::SetPointerToWall(const int nMapIdx, const int nCreatedIdx, CSecurityCamera * pObj)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() || nCreatedIdx < 0 || nCreatedIdx >(int)m_mapData.data()[nMapIdx].vWallData.size())
		return;

	m_mapData.data()[nMapIdx].vWallData.data()[nCreatedIdx].pCamera = pObj;		//ギミックのポインタを保存する
}

//ハッカーが探すオブジェクトのポインタの保存処理
void CMapDataManager::SetToFindPointer(const int nMapIdx, CTask::HACKER_TASK_TYPE type, CObject * pPointer)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size())
		return;

	OBJ_TO_FIND_DATA data;

	data.pObj = pPointer;
	data.type = type;
	data.bFound = false;
	data.bSent = false;
	data.bGot = false;

	m_mapData.data()[nMapIdx].vToFindData.push_back(data);
}

//マップのポインタのクリア処理
void CMapDataManager::ClearAllPointers(const int nMapIdx)
{
	//マップインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size())
		return;

	//全部のメッシュフィールドのポインタをnullにする
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vMeshData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vMeshData.data()[nCnt].pMesh = nullptr;
	}
	//全部のモデルのポインタをnullにする
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vModelData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vModelData.data()[nCnt].pModel = nullptr;
	}
	//全部のエネミーのポインタをnullにする
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vEnemyData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vEnemyData.data()[nCnt].pEnemy = nullptr;
	}
	//全部のギミックのポインタをnullにする
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vGimmickData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vGimmickData.data()[nCnt].pGimmick = nullptr;
	}
	//全部の壁のポインタをnullにする
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vWallData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vWallData.data()[nCnt].pWall = nullptr;
		m_mapData.data()[nMapIdx].vWallData.data()[nCnt].pCamera = nullptr;
	}
	for (int nCnt = 0; nCnt < (int)m_mapData.data()[nMapIdx].vToFindData.size(); nCnt++)
	{
		m_mapData.data()[nMapIdx].vToFindData.clear();
		m_mapData.data()[nMapIdx].vToFindData.shrink_to_fit();
	}
}

//オブジェクトを破棄する
void CMapDataManager::DeleteObj(const int nMapIdx, const int nIdx)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() && nIdx >= 0 && nIdx < (int)m_mapData.data()[nMapIdx].vToFindData.size())
		return;

	m_mapData.data()[nMapIdx].vToFindData.data()[nIdx].bGot = true;
	m_mapData.data()[nMapIdx].vToFindData.data()[nIdx].pObj = nullptr;
}

//ハッカーが探すオブジェクトが見つかったフラグをtrueにする処理
void CMapDataManager::FoundObj(const int nMapIdx, const int nIdx)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() && nIdx >= 0 && nIdx < (int)m_mapData.data()[nMapIdx].vToFindData.size())
		return;

	m_mapData.data()[nMapIdx].vToFindData.data()[nIdx].bFound = true;
}

//ハッカーが探すオブジェクトのデータが送信されたにする処理
void CMapDataManager::SentObjData(const int nMapIdx, const int nIdx)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() && nIdx >= 0 && nIdx < (int)m_mapData.data()[nMapIdx].vToFindData.size())
		return;

	m_mapData.data()[nMapIdx].vToFindData.data()[nIdx].bSent = true;
}

//ハッカーが探すオブジェクトのタスクがエージェントに達成したフラグをtrueにする処理
void CMapDataManager::GotObj(const int nMapIdx, const int nIdx)
{
	//全部のインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= (int)m_mapData.size() && nIdx >= 0 && nIdx < (int)m_mapData.data()[nMapIdx].vToFindData.size())
		return;

	m_mapData.data()[nMapIdx].vToFindData.data()[nIdx].bGot = true;
}




//=============================================================================
//
//								静的関数
//
//=============================================================================




//生成処理
CMapDataManager* CMapDataManager::Create()
{
	CMapDataManager* pManager = new CMapDataManager;		//インスタンスを生成する

	if (FAILED(pManager->Init()))
	{//初期化処理
		return nullptr;
	}

	return pManager;			//生成したインスタンスを返す
}





//=============================================================================
//
//							プライベート関数
//
//=============================================================================




//マップデータのファイルパスの読み込み
void CMapDataManager::LoadFilePaths(std::vector<std::string>& vPath)
{
	std::ifstream strm("data\\FILE\\LoadMapPaths.txt");		//ファイルを開く

	if (strm.good())
	{//ファイルを開くことができたら

		char aStr[MAX_PATH] = {};
		int nSecurityCheck = 0;

		while (true)
		{
			strm >> aStr;

			if (strcmp(aStr, "END_SCRIPT") == 0)
				break;
			else if (strcmp(aStr, "MODEL_FILENAME") == 0)
			{
				nSecurityCheck = 0;
				std::string str;

				strm >> aStr;
				strm >> aStr;
				str = aStr;

				vPath.push_back(str);
			}
			else
				nSecurityCheck++;

			if (nSecurityCheck >= 100)
				break;
		}

	}
	else
	{
		LPCSTR lpStr = "ファイルを読み込むことに失敗しました。\ndata/FILE/LoadMapPaths.txt　というファイルがあるかどうかご確認ください。";
		MessageBox(CApplication::GetInstance()->GetWnd(), lpStr, NULL, MB_OK | MB_ICONERROR);
	}
}

//マップデータの読み込み
void CMapDataManager::LoadMapData(std::string path)
{
	std::ifstream strm(path);

	if (strm.good())
	{
		MAP_DATA mapData;
		char aStr[MAX_PATH] = {};
		int aType[DATA_TYPE_MAX] = { 0, 0, 0, 0, 0 };
		mapData.agentPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		while (true)
		{
			strm >> aStr;

			if (strcmp(aStr, "END_SCRIPT") == 0)
				break;
			else if (strcmp(aStr, "MESHFIELD") == 0)
			{
				MESHFIELD_DATA data;

				while (true)
				{
					strm >> aStr;

					if (strcmp(aStr, "END_FIELD") == 0)
						break;
					else if (strcmp(aStr, "POS") == 0)
					{
						strm >> data.pos.x;
						strm >> data.pos.y;
						strm >> data.pos.z;
					}
					else if (strcmp(aStr, "BLOCKS") == 0)
					{
						strm >> data.blockNum.x;
						strm >> data.blockNum.y;
					}
					else if (strcmp(aStr, "FIELD_SIZE") == 0)
					{
						strm >> data.fieldSize.x;
						strm >> data.fieldSize.y;
						strm >> data.fieldSize.z;
					}
					else if (strcmp(aStr, "COLOR") == 0)
					{
						strm >> data.color.r;
						strm >> data.color.g;
						strm >> data.color.b;
						strm >> data.color.a;
					}
					else if (strcmp(aStr, "TEXTURE") == 0)
					{
						strm >> data.nTextureIdx;
					}
					else if (strcmp(aStr, "SPLIT_TEXTURE") == 0)
					{
						char aSplit[16] = {};

						strm >> aSplit;

						if (strcmp(aSplit, "YES") == 0)
							data.bSplitTexture = true;
						else
							data.bSplitTexture = false;
					}
					else if (strcmp(aStr, "VERTEX_HEIGHT") == 0)
					{
						int nVtx = 0;

						strm >> nVtx;

						for (int nCnt = 0; nCnt < nVtx; nCnt++)
						{
							float fHeight = 0.0f;

							strm >> fHeight;
							data.fVtxHeight.push_back(fHeight);
						}
					}
				}

				data.nCreatedIdx = aType[DATA_TYPE_FIELD];
				aType[DATA_TYPE_FIELD]++;
				mapData.vMeshData.push_back(data);
			}
			else if (strcmp(aStr, "MODEL") == 0)
			{
				MODEL_DATA data;
				data.bIgnoreDistance = false;

				while (true)
				{
					strm >> aStr;

					if (strcmp(aStr, "END_MODEL") == 0)
						break;
					else if (strcmp(aStr, "POS") == 0)
					{
						strm >> data.pos.x;
						strm >> data.pos.y;
						strm >> data.pos.z;
					}
					else if (strcmp(aStr, "ROT") == 0)
					{
						strm >> data.rot.x;
						strm >> data.rot.y;
						strm >> data.rot.z;
					}
					else if (strcmp(aStr, "TYPE") == 0)
					{
						strm >> data.nType;
					}
					else if (strcmp(aStr, "EFFECT") == 0)
					{
						strm >> data.nEffectIdx;
					}
					else if (strcmp(aStr, "SHADOW") == 0)
					{
						char aShadow[16] = {};

						strm >> aShadow;

						if (strcmp(aStr, "YES") == 0)
							data.bShadow = true;
						else
							data.bShadow = false;
					}
					else if (strcmp(aStr, "HITBOX") == 0)
					{
						char aHitbox[16] = {};

						strm >> aHitbox;

						if (strcmp(aHitbox, "YES") == 0)
							data.bHitbox = true;
						else
						{
							data.bHitbox = false;
						}
					}
					else if (strcmp(aStr, "IGNORE_DISTANCE") == 0)
					{
						char aIgnore[16] = {};

						strm >> aIgnore;

						if (strcmp(aIgnore, "YES") == 0)
							data.bIgnoreDistance = true;
						else
						{
							data.bIgnoreDistance = false;
						}
					}
					else if (strcmp(aStr, "HITBOX_POS") == 0)
					{
						strm >> data.hitboxPos.x;
						strm >> data.hitboxPos.y;
						strm >> data.hitboxPos.z;
					}
					else if (strcmp(aStr, "HITBOX_SIZE") == 0)
					{
						strm >> data.hitboxSize.x;
						strm >> data.hitboxSize.y;
						strm >> data.hitboxSize.z;
					}
				}

				data.nCreatedIdx = aType[DATA_TYPE_MODEL];
				aType[DATA_TYPE_MODEL]++;
				mapData.vModelData.push_back(data);
			}
			else if (strcmp(aStr, "ENEMY") == 0)
			{
				ENEMY_DATA data;

				while (true)
				{
					strm >> aStr;

					if (strcmp(aStr, "END_ENEMY") == 0)
						break;
					else if (strcmp(aStr, "POS") == 0)
					{
						strm >> data.pos.x;
						strm >> data.pos.y;
						strm >> data.pos.z;
					}
					else if (strcmp(aStr, "ROT") == 0)
					{
						strm >> data.rot.x;
						strm >> data.rot.y;
						strm >> data.rot.z;
					}
					else if (strcmp(aStr, "TYPE") == 0)
					{
						strm >> data.nType;
					}
					else if (strcmp(aStr, "WAKE_RADIUS") == 0)
					{
						strm >> data.fWakeRadius;
					}
					else if (strcmp(aStr, "PATROLLING_POINTS") == 0)
					{
						D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

						strm >> pos.x;
						strm >> pos.y;
						strm >> pos.z;

						data.vPatrollingPoint.push_back(pos);
					}

				}

				data.nCreatedIdx = aType[DATA_TYPE_ENEMY];
				aType[DATA_TYPE_ENEMY]++;
				mapData.vEnemyData.push_back(data);
			}
			else if (strcmp(aStr, "GIMMICK") == 0)
			{
				GIMMICK_DATA data;
				data.nModelType = -1;

				while (true)
				{
					strm >> aStr;

					if (strcmp(aStr, "END_GIMMICK") == 0)
						break;
					else if (strcmp(aStr, "POS") == 0)
					{
						strm >> data.pos.x;
						strm >> data.pos.y;
						strm >> data.pos.z;
					}
					else if (strcmp(aStr, "ROT") == 0)
					{
						strm >> data.rot.x;
						strm >> data.rot.y;
						strm >> data.rot.z;
					}
					else if (strcmp(aStr, "TYPE") == 0)
					{
						strm >> data.nType;
					}
					else if (strcmp(aStr, "EFFECT") == 0)
					{
						strm >> data.nEffectIdx;
					}
					else if (strcmp(aStr, "MODEL_TYPE") == 0)
					{
						strm >> data.nModelType;
					}
					else if (strcmp(aStr, "ROT_SWITCH") == 0)
					{
						strm >> data.nRot;
					}
					else if (strcmp(aStr, "EXTRA_OBJ_POS") == 0)
					{
						strm >> data.extraPos.x;
						strm >> data.extraPos.y;
						strm >> data.extraPos.z;
					}
				}

				data.nCreatedIdx = aType[DATA_TYPE_GIMMICK];
				aType[DATA_TYPE_GIMMICK]++;
				mapData.vGimmickData.push_back(data);
			}
			else if (strcmp(aStr, "WALL") == 0)
			{
				WALL_DATA data;
				GIMMICK_DATA pcData;	

				data.pCamera = nullptr;
				data.bCamera = false;
				data.bPc = false;
				data.nPcIdx = -1;

				while (true)
				{
					strm >> aStr;

					if (strcmp(aStr, "END_WALL") == 0)
						break;
					else if (strcmp(aStr, "POS") == 0)
					{
						strm >> data.pos.x;
						strm >> data.pos.y;
						strm >> data.pos.z;
					}
					else if (strcmp(aStr, "ROT_SWITCH") == 0)
					{
						strm >> data.nRot;
					}
					else if (strcmp(aStr, "TYPE") == 0)
					{
						strm >> data.nType;
					}
					else if (strcmp(aStr, "USE_CAMERA") == 0)
					{
						char aCamera[5] = "";

						strm >> aCamera;

						if (strcmp(aCamera, "YES") == 0)
							data.bCamera = true;
					}
					else if (strcmp(aStr, "CAMERA_POS") == 0)
					{
						strm >> data.cameraPos.x;
						strm >> data.cameraPos.y;
						strm >> data.cameraPos.z;
					}
					else if (strcmp(aStr, "CAMERA_INCLINATION") == 0)
					{
						strm >> data.fCameraInclination;
					}
					else if (strcmp(aStr, "USE_PC") == 0)
					{
						char aPc[5] = "";

						strm >> aPc;

						if (strcmp(aPc, "YES") == 0)
							data.bPc = true;
						else
							data.nPcIdx = -1;

						pcData.nType = CGimmick::GIMMICK_TYPE_HACK_PC;
					}
					else if (strcmp(aStr, "PC_RELATIVE_POS") == 0)
					{
						D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

						strm >> pos.x;
						strm >> pos.y;
						strm >> pos.z;

						pcData.pos = data.pos + pos;
					}
					else if (strcmp(aStr, "PC_ROTATION_SWITCH") == 0)
					{
						strm >> pcData.nRot;

						D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

						if (pcData.nRot == 1)
							rot.y = D3DX_PI * 0.5f;
						else if (pcData.nRot == 2)
							rot.y = D3DX_PI;
						else if (pcData.nRot == 3)
							rot.y = D3DX_PI * 1.5f;

						pcData.rot = rot;
					}
				}

				data.nCreatedIdx = aType[DATA_TYPE_WALL];

				if (data.bPc)
				{
					pcData.nCreatedIdx = aType[DATA_TYPE_GIMMICK];
					data.nPcIdx = aType[DATA_TYPE_GIMMICK];
					aType[DATA_TYPE_GIMMICK]++;
					mapData.vGimmickData.push_back(pcData);
				}

				aType[DATA_TYPE_WALL]++;
				mapData.vWallData.push_back(data);
			}
			else if (strcmp(aStr, "PLAYER_SPAWN_POS") == 0)
			{
				strm >> mapData.agentPos.x;
				strm >> mapData.agentPos.y;
				strm >> mapData.agentPos.z;
			}
		}

		m_mapData.push_back(mapData);
	}
}
