#include "application.h"
#include "particle_manager.h"
#include "file.h"
#include "utility.h"
#include "fileutil.h"

namespace
{
	// 列挙型の読み込みをする処理
	template<typename T>
	inline void SafeLoadEnum(nlohmann::json check, std::string str, T& value)
	{
		if (check[str].empty())
		{
			value = utility::IntToEnum<T>(0);
		}
		else
		{
			value = utility::IntToEnum<T>(check[str]);
		}
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
//パーティクルマネージャのコンストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CParticleManager::CParticleManager()
{
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルマネージャのデストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CParticleManager::~CParticleManager()
{
	assert(m_data.empty());
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルマネージャの初期化
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT CParticleManager::Init()
{
	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルマネージャの解放
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::ReleaseAll()
{
	for (auto& p : m_data)
	{
		if (!p.second.empty())
		{
			p.second.clear();
		}
	}
	m_data.clear();
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ファイル読み込み
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::LoadJson(nlohmann::json& list)
{
	size_t size = list["Object"].size();

	std::vector <CParticleManager::SBundleData> data = {};
	data.resize(size);

	for (size_t i = 0; i < size; i++)
	{
		int objectType = 0;

		data[i].emitterData.particleTag = list["DataTag"];
		objectType					    = list["Object"][i]["ParticleType"];

		data[i].emitterData.objType = static_cast<CParticleEmitter::EObjectType>(objectType);

		switch (data[i].emitterData.objType)
		{
		case CParticleEmitter::OBJECT_BILLBOARD:
			LoadJsonBillboard(list["Object"][i], data[i]);
			break;
		}
	}

	m_data.insert(std::make_pair(list["DataTag"], data));
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ファイルの全ての要素を読み込み
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::LoadAll()
{
	nlohmann::json list = CFile::LoadJson(L"Data/FILE/particle.json");

	size_t size = list["PARTICLE"].size();
	for (size_t i = 0; i < size; i++)
	{
		if (list["PARTICLE"][i]["DataTag"].empty())
		{
			continue;
		}

		LoadJson(list["PARTICLE"].at(i));
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// テキストファイルのデータ読み込み
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::LoadText(std::string path, std::map<std::string, std::vector<SBundleData>, std::less<>>& data)
{
	std::string name = "Data/FILE/" + path;
	std::ifstream file(name);

	std::map<std::string, std::vector<SBundleData>, std::less<>> bundleData = {};

	std::vector<std::string> tag(32, "");
	std::string line;
	int		    index	  = 0;
	int		    tagIndex  = 0;
	int		    dataIndex = 0;

	while (std::getline(file, line))
	{	// 一行ずつ読み込む
		// 空白を削除
		line.erase(std::remove_if(line.begin(), line.end(), [](char c) { return std::isspace(c); }), line.end());

		std::stringstream ss(line);

		if (fileutil::ReadString(ss, "Tag", tag[tagIndex]))
		{
			dataIndex = tagIndex;
			tagIndex++;
			continue;
		}

		if (fileutil::ReadInt(ss, "Index", index))		// インデックス
		{
			bundleData[tag[dataIndex]].resize(index + 1);
			continue;
		}

		bundleData[tag[dataIndex]][index].emitterData.emitterIndex = index;
		int behaviorIndex;
		int setRandomPosIndex;
		int lockPosIndex;
		int blendIndex;

		fileutil::ReadBool(ss, "UseRandom", bundleData[tag[dataIndex]][index].info.useRandom);							// ランダムを使用するかどうか
		fileutil::ReadVector3(ss, "RandomVelocityMin", bundleData[tag[dataIndex]][index].info.random.randomVelocityMin);	// 移動量のランダムの最低値
		fileutil::ReadVector3(ss, "RandomVelocityMax", bundleData[tag[dataIndex]][index].info.random.randomVelocityMax);	// 移動量のランダムの最大値
		fileutil::ReadFloat(ss, "RandomRotateMin", bundleData[tag[dataIndex]][index].info.random.randomRotate.y);			// 回転のランダムの最低値
		fileutil::ReadFloat(ss, "RandomRotateMax", bundleData[tag[dataIndex]][index].info.random.randomRotate.x);			// 回転のランダムの最大値
		fileutil::ReadFloat(ss, "RandomWeightMin", bundleData[tag[dataIndex]][index].info.random.randomWeight.y);			// 重さのランダムの最低値
		fileutil::ReadFloat(ss, "RandomWeightMax", bundleData[tag[dataIndex]][index].info.random.randomWeight.x);			// 重さのランダムの最大値
		fileutil::ReadFloat(ss, "DistortionMin", bundleData[tag[dataIndex]][index].info.random.distortion.y);
		fileutil::ReadFloat(ss, "DistortionMax", bundleData[tag[dataIndex]][index].info.random.distortion.x);
		fileutil::ReadInt(ss, "RandomCreate", setRandomPosIndex);															// ランダムな位置設定の列挙
		fileutil::ReadInt(ss, "LockPosition", lockPosIndex);																// 位置のロック設定
		fileutil::ReadInt(ss, "DelayStartTime", bundleData[tag[dataIndex]][index].info.beginDelay);						// 開始時間の遅延
		fileutil::ReadFloat(ss, "CreateRange", bundleData[tag[dataIndex]][index].info.circleDistance);					// 生成位置の範囲
		fileutil::ReadVector3(ss, "PosOffset", bundleData[tag[dataIndex]][index].info.posOffset);							// 位置のオフセット
		fileutil::ReadVector3(ss, "Velocity", bundleData[tag[dataIndex]][index].info.move);								// 移動量
		fileutil::ReadFloat(ss, "VelocityAttenuation", bundleData[tag[dataIndex]][index].info.moveAttenuation);			// 移動の減衰量
		fileutil::ReadInt(ss, "Behavior", behaviorIndex);																	// 挙動の列挙
		fileutil::ReadVector3(ss, "Scale", bundleData[tag[dataIndex]][index].info.scale);									// 大きさ
		fileutil::ReadColor(ss, "Color", bundleData[tag[dataIndex]][index].info.col);										// 色
		fileutil::ReadColor(ss, "DestColor", bundleData[tag[dataIndex]][index].info.destCol);								// 目的の色
		fileutil::ReadFloat(ss, "ColorAttenuation", bundleData[tag[dataIndex]][index].info.colAttenuation);				// 色の減衰量
		fileutil::ReadInt(ss, "BlendType", blendIndex);
		fileutil::ReadVector3(ss, "Scaling", bundleData[tag[dataIndex]][index].info.scalingValue);						// 拡縮量
		fileutil::ReadFloat(ss, "Weight", bundleData[tag[dataIndex]][index].info.weight);									// 重さ
		fileutil::ReadFloat(ss, "Radius", bundleData[tag[dataIndex]][index].emitterData.radius);							// 円周
		fileutil::ReadFloat(ss, "RotateAngle", bundleData[tag[dataIndex]][index].info.rotate.angle);						// 螺旋状の回転量
		fileutil::ReadFloat(ss, "RotateRadius", bundleData[tag[dataIndex]][index].info.rotate.radius);					// 螺旋状の半径
		fileutil::ReadFloat(ss, "RandomRotateMaxBH", bundleData[tag[dataIndex]][index].info.rotate.randomMax);				// 回転量のランダムの最大値
		fileutil::ReadFloat(ss, "RandomRotateMinBH", bundleData[tag[dataIndex]][index].info.rotate.randomMin);				// 回転量のランダムの最低値
		fileutil::ReadInt(ss, "PopTime", bundleData[tag[dataIndex]][index].info.popTime);									// 生存時間
		fileutil::ReadInt(ss, "Quantity", bundleData[tag[dataIndex]][index].emitterData.popParticleNum);					// 生成量
		fileutil::ReadString(ss, "TextureTag", bundleData[tag[dataIndex]][index].emitterData.texTag);						// テクスチャのタグ
		fileutil::ReadInt(ss, "TexIndex", bundleData[tag[dataIndex]][index].emitterData.texIndex);

		bundleData[tag[dataIndex]][index].emitterData.behavior		   = (CParticleEmitter::EBehavior)behaviorIndex;
		bundleData[tag[dataIndex]][index].info.random.randomBehavior   = (CParticle::ERandomBehavior)setRandomPosIndex;
		bundleData[tag[dataIndex]][index].info.random.randomLockVector = (CParticle::ELockVector)lockPosIndex;
	}

	int cnt = 0;
	std::string old_tag;
	for (size_t i = 0; i < bundleData.size(); i++)
	{
		if (tag[i] == "")
		{
			break;
		}

		if (tag[i] != old_tag)
		{
			cnt = 0;
		}

		old_tag = tag[i];
		m_data[tag[i]] = std::move(bundleData[tag[i]]);
		cnt++;
	}

	data = m_data;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの読み込み（ビルボード）
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::LoadJsonBillboard(nlohmann::json& list, CParticleManager::SBundleData& data)
{
	fileutil::SafeLoad(list, "Tag", data.emitterData.particleTag);					// タグ
	fileutil::SafeLoad(list, "RandomWeight", data.info.random.randomWeight.y);		// ランダムの最低値
	fileutil::SafeLoad(list, "RandomMax", data.info.random.randomWeight.x);			// ランダムの最大値
	fileutil::SafeLoad(list, "DistortionMin", data.info.random.distortion.y);			// 球状放出歪み最低値
	fileutil::SafeLoad(list, "DistortionMax", data.info.random.distortion.x);			// 球状放出歪み最大値
	SafeLoadEnum(list, "RandomCreate", data.info.random.randomBehavior);	// ランダムな位置設定の列挙
	SafeLoadEnum(list, "LockPosition", data.info.random.randomLockVector);	// 位置のロック設定
	fileutil::SafeLoad(list, "DelayStartTime", data.info.beginDelay);					// 開始時間の遅延
	fileutil::SafeLoad(list, "UseRandom", data.info.useRandom);						// ランダムを適用
	fileutil::SafeLoad(list, "CreateRange", data.info.circleDistance);				// 生成位置の範囲
	fileutil::SafeLoad(list, "PosOffset", data.info.posOffset);						// 位置のオフセット
	fileutil::SafeLoad(list, "Velocity", data.info.move);								// 移動量
	fileutil::SafeLoad(list, "VelocityAttenuation", data.info.moveAttenuation);		// 移動の減衰量
	SafeLoadEnum(list, "Behavior", data.emitterData.behavior);				// 挙動の列挙
	fileutil::SafeLoad(list, "Scale", data.info.scale);								// 大きさ
	fileutil::SafeLoad(list, "Color", data.info.col);									// 色
	fileutil::SafeLoad(list, "DestColor", data.info.destCol);							// 目的の色
	fileutil::SafeLoad(list, "ColorAttenuation", data.info.colAttenuation);			// 色の減衰量
	fileutil::SafeLoad(list, "Scaling", data.info.scalingValue);						// 拡縮量
	fileutil::SafeLoad(list, "Weight", data.info.weight);								// 重さ
	fileutil::SafeLoad(list, "Radius", data.emitterData.radius);						// 円周
	fileutil::SafeLoad(list, "RotateAngle", data.info.rotate.angle);					// 螺旋状の回転量
	fileutil::SafeLoad(list, "RotateRadius", data.info.rotate.radius);				// 螺旋状の半径
	fileutil::SafeLoad(list, "RandomRotateMax", data.info.rotate.randomMax);			// 回転量のランダムの最大値
	fileutil::SafeLoad(list, "RandomRotateMin", data.info.rotate.randomMin);			// 回転量のランダムの最低値
	fileutil::SafeLoad(list, "PopTime", data.info.popTime);							// 生存時間
	fileutil::SafeLoad(list, "Quantity", data.emitterData.popParticleNum);			// 生成量
	fileutil::SafeLoad(list, "TextureTag", data.emitterData.texTag);					// テクスチャのタグ
	fileutil::SafeLoad(list, "TexIndex", data.emitterData.texIndex);
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// データを設定する処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticleManager::SetParticleData(std::string& str, std::vector<SBundleData>& info)
{
	m_data[str] = info;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 抽出したデータを返す処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
std::vector<CParticleManager::SBundleData> CParticleManager::GetParticleData(std::string str)
{
	if (m_data.count(str) == 0)
	{	//モデルデータが入ってなかった場合
		assert(false);
	}

	return m_data[str];
}