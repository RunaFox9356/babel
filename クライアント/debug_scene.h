//=============================================================================
//
// �Q�[���N���X(game.h)
// Author : �������l
// �T�v : �Q�[���N���X�̊Ǘ����s��
//
//=============================================================================
#ifndef _DEBUG_SCENE_H_		// ���̃}�N����`������ĂȂ�������
#define _DEBUG_SCENE_H_		// ��d�C���N���[�h�h�~�̃}�N����`

//*****************************************************************************
// �C���N���[�h
//*****************************************************************************
#include "scene_mode.h"

//*****************************************************************************
// �O���錾
//*****************************************************************************
class CPlayer;
class CDrone;
class CMesh3D;
class CLine;
class CModelObj;
class CEnemy_Socket;
class CUdp_Socket;
//=============================================================================
// �Q�[���N���X
// Author : �������l
// �T�v : �Q�[���������s���N���X
//=============================================================================
class CDebug_Scene : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// �\����
	//--------------------------------------------------------------------
	//struct CAMERAPOS
	//{//�J�������W
	//	D3DXVECTOR3 PosV;
	//	D3DXVECTOR3 PosR;
	//};

	//--------------------------------------------------------------------
	// �ÓI�����o�֐�
	//--------------------------------------------------------------------
	static CPlayer *GetPlayer() { return m_pPlayer; }				// �v���C���[
	static void SetGame(const bool bGame) { m_bGame = bGame; }		// �Q�[���̏󋵂̐ݒ�
	static CDrone *GetDrone() { return m_pDrone; }					// �h���[��

	//--------------------------------------------------------------------
	// �R���X�g���N�^�ƃf�X�g���N�^
	//--------------------------------------------------------------------
	CDebug_Scene();
	~CDebug_Scene() override;

	//--------------------------------------------------------------------
	// �ÓI�����o�ϐ�
	//--------------------------------------------------------------------
	static CPlayer *m_pPlayer;					// �v���C���[�N���X
	static CDrone *m_pDrone;						// �h���[��
	static D3DXCOLOR fogColor;						// �t�H�O�J���[
	static float fFogStartPos;						// �t�H�O�̊J�n�_
	static float fFogEndPos;						// �t�H�O�̏I���_
	static float fFogDensity;						// �t�H�O�̖��x
	static bool m_bGame;							// �Q�[���̏�
	CLine **m_pLine;
	//CAMERAPOS m_CameraList[];				// �J�������X�g

	CModelObj *pDoor;
	CModelObj *pDoorGimmick;

	//CEnemy_Socket*m_EnemySocket;
	//CSocket*m_Socket;

	int m_worldTimer;
	//--------------------------------------------------------------------
	// �����o�֐�
	//--------------------------------------------------------------------
	HRESULT Init() override;					// ������
	void Uninit() override;						// �I��
	void Update() override;						// �X�V
	//CAMERAPOS GetCameraPos(int number) { return m_CameraList[number]; }

};

#endif



#pragma once
