//=============================================================================
//
// Q[NX(game.cpp)
// Author : ú±l
// Tv : Q[NXÌÇðs¤
//
//=============================================================================

//*****************************************************************************
// CN[h
//*****************************************************************************
#include <assert.h>
#include <vector>

#include "EscapePoint.h"
#include "game.h"
#include "calculation.h"
#include "input.h"
#include "application.h"
#include "camera.h"
#include "renderer.h"
#include "object.h"
#include "polygon2D.h"
#include "polygon3D.h"
#include "player.h"
#include "motion_model3D.h"
#include "model3D.h"
#include "model_obj.h"
#include "mesh.h"
#include "sphere.h"
#include "bg.h"
#include "model_obj.h"
#include "debug_proc.h"
#include "sound.h"
#include "line.h"
#include "particle_emitter.h"
#include "collision_rectangle3D.h"
#include "drone.h"
#include "agent.h"
#include "enemy.h"
#include "GimmicDoor.h"
#include "model_data.h"
#include "tcp_client.h"
#include "application.h"
#include "map.h"
#include "agent.h"
#include "minigame_ring.h"
#include "task.h"
#include "gun.h"
#include "cylinder.h"
#include "mesh2D.h"
#include "ranking.h"
#include "Score.h"

#include "walkingEnemy.h"
#include "hostage.h"
#include "carry_player.h"
#include "radio.h"
#include "AgentUI.h"
#include "stone.h"

#include "shadow_volume.h"

#include "model_skin.h"
#include "stencil_canvas.h"

#include "HackingObj.h"
#include "securityTimer.h"
#include "carAnimation.h"

#include "cardboard.h"
#include "stealObj.h"

#include "hackingdoor.h"
//*****************************************************************************
// ÃIoÏé¾
//*****************************************************************************
CAgent *CGame::m_pPlayer = nullptr;						// vC[NX
CPlayer *CGame::m_pDrone = nullptr;						// h[NX
bool CGame::m_bGame = false;							// Q[Ìóµ
CTask* CGame::m_pTask = nullptr;						// ^XN}l[W[
CMesh3D* CGame::m_Mesh = nullptr;						// bV
CRadio*CGame::m_data = nullptr;							// bV
CAgentUI* CGame::m_pUiManager = nullptr;				// UI}l[W[
const int		CGame::DEFAULT_ALLERT_TIME = 900;

namespace
{
	// tHOÌlÝè
	const D3DXCOLOR fogColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f);		// tHOJ[
	const float fogStartPos = 600.0f;								// tHOÌJn_
	const float fogEndPos = 2000.0f;								// tHOÌI¹_
	const float fogDensity = 0.00001f;								// tHOÌ§x
}

//=============================================================================
// RXgN^
// Author : ú±l
// Tv : CX^X¶¬És¤
//=============================================================================
CGame::CGame() : m_nSecurityLevel(0),
m_Map(0),
m_pSecurityTimer(nullptr)
{
	
}

//=============================================================================
// fXgN^
// Author : ú±l
// Tv : CX^XI¹És¤
//=============================================================================
CGame::~CGame()
{

}

//=============================================================================
// ú»
// Author : ú±l
// Tv : ¸_obt@ð¶¬µAoÏÌúlðÝè
//=============================================================================
HRESULT CGame::Init()
{
	//CHackDoor::Create()->SetDoorPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	//CCardboard::Create({ 0.0f,0.0f,0.0f });
	//MAPÌf[^ðú»·éB
	InitPosh();

	CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->SetPlayerClear();
	CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->SetPlayerClear();

	CApplication::GetInstance()->SetConnect(true);
	CApplication::GetInstance()->SetEnemyConnect(true);
	{
		bOpenflg = false;
	}
	
	// üÍfoCXÌæ¾
	CInput *pInput = CInput::GetKey();

	// foCXÌæ¾
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// TEhîñÌæ¾
	CSound *pSound = CApplication::GetInstance()->GetSound();
	//pSound->SetVolume(CSound::SOUND_LABEL_BGM001, 0.01f);
	pSound->PlaySound(CSound::SOUND_LABEL_BGM001);

	m_pTask = CTask::Create();

	m_Map = CApplication::GetInstance()->GetMap();

	if (m_Map < 0 || m_Map >= CApplication::GetInstance()->GetMapDataManager()->GetMaxMap())
	{
		m_Map = 0;
	}

	{
		m_pFloor = CPolygon3D::Create();

		if (m_pFloor)
		{
			m_pFloor->SetPos(D3DXVECTOR3(0.0f, 1.0f, 0.0f));
			m_pFloor->SetSize(D3DXVECTOR3(20000.0f, 20000.0f, 0.0f));
			m_pFloor->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(400.0f, 400.0f));
			m_pFloor->SetRot(D3DXVECTOR3(D3DX_PI * 0.5f, 0.0f, 0.0f));

			if (m_Map == 0)
			{
				m_pFloor->LoadTex(92);
			}
			else if (m_Map == 1)
			{
				m_pFloor->LoadTex(120);
			}
			else if (m_Map == 2)
			{
				m_pFloor->LoadTex(114);
			}
		}
	}

	CMap* pMap = CMap::Create(m_Map);

	SetMap(pMap);

	CApplication::GetInstance()->SetMap(m_Map);

	CApplication::SMapList MapData;
	MapData.enemyCount = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vEnemyData.size();
	MapData.gimmickConnect = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vGimmickData.size();
	CApplication::GetInstance()->SetList(MapData);

	// dÍÌlðÝè
	CCalculation::SetGravity(0.2f);

	if (m_Map != 1)
	{
		// XJC{bNXÌÝè
		CSphere *pSphere = CSphere::Create();
		pSphere->SetRot(D3DXVECTOR3(D3DX_PI, 0.0f, 0.0f));
		pSphere->SetSize(D3DXVECTOR3(10.0f, 0, 10.0f));
		pSphere->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
		pSphere->SetRadius(10000.0f);
		pSphere->SetSphereRange(D3DXVECTOR2(D3DX_PI * 2.0f, D3DX_PI * -0.5f));
		pSphere->LoadTex(1);
	}

	// vC[ÌÝè
	m_pPlayer = CAgent::Create();
	m_pPlayer->SetMotion("data/MOTION/motion.txt");
	//m_pPlayer->SetRenderMode(CModel3D::ERenderMode::Render_Highlight);
	//m_pPlayer->SetPos(D3DXVECTOR3(0.0f, 500.0f, -1000.0f));

	// h[Ýè
	m_pDrone = CPlayer::Create();
	m_pDrone->SetRotMove(false);
	m_pDrone->SetMotion("data/MOTION/motion002_drone.txt");
	CApplication::GetInstance()->SetMoveModel(m_pDrone);
	CApplication::GetInstance()->SetModelSet(true);

	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (pManager)
	{
		CMapDataManager::MAP_DATA MapData = pManager->GetMapData(CApplication::GetInstance()->GetMap());

		if (m_pPlayer)
			m_pPlayer->SetPos(MapData.agentPos);
		if (m_pDrone)
			m_pDrone->SetPos(MapData.agentPos);
	}

	CGun *pGun = CGun::Create();
	pGun->SetType(56);
	CCollision_Rectangle3D *pCollision = pGun->GetCollision();
	D3DXVECTOR3 modelSize = pGun->GetModel()->GetMyMaterial().size;
	pCollision->SetPos(D3DXVECTOR3(0.f, modelSize.y / 2.0f, 0.f));
	pCollision->SetSize(modelSize);

	if (m_Map == 1)
		pGun->SetPos(D3DXVECTOR3(7400.0f, 0.0f, 0.0f));
	else
		pGun->SetPos(D3DXVECTOR3(0.0f, 0.0f, -100.0f));

	pGun->SetAllReload(true);
	pGun->SetRapidFire(false);
	pGun->SetMaxBullet(15);
	pGun->SetMaxReload(60);


	// JÌÇ]Ýè(ÚW : vC[)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	// JÌÇ]Ýè(ÚW : vC[)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	m_pStencilCanvas = CStencilCanvas::Create();
	m_pStencilCanvas->SetCol(D3DXCOLOR(0.f, 0.f, 0.f, 1.f));

	// }EXJ[\ðÁ·
	pInput->SetCursorErase(false);
	pInput->SetCursorLock(true);

	// tHOÌÝè
	SetFog(pDevice, fogStartPos, fogEndPos, fogDensity, fogColor);

	m_bGame = true;

	m_pUiManager = CAgentUI::Create();
	m_pUiManager->SetAgent(m_pPlayer);
	m_pUiManager->SetHpGauge(m_pPlayer->GetHpGauge());

	if (m_pTask)
	{//null`FbN
		m_pTask->CreateUI();		//UIÌ¶¬
	}
	//m_data = CRadio::Create(D3DXVECTOR3(200.0f, 500.0f, 0.0f), D3DXVECTOR3(100.0f, 100.0f, 0.0f), 0, "^XNâI", D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));


	std::thread th(CApplication::Recv, 1);
	// XbhðØè£·
	th.detach();

	//CModel3D::SaveModelAll();
	//CStone::Create(D3DXVECTOR3(350.0f, 0.0f, -300.0f));


	return S_OK;
}

//=============================================================================
// I¹
// Author : ú±l
// Tv : eNX`Ì|C^Æ¸_obt@Ìðú
//=============================================================================
void CGame::Uninit()
{// üÍfoCXÌæ¾
	CInput *pInput = CInput::GetKey();
	CApplication::GetInstance()->SetModelSet(false);
	CApplication::GetInstance()->SetConnect(false);
	CApplication::GetInstance()->SetEnemyConnect(false);
	CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->SetPlayerClear();
	CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->SetPlayerClear();
	// foCXÌæ¾
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// TEhîñÌæ¾
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// TEhI¹
	pSound->StopSound();

	// tHOÌLøÝè
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	// }EXJ[\ðÁ·
	pInput->SetCursorErase(true);
	pInput->SetCursorLock(false);

	// JÌÇ]Ýè
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);
	pCamera->SetUseRoll(false, false);

	// JÌÇ]Ýè
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);
	pCamera->SetUseRoll(false, false);

	// ÃIÏÌú»
	if (m_pPlayer != nullptr)
	{
		m_pPlayer = nullptr;
	}
	// ÃIÏÌú»
	if (m_pDrone != nullptr)
	{
		m_pDrone = nullptr;
	}

	if (m_pSecurityTimer)
	{
		m_pSecurityTimer = nullptr;
	}

	//if (m_EnemySocket != nullptr)
	//{
	//	m_EnemySocket->Uninit();
	//	delete m_EnemySocket;
	//}
	//

	//^XN}l[W[Ì|C^ðnullÉß·
	if (m_pTask)
	{
		m_pTask = nullptr;
	}

	////UI}l[W[Ì|C^ðnullÉß·
	//if (m_pUiManager)
	//{
	//	m_pUiManager = nullptr;
	//}



	// XRAÌðú
	Release();
}

//=============================================================================
// XV
// Author : ú±l
// Tv : XVðs¤
//=============================================================================
void CGame::Update()
{
	//m_EnemySocket->Update();
	// JÌÇ]Ýè

	//±±É¨ñç¢ñÌÅ[½ðüêé
	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);

	D3DXVECTOR3 PlayerPos = CGame::GetPlayer()->GetPos();
	int Gim = GetGimmickSize();
	for (int i = 0; i < Gim; i++)
	{
		Model->GetPlayerData()->Player.m_isPopGimmick[i].isUse = GetGimmick(i)->GetAnimation();
		Model->GetPlayerData()->Player.m_isPopGimmick[i].isMap = GetGimmick(i)->GetIsOnMap();

	}
	Model->GetPlayerData()->Player.m_popGimmick = Gim;
	int Enemy = GetEnemySize();
	Model->GetPlayerData()->Player.m_popEnemy = Enemy;
	for (int i = 0; i < Enemy; i++)
	{
		Model->GetPlayerData()->Player.m_isPopEnemy[i].Pos = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).Pos;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isDiscovery = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isDiscovery;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isUse = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isUse;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isMap = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isMap;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isDes = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isDes;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isMotion = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isMotion;
	}		
	//±±ÜÅ

	// üÍfoCXÌæ¾
	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(DIK_8))
	{
		GetRadio()->CreateText(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(100.0f, 100.0f, 0.0f),"^XNâI", D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}

	//ZLeB[xÌXV
	UpdateSecurityLevel();

#ifdef _DEBUG
	

	if (pInput->Trigger(DIK_P))
	{
		m_pPlayer->MovePriorityEnd();
	}


	if (pInput->Trigger(DIK_U))
	{
		CCarAnimation::Create();
	}

	//static int testTime = 0;
	//testTime++;
	//if ((testTime % 50) == 0)
	//{
	//	CParticleEmitter* pParticle = CParticleEmitter::Create("Test");
	//	pParticle->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	//}

	if (pInput->Trigger(DIK_RETURN))
	{
		m_bGame = false;
	}

	// fobN\¦
	CDebugProc::Print("F1 Ug| F2 G[WFg| F3 nbJ[| F4 `[gA| F5 ^Cg| F7 fobN\¦í\n");
	CDebugProc::Print("VhE{[Ìgp : %dÂgp\n", CShadowVolume::nCntMyObject);

	if (pInput->Trigger(DIK_F1))
	{
		ResultScore();
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		ResultScore();
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_AGENT);
	}
	if (pInput->Trigger(DIK_F3))
	{
		ResultScore();
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_HACKER);
	}
	if (pInput->Trigger(DIK_F4))
	{
		ResultScore();
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		ResultScore();
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TITLE);
	}

	if (pInput->Press(DIK_LSHIFT))
	{
		CApplication::GetInstance()->GetCamera()->Zoom();
	}

#endif // _DEBUG

	if (!m_bGame)
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}

	HWND activeWindowHandle;
	HWND hWnd = CApplication::GetInstance()->GetWnd();
	bool isActiveWindowThis = CApplication::GetInstance()->GetActiveWindowThis();
	activeWindowHandle = GetForegroundWindow();
	if (hWnd != activeWindowHandle && isActiveWindowThis)
	{ // ©ªªÅOÉ¢È¢Èç
		isActiveWindowThis = false;
		pInput->SetCursorErase(true);

	}
	if (hWnd == activeWindowHandle && !isActiveWindowThis)
	{ // ©ªªÅOÈç
		isActiveWindowThis = true;
		pInput->SetCursorErase(false);
	}
	CApplication::GetInstance()->SetActiveWindowThis(isActiveWindowThis);

	pInput->SetCursorLock(isActiveWindowThis);

}

void CGame::IncreaseSecurity()
{
	if (m_nSecurityLevel < 1)
	{
		m_nSecurityLevel = 1;

		m_pSecurityTimer = CSecurityTimer::Create();

		CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

		if (pManager)
		{
			CMapDataManager::MAP_DATA MapData = pManager->GetMapData(CApplication::GetInstance()->GetMap());

			for (int nCnt = 0; nCnt < (int)MapData.vEnemyData.size(); nCnt++)
			{
				CEnemy* pEnemy = MapData.vEnemyData.data()[nCnt].pEnemy;

				if (pEnemy)
				{
					pEnemy->SetSecurityLevel(m_nSecurityLevel);
				}
				if (m_pPlayer)
				{
					m_pPlayer->SetSecurityLevel(m_nSecurityLevel);
				}
			}
		}
	}

	if (m_pSecurityTimer)
		m_pSecurityTimer->ResetTimer();
	else
	{
		m_pSecurityTimer = CSecurityTimer::Create();
	}
}

void CGame::SetStencilCanvas(CStencilCanvas * pObj)
{
	m_pStencilCanvas = pObj;
}

//==============================================
//I¹ÌXRAÁZ
//==============================================
void CGame::ResultScore()
{
	int nScore = 0;

	/*if (m_pUiManager && m_pUiManager->GetScore())
	{
		nScore = m_pUiManager->GetScore()->GetScore();

		if (CApplication::GetInstance()->GetRanking())
		{
			CApplication::GetInstance()->GetRanking()->GetRank(nScore);
		}
	}
	CApplication::GetInstance()->GetRanking()->GetRank(nScore);*/
}

// ZLeB[xÌXV
void CGame::UpdateSecurityLevel()
{
	if (m_nSecurityLevel > 0 && m_pSecurityTimer)
	{
		if (m_pSecurityTimer->GetEnd())
		{
			m_nSecurityLevel = 0;

			CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

			if (pManager)
			{
				CMapDataManager::MAP_DATA MapData = pManager->GetMapData(CApplication::GetInstance()->GetMap());

				for (int nCnt = 0; nCnt < (int)MapData.vEnemyData.size(); nCnt++)
				{
					CEnemy* pEnemy = MapData.vEnemyData.data()[nCnt].pEnemy;

					if (pEnemy)
					{
						pEnemy->SetSecurityLevel(m_nSecurityLevel);
					}
					if (m_pPlayer)
					{
						m_pPlayer->SetSecurityLevel(m_nSecurityLevel);
					}
				}
			}

			if (m_pSecurityTimer)
			{
				m_pSecurityTimer->Uninit();
				m_pSecurityTimer = nullptr;
			}
		}
	}
}
