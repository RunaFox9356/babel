//=============================================================================
//
// リザルトクラス(result.h)
// Author : 唐�ｱ結斗
// 概要 : リザルトクラスの管理を行う
//
//=============================================================================
#ifndef _RESULT_H_		// このマクロ定義がされてなかったら
#define _RESULT_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "scene_mode.h"
#include "application.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CPolygon2D;
class CPolygon3D;
class CModelObj;
class CResultScoreUI;

//=============================================================================
// リザルトクラス
// Author : 唐�ｱ結斗
// 概要 : リザルト生成を行うクラス
//=============================================================================
class CResult : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CResult();
	~CResult() override;

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新

private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void AutoTransition();
	void ScoresUpdate();				//スコアのアニメーション
	bool SetMapRecv();
	void CreateRankingUI();				//ランキングUIの生成処理
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------

	enum STATE
	{
		STATE_WAIT = 0,
		STATE_CALC_SCORE,
		STATE_DISPLAY_RANKING,

		STATE_MAX
	};


	static const D3DXCOLOR		DEFAULT_NORMAL_COLOR;			// ディフォルトの普通の色
	static const D3DXCOLOR		DEFAULT_ANIM_COLOR;				// ディフォルトのアニメーション用の色

	CApplication::SCENE_MODE	m_nextMode;						// 次に設定するモード
	D3DXVECTOR3					m_roadSize;						// 道路のサイズ
	D3DXVECTOR3					m_guardrailSize;				// ガードレールのサイズ
	int							m_nCntFrame;					// フレームカウント
	int							m_nRankedScoreIdx;				//点滅しているスコアのインデックス
	float						m_fScroll;						//スクロールの係数
	bool						m_bPressEnter;					// プレスエンターを使用できるか
	bool						m_check;
	bool						m_isConnect;
	CPolygon2D*					m_pScore[5][5];					//スコアUIへのポインタ
	CPolygon2D*					m_pRank[5];						//ランクUIへのポインタ
	CPolygon3D*					m_pBg;							//背景へのポインタ
	CPolygon3D*					m_pGround;						//地面へのポインタ
	CModelObj*					m_pRoad[2];						//道路
	CModelObj*					m_pCar;							//自動車へのポインタ
	CModelObj*					m_pGuardrail[5];				//ガードレール
	CModelObj*					m_pStreetlight;					//街灯へのポインタ
	CResultScoreUI*				m_pScoreResumeUI;				//スコア詳細のUI
	
};

#endif




