//=============================================================================
//
// メイン
// Author : 
//
//=============================================================================
#ifndef _MAIN_H_		// このマクロ定義がされてなかったら
#define _MAIN_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"ws2_32.lib")		// サーバーの処理に必要
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ
#pragma comment(lib,"dxguid.lib")		// DirectXのコンポネート(部品)の使用に必要
#pragma comment(lib,"winmm.lib")		// システム時刻取得に必要
#pragma comment(lib,"xinput.lib")		// ジョイパッド入力に必要

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <Windows.h>
#include <tchar.h>							// _T
#include "d3dx9.h"							// 描画処理に必要
#include "xaudio2.h"						// サウンド処理に必要
#include "Xinput.h"							// ジョイパット処理に必要

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
int GetFps();

#ifdef _DEBUG
#define CHECK_MEM_LEAK
#endif // _DEBUG:

//普段使用禁止
#ifdef CHECK_MEM_LEAK
#define new new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#endif // CHECK_MEM_LEAK

#endif