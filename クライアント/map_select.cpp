//=============================================================================
//
// selectScreen.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "map_select.h"
#include "input.h"
#include "sound.h"
#include "renderer.h"
#include "camera.h"
#include "polygon2D.h"
#include "menuButton.h"
#include "menu.h"
#include "fade.h"
#include "loading.h"
#include "text.h"
#include "tcp_client.h"
#include "swipingImage.h"
#include "client.h"
#include "optionMenu.h"



//=============================================================================
// インクルード
//=============================================================================
const D3DXVECTOR3	CMapSelect::DEFAULT_MAP_IMAGE_SPAWN_POS = { 920.0f, 1000.0f, 0.0f };	//ディフォルトのマップの画像のスポーンの位置
const D3DXVECTOR3	CMapSelect::DEFAULT_MAP_IMAGE_TARGET_POS = { 920.0f, 430.0f, 0.0f };	//ディフォルトのマップの画像の目的の位置
const D3DXVECTOR3	CMapSelect::DEFAULT_MAP_IMAGE_END_POS = { 1600.0f, 430.0f, 0.0f };		//ディフォルトのマップの画像の終点
const D3DXVECTOR3	CMapSelect::DEFAULT_MAP_IMAGE_SIZE = { 500.0f, 500.0f, 0.0f };			//ディフォルトのマップの画像のサイズ
const D3DXVECTOR3	CMapSelect::DEFAULT_MENU_POS = { 275.0f, 430.0f, 0.0f };				//ディフォルトのメニューの位置
//マップの画像のテクスチャインデックス
const int	CMapSelect::MAP_IMAGE_IDX[MAP_MAX] =
{
	80,
	122,
	139
};

namespace
{
	const int DEFAULT_DELAY = 30;			//ディフォルトのディレイ
};


//=============================================================================
// コンストラクタ
// Author : 浜田琉雅
// 概要 : 初期数値を入れる
//=============================================================================
CMapSelect::CMapSelect() : m_bTransition(false),
m_bTutorial(false),
m_pMenu(nullptr),
m_pSwipingImg(nullptr),
m_pTutorialButton(nullptr),
m_pTutorial(nullptr),
m_pOptionMenu(nullptr),
m_option(nullptr),
m_optionUI(nullptr),
m_nSelectedIdx(0),
m_nDelay(0)
{

}

//=============================================================================
// デス
// Author : 浜田琉雅
// 概要 : 削除前にしたいことを入力
//=============================================================================
CMapSelect::~CMapSelect()
{

}

//=============================================================================
// 初期化
// Author : 浜田琉雅
// 概要 : 初期化
//=============================================================================
HRESULT CMapSelect::Init()
{
	CApplication::GetInstance()->SetIsMap(false);
	m_Receive.isMap = false;
	m_Receive.sendMap = 0;
	m_isCommand = false;

	//カメラを取得する
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_MAPSERECTBGM004);


	{
		//背景を生成する
		CPolygon2D* pBg = CPolygon2D::Create();

		if (pBg)
		{//nullチェック

			pBg->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));	//位置の設定
			pBg->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));				//サイズの設定
			pBg->LoadTex(51);																								//テクスチャの設定
		}
	}

	{//画面のタイトル

		//ポリゴンを生成する
		CPolygon2D* pPolygon = CPolygon2D::Create();

		if (pPolygon)
		{//nullチェック

			pPolygon->SetPos(D3DXVECTOR3(640.0f, 100.0f, 0.0f));		//位置の設定
			pPolygon->SetSize(D3DXVECTOR3(300.0f, 150.0f, 0.0f));		//サイズの設定
			pPolygon->LoadTex(16);										//テクスチャの設定
		}
	}

	//マップの画像を生成する
	m_pSwipingImg = CSwipingImage::Create(DEFAULT_MAP_IMAGE_SPAWN_POS, DEFAULT_MAP_IMAGE_TARGET_POS, DEFAULT_MAP_IMAGE_END_POS);
	
	if (m_pSwipingImg)
	{//nullチェック

		m_pSwipingImg->SetImageSpeedOut(50.0f);					//画像が画面を出る速度の設定
		m_pSwipingImg->SetImageSize(DEFAULT_MAP_IMAGE_SIZE);	//画像のサイズのの設定

		//マップ画像の生成
		for (int nCnt = 0; nCnt < MAP_MAX; nCnt++)
		{
			m_pSwipingImg->AddImage(MAP_IMAGE_IDX[nCnt]);
		}
	}

	//if (CApplication::GetInstance()->GetSelectMode() == CApplication::MODE_GAME)
	{
		//メニューを生成する
		m_pMenu = CMenu::Create(DEFAULT_MENU_POS, D3DXVECTOR3(500.0f, 500.0f, 0.0f));			

		if (m_pMenu)
		{//nullチェック

			m_pMenu->SetBackgroundTexture(9);		//メニューの背景のテクスチャの設定

			CMenuButton* pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, -150.0f, 0.0f));	//メニューのボタンを生成する

			if (pButton)
			{//nullチェック

				pButton->SetTexture(134);		//ボタンのテクスチャの設定
				m_pMenu->AddButton(pButton);	//ボタンをメニューに追加する
			}

			pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f));					//メニューのボタンを生成する

			if (pButton)
			{//nullチェック

				pButton->SetTexture(135);		//ボタンのテクスチャの設定
				m_pMenu->AddButton(pButton);	//ボタンをメニューに追加する
			}

			pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, 150.0f, 0.0f));					//メニューのボタンを生成する

			if (pButton)
			{//nullチェック

				pButton->SetTexture(16);		//ボタンのテクスチャの設定
				m_pMenu->AddButton(pButton);	//ボタンをメニューに追加する
			}
		}

		m_pTutorialButton = CMenuButton::Create(D3DXVECTOR3(1040.0f, 100.0f, 0.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), D3DXCOLOR(1.0f, 0.2f, 0.2f, 1.0f));

		if (m_pTutorialButton)
		{
			m_pTutorialButton->LoadTex(20);
			m_pTutorialButton->SetSize(D3DXVECTOR3(300.0f, 150.0f, 0.0f));
		}

		m_pTutorial = CPolygon2D::Create(CSuper::PRIORITY_LEVEL1);

		int nIdx = 132;

		if (CApplication::GetInstance()->GetSelectMode() == CApplication::MODE_HACKER)
			nIdx = 133;

		if (m_pTutorial)
		{
			m_pTutorial->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));
			m_pTutorial->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));
			m_pTutorial->LoadTex(nIdx);
			m_pTutorial->SetDraw(false);
			m_bTutorial = true;
		}
	}
	/*else
	{
		CText::Create(D3DXVECTOR3(0.0f, 100.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "マッチング待機", D3DXVECTOR3(10.0f, 20.0f, 20.0f));
	}*/
	//m_socket->Init();

	m_option = CText::Create(D3DXVECTOR3(70.0f, 70.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "option", D3DXVECTOR3(10.0f, 20.0f, 20.0f), D3DXCOLOR(0.3f, 1.0f, 0.3f, 1.0f));
	
	{
		m_optionUI = CPolygon2D::Create(CSuper::PRIORITY_LEVEL1);

		if (m_optionUI)
		{
			m_optionUI->SetPos(D3DXVECTOR3(40.0f, 70.0f, 0.0f));
			m_optionUI->SetSize(D3DXVECTOR3(75.0f, 75.0f, 0.0f));
			m_optionUI->LoadTex(136);
		}
	}

	return S_OK;
}

//=============================================================================
// 破棄
// Author : 浜田琉雅
// 概要 : 破棄
//=============================================================================
void CMapSelect::Uninit()
{
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	// フォグの有効設定
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	// マウスカーソルを出す
	pInput->SetCursorErase(true);

	// カメラの追従設定
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// カメラの追従設定
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	//マップ画像のポインタをnullに戻す(別のところで破棄されている)
	if (m_pSwipingImg)
	{
		m_pSwipingImg = nullptr;
	}
	if (m_pOptionMenu)
	{
		m_pOptionMenu = nullptr;
	}
	if (m_option)
	{
		m_option = nullptr;
	}
	if (m_optionUI)
	{
		m_optionUI = nullptr;
	}

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 浜田琉雅
// 概要 : 更新処理を入れる
//=============================================================================
void CMapSelect::Update()
{

	// 入力情報の取得
	//CInput *pInput = CInput::GetKey();

	if (m_isCommand)
	{//数値入れて確認したら遷移
	
		CApplication::GetInstance()->SetIsMap(m_isCommand);
		CApplication::GetInstance()->SetMap(m_Receive.sendMap);
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->GetSelectMode());
	}
	else
	{
		//if (pInput->Trigger(DIK_RETURN))
		//{//エンターキーを押したら、選択されているマップのインデックスを保存し、ゲームに進む

		//	CApplication::GetInstance()->SetMap(m_nSelectedIdx);
		//	CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->GetSelectMode());
		//}
	}

	if (!m_bTutorial && !m_pOptionMenu)
	{
		if (m_nDelay <= 0)
		{
			if (m_pTutorialButton && m_pTutorialButton->GetTriggerState())
			{
				m_pTutorialButton->SetTriggerState(false);
				m_bTutorial = true;
				m_nDelay = DEFAULT_DELAY;

				if (m_pTutorial)
				{
					m_pTutorial->SetDraw(true);
				}
			}
		}
		else
		{
			if (m_nDelay == DEFAULT_DELAY)
				m_pTutorialButton->SetTriggerState(false);

			m_nDelay--;
		}

		//メニューの更新
		if (m_pMenu)
		{//nullチェック

			int nButton = m_pMenu->GetHoverButton();		//マウスカーソルと重なっているボタンインデックスを取得する

			if (m_pSwipingImg && !m_pSwipingImg->IsUpdating() && m_nSelectedIdx != nButton)
			{//画像が更新中ではなく、上取得したインデックスが前のインデックスと違う場合

				m_nSelectedIdx = nButton;					//選択されているマップのインデックスを保存する

				m_pSwipingImg->ChangeImage(m_nSelectedIdx);	//マップの画像を変更します
			}
		}

		if (CApplication::GetInstance()->GetSelectMode() == CApplication::MODE_GAME)
		{
			CMapSelect::AgentUpdate();
		}
		else
		{
			CMapSelect::DrawnUpdate();
		}

		CInput* pInput = CApplication::GetInstance()->GetInput();

		if (pInput && pInput->Trigger(DIK_O))
		{
			if (m_option)
			{
				m_option->SetPos(D3DXVECTOR3(1000.0f, 600.0f, 0.0f));
				m_option->TexChange("exit");
			}

			if (m_optionUI)
			{
				m_optionUI->SetPos(D3DXVECTOR3(965.0f, 600.0f, 0.0f));
			}

			m_pOptionMenu = COptionMenu::Create();
		}
	}
	else if(!m_pOptionMenu)
	{
		if (m_nDelay <= 0)
		{
			// 入力情報の取得
			CInput *pInput = CInput::GetKey();

			if (pInput)
			{
				if (pInput->Trigger(MOUSE_KEY::MOUSE_INPUT_LEFT) || pInput->Trigger(DIK_SPACE) || pInput->Trigger(DIK_RETURN))
				{
					m_pTutorialButton->SetTriggerState(false);
					m_bTutorial = false;
					m_nDelay = DEFAULT_DELAY;

					if (m_pTutorial)
					{
						m_pTutorial->SetDraw(false);
					}
				}
			}
		}
		else
			m_nDelay--;
	}
	else
	{
		CInput* pInput = CApplication::GetInstance()->GetInput();

		if (m_pOptionMenu && pInput && pInput->Trigger(DIK_O))
		{
			if (m_option)
			{
				m_option->SetPos(D3DXVECTOR3(75.0f, 75.0f, 0.0f));
				m_option->TexChange("option");
			}

			if (m_optionUI)
			{
				m_optionUI->SetPos(D3DXVECTOR3(40.0f, 75.0f, 0.0f));
			}

			m_pOptionMenu->Uninit();
			m_pOptionMenu = nullptr;
		}
	}
}

//=============================================================================
// オンラインでデータを取得する
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
bool CMapSelect::SetMap()
{
	bool comand = false;
	CClient *pTcp = CApplication::GetInstance()->GetClient();	// 通信クラスの取得
	pTcp->Send((const char*)&m_Receive, sizeof(SSendMap), CClient::TYPE_TCP);
	char aRecvData[1024];	// 受信データ
	while (!comand)
	{
		int nRecvSize = pTcp->Recv(aRecvData, sizeof(bool), CClient::TYPE_TCP);
	
		memcpy(&comand, &aRecvData[0], sizeof(bool));

	}   
	return comand;
}


//=============================================================================
// オンラインでデータを取得する
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
bool CMapSelect::SetMapRecv()
{
	bool comand = false;
	CClient *pTcp = CApplication::GetInstance()->GetClient();	// 通信クラスの取得
	char aRecvData[1024];	// 受信データ
	while (!comand)
	{
		
		pTcp->Recv(aRecvData, sizeof(SSendMap), CClient::TYPE_TCP);
		memcpy(&m_Receive, &aRecvData[0], sizeof(SSendMap));
		
		pTcp->Recv(aRecvData, sizeof(bool), CClient::TYPE_TCP);
		memcpy(&comand, &aRecvData[0], sizeof(bool));
	}
	return comand;
}


//=============================================================================
// オンラインでデータを取得する
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
void CMapSelect::SetTransition(MAP mode)
{
	CSound* pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_SE_DECIDE);
	m_Receive.sendMap = mode;
	m_Receive.isMap = true;

	std::thread ConnectOn([&] {m_isCommand = CMapSelect::SetMap(); });

	// スレッドをきりはなす
	ConnectOn.detach();

	m_bTransition = true;
	CLoading*load = new CLoading;
	load->Init();

}

//=============================================================================
// エージェントを選んだ時の更新を
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
void CMapSelect::AgentUpdate()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	bool bFade = CApplication::GetInstance()->GetFade()->GetFadeSituation();

	if (!bFade && !m_bTransition && m_pMenu)
	{
		int nButton = m_pMenu->GetTriggeredButton();

		if (nButton >= 0 && nButton < BUTTON_MAX)
		{
			SetTransition((MAP)nButton);
		}
	}
}
//=============================================================================
// エージェントを選んだ時の更新を
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
void CMapSelect::DrawnUpdate()
{
	//特に思いつかない

	if (!m_bTransition)
	{
		std::thread ConnectOn([&] {	m_isCommand = CMapSelect::SetMapRecv(); });

		// スレッドをきりはなす
		ConnectOn.detach();
	
		m_bTransition = true;
	}
}