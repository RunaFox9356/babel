//=============================================================================
//
// プレイヤークラス(player.cpp)
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "labyrinth.h"
#include "maze_block.h"
#include "move.h"
#include "collision_rectangle2D.h"
#include "input.h"
#include "utility.h"
#include "MiniGameMNG.h"
#include "application.h"
#include "scene_mode.h"
#include "minigame.h"
#include "text.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************

//--------------------------------------------------------------------
// 静的メンバ変数の定義
//--------------------------------------------------------------------

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CLabyrinth *CLabyrinth::Create(int count)
{
	// オブジェクトインスタンス
	CLabyrinth *pLabyrinth = nullptr;

	// メモリの解放
	pLabyrinth = new CLabyrinth(count);

	if (pLabyrinth != nullptr)
	{// 数値の初期化
		pLabyrinth->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pLabyrinth;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CLabyrinth::CLabyrinth(int nPriority) :CMiniGame(nPriority),
m_pPlayer(nullptr),
m_pPlayerCollision(nullptr),
m_pMove(nullptr),
m_pBg(nullptr),
m_nNumBlock(3),
m_nStartBulock(-1)
{
	m_pBlock.clear();
	m_nRoad.clear();

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pText[nCnt] = nullptr;
	}
	
	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		m_pCommandUI[nCnt] = nullptr;
	}
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CLabyrinth::~CLabyrinth()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CLabyrinth::Init()
{
	CMiniGame::Init();

	// 移動クラスのメモリ確保
	m_pMove = new CMove;
	assert(m_pMove != nullptr);
	m_pMove->SetMoving(0.3f, 1000.0f, 0.01f, 0.05f);

	// 3D矩形の当たり判定の設定
	m_pPlayerCollision = CCollision_Rectangle2D::Create();

	SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));
	SetSize(D3DXVECTOR3(500.0f, 500.0f, 0.0f));
	SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.f));
	SetNumBlock(11);

	m_pBg = CPolygon2D::Create();
	m_pBg->SetPos(GetPos());
	m_pBg->SetSize(GetSize());
	m_pBg->SetColor(D3DXCOLOR(0.5f, 0.5f, 0.5f, 0.9f));

	m_pText[0] = CText::Create(D3DXVECTOR3(420.0f, 80.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "壁に当たらずゴールを目指せ！！",
		D3DXVECTOR3(15.0f, 30.0f, 20.0f), D3DXCOLOR(1.0f, 0.1f, 0.1f, 1.0f), CFont::FONT_SOUEIKAKU);

	m_pText[1] = CText::Create(D3DXVECTOR3(250.0f, 350.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "移動キー",
		D3DXVECTOR3(10.0f, 20.0f, 20.0f), D3DXCOLOR(0.1f, 0.6f, 1.0f, 1.0f), CFont::FONT_SOUEIKAKU);

	D3DXVECTOR3 p[4] = { { 300.0f, 400.0f, 0.0f }, { 250.0f, 450.0f, 0.0f }, { 300.0f, 450.0f, 0.0f }, { 350.0f, 450.0f, 0.0f } };
	int aTexIdx[4] = { 102, 103, 104, 105 };

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		//操作のUIを生成する
		m_pCommandUI[nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

		if (m_pCommandUI)
		{//nullチェック

			m_pCommandUI[nCnt]->SetPos(p[nCnt]);
			m_pCommandUI[nCnt]->SetSize(D3DXVECTOR3(70.0f, 70.0f, 0.0f));
			m_pCommandUI[nCnt]->LoadTex(aTexIdx[nCnt]);
		}
	}

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CLabyrinth::Uninit()
{
	if (m_pBg != nullptr)
	{
		m_pBg->Uninit();
		m_pBg = nullptr;
	}

	
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pText[nCnt] != nullptr)
		{
			m_pText[nCnt]->Uninit();
			m_pText[nCnt] = nullptr;
		}
	}

	if (m_pMove != nullptr)
	{// メモリの解放
		delete m_pMove;
		m_pMove = nullptr;
	}

	if (m_pPlayerCollision != nullptr)
	{// 終了処理
		m_pPlayerCollision->Uninit();
		m_pPlayerCollision = nullptr;
	}

	if (m_pPlayer != nullptr)
	{
		m_pPlayer->Uninit();
		m_pPlayer = nullptr;
	}

	if (m_pBlock.size() != 0)
	{
		for (int nCnt = 0; nCnt < (int)m_pBlock.size(); nCnt++)
		{
			if (m_pBlock.at(nCnt) != nullptr)
			{
				m_pBlock.at(nCnt)->Uninit();
				m_pBlock.at(nCnt) = nullptr;
			}
		}

		m_pBlock.clear();
	}

	//操作のUIの破棄
	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		if (m_pCommandUI[nCnt])
		{//nullチェック
			m_pCommandUI[nCnt]->Uninit();		//終了処理
			m_pCommandUI[nCnt] = nullptr;		//ポインタをnullに戻す
		}
	}
	

	CMiniGame::Uninit();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CLabyrinth::Update()
{
	m_pPlayer->SetPosOld(m_pPlayer->GetPos());

	CMiniGame::Update();

	MovePlayer();

	if (m_pPlayerCollision->Collision(OBJETYPE_LABYRINTH_BLOCK, true))
	{
		m_pPlayer->SetPos(m_pBlock.at(m_nStartBulock)->GetPos());
		m_pMove->SetMove(D3DXVECTOR3(0.f,0.f,0.f));
	}

	if (m_pPlayerCollision->Collision(OBJETYPE_LABYRINTH_GOAL, false))
	{
		CMiniGameMNG* pMng = CApplication::GetInstance()->GetSceneMode()->GetMiniMng();
		pMng->SetClear(true);
		return Uninit();
	}
}

//=============================================================================
// ブロック数の設定
// Author : 唐�ｱ結斗
// 概要 : ブロック数の設定して、ブロックを配置する。
//=============================================================================
void CLabyrinth::SetNumBlock(const int nCntBlock)
{
	m_nNumBlock = nCntBlock;

	if (nCntBlock / 2 == 0)
	{
		m_nNumBlock = nCntBlock + 1;
	}

	SetBlock();

	SetMaze();
}

//=============================================================================
// ブロックの配置
// Author : 唐�ｱ結斗
// 概要 : ブロックの配置を行う
//=============================================================================
void CLabyrinth::SetBlock()
{
	const int nMaxBulock = m_nNumBlock * m_nNumBlock;

	// サイズの再設定
	m_pBlock.resize(nMaxBulock);

	for (int nCntBulockY = 0; nCntBulockY < m_nNumBlock; nCntBulockY++)
	{
		for (int nCntBulockX = 0; nCntBulockX < m_nNumBlock; nCntBulockX++)
		{
			int nCntBlock = (m_nNumBlock * nCntBulockY + nCntBulockX);

			m_pBlock.at(nCntBlock) = CMazeBlock::Create();
			CMazeBlock * pBlock = m_pBlock.at(nCntBlock);

			D3DXVECTOR3 pos = GetPos();
			D3DXVECTOR3 size = GetSize();
			D3DXVECTOR3 blockPos = pos - size / 2;
			D3DXVECTOR3 blockSize = size / m_nNumBlock;

			pBlock->SetSize(blockSize);
			blockPos.x += (blockSize.x * nCntBulockX) + (blockSize.x / 2.f);
			blockPos.y += (blockSize.y * nCntBulockY) + (blockSize.y / 2.f);
			pBlock->SetPos(blockPos);
			pBlock->SetColor(D3DXCOLOR(0.5f,0.5f,1.0f,1.0f));
		}
	}
}

//=============================================================================
// 迷路の生成
// Author : 唐�ｱ結斗
// 概要 : 穴掘り法を使用し、迷路を生成する
//=============================================================================
void CLabyrinth::SetMaze()
{
	// 外周を通路にする
	SetUpMaze();

	// スタート地点の検索(奇数マスをランダムで検索)
	int nStart = SearchStart();
	m_nStartBulock = nStart;
	SetPlayer(m_nStartBulock);

	while (nStart != -1)
	{
		int nCopy = nStart;

		// 穴掘り
		nStart = SetDigg(nCopy);

		if (nStart == -1
			&& (int)m_nRoad.size() > 0)
		{
			nStart = ReSearchStart();
		}

		if ((int)m_nRoad.size() > m_nNumBlock * m_nNumBlock)
		{
			CMazeBlock * pBlock = m_pBlock.at(nCopy);
			pBlock->SetObjType(OBJETYPE_LABYRINTH_GOAL);
			pBlock->SetColor(D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
			pBlock->SetUseFlag(true);
			break;
		}
	}

	// 外周を壁に戻す
	TearDownMaze();
}

//=============================================================================
// 迷路生成を行う準備を行う
// Author : 唐�ｱ結斗
// 概要 : 迷路の外周を削除し、迷路生成を行う準備をする
//=============================================================================
void CLabyrinth::SetUpMaze()
{
	for (int nCntY = 0; nCntY < m_nNumBlock; nCntY++)
	{
		int omit = m_nNumBlock - 2;

		if (nCntY == 0
			|| nCntY == m_nNumBlock - 1)
		{
			omit = 0;
		}

		for (int nCntX = 0; nCntX < m_nNumBlock; nCntX++)
		{
			int nCntXY = nCntX + (m_nNumBlock * nCntY);

			CMazeBlock * pBlock = m_pBlock.at(nCntXY);
			pBlock->SetUseFlag(false);

			//if (nCntXY % 2 == 1)
			//{
			//	m_nRoad.push_back(nCntXY);
			//}

			nCntX += omit;
		}
	}
}

//=============================================================================
// 外周を元に戻す
// Author : 唐�ｱ結斗
// 概要 : 迷路の外周を元に戻す
//=============================================================================
void CLabyrinth::TearDownMaze()
{
	for (int nCntY = 0; nCntY < m_nNumBlock; nCntY++)
	{
		int omit = m_nNumBlock - 2;

		if (nCntY == 0
			|| nCntY == m_nNumBlock - 1)
		{
			omit = 0;
		}

		for (int nCntX = 0; nCntX < m_nNumBlock; nCntX++)
		{
			int nCntXY = nCntX + (m_nNumBlock * nCntY);

			CMazeBlock * pBlock = m_pBlock.at(nCntXY);
			pBlock->SetUseFlag(true);

			nCntX += omit;
		}
	}
}

//=============================================================================
// スタート地点の検索
// Author : 唐�ｱ結斗
// 概要 : スタート地点をランダムで検索する
//=============================================================================
int CLabyrinth::SearchStart()
{
	int x = 0;
	int y = 0;
	int nStart = 0;

	while (x % 2 != 1)
	{
		x = hmd::IntRandom(m_nNumBlock - 1, 0);
	}
	while (y % 2 != 1)
	{
		y = hmd::IntRandom(m_nNumBlock - 1, 0);
		
	}

	nStart = x + (m_nNumBlock * y);

	if (nStart > m_nNumBlock * m_nNumBlock)
	{
		assert(false);
	}

	return nStart;
}

//=============================================================================
// スタート位置の再検索
// Author : 唐�ｱ結斗
// 概要 : スタート位置の再検索を行う。
//=============================================================================
int CLabyrinth::ReSearchStart()
{
	int nSearch = -1;

	// イテレータを使用して指定位置の要素を取得
	int nPosition = hmd::IntRandom((int)(m_nRoad.size()) - 1, 0);
	std::list<int>::iterator it = m_nRoad.begin();
	std::advance(it, nPosition);

	if (it != m_nRoad.end())
	{
		nSearch = *it;
	}
	else 
	{
		assert(false);
	}

	return nSearch;
}

//=============================================================================
// 穴を掘る
// Author : 唐�ｱ結斗
// 概要 : 穴掘り法を行う。
//=============================================================================
int CLabyrinth::SetDigg(const int nNumID)
{
	// 穴掘り
	CMazeBlock * pBlock = m_pBlock.at(nNumID);
	pBlock->SetUseFlag(false);

	int x = nNumID % m_nNumBlock;
	int y = nNumID / m_nNumBlock;

	if (x % 2 == 1 && y % 2 == 1)
	{
		bool bPush = true;

		for (auto it = m_nRoad.begin(); it != m_nRoad.end();)
		{
			if (*it == nNumID)
			{
				bPush = true;
				++it;
			}
			else
			{
				++it;
			}
		}

		if (bPush)
		{
			m_nRoad.push_back(nNumID);
		}
	}

	int nSearch = hmd::IntRandom(3, 0);
	int nSearchID[2] = { -1,-1 };
	int nOK = -1;
	bool bDigg = false;

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		int SearchCopy = nSearch;

		nSearch++;

		if (nSearch >= 4)
		{
			nSearch = 0;
		}

		if (SearchCopy == 0)
		{// 上方向
			if (y - 1 > 0 && y - 2 >= 0)
			{
				nSearchID[0] = x + (m_nNumBlock * (y - 1));
				nSearchID[1] = x + (m_nNumBlock * (y - 2));

				bDigg = true;
			}
		}
		else if (SearchCopy == 1)
		{// 右方向
			if (x + 1 < m_nNumBlock - 1 && x + 2 <= m_nNumBlock - 1)
			{
				nSearchID[0] = (x + 1) + (m_nNumBlock * y);
				nSearchID[1] = (x + 2) + (m_nNumBlock * y);

				bDigg = true;
			}
		}
		else if (SearchCopy == 2)
		{// 左方向
			if (x - 1 > 0 && x - 2 >= 0)
			{
				nSearchID[0] = (x - 1) + (m_nNumBlock * y);
				nSearchID[1] = (x - 2) + (m_nNumBlock * y);

				bDigg = true;
			}
		}
		else if (SearchCopy == 3)
		{// 下方向
			if (y + 1 < m_nNumBlock - 1 && y + 2 <= m_nNumBlock - 1)
			{
				nSearchID[0] = x + (m_nNumBlock * (y + 1));
				nSearchID[1] = x + (m_nNumBlock * (y + 2));

				bDigg = true;
			}
		}

		if (bDigg)
		{
			CMazeBlock * pSearchBlock[2] = { nullptr };
			pSearchBlock[0] = m_pBlock.at(nSearchID[0]);
			pSearchBlock[1] = m_pBlock.at(nSearchID[1]);

			if (pSearchBlock[0]->GetUseFlag() && pSearchBlock[1]->GetUseFlag())
			{
				pSearchBlock[0]->SetUseFlag(false);
				pSearchBlock[1]->SetUseFlag(false);

				nOK = nSearchID[1];
				break;
			}
		}
		else
		{
			for (auto it = m_nRoad.begin(); it != m_nRoad.end();)
			{
				if (*it == nNumID)
				{
					it = m_nRoad.erase(it);
				}
				else 
				{
					++it;
				}
			}
		}
	}

	return nOK;
}

//=============================================================================
// プレイヤーの生成
// Author : 唐�ｱ結斗
// 概要 : プレイヤーを生成する
//=============================================================================
void CLabyrinth::SetPlayer(const int nNumID)
{
	m_pPlayer = CPolygon2D::Create(3);
	m_pPlayer->SetPos(m_pBlock.at(nNumID)->GetPos());
	m_pPlayer->SetSize(m_pBlock.at(nNumID)->GetSize() * 0.45f);
	m_pPlayer->SetColor(D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	m_pPlayer->SetObjType(CObject::OBJETYPE_LABYRINTH_PLAYER);

	D3DXVECTOR3 size = m_pPlayer->GetSize();

	m_pPlayerCollision->SetParent(m_pPlayer);
	m_pPlayerCollision->SetSize(size);
}

//=============================================================================
// プレイヤーの移動
// Author : 唐�ｱ結斗
// 概要 : プレイヤーの移動を行う
//=============================================================================
void CLabyrinth::MovePlayer()
{
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (pInput->Press(DIK_W)
		|| pInput->Press(DIK_A)
		|| pInput->Press(DIK_S)
		|| pInput->Press(DIK_D))
	{
		if (pInput->Press(DIK_W))
		{
			move.y += -1.0f;
		}
		if (pInput->Press(DIK_D))
		{
			move.x += 1.0f;
		}
		if (pInput->Press(DIK_A))
		{
			move.x += -1.0f;
		}
		if (pInput->Press(DIK_S))
		{
			move.y += 1.0f;
		}

		D3DXVec3Normalize(&move, &move);
	}

	// 移動情報の計算
	m_pMove->Moving(move);

	// 移動情報の取得
	move = m_pMove->GetMove();

	D3DXVECTOR3 pos = m_pPlayer->GetPos();
	pos += move;
	m_pPlayer->SetPos(pos);
}

