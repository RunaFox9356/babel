//=============================================================================
//
// minimap.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _MINIMAP_H_		// このマクロ定義がされてなかったら
#define _MINIMAP_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"
#include "mapDataManager.h"

//=============================================================================
// 前方宣言
//=============================================================================



class CMinimap : public CPolygon2D
{
public:

	CMinimap();						//コンストラクタ
	~CMinimap() override;			//デストラクタ

	HRESULT Init() override;		//初期化
	void Uninit() override;			//終了
	void Update() override;			//更新

	static CMinimap* Create(D3DXVECTOR3 pos, D3DXVECTOR3 size = DEFAULT_MINIMAP_SIZE);		//生成

private:

	void PlayerPosUpdate();							//プレイヤーの位置の更新
	void CheckObjToFind();							//ドローンがオブジェクトを送信したかどうかをチェックする
	void UpdateFoundObjPos();						//ハッカーが見つかったオブジェクトの位置の更新処理
	void CheckInput();								//インプットの確認
	void AddEscapeIcon();							//エスケープポイントのアイコンの追加

private:

	struct SentObj
	{
		int				nIdx;			//オブジェクトのインデックス
		CPolygon2D*		pIcon;			//オブジェクトのアイコン
	};

	struct StaticIcon
	{
		D3DXVECTOR3		pos;			//位置
		CPolygon2D*		pIcon;			//オブジェクトのアイコン
	};


	static const D3DXVECTOR3	DEFAULT_MINIMAP_SIZE;			//ディフォルトのミニマップのサイズ
	static const D3DXVECTOR3	DEFAULT_OPEN_MAP_SIZE;			//ディフォルトの大きいミニマップのサイズ
	static const float			DEFAULT_MAP_RADIUS;				//ディフォルトのマップの半径
	static const D3DXVECTOR3	DEFAULT_ICON_SIZE;				//ディフォルトのアイコンサイズ
	static const D3DXVECTOR3	DEFAULT_BIG_ICON_SIZE;			//ディフォルトの大きいアイコンサイズ
	static const D3DXCOLOR		DEFAULT_PLAYER_COLOR;			//ディフォルトのプレイヤーの色
	static const D3DXCOLOR		DEFAULT_DRONE_COLOR;			//ディフォルトのドローンの色


	D3DXVECTOR3					m_originalPos;					//元の位置
	D3DXVECTOR3					m_originalSize;					//元のサイズ
	int							m_nCntAnim;						//アニメーションカウンター
	bool						m_bOpen;						//マップが大きいかどうか
	bool						m_bAgent;						//エージェントモードであるかどうか

	CPolygon2D*					m_pPlayer;						//プレイヤーのアイコン
	CPolygon2D*					m_pDrone;						//ドローンのアイコン

	std::vector<SentObj>		m_vObj;							//ハッカーに送信されたオブジェクトのアイコン
	std::vector<StaticIcon>		m_vStaticIcon;					//動けないオブジェクトのアイコン
	std::vector<CMapDataManager::OBJ_TO_FIND_DATA>	m_vLastObjList;				//過去のオブジェクトのベクトル
};

#endif