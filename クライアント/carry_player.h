#ifndef _CARRY_PLAYER_H_		// このマクロ定義がされてなかったら
#define _CARRY_PLAYER_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "GimmicDoor.h"
#include "model_obj.h"
#include "gimmick.h"
//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CModelObj;
class CMove;
class CGimmick;
class CCollision_Rectangle3D;

class CGimmicCarry :public CGimmick
{
public:
	static CGimmicCarry *Create();							// モデルオブジェクトの生成


	CGimmicCarry();
	~CGimmicCarry()override;

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override {}			// 描画

	void SetMove(D3DXVECTOR3 move) { m_move = move; }

	void SetLeft(bool move) { m_IsLeft = move; }

	void SetPos(const D3DXVECTOR3 &pos) override { m_door->SetPos(pos); }				// 位置のセッター
	void SetPosKey(const D3DXVECTOR3 &pos) { m_key->SetPos(pos); }				// 位置のセッター
	void SetPosDefault(D3DXVECTOR3 pos) { m_posDefault = pos; }																// 位置の設定
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {};				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};			// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return m_door->GetPos(); }							// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }						// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void SetModelType(int nIdx);								// モデルの種類の設定

private:
	D3DXVECTOR3 Move();

	CMove *m_pMove;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_posDefault;
	bool m_IsLeft;
	bool m_flg;

	CModelObj* m_door;
	CModelObj* m_key;
	CModelObj* m_hitModle;

	CCollision_Rectangle3D *pCollisionDoorGimmick;
	CCollision_Rectangle3D *pCollisionDoor;
	CCollision_Rectangle3D *pCollisionkey;

	int m_Sin;
};


#endif
