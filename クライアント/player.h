//=============================================================================
//
// プレイヤークラス(player.h)
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行う
//
//=============================================================================
#ifndef _PLAYER_H_			// このマクロ定義がされてなかったら
#define _PLAYER_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <Windows.h>
#include <vector>
#include "d3dx9.h"							// 描画処理に必要
#include "motion_model3D.h"
#include "sound.h"
//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMove;
class CCollision_Rectangle3D;
class CItemObj;
class CEscapePoint;
class CSoundSource;
class CSkinMesh;
//=============================================================================
// プレイヤークラス
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行うクラス
//=============================================================================
class CPlayer : public CMotionModel3D
{
public:
	//--------------------------------------------------------------------
	// 定数定義
	//--------------------------------------------------------------------
	static const unsigned int nMaxItem = 3;

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CPlayer *Create();			// プレイヤーの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CPlayer(int nPriority = PRIORITY_LEVEL0);
	~CPlayer();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	virtual HRESULT Init();												// 初期化
	void Uninit() override;												// 終了
	void Update() override;												// 更新
	void Draw() override;												// 描画
	CMove *GetMove() { return m_pMove; }								// 移動情報の取得
	CCollision_Rectangle3D *GetCollision() { return m_pCollision; }

	D3DXVECTOR3 GetRotDest() { return m_rotDest; }
	void SetRotDest(D3DXVECTOR3 rotDest) { m_rotDest = rotDest; }

	int GetNumMotionOld() { return m_nNumMotionOld; }
	void SetNumMotionOld(int rotDest) { m_nNumMotionOld = rotDest; }

	int GetHavePartsId() { return m_nHavePartsId; }
	void SetHavePartsId(const int nHavePartsId) { m_nHavePartsId = nHavePartsId; }

	const bool GetHiddenState() { return m_bHidden; }						// 隠れているフラグの取得
	void SetHiddenState(const bool bHidden) { m_bHidden = bHidden; }		// 隠れているフラグの設定

	const bool GetBomCommand() { return m_bBomCommand; }					// 隠れているフラグの取得
	void SetBomCommand(const bool bHidden) { m_bBomCommand = bHidden; }		// 隠れているフラグの設定

	void SetNearEscape(CEscapePoint* bEsc) { m_pNearEscape = bEsc; }		//出口の近くにあるかどうかのフラグのセッター
	CEscapePoint* GetNearEscape() { return m_pNearEscape; }					//出口の近くにあるかどうかのフラグのゲッター

	void SetHaveItem(CItemObj* pMyItem);									// アイテムの設定
	
	void SetCurrentItem(const int nCurrentItem);
	int GetCurrentItem() { return m_nCurrentItem; }

	void SetMtxItemParent(D3DXMATRIX *pMtxItemParent) { m_pMtxItemParent = pMtxItemParent; }

	CItemObj* GetMyItem() { return m_pMyItem[m_nCurrentItem]; }				// アイテムの取得
																	
	void SetStopState(const bool bStop) { m_bStop = bStop; }				//動けない状態であるかどうかのフラグの設定処理
	const bool GetStopState() { return m_bStop; }							//動けない状態であるかどうかのフラグの取得処理

	void SetRotationFlag(bool bRotation) { m_bRotation = bRotation; }

	void SetScore(int Score);

	void SetMyHaveId(const int MyHaveId) { m_nMyHaveId = MyHaveId; }		//動けない状態であるかどうかのフラグの設定処理
	const int GetMyHaveId() { return m_nMyHaveId; }							//動けない状態であるかどうかのフラグの取得処理

	CItemObj* SetListItem(int Id);

	CSkinMesh*	GetSkin() { return m_pSkinbody; }
	void SetSkin(CSkinMesh* Skin) { m_pSkinbody = Skin; }
	
	const bool GetRotMove() { return m_bRotMove; }						// 隠れているフラグの取得
	void SetRotMove(const bool RotMove) { m_bRotMove = RotMove; }		// 隠れているフラグの設定

protected:
	void CreateSound(CSound::SOUND_LABEL label, const int nPlayDelay);		//音の設定
	CSoundSource* GetSoundSource() { return m_pSoundSource; }				//サウンドの取得

private:

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	D3DXVECTOR3 Move();						// 移動
	void Rotate();							// 回転
	D3DXVECTOR3 KeySetRotDest();			// キー入力で移動方向を設定し移動値を算出
	D3DXVECTOR3 JoyKeySetRotDest();			// ジョイパッドで移動方向を設定し移動値を算出
	virtual void SetItemOffset();

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<CItemObj*> 		m_pMyItem;						// アイテム
	CMove						*m_pMove;						// 移動情報
	CCollision_Rectangle3D		*m_pCollision;					// 3D矩形の当たり判定
	D3DXMATRIX					*m_pMtxItemParent;				// アイテムの親マトリックス
	D3DXVECTOR3					m_rotDest;						// 目的の向き
	int							m_nHavePartsId;					// アイテムを持つ場所のパーツID
	int							m_nCurrentItem;					// 現在のアイテム
	int							m_nNumMotion;					// 現在のモーション番号
	int							m_nNumMotionOld;				// 現在のモーション番号
	int							m_nMyHaveId;					// 自分が現在持ってるアイテム
	bool						m_bHidden;						// 隠れているフラグ
	bool						m_bCardBoard;					// 段ボールに隠れているフラグ
	bool						m_bBomCommand;					//ばくだんの起爆フラグ
	bool						m_bStop;						//動けない状態であるかどうか
	bool						m_bRotation;					// 回転を行うか否か
	bool						m_bRotMove;						//回転をするかどうか
	CEscapePoint*				m_pNearEscape;					// 近くにある出口へのポインタ
	CSoundSource*				m_pSoundSource;					// 音
	CSkinMesh*					m_pSkinbody;					//スキンメッシュの体



};

#endif

