//*****************************************************************************
// インクルード
//*****************************************************************************
#include "gameover.h"

#include <assert.h>
#include "camera.h"
#include "polygon2D.h"
#include "application.h"
#include "sound.h"
#include "input.h"
#include "tex_anim.h"

#include "typing.h"

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CGameover::CGameover()
{

}
//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CGameover::~CGameover()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CGameover::Init()
{
	{//初期化
	 UiCount = 0;
	 SceneCount = 0;
	 Uiflg = true;
	 enterflg = false;
	}
	// タイトル背景
	pPolygon = CPolygon2D::Create();
	pPolygon->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));
	pPolygon->SetSize(D3DXVECTOR3(1280.0f*1.0f, 720.0f*1.0f, 0.0f));
	pPolygon->LoadTex(86);

	//点滅中指ばぁーさん
	pPolygonfinger = CPolygon2D::Create();
	pPolygonfinger->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));
	pPolygonfinger->SetSize(D3DXVECTOR3(1280.0f*0.3f, 720.0f*0.3f, 0.0f));
	pPolygonfinger->LoadTex(87);

	//エンター押せに変更
	pPolygonEnter = CPolygon2D::Create();
	pPolygonEnter->SetPos(D3DXVECTOR3(640.0f, 560.0f, 0.0f));
	pPolygonEnter->SetSize(D3DXVECTOR3(1280.0f*0.3f, 720.0f*0.3f, 0.0f));
	pPolygonEnter->LoadTex(93);

	//ゲームオーバーUIに変更
	pPolygonGameOverUI = CPolygon2D::Create();
	pPolygonGameOverUI->SetPos(D3DXVECTOR3(640.0f, 160.0f, 0.0f));
	pPolygonGameOverUI->SetSize(D3DXVECTOR3(1280.0f*0.8f, 720.0f*0.8f, 0.0f));
	pPolygonGameOverUI->LoadTex(94);

	
	//アニメーション
	animPoly = new CTexAnim;
	animPoly->SetAnim(3, 1, 10, true);
	m_move = 0.0f;

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CGameover::Uninit()
{
	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CGameover::Update()
{
	//アニメーション
	m_move += 0.001f;
	if (m_move > 1.0f)
	{
		m_move = 0;
	}
	pPolygon->SetTex(0, D3DXVECTOR2(m_move, 0.0f),
		D3DXVECTOR2(1.0f + m_move, 1.0f));
	//animPoly->PlayAnim();
	//----------------

	//UI表示の為のカウント
	UiCount++;

	//サインカーブ
	float RotSpeed = 0.3f;
	float a = (float)(sinf(D3DXToRadian(UiCount * RotSpeed)) * 2.0f);	
	if (UiCount < 30)
	{
		pPolygonfinger->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else if (UiCount > 60)
	{
		pPolygonfinger->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
	if (UiCount == 90)
	{
		UiCount = 0;
	}

	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	//エンター押したとき
	if (pInput->Trigger(DIK_RETURN))
	{
		if (enterflg)
		{
			return;
		}
		enterflg = true;
	}

	if (enterflg)
	{
		SceneCount++;

		if (SceneCount >= 40)
		{
			CApplication::GetInstance()->SetNextMode(CApplication::MODE_MAPSELECT);
		}
	}
}
