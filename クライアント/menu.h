//=============================================================================
//
// menu.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _MENU_H_		// このマクロ定義がされてなかったら
#define _MENU_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CPolygon2D;
class CMenuButton;


class CMenu : public CObject
{
public:

	struct BUTTON_DATA
	{
		D3DXVECTOR3 size;			//ボタンのサイズ
		D3DXCOLOR	normalCol;		//普通の色
		D3DXCOLOR	triggerCol;		//トリガーの色
	};

public:
	CMenu();						//コンストラクタ
	~CMenu() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &pos);								// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) {}						// 過去位置のセッタ
	void SetRot(const D3DXVECTOR3 &/*rot*/) {}								// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size);								// 大きさのセッター
	D3DXVECTOR3 GetPos() { return m_pos; }								// 位置のゲッター
	D3DXVECTOR3 GetPosOld() { return D3DXVECTOR3(); }					// 過去位置のゲッタ
	D3DXVECTOR3 GetRot() { return D3DXVECTOR3(); }						// 向きのゲッター
	D3DXVECTOR3 GetSize() { return m_size; }							// 大きさのゲッター

	void SetBackgroundTexture(const int nTexIdx);						//背景のテクスチャの設定
	void SetButtonTexture(const int nButtonNumber, const int nTexIdx);	//ボタンのテクスチャの設定

	void AddButton(CMenuButton* pButton);				//ボタンの追加
	void CreateButton(const D3DXVECTOR3 pos);			//ボタンの生成(位置は(相対です)
	void CreateButton(const D3DXVECTOR3 pos, const D3DXCOLOR normalCol, const D3DXCOLOR triggerCol);	//ボタンの生成(位置は相対です)

	const int GetTriggeredButton();						//押されたボタンのインデックス取得
	const int GetHoverButton();							//マウスカーソルと重なっているボタンのインデックス取得

	static CMenu* Create(const D3DXVECTOR3 pos = DEFAULT_POS, const D3DXVECTOR3 size = DEFAULT_SIZE);	//生成

private:

	static const D3DXVECTOR3 DEFAULT_POS;			//ディフォルトの位置
	static const D3DXVECTOR3 DEFAULT_SIZE;			//ディフォルトのサイズ


	D3DXVECTOR3 m_pos;				//位置
	D3DXVECTOR3 m_size;				//サイズ

	CPolygon2D* m_pBg;				//背景ポリゴン

	std::vector <CMenuButton*> m_vButton;		//ボタン
};


#endif