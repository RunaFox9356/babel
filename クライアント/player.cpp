//=============================================================================
//
// プレイヤークラス(player.cpp)
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "player.h"
#include "game.h"
#include "mesh.h"
#include "motion.h"
#include "renderer.h"
#include "application.h"
#include "camera.h"
#include "input.h"
#include "calculation.h"
#include "move.h"
#include "collision_rectangle3D.h"
#include "debug_proc.h"
#include "parts.h"
#include "sound.h"
#include "scene_mode.h"
#include "itemObj.h"
#include "gun.h"
#include "AgentUI.h"
#include "HackerUI.h"
#include "hacker.h"
#include "Score.h"
#include "sphere.h"
#include "soundSource.h"
#include "model_skin.h"
//--------------------------------------------------------------------
// 静的メンバ変数の定義
//--------------------------------------------------------------------

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CPlayer * CPlayer::Create()
{
	// オブジェクトインスタンス
	CPlayer *pPlayer = nullptr;

	// メモリの解放
	pPlayer = new CPlayer();

	// メモリの確保ができなかった
	assert(pPlayer != nullptr);

	// 数値の初期化
	pPlayer->Init();

	// インスタンスを返す
	return pPlayer;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CPlayer::CPlayer(int nPriority) : CMotionModel3D(nPriority),
m_pMove(nullptr),
m_pCollision(nullptr),
m_pMtxItemParent(nullptr),									
m_rotDest(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nNumMotion(0),
m_nNumMotionOld(0),
m_bHidden(false), 
m_pNearEscape(nullptr),
m_nCurrentItem(0),
m_bStop(false),
m_bRotation(true),
m_bRotMove(true),
m_nMyHaveId(0),
m_nHavePartsId(3),
m_pSoundSource(nullptr)
{
	m_pMyItem.clear();
	m_pMyItem.resize(nMaxItem);
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CPlayer::~CPlayer()
{
}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CPlayer::Init()
{
	// 初期化
	CMotionModel3D::Init();

	// オブジェクトタイプの設定
	SetObjType(OBJETYPE_PLAYER);

	// 移動クラスのメモリ確保
	m_pMove = new CMove;
	assert(m_pMove != nullptr);

	// 3D矩形の当たり判定の設定
	m_pCollision = CCollision_Rectangle3D::Create();
	m_pCollision->SetParent(this);
	//SetHaveItem(CItemObj::ITEM_NON);
	m_pSkinbody = nullptr;
	 
	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CPlayer::Uninit()
{
	// カメラの追従設定(目標 : プレイヤー)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);
	
	if (m_pMove != nullptr)
	{// メモリの解放
		delete m_pMove;
		m_pMove = nullptr;
	}

	//当たり判定の破棄
	if (m_pCollision != nullptr)
	{// 終了処理
		m_pCollision->Uninit();
		m_pCollision = nullptr;
	}

	//音の破棄
	if (m_pSoundSource)
	{//終了処理
		m_pSoundSource->Uninit();
		m_pSoundSource = nullptr;
	}

	// 終了
	CMotionModel3D::Uninit();

	// ゲーム終了
	CGame::SetGame(false);
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CPlayer::Update()
{
	// 位置の取得
	D3DXVECTOR3 pos = GetPos();

	if (!m_bStop)
	{
		// 移動
		pos += Move();

		// 位置の設定
		SetPos(pos);
	}

	// 回転
	if (!m_bStop)
		Rotate();

	// 更新
	CMotionModel3D::Update();

	//スカイボックスの半径を取得する
	float fRadius = CSphere::GetDefaultSphereRadius();

	if (CApplication::GetInstance()->GetMap() == 0 && (pos.x * pos.x) + (pos.y * pos.y) + (pos.z * pos.z) >= fRadius * fRadius)
	{//スカイボックスを出ないようにする

		D3DXVECTOR3 vec = pos;

		D3DXVec3Normalize(&vec, &vec);

		pos.x = vec.x * fRadius;
		pos.y = vec.y * fRadius;
		pos.z = vec.z * fRadius;
	}

	// 位置の設定
	SetPos(pos);
	if (GetSkin() != nullptr)
	{
		GetSkin()->SetPos(GetPos());
		GetSkin()->SetRot(GetRot());
	}
	SetItemOffset();
	// デバック表示
	CDebugProc::Print("プレイヤーの位置 | X : %.3f | Y : %.3f | Z : %.3f |\n", pos.x, pos.y, pos.z);
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CPlayer::Draw()
{
	// 描画
	CMotionModel3D::Draw();
}

//=============================================================================
// 移動
// Author : 浜田琉雅
// 概要 : モデルの貼り付け
//=============================================================================
void CPlayer::SetHaveItem(CItemObj* pMyItem)
{
	if (pMyItem == nullptr)
	{
		return;
	}

	// モーション情報の取得
	CMotion *pMotion = CMotionModel3D::GetMotion();
	CParts *pHaveParts = nullptr;
	if (pMotion != nullptr)
	{
		pHaveParts = pMotion->GetParts(m_nHavePartsId);
	}

	bool bHaved = false;

	int nSize = m_pMyItem.size();
	if (nSize > 0
		&& pMyItem->GetParent() == nullptr)
	{
		for (int nCnt = 0; nCnt < nSize; nCnt++)
		{
			if (m_pMyItem[nCnt] == nullptr)
			{
				m_pMyItem[nCnt] = pMyItem;

				if (m_pMtxItemParent == nullptr)
				{
					if (pHaveParts != nullptr)
					{
						m_pMyItem[nCnt]->SetParent(pHaveParts);
					}
				}
				else
				{
					m_pMyItem[nCnt]->SetMtxParent(m_pMtxItemParent);
				}

				SetCurrentItem(nCnt);
				bHaved = true;
				break;
			}
		}
	}

	if (!bHaved)
	{
		if (m_pMtxItemParent == nullptr)
		{
			m_pMyItem[m_nCurrentItem]->SetParent();
		}
		else
		{
			m_pMyItem[m_nCurrentItem]->SetMtxParent();
		}
		m_pMyItem[m_nCurrentItem] = pMyItem;
		m_nMyHaveId = m_pMyItem[m_nCurrentItem]->GetId();
		
		if (m_pMtxItemParent == nullptr)
		{
			if (pHaveParts != nullptr)
			{
				m_pMyItem[m_nCurrentItem]->SetParent(pHaveParts);
			}
		}
		else
		{
			m_pMyItem[m_nCurrentItem]->SetMtxParent(m_pMtxItemParent);
		}
	}
}

//=============================================================================
// 移動
// Author : 浜田琉雅
// 概要 : モデルの貼り付け
//=============================================================================
void CPlayer::SetCurrentItem(const int nCurrentItem)
{
	if(nCurrentItem < nMaxItem)
	{
		int nCurrentItemOld = m_nCurrentItem;
		m_nCurrentItem = nCurrentItem;

		if (m_pMyItem[nCurrentItemOld] != nullptr
			&& m_pMyItem[m_nCurrentItem] != nullptr)
		{
			m_pMyItem[nCurrentItemOld]->SetDrawFlag(false);

			if (m_pMyItem[nCurrentItemOld]->GetObjType() == CObject::OBJETYPE_GUN)
			{
				CGun *pGun = (CGun*)m_pMyItem[nCurrentItemOld];
				pGun->SetReload(false);
			}
			m_nMyHaveId = m_pMyItem[m_nCurrentItem]->GetId();
			m_pMyItem[m_nCurrentItem]->SetDrawFlag(true);
		}
		else
		{
			m_nCurrentItem = nCurrentItemOld;
		}
	}
	else
	{
		assert(false);
	}
}

//=============================================================================
// スコアの加算
// Author : 浜田
// 概要 : 
//=============================================================================
void CPlayer::SetScore(int Score)
{
	CAgentUI* pUiManager = CGame::GetUiManager();
	int nScore = 0;
	if (pUiManager)
	{
		nScore =  Score;
		pUiManager->SetScore(nScore);
	}
	CHackerUI* pHackerUiManager = CHacker::GetHackerUI();

	if (pHackerUiManager)
	{
		nScore = Score;
		pHackerUiManager->SetScore(nScore);
	}
}
//=============================================================================
// 移動
// Author : 唐�ｱ結斗
// 概要 : キー入力で方向を決めて、移動ベクトルを算出する
//=============================================================================
D3DXVECTOR3 CPlayer::Move()
{
	// 変数宣言
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 moveing = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (!m_bStop)
	{
		// 移動情報の取得
		move = KeySetRotDest();

		// 移動情報の計算
		m_pMove->Moving(move);

		// 移動情報の取得
		moveing = m_pMove->GetMove();

		// デバック表示
		CDebugProc::Print("移動ベクトル : %.3f\n", m_pMove->GetMoveLength());

		// 向きの取得
		D3DXVECTOR3 rot = GetRot();

		// 目的の向きの補正
		if (m_rotDest.y - rot.y >= D3DX_PI)
		{// 移動方向の正規化
			m_rotDest.y -= D3DX_PI * 2;
		}
		else if (m_rotDest.y - rot.y <= -D3DX_PI)
		{// 移動方向の正規化
			m_rotDest.y += D3DX_PI * 2;
		}
	}

	return moveing;
}

//=============================================================================
// 回転
// Author : 唐�ｱ結斗
// 概要 : 目的の向きまで回転する
//=============================================================================
void CPlayer::Rotate()
{
	// 向きの取得
	D3DXVECTOR3 rot = GetRot();
	float fFriction = 0.5f;

	// 向きの更新
	rot.y += (m_rotDest.y - rot.y) * fFriction;

	// 向きの正規化
	rot.y = CCalculation::RotNormalization(rot.y);

	// 向きの設定
	SetRot(rot);
}

//=============================================================================
// キー入力で移動方向を設定し移動値を算出
// Author : 唐�ｱ結斗
// 概要 : 入力された値から移動方向を設定し移動値を算出
//=============================================================================
D3DXVECTOR3 CPlayer::KeySetRotDest()
{
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	if (m_bRotMove)
	{
		// 入力デバイスの取得
		CInput *pInput = CInput::GetKey();

		if ((pInput->Press(DIK_W)
			|| pInput->Press(DIK_A)
			|| pInput->Press(DIK_D)
			|| pInput->Press(DIK_S)))
		{// 移動キーが押された
			if (pInput->Press(DIK_W))
			{// [W]キーが押された時
				if (pInput->Press(DIK_A))
				{// [A]キーが押された時
				 // 移動方向の更新
					rotDest.y = D3DX_PI * -0.25f;
				}
				else if (pInput->Press(DIK_D))
				{// [D]キーが押された時
				 // 移動方向の更新
					rotDest.y = D3DX_PI * 0.25f;
				}
				else
				{// 移動方向の更新
					rotDest.y = D3DX_PI * 0.0f;
				}
			}
			else if (pInput->Press(DIK_S))
			{// [S]キーが押された時
				if (pInput->Press(DIK_A))
				{// [A]キーが押された時
				 // 移動方向の更新
					rotDest.y = D3DX_PI * -0.75f;
				}
				else if (pInput->Press(DIK_D))
				{// [D]キーが押された時
				 // 移動方向の更新
					rotDest.y = D3DX_PI * 0.75f;
				}
				else
				{// 移動方向の更新q
					rotDest.y = D3DX_PI;
				}
			}
			else if (pInput->Press(DIK_A))
			{// [A]キーが押された時
			 // 移動方向の更新
				rotDest.y = D3DX_PI * -0.5f;
			}
			else if (pInput->Press(DIK_D))
			{// [D]キーが押された時
			 // 移動方向の更新
				rotDest.y = D3DX_PI * 0.5f;
			}

			// カメラ情報の取得
			CCamera *pCamera = CApplication::GetInstance()->GetCamera();

			// 移動方向の算出
			rotDest.y += pCamera->GetRot().y;

			// 移動方向の正規化
			rotDest.y = CCalculation::RotNormalization(rotDest.y);

			// 移動量の計算
			move = D3DXVECTOR3(sinf(rotDest.y), 0.0f, cosf(rotDest.y));

			// 角度の正規化
			rotDest.y -= D3DX_PI;

			// 移動方向の正規化
			rotDest.y = CCalculation::RotNormalization(rotDest.y);

			if (!m_bRotation)
			{
				return move;
			}

			m_rotDest.y = rotDest.y;
		}
	}
	return move;
}

//=============================================================================
// itemを探す
// Author : 浜田
// 概要 : 
//=============================================================================
CItemObj* CPlayer::SetListItem(int Id)
{
	CSuper *pCollision = CSuper::GetTop(0);
	if (pCollision != nullptr)
	{
		while (pCollision)
		{// 現在のオブジェクトの次のオブジェクトを保管
			CSuper *pCollisionNext = pCollision->GetNext();
			CObject *Obj = (CObject*)pCollision;
			if (Obj->GetObjType() == OBJETYPE_GUN)
			{	
				CItemObj* ItemObj = (CItemObj*)Obj;
				if (Id == ItemObj->GetId())
				{
					return ItemObj;
				}
			}
			pCollision = pCollisionNext;
		}
	}
	return nullptr;
}

//音の設定
void CPlayer::CreateSound(CSound::SOUND_LABEL label, const int nPlayDelay)
{
	if (!m_pSoundSource)
	{
		m_pSoundSource = CSoundSource::Create(this, label, nPlayDelay, 10.0f, 500.0f);
	}
}

// アイテムの位置と向きの設定
void CPlayer::SetItemOffset()
{
	CItemObj* pMyItem = GetMyItem();

	if (pMyItem == nullptr)
	{
		return;
	}

	D3DXVECTOR3 posOffset = D3DXVECTOR3(0.f, 0.f, 0.f);
	D3DXVECTOR3 rotOffset = D3DXVECTOR3(0.f, 0.f, 0.f);

	switch (pMyItem->GetObjType())
	{
	case CObject::OBJETYPE_GUN:
		posOffset = D3DXVECTOR3(3.0f, 0.f, 0.f);
		rotOffset = D3DXVECTOR3(D3DX_PI * 0.5f, -D3DX_PI, 0.f);
		break;
	default:
		break;
	}

	pMyItem->SetPosOffset(posOffset);
	pMyItem->SetRotOffset(rotOffset);
}



