//=============================================================================
//
// bomb.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _BOMB_H
#define _BOMB_H


//=============================================================================
// インクルード
//=============================================================================
#include "model_obj.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CBomb : public CModelObj
{
public:
	CBomb();						//コンストラクタ
	~CBomb() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新

	void SetDeath(const bool bDeath) { m_bDead = bDeath; }		//爆発したかどうかの設定
	const bool GetDeath() { return m_bDead; }					//爆発したかどうかの取得


	static CBomb* Create(const D3DXVECTOR3 pos);	//生成

private:

	static const int DEFAULT_BOMB_MODEL;			//ディフォルトのモデルのインデックス
	static const int DEFAULT_BOMB_LIFE;				//ディフォルトの爆弾の寿命

	int		m_nLife;				//爆発までのフレーム数
	bool	m_bDead;				//爆発したかどうか
};




#endif