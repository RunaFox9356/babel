#include "filter.h"
#include "renderer.h"

CFilter::CFilter()
{
	m_pRenderTexture = nullptr;			// レンダーテクスチャ用のポインタ
	m_pDrowSurface = nullptr;			// 描画用サーフェイスのポインタ
}

CFilter::~CFilter()
{
}

void CFilter::SetUp(LPDIRECT3DDEVICE9 dev)
{
	m_pDevice = dev;

	// レンダーターゲットの作成
	m_pDevice->CreateTexture(CRenderer::SCREEN_WIDTH,
		CRenderer::SCREEN_HEIGHT,
		1,
		D3DUSAGE_RENDERTARGET,
		D3DFMT_X8R8G8B8,
		D3DPOOL_DEFAULT, 
		&m_pRenderTexture,
		NULL);
	// 描画サーフェイスの設定
	m_pRenderTexture->GetSurfaceLevel(0, &m_pDrowSurface);

	// 元のサーフェイスの保存
	m_pDevice->GetRenderTarget(0, &m_pOrgSurface);
}

void CFilter::CleanUp()
{
	// レンダーテクスチャの破棄
	if (m_pRenderTexture != nullptr)
	{
		m_pRenderTexture->Release();
		m_pRenderTexture = nullptr;
	}

	// 描画用サーフェイスの破棄
	if (m_pDrowSurface != nullptr)
	{
		m_pDrowSurface->Release();
		m_pDrowSurface = nullptr;
	}
}