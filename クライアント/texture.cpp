//=============================================================================
//
// テクスチャ設定処理(texture.cpp)
// Author : 唐�ｱ結斗
// 概要 : テクスチャ設定を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "texture.h"
#include "renderer.h" 
#include "application.h"
#include "file.h"

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CTexture::CTexture()
{
	m_pTexture = nullptr;		// テクスチャ情報
	m_nMaxTexture = 0;			// テクスチャの最大数
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CTexture::~CTexture()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 貼り付けるテクスチャ情報を格納する
//=============================================================================
void CTexture::Init(void)
{// レンダラーのゲット
	
	
	// ファイルの読み込み
	LoadFileJson();
	//SaveFile();
	for (int nCnt = 0; nCnt < m_nMaxTexture; nCnt++)
	{//ポリゴンに貼り付けるテクスチャの読み込み

		
	}
}

std::future<void> CTexture::InitAsync()
{
	return std::async(std::launch::async, [this]() { LoadFileJsonAsync(); });
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャの解放
//=============================================================================
void CTexture::Uninit(void)
{
	
}

//=============================================================================
// テクスチャポインタのゲッター	
// Author : 唐�ｱ結斗
// 概要 : テクスチャのゲッター
//=============================================================================
LPDIRECT3DTEXTURE9 CTexture::GetTexture(const int nNumTex)
{
	LPDIRECT3DTEXTURE9 pTexture = nullptr;

	if (nNumTex != -1)
	{// タイプが設定されてる

		m_key = m_KeyTexMap[nNumTex];
		pTexture = m_TexMap[m_key].pTexture;
	}

	return pTexture;
}

//=============================================================================
// ファイルの読み込み
// Author : 唐�ｱ結斗
// 概要 : ファイルから読み込むテクスチャ数と名前を読み込む
//=============================================================================
void CTexture::LoadFile()
{
	// 変数宣言
	char aStr[128];
	int nCntTex = 0;

	// ファイルの読み込み
	FILE *pFile = fopen("data/FILE/texture.txt", "r");

	if (pFile != nullptr)
	{
		while (fscanf(pFile, "%s", &aStr[0]) != EOF)
		{// "EOF"を読み込むまで 
			if (strncmp(&aStr[0], "#", 1) == 0)
			{// 一列読み込む
				fgets(&aStr[0], sizeof(aStr), pFile);
			}

			if (strstr(&aStr[0], "MAX_TEXTURE") != NULL)
			{
				fscanf(pFile, "%s", &aStr[0]);
				fscanf(pFile, "%d", &m_nMaxTexture);
				m_pTexture = new TEXTURE[m_nMaxTexture];
				assert(m_pTexture != nullptr);
				memset(&m_pTexture[0], 0, sizeof(TEXTURE));
			}

			if (strstr(&aStr[0], "TEXTURE_FILENAME") != NULL)
			{
				fscanf(pFile, "%s", &aStr[0]);
				fscanf(pFile, "%s", &m_pTexture[nCntTex].aFileName[0]);
				nCntTex++;
			}
		}
	}
	else
	{
		assert(false);
	}
}
//=============================================================================
// ファイルの読み込み
// Author : 唐�ｱ結斗
// 概要 : ファイルから読み込むテクスチャ数と名前を読み込む
//=============================================================================
void CTexture::LoadFileJson()
{
	nl::json m_Loadjson;

	CFile*Json = new CFile;

	m_Loadjson = Json->LoadJson(L"data/FILE/tex.json");

	m_nMaxTexture = m_Loadjson["TEX"].size();

	// デバイスの取得
	for (int nCnt = 0; nCnt < m_nMaxTexture; nCnt++)
	{
		int ID = m_Loadjson["TEX"][nCnt][0];
		std::string PASS = m_Loadjson["TEX"][nCnt][1];
		std::string KEY = m_Loadjson["TEX"][nCnt][2];

		m_KeyTexMap.push_back(KEY);

		CTexture::TEXTURE data = CTexture::LoadTex(PASS);
		data.nindex = nCnt;
		//値をマップに保存
		m_TexMap[KEY] = data;
	}
	delete Json;
}

void CTexture::LoadFileJsonAsync()
{
	std::future<void> loadTexAsync = std::async(std::launch::async, [&]()
	{
		CFile* Json = new CFile;

		nlohmann::json m_Loadjson = Json->LoadJson(L"data/FILE/tex.json");

		m_nMaxTexture = m_Loadjson["TEX"].size();

		std::vector<std::future<TEXTURE>> futures;

		for (int nCnt = 0; nCnt < m_nMaxTexture; nCnt++)
		{
			int ID = m_Loadjson["TEX"][nCnt][0];
			std::string PASS = m_Loadjson["TEX"][nCnt][1];
			std::string KEY = m_Loadjson["TEX"][nCnt][2];

			m_KeyTexMap.push_back(KEY);

			// 非同期でテクスチャ読み込み
			futures.push_back(std::async(std::launch::async, &CTexture::LoadTex, this, PASS));
		}

		// 待機処理
		for (int nCnt = 0; nCnt < m_nMaxTexture; nCnt++)
		{
			CTexture::TEXTURE data = futures[nCnt].get();
			data.nindex = nCnt;

			m_TexMap[m_KeyTexMap[nCnt]] = data;
		}

		delete Json;
	});

	// 非同期処理待機
	loadTexAsync.get();
}

//=============================================================================
// ファイルの読み込み
// Author : 唐�ｱ結斗
// 概要 : ファイルから読み込むテクスチャ数と名前を読み込む
//=============================================================================
void CTexture::SaveFileJson()
{
	nl::json m_savejson;

	CFile*Json = new CFile;

	m_savejson["INDEX"] = m_nMaxTexture;

	for (int i = 0; i < m_nMaxTexture; i++)
	{
		std::string name = "TEX";

		std::string KEY;
		std::string Pass;
		Pass = &m_pTexture[i].aFileName[0];

		int data = Pass.rfind("/");
		int hoge = Pass.find(".");
		int size = Pass.size();
		data += 1;
		size -= data+4;
		std::string key = Pass.substr(data, size);

		m_savejson[name.c_str()][i] = { i,Pass.c_str(),key.c_str() };
	}

	Json->SaveJson(m_savejson, "data/FILE/TEX.json");

	delete Json;
}

//=============================================================================
// テクスチャ読み込む
// Author : 唐�ｱ結斗
// 概要 : 貼り付けるテクスチャ情報を格納する
//=============================================================================
void CTexture::SetTex(std::string pass)
{
	std::string Pass;
	Pass = pass;

	int data = Pass.rfind("/");
	int hoge = Pass.find(".");
	int size = Pass.size();
	data += 1;
	size -= data + 4;
	std::string key = Pass.substr(data, size);
	CTexture::TEXTURE tex = CTexture::LoadTex(pass.c_str());
	tex.nindex = m_nMaxTexture;
	//m_KeyTexMap.push_back(m_nMaxTexture);
	m_nMaxTexture++;
	m_TexMap[key] = tex;

	SaveFileJson();
}


//=============================================================================
// テクスチャ読み込む
// Author : 唐�ｱ結斗
// 概要 : 貼り付けるテクスチャ情報を格納する
//=============================================================================
CTexture::TEXTURE CTexture::LoadTex(std::string pass)
{
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	TEXTURE tex;
	tex.aFileName = pass;
	D3DXIMAGE_INFO imgInfo;
	D3DXGetImageInfoFromFile(&tex.aFileName[0], &imgInfo);

	D3DXCreateTextureFromFileEx(pRenderer->GetDevice(),
		&tex.aFileName[0],
		imgInfo.Width,
		imgInfo.Height,
		imgInfo.MipLevels,
		0,
		imgInfo.Format,
		D3DPOOL_MANAGED,
		D3DX_FILTER_LINEAR,
		D3DX_FILTER_LINEAR,
		0xff,
		nullptr,
		nullptr,
		&tex.pTexture);
	
	return tex;
}

