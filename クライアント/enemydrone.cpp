//=============================================================================
//
// walkingEnemy.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "enemydrone.h"
#include "game.h"
#include "agent.h"
#include "viewField.h"
#include "move.h"
#include "calculation.h"
#include "motion.h"
#include "collision_rectangle3D.h"

#include "debug_proc.h"
#include "input.h"

#include "hacker.h"

#include "application.h"
#include "HackerUI.h"
#include "Timer.h"
#include "camera.h"
#include "bullet.h"
//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3 CEnemyDrone::EYE_RELATIVE_POS = D3DXVECTOR3(0.0f, 42.0f, 0.0f);			//目の相対位置
const float		  CEnemyDrone::TARGET_RADIUS = 10.0f;										//ターゲットの半径
const float		  CEnemyDrone::LOOK_AROUND_ANGLE = D3DX_PI * 0.25f;						//周りを見る時の加算される角度
const D3DXVECTOR3 CEnemyDrone::DEFAULT_COLLISION_POS = D3DXVECTOR3(0.0f, 30.0f, 0.0f);	//ディフォルトの当たり判定の相対位置
const D3DXVECTOR3 CEnemyDrone::DEFAULT_COLLISION_SIZE = D3DXVECTOR3(30.0f, 60.0f, 30.0f);	//ディフォルトの当たり判定のサイズ


//=============================================================================
//コンストラクタ
//=============================================================================
CEnemyDrone::CEnemyDrone() : m_nCurrentTarget(0),
m_newTarget(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nCntAnim(0),
m_nCntPhase(0),
m_bSeen(false),
m_pViewField(nullptr)
{
	m_side = SIDE_NEUTRAL;
	CEnemy::SetObjType(CObject::OBJETYPE_HACKENEMY);
	for (int nCnt = 0; nCnt < MAX_TARGET_POS; nCnt++)
	{
		m_targetPos[nCnt] = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}
}

//=============================================================================
//デストラクタ
//=============================================================================
CEnemyDrone::~CEnemyDrone()
{

}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CEnemyDrone::Init()
{
	//親クラスの初期化処理
	CEnemy::Init();

	CApplication::GetInstance()->GetSceneMode()->SetEnemyUse(true, GetId());	//モデル生成？
	m_pViewField = CViewField::Create((CEnemy*)this);		//視野の生成

	if (m_pViewField)
	{//nullチェック

		m_pViewField->SetPosV(EYE_RELATIVE_POS);					//視野の相対位置の設定
		m_pViewField->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));		//視野の相対回転の設定
	}

	m_state = STATE_WALKING;			//現在の状態の設定

	CCollision_Rectangle3D* pCollision = CEnemy::GetCollision();		//当たり判定の取得
	CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
	if (pCollision)
	{//当たり判定のサイズと相対位置の設定

		CEnemy::GetCollision()->SetSize(DEFAULT_COLLISION_SIZE);
		CEnemy::GetCollision()->SetPos(DEFAULT_COLLISION_POS);
	}

	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CEnemyDrone::Uninit()
{
	//視野の破棄
	if (m_pViewField)
	{
		m_pViewField->Uninit();
		m_pViewField = nullptr;
	}

	//親クラスの終了処理
	CEnemy::Uninit();
}

//=============================================================================
// 更新
//=============================================================================
void CEnemyDrone::Update()
{
	switch (m_side)
	{
	case CEnemyDrone::SIDE_ENEMY:
		EnemyMode();
		CApplication::GetInstance()->GetSceneMode()->SetEnemyPos(CEnemy::GetPos(), GetId());

		if (GetDeath())
		{//死亡フラグがtrueだったら

			Kill();			//死ぬように設定する
			return;
		}
		break;
	case CEnemyDrone::SIDE_NEUTRAL:
		break;
	case CEnemyDrone::SIDE_PLAYER:
		Control();
		//dynamic_cast<CDrone*>(this)->Update();
		break;
	}

#ifdef _DEBUG
	//デバッグ用の処理（殺す）
	CInput *pInput = CInput::GetKey();
	if (pInput->Trigger(DIK_5))
	{
		// これ通った時めっちゃメッシュが暴走します
		Kill();
	}
	if (pInput->Trigger(DIK_9))
	{
		Hacking();
	}
#endif // _DEBUG
}

//=============================================================================
//目の相対位置の設定
//=============================================================================
void CEnemyDrone::SetEyePos(const D3DXVECTOR3 eyePos)
{
	if (m_pViewField)
	{
		m_pViewField->SetPosV(eyePos);
	}
}


//=============================================================================
//ハッキングされたときの設定
//=============================================================================
void CEnemyDrone::Hacking()
{
	SetSide(SIDE_PLAYER);
}

//生成
CEnemyDrone* CEnemyDrone::Create(const D3DXVECTOR3 pos)
{
	CEnemyDrone* pEnemy = new CEnemyDrone;	//インスタンスを生成する

											//初期化処理
	if (FAILED(pEnemy->Init()))
		return nullptr;

	pEnemy->CEnemy::SetPos(pos);								//位置の設定
	pEnemy->m_targetPos[0] = pos;						//移動の始点の設定
	pEnemy->m_targetPos[1] = D3DXVECTOR3(1000,0,0);		//移動の終点の設定
	pEnemy->m_nCurrentTarget = 1;						//現在の移動の目的の位置の設定

	return pEnemy;										//生成したインスタンスを返す
}


//=============================================================================
//操作
//=============================================================================
void CEnemyDrone::Control()
{
	// 変数宣言
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	// 移動クラスのメモリ確保
	CMove *pMove = CEnemy::GetMove();

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	if (pInput->Press(DIK_SPACE) ||
		pInput->Press(DIK_LSHIFT) ||
		pInput->Press(MOUSE_INPUT_LEFT) ||
		pInput->Press(MOUSE_INPUT_RIGHT))
	{// 移動キーが押された
		if (pInput->Press(DIK_SPACE))
		{// スペースキーが押された時
		 // 移動方向の更新
			move.y = 1.0f;
		}
		if (pInput->Press(DIK_LSHIFT))
		{// 左シフトが押された時
		 // 移動方向の更新
			move.y = -1.0f;
		}
		if (pInput->Trigger(MOUSE_INPUT_LEFT) && m_nHealTimer <= 0)
		{// スペースキーが押された時
		 // 移動方向の更新
			Shot(3);
			m_nHealTimer = 600;
		}
		if (pInput->Trigger(MOUSE_INPUT_RIGHT) && m_nShockTimer <= 0)
		{// スペースキーが押された時
		 // 移動方向の更新
			Shot(2);

		}
	}

#ifdef _DEBUG
	if (pInput->Trigger(DIK_E))
	{// スペースキーが押された時
	 // 移動方向の更新
		m_nShockTimer = 0;
		m_nHealTimer = 0;
	}
#endif // _DEBUG

	m_nHealTimer--; m_nShockTimer--;

	// 移動情報の計算
	pMove->Moving(move);

	// 移動情報の取得
	D3DXVECTOR3 moveing = CEnemy::GetMove()->GetMove();
}

//=============================================================================
// 敵状態の処理
// Author : 有田明玄
//=============================================================================
void CEnemyDrone::EnemyMode()
{
	if (m_pViewField)
	{//視野のnullチェック	
		CPlayer* checkPlayer = CHacker::GetPlayer();		//プレイヤーの取得
		if (checkPlayer)
		{//ハッカーでは接続されてないと動かなくなるぞ！	
			CEnemy::SetPos(CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].Pos);
		}
		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDiscovery)
		{
			m_bSeen = true;
			m_state = STATE_ONLINEATTACK;
		}
		else
		{
			m_bSeen = false;
		}
		CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

		if (pPlayer && !pPlayer->GetHiddenState())
		{//プレイヤーが見える状態だったら

		 //プレイヤーが見えるかどうかを確認する
			D3DXVECTOR3 pos = pPlayer->GetPos();

			if (m_pViewField->IsPontInView(pos) ||
				m_pViewField->IsPontInView(pos + D3DXVECTOR3(0.0f, 25.0f, 0.0f)) ||
				m_pViewField->IsPontInView(pos + D3DXVECTOR3(0.0f, 50.0f, 0.0f)))
			{
				m_bSeen = true;
				CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(true, GetId());
				m_state = STATE_ATTACK;
			}
			else
			{
				CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
				m_bSeen = false;
			}
		}
	}

	//UpdateState();					//現在の状態によっての更新処理

	CEnemy::Update();				//親クラスの更新処理

	if (GetDeath())
	{//死亡フラグがtrueだったら

		Kill();			//死ぬように設定する

		return;
	}

	CApplication::GetInstance()->GetSceneMode()->SetEnemyPos(CEnemy::GetPos(), GetId());

#ifdef _DEBUG
	//デバッグ用の処理（殺す）
	CInput *pInput = CInput::GetKey();
	if (pInput->Trigger(DIK_5))
	{
		// これ通った時めっちゃメッシュが暴走します
		Kill();
	}
#endif // _DEBUG
}

//=============================================================================
// 射撃
// Author : 有田明玄
// 概要 : ドローンから弾を飛ばす
//=============================================================================
void CEnemyDrone::Shot(int nType)
{
	// カメラ情報の取得
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	CBullet* pBul = CBullet::Create();				//生成
	pBul->SetPos(CEnemy::GetPos());							//位置設定

	D3DXVECTOR3 dir = pCamera->GetPosR() - pCamera->GetPosV();
	D3DXVec3Normalize(&dir, &dir);
	pBul->SetMoveRot(dir);
	pBul->SetParent((CEnemy*)this);
	pBul->SetObjType(CObject::OBJTYPE_PLAYER_BULLET);

	//当たり判定設定
	CCollision_Rectangle3D* pCollision = pBul->GetCollision();
	D3DXVECTOR3 modelDoorGimmick = pBul->GetModel()->GetMyMaterial().size;
	pCollision = pBul->GetCollision();
	pCollision->SetSize(modelDoorGimmick);
	pCollision->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y / 2.0f, 0.0f));
}