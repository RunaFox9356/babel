//=============================================================================
//
// wall.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "wall.h"
#include "model3D.h"
#include "collision_rectangle3D.h"



//壁のモデルインデックス
const int CWall::WALL_MODEL_IDX[WALL_TYPE_MAX] =
{
	18,
	32
};




//コンストラクタ
CWall::CWall() : m_nWallType(0)
{

}

//デストラクタ
CWall::~CWall()
{

}

//初期化
HRESULT CWall::Init()
{
	CModelObj::Init();

	SetType(WALL_MODEL_IDX[m_nWallType]);

	return S_OK;
}

//終了処理
void CWall::Uninit(void)
{
	CModelObj::Uninit();
}

//更新処理
void CWall::Update(void)
{
	CModelObj::Update();
}

//描画処理
void CWall::Draw(void)
{
	CModelObj::Draw();
}

//種類の設定
void CWall::SetWallType(const EWallType type)
{
	if (type >= WALL_TYPE_MAX || type < 0)
		return;

	m_nWallType = type;

	//当たり判定のサイズと相対位置の設定=================================
	CCollision_Rectangle3D* pCollision = GetCollision();
	CModel3D* pModel = GetModel();
	D3DXVECTOR3 size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (pCollision && pModel)
	{
		size = GetModel()->GetMyMaterial().size;
		pCollision->SetSize(size);
		pCollision->SetPos(D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f));
	}
	//===================================================================
}






//生成処理
CWall* CWall::Create(const D3DXVECTOR3 pos, const EWallType type, const int nDir)
{
	if (type >= WALL_TYPE_MAX || type < 0)
		return nullptr;

	CWall* pWall = new CWall;			//インスタンスを生成する

	if (FAILED(pWall->Init()))
	{//初期化処理
		return nullptr;
	}

	pWall->SetPos(pos);			//位置の設定
	pWall->SetWallType(type);	//壁の種類の設定
	pWall->SetDir(nDir);		//向きの設定

	return pWall;				//生成したインスタンスを返す
}








//向きの設定
void CWall::SetDir(const int nDir)
{
	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (nDir == 1)
		rot.y = D3DX_PI * 0.5f;
	else if (nDir == 2)
		rot.y = D3DX_PI;
	else if (nDir == 3)
		rot.y = D3DX_PI * 1.5f;

	SetRot(rot);

	RotateHitbox(nDir);			//当たり判定の向きの設定
}

//当たり判定の向きの設定
void CWall::RotateHitbox(const int nDir)
{
	if (nDir % 2 == 0)
		return;

	//当たり判定のサイズと相対位置の設定=================================
	CCollision_Rectangle3D* pCollision = GetCollision();
	CModel3D* pModel = GetModel();
	D3DXVECTOR3 size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (pCollision && pModel)
	{
		size = GetModel()->GetMyMaterial().size;
		pCollision->SetSize(D3DXVECTOR3(size.z, size.y, size.x));
		pCollision->SetPos(D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f));
	}
	//===================================================================
}

