//=============================================================================
//
// 3Dモデルクラス(model3D.h)
// Author : 浜田琉雅
// 概要 : 3Dモデル生成を行う
//
//=============================================================================
#ifndef _MODEL_SKIN_H_			// このマクロ定義がされてなかったら
#define _MODEL_SKIN_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "object.h"
#include "main.h"
#include "manager.h"
#include "model_obj.h"
#include "shader_manager.h"

#define SKIN_ANIME_SPEED 60.0f / 4800.0f

#define SAFE_RELEASE(p) if (p) { (p)->Release(); (p) = nullptr; }
#define SAFE_DELETE(p) if (p) { delete (p); (p) = nullptr; }
#define SAFE_DELETE_ARRAY(p) if (p) { delete[] (p); (p) = nullptr; }

//派生フレーム構造体 (それぞれのメッシュ用の最終ワールド行列を追加する）
struct MYFRAME : public D3DXFRAME
{
	D3DXMATRIX CombinedTransformationMatrix;
	// オフセット行列(インデックス付描画用)
	D3DXMATRIX OffsetMat;
	// 行列テーブルのインデックス番号(インデックス付用)
	DWORD OffsetID;
};
//派生メッシュコンテナー構造体(
//コンテナーがテクスチャを複数持てるようにポインターのポインターを追加する）
struct MYMESHCONTAINER : public D3DXMESHCONTAINER
{
	LPDIRECT3DTEXTURE9*  ppTextures;
	DWORD dwWeight; //重みの個数（重みとは頂点への影響。）
	DWORD dwBoneNum; //ボーンの数
	LPD3DXBUFFER pBoneBuffer; //ボーン・テーブル
	D3DXMATRIX** ppBoneMatrix; //全てのボーンのワールド行列の先頭ポインター
	D3DXMATRIX* pBoneOffsetMatrices; //フレームとしてのボーンのワールド行列のポインター
	LPD3DXMESH pOriMesh; //オリジナルメッシュ用

	// 影響するフレームへの参照配列。描画時にこのフレームの行列を使う。
	MYMESHCONTAINER() {
		ppBoneMatrix = NULL;
		pBoneOffsetMatrices = NULL;
	}
};


//Xファイル内のアニメーション階層を読み下してくれるクラスを派生させる。
//ID3DXAllocateHierarchyは派生すること想定して設計されている。
class MY_HIERARCHY : public ID3DXAllocateHierarchy
{
public:
	MY_HIERARCHY() {}
	STDMETHOD(CreateFrame)(THIS_ LPCSTR, LPD3DXFRAME *);
	STDMETHOD(CreateMeshContainer)(THIS_ LPCTSTR, CONST D3DXMESHDATA*, CONST D3DXMATERIAL*,
		CONST D3DXEFFECTINSTANCE*, DWORD, CONST DWORD *, LPD3DXSKININFO, LPD3DXMESHCONTAINER *);
	STDMETHOD(DestroyFrame)(THIS_ LPD3DXFRAME);
	STDMETHOD(DestroyMeshContainer)(THIS_ LPD3DXMESHCONTAINER);
private:
};

//=============================================================================
// 3Dモデルクラス
// Author : 浜田琉雅
// 概要 : 3Dモデル生成を行うクラス
//=============================================================================
// スキンメッシュクラス
class CSkinMesh :public CManager
{
public:
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CSkinMesh *Create(std::string Name);										// 3Dモデルの生成

	//メッシュクラスの初期化
	// void InitBase(SMESH_DATA_FILE* _pSMeshData);
	//メッシュの現在のMatrixデータ取得
	D3DXMATRIX GetMatrix();
	CSkinMesh();
	~CSkinMesh() {
	
	}
	HRESULT Init() override { return E_NOTIMPL; }
	void Uninit() override { Release();CSuper::Release(); }
	void Update()override;
	void Draw()override;
	//スキンメッシュ内部処理
	HRESULT Load(std::string pMeshPath);
	HRESULT AllocateBoneMatrix(LPD3DXFRAME pFrameRoot, LPD3DXMESHCONTAINER pMeshContainerBase);
	HRESULT AllocateAllBoneMatrices(LPD3DXFRAME pFrameRoot, LPD3DXFRAME pFrameBase);
	void RenderMeshContainer(MYMESHCONTAINER*, MYFRAME*);
	void UpdateFrameMatrices(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix);
	void DrawFrame(LPD3DXFRAME);
	//フレーム解放
	void FreeAnim(LPD3DXFRAME pFrame);
	//解放処理
	void Release();

	//描画処理

	//オブジェクトのアニメーション変更( メッシュオブジェクトの操作用番号, 変更するアニメーション番号 )
	void ChangeAnim(DWORD NewAnimNum);
	//現在のアニメーション番号取得
	DWORD GetAnimTrack() { return m_CurrentTrack; }
	//現在のアニメーションタイム(アニメーション開始からの時間)を取得
	DWORD GetAnimTime() { return m_AnimeTime; }
	//アニメーション速度を取得
	float GetAnimSpeed() { return m_AnimSpeed; }
	//アニメーション速度を設定
	void SetAnimSpeed(float _AnimSpeed) { m_AnimSpeed = _AnimSpeed; }
	// モーションの停止
	void SetStopFlag(bool bStop) { m_bStopMotion = bStop; }

	void SetPos(D3DXVECTOR3 pos) { m_pos = pos; };								//プレイヤーの体の相対位置
	void SetRot(D3DXVECTOR3 rot) { m_rot = rot; };								//プレイヤーの体の相対位置
	D3DXVECTOR3 GetRot() {return m_rot; };
private:
	//対象のボーンを検索
	MYFRAME* SearchBoneFrame(std::string  _BoneName, D3DXFRAME* _pFrame);
	// モーションブレンド
	void BlendAnimations(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix);
	
	void DrawMaterial(MYMESHCONTAINER *pMeshContainer);
public:
	//ボーンのマトリックス取得( ボーンの名前 )
	D3DXMATRIX GetBoneMatrix(std::string  _BoneName);
	//ボーンのマトリックスポインタ取得( ボーンの名前 )
	D3DXMATRIX* GetpBoneMatrix(std::string  _BoneName);
private:
	//追加
	//すべてのフレームポインタ格納処理関数
	void CreateFrameArray(LPD3DXFRAME _pFrame);
	// フレーム参照用配列
	std::vector<MYFRAME*> m_FrameArray; // 全フレーム参照配列
	std::vector<MYFRAME*> m_IntoMeshFrameArray;// メッシュコンテナありのフレーム参照配列
											   //ボーン情報
	LPD3DXFRAME m_pFrameRoot;
	//アニメーションコントローラ
	LPD3DXANIMATIONCONTROLLER m_pAnimController;
	//ヒエラルキークラス変数
	MY_HIERARCHY m_cHierarchy;
	//アニメーションデータ格納用変数(ここは可変に変更したほうがいい)
	LPD3DXANIMATIONSET m_pAnimSet[20];
	//現在のアニメーションが開始されてからの時間(1/60秒)
	DWORD m_AnimeTime;
	//アニメーションスピード
	float m_AnimSpeed;
	//現在のアニメーショントラック
	DWORD m_CurrentTrack;
	//現在のアニメーションデータトラック	
	D3DXTRACK_DESC m_CurrentTrackDesc;	//現在のモーショントラックの中に重さスピードなどがある

	//　D3DXPRIORITY_TYPE Priority;: トラックの優先度を示す列挙型 D3XPRIORITY_TYPE の変数です。トラックの優先度は、アニメーションが複数のトラックで構成されている場合に、どのトラックが他のトラックよりも優先的に実行されるかを指定します。
	//	FLOAT Weight; : トラックのウェイト（重み）を示す変数です。ウェイトは、複数のアニメーショントラックが混合される際に、それぞれのトラックがどれだけ影響を与えるかを制御するために使用されます。
	//	FLOAT Speed; : アニメーショントラックの再生速度を示す変数です。この値が1.0であれば通常の速度で再生されます。値が大きいほど速く、小さいほど遅く再生されます。
	//	DOUBLE Position; : トラックの現在の再生位置を示す変数です。アニメーションの途中でトラックを一時停止し、再生位置を変更するために使用されます。
	//	BOOL Enable; : トラックが有効かどうかを示すブール変数です。有効であればトラックが再生され、無効であれば無視されます。

	//メッシュのマテリアル関係
	//マテリアル変更フラグ
	BOOL m_MaterialFlg;
	//マテリアルデータ
	D3DMATERIAL9 m_Material;

	D3DXMATRIX					m_mtxWorld;				// ワールドマトリックス
	D3DXVECTOR3					m_pos;					// 位置
	D3DXVECTOR3					m_posOld;				// 過去位置
	D3DXVECTOR3					m_rot;					// 向き

	enum ERenderMode
	{
		Render_Default,
		Render_Highlight,
		Render_Hologram,
		Render_MAX
	};

	//=========================================
	//ハンドル一覧
	//=========================================
	struct Handler
	{
		IDirect3DTexture9*	tex0;	// テクスチャ保存用
		D3DXHANDLE			WVP;
		D3DXHANDLE			vLightDir;	// ライトの方向
		D3DXHANDLE			vDiffuse;	// 頂点色
		D3DXHANDLE			vAmbient;	// 頂点色
		D3DXHANDLE			vCameraPos;
		D3DXHANDLE			Technique;	// テクニック
		D3DXHANDLE			Texture;		// テクスチャ
		D3DXHANDLE time;
		D3DXHANDLE numBlendMatrix;
		D3DXHANDLE isUseTexture;
		D3DXHANDLE boneMatrix;
		D3DXHANDLE boneOffsetMatrix;
		D3DXHANDLE boneIndex;
	};

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void Shadow();		// 影の描画
	void RenderDefault(MYMESHCONTAINER *pMeshContainer,LPD3DXEFFECT& effect, UINT pass = 0);
	void RenderHighLight(MYMESHCONTAINER *pMeshContainer,LPD3DXEFFECT& effect);

	ERenderMode m_renderMode;
	int		m_nRenderTime;
	Handler m_handler;

	CShaderManager::WVP m_wvp;
	bool m_bStopMotion;
};

#endif