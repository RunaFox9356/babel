//=============================================================================
//
// taskUI.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _TASK_UI_H
#define _TASK_UI_H


//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"
#include "task.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CText;
class CWords;


class CTaskUI : public CPolygon2D
{
public:

	CTaskUI();						//コンストラクタ
	~CTaskUI() override;			//デストラクタ

	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新
	void Draw() override;							// 描画

	void UpdateTask(CTask::AGENT_TASK_TYPE type);	//エージェントのタスクのテキストの更新
	void UpdateTask(CTask::HACKER_TASK_TYPE type);	//ハッカーのタスクのテキストの更新
	void CreateText(CTask* pTask);					//テキストの生成

	static CTaskUI* Create();						//生成

private:

	void CreateAgentUI(CTask* pTask);				//エージェントのUIの生成
	void CreateHackerUI(CTask* pTask);				//ハッカーのUIの生成

private:

	static const int		 TEXT_MAX_CHARACTER = 16;				//テキストの最大文字数
	static const char		 DEFAULT_AGENT_TEXT[CTask::AGENT_TASK_MAX][TEXT_MAX_CHARACTER];		//ディフォルトのテキスト
	static const char		 DEFAULT_HACKER_TEXT[CTask::HACKER_TASK_MAX][TEXT_MAX_CHARACTER];	//ディフォルトのテキスト

	static const D3DXVECTOR3 DEFAULT_POS;			//ディフォルトの位置
	static const D3DXVECTOR3 DEFAULT_SIZE;			//ディフォルトのサイズ
	static const D3DXCOLOR	 DEFAULT_COLOR;			//ディフォルトの色
	static const int		 DEFAULT_TEXTURE;		//ディフォルトのテクスチャ

	static const D3DXCOLOR	 DEFAULT_TEXT_COLOR;	//ディフォルトテキストの色
	static const D3DXVECTOR3 DEFAULT_LETTER_SIZE;	//ディフォルト文字のサイズ
	static const D3DXVECTOR3 DEFAULT_TEXT_POS;		//ディフォルト文字の相対位置
	static const D3DXVECTOR3 DEFAULT_NUMBER_POS;	//ディフォルト数値の相対位置


	D3DXVECTOR3		m_textPos;						//テキストの相対位置
	D3DXCOLOR		m_textCol;						//テキストの色
	bool			m_bText;						//テキストを更新できるかどうか

	CText*			m_pText[CTask::AGENT_TASK_MAX];			//テキストへのポインタ
	CText*			m_pNum[CTask::AGENT_TASK_MAX];			//テキストへのポインタ

	CText*			m_pHackerText[CTask::HACKER_TASK_MAX];	//テキストへのポインタ
	CText*			m_pHackerNum[CTask::HACKER_TASK_MAX];	//テキストへのポインタ

};


#endif