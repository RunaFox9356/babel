//=============================================================================
//
// selectScreen.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SELECT_SCREEN_H_		// このマクロ定義がされてなかったら
#define _SELECT_SCREEN_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "scene_mode.h"
#include "application.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CMenu;
class CText;

class CSelectScreen : public CSceneMode
{
public:

	enum BUTTON
	{
		BUTTON_GAME = 0,
		BUTTON_HACKER,
		BUTTON_TITLE,
		
		BUTTON_MAX
	};

public:
	CSelectScreen();				//コンストラクタ
	~CSelectScreen() override;		//デストラクタ


	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新

private:
	void SetTransition(CApplication::SCENE_MODE mode);	//遷移の設定

	bool m_bTransition;				//遷移中であるかどうか
	bool m_isCommand;
	CApplication::SCENE_MODE m_ConnectMode;
	CApplication::SConnectCheck m_Connect;
	CText*selectText;
	CMenu* m_pMenu;					//メニュー
};


#endif