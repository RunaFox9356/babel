//=============================================================================
//
// sleepingEnemy.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "sleepingEnemy.h"
#include "viewField.h"
#include "application.h"
#include "scene_mode.h"
#include "collision_rectangle3D.h"
#include "agent.h"
#include "game.h"
#include "hacker.h"
#include "move.h"
#include "motion.h"
#include "soundSource.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3		CSleepingEnemy::EYE_RELATIVE_POS = D3DXVECTOR3(0.0f, 42.0f, 0.0f);				//目の相対位置
const float				CSleepingEnemy::DEFAULT_HEARING_RADIUS = 300.0f;								//ディフォルトのプレイヤー聞こえる範囲の半径
const D3DXVECTOR3		CSleepingEnemy::DEFAULT_COLLISION_POS = D3DXVECTOR3(0.0f, 70.0f, 0.0f);			//ディフォルトの当たり判定の相対位置
const D3DXVECTOR3		CSleepingEnemy::DEFAULT_COLLISION_SIZE = D3DXVECTOR3(40.0f, 140.0f, 40.0f);		//ディフォルトの当たり判定のサイズ
const int				CSleepingEnemy::DEFAULT_HAND_INDEX = 3;											//ディフォルトの手のモデルのインデックス
const int				CSleepingEnemy::DEFAULT_WAKE_FRAMES = 60;										//起きる前のフレーム数
const float				CSleepingEnemy::DEFAULT_ROTATION_FRICTION = 0.02f;								//ディフォルトの回転の摩擦係数
const float				CSleepingEnemy::ATTACK_ROTATION_FRICTION = 0.1f;								//攻撃の時の回転の摩擦係数
const int				CSleepingEnemy::DEFAULT_WAIT_FRAMES = 150;										//ディフォルトの待つフレーム数
const float				CSleepingEnemy::LOOK_AROUND_ANGLE = D3DX_PI * 0.25f;							//周りを見る時のディフォルトの最大角度




//コンストラクタ
CSleepingEnemy::CSleepingEnemy() : m_startingPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_targetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nCntAnim(0),
m_nCntPhase(0),
m_fLookAroundAngle(0.0f),
m_fOriginalRot(0.0f),
m_fHearRadius(0.0f),
m_fAnimRot(0.0f),
m_bSeen(false),
m_bChased(false),
m_state((EState)0),
m_pView(nullptr)
{

}

//デストラクタ
CSleepingEnemy::~CSleepingEnemy()
{

}

// 初期化
HRESULT CSleepingEnemy::Init()
{
	//親クラスの初期化処理
	CEnemy::Init();

	CApplication::GetInstance()->GetSceneMode()->SetEnemyUse(true, GetId());

	SetMotion("data/MOTION/motion001.txt");		//アニメーションの設定

	m_pView = CViewField::Create(this);							//視野の生成

	if (m_pView)
	{//nullチェック

		m_pView->SetPosV(EYE_RELATIVE_POS);						//視野の相対位置の設定
		m_pView->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));		//視野の相対回転の設定
	}

	//移動用の情報の設定
	CMove* pMove = GetMove();

	if (pMove)
	{
		pMove->SetMoving(0.2f, 1000.0f, 0.1f, 0.1f);
	}

	m_state = STATE_SLEEP;										//現在の状態の設定

	CCollision_Rectangle3D* pCollision = GetCollision();		//当たり判定の取得
	CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());

	if (pCollision)
	{//当たり判定のサイズと相対位置の設定

		GetCollision()->SetSize(DEFAULT_COLLISION_SIZE);
		GetCollision()->SetPos(DEFAULT_COLLISION_POS);
	}

	m_fHearRadius = DEFAULT_HEARING_RADIUS;						//プレイヤーが聞こえる範囲の半径の設定
	SetRotFriction(DEFAULT_ROTATION_FRICTION);					//回転の摩擦係数の設定
	SetActionState(CEnemy::ACTION_STATE::NEUTRAL_ACTION);

	{
		m_pSoundSource = CSoundSource::Create(this, CSound::SOUND_LABEL_SE_LIGHT_STEP, 45, 1.0f, 2000.0f);		//制作中
	}

	return S_OK;
}

// 終了
void CSleepingEnemy::Uninit()
{
	//視野の破棄
	if (m_pView)
	{
		m_pView->Uninit();
		m_pView = nullptr;
	}

	if (m_pSoundSource)
	{
		m_pSoundSource->Uninit();
		m_pSoundSource = nullptr;
	}

	//親クラスの終了処理
	CEnemy::Uninit();
}

// 更新
void CSleepingEnemy::Update()
{
	if (CApplication::GetInstance()->GetSceneMode()->GetEnd())
		return;

	if (!GetDeath() && !GetStun())
	{
		CreateGun(DEFAULT_HAND_INDEX);							//銃の生成処理

		if (m_pView && (m_state != STATE_SLEEP && m_state != STATE_WAKE))
		{//視野のnullチェック	
			CPlayer* checkPlayer = CHacker::GetPlayer();		//プレイヤーの取得
			if (checkPlayer && CApplication::GetInstance()->GetEnemyConnect())
			{//ハッカーでは接続されてないと動かなくなるぞ！	
				//SetPos(CApplication::GetInstance()->GetPlayer(1)->GetCommu()->Player.m_isPopEnemy[GetId()].Pos);
			}
			if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDiscovery)
			{
				m_bSeen = true;
				m_state = STATE_ONLINEATTACK;
				SetRotFriction(ATTACK_ROTATION_FRICTION);					//攻撃回転の摩擦係数の設定
				SetActionState(CEnemy::ACTION_STATE::AIM_ACTION);

				if (m_pSoundSource)
				{
					m_pSoundSource->SetUpdatingFlag(false);
					m_pSoundSource->SetDelay(45);
				}
			}
			else
			{
				SetRotFriction(DEFAULT_ROTATION_FRICTION);					//回転の摩擦係数の設定
				m_bSeen = false;
			}
			CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

			if (pPlayer && !pPlayer->GetHiddenState())
			{//プレイヤーが見える状態だったら

			 //プレイヤーが見えるかどうかを確認する
				D3DXVECTOR3 pos = pPlayer->GetPos();
				D3DXVECTOR3 dist = pos - GetPos();

				if (m_pView->IsPontInView(pos) ||
					m_pView->IsPontInView(pPlayer->GetPlayerBody()) ||
					m_pView->IsPontInView(pPlayer->GetPlayerHead()) ||
					(!m_bSeen && pPlayer->GetDash() && D3DXVec3Length(&dist) < 500.0f))
				{
					m_bSeen = true;
					CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(true, GetId());
					m_state = STATE_ATTACK;
					SetActionState(CEnemy::ACTION_STATE::AIM_ACTION);
					IncreaseSecurity();
					SetExclamationMarkDraw(true);

					if (m_pSoundSource)
					{
						m_pSoundSource->SetUpdatingFlag(false);
						m_pSoundSource->SetDelay(45);
					}
				}
				else if (m_bSeen)
				{
					CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
					m_bSeen = false;
					SetExclamationMarkDraw(false);
				}
			}
		}

		//状態によっての更新処理
		UpdateState();

		//親クラスの更新処理
		CEnemy::Update();

		if (GetDeath())
		{//死亡フラグがtrueだったら

			Kill();			//死ぬように設定する

			return;
		}
	}
	else
	{
		//親クラスの更新処理
		CEnemy::Update();
	}

	CApplication::GetInstance()->GetSceneMode()->SetEnemyPos(GetPos(), GetId());
}

//目の相対位置の設定
void CSleepingEnemy::SetEyePos(const D3DXVECTOR3 eyePos)
{
	if (m_pView)
	{
		m_pView->SetPosV(eyePos);
	}
}

//セキュリティーレベルのセッター
void CSleepingEnemy::SetSecurityLevel(const int nSecurity)
{
	CEnemy::SetSecurityLevel(nSecurity);

	if (m_pView)
	{
		m_pView->SetViewDistance(CViewField::DEFAULT_VIEW_DISTANCE + (CViewField::DEFAULT_VIEW_DISTANCE * 0.5f * nSecurity));
		m_pView->SetViewAngle(CViewField::DEFAULT_VIEW_ANGLE + (CViewField::DEFAULT_VIEW_ANGLE * 0.5f * nSecurity));
	}
}

//スタン
void CSleepingEnemy::Stun()
{
	CEnemy::Stun();

	if (m_pSoundSource)
		m_pSoundSource->SetUpdatingFlag(false);
}




//=============================================================================
//
//									静的関数
//
//=============================================================================




//生成
CSleepingEnemy * CSleepingEnemy::Create(const D3DXVECTOR3 pos, D3DXVECTOR3 rot, float fHearRadius)
{
	CSleepingEnemy* pEnemy = new CSleepingEnemy;		//インスタンスを生成する

	//初期化処理
	if (FAILED(pEnemy->Init()))
		return nullptr;

	pEnemy->SetPos(pos);							//位置の設定
	pEnemy->SetRot(rot);							//向きの設定
	pEnemy->SetRotDest(rot);						//目的の向きの設定
	pEnemy->m_startingPos = pos;					//スポーンの位置を保存する
	pEnemy->m_fHearRadius = fHearRadius;			//プレイヤーが聞こえる範囲の半径の設定
	pEnemy->m_fOriginalRot = rot.y;					//向きのY座標を保存する

	return pEnemy;									//生成したインスタンスを返す
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================




//状態によって更新
void CSleepingEnemy::UpdateState()
{
	switch (m_state)
	{
	case CSleepingEnemy::STATE_SLEEP:

		//眠る
		Sleep();				

		break;

	case CSleepingEnemy::STATE_WAKE:

		//起きる
		Wake();
		
		break;

	case CSleepingEnemy::STATE_SEARCH:

		//音の方を確認する
		Search();

		break;

	case CSleepingEnemy::STATE_LOOK_AROUND:

		//周りを見る
		LookAround();
		
		break;

	case CSleepingEnemy::STATE_ATTACK:

		//攻撃
		Attack();

		break;

	case CSleepingEnemy::STATE_ONLINEATTACK:

		//オンライン攻撃
		Attack(true);

		break;

	case CSleepingEnemy::STATE_CHASE:

		//プレイヤーを追いかける
		Chase();

		break;

	case CSleepingEnemy::STATE_RETURN:

		//スポーンの位置に戻る
		Return();

		break;

	case CSleepingEnemy::STATE_HEARD_SOUND:

		//音を聞いた
		HeardSound();

		break;

	default:
		break;
	}
}

//眠る
void CSleepingEnemy::Sleep()
{
	//プレイヤーの取得
	CAgent* pAgent = CGame::GetPlayer();

	//nullチェック
	if (!pAgent)
		return;

	D3DXVECTOR3 playerPos = pAgent->GetPos();		//プレイヤーの位置の取得
	D3DXVECTOR3 pos = GetPos();						//位置の取得
	D3DXVECTOR3 dist = playerPos - pos;				//このエネミーからプレイヤーまでのベクトルを計算する
	float fDist = D3DXVec3Length(&dist);			//上計算したベクトルの長さを計算する

	if (fDist < m_fHearRadius || GetSecurityLevel() > 0)
	{//プレイヤーまでの距離が聞こえる範囲の半径より小さかったら

		m_state = STATE_WAKE;			//起きる状態にする
	}
	else if (m_bHeardSound)
	{
		m_state = STATE_HEARD_SOUND;	//音を聞いた状態にする
	}
}

//起きる
void CSleepingEnemy::Wake()
{
	m_nCntAnim++;

	if (m_nCntAnim >= DEFAULT_WAKE_FRAMES)
	{
		m_nCntAnim = 0;					//カウンターを0に戻す

		if (GetSecurityLevel() <= 0)
			m_state = STATE_SEARCH;			//音の方を確認する状態にする
		else
			m_state = STATE_LOOK_AROUND;	//周りを見る状態にする

		//プレイヤーの取得
		CAgent* pPlayer = CGame::GetPlayer();

		//nullチェック
		if (!pPlayer)
			return;

		//音が聞こえた位置を保存する
		m_targetPos = pPlayer->GetPos();
	}
}

//音の方を確認する
void CSleepingEnemy::Search()
{
	//プレイヤーの取得
	CAgent* pPlayer = CGame::GetPlayer();

	//nullチェック
	if (!pPlayer)
		return;

	D3DXVECTOR3 playerPos = pPlayer->GetPos();		//プレイヤーの位置の取得
	D3DXVECTOR3 pos = GetPos();						//位置の取得
	D3DXVECTOR3 dist = playerPos - pos;				//このエネミーからプレイヤーまでのベクトルを計算する
	float fDist = D3DXVec3Length(&dist);			//上計算したベクトルの長さを計算する

	if (fDist < m_fHearRadius)
	{//見えないが、聞こえる範囲の半径ないだったら
		m_targetPos = playerPos;		//ターゲットとして現在のプレイヤーの位置を保存する
		m_nCntAnim = 0;					//カウンターを0に戻す
	}
	else
	{
		m_nCntAnim++;					//カウンターをインクリメントする
	}

	//このエネミーからプレイヤーまでの単位ベクトルを計算する
	D3DXVECTOR3 dir = m_targetPos - GetPos();
	D3DXVec3Normalize(&dir, &dir);

	// 向きの取得
	D3DXVECTOR3 rot = GetRot(), rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
	float fDot = D3DXVec3Dot(&dir, &unit);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = -acosf(fDot);		//目的の回転角度を計算する

	//角度を正規化する
	if (GetPos().x > m_targetPos.x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);			//目的の回転角度の設定

	if (m_nCntAnim >= 120)
	{//何も聞こえなくなって、2秒が経ったら

		m_nCntAnim = 0;							//カウンターを0に戻す
		m_state = STATE_LOOK_AROUND;			//周りを見る状態にする
		m_fAnimRot = GetRot().y;				//向きのY座標を保存する
	}
}

//周りを見る
void CSleepingEnemy::LookAround()
{
	//目的の角度を取得する
	D3DXVECTOR3 rotDest = GetRotDest();

	switch (m_nCntPhase)
	{
	case 0:

	{
		//新しい目的の角度を計算する
		rotDest.y = m_fAnimRot + LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);	//回転角度の設定
		m_nCntAnim++;			//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{

			m_nCntPhase++;		//次へ進む
			m_nCntAnim = 0;		//フレーム数を0に戻す
		}
	}

	break;

	case 1:

	{
		m_nCntAnim++;			//アニメーションカウンターをインクリメントする

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES / (1 + (1 * GetSecurityLevel())))
		{//決まった時間が経ったら

			m_nCntAnim = 0;		//カウンターを0に戻す
			m_nCntPhase++;		//次へ進む
		}
	}

	break;

	case 2:

	{
		//新しい目的の角度を計算する
		rotDest.y = m_fAnimRot - LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);		//回転角度の設定
		m_nCntAnim++;				//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{
			m_nCntPhase++;			//次へ進む
			m_nCntAnim = 0;			//フレーム数を0に戻す
		}
	}

	break;

	case 3:

	{
		m_nCntAnim++;				//アニメーションカウンターをインクリメントする

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES / (1 + (1 * GetSecurityLevel())))
		{//決まった時間が経ったら

			m_nCntPhase = 0;		//カウンターを0に戻す
			m_nCntAnim = 0;			//カウンターを0に戻す

			if (GetSecurityLevel() <= 0)
			{
				m_state = STATE_RETURN;	//次へ進む
				SetActionState(CEnemy::ACTION_STATE::MOVE_ACTION);

				if (m_pSoundSource)
				{
					m_pSoundSource->SetUpdatingFlag(true);
				}
			}
		}
	}

	break;

	default:
		break;
	}
}

//弾を撃つ
void CSleepingEnemy::Attack(const bool isOnline)
{
	//プレイヤーのほうに向かう
	LookAtPlayer(isOnline);

	if (Shoot() && !m_bSeen)
	{//攻撃が終わった時、プレイヤーが見えない場合、プレイヤーを追いかける状態にする

		if (!m_bChased)
		{
			m_state = STATE_CHASE;
			m_bChased = true;
			SetActionState(CEnemy::ACTION_STATE::DASH_ACTION);	

			if (m_pSoundSource)
			{
				m_pSoundSource->SetUpdatingFlag(true);
				m_pSoundSource->SetDelay(25);
			}
		}
		else
		{
			m_state = STATE_RETURN;
			SetActionState(CEnemy::ACTION_STATE::MOVE_ACTION);

			if (m_pSoundSource)
			{
				m_pSoundSource->SetUpdatingFlag(true);
				m_pSoundSource->SetDelay(45);
			}
		}
	}
}

//プレイヤーのほうに向かう
void CSleepingEnemy::LookAtPlayer(bool isOnline)
{
	CGun* pGun = GetGun();					//銃の取得
	CPlayer* pPlayer = nullptr;//プレイヤーの取得
	if (isOnline)
	{
		pPlayer = CHacker::GetPlayer();	//プレイヤーの取得
	}
	else
	{
		pPlayer = CGame::GetPlayer();	//プレイヤーの取得
	}

	if (!pGun || !pPlayer)
	{//銃がない又は、プレイヤーがいない場合

		m_state = STATE_LOOK_AROUND;	//周りを見る状態にする	
		return;
	}

	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - pPlayer->GetPos();
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > pPlayer->GetPos().x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	m_targetPos = pPlayer->GetPos();	//目的の位置としてプレイヤーの位置を保存する
	m_fAnimRot = rotDest.y;				//アニメーション用の角度を保存する
	SetRotDest(rotDest);				//目的の回転角度の設定
}

//移動
bool CSleepingEnemy::Move(const D3DXVECTOR3 targetPos)
{
	CMove* pMove = GetMove();			//移動の状態の取得

	if (pMove)
	{//取得できたら

		D3DXVECTOR3 pos = GetPos(), target = targetPos;		//必要な位置の設定

															//現在の位置と目的の位置の距離を計算する
		float dist = sqrtf(((target.x - pos.x) * (target.x - pos.x)) + ((target.y - pos.y) * (target.y - pos.y)) + ((target.z - pos.z) * (target.z - pos.z)));

		if (dist <= 10.0f)
		{//前計算した距離が目的の位置の半径より小さかったら
			return true;
		}

		//移動の向きを計算する
		D3DXVECTOR3 move = target - GetPos();
		D3DXVec3Normalize(&move, &move);
		pMove->Moving(move);

		//位置の更新
		D3DXVECTOR3 delta = pMove->GetMove();
		pos += delta;
		SetPos(pos);

		// 向きの取得
		D3DXVECTOR3 rot = GetRot(), rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
		float fDot = D3DXVec3Dot(&move, &unit);

		//近似がないようにする
		if (fDot < -1.0f)
			fDot = -1.0f;
		else if (fDot > 1.0f)
			fDot = 1.0f;

		rotDest.y = -acosf(fDot);		//目的の回転角度を計算する

		if (GetPos().x > target.x)
			rotDest.y = -rotDest.y;

		// 目的の向きの補正
		if (rotDest.y - rot.y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - rot.y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);			//目的の回転角度の設定
	}

	return false;
}

//プレイヤーを追いかける
void CSleepingEnemy::Chase()
{
	if (Move(m_targetPos))
	{
		m_state = STATE_LOOK_AROUND;
		SetActionState(CEnemy::ACTION_STATE::NEUTRAL_ACTION);

		if (m_pSoundSource)
		{
			m_pSoundSource->SetUpdatingFlag(false);
			m_pSoundSource->SetDelay(45);
		}
	}
}

//スポーンの位置に戻る
void CSleepingEnemy::Return()
{
	if (m_nCntPhase == 0)
	{//スポーンの位置に戻る
		if (Move(m_startingPos))
		{//位置に着いたら

			m_nCntPhase++;		//次へ進む
		}
	}
	else
	{
		m_nCntAnim++;											//カウンターをインクリメントする
		SetRotDest(D3DXVECTOR3(0.0f, m_fOriginalRot, 0.0f));	//目的の向きの設定

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{//決まった時間が経ったら

			m_nCntAnim = 0;					//カウンターを0に戻す
			m_nCntPhase = 0;				//カウンターを0に戻す

			if (GetSecurityLevel() <= 0)
				m_state = STATE_SLEEP;			//眠る状態にする
			else
				m_state = STATE_LOOK_AROUND;	//周りを見る状態にする

			SetActionState(CEnemy::ACTION_STATE::NEUTRAL_ACTION);
			m_bChased = false;				//フラグをfalseに戻す

			if (m_pSoundSource)
			{
				m_pSoundSource->SetUpdatingFlag(false);
				m_pSoundSource->SetDelay(45);
			}
		}
	}
}

//音を聞いた
void CSleepingEnemy::HeardSound()
{
	m_nCntAnim++;

	if (m_nCntAnim >= DEFAULT_WAKE_FRAMES)
	{
		m_nCntAnim = 0;					//カウンターを0に戻す
		m_state = STATE_SEARCH;			//音の方を確認する状態にする

		//音が聞こえた位置を保存する
		m_targetPos = GetSoundPos();

		SoundPosReset();
	}
}
