//=============================================================================
//
// UIΗNX(HackerUI.h)
// Author : ϊ±l
// Tv : UIΗπs€
//
//=============================================================================
#ifndef _HACKERUI_H_		// ±Μ}Nθ`ͺ³κΔΘ©Α½η
#define _HACKERUI_H_		// ρdCN[hh~Μ}Nθ`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "manager.h"
#include "UImanager.h"
#include "number.h"

//*****************************************************************************
// OϋιΎ
//*****************************************************************************
class CHackingObj;
class CPolygon2D;
class CPolygon3D;
class CTimer;
class CNumber;
class CScore;
//=============================================================================
// }l[W[NX
// Author : ϊ±l
// Tv : }l[W[Ά¬πs€NX
//=============================================================================
class CHackerUI :public CUIManager
{

public:
	//--------------------------------------------------------------------
	// ΓIoΦ
	//--------------------------------------------------------------------
	static CHackerUI *Create();	// UIΜΆ¬

private:
	//--------------------------------------------------------------------
	// θ
	//--------------------------------------------------------------------
	static const int   MAX_ITEM = 3;			// g¦ιACeΜ
	static const int   MAX_CAMERA = 5;			// g¦ιJEh[Μ
	static const float ITEM_NORMAL_SIZE;		// ACeκΜUIΜIΞκΔ’Θ’ΜTCY
	static const float ITEM_WIDTH;				// ACeκΜUIzuΤu
	static const float ITEM_DISTANCE_FROM_EDGE;	// [©ηΜ£
	static const int HEAL_RECAST_TIME;			// ρΜLXgΤ
	static const int NUMBER_0;					// TEXTUREΜΜ0Μκ
	static const float BULLET_TIMER_SIZE;				// ^C}[ΜTCY
	static const D3DXVECTOR3 SCORE_SIZE;		// XRA
	static const D3DXVECTOR3 TIMER_SIZE;		// ^[Qbg


public:
//--------------------------------------------------------------------
// RXgN^ΖfXgN^
//--------------------------------------------------------------------
	CHackerUI();
	~CHackerUI();
	HRESULT Init()override;
	void Uninit()override;
	void Update()override;
	void Draw()override;
	CTimer*GetHealTimer() { return m_pHealTimer; };
	CTimer*GetShockTimer() { return m_pShockTimer; };
	CTimer*GetTimer() { return m_pTimer; };
	CScore* GetScore() { return m_Score; };
	//CPolygon2D* GetHackUI() { return m_pHackUI; };
	void SetCameraNumber(int camera, int num) { m_pCameraNumber[num]->SetNumber(camera); };
	CNumber* GetCameraNumber(int num) { return m_pCameraNumber[num]; };
	CPolygon3D* GetTargetUI() { return m_pTarget; };
	void AllClear();							//UIS§ΎΙ
	void AllClearCancel();						//UIS§Ύπ
	void AddScore(const int nScore);			//XRAΜΑZ
	void SetScore(const int nScore);
	void SetDroneCommandUi(const bool bDraw);	//h[μΜUI`ζtOΜZb^[

private:
	//UIάνθ
	//CPolygon2D* m_pHackUI;						//	nbLOΕ«ι©Η€©ΜUI
	CPolygon2D* m_pDroneCommand[2];				//	h[μΜUI
	CPolygon2D* m_pCameraIcon;					//	gΑΔιJEh[ΜACR
	CNumber* m_pCameraNumber[2];				//	JΤ\¦(1zθ)
	CPolygon2D* m_pCamNumberSlash;				//	JΤ\¦(/)
	CTimer* m_pHealTimer;						//	ρe^C}[(2)
	CTimer* m_pShockTimer;						//	dCVbN^C}[(2)
	CScore* m_Score;							//	XRA
	CTimer* m_pTimer;							//	^C}[
	CPolygon3D* m_pTarget;						//	nbLOΞΫπ¦·UI
	CPolygon2D* m_pMinimap;						//	~j}bv
	CPolygon2D*	m_pChangeCameraUI[2];			//	ΔJΜΨθΦ¦UI
};
#endif