//=============================================================================
//
// モデルオブジェクトクラス(model_obj.cpp)
// Author : 唐�ｱ結斗
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>

#include "gun.h"
#include "itemObj.h"
#include "bullet.h"
#include "debug_proc.h"
#include "collision_rectangle3D.h"
#include "model3D.h"
#include "calculation.h"
#include "application.h"
#include "sound.h"
#include "UImessage.h"
#include "game.h"
#include "agent.h"

//=============================================================================
//								静的変数の初期化
//=============================================================================
const int	CGun::DEFAULT_GUN_MODEL = 56;					//ディフォルトのモデル
const int	CGun::DEFAULT_MAX_BULLET = 30;					//ディフォルトの弾の最大数
const int	CGun::DEFAULT_MAX_INTERVAL = 5;
const int	CGun::DEFAULT_MAX_RELOAD = 300;					//ディフォルトのリロードフレーム数
const D3DXVECTOR3	CGun::DEFAULT_BULLET_SPAWN = { -12.0f, 2.5f, 0.0f };		//ディフォルトの弾のスポーンの相対位置


namespace
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
};


//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : インスタンスを生成する
//=============================================================================
CGun * CGun::Create()
{
	// オブジェクトインスタンス
	CGun *pItemObj = nullptr;

	// メモリの解放
	pItemObj = new CGun;

	// メモリの確保ができなかった
	assert(pItemObj != nullptr);

	// 数値の初期化
	pItemObj->Init();

	// インスタンスを返す
	return pItemObj;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CGun::CGun() : m_bulletMove(D3DXVECTOR3(0.f, 0.f, 0.f)),	// 弾の発射方向	
m_nCntBullet(0),										// 弾数カウント
m_nCntInterval(0),										// 弾発射までのインターバル
m_nCntReload(0),										// リロードカウント
m_nMaxBullet(0),										// 弾数カウントの最大数
m_nMaxInterval(0),										// 弾発射までのインターバルの最大数
m_nMaxReload(0),										// リロードカウントの最大数
m_bReload(false),										// 弾のリロードを行うか否か
m_bShot(false),											// 弾の発射を行うか否か
m_bRapidFire(false),									// 弾の連射を行うか否か
m_bAllReload(false),									// 弾をまとめてリロードするか否か
m_bulletType((CObject::EObjectType)0),					// 弾の
m_bSound(false),
m_pUiMessage(nullptr),
m_bHeld(false)
{
	CObject::SetObjType(OBJETYPE_GUN);
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CGun::~CGun()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CGun::Init()
{
	m_Sin = 0;
	m_pParent = nullptr;
	// モデルオブジェクトの初期化
	CItemObj::Init();

	m_bulletType = CObject::OBJTYPE_ENEMY_BULLET;

	SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	SetType(DEFAULT_GUN_MODEL);
	SetMaxBullet(DEFAULT_MAX_BULLET);
	SetMaxInterval(DEFAULT_MAX_INTERVAL);
	SetMaxReload(DEFAULT_MAX_RELOAD);
	SetRapidFire(true);

	CCollision_Rectangle3D* pCollision = GetCollision();
	D3DXVECTOR3 modelSize = GetModel()->GetMyMaterial().size;
	pCollision->SetSize(modelSize);
	pCollision->SetPos(D3DXVECTOR3(0.0f, modelSize.y * 0.5f, 0.0f));
	pCollision->SetParent(this);

	SetShadowDraw(false);
	
	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CGun::Uninit()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	// モデルオブジェクトの終了
	CItemObj::Uninit();

	// オブジェクトの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CGun::Update()
{
	m_Sin++;
	// モデルオブジェクトの更新
	CItemObj::Update();

	if (CApplication::GetInstance()->GetMode() != CApplication::MODE_AGENT && CApplication::GetInstance()->GetMode() != CApplication::MODE_GAME)
		return;

	// 弾の発射
	Shot();

	// リロード
	Reload(m_bSound);

	// デバック表示
	//CDebugProc::Print("残弾数 : %d\n", m_nCntBullet);

	D3DXVECTOR3 move;

	move.y = cosf((D3DX_PI*2.0f) * 0.01f * (m_Sin + (20)));

	D3DXVECTOR3 Pos = GetPos();
	Pos.y = 20.0f;
	Pos.y += move.y * 5;
	SetPos(D3DXVECTOR3(Pos.x, Pos.y, Pos.z));

	CCollision_Rectangle3D* pCollision = GetCollision();

	if (!m_bHeld && pCollision && pCollision->Collision(CObject::OBJETYPE_PLAYER, false))
	{
		if (!m_pUiMessage && CGame::GetPlayer() && !CGame::GetPlayer()->GetMyItem())
			CreateUiMessage("Pick Up", E_BUTTON_TEXTURE_IDX);
	}
	else if (m_pUiMessage)
	{
		DestroyUiMessage();
	}

}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 描画を行う
//=============================================================================
void CGun::Draw()
{
	// モデルオブジェクトの描画
	CItemObj::Draw();
}

//=============================================================================
// リロードフラグの設定
// Author : 唐�ｱ結斗
// 概要 : リロードフラグの設定を行う
//=============================================================================
void CGun::SetReload(bool bReload)
{
	m_bReloadSound = false;
	m_bReload = bReload;
	m_bShot = false;
	int nCntReload = 0;

	if (m_bAllReload)
	{
		nCntReload = m_nMaxReload;
	}
	else
	{
		nCntReload = m_nMaxReload / m_nMaxBullet;
	}

	m_nCntReload = nCntReload;
}

//=============================================================================
// リロードフラグの設定
// Author : 唐�ｱ結斗
// 概要 : リロードフラグの設定を行う
//=============================================================================
void CGun::SetShot(bool bShot)
{
	if (m_nCntBullet > 0)
	{
		if (bShot)
		{
			SetReload(false);
		}

		m_bShot = bShot;
	}
	else
	{// 弾の発射音
		CSound *pSound = CApplication::GetInstance()->GetSound();
		pSound->PlaySound(CSound::SOUND_LABEL_SE_BULLET_OUT000);
	}
}

//弾の種類の設定
void CGun::SetBulletType(CObject::EObjectType type)
{
	if (type != CObject::OBJTYPE_PLAYER_BULLET)
		type = CObject::OBJTYPE_ENEMY_BULLET;

	m_bulletType = type;
}

//弾のスポーンの位置の取得
const D3DXVECTOR3 CGun::GetBulletSpawn()
{
	// 弾の発射を行う
	D3DXMATRIX mtxWorld = GetMtxWorld(), mtxOut;
	D3DXVECTOR3 posBullet = D3DXVECTOR3(0.0f, 0.0f, 0.0f), p = GetPos();

	D3DXMatrixIdentity(&mtxOut);
	D3DXMatrixTranslation(&mtxOut, DEFAULT_BULLET_SPAWN.x, DEFAULT_BULLET_SPAWN.y, DEFAULT_BULLET_SPAWN.z);
	D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxWorld);

	D3DXVec3TransformCoord(&posBullet, &DEFAULT_BULLET_SPAWN, &mtxOut);

	return posBullet;
}

//=============================================================================
// 弾の発射
// Author : 唐�ｱ結斗
// 概要 : 弾の発射を行う
//=============================================================================
void CGun::Shot()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	m_nCntInterval--;

	if (m_nCntInterval <= 0
		&& m_nCntBullet > 0
		&& m_bShot
		&& !m_bReload)
	{
		// 弾の発射を行う
		D3DXMATRIX mtxWorld = GetMtxWorld(), mtxOut;
		D3DXVECTOR3 posBullet = D3DXVECTOR3(0.0f, 0.0f, 0.0f), p = GetPos();

		D3DXMatrixIdentity(&mtxOut);
		D3DXMatrixTranslation(&mtxOut, DEFAULT_BULLET_SPAWN.x, DEFAULT_BULLET_SPAWN.y, DEFAULT_BULLET_SPAWN.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxWorld);

		D3DXVec3TransformCoord(&posBullet, &DEFAULT_BULLET_SPAWN, &mtxOut);

		CBullet *pBullet = CBullet::Create();
		pBullet->SetMoveRot(m_bulletMove);
		pBullet->SetPos(posBullet);
		pBullet->SetParent(m_pParent);
		pBullet->SetObjType(m_bulletType);

		// 弾の発射音
		pSound->PlaySound(CSound::SOUND_LABEL_SE_SHOT000);

		{//弾の向きを計算し、設定する
			D3DXVECTOR3 move = m_bulletMove, rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			rotDest.y = atan2f(move.x, move.z) - D3DX_PI;
			rotDest.x = atan2f(move.y, sqrtf((move.x * move.x) + (move.z * move.z)));
			rotDest.z = 0.0f;

			// 向きの正規化
			rotDest.x = CCalculation::RotNormalization(rotDest.x);
			rotDest.y = CCalculation::RotNormalization(rotDest.y);

			// 向きの取得
			D3DXVECTOR3 rot = rotDest;

			// 向きの正規化
			rot.x = CCalculation::RotNormalization(rot.x);
			rot.y = CCalculation::RotNormalization(rot.y);
			rot.z = CCalculation::RotNormalization(rot.z);

			// 向きの設定
			pBullet->SetRot(rot);
		}

		m_nCntInterval = m_nMaxInterval;
		m_nCntBullet--;

		if (m_nCntBullet <= 0)
		{
			m_bShot = false;
			SetReload(true);
		}

		if(!m_bRapidFire)
		{
			m_bShot = false;
		}
	}
}

//=============================================================================
// リロード
// Author : 唐�ｱ結斗
// 概要 : リロードを行う
//=============================================================================
void CGun::Reload(bool isSound)
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	if (m_bReload
		&& !m_bShot)
	{
		m_nCntReload--;

		if (m_nCntReload <= 0)
		{
			if (m_bAllReload)
			{
				if (isSound&&!m_bReloadSound)
				{
					m_bReloadSound = true;
					// 弾の装填音
					pSound->PlaySound(CSound::SOUND_LABEL_SE_RELOAD001);
				}		
				m_nCntBullet = m_nMaxBullet;
			}
			else
			{// 弾のリロード
				m_nCntBullet++;
				if (isSound)
				{
					// 弾の装填音
					pSound->PlaySound(CSound::SOUND_LABEL_SE_RELOAD000);
				}

				if (m_nCntBullet >= m_nMaxBullet)
				{
					m_bReload = false;
					m_nCntBullet = m_nMaxBullet;
				}
				else
				{
					m_nCntReload = m_nMaxReload / m_nMaxBullet;
				}
			}
		}
	}
}

//UIメッセージの生成処理
void CGun::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage();

	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 300.0f, 0.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CGun::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}
