//=============================================================================
//
// シャドウボリュームクラス(shadow_volume.h)
// Author : 唐�ｱ結斗
// 概要 : ポリゴン生成を行う
//
//=============================================================================
#ifndef _SHADOW_VOLUME_H_		// このマクロ定義がされてなかったら
#define _SHADOW_VOLUME_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <vector>
#include "polygon3D.h"
#include "texture.h"

//=============================================================================
// シャドウボリュームクラス
// Author : 唐�ｱ結斗
// 概要 : シャドウボリューム生成を行うクラス
//=============================================================================
class CShadowVolume : public CPolygon3D
{
public:
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CShadowVolume *Create(void);				// 2Dポリゴンの生成

	//--------------------------------------------------------------------
	// 静的メンバ変数
	//--------------------------------------------------------------------
	static unsigned int nCntMyObject;

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CShadowVolume();
	~CShadowVolume() override;

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void Draw() override;			// 描画
	void SetVtx() override;			// 頂点座標などの設定
	void SetVtxPolygon(std::vector<D3DXVECTOR3> vtx) { m_vtx = vtx; SetVtx(); }

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<D3DXVECTOR3> m_vtx;
};

#endif





