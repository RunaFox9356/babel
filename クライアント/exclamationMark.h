//=============================================================================
//
// exclamationMark.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _EXCLAMATION_MARK_H_		// このマクロ定義がされてなかったら
#define _EXCLAMATION_MARK_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon3D.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CExclamationMark : public CPolygon3D
{
public:

	CExclamationMark();				//コンストラクタ
	~CExclamationMark() override;	//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetParent(CObject* pParent) { m_pParent = pParent; }			//親の設定

	static CExclamationMark* Create(CObject* pParent = nullptr, D3DXVECTOR3 relativePos = DEFAULT_RELATIVE_POS, D3DXVECTOR3 size = DEFAULT_SIZE);		//生成

private:

	static const int			DEFAULT_TEXTURE_IDX;		//ディフォルトのテクスチャインデックス
	static const D3DXVECTOR3	DEFAULT_RELATIVE_POS;		//ディフォルトの相対位置
	static const D3DXVECTOR3	DEFAULT_SIZE;				//ディフォルトのサイズ

	D3DXVECTOR3	m_relativePos;		//相対位置

	CObject*	m_pParent;			// 親へのポインタ
};

#endif