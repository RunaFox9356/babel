//=============================================================================
//
// タイトルクラス(title.h)
// Author : 唐�ｱ結斗
// 概要 : タイトルクラスの管理を行う
//
//=============================================================================
#ifndef _TITLE_H_		// このマクロ定義がされてなかったら
#define _TITLE_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "scene_mode.h"
#include "application.h"
#include <vector>

//*****************************************************************************
// 前方宣言
//*****************************************************************************
//class CSocket;
class CPolygon;
class CPolygon2D;
class CPolygon3D;
class CLoading;
class CMove;
class COptionMenu;
class CText;

//=============================================================================
// シーンモードクラス
// Author : 唐�ｱ結斗
// 概要 : シーンモード生成を行うクラス
//=============================================================================
class CTitle : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// 次のモードの列挙型
	//--------------------------------------------------------------------
	enum NEXT_MODE
	{
		// 通常
		MODE_NONE = -1,			// 選択なし
		MODE_GAME,				// ゲーム
		MODE_TUTORIAL,			// チュートリアル
		MODE_END,				// ゲーム終了
		MAX_NEXT_MODE,			// 最大数
	};

	//--------------------------------------------------------------------
	// タイトルモードの列挙型
	//--------------------------------------------------------------------
	enum TITLE_MODE
	{
		// 通常
		MODE_TITLE = 0,			// タイトル
		MODE_SELECT,			// 選択
		MODE_GAME_SELECT,		// ゲームモードの選択
		MAX_MODE,				// 最大数
	};

	//--------------------------------------------------------------------
	// プレイヤー種別の列挙型
	//--------------------------------------------------------------------
	enum PLAYER_TYPE
	{
		// 通常
		TYPE_NONE = -1,			// 選択なし
		TYPE_AGENT,				// エージェント
		TYPE_HACKER,			// ハッカー

		MAX_TYPE,				// 最大数
	};

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CTitle();
	~CTitle() override;

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新

private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void NextMode(TITLE_MODE nextMode);
	void TitleMode();
	void CharaSelect();
	void FlashPolygon(CPolygon *pPolygon);

	//遷移の設定
	void SetTransition(PLAYER_TYPE mode);
	
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<CPolygon2D*> m_pPolygon;		// ポリゴン
	std::vector<CPolygon3D*> m_pPolygon3D;		// 3Dポリゴン
	std::vector<CText*> m_pText;				// 当たり判定
	NEXT_MODE m_ENextMode;						// 次のモード
	TITLE_MODE m_ETitleMode;					// タイトルモードの列挙
	PLAYER_TYPE m_EPlayerType;					// プレイヤータイプの列挙
	CPolygon3D* m_pDisplay;						// ディスプレイ
	CLoading *m_pLoading;						// ローディング
	CMove *m_pMove;								// 移動クラス
	int m_nCntFrame;							// フレームカウント
	float m_fFlash;								// フラッシュ
	float m_fAddFlash;							// フラッシュの加算値
	float m_fAlpha;								// α値
	bool m_bKey;								// キーを使用
	bool m_bTransition;							//遷移中であるかどうか
	CApplication::SConnectCheck m_Connect;
	CApplication::SCENE_MODE m_model;
	COptionMenu*	m_pOptionMenu;				//オプションメニュー
	CPolygon3D*		m_pPlayerSel[2];
	CPolygon3D*		m_pBg;
	CPolygon2D*		m_pLog;		// ポリゴン
	CText*			m_list;
	CText*			m_option;
};

#endif


