//=============================================================================
//
// slider.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "slider.h"
#include "polygon2D.h"
#include "application.h"
#include "input.h"
#include "renderer.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CSlider::DEFAULT_SIZE = { 120.0f, 30.0f, 0.0f };					//ディフォルトのサイズ
const D3DXCOLOR		CSlider::DEFAULT_BACK_COLOR = { 0.75f, 0.75f, 0.0f, 1.0f };			//ディフォルトの左側のポリゴンの色
const D3DXCOLOR		CSlider::DEFAULT_CURSOR_COLOR = { 0.0f, 0.75f, 0.75f, 0.5f };		//ディフォルトのカーソルの色



//コンストラクタ
CSlider::CSlider() : m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_size(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_fValue(0.0f),
m_fMin(0.0f),
m_fMax(0.0f),
m_fSlope(0.0f),
m_fIntercept(0.0f),
m_fDefault(0.0f),
m_bInput(false),
m_bDefault(false),
m_pBack(nullptr),
m_pCursor(nullptr)
{

}

//デストラクタ
CSlider::~CSlider()
{

}

//初期化
HRESULT CSlider::Init()
{
	//ポリゴンを生成する
	m_pBack = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

	if (m_pBack)
	{//nullチェック
		m_pBack->SetColor(DEFAULT_BACK_COLOR);		//色の設定
	}

	//ポリゴンを生成する
	m_pCursor = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

	if (m_pCursor)
	{//nullチェック
		m_pCursor->SetColor(DEFAULT_CURSOR_COLOR);		//色の設定
	}

	return S_OK;
}

//終了
void CSlider::Uninit()
{
	//ポリゴンの破棄
	if (m_pBack)
	{
		m_pBack->Uninit();
		m_pBack = nullptr;
	}
	if (m_pCursor)
	{
		m_pCursor->Uninit();
		m_pCursor = nullptr;
	}
}

//更新
void CSlider::Update()
{
	//インプットデバイスの情報を取得する
	CInput* pInput = CApplication::GetInstance()->GetInput();

	//nullチェック
	if (!pInput)
		return;

	D3DXVECTOR3 mousePos = pInput->GetMouseCursor();		//マウスカーソルの位置を取得する
	bool bInside = ((mousePos.x < m_pos.x + (m_size.x)) && (mousePos.x > m_pos.x - (m_size.x)) 
		&& (mousePos.y < m_pos.y + (m_size.y)) && (mousePos.y > m_pos.y - (m_size.y)));

	if (bInside && pInput->Trigger(MOUSE_INPUT_LEFT))
	{
		m_bInput = true;
	}
	else if (m_bInput && !pInput->Press(MOUSE_INPUT_LEFT))
		m_bInput = false;

	if (!m_bInput)
		return;

	float fNum = mousePos.x * m_fSlope + m_fIntercept;

	if (fNum < m_fMin)
		fNum = m_fMin;
	else if (fNum > m_fMax)
		fNum = m_fMax;

	m_fValue = fNum;

	CalcPolygonPosAndSize();
}

//描画
void CSlider::Draw()
{

}

// 位置のセッター
void CSlider::SetPos(const D3DXVECTOR3& pos)
{
	m_pos = pos;

	CalcParameters();
	CalcPolygonPosAndSize();
}

// 大きさのセッター
void CSlider::SetSize(const D3DXVECTOR3& size)
{
	m_size = size;

	CalcParameters();
	CalcPolygonPosAndSize();
}

// 最大数と最小数のセッター
void CSlider::SetSliderLimits(float fMax, float fMin)
{
	if (fMin == fMax)
	{
		fMin = 0.0f;
		fMax = 1.0f;
	}

	m_fMax = fMax;
	m_fMin = fMin;

	if (m_fValue < fMin)
		m_fValue = fMin;
	else if (m_fValue > fMax)
		m_fValue = fMax;

	CalcParameters();
}

//ディフォルトの値のセッター
void CSlider::SetSliderDefault(const float fValue)
{
	m_bDefault = true;
	m_fDefault = fValue;

	m_fValue = m_fDefault;

	CalcParameters();
	CalcPolygonPosAndSize();
}

//ディフォルトの値にリセットする
void CSlider::ResetValueToDefault()
{
	if (m_bDefault)
		m_fValue = m_fDefault;
	else
		m_fValue = m_fMax;

	CalcParameters();
	CalcPolygonPosAndSize();
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
CSlider* CSlider::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, float fMax, float fMin)
{
	//インスタンスを生成する
	CSlider* pObj = new CSlider;			

	//初期化処理
	if (FAILED(pObj->Init()))
		return nullptr;

	pObj->SetSliderLimits(fMax, fMin);		//スライダーの最大数と最小数の設定
	pObj->SetPos(pos);						//位置の設定
	pObj->SetSize(size);					//サイズの設定

	return pObj;							//生成したインスタンスを返す
}




//直線のパラメーターの計算
void CSlider::CalcParameters()
{
	m_fSlope = (m_fMax - m_fMin) / ((m_pos.x + (m_size.x * 0.5f)) - (m_pos.x - (m_size.x * 0.5f)));
	m_fIntercept = m_fMin - (m_fSlope * (m_pos.x - (m_size.x * 0.5f)));
}

//ポリゴンのサイズと位置の計算処理
void CSlider::CalcPolygonPosAndSize()
{
	D3DXVECTOR3 cursorPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), s = D3DXVECTOR3(0.0f, 0.0f, 0.0f), p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pCursor)
	{
		cursorPos = m_pos;
		cursorPos.x = (m_fValue - m_fIntercept) / m_fSlope;

		m_pCursor->SetSize(D3DXVECTOR3(m_size.y, m_size.y, 0.0f));
		m_pCursor->SetPos(cursorPos);
	}

	if (m_pBack)
	{
		m_pBack->SetPos(m_pos);
		m_pBack->SetSize(m_size);
	}
}
