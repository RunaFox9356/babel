//=============================================================================
//
// emptyObj.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _EMPTY_OBJ_H
#define _EMPTY_OBJ_H


//=============================================================================
// インクルード
//=============================================================================
#include "object.h"



class CEmptyObj : public CObject
{
public:
	CEmptyObj();						//コンストラクタ
	~CEmptyObj() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &pos) override { m_pos = pos; }						// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override { }								// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override { m_rot = rot; }						// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override { };									// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return m_pos; }										// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }							// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return m_rot; }									// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }							// 大きさのゲッター


	static CEmptyObj* Create();

private:

	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_rot;
};


#endif