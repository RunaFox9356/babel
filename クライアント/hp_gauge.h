//=============================================================================
//
// HPQ[WNX(hp_gauge.h)
// Author : ú±l
// Tv : IuWFNg¶¬ðs¤
//
//=============================================================================
#ifndef _HP_GAUGE_H_		// ±Ì}Nè`ª³êÄÈ©Á½ç
#define _HP_GAUGE_H_		// ñdCN[hh~Ì}Nè`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "object.h"
#include <vector>

//*****************************************************************************
// Oûé¾
//*****************************************************************************
class CPolygon;

//=============================================================================
// |SNX
// Author : ú±l
// Tv : |S¶¬ðs¤NX
//=============================================================================
class CHpGauge : public CObject
{
public:
	//--------------------------------------------------------------------
	// èè`
	//--------------------------------------------------------------------
	static const int MAX_POLYGON = 3;

	//--------------------------------------------------------------------
	// HPQ[WÌ`æû@Ìñ^
	//--------------------------------------------------------------------
	enum DROW_TYPE
	{
		TYPE_2D = 0,		// 2D
		TYPE_3D,			// 3D
		MAX_TYPE,			// Åå
	};

	//--------------------------------------------------------------------
	// ÃIoÖ
	//--------------------------------------------------------------------
	static CHpGauge *Create(DROW_TYPE EDrowType);				// HPQ[WÌ¶¬

	//--------------------------------------------------------------------
	// RXgN^ÆfXgN^
	//--------------------------------------------------------------------
	CHpGauge();
	~CHpGauge();

	//--------------------------------------------------------------------
	// oÖ
	//--------------------------------------------------------------------
	HRESULT Init() override;															// ú»
	HRESULT Init(DROW_TYPE EDrowType);													// ú»
	void Uninit() override;																// I¹
	void Update() override;																// XV
	void Draw() override;																// `æ
	void SetDrowType(DROW_TYPE EDrowType);												// `æ^CvÌÝè
	void SetPos(const D3DXVECTOR3 &pos) override;										// ÊuÌZb^[
	void SetPosOld(const D3DXVECTOR3 &posOld) override { m_posOld = posOld; }			// ßÊuÌZb^[
	void SetRot(const D3DXVECTOR3 &rot) override;										// ü«ÌZb^[
	void SetSize(const D3DXVECTOR3 &size) override;										// å«³ÌZb^[
	void SetRatio(const float fRatio);													// Q[WTCYÌä¦ÌÝè
	D3DXVECTOR3 GetPos() override { return m_pos; }										// ÊuÌQb^[
	D3DXVECTOR3 GetPosOld()  override { return m_posOld; }								// ßÊuÌQb^[
	D3DXVECTOR3 GetRot()  override { return m_rot; }									// ü«ÌQb^[
	D3DXVECTOR3 GetSize()  override { return m_size; }									// å«³ÌQb^[
	void SetMaxSize(D3DXVECTOR3 size);
	void SetMaxNumber(const float fMaxNumber);
	void SetNumber(const float fNumber);

private:
	//--------------------------------------------------------------------
	// oÖ
	//--------------------------------------------------------------------
	void Setting();

	//--------------------------------------------------------------------
	// oÏ
	//--------------------------------------------------------------------
	CPolygon* m_pPolygon[MAX_POLYGON];		// |S
	DROW_TYPE m_EDrowType;					// `æû@Ýè
	D3DXVECTOR3 m_pos;						// Êu
	D3DXVECTOR3 m_posOld;					// ßÊu
	D3DXVECTOR3 m_rot;						// ü«
	D3DXVECTOR3 m_size;						// å«³
	float m_fRatio;							// Q[WTCYÌä¦ 						
};

#endif




