//=============================================================================
//
// レンダラークラス(renderer.h)
// Author : 唐�ｱ結斗
// 概要 : 描画を行うクラス
//
//=============================================================================
#ifndef _RENDERER_H_		// このマクロ定義がされてなかったら
#define _RENDERER_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ
#pragma comment(lib,"dxguid.lib")		// DirectXのコンポネート(部品)の使用に必要

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "d3dx9.h"							// 描画処理に必要

//=============================================================================
// レンダラークラス(renderer.h)
// Author : 唐�ｱ結斗
// 概要 : 描画を行うクラス
//=============================================================================
class CRenderer
{
public:
	//*****************************************************************************
	// 定数定義
	//*****************************************************************************
	static const int SCREEN_WIDTH = 1280;			// スクリーンの幅
	static const int SCREEN_HEIGHT = 720;			// スクリーンの高さ

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CRenderer();
	~CRenderer();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init(HWND hWnd, bool bWindow);
	void Uninit();
	void Update();
	void Draw();
	void SetStencil(const int nStencilTest, D3DCMPFUNC EStencilFunc);
	void SetStencilReflection(D3DSTENCILOP EAllSuccess, D3DSTENCILOP EAllFailure, D3DSTENCILOP EZFailure);
	void ClearStencil();
	LPDIRECT3DDEVICE9 GetDevice() { return m_pD3DDevice; }
	LPDIRECT3DTEXTURE9 GetRenderTexture() { return m_pRenderTexture; }

#ifdef _DEBUG
	double GetUpdateTime() { return m_updateTime; }
	double GetRenderTime() { return m_renderTime; }
#endif // _DEBUG

private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
//#ifdef _DEBUG
//	void DrawFPS();
//#endif // _DEBUG

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	LPDIRECT3D9						m_pD3D;						// Direct3Dオブジェクト
	LPDIRECT3DDEVICE9				m_pD3DDevice;				// Deviceオブジェクト
	LPDIRECT3DTEXTURE9				m_pRenderTexture;			// レンダーテクスチャ用のポインタ
	IDirect3DSurface9				*m_pDrowSurface;			// 描画用サーフェイスのポインタ
	IDirect3DSurface9				*m_pZBufferSurface;			// Zバッファ用サーフェイスのポインタ
	IDirect3DSurface9*				m_pOrgSurface;				// 元の描画用サーフェイスのポインタ
	IDirect3DSurface9*				m_pOrgZBuffeSurface;		// 元のZバッファ用サーフェイスのポインタ

#ifdef _DEBUG
	// フォント
	LPD3DXFONT	m_pFont;

	LARGE_INTEGER m_updateBeginTime;
	LARGE_INTEGER m_updateEndTime;
	LARGE_INTEGER m_updateFrequency;
	double m_updateTime;

	LARGE_INTEGER m_drawBeginTime;
	LARGE_INTEGER m_drawEndTime;
	LARGE_INTEGER m_drawFrequency;
	double m_renderTime;
#endif // _DEBUG
};

#endif
