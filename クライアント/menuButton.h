//=============================================================================
//
// menuButton.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _MENU_BUTTON_H_		// このマクロ定義がされてなかったら
#define _MENU_BUTTON_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CMenuButton : public CPolygon2D
{
public:
	CMenuButton();					//コンストラクタ
	~CMenuButton() override;		//デストラクタ


	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetParent(CObject* pParent);										//親へのポインタの設定
	void SetTriggerState(const bool bTrigger) { m_bTrigger = bTrigger; }	//トリガーの設定
	void SetRelativePos(const D3DXVECTOR3 pos);								//相対位置の設定
	CObject* GetParent() { return m_pParent; }								//親へのポインタの取得
	const bool GetTriggerState() { return m_bTrigger; }						//トリガーの取得
	const bool GetHoverState() { return m_bMouseOver; }						//マウスカーソルと重なっているかどうかの取得
	const D3DXVECTOR3 GetRelativePos();										//相対位置の取得

	void SetTexture(const int nTexIdx);										//テクスチャの設定

	static CMenuButton* Create();																				//生成
	static CMenuButton* Create(const D3DXVECTOR3 pos);															//生成
	static CMenuButton* Create(const D3DXVECTOR3 pos, const D3DXCOLOR normalCol, const D3DXCOLOR triggerCol);	//生成

private:

	static const D3DXCOLOR DEFAULT_NORMAL_COLOR;			//ディフォルトの普通色
	static const D3DXCOLOR DEFAULT_MOUSE_OVER_COLOR;		//ディフォルトのマウスカーソルと重なっている時の色
	static const D3DXVECTOR3 DEFAULT_SIZE;					//ディフォルトのサイズ

	D3DXVECTOR3 m_relativePos;		//位置
	D3DXCOLOR	m_normalColor;		//普通の色
	D3DXCOLOR	m_mouseOverColor;	//ウスカーソルと重なっている時の色

	bool		m_bMouseOver;		//マウスカーソルと重なっているかどうか
	bool		m_bTrigger;			//トリガー

	CObject*	m_pParent;			//親へのポインタ
};


#endif