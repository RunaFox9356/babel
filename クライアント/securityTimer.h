//=============================================================================
//
// securityTimer.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SECURITY_TIMER_H_		// このマクロ定義がされてなかったら
#define _SECURITY_TIMER_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CNumber;
class CText;


class CSecurityTimer : public CPolygon2D
{
public:

	CSecurityTimer();				//コンストラクタ
	~CSecurityTimer() override;		//デストラクタ

	HRESULT Init() override;		//初期化
	void Uninit() override;			//終了
	void Update() override;			//更新
	void Draw() override;			//描画

	void SetPos(const D3DXVECTOR3 &pos) override;		//位置の設定
	const bool GetEnd() { return m_bEnd; }				//終わったかどうかのフラグの取得

	void ResetTimer();				//タイマーをリセットする

	static CSecurityTimer* Create(D3DXVECTOR3 pos = DEFAULT_POS, D3DXVECTOR3 size = DEFAULT_SIZE, float fTime = DEFAULT_TIME);		//生成

private:

	void UpdateTimer();				//タイマーの更新処理
	void IconUpdate();				//アイコンアニメーション

private:

	static const int			DEFAULT_TIMER_DIGIT = 2;	//タイマーの桁数
	static const float			DEFAULT_TIME;				//ディフォルトの最大時間
	static const D3DXVECTOR3	DEFAULT_POS;				//ディフォルトの位置
	static const D3DXVECTOR3	DEFAULT_SIZE;				//ディフォルトのサイズ
	static const D3DXVECTOR3	DEFAULT_DIGIT_SIZE;			//ディフォルトの数値のサイズ
	static const char*			DEFAULT_TEXT;				//ディフォルトのテキスト


	int					m_nCntAnim;							//アニメーションカウンター
	int					m_nLastTime;						//前のフレームの時間
	float				m_fMaxTime;							//残っている秒
	float				m_fStartTime;						//はじめの時間
	bool				m_bEnd;								//終わったかどうかのフラグ

	CPolygon2D*			m_pTimer[DEFAULT_TIMER_DIGIT];		//タイマーの桁
	CText*				m_pText;							//テキストへのポインタ
	CPolygon2D*			m_pIcon[2];							//アイコンへのポインタ

};

#endif