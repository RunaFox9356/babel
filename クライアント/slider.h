//=============================================================================
//
// slider.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SLIDER_H_		// このマクロ定義がされてなかったら
#define _SLIDER_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CPolygon2D;

class CSlider : public CObject
{
public:

	CSlider();															//コンストラクタ
	~CSlider() override;												//デストラクタ

	HRESULT Init() override;											//初期化
	void Uninit() override;												//終了
	void Update() override;												//更新
	void Draw() override;												//描画

	void SetPos(const D3DXVECTOR3 &pos) override;						// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}			// 過去位置のセッタ
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {}					// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size) override;						// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return m_pos; }						// 位置のゲッター
	D3DXVECTOR3 GetPosOld() override { return D3DXVECTOR3(); }			// 過去位置のゲッタ
	D3DXVECTOR3 GetRot() override { return D3DXVECTOR3(); }				// 向きのゲッター
	D3DXVECTOR3 GetSize() override { return m_size; }					// 大きさのゲッター

	const float GetSliderValue() { return m_fValue; }					//スライダーの値のゲッター
	const bool	IsBeingChanged() { return m_bInput; }					//使われているかどうかのフラグのゲッター

	void SetSliderLimits(float fMax, float fMin);						// 最大数と最小数のセッター
	void SetSliderDefault(const float fValue);							//ディフォルトの値のセッター
	void ResetValueToDefault();											//ディフォルトの値にリセットする


	static CSlider* Create(D3DXVECTOR3 pos, D3DXVECTOR3 size = DEFAULT_SIZE, float fMax = 1.0f, float fMin = 0.0f);		//生成

private:

	void CalcParameters();								//直線のパラメーターの計算
	void CalcPolygonPosAndSize();						//ポリゴンのサイズと位置の計算処理

private:

	static const D3DXVECTOR3	DEFAULT_SIZE;			//ディフォルトのサイズ
	static const D3DXCOLOR		DEFAULT_BACK_COLOR;		//ディフォルトの左側のポリゴンの色
	static const D3DXCOLOR		DEFAULT_CURSOR_COLOR;	//ディフォルトのカーソルの色


	D3DXVECTOR3		m_pos;					//スライダーの位置
	D3DXVECTOR3		m_size;					//スライダーのサイズ
											
	float			m_fValue;				//スライダーの値
	float			m_fMin;					//最少数
	float			m_fMax;					//最大数
	float			m_fDefault;				//ディフォルトの値

	float			m_fSlope;				//
	float			m_fIntercept;			//

	bool			m_bInput;				//使われているかどうかのフラグ
	bool			m_bDefault;				//ディフォルトの値が設定されていたかのフラグ

	CPolygon2D*		m_pBack;				//背景のポリゴン
	CPolygon2D*		m_pCursor;				//カーソル
};

#endif