//=============================================================================
//
// task.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _TASK_H_		// このマクロ定義がされてなかったら
#define _TASK_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "manager.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CTaskUI;

class CTask : public CManager
{
public:

	//エージェントのタスク
	enum AGENT_TASK_TYPE
	{
		AGENT_TASK_RESCUE = 0,									//救出
		AGENT_TASK_DESTROY,										//破壊
		AGENT_TASK_KILL,										//暗殺
		AGENT_TASK_STEAL,										//ぬすみだす

		AGENT_TASK_MAX
	};

	//ハッカーのタスク
	enum HACKER_TASK_TYPE
	{
		HACKER_TASK_HACK = 0,									//ハッキング
		HACKER_TASK_FIND_PRISONER,								//救出ターゲットを見つける
		HACKER_TASK_FIND_DESTROYABLE,							//破壊ターゲットを見つける
		HACKER_TASK_FIND_ENEMY,									//敵を見つける

		HACKER_TASK_MAX
	};

public:
	CTask();													//コンストラクタ
	~CTask();													//デストラクタ

	HRESULT Init() override;									//初期化
	void Uninit() override;										//終了
	void Update() override;										//更新
	void Draw() override;										//描画

	void AddTask(const AGENT_TASK_TYPE type);					//エージェントのタスクの目的の追加
	void SubtractTask(const AGENT_TASK_TYPE type);				//エージェントのタスクの目的の削減
	void AddHackerTask(const HACKER_TASK_TYPE type);			//ハッカーのタスクの目的の追加
	void SubtractHackerTask(const HACKER_TASK_TYPE type);		//ハッカーのタスクの目的の削減

	const int GetTaskObjectiveMax(const AGENT_TASK_TYPE type);	//タスクの目的の最大数の取得
	int* GetAllTaskObjectiveMax();								//タスクの目的の最大数の取得(全部)

	const int GetTaskObjectiveMax(const HACKER_TASK_TYPE type);	//ハッカーのタスクの目的の最大数の取得
	int* GetAllHackerTaskObjectiveMax();						//ハッカーのタスクの目的の最大数の取得(全部)

	const int GetTaskObjective(const AGENT_TASK_TYPE type);		//タスクの目的の取得
	int* GetAllTaskObjective();									//タスクの目的の取得(全部)

	const int GetTaskObjective(const HACKER_TASK_TYPE type);	//ハッカーのタスクの目的の取得
	int* GetHackerAllTaskObjective();							//ハッカーのタスクの目的の取得(全部)

	void CreateUI();											//UIの生成処理

	static CTask* Create();										//生成

private:

	int			m_nAgentObjectiveNum[AGENT_TASK_MAX];			//エージェントの目的の数
	int			m_nAgentObjectiveMax[AGENT_TASK_MAX];			//エージェントのタスクの目的の最大数

	int			m_nHackerObjectiveNum[HACKER_TASK_MAX];			//ハッカーの目的の数
	int			m_nHackerObjectiveMax[HACKER_TASK_MAX];			//ハッカーのタスクの目的の最大数

	CTaskUI*	m_pUi;											//UIへのポインタ
};



#endif