#include "typing.h"
#include "polygon2D.h"
#include "input.h"
#include "debug_proc.h"
#include "application.h"

CMinGame_Typing::CMinGame_Typing(int nPriority)
{


}

CMinGame_Typing::~CMinGame_Typing()
{


}

HRESULT CMinGame_Typing::Init()
{
	{//初期化
		uninitflg = true;
		uninitflg2 = true;
		uninitflg3 = true;
		uninitflg4 = true;
	}

	//switch (switch_on)
	//{
	//default:
	//	break;
	//}

	CMiniGame::Init();

	//エンター押せに変更
	pPolygonUp = CPolygon2D::Create();
	pPolygonUp->SetPos(D3DXVECTOR3(240.0f, 460.0f, 0.0f));
	pPolygonUp->SetSize(D3DXVECTOR3(1280.0f*0.2f, 720.0f*0.2f, 0.0f));
	pPolygonUp->LoadTex(102);

	pPolygonDown = CPolygon2D::Create();
	pPolygonDown->SetPos(D3DXVECTOR3(640.0f, 460.0f, 0.0f));
	pPolygonDown->SetSize(D3DXVECTOR3(1280.0f*0.2f, 720.0f*0.2f, 0.0f));
	pPolygonDown->LoadTex(104);

	pPolygonRight = CPolygon2D::Create();
	pPolygonRight->SetPos(D3DXVECTOR3(840.0f, 460.0f, 0.0f));
	pPolygonRight->SetSize(D3DXVECTOR3(1280.0f*0.2f, 720.0f*0.2f, 0.0f));
	pPolygonRight->LoadTex(105);

	pPolygonLeft = CPolygon2D::Create();
	pPolygonLeft->SetPos(D3DXVECTOR3(440.0f, 460.0f, 0.0f));
	pPolygonLeft->SetSize(D3DXVECTOR3(1280.0f*0.2f, 720.0f*0.2f, 0.0f));
	pPolygonLeft->LoadTex(103);

	pyajirushi = CPolygon2D::Create();
	pyajirushi->SetPos(D3DXVECTOR3(640.0f, 150.0f, 0.0f));
	pyajirushi->SetSize(D3DXVECTOR3(1280.0f*0.7f, 720.0f*0.3f, 0.0f));
	pyajirushi->LoadTex(120);

	return S_OK;
}

void CMinGame_Typing::Uninit()
{
	CMiniGame::Uninit();
}

void CMinGame_Typing::Update()
{
	int Count;

	CMiniGame::Update();

// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	//押したとき	優先順位 1
	if (pInput->Trigger(DIK_W))
	{
		if (uninitflg)
		{
			pPolygonUp->Uninit();
		}
		uninitflg = false;
	}
	//押したとき	優先順位 2
	if (!uninitflg)
	{
		if (pInput->Trigger(DIK_A))
		{
			if (uninitflg4)
			{
				pPolygonLeft->Uninit();
			}
			uninitflg4 = false;
		}

	}
	//押したとき	優先順位 3
	if (!uninitflg2)
	{
		if (pInput->Trigger(DIK_D))
		{
			if (uninitflg3)
			{
				pPolygonRight->Uninit();
				pyajirushi->Uninit();
			}
			uninitflg3 = false;
		}
	}
	//押したとき	優先順位 4
	if (!uninitflg4)
	{
		if (pInput->Trigger(DIK_S))
		{
			if (uninitflg2)
			{
				pPolygonDown->Uninit();
			}
			uninitflg2 = false;
		}
	}



}

void CMinGame_Typing::Draw()
{
	CMiniGame::Draw();
}

CMinGame_Typing * CMinGame_Typing::Create(int count)
{

	// オブジェクトインスタンス
	CMinGame_Typing *pTypingGame = nullptr;

	// メモリの解放
	pTypingGame = new CMinGame_Typing(count);

	if (pTypingGame != nullptr)
	{// 数値の初期化
		pTypingGame->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pTypingGame;
}
