//=============================================================================
//
// soundSource.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SOUND_SOURCE_H_		// このマクロ定義がされてなかったら
#define _SOUND_SOURCE_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"
#include "sound.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CSoundSource : public CObject
{
public:

	CSoundSource();					//コンストラクタ
	~CSoundSource() override;		//デストラクタ

	HRESULT Init() override;		//初期化
	void Uninit() override;			//終了
	void Update() override;			//更新
	void Draw() override;			//描画

	void SetPos(const D3DXVECTOR3 &pos) { m_pos = pos; }	// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) {}			// 過去位置のセッタ
	void SetRot(const D3DXVECTOR3 &/*rot*/) {}					// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) {}				// 大きさのセッター
	D3DXVECTOR3 GetPos() { return m_pos; }					// 位置のゲッター
	D3DXVECTOR3 GetPosOld() { return D3DXVECTOR3(); }		// 過去位置のゲッタ
	D3DXVECTOR3 GetRot() { return D3DXVECTOR3(); }			// 向きのゲッター
	D3DXVECTOR3 GetSize() { return D3DXVECTOR3(); }			// 大きさのゲッター

	void SetDelay(const int nDelay) { m_nDelay = nDelay; m_nCntDelay = 0; }		//ディレイの設定
	void SetUpdatingFlag(const bool bUpdate) { m_bUpdate = bUpdate; m_nCntDelay = 0; }	//更新するかどうかのフラグのセッター
	void SetSoudLabel(CSound::SOUND_LABEL label);			//サウンドラベルの設定
	void SetSoundMax(const float fSoundMax) { m_fSoundMax = fSoundMax; }		//最大音量の設定

	CSound::SOUND_LABEL GetSoundLabel() { return m_label; }	//サウンドラベルの取得

	static CSoundSource* Create(D3DXVECTOR3 pos, CSound::SOUND_LABEL label, const int nPlayDelay, const float fMaxSound, const float fMaxDist);		//生成
	static CSoundSource* Create(CObject* pParent, CSound::SOUND_LABEL label, const int nPlayDelay, const float fMaxSound, const float fMaxDist);	//生成

private:

	void CalcParameters();						//更新処理に必要なパラメーターの計算処理

private:

	D3DXVECTOR3				m_pos;				//位置
	int						m_nDelay;			//再生のディレイフレーム数
	int						m_nCntDelay;		//ディレイのカウンター
	float					m_fMaxDist;			//音波減衰係数
	float					m_fSoundMax;		//最大音量
	CSound::SOUND_LABEL		m_label;			//サウンドのラベル
	CObject*				m_pParent;			//親へのポインタ
	CSound*					m_pSound;			//サウンドへのポインタ

	float					m_fSlope;			//音波減衰のための変数
	float					m_fIntercept;		//音波減衰のための変数

	bool					m_bUpdate;			//更新するかどうかのフラグ

};

#endif