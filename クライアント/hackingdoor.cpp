//=============================================================================
//
// ハッキング可能オブジェクトクラス(HackingObj.cpp)
// Author : 浜田琉雅
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>

#include "model3D.h"
#include "hackingdoor.h"
#include "model_obj.h"
#include "renderer.h"
#include "application.h"
#include "collision_rectangle3D.h"
#include "MiniGameMNG.h"
#include "map.h"
#include "scene_mode.h"
#include "task.h"
#include "hacker.h"
#include "GimmicDoor.h"

//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
//CModelObj*					m_HackObj;				// モデル

//=============================================================================
// インスタンス生成
// Author : 浜田琉雅
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CHackDoor * CHackDoor::Create(const bool bOpenInXDirection)
{
	// オブジェクトインスタンス
	CHackDoor *pModelObj = nullptr;

	// メモリの解放
	pModelObj = new CHackDoor;

	// メモリの確保ができなかった
	assert(pModelObj != nullptr);

	pModelObj->m_bOpenInXDirection = bOpenInXDirection;

	// 数値の初期化
	pModelObj->Init();

	// インスタンスを返す
	return pModelObj;
}

//=============================================================================
// ファイルを読み込み処理
// Author : 唐�ｱ結斗
// 概要 : ファイルを読み込みモデルを生成する
//=============================================================================
void CHackDoor::LoadFile(const char *pFileName)
{
	CModelObj::LoadFile(pFileName);
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CHackDoor::CHackDoor() : m_bOpenInXDirection(true),
m_doorModle(nullptr)
{

}

//=============================================================================
// デストラクタ
// Author : 浜田琉雅
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CHackDoor::~CHackDoor()
{

}

//=============================================================================
// 初期化
// Author : 浜田琉雅
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CHackDoor::Init()
{
	SetObjType(OBJETYPE_HACKOBJ);

	CHackObj::Init();

	SetHackState(STATE_NOT_HACK);

	CTask* pTask = CHacker::GetTask();			//タスクの取得

	if (pTask)
	{//nullチェック
		pTask->AddHackerTask(CTask::HACKER_TASK_HACK);	//タスクの更新
	}
	m_doorModle = CGimmicDoor::Create(m_bOpenInXDirection);
	
	m_doorModle->bSetType(CGimmicDoor::Type_None);
	if (m_doorModle)
	{
		D3DXVECTOR3 Pos = GetPos() + D3DXVECTOR3(150, 0.f, 100.0f);
		m_doorModle->SetPos(Pos);
		//m_doorModle->SetObjType(CObject::EObjectType::OBJETYPE_GIMMICK);
	}

	SetDraw(false);

	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 浜田琉雅
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CHackDoor::Uninit()
{

}

//=============================================================================
// 更新
// Author : 浜田琉雅
// 概要 : 更新を行う
//=============================================================================
void CHackDoor::Update()
{
	if (GetHackState() == STATE_HACKED)
	{
		m_doorModle->SetAnimation(true);
	}
	else
	{
		m_doorModle->SetAnimation(false);
	}
}

//=============================================================================
// 描画
// Author : 浜田琉雅
// 概要 : 描画を行う
//=============================================================================
void CHackDoor::Draw()
{
}

//=============================================================================
// ハッキング
// Author : 浜田琉雅
// 概要 :　sceneからハッキングを呼び出して判定
//=============================================================================
void CHackDoor::Hacking()
{
	CMiniGameMNG* pMane = CApplication::GetInstance()->GetSceneMode()->GetMiniMng();
	pMane->LoadMinigame();
}

void CHackDoor::SetModelType(int nType, const bool bRot)
{
	if (m_doorModle)
	{
		m_doorModle->SetModelType(nType, bRot);
	}
}

//位置の設定
void CHackDoor::SetPos(const D3DXVECTOR3 &pos)
{
	CHackObj::SetPos(pos);

	if (m_doorModle)
	{
		m_doorModle->SetPos(pos);
	}
}