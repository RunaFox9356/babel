//=============================================================================
//
// endAnimation.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _END_ANIMATION_H_		// このマクロ定義がされてなかったら
#define _END_ANIMATION_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CEndAnimation : public CObject
{
public:

	CEndAnimation();												//コンストラクタ
	virtual ~CEndAnimation() override;								//デストラクタ

	virtual HRESULT Init() override;								//初期化
	virtual void Uninit() override;									//終了
	virtual void Update() override;									//更新
	virtual void Draw() override;									//描画

	void SetPos(const D3DXVECTOR3 &pos) override { m_pos = pos; }	//位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) {}				//過去位置のセッタ
	void SetRot(const D3DXVECTOR3 &rot) { m_rot = rot; }			//向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) {}					//大きさのセッター
	D3DXVECTOR3 GetPos() override { return m_pos; }					//位置のゲッター
	D3DXVECTOR3 GetPosOld() override { return D3DXVECTOR3(); }		//過去位置のゲッタ
	D3DXVECTOR3 GetRot() override { return m_rot; }					//向きのゲッター
	D3DXVECTOR3 GetSize() override  { return D3DXVECTOR3(); }		//大きさのゲッター

	const bool GetEnd() { return m_bEnd; }							//終わったかどうかのフラグの取得処理

protected:

	void SetEnd(const bool bEnd) { m_bEnd = bEnd; }					//終わったかどうかのフラグの設定処理

private:

	D3DXVECTOR3		m_pos;				//位置
	D3DXVECTOR3		m_rot;				//向き
	bool			m_bEnd;				//アニメーションが終わったかどうかのフラグ

};

#endif