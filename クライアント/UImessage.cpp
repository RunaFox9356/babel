//=============================================================================
//
// UImessage.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "UImessage.h"
#include "text.h"


//コンストラクタ
CUiMessage::CUiMessage() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL4),
m_pText(nullptr)
{

}

//デストラクタ
CUiMessage::~CUiMessage()
{

}

// 初期化
HRESULT CUiMessage::Init()
{
	//基本クラスの初期化処理
	CPolygon2D::Init();

	return S_OK;
}

// 終了
void CUiMessage::Uninit()
{
	//テキストの破棄
	if (m_pText)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	//基本クラスの終了処理
	CPolygon2D::Uninit();
}

// 更新
void CUiMessage::Update()
{
	//基本クラスの更新処理
	CPolygon2D::Update();
}

// 描画
void CUiMessage::Draw()
{
	//基本クラスの描画処理
	CPolygon2D::Draw();
}

//生成
CUiMessage* CUiMessage::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const int nTextureIdx, const char* pMessageText, const D3DXCOLOR col)
{
	CUiMessage* pUi = new CUiMessage;		//インスタンスを生成する

	if (FAILED(pUi->Init()))
	{//初期化処理
		return nullptr;
	}

	pUi->SetPos(pos);			//位置の設定
	pUi->SetSize(size);			//サイズの設定
	std::vector<int> pTex;
	pTex.push_back(nTextureIdx);
	pUi->LoadTex(pTex);

	if (pMessageText && strlen(pMessageText) > 0)
		pUi->CreateText(pos, size, pMessageText, col);		//テキストの生成

	return pUi;				//生成したインスタンスを返す
}

//テキストの生成
void CUiMessage::CreateText(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const char * pMessageText, const D3DXCOLOR col)
{
	D3DXVECTOR3 fontSize = D3DXVECTOR3(10.0f, 10.0f, 10.0f);							//フォントサイズ
	D3DXVECTOR3 P = pos + D3DXVECTOR3(size.x * 0.5f + fontSize.x, 0.0f, 0.0f);			//絶対位置の設定
	m_pText = CText::Create(P, size, CText::MAX, 1000, 5, pMessageText, fontSize, col);	//テキストの生成
}
