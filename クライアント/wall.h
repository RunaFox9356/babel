//=============================================================================
//
// wall.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _WALL_H
#define _WALL_H


//=============================================================================
// インクルード
//=============================================================================
#include "model_obj.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CWall : public CModelObj
{
public:

	enum EWallType
	{
		WALL_TYPE_1 = 0,
		WALL_TYPE_2,

		WALL_TYPE_MAX
	};

	static const int WALL_MODEL_IDX[WALL_TYPE_MAX];			//壁のモデルインデックス

	CWall();												//コンストラクタ
	~CWall() override;										//デストラクタ

	HRESULT Init() override;								//初期化処理
	void Uninit(void) override;								//終了処理
	void Update(void) override;								//更新処理
	void Draw(void) override;								//描画処理

	void SetWallType(const EWallType type);					//種類の設定

	const int GetType() { return m_nWallType; }				//壁の種類の取得

	static CWall* Create(const D3DXVECTOR3 pos, const EWallType type, const int nDir);			//生成処理

private:

	void SetDir(const int nDir);							//向きの設定
	void RotateHitbox(const int nDir);						//当たり判定を回転させる

private:

	int						m_nWallType;					//壁の種類
};


#endif