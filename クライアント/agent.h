//=============================================================================
//
// エージェントクラス(agent.h)
// Author : 有田　明玄
// 概要 : エージェント生成を行う
//
//=============================================================================
#ifndef _AGENT_H_			// このマクロ定義がされてなかったら
#define _AGENT_H_			// 二重インクルード防止のマクロ定義


#include "player.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMove;
class CUdp_Socket;
class CModelObj;
class CCollision_Rectangle3D;
class CHpGauge;
class CGimmick;
class CEscapePoint;
class CInput;
class CEmptyObj;
class CSkinMesh;
class CPolygon3D;
class CEnemy;
class CUiMessage;
//=============================================================================
// プレイヤークラス
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行うクラス
//=============================================================================
class CAgent : public CPlayer
{
public:
	//--------------------------------------------------------------------
	// 定数定義
	//--------------------------------------------------------------------
	static const float fSPEED;
	static const float fDASH;
	static const float fJAMP;

	//--------------------------------------------------------------------
	// プレイヤーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_STATE
	{
		// 通常
		NEUTRAL_ACTION = 0,		// ニュートラル
		MOVE_ACTION,			// 移動
		DASH_ACTION,			// 高速移動
		JAMP_ACTION,			// ジャンプ
		LANDING_ACTION,			// 着地
		//SQUAT_ACTION,			// しゃがむ
		AIM_ACTION,				// 構える

		MAX_ACTION,				// 最大数
	};

	//--------------------------------------------------------------------
	// プレイヤーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_STATE_SKIN
	{
		// 通常
		GAN_SKIN = 0,			// 銃を持って歩く
		MOVE_SKIN,			// 移動
		DASH_SKIN,			// 高速移動
	//	LANDING_SKIN,			// 着地
	//	SQUAT_SKIN,			// しゃがむ
		HOLD_SKIN,				//掴み
		HOLDMOVE_SKIN,			//掴み移動
		MAX_SKIN,				// 最大数
	};
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CAgent *Create();			// プレイヤーの生成

	static void SetNormalMouseSensibility(const float fValue) { m_fMouseSensibilityNormal = fValue; }	//普通のマウス感度のセッター
	static void SetAimMouseSensibility(const float fValue) { m_fMouseSensibilityAim = fValue; }			//狙うモードのマウス感度のセッター

	static float GetNormalMouseSensibility() { return m_fMouseSensibilityNormal; }						//普通のマウス感度のゲッター
	static float GetAimMouseSensibility() { return m_fMouseSensibilityAim; }							//狙うモードのマウス感度のゲッター

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CAgent(int nPriority = PRIORITY_LEVEL0);
	~CAgent();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	virtual HRESULT Init();											// 初期化
	void Uninit() override;											// 終了
	void Update() override;											// 更新
	void Draw() override;											// 描画
	bool bGetDoor() { return m_door; }
	void bSetDoor(bool flg) { m_door = flg; }
	bool GetHide() { return m_bhide; }
	void SetHide(bool flg) { m_bhide = flg; }
	void SetGimmick(CGimmick* pGimmick);							//使っているギミックの設定
	CGimmick* GetActiveGimmick() { return m_pActiveGimmick; }		//使っているギミックの取得
	const D3DXVECTOR3 GetPlayerBody();								//プレイヤーの体の相対位置
	const D3DXVECTOR3 GetPlayerHead();								//プレイヤーの頭の相対位置
	const bool PickUpStone();										//石を拾う
	const bool ThrowStone();										//石を投げる
	int GetLeftBullet();											//残数弾の取得
	void SetSecurityLevel(const int nSecurity) { m_nSecurityLevel = nSecurity; }
	void SetSkinAction(ACTION_STATE_SKIN state);
	const bool GetDash() { return m_bDash; }						//走っているかどうかのゲッター
	void SetInvulnerability(const int nInvulnerability) { m_nInvulnerability = nInvulnerability; }		//無敵状態のカウンターのセッター

	CHpGauge* GetHpGauge() { return m_pHpGauge; }					//HPゲージの取得処理


private:

	enum UiMessageType
	{
		TYPE_INTERACTION = 0,
		TYPE_RELOAD,

		TYPE_MAX
	};

	//--------------------------------------------------------------------
	// 定数定義
	//--------------------------------------------------------------------
	static const D3DXVECTOR3	BODY_RELATIVE_POS;				//プレイヤーの体の相対位置
	static const D3DXVECTOR3	HEAD_RELATIVE_POS;				//プレイヤーの頭の相対位置
	static const D3DXVECTOR3	EYE_RELATIVE_POS;				//目の相対位置
	static const int			DEFAULT_LIFE;					//ディフォルトのライフ
	static const int			DEFAULT_INVULNERABILITY;		//無敵状態のフレーム数
	static const D3DXVECTOR3	DEFAULT_TARGET_POS;				//ターゲットUIの位置
	static const float			DEFAULT_SHOOT_SOUND_RADIUS;		//弾の音の半径


	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void MotionSetting();
	void Dash();
	void CancelDash();
	void Squat();
	void Jamp();
	void Shot();
	void Landing();
	void KeySet();
	void CollisionCheck(CInput* pInput);					// 当たり判定
	void SwitchAimMode();									// 狙うモードを切り替える
	void CalcEyePos();										// 目の位置の計算処理
	void SetItemOffset() override;
	void AllertEnemies();									// 弾をうつと周りの敵のが聞こえるようにする
	void CreateUiMessage(UiMessageType type, const char* pMessage, const int nTexIdx);		//UIメッセージの生成処理
	void DestroyUiMessage(UiMessageType type);								//UIメッセージの破棄処理
	
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	CHpGauge		*m_pHpGauge;		// HPゲージ
	ACTION_STATE	m_EState;			// アクションタイプ
	float			m_fGravity;			// 重力加速度
	int				m_nNumMotion;		// 現在のモーション番号
	int				m_nNumHandParts;	// 手パーツの番号取得
	int				m_nLife;			// ライフ
	int				m_nInvulnerability;	// 無敵状態のカウンター
	int				m_nSecurityLevel;	
	bool			m_bDash;			// ダッシュフラグ
	bool			m_bJamp;			// ジャンプフラグ
	bool			m_bAimMode;			// 狙うモードであるかどうか
	bool			m_bStone;			// 石を拾ったかのフラグ

	bool			m_door;
	bool			m_bCarry;			//物を運んでいる状態かどうか
	bool			m_bhide;
	CGimmick*		m_pActiveGimmick;	//使っているギミックへのポインタ
	CEmptyObj*		m_pEyeObj;			//目の前の空のオブジェクト(狙うモード用)

	ACTION_STATE_SKIN				m_ESkinState;	//スキンメッシュのアニメ
	CPolygon3D*		m_pTargetUi;		//ターゲットのUI

	static float	m_fMouseSensibilityNormal;	//普通のマウス感度
	static float	m_fMouseSensibilityAim;		//狙うモードのマウス感度
	CEnemy*		m_pCarryEnemy;		//運んでいる敵
	CUiMessage*		m_pUiMessage;		//メッセージのUI
	CUiMessage*	m_pReloadUi;					//	メッセージのUI
};

#endif

