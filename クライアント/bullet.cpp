//=============================================================================
//
// モデルオブジェクトクラス(model_obj.cpp)
// Author : 唐�ｱ結斗
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>

#include "model3D.h"
#include "bullet.h"
#include "model_obj.h"
#include "renderer.h"
#include "application.h"
#include "collision_rectangle3D.h"
#include "move.h"
#include "camera.h"
#include "calculation.h"
#include "trail.h"


//=============================================================================
//
//								静的変数の初期化
//
//=============================================================================
const D3DXVECTOR3	CBullet::DEFAULT_SIZE = { 10.0f, 5.0f, 10.0f };		//ディフォルトのサイズ

namespace
{
	CTrail::SInfo trailData = { D3DXCOLOR(1.0f, 0.5f, 0.0f, 1.0f), D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f), "", 25.0f, false };

	const int maxLife = 100;
	const float friction = 0.5f;
}

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CBullet * CBullet::Create()
{
	// オブジェクトインスタンス
	CBullet *pModelObj = nullptr;

	// メモリの解放
	pModelObj = new CBullet;

	// メモリの確保ができなかった
	assert(pModelObj != nullptr);

	// 数値の初期化
	pModelObj->Init();

	// インスタンスを返す
	return pModelObj;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CBullet::CBullet() : m_life(0), m_pTrail(nullptr)
{
}

//=============================================================================
// デストラクタ
// Author : 有田明玄
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CBullet::~CBullet()
{

}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CBullet::Init()
{
	CModelObj::Init();
	SetObjType(OBJTYPE_PLAYER_BULLET);
	SetType("bullet");

	D3DXMATRIX mtx;
	m_pTrail = CTrail::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), mtx);

	CCollision_Rectangle3D* pCollision = GetCollision();

	if (pCollision)
	{
		pCollision->SetSize(DEFAULT_SIZE);
		pCollision->SetPos(D3DXVECTOR3(0.0f, DEFAULT_SIZE.y * 0.5f, 0.0f));
	}

	// 移動クラスのメモリ確保
	m_pMove = new CMove;
	assert(m_pMove != nullptr);
	m_pMove->SetMoving(15.0f, 5000.0f, 0.0f, 0.1f);

	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CBullet::Uninit()
{
	CModelObj::Uninit();

	if (m_pTrail != nullptr)
	{
		m_pTrail->Uninit();
		m_pTrail = nullptr;
	}

	if (m_pMove != nullptr)
	{// メモリの解放
		delete m_pMove;
		m_pMove = nullptr;
	}
}

void CBullet::Update()
{
	SetPosOld(GetPos());
	CModelObj::Update();
	SetPos(GetPos() + Move());

	if (m_pTrail != nullptr)
	{
		m_pTrail->SetTrail(CModelObj::GetMtxWorld(), trailData);
	}

	m_life++;

	// 当たり判定
	CCollision_Rectangle3D *pCollision = GetCollision();

	//当たり判定のクラスを取得できなかったら、中断する
	if (!pCollision)
		return;

	if (pCollision->Collision(OBJTYPE_3DMODEL, true))
	{
		m_life = maxLife;
	}

	if (m_life >= maxLife)
	{
		Uninit();
		return;
	}
}

//=============================================================================
// 描画
// Author : 有田明玄
// 概要 : 描画を行う
//=============================================================================
void CBullet::Draw()
{
	CModelObj::Draw();
}

//=============================================================================
// 移動
// Author : 有田明玄
// 概要 : 移動の処理
//=============================================================================
D3DXVECTOR3 CBullet::Move()
{
	D3DXVECTOR3 move = D3DXVECTOR3(0.f, 0.f, 0.f);

	m_pMove->Moving(m_moveRot);
	move = m_pMove->GetMove();
	//move *= 0.1f;
	return move;
}
