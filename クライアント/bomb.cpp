//=============================================================================
//
// bomb.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "bomb.h"
#include "particle_emitter.h"

//=============================================================================
//								 静的変数の初期化
//=============================================================================
const int CBomb::DEFAULT_BOMB_MODEL = 54;				//ディフォルトのモデルのインデックス
const int CBomb::DEFAULT_BOMB_LIFE = 180;				//ディフォルトの爆弾の寿命

//コンストラクタ
CBomb::CBomb() : m_nLife(0),
m_bDead(false)
{

}

//デストラクタ
CBomb::~CBomb()
{

}

// 初期化
HRESULT CBomb::Init()
{
	//親クラスの初期化処理
	CModelObj::Init();

	SetType(DEFAULT_BOMB_MODEL);		//モデルの設定
	m_nLife = DEFAULT_BOMB_LIFE;		//ディフォルトの寿命の設定

	return S_OK;
}

// 終了
void CBomb::Uninit()
{
	//親クラスの終了処理
	CModelObj::Uninit();
}

// 更新
void CBomb::Update()
{
	//親クラスの更新処理
	CModelObj::Update();

	m_nLife--;
	if (m_nLife <= 0)
	{
		CParticleEmitter* pEmitter = CParticleEmitter::Create("Explosion");
		pEmitter->SetPos(GetPos());
		m_bDead = true;
	}
}

//生成
CBomb * CBomb::Create(const D3DXVECTOR3 pos)
{
	CBomb* pBomb = new CBomb;		//インスタンスを生成する

	//初期化処理
	if (FAILED(pBomb->Init()))
	{
		return nullptr;
	}

	pBomb->SetPos(pos);				//位置の設定

	return pBomb;					//生成したインスタンスを返す
}
