//=============================================================================
//
// UIマネージャークラス(UImanager.cpp)
// Author : 唐�ｱ結斗
// 概要 : UIの生成
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include"UImanager.h"
#include"HackingObj.h"
#include "utility.h"
#include"polygon2D.h"
#include"polygon3D.h"
#include"Score.h"
#include"number.h"
#include"Timer.h"
#include"task.h"
#include "TargetUI.h"
#include "minimap.h"

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : ミニゲームを生成する
//=============================================================================
CUIManager * CUIManager::Create()
{
	// オブジェクトインスタンス
	CUIManager *pMinigame = nullptr;

	// メモリの解放
	pMinigame = new CUIManager;

	// メモリの確保ができなかった
	assert(pMinigame != nullptr);

	// インスタンスを返す
	return pMinigame;
}
//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CUIManager::CUIManager()
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CUIManager::~CUIManager()
{

}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
HRESULT CUIManager::Init()
{
	return S_OK;
}
//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CUIManager::Uninit()
{
}
//=============================================================================
// 更新
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CUIManager::Update()
{
}
//=============================================================================
// 描画
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CUIManager::Draw()
{
}

//=============================================================================
// UI生成
// Author : 有田明玄
// 概要 : UI生成
//=============================================================================
CPolygon2D * CUIManager::LoadUI(EUIType type, D3DXVECTOR3 pos, D3DXVECTOR3 size, int texnumber)
{
	CPolygon2D* pUI = nullptr;
	if (type < UI_TYPE_MAX)
	{
		switch (type)
		{
		case CUIManager::UI_TYPE_UI:
		{
			pUI = CPolygon2D::Create((int)PRIORITY_LEVEL4);
			pUI->SetPos(pos);
			pUI->SetSize(size);
			break;
		}

		case CUIManager::UI_TYPE_TIMER:		//タイマー
		{
			pUI = CTimer::Create((int)PRIORITY_LEVEL4);
			static_cast<CTimer*>(pUI)->CTimer::SetSize(size);
			static_cast<CTimer*>(pUI)->CTimer::SetPos(pos);
			break;
		}
		case CUIManager::UI_TYPE_SCORE:		//スコア
		{
			pUI = CScore::Create((int)PRIORITY_LEVEL4, 3);
			static_cast<CScore*>(pUI)->CScore::SetSize(size);
			static_cast<CScore*>(pUI)->CScore::SetPos(pos);
			break;

		}
		case CUIManager::UI_TYPE_NUMBER:		//数字
		{
			pUI = CNumber::Create();
			pUI->SetPos(pos);
			pUI->SetSize(size);
			break;
		}
		case CUIManager::UI_TYPE_MINIMAP:
		{
			pUI = CMinimap::Create(pos, size);

			break;
		}
		}
		
		if (texnumber >= 0)
		{//マイナスでテクスチャなし
			std::vector<int> pTex;
			pTex.push_back(texnumber);
			pUI->LoadTex(pTex);
		}
	}
	return pUI;
}


//=============================================================================
// 3DUI生成
// Author : 有田明玄
// 概要 : UI生成
//=============================================================================
CPolygon3D * CUIManager::Load3DUI(EUIType type, D3DXVECTOR3 pos, D3DXVECTOR3 size, int texnumber)
{
	CPolygon3D* pUI = nullptr;

	if (type < UI_TYPE_MAX)
	{
		switch (type)
		{
		case CUIManager::UI_TYPE_BILLBOARD:		//数字
		{
			pUI = CTargetUI::Create((int)PRIORITY_LEVEL4);
			pUI->SetBillboard(true);
			pUI->SetZFunc(D3DCMP_ALWAYS);
			break;
		}
		}
		pUI->SetPos(pos);
		pUI->SetSize(size);
		if (texnumber >= 0)
		{//マイナスでテクスチャなし
			std::vector<int> pTex;
			pTex.push_back(texnumber);
			pUI->LoadTex(pTex);
		}
	}
	return pUI;
}
