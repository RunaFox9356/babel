//=============================================================================
//
// selectScreen.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _MAP_SELECT_H_		// このマクロ定義がされてなかったら
#define _MAP_SELECT_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "scene_mode.h"
#include "application.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CMenu;
class CText;
class CPolygon2D;
class CSwipingImage;
class CMenuButton;
class COptionMenu;

class CMapSelect : public CSceneMode
{
public:

	enum BUTTON
	{
		BUTTON_GAME = 0,
		BUTTON_HACKER,
		BUTTON_TITLE,

		BUTTON_MAX
	};
	enum MAP
	{
		MAP_000 = 0,
		MAP_001,
		MAP_002,
		
		MAP_MAX		
	};

public:
	CMapSelect();				//コンストラクタ
	~CMapSelect() override;		//デストラクタ


	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	bool SetMap();
	bool SetMapRecv();	//受け取る
private:

	static const int			MAP_IMAGE_IDX[MAP_MAX];			//マップの画像のテクスチャインデックス
	static const D3DXVECTOR3	DEFAULT_MAP_IMAGE_SPAWN_POS;	//ディフォルトのマップの画像のスポーンの位置
	static const D3DXVECTOR3	DEFAULT_MAP_IMAGE_TARGET_POS;	//ディフォルトのマップの画像の目的の位置
	static const D3DXVECTOR3	DEFAULT_MAP_IMAGE_END_POS;		//ディフォルトのマップの画像の終点
	static const D3DXVECTOR3	DEFAULT_MAP_IMAGE_SIZE;			//ディフォルトのマップの画像のサイズ
	static const D3DXVECTOR3	DEFAULT_MENU_POS;				//ディフォルトのメニューの位置



	void SetTransition(MAP mode);	//遷移の設定
	void AgentUpdate();				// 更新
	void DrawnUpdate();

	bool m_bTransition;				//遷移中であるかどうか
	bool m_isCommand;
	bool m_bTutorial;				//チュートリアル
	int m_Map;
	int m_nSelectedIdx;				//選択されたマップのインデックス
	int m_nDelay;					//ディレイ
	SSendMap m_Receive;
	MAP m_ConnectMode;
	CText*selectText;
	CMenu* m_pMenu;						//メニュー
	CMenuButton*	m_pTutorialButton;	//チュートリアルボタン
	CPolygon2D*		m_pTutorial;		//チュートリアルの画像
	COptionMenu*	m_pOptionMenu;		//オプションメニュー
	CText*			m_option;
	CPolygon2D*		m_optionUI;
	
	CSwipingImage* m_pSwipingImg;		//マップの画像
};


#endif