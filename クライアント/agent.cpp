//=============================================================================
//
// プレイヤークラス(player.cpp)
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "agent.h"
#include "game.h"
#include "mesh.h"
#include "motion.h"
#include "renderer.h"
#include "application.h"
#include "camera.h"
#include "input.h"
#include "calculation.h"
#include "move.h"
#include "debug_proc.h"
#include "parts.h"
#include "sound.h"
#include "scene_mode.h"
#include "collision_rectangle3D.h"
#include "model_obj.h"
#include "application.h"
#include "GimmicDoor.h"
#include "move.h"
#include "enemy.h"
#include "client.h"
#include "model_data.h"
#include "tcp_client.h"
#include "hp_gauge.h"
#include "gimmick.h"
#include "gun.h"
#include "mapDataManager.h"
#include "Score.h"
#include "AgentUI.h"
#include "ranking.h"
#include "emptyObj.h"
#include "model_skin.h"
#include "polygon3D.h"
#include "TargetUI.h"
#include "utility.h"
#include "soundSource.h"
#include "UImessage.h"
#include "AgentUI.h"
#include "Timer.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************

//--------------------------------------------------------------------
// 静的メンバ変数の定義
//--------------------------------------------------------------------
const float			CAgent::fSPEED = 0.5f; 
const float			CAgent::fDASH = fSPEED * 1.5f;
const float			CAgent::fJAMP = 15.0f;
const D3DXVECTOR3	CAgent::BODY_RELATIVE_POS = { 0.0f, 50.0f, 0.0f };				//プレイヤーの体の相対位置
const D3DXVECTOR3	CAgent::HEAD_RELATIVE_POS = { 0.0f, 100.0f, 0.0f };				//プレイヤーの頭の相対位置
const D3DXVECTOR3	CAgent::EYE_RELATIVE_POS = { -7.0f, 90.0f, -30.0f };			//目の相対位置
const int			CAgent::DEFAULT_LIFE = 10;										//ディフォルトのライフ
const int			CAgent::DEFAULT_INVULNERABILITY = 90;							//無敵状態のフレーム数
const D3DXVECTOR3	CAgent::DEFAULT_TARGET_POS = { 0.0f, -50.0f, -1000.0f };		//ターゲットUIの位置
float				CAgent::m_fMouseSensibilityNormal = 1.0f;						//普通のマウス感度
float				CAgent::m_fMouseSensibilityAim = 0.5f;							//狙うモードのマウス感度
const float			CAgent::DEFAULT_SHOOT_SOUND_RADIUS = 1000.0f;		//弾の音の半径


namespace
{
	const int E_BUTTON_TEXTURE_IDX = 44;
	const int Q_BUTTON_TEXTURE_IDX = 46;
};

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CAgent * CAgent::Create()
{
	// オブジェクトインスタンス
	CAgent *pPlayer = nullptr;

	// メモリの解放
	pPlayer = new CAgent();

	// メモリの確保ができなかった
	assert(pPlayer != nullptr);

	// 数値の初期化
	pPlayer->Init();

	// インスタンスを返す
	return pPlayer;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CAgent::CAgent(int nPriority) : CPlayer(nPriority),
m_EState(NEUTRAL_ACTION),
m_fGravity(0.0f),
m_nNumMotion(0),
m_nLife(0),
m_nInvulnerability(0),
m_bDash(false),
m_bJamp(false),
m_bAimMode(false),
m_bStone(false),
m_pActiveGimmick(nullptr),
m_pEyeObj(nullptr),
m_pTargetUi(nullptr),
m_bCarry(false),
m_bhide(false),
m_pReloadUi(nullptr),
m_pUiMessage(nullptr)
{
	SetHavePartsId(3);
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CAgent::~CAgent()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CAgent::Init()
{
	CPlayer::Init();

	// 移動クラスのメモリ確保
	CMove *pMove = GetMove();
	pMove->SetMoving(fSPEED, 1000.0f, 0.1f, 0.1f);
	GetCollision()->SetSize(D3DXVECTOR3(40.0f, 100.0f, 40.0f));
	GetCollision()->SetPos(D3DXVECTOR3(0.0f, 50.0f, 0.0f));

	m_pHpGauge = CHpGauge::Create(CHpGauge::TYPE_2D);
	m_pHpGauge->SetPos(D3DXVECTOR3(1050.0f, 30.0f, 0.0f));
	m_pHpGauge->SetRot(D3DXVECTOR3(0.0f, 0.0f, -D3DX_PI / 2.0f));
	m_pHpGauge->SetMaxSize(D3DXVECTOR3(50.0f, 400.0f, 0.0f));
	m_pHpGauge->SetRatio(0.9f);
	m_pHpGauge->SetMaxNumber((float)(DEFAULT_LIFE));
	//SetHaveItem(CItemObj::ITEM_NON);
	m_nNumHandParts = 3;
	SetBomCommand(false);

	m_nLife = DEFAULT_LIFE;				//ライフの設定

	m_pEyeObj = CEmptyObj::Create();	//からのオブジェクトを生成する


	SetSkin( CSkinMesh::Create("data/MODEL/skin/default.x"));
	SetMtxItemParent(GetSkin()->GetpBoneMatrix("NewJoint21_Joint"));
	m_ESkinState = MOVE_SKIN;


	m_pTargetUi = /*CPolygon3D::Create(CSuper::PRIORITY_LEVEL3)*/CTargetUI::Create(CSuper::PRIORITY_LEVEL3);

	if (m_pTargetUi)
	{
		m_pTargetUi->SetPos(DEFAULT_TARGET_POS);
		m_pTargetUi->LoadTex(41);
		m_pTargetUi->SetBillboard(true);
		m_pTargetUi->SetDraw(false);
		m_pTargetUi->SetZFunc(D3DCMP_ALWAYS);
	}
	CModelData* Model = CApplication::GetInstance()->GetPlayer(1);
	Model->GetPlayerData()->Player.m_MyId = P2;

	CreateSound(CSound::SOUND_LABEL_SE_PLAYER_STEP, 45);

	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CAgent::Uninit()
{
	
	if (m_bAimMode)
	{//狙うモードだったら

		//カメラ情報を取得する
		CCamera* pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
		{//nullチェック

			//マウス感度を元に戻す
			pCamera->SetMouseSensibility(m_fMouseSensibilityNormal);
		}
	}

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_AGENT || CApplication::GetInstance()->GetMode() == CApplication::MODE_GAME)
	{
		CRanking* pRanking = CApplication::GetInstance()->GetRanking();
		CAgentUI* pUi = CGame::GetUiManager();

		if (pRanking && pUi)
		{
			pRanking->GetRank(pUi->GetScore()->GetScore());
		}
	}

	//ターゲットのUIの破棄
	if (m_pTargetUi)
	{
		m_pTargetUi->Uninit();
		m_pTargetUi = nullptr;
	}

	//UIメッセージの破棄
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
	if (m_pReloadUi)
	{
		m_pReloadUi->Uninit();
		m_pReloadUi = nullptr;
	}
	
	//最後にデータを削除させる。
	CAgentUI* pUiManager = CGame::GetUiManager();
	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);
	CModelData::SSendPack data;
	data = Model->GetPlayerData()->Player;
	data.m_PlayData.m_pos = GetPos();
	data.m_PlayData.m_rot = GetRotDest();
	data.m_PlayData.m_haveItemLeftId = 0;
	data.m_PlayData.m_haveItemRightId = 0;
	data.m_PlayData.m_motion = GetMotion()->GetNumMotion();
	data.m_log = Model->GetPlayerData()->Player.m_log + 1;
	data.m_PlayData.m_pushBomComands = GetBomCommand();
	data.m_IsGame = false;
	data.m_MyId = P1;

	if (pUiManager)
	{
		data.m_addscore = pUiManager->GetScore()->GetSendScore();
	}

	Model->GetPlayerData()->SetPlayerCast((const char*)&data);
	CApplication::GetInstance()->GetClient()->Send((const char*)&data, sizeof(CModelData::SSendPack), CClient::TYPE_UDP);

	data.m_IsGame = false;
	CPlayer::Uninit();
	//要らなくなったポインタをnullに戻す
	if (m_pActiveGimmick)
	{
		m_pActiveGimmick = nullptr;
	}
	if (m_pEyeObj)
	{
		m_pEyeObj = nullptr;
	}
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CAgent::Update()
{
	SetNumMotionOld(GetMotion()->GetNumMotion());
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	// 過去位置の更新
	SetPosOld(GetPos());

	// キーボード入力
	KeySet();

	// 位置の取得
	D3DXVECTOR3 pos = GetPos();

	if (m_bJamp)
	{// ジャンプ
		Jamp();
	}

	// 弾の発射
	Shot();

	// 重力の設定
	m_fGravity -= CCalculation::Gravity();

	pos.y += m_fGravity;

	// 位置の設定
	SetPos(pos);

	// プレイヤークラスの更新
	CPlayer::Update();

	// 位置の取得
	pos = GetPos();

	// モーションの切り替え
	MotionSetting();

	int nMap = CApplication::GetInstance()->GetMap();

	if (CApplication::GetInstance()->GetMapDataManager()->GetMapData(nMap).vMeshData[0].pMesh->MeshtoSelfCollison(this))
	{
		Landing();
	}

	//当たり判定
	CollisionCheck(pInput);
	
	CAgentUI* pUiManager = CGame::GetUiManager();

	if (m_bAimMode)
	{
		//目の位置を計算する
		CalcEyePos();

		CCamera* pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
		{
			D3DXVECTOR3 rot = pCamera->GetRot();
			rot.y -= D3DX_PI;

			if (rot.y >= D3DX_PI)
				rot.y += -2.0f * D3DX_PI;
			else if(rot.y < -D3DX_PI)
				rot.y += +2.0f * D3DX_PI;

			SetRotDest(rot);
		}

		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		if (pMotion)
		{
			if (pMotion->GetAnimEnd())
			{
				pMotion->StopMotion(true);
			}

			for (int nCnt = 0; nCnt < 2; nCnt++)
			{
				CParts* pArm = pMotion->GetParts((nCnt * 2) + 2);

				if (pArm)
				{
					D3DXVECTOR3 rot = pArm->GetRot();
					rot.x = pCamera->GetRot().x;

					pArm->SetRot(rot);
				}
			}
		}
	}
	
	//オンラインの同期をデータパックにまとめる。
	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);
	CModelData::SSendPack data;
	data = Model->GetPlayerData()->Player;
	data.m_PlayData.m_pos = GetPos();
	data.m_PlayData.m_rot = GetRotDest();
	data.m_PlayData.m_haveItemLeftId = GetMyHaveId();
	data.m_PlayData.m_motion = m_ESkinState;
	data.m_log = Model->GetPlayerData()->Player.m_log+1;
	data.m_PlayData.m_Life = m_nLife;
	if (CGame::GetUiManager() != nullptr)
	{
		data.m_PlayData.m_Timer = CGame::GetUiManager()->GetTimer()->GetTimer();
	}
	else
	{
		data.m_PlayData.m_Timer = 0;
	}
	if (Model->GetPlayerData()->Player.m_log + 1 >= 9999999999)
	{
		Model->GetPlayerData()->Player.m_log = 0;
	} 
	data.m_PlayData.m_pushBomComands = GetBomCommand();
	data.m_IsGame = true;
	data.m_MyId = P1;
	if (pUiManager)
	{
		data.m_addscore = pUiManager->GetScore()->GetSendScore();
		data.m_score = pUiManager->GetScore()->GetScore();
	}

	Model->GetPlayerData()->SetPlayerCast((const char*)&data);
	CApplication::GetInstance()->GetClient()->Send((const char*)&data, sizeof(CModelData::SSendPack), CClient::TYPE_UDP);


	GetSkin()->ChangeAnim(m_ESkinState);

	// アイテムの位置と向きの設定
	//SetItemOffset();
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CAgent::Draw()
{
	/*if (m_nInvulnerability % 10 <= 5)
		CPlayer::Draw();*/
}

//使っているギミックの設定
void CAgent::SetGimmick(CGimmick * pGimmick)
{
	if (!m_pActiveGimmick)
		m_pActiveGimmick = pGimmick;
}

//プレイヤーの体の相対位置
const D3DXVECTOR3 CAgent::GetPlayerBody()
{
	return GetPos() + BODY_RELATIVE_POS;
}

//プレイヤーの頭の相対位置
const D3DXVECTOR3 CAgent::GetPlayerHead()
{
	return GetPos() + HEAD_RELATIVE_POS;
}

//石を拾う
const bool CAgent::PickUpStone()
{
	if (m_bStone)
		return false;

	m_bStone = true;

	return true;
}

//石を投げる
const bool CAgent::ThrowStone()
{
	if (m_bStone)
	{
		m_bStone = false;
		return true;
	}

	return false;
}

//残数弾の取得
int CAgent::GetLeftBullet()
{
	int nBulletNum = 0;

	if (GetMyItem() && GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
	{
		CGun* pGun = nullptr;

		pGun = dynamic_cast<CGun*>(GetMyItem());

		if (pGun)
		{
			nBulletNum = pGun->GetLeftBullet();
		}
	}


	return nBulletNum;
}


void CAgent::SetSkinAction(ACTION_STATE_SKIN state)
{
	m_ESkinState = state;

	GetSkin()->ChangeAnim(m_ESkinState);
}

//=============================================================================
// モーションのセッティング
// Author : 唐�ｱ結斗
// 概要 : モーションの切り替えを行う
//=============================================================================
void CAgent::MotionSetting()
{
	// モーション情報の取得
	CMotion *pMotion = CMotionModel3D::GetMotion();

	// 移動情報
	CMove *pMove = GetMove();
	float fLength = pMove->GetMoveLength();

	//サウンドの取得
	CSoundSource* pSoundSource = GetSoundSource();

	if (pMotion != nullptr
		&& !pMotion->GetMotion()
		&& !m_bJamp)
	{// ニュートラルモーションの設定
		m_EState = NEUTRAL_ACTION;
	
		pMotion->SetNumMotion(m_EState);

		if (pSoundSource)
		{
			pSoundSource->SetUpdatingFlag(false);
		}
	}

	if (fLength > 0.0f
		&& (m_EState == NEUTRAL_ACTION
			|| m_EState == MOVE_ACTION
			|| m_EState == DASH_ACTION))
	{
		if (m_bDash
			&& (m_EState == NEUTRAL_ACTION
				|| m_EState == MOVE_ACTION))
		{// ダッシュモーション
			m_EState = DASH_ACTION;
			m_ESkinState = DASH_SKIN;
			pMotion->SetNumMotion(m_EState);

			if (pSoundSource)
			{
				pSoundSource->SetUpdatingFlag(true);
				pSoundSource->SetDelay(25);
			}
		}
		else if(!m_bDash
			&& (m_EState == NEUTRAL_ACTION
				|| m_EState == DASH_ACTION))
		{// 歩きモーション
			m_EState = MOVE_ACTION;
			pMotion->SetNumMotion(m_EState);

			if (pSoundSource)
			{
				pSoundSource->SetUpdatingFlag(true);
				pSoundSource->SetDelay(45);
			}
		}
	}
	else if (fLength <= 0.0f
		&& (m_EState == MOVE_ACTION
			|| m_EState == DASH_ACTION))
	{
		m_EState = NEUTRAL_ACTION;
		pMotion->SetNumMotion(m_EState);
		m_ESkinState = MOVE_SKIN;
		if (GetMyItem() != nullptr
			&& GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
		{
			m_ESkinState = GAN_SKIN;
		}

		if (pSoundSource)
		{
			pSoundSource->SetUpdatingFlag(false);
		}
	}
	if (m_bCarry)
	{
		// 正規化
		D3DXVECTOR3 myVec = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 vec = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

		// 移動距離の設定
		float fLengthSprtf = sqrtf((myVec.x * myVec.x) + (myVec.y * myVec.y) + (myVec.z * myVec.z));
		float fmove= pMove->GetMoveLength();
		m_ESkinState = HOLD_SKIN;
		if (fmove>0.0f)
		{// 歩きモーション
			m_ESkinState = HOLDMOVE_SKIN;
		}
		m_pCarryEnemy->SetPos(GetPos()+(myVec * fLengthSprtf));
		m_pCarryEnemy->SetRot(D3DXVECTOR3(D3DX_PI*0.5f, GetRot().y,0.0f));
	}
}

//=============================================================================
// 高速移動
// Author : 唐�ｱ結斗
// 概要 : 移動速度を上げる
//=============================================================================
void CAgent::Dash()
{
	if (!m_bJamp && !m_bAimMode)
	{// 移動クラスのメモリ確保
		CMove *pMove = GetMove();

		pMove->SetMoving(fDASH, 1000.0f, 0.1f, 0.1f);
		m_bDash = true;
	}
}

//=============================================================================
// 高速移動の解除
// Author : 唐�ｱ結斗
// 概要 : 移動速度を元に戻す
//=============================================================================
void CAgent::CancelDash()
{
	// 移動クラスのメモリ確保
	CMove *pMove = GetMove();
	pMove->SetMoving(fSPEED, 1000.0f, 0.1f, 0.1f);
	m_bDash = false;
}

//=============================================================================
// しゃがむ
// Author : 唐�ｱ結斗
// 概要 : しゃがみ状態になる
//=============================================================================
void CAgent::Squat()
{

}

//=============================================================================
// ジャンプ
// Author : 唐�ｱ結斗
// 概要 : ジャンプする
//=============================================================================
void CAgent::Jamp()
{
	if (m_EState != JAMP_ACTION
		&& m_fGravity == 0.0f)
	{// 移動情報の計算
		CMove *pMove = GetMove();
		D3DXVECTOR3 move = pMove->GetMove();
		move.y = 0.0f;
		m_fGravity = 0.0f;
		pMove->SetMove(move);
		pMove->SetSpeed(fJAMP);
		pMove->Moving(D3DXVECTOR3(0.0f, 1.0f, 0.0f));
		float fSpeed = fSPEED;

		if (m_bDash)
		{
			fSpeed = fDASH;
		}

		pMove->SetSpeed(fSpeed);
		m_bJamp = true;

		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		if (pMotion != nullptr)
		{
			m_EState = JAMP_ACTION;
			pMotion->SetNumMotion(m_EState);
		}
	}
}

//=============================================================================
// 弾の発射
// Author : 唐�ｱ結斗
// 概要 : 弾を発射する
//=============================================================================
void CAgent::Shot()
{
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	D3DXVECTOR3 dir = pCamera->GetPosR() - pCamera->GetPosV();
	D3DXVec3Normalize(&dir, &dir);

	if (GetMyItem() != nullptr
		&& GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
	{
		CGun *pGun = (CGun*)GetMyItem();
		pGun->SetBulletMove(dir);

		if (pInput->Trigger(MOUSE_INPUT_LEFT) && !GetHiddenState())
		{
			pGun->SetShot(true);
			AllertEnemies();
		}
		else if (pInput->Release(MOUSE_INPUT_LEFT) && !GetHiddenState())
		{
			pGun->SetShot(false);
		}

		if (pInput->Trigger(DIK_R))
		{
			pGun->SetSound(true);
			pGun->SetReload(true);
		}
	}
}

//=============================================================================
// 着地
// Author : 唐�ｱ結斗
// 概要 : 着地モーションを行い、重力加速度の値
//=============================================================================
void CAgent::Landing()
{
	m_fGravity = 0.0f;
	m_bJamp = false;

	// モーション情報の取得
	CMotion *pMotion = CMotionModel3D::GetMotion();

	if (m_EState == JAMP_ACTION)
	{
		m_EState = LANDING_ACTION;
		pMotion->SetNumMotion(m_EState);
	}
}

//=============================================================================
// キーボード入力
// Author : 唐�ｱ結斗
// 概要 : キーボードから数値を入力する
//=============================================================================
void CAgent::KeySet()
{
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();
	
	//bool bHidden = GetHiddenState();

	if (!GetStopState())
	{
		if (pInput->Trigger(DIK_SPACE))
		{
			Jamp();
		}

		if (pInput->Trigger(DIK_LSHIFT)&& m_bCarry==false)
		{
			Dash();
		}
		else if (pInput->Release(DIK_LSHIFT) && m_bCarry == false)
		{
			CancelDash();
		}
	}
	else
	{
		if (pInput->Trigger(DIK_Q))
		{
			if (m_pActiveGimmick)
			{
				m_pActiveGimmick->SetInteraction(false);
				SetHiddenState(false);
			}
			else
			{
				SetHiddenState(false);
				SetStopState(false);
			}
			// モーション情報の取得
			CMotion *pMotion = CMotionModel3D::GetMotion();

			if (pMotion)
			{
				m_EState = CAgent::ACTION_STATE::MOVE_ACTION;
				pMotion->SetNumMotion(m_EState);
			}
		}
	}

	if (pInput->Trigger(DIK_B))
	{
		m_pHpGauge->SetNumber(2.0f);
	}

	if (pInput->Trigger(MOUSE_INPUT_RIGHT)&& !GetHiddenState())
	{
		SwitchAimMode();
	}

	int nWheel = pInput->GetMouseWheel();

	if (nWheel != 0)
	{
		int nCurrentItem = GetCurrentItem();

		if (nWheel >= 0)
		{
			nCurrentItem--;

			if (nCurrentItem <= -1)
			{
				nCurrentItem = nMaxItem - 1;
			}
		}
		else if (nWheel <= 0)
		{
			nCurrentItem++;

			if (nCurrentItem >= nMaxItem)
			{
				nCurrentItem = 0;
			}
		}

		SetCurrentItem(nCurrentItem);
	}

#ifdef _DEBUG
	if (pInput->Trigger(DIK_L))
	{
		GetSkin()->SetStopFlag(true);
	}
	else if (pInput->Trigger(DIK_K))
	{
		GetSkin()->SetStopFlag(false);
	}
#endif // _DEBUG

}

// 当たり判定
void CAgent::CollisionCheck(CInput* pInput)
{
	// 当たり判定
	CCollision_Rectangle3D *pCollision = GetCollision();

	//当たり判定のクラスを取得できなかったら、中断する
	if (!pCollision)
		return;

	//if (pCollision->Collision(OBJTYPE_TEST, false))
	//{
	//	std::vector<CObject*> pCollidedObj = pCollision->GetCollidedObj();
	//	int nSize = pCollidedObj.size();

	//	for (int nCntObj = 0; nCntObj < nSize; nCntObj++)
	//	{
	//		CModelObj *pModelObj = (CModelObj*)pCollidedObj.at(nCntObj);
	//		CModel3D *pModel = pModelObj->GetModel();
	//		D3DXVECTOR3 p = GetPos();

	//		static bool bLanding = false;

	//		if (bLanding)
	//		{
	//			p.y = GetPosOld().y;
	//			SetPos(p);
	//			m_fGravity = 0.0f;
	//		}

	//		CDebugProc::Print("Gravity: %f", m_fGravity);

	//		bLanding = false;

	//		//bool bCollision = pModel->Collison(this, D3DXVECTOR3(0.0f, 0.2f, 0.0f), bLanding);
	//		bool bCollision = CModel3D::SearchCollison(this, 300.0f, D3DXVECTOR3(0.0f, 0.2f, 0.0f), bLanding);

	//		if (bCollision && bLanding)
	//		{
	//			Landing();
	//		}

	//		/*pModel->Collison(this, BODY_RELATIVE_POS, bCollision);*/
	//		CModel3D::SearchCollison(this, 300.0f, BODY_RELATIVE_POS, bLanding);
	//		//pModel->Collison(this, HEAD_RELATIVE_POS, bCollision);
	//		CModel3D::SearchCollison(this, 300.0f, HEAD_RELATIVE_POS, bLanding);

	//		D3DXVECTOR3 n = GetPos() - p;
	//		D3DXVec3Normalize(&n, &n);
	//		n *= 0.5f;
	//		SetPosOld(GetPos() + n);
	//	}
	//}

	if (pCollision->Collision(OBJTYPE_3DMODEL, true)
		&& GetPosOld().y == GetPos().y)
	{
		if (!pCollision->GetPlusMinus())
		{
			CMove *pMove = GetMove();
			pMove->SetMoveVec(D3DXVECTOR3(0.f, 0.f, 0.f));
		}

		Landing();
	}

	{
		D3DXVECTOR3 p = GetPos();
		bool bLanding = false;

		bool bCollision = CModel3D::SearchCollison(this, 10.0f, D3DXVECTOR3(0.0f, 0.2f, 0.0f), bLanding);

		if (bCollision && bLanding)
		{
			Landing();
		}

		CModel3D::SearchCollison(this, 10.0f, BODY_RELATIVE_POS, bLanding);
		CModel3D::SearchCollison(this, 10.0f, HEAD_RELATIVE_POS, bLanding);

		D3DXVECTOR3 n = GetPos() - p;
		D3DXVec3Normalize(&n, &n);
		n *= 0.5f;
		SetPosOld(GetPos() + n);
	}

	//弾の当たり判定
	if (m_nInvulnerability <= 0 && pCollision->Collision(CObject::OBJTYPE_ENEMY_BULLET, false) && m_nLife > 0)
	{//無敵状態ではなく、弾と当たった場合

		m_nLife--;			//ライフの更新

		if (m_pHpGauge)
		{
			m_pHpGauge->SetNumber((float)m_nLife);		//HPゲージの更新


		}


		if (m_nLife <= 0 && CApplication::GetInstance()->GetMode() != CApplication::MODE_AGENT_TUTORIAL)
		{//ライフが0になったら、画面遷移

			CApplication::GetInstance()->SetNextMode(CApplication::MODE_RESULT);
		}
		else
		{//ライフがまだ残る場合、無敵状態にする

			m_nInvulnerability = DEFAULT_INVULNERABILITY;
		}
	}
	else if (m_nInvulnerability > 0)
	{//無敵状態カウンターが0より大きければ

		m_nInvulnerability--;		//無敵状態カウンターの更新
	}

	if (pCollision->Collision(OBJETYPE_GIMMICK, true))
	{
		std::vector<CObject*> pCollidedObj = GetCollision()->GetCollidedObj();
		CGimmicDoor *pDoor = (CGimmicDoor*)pCollidedObj.at(0);
		pDoor->SetMove(D3DXVECTOR3(0.f, 0.4f, 0.f));
	}

	if (!GetMyItem() && pCollision->Collision(OBJETYPE_GUN, false) && !m_bAimMode)
	{//銃の辺り判定
		std::vector<CObject*> pCollidedObj = pCollision->GetCollidedObj();

		CGun *pGun = nullptr;

		for (int nCnt = 0; nCnt < (int)pCollidedObj.size(); nCnt++)
		{
			pGun = (CGun*)pCollidedObj.at(nCnt);

			if (pGun != nullptr)
			{//あったら抜ける
				break;
			}
		}

		if (pInput && pInput->Trigger(DIK_E))
		{//持つ判定
			pGun->SetBulletType(CObject::OBJTYPE_PLAYER_BULLET);
			pGun->SetHeldFlag(true);
			SetHaveItem(pGun);
			DestroyUiMessage(TYPE_INTERACTION);
		}
	}

	if (GetMyItem() && GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN && !m_pReloadUi)
	{
		CreateUiMessage(TYPE_RELOAD, "Reload", 117);
	}
	else if (!GetMyItem() && m_pReloadUi)
	{
		DestroyUiMessage(TYPE_RELOAD);
	}
	
	if (m_bCarry)
	{
		//if (pInput && pInput->Trigger(DIK_Q))
		{//離れる判定

			if (m_pUiMessage)
			{
				DestroyUiMessage(TYPE_INTERACTION);
			}

			m_bCarry = false;
			m_pCarryEnemy = nullptr;
			m_ESkinState = MOVE_SKIN;
		}
	}
	else if (pCollision->Collision(CObject::OBJETYPE_ENEMY, true))
	{
		std::vector<CObject*> pCollidedObj = pCollision->GetCollidedObj();

		CEnemy* pEnemy = nullptr;

		for (int nCnt = 0; nCnt < (int)pCollidedObj.size(); nCnt++)
		{
			pEnemy = (CEnemy*)pCollidedObj.at(nCnt);

			if (pEnemy != nullptr && !pEnemy->GetStun())
			{//あったら抜ける
				break;
			}
			else
				pEnemy = nullptr;
		}

		if (pEnemy && pEnemy->GetSecurityLevel() <= 0 && !pEnemy->GetDeath())
		{
			if (!m_pUiMessage)
				CreateUiMessage(TYPE_INTERACTION, "Stun", Q_BUTTON_TEXTURE_IDX);

			if (pInput && pInput->Trigger(DIK_Q))
			{//持つ判定
				if (m_bCarry == false)
				{
					pEnemy->Stun();
					//if (pEnemy->GetDeath() == false)
					{
						GetSkin()->SetRot(GetRot() - D3DXVECTOR3(0, (D3DX_PI), 0));
						// モーション情報の取得
						m_pCarryEnemy = pEnemy;
						m_bCarry = true;

						CreateUiMessage(TYPE_INTERACTION, "Release", Q_BUTTON_TEXTURE_IDX);
					}
				}
			}
		}

	}
	else if (m_pUiMessage)
		DestroyUiMessage(TYPE_INTERACTION);
	
	//if (pCollision->Collision(OBJTYPE_GIMMICKKEY, false)
	//	&& GetMyItemKey() == nullptr)
	//{//鍵を探す判定
	//	std::vector<CObject*> pCollidedObj = pCollision->GetCollidedObj();

	//	CItemObj *pItemObj = nullptr;

	//	for (int nCnt = 0; nCnt < (int)pCollidedObj.size(); nCnt++)
	//	{
	//		pItemObj = (CItemObj*)pCollidedObj.at(nCnt);

	//		if (pItemObj != nullptr)
	//		{//鍵あったら抜ける
	//			break;
	//		}
	//	}

	//	if (pInput && pInput->Trigger(DIK_E))
	//	{//鍵を持つ判定
	//		SetHaveItemKey(pItemObj->GetId());
	//		GetItemKey();
	//	}
	//}
	//else if (GetMyItemKey() != nullptr)
	//{
	//	if (pInput && pInput->Trigger(DIK_E))
	//	{//鍵をすてる
	//		SetHaveItemKey(0);
	//	}
	//}
}

// 狙うモードを切り替える
void CAgent::SwitchAimMode()
{
	if (!GetMyItem())
		return;

	if (GetMyItem()->GetObjType() != CObject::OBJETYPE_GUN)
		return;

	//狙うモードを切り替える
	m_bAimMode = !m_bAimMode;
	SetRotationFlag(!m_bAimMode);

	//カメラ情報の取得
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();

	if (m_bAimMode)
	{
		pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));			//カメラの視点のオフセットを設定する
		pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));			//カメラの注視点のオフセットを設定する
		pCamera->SetPitchRotLimits(D3DX_PI * 0.25f, D3DX_PI * -0.45f);		//カメラのX軸の回転の最大と最小角度を変える

		if (GetMyItem() && GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
		{
			GetMyItem()->SetDraw(false);
		}

		if (m_pEyeObj)
			pCamera->SetFollowTarget(m_pEyeObj, 1.0f);

		// 移動速度を下げる
		CMove *pMove = GetMove();
		pMove->SetMoving(fSPEED * 0.4f, 1000.0f, 0.1f, 0.1f);

		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		if (pMotion)
		{
			m_EState = CAgent::ACTION_STATE::AIM_ACTION;
			pMotion->SetNumMotion(m_EState);
			m_ESkinState = GAN_SKIN;
			GetSkin()->ChangeAnim(m_ESkinState);

			for (int nCnt = 0; nCnt < 2; nCnt++)
			{
				CParts* pArm = pMotion->GetParts((nCnt * 2) + 2);

				if (pArm)
				{
					D3DXVECTOR3 rot = pArm->GetRot();
					rot.x = pCamera->GetRot().x;

					pArm->SetRot(rot);
				}
			}
		}

		if (m_pTargetUi && GetMyItem())
		{
			m_pTargetUi->SetDraw(true);
		}

		D3DXVECTOR3 rot = GetRotDest();
		rot.y = pCamera->GetRot().y;
		SetRotDest(rot);

		//マウス感度を半分にする
		pCamera->SetMouseSensibility(m_fMouseSensibilityAim);
	}
	else
	{
		pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));			//カメラの視点のオフセットを設定する
		pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));			//カメラの注視点のオフセットを設定する
		pCamera->SetPitchRotLimits(D3DX_PI * 0.25f, 0.0f);					//カメラのX軸の回転の最大と最小角度を元に戻す
		pCamera->SetFollowTarget(this, 1.0f);							

		if (GetMyItem() && GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
		{
			GetMyItem()->SetDraw(true);
		}

		// 移動速度を元に戻す
		CMove *pMove = GetMove();
		pMove->SetMoving(fSPEED, 1000.0f, 0.1f, 0.1f);

		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		if (pMotion)
		{
			m_EState = CAgent::ACTION_STATE::NEUTRAL_ACTION;
			pMotion->SetNumMotion(m_EState);
			pMotion->StopMotion(false);

			for (int nCnt = 0; nCnt < 2; nCnt++)
			{
				CParts* pArm = pMotion->GetParts((nCnt * 2) + 2);

				if (pArm)
				{
					D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

					pArm->SetRot(rot);
				}
			}
		}

		if (m_pTargetUi && GetMyItem())
		{
			m_pTargetUi->SetDraw(false);
		}

		//マウス感度を元に戻す
		pCamera->SetMouseSensibility(m_fMouseSensibilityNormal);
	}
}

// 目の位置の計算処理
void CAgent::CalcEyePos()
{
	if (m_pEyeObj)
	{
		//計算用のマトリックス
		D3DXMATRIX mtxOut, mtxRot, mtxTrans;

		//プレイヤーの位置と向きを取得し、出力用の変数を初期化する
		D3DXVECTOR3 pos = GetPos(), rot = GetRot(), p = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		//マトリックスの初期化
		D3DXMatrixIdentity(&mtxOut);

		//向きを反映する
		D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxRot);

		//位置を反映する
		D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxTrans);

		//目の絶対位置を計算し、設定する
		D3DXVec3TransformCoord(&p, &EYE_RELATIVE_POS, &mtxOut);
		m_pEyeObj->SetPos(p);

		if (m_pTargetUi && GetMyItem() && GetMyItem()->GetObjType() == CObject::OBJETYPE_GUN)
		{
			CCamera *pCamera = CApplication::GetInstance()->GetCamera();
			D3DXVECTOR3 dir = pCamera->GetPosR() - pCamera->GetPosV();
			D3DXVec3Normalize(&dir, &dir);

			dir *= 1000.0f;

			CGun* pObj = (CGun*)GetMyItem();

			if (pObj)
			{
				// 弾の発射を行う
				D3DXMATRIX mtxWorld = pObj->GetMtxWorld();
				D3DXVECTOR3 posBullet = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
				p = pObj->GetPos();

				D3DXMatrixIdentity(&mtxOut);
				D3DXMatrixTranslation(&mtxOut, CGun::DEFAULT_BULLET_SPAWN.x, CGun::DEFAULT_BULLET_SPAWN.y, CGun::DEFAULT_BULLET_SPAWN.z);
				D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxWorld);

				D3DXVec3TransformCoord(&posBullet, &CGun::DEFAULT_BULLET_SPAWN, &mtxOut);

				posBullet += dir;

				m_pTargetUi->SetPos(posBullet);

			}
		}
	}
}

// アイテムの位置と向きの設定
void CAgent::SetItemOffset()
{
	CItemObj* pMyItem = GetMyItem();

	if (pMyItem == nullptr)
	{
		return;
	}

	D3DXVECTOR3 posOffset = D3DXVECTOR3(0.f, 0.f, 0.f);
	D3DXVECTOR3 rotOffset = D3DXVECTOR3(0.f, 0.f, 0.f);

	switch (pMyItem->GetObjType())
	{
	case CObject::OBJETYPE_GUN:
		posOffset = D3DXVECTOR3(3.0f, 0.f, 0.f);
		rotOffset = D3DXVECTOR3(D3DX_PI * 0.4f, -D3DX_PI * 1.1f, 0.f);
		break;
	default:
		break;
	}

	pMyItem->SetPosOffset(posOffset);
	pMyItem->SetRotOffset(rotOffset);
}

// 弾をうつと周りの敵のが聞こえるようにする
void CAgent::AllertEnemies()
{
	if (m_nSecurityLevel < 1)
		return;

	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (pManager)
	{
		CMapDataManager::MAP_DATA MapData = pManager->GetMapData(CApplication::GetInstance()->GetMap());

		for (int nCnt = 0; nCnt < (int)MapData.vEnemyData.size(); nCnt++)
		{
			CEnemy* pEnemy = MapData.vEnemyData.data()[nCnt].pEnemy;

			if (pEnemy)
			{
				D3DXVECTOR3 dist = pEnemy->GetPos() - GetPos();
				float fDist = D3DXVec3Length(&dist);

				if (fDist < DEFAULT_SHOOT_SOUND_RADIUS)
				{
					pEnemy->SetSoundPos(GetPos());
				}
			}
		}
	}
}

//UIメッセージの生成処理
void CAgent::CreateUiMessage(UiMessageType type, const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage(type);

	if (type == TYPE_INTERACTION)
		m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 360.0f, 0.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
	else if(type == TYPE_RELOAD)
		m_pReloadUi = CUiMessage::Create(D3DXVECTOR3(1100.0f, 690.0f, 0.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f), nNum, "Reload", D3DXCOLOR(0.75f, 1.0f, 0.75f, 1.0f));
}

//UIメッセージの破棄処理
void CAgent::DestroyUiMessage(UiMessageType type)
{
	if (type == TYPE_INTERACTION && m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
	else if (type == TYPE_RELOAD && m_pReloadUi)
	{
		m_pReloadUi->Uninit();
		m_pReloadUi = nullptr;
	}
}
