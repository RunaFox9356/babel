//=============================================================================
//
// carAnimation.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "carAnimation.h"
#include "model_obj.h"
#include "game.h"
#include "hacker.h"
#include "player.h"
#include "application.h"
#include "agent.h"
#include "camera.h"
#include "drone.h"


//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			CCarAnimation::DEFAULT_MODEL_IDX = 213;						//ディフォルトのモデルインデックス
const D3DXVECTOR3	CCarAnimation::DEFAULT_POS = { 1000.0f, 0.0f, -50.0f };		//ディフォルトの位置
const D3DXVECTOR3	CCarAnimation::DEFAULT_SPEED = { -10.0f, 0.0f, 0.0f };		//ディフォルトの速度



//コンストラクタ
CCarAnimation::CCarAnimation() : m_speed(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_lastDist(0.0f),
m_nDelay(0),
m_bStop(false),
m_pModel(nullptr),
m_animState((EAnimState)0)
{

}

//デストラクタ
CCarAnimation::~CCarAnimation()
{

}

//初期化
HRESULT CCarAnimation::Init()
{
	//親クラスの初期化処理
	if (FAILED(CEndAnimation::Init()))
		return E_FAIL;

	m_animState = STATE_DRONE_OUT;			//現在の状態を設定する
	m_lastDist = 1000000.0f;			//前のフレームの距離を設定する

	return S_OK;
}

//終了
void CCarAnimation::Uninit()
{
	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (pCamera)
	{
		pCamera->BlockCamera(false);
	}

	//親クラスの終了処理
	CEndAnimation::Uninit();
}

//更新
void CCarAnimation::Update()
{
	if (!m_pModel)
		return;

	UpdateState();
}

//生成処理
CCarAnimation * CCarAnimation::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	CCarAnimation* pObj = new CCarAnimation;			//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);			//位置の設定
	pObj->SetRot(rot);			//向きの設定
	pObj->CreateModel();		//モデルを生成し、速度を計算する

	return pObj;				//生成したインスタンスを返
}






//速度の計算処理
void CCarAnimation::CreateModel()
{
	D3DXVECTOR3 pos = GetPos(), rot = GetRot();
	D3DXMATRIX mtxOut, mtxRot, mtxTrans;

	D3DXMatrixIdentity(&mtxOut);

	D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
	D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxRot);

	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxTrans);

	D3DXVec3TransformCoord(&m_speed, &DEFAULT_SPEED, &mtxRot);
	D3DXVec3TransformCoord(&pos, &DEFAULT_POS, &mtxOut);

	m_pModel = CModelObj::Create();

	if (m_pModel)
	{
		m_pModel->SetPos(pos);
		m_pModel->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
		m_pModel->SetType(DEFAULT_MODEL_IDX);
		m_pModel->SetIgnoreDistance(true);
	}

	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (pCamera)
	{
		D3DXVECTOR3 posV = D3DXVECTOR3(0.0f, 40.0f, -150.0f);
		D3DXVec3TransformCoord(&posV, &posV, &mtxOut);

		pCamera->SetFollowTarget(false);
		pCamera->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		pCamera->SetPosVOffset(posV);
		//pCamera->SetTargetPosR(m_pModel);
		pCamera->BlockCamera(true);
		m_posV = posV;
		//pCamera->SetPosV(posV);
		//pCamera->SetPosR(m_pModel->GetPos());


		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
		CPlayer* pDrone = nullptr;

		if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT)
		{
			pDrone = CGame::GetDrone();

			if (pDrone)
			{
				CDrone* pDr = dynamic_cast<CDrone*>(pDrone);

				if (pDr)
					pDr->SetControl(false);
			}
			if (CGame::GetPlayer())
			{
				CGame::GetPlayer()->SetStopState(true);
				CGame::GetPlayer()->SetInvulnerability(1800);
			}
		}
		else if (mode == CApplication::MODE_HACKER)
		{
			pDrone = CHacker::GetDrone();
			CHacker::GetDrone()->SetControl(false);
		}

		if (!pDrone)
			return;

		pCamera->SetPosR(pDrone->GetPos());
		pCamera->SetPosV(pDrone->GetPos() + D3DXVECTOR3(0.0f, 60.0f, -200.0f));
	}
}

//状態の更新処理
void CCarAnimation::UpdateState()
{
	D3DXVECTOR3 pos = m_pModel->GetPos();		//モデルの位置の設定

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	CPlayer* pPlayer = nullptr;
	CPlayer* pDrone = nullptr;

	if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT)
	{
		pPlayer = CGame::GetPlayer();
		pDrone = CGame::GetDrone();
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pPlayer = CHacker::GetPlayer();
		pDrone = CHacker::GetDrone();
	}

	if (!pPlayer || !pDrone)
		return;

	switch (m_animState)
	{
	case CCarAnimation::STATE_DRONE_OUT:

	{
		m_nDelay++;

		if (m_nDelay > 30)
		{
			D3DXVECTOR3 dronePos = pDrone->GetPos();

			dronePos.y += 5.0f;

			pDrone->SetPos(dronePos);

			if (dronePos.y >= 300.0f)
			{
				m_animState = STATE_DRONE_IN;
				m_nDelay = 0;

				CCamera* pCamera = CApplication::GetInstance()->GetCamera();

				if (pCamera)
				{
					pCamera->SetTargetPosR(m_pModel);
					pCamera->BlockCamera(true);
					pCamera->SetPosV(m_posV);
					pCamera->SetPosR(pos);

					pDrone->SetPos(pos + D3DXVECTOR3(-m_speed.x * 10.0f, 1000.0f, -m_speed.z * 10.0f));
				}
			}
		}
	}

		break;

	case CCarAnimation::STATE_DRONE_IN:

	{
		//ドローンの位置を取得し、更新する
		D3DXVECTOR3 dronePos = pDrone->GetPos();
		dronePos.y += -10.0f;

		////ターゲットに着いたかどうかの判定
		//D3DXVECTOR3 dir = -m_speed;
		//D3DXVec3Normalize(&dir, &dir);
		//D3DXVECTOR3 target = pos + D3DXVECTOR3(-m_speed.x * 10.0f, 100.0f, -m_speed.z * 10.0f), dist = target - dronePos;

		if (dronePos.y <= 201.0f)
		{
			m_animState = STATE_ENTER;
		}

		pDrone->SetPos(dronePos);
	}

		break;

	case CCarAnimation::STATE_ENTER:

	{
		pos += m_speed;

		D3DXVECTOR3 playerPos = pPlayer->GetPos(), dist = playerPos - pos, dronePos = pDrone->GetPos();
		float fDist = D3DXVec3Length(&dist);

		dronePos += m_speed;

		if (fDist >= m_lastDist)
		{
			m_bStop = true;
			m_animState = STATE_WAIT;

			/*CAgent* pAgent = dynamic_cast<CAgent*>(pPlayer);

			if (pAgent)
			{
				pAgent->SetSkinAction(CAgent::ACTION_STATE_SKIN::HOLD_SKIN);
			}*/
		}

		m_lastDist = fDist;

		m_pModel->SetPos(pos);
		pDrone->SetPos(dronePos);

		CCamera* pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
			pCamera->SetPosR(m_pModel->GetPos());
	}

		break;

	case CCarAnimation::STATE_WAIT:

	{
		m_nDelay++;

		if (m_nDelay == 90)
			pPlayer->SetPos(pPlayer->GetPos() + D3DXVECTOR3(0.0f, -10000.0f, 0.0f));
		else if (m_nDelay >= 180)
		{
			m_nDelay = 0;
			m_bStop = false;
			m_animState = STATE_ESCAPE;
		}
	}

		break;

	case CCarAnimation::STATE_ESCAPE:

	{
		pos += m_speed;
		m_pModel->SetPos(pos);

		pPlayer->SetPos(pPlayer->GetPos() + m_speed);

		D3DXVECTOR3 dronePos = pDrone->GetPos() + m_speed;

		CCamera* pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
			pCamera->SetPosR(m_pModel->GetPos());

		m_nDelay++;

		pDrone->SetPos(dronePos);

		if (m_nDelay >= 240)
		{
			SetEnd(true);
			//Uninit();
		}
	}

		break;

	default:
		break;
	}
}
