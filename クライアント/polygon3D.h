//=============================================================================
//
// 3Dポリゴンクラス(polygon3D.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================
#ifndef _POLYGON3D_H_		// このマクロ定義がされてなかったら
#define _POLYGON3D_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"

//=============================================================================
// 3Dオブジェクトクラス
// Author : 唐�ｱ結斗
// 概要 : 3Dオブジェクト生成を行うクラス
//=============================================================================
class CPolygon3D : public CPolygon
{
public:
	// 頂点フォーマット
	static const unsigned int	FVF_VERTEX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);
	static const unsigned int	FVF_MULTI_TEX_VTX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEX2);
	static const unsigned int	MAX_TEX = 2;

	//*****************************************************************************
	// 構造体定義
	//*****************************************************************************
	// 頂点データ
	typedef struct
	{
		D3DXVECTOR3			pos;				// 頂点座標
		D3DXVECTOR3			nor;				// 法線ベクトル
		D3DCOLOR			col;				// 頂点カラー
		D3DXVECTOR2			tex[MAX_TEX];		// テクスチャ座標
	}VERTEX_3D;

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CPolygon3D *Create(void);				// 2Dオブジェクトの生成
	static CPolygon3D *Create(int nPriority);		// 2Dポリゴンの生成(オーバーロード)

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CPolygon3D(int nPriority = PRIORITY_LEVEL0);
	~CPolygon3D();

	//--------------------------------------------------------------------
	// オーバーライド関数
	//--------------------------------------------------------------------
	HRESULT Init() override;																	// 初期化
	void Uninit() override;																		// 終了
	void Update() override;																		// 更新
	void Draw() override;																		// 描画

	void SetMtxWorld(D3DXMATRIX mtxWorld) { m_mtxWorld = mtxWorld; }							// ワールドマトリックスのセッター
	void SetBillboard(bool bBillboard) { m_bBillboard = bBillboard; }							// ビルボードの設定
	void SetColor(const D3DXCOLOR &color) override;												// 色の設定
	void SetVtx() override;																		// 頂点座標などの設定
	void SetTex(int nTex, const D3DXVECTOR2 &minTex, const D3DXVECTOR2 &maxTex) override;		// テクスチャ座標の設定
	void SetZFunc(const _D3DCMPFUNC zFunc) { m_zFunc = zFunc; }									// Zテストの優先度のセッター
	void SetAlphaValue(const int nAlphaValue) { m_nAlphaValue = nAlphaValue; }					// アルファテストの透過率のセッター
	void SetPlaneState(const bool set) { m_isPlaneState = set; }
	void SetDraw(const bool bDraw) { m_bDraw = bDraw; }											//描画するかどうかのフラグのセッター

	D3DXMATRIX GetMtxWorld() { return m_mtxWorld; }												// ワールドマトリックスのゲッター
	bool GetBillboard() { return m_bBillboard; }												// ビルボードの取得
	const bool GetDraw() { return m_bDraw; }													//描画するかどうかのフラグのゲッター

protected:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	LPDIRECT3DVERTEXBUFFER9			m_pVtxBuff;			// 頂点バッファ

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	D3DXMATRIX						m_mtxWorld;			// ワールドマトリックス
	_D3DCMPFUNC						m_zFunc;			// Zテストの優先度
	int								m_nAlphaValue;		// アルファテストの透過率
	bool							m_bBillboard;		// ビルボードかどうか
	bool m_isPlaneState;
	bool							m_bDraw;			// 描画するかどうかのフラグ

	struct Handler
	{
		IDirect3DTexture9*	tex0;	// テクスチャ保存用
		D3DXHANDLE			WVP;
		D3DXHANDLE			vLightDir;	// ライトの方向
		D3DXHANDLE			vDiffuse;	// 頂点色
		D3DXHANDLE			vAmbient;	// 頂点色
		D3DXHANDLE			vCameraPos;
		D3DXHANDLE			Technique;	// テクニック
		D3DXHANDLE			Texture;		// テクスチャ
		D3DXHANDLE time;
		D3DXHANDLE isUseTexture;
	};
	Handler m_handler;
};

#endif



