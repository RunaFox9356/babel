//=============================================================================
//
// シーンモードクラス(scene_mode.cpp)
// Author : 唐�ｱ結斗
// 概要 : シーンモードの派生を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "scene_mode.h"
#include "MiniGameMNG.h"
#include "securityCamera.h"

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CSceneMode::CSceneMode() : m_bEnd(false)
{
	m_NumItemId = 0;
	m_NumEnemy = 0;
	m_NumGimmick = 0;
	// 要素をシーンに設定
	SetElement(ELEMENT_SCENE);
	m_MiniMng = CMiniGameMNG::Create();

	//カメラのクリア処理
	CSecurityCamera::ClearCameras();

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CSceneMode::~CSceneMode()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CSceneMode::Init()
{
	for (int i = 0; i < MaxModel; i++)
	{
		m_popEnemy[i].Pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		m_popEnemy[i].isMap = false;
		m_popEnemy[i].isUse = true;
		m_popEnemy[i].isDiscovery = false;
	}

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CSceneMode::Uninit()
{
	if (m_MiniMng != nullptr)
	{
		m_MiniMng = nullptr;
	}

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CSceneMode::Update()
{
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CSceneMode::Draw()
{
}

void CSceneMode::InitPosh()
{
	for (int i = 0; i < MaxModel; i++)
	{
		m_popEnemy[i].Pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		m_popEnemy[i].isMap = false;
		m_popEnemy[i].isUse = true;
		m_popEnemy[i].isDiscovery = false;
	}
}

//=============================================================================
// フォグの設定
//=============================================================================
void CSceneMode::SetFog(const LPDIRECT3DDEVICE9& pDev, float start, float end, float density, D3DXCOLOR color)
{
	// フォグの有効設定
	pDev->SetRenderState(D3DRS_FOGENABLE, TRUE);

	// フォグカラーの設定
	pDev->SetRenderState(D3DRS_FOGCOLOR, color);

	// フォグの範囲設定
	pDev->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);
	pDev->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&start));
	pDev->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&end));

	// フォグの密度の設定
	pDev->SetRenderState(D3DRS_FOGDENSITY, *(DWORD*)(&density));
}