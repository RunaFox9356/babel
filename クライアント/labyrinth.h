#ifndef _LABYRINTH_H_		// このマクロ定義がされてなかったら
#define _LABYRINTH_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "minigame.h"
#include <vector>
#include <list>

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMove;
class CCollision_Rectangle2D;
class CMazeBlock;
class CText;

class CLabyrinth : public CMiniGame
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CLabyrinth(int nPriority = PRIORITY_LEVEL0);
	~CLabyrinth();

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CLabyrinth *Create(int count);

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init();								// 初期化
	void Uninit();								// 終了
	void Update();								// 更新
	void SetNumBlock(const int nCntBlock);

private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void SetBlock();
	void SetMaze();
	void SetUpMaze();
	void TearDownMaze();
	int SearchStart();
	int ReSearchStart();
	int SetDigg(const int nNumID);
	void SetPlayer(const int nNumID);
	void MovePlayer();

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<CMazeBlock*> m_pBlock;
	std::list<int> m_nRoad;
	CPolygon2D *m_pPlayer;
	CCollision_Rectangle2D *m_pPlayerCollision;
	CMove *m_pMove;
	int m_nNumBlock;
	int m_nStartBulock;
	CPolygon2D*	m_pCommandUI[4];					//操作のUI
	CPolygon2D *m_pBg;
	CText* m_pText[2];
};
#endif
