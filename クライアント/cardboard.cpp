//=============================================================================
//
// hostage.cpp
// Author: 磯江 寿希亜
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "cardboard.h"
#include "agent.h"
#include"collision_rectangle3D.h"
#include"model_obj.h"
#include"model3D.h"
#include "game.h"
#include "application.h"
#include "model_data.h"
#include "input.h"
#include "hacker.h"
#include "task.h"
#include "UImessage.h"
#include "motion_model3D.h"
#include "move.h"
#include "calculation.h"
#include "motion.h"
#include "EscapePoint.h"
#include "mapDataManager.h"
#include "hacker.h"


//=============================================================================
//									静的変数の初期化
//=============================================================================
namespace
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
	const int Q_BUTTON_TEXTURE_IDX = 46;			//Qボタンのテクスチャのインデックス
	const float offsetHostagePos = 50.0f;
	const float selfPlayerDistance = 100.0f;
	const float stopTrackingDist = 750.0f;
	const float speed = 5.0f;
}

CCardboard::CCardboard() 
{
}

CCardboard::~CCardboard()
{
}

CCardboard * CCardboard::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	// オブジェクトインスタンス
	CCardboard *pHostageObj = nullptr;

	// メモリの解放
	pHostageObj = new CCardboard;

	// メモリの確保ができなかった
	assert(pHostageObj != nullptr);

	// 数値の初期化
	pHostageObj->Init();

	//位置の設定
	pHostageObj->SetPos(pos);

	//向きの設定
	pHostageObj->SetRot(rot);

	// インスタンスを返す
	return pHostageObj;
}

HRESULT CCardboard::Init()
{
	m_pearentu = nullptr;
	//ギミックにはIDを絶対つけてね
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));
	SetAnimation(false);
	m_pModel = CModelObj::Create();
	assert(m_pModel != nullptr);
	m_pModel->SetType(49);

	m_pModel->SetPos(D3DXVECTOR3(150.0f, 0.f, 100.0f));
	m_pModel->SetObjType(CObject::OBJETYPE_NON_SOLID_GIMMICK);
	D3DXVECTOR3 modelDoorGimmick = m_pModel->GetModel()->GetMyMaterial().size;
	m_pCollision = m_pModel->GetCollision();
	m_pCollision->SetSize(D3DXVECTOR3(100.0f, modelDoorGimmick.y * 0.5f, 100.0f));
	m_pCollision->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y * 0.5f, 0.0f));
	

	return S_OK;
}

// 終了
void CCardboard::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}


	//メモリの解放
	Release();
}

void CCardboard::Update()
{
	CInput *pInput = CInput::GetKey();
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
	if (mode == CApplication::MODE_HACKER)
	{
		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
		{
			m_pearentu = CHacker::GetPlayer();
			SetAnimation(true);
		}
		else
		{
			m_pearentu = nullptr;
			SetAnimation(false);
		}
	}

	//かぶってる判定
	if (m_pCollision->Collision(CObject::OBJETYPE_PLAYER, false))
	{
		if (pInput->Trigger(DIK_E))
		{
			SetAnimation(true);
			std::vector<CObject*> obj = m_pCollision->GetCollidedObj();
			for (int i = 0; i < obj.size(); i++)
			{
				m_pearentu = (CAgent*)obj[i];
				if (m_pearentu != nullptr)
				{
					break;
				}
			}
		}
		if (pInput->Trigger(DIK_Q))
		{
			SetAnimation(false);
			m_pearentu->SetHiddenState(false);		//プレイヤーを隠れている状態に設定する
			m_pearentu = nullptr;
		}
	}

	if (GetAnimation()&& m_pearentu)
	{

		SetPos(m_pearentu->GetPos());
		m_pearentu->SetHiddenState(true);		//プレイヤーを隠れている状態に設定する
	}
}

//位置のセッター
void CCardboard::SetPos(const D3DXVECTOR3 & pos)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPos(pos);
	}
}

//向きのセッター
void CCardboard::SetRot(const D3DXVECTOR3& rot)
{
	if (m_pModel)
	{//モデルのnullチェック

		m_pModel->SetRot(rot);			//モデルの向きを設定する
	}
}

//位置のゲッター
D3DXVECTOR3 CCardboard::GetPos()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//モデルが存在したら、モデルの位置を取得する
		pos = m_pModel->GetPos();
	}

	return pos;			//位置を返す
}

//向きのゲッター
D3DXVECTOR3 CCardboard::GetRot()
{
	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//モデルのnullチェック

		rot = m_pModel->GetRot();		//モデルの向きを取得する
	}

	return rot;				//向きを返す
}

//エフェクトのインデックスの設定
void CCardboard::SetRenderMode(int mode)
{
	//モードの確認
	if (mode < 0 || mode >= CModel3D::ERenderMode::Render_MAX)
		mode = 0;

	m_pModel->SetRenderMode((CModel3D::ERenderMode)mode);
}

//UIメッセージの生成処理
void CCardboard::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();	//テクスチャの最大数の取得
	int nNum = nTexIdx;

	//引数のテクスチャインデックスを確認する
	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	//UIメッセージの破棄
	DestroyUiMessage();

	//新しいUIメッセージを生成する
	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(50.0f, 50.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CCardboard::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}