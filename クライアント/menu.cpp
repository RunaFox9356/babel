//=============================================================================
//
// menu.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "menu.h"
#include "menuButton.h"
#include "renderer.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3 CMenu::DEFAULT_POS = { (float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f };	//ディフォルトの位置
const D3DXVECTOR3 CMenu::DEFAULT_SIZE = { 500.0f, 250.0f, 0.0f };			//ディフォルトのサイズ




//コンストラクタ
CMenu::CMenu() : m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_size(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_pBg(nullptr)
{
	m_vButton.clear();
	m_vButton.shrink_to_fit();
}

//デストラクタ
CMenu::~CMenu()
{

}

// 初期化
HRESULT CMenu::Init()
{
	m_pos = DEFAULT_POS;
	m_size = DEFAULT_SIZE;

	m_pBg = CPolygon2D::Create();		//背景の生成

	if (m_pBg)
	{//生成出来たら
		m_pBg->SetPos(m_pos);		//背景の位置の設定
		m_pBg->SetSize(m_size);		//背景のサイズの設定
	}

	return S_OK;
}

// 終了
void CMenu::Uninit()
{
	m_vButton.clear();
	m_vButton.shrink_to_fit();

	//背景の破棄
	if (m_pBg)
	{
		m_pBg->Uninit();
		m_pBg = nullptr;
	}

	//メモリを解放
	Release();
}

// 更新
void CMenu::Update()
{

}

// 描画
void CMenu::Draw()
{

}

// 位置のセッター
void CMenu::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;

	if (m_pBg)
	{
		m_pBg->SetPos(pos);
	}
}

// 大きさのセッター
void CMenu::SetSize(const D3DXVECTOR3 & size)
{
	m_size = size;

	if (m_pBg)
	{
		m_pBg->SetSize(size);
	}
}

//背景のテクスチャの設定
void CMenu::SetBackgroundTexture(const int nTexIdx)
{
	if (m_pBg)
	{
		std::vector<int> pTex;
		pTex.push_back(nTexIdx);
		m_pBg->LoadTex(pTex);
	}
}

//ボタンのテクスチャの設定
void CMenu::SetButtonTexture(const int nButtonNumber, const int nTexIdx)
{
	if ((int)m_vButton.size() > 0 && nButtonNumber < (int)m_vButton.size() && nButtonNumber >= 0)
	{
		if (m_vButton.data()[nButtonNumber])
			m_vButton.data()[nButtonNumber]->SetTexture(nTexIdx);
	}
}

//ボタンの追加
void CMenu::AddButton(CMenuButton* pButton)
{
	if (pButton)
	{
		m_vButton.push_back(pButton);
		pButton->SetParent(this);
	}
}

//ボタンの生成(位置は(相対です)
void CMenu::CreateButton(const D3DXVECTOR3 pos)
{
	CMenuButton* pButton = CMenuButton::Create(pos);

	if (pButton)
	{
		m_vButton.push_back(pButton);
		pButton->SetParent(this);
	}
}

//ボタンの生成(位置は(相対です)
void CMenu::CreateButton(const D3DXVECTOR3 pos, const D3DXCOLOR normalCol, const D3DXCOLOR triggerCol)
{
	CMenuButton* pButton = CMenuButton::Create(pos, normalCol, triggerCol);

	if (pButton)
	{
		m_vButton.push_back(pButton);
		pButton->SetParent(this);
	}
}

//押されたボタンのインデックス取得
const int CMenu::GetTriggeredButton()
{
	int nIdx = -1;

	for (int nCnt = 0; nCnt < (int)m_vButton.size(); nCnt++)
	{
		if (m_vButton.data()[nCnt] && m_vButton.data()[nCnt]->GetTriggerState())
		{
			nIdx = nCnt;
			break;
		}
	}

	return nIdx;
}

//マウスカーソルと重なっているボタンのインデックス取得
const int CMenu::GetHoverButton()
{
	int nIdx = -1;

	for (int nCnt = 0; nCnt < (int)m_vButton.size(); nCnt++)
	{
		if (m_vButton.data()[nCnt] && m_vButton.data()[nCnt]->GetHoverState())
		{
			nIdx = nCnt;
			break;
		}
	}

	return nIdx;
}

//生成
CMenu* CMenu::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size)
{
	// オブジェクトインスタンス
	CMenu *pObj = new CMenu;

	if (pObj != nullptr)
	{// 数値の初期化
		pObj->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	pObj->SetPos(pos);			//位置の設定
	pObj->SetSize(size);		//サイズの設定

	return pObj;				// インスタンスを返す
}
