//=============================================================================
//
// minimap.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "minimap.h"
#include "player.h"
#include "game.h"
#include "hacker.h"
#include "application.h"
#include "drone.h"
#include "agent.h"
#include "renderer.h"
#include "input.h"
#include "task.h"
#include "gimmick.h"
#include "camera.h"
#include "enemy.h"
#include "scene_mode.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CMinimap::DEFAULT_MINIMAP_SIZE = { 100.0f, 100.0f, 0.0f };			//ディフォルトのミニマップのサイズ
const D3DXVECTOR3	CMinimap::DEFAULT_OPEN_MAP_SIZE = { (float)CRenderer::SCREEN_HEIGHT, (float)CRenderer::SCREEN_HEIGHT, 0.0f };			//ディフォルトの大きいミニマップのサイズ
const float			CMinimap::DEFAULT_MAP_RADIUS = 10000.0f;							//ディフォルトのマップの半径
const D3DXVECTOR3	CMinimap::DEFAULT_ICON_SIZE = { 7.5f, 7.5f, 0.0f };					//ディフォルトのアイコンサイズ
const D3DXVECTOR3	CMinimap::DEFAULT_BIG_ICON_SIZE = { 25.0f, 25.0f, 0.0f };			//ディフォルトの大きいアイコンサイズ
const D3DXCOLOR		CMinimap::DEFAULT_PLAYER_COLOR = { 0.8f, 0.2f, 0.8f, 1.0f };		//ディフォルトのプレイヤーの色
const D3DXCOLOR		CMinimap::DEFAULT_DRONE_COLOR = { 0.2f, 0.8f, 0.8f, 1.0f };			//ディフォルトのドローンの色


namespace nsMinimap
{
	const D3DXVECTOR3		SCREEN_CENTER = { CRenderer::SCREEN_WIDTH * 0.5f, CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f };		//画面の中心の座標
	const int				DEFAULT_DRONE_TEXTURE = 37;						//ドローンのアイコンテクスチャ
	const int				DEFAULT_DESTROYABLE_TEXTURE = 81;				//破壊できるオブジェクトのアイコンテクスチャ
	const int				DEFAULT_ENEMY_TEXTURE = 82;						//敵のアイコンテクスチャ
	const int				DEFAULT_PRISONER_TEXTURE = 83;					//人質のアイコンテクスチャ
	const int				DEFAULT_ESCAPE_TEXTURE = 84;					//エスケープポイントのアイコンのテクスチャ
	const int				DEFAULT_PLAYING_CHARACTER_TEXTURE = 85;			//プレーしているキャラクターのアイコンテクスチャ

	const D3DXCOLOR			DEFAULT_ENEMY_COLOR = { 1.0f, 0.1f, 0.1f, 1.0f };		//敵のアイコンの色
	const D3DXCOLOR			DEFAULT_PRISONER_COLOR = { 0.1f, 1.0f, 0.1f, 1.0f };	//敵のアイコンの色
	const D3DXCOLOR			DEFAULT_DESTROYABLE_COLOR = { 1.0f, 0.6f, 0.3f, 1.0f };	//敵のアイコンの色
};



//コンストラクタ
CMinimap::CMinimap() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL3),
m_originalPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_originalSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nCntAnim(0),
m_bOpen(false),
m_bAgent(false),
m_pPlayer(nullptr),
m_pDrone(nullptr)
{
	m_vObj.clear();
	m_vObj.shrink_to_fit();
	m_vStaticIcon.clear();
	m_vStaticIcon.shrink_to_fit();
	m_vLastObjList.clear();
	m_vLastObjList.shrink_to_fit();
}

//デストラクタ
CMinimap::~CMinimap()
{

}

//初期化
HRESULT CMinimap::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	//エージェントモードであるかどうかを設定する
	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{
		m_bAgent = false;
	}
	else
		m_bAgent = true;

	SetSize(DEFAULT_MINIMAP_SIZE);					//サイズの設定
	SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));	//色の設定
	SetTex(0, D3DXVECTOR2(1.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f));

	{//プレイヤーのアイコンを生成する
		m_pPlayer = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

		if (m_pPlayer)
		{//nullチェック
			m_pPlayer->SetSize(DEFAULT_ICON_SIZE);		//アイコンサイズの設定
			//m_pPlayer->SetColor(DEFAULT_PLAYER_COLOR);	//アイコンの色の設定

			if (m_bAgent)
				m_pPlayer->LoadTex(nsMinimap::DEFAULT_PLAYING_CHARACTER_TEXTURE);
			else
				m_pPlayer->SetColor(DEFAULT_PLAYER_COLOR);	//アイコンの色の設定
		}
	}

	{//プレイヤーのアイコンを生成する
		m_pDrone = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

		if (m_pDrone)
		{//nullチェック
			m_pDrone->SetSize(DEFAULT_ICON_SIZE);						//アイコンサイズの設定

			if (!m_bAgent)
				m_pDrone->LoadTex(nsMinimap::DEFAULT_PLAYING_CHARACTER_TEXTURE);		//アイコンの色の設定
			else
				m_pDrone->LoadTex(nsMinimap::DEFAULT_DRONE_TEXTURE);	//アイコンのテクスチャの設定
		}
	}

	//マップデータマネージャーの取得
	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (pManager)
	{//nullチェック
		//ハッカーのターゲットのデータを保存する
		m_vLastObjList = pManager->GetMapData(CApplication::GetInstance()->GetMap()).vToFindData;
	}

	return S_OK;
}

//終了
void CMinimap::Uninit()
{
	//アイコンの破棄
	if (m_pPlayer)
	{
		m_pPlayer->Uninit();
		m_pPlayer = nullptr;
	}
	if (m_pDrone)
	{
		m_pDrone->Uninit();
		m_pDrone = nullptr;
	}

	//ベクトルのクリア処理
	m_vObj.clear();
	m_vObj.shrink_to_fit();
	m_vStaticIcon.clear();
	m_vStaticIcon.shrink_to_fit();
	m_vLastObjList.clear();
	m_vLastObjList.shrink_to_fit();

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

//更新
void CMinimap::Update()
{
	//インプットの確認
	CheckInput();

	//親クラスの更新処理
	CPolygon2D::Update();

	//プレイヤーの位置の更新処理
	PlayerPosUpdate();

	//ドローンがオブジェクトを送信したかどうかをチェックする
	CheckObjToFind();

	//ハッカーが見つかったオブジェクトの位置の更新処理
	UpdateFoundObjPos();
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
CMinimap* CMinimap::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size)
{
	CMinimap* pObj = new CMinimap;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);					//位置の設定
	pObj->m_originalPos = pos;			//元の位置の設定
	pObj->SetSize(size);				//サイズの設定
	pObj->m_originalSize = size;		//元のサイズの設定
	pObj->AddEscapeIcon();				//エスケープポイントのアイコンを追加する

	return pObj;						//生成したインスタンスを返
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



//プレイヤーの位置の更新
void CMinimap::PlayerPosUpdate()
{
	D3DXVECTOR3 playerPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), dronePos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_nCntAnim++;			//アニメーションカウンターをインクリメントする

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_GAME
		|| CApplication::GetInstance()->GetMode() == CApplication::MODE_AGENT_TUTORIAL
		|| CApplication::GetInstance()->GetMode() == CApplication::MODE_AGENT)
	{//エージェントモードだったら

		//プレイヤーの位置とドローンの位置を取得する
		if (CGame::GetPlayer())
			playerPos = CGame::GetPlayer()->GetPos();

		if (CGame::GetDrone())
			dronePos = CGame::GetDrone()->GetPos();
	}
	else if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{//ハッカーモードだったら

	 //プレイヤーの位置とドローンの位置を取得する
		if (CHacker::GetPlayer())
			playerPos = CHacker::GetPlayer()->GetPos();

		if (CHacker::GetDrone())
			dronePos = CHacker::GetDrone()->GetPos();
	}

	if (m_pPlayer)
	{//プレイヤーのアイコンのnullチェック
		D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		//アイコンの位置を計算し、設定する
		pos.x = (-(GetSize().x * 0.5f) * playerPos.x) / DEFAULT_MAP_RADIUS;
		pos.y = ((GetSize().y * 0.5f) * playerPos.z) / DEFAULT_MAP_RADIUS;

		m_pPlayer->SetPos(GetPos() + pos);

		if (m_bAgent)
		{//エージェントモードだったら、アイコンを点滅させる

			D3DXVECTOR3 rot = CApplication::GetInstance()->GetCamera()->GetRot();

			m_pPlayer->SetRot(D3DXVECTOR3(0.0f, 0.0f, -rot.y));

			if (m_nCntAnim == 30)
			{
				m_pPlayer->SetColor(D3DXCOLOR(1.0f - DEFAULT_PLAYER_COLOR.r, 1.0f - DEFAULT_PLAYER_COLOR.g, 1.0f - DEFAULT_PLAYER_COLOR.b, 1.0f));
			}
			else if (m_nCntAnim >= 60)
			{
				m_pPlayer->SetColor(DEFAULT_PLAYER_COLOR);
				m_nCntAnim = 0;
			}
		}
	}
	if (m_pDrone)
	{//ドローンのアイコンのnullチェック
		D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		//アイコンの位置を計算し、設定する
		pos.x = (-(GetSize().x * 0.5f) * dronePos.x) / DEFAULT_MAP_RADIUS;
		pos.y = ((GetSize().y * 0.5f) * dronePos.z) / DEFAULT_MAP_RADIUS;

		m_pDrone->SetPos(GetPos() + pos);

		if (!m_bAgent)
		{
			D3DXVECTOR3 rot = CApplication::GetInstance()->GetCamera()->GetRot();

			m_pDrone->SetRot(D3DXVECTOR3(0.0f, 0.0f, -rot.y));

			if (m_nCntAnim == 30)
			{//ドローンモードだったら、アイコンを点滅させる
				//m_pDrone->SetColor(D3DXCOLOR(DEFAULT_DRONE_COLOR.r * 0.75f, DEFAULT_DRONE_COLOR.g * 0.75f, DEFAULT_DRONE_COLOR.b * 0.75f, 1.0f));
				m_pDrone->SetColor(D3DXCOLOR(1.0f - DEFAULT_DRONE_COLOR.r, 1.0f - DEFAULT_DRONE_COLOR.g, 1.0f - DEFAULT_DRONE_COLOR.b, 1.0f));
			}
			else if (m_nCntAnim >= 60)
			{
				m_pDrone->SetColor(DEFAULT_DRONE_COLOR);
				m_nCntAnim = 0;
			}
		}
	}
}

//ドローンがオブジェクトを送信したかどうかをチェックする
void CMinimap::CheckObjToFind()
{
	//マップデータマネージャーを取得する
	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (!pManager)
		return;

	//ハッカーのターゲットのベクトルを取得する
	std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pManager->GetMapData(CApplication::GetInstance()->GetMap()).vToFindData;

	for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
	{
		CMapDataManager::OBJ_TO_FIND_DATA data = vData.data()[nCnt], lastData = m_vLastObjList.data()[nCnt];

		if (data.bSent && !data.bGot && !lastData.bSent)
		{
			//アイコンを生成する
			CPolygon2D* pPolygon = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

			if (pPolygon)
			{//nullチェック
				//アイコンサイズの設定
				if (!m_bOpen)
					pPolygon->SetSize(DEFAULT_ICON_SIZE);
				else
					pPolygon->SetSize(DEFAULT_BIG_ICON_SIZE);

				//アイコンテクスチャの設定
				if (data.type == CTask::HACKER_TASK_FIND_DESTROYABLE)
				{
					pPolygon->LoadTex(nsMinimap::DEFAULT_DESTROYABLE_TEXTURE);
					pPolygon->SetColor(nsMinimap::DEFAULT_DESTROYABLE_COLOR);

					CGimmick* pGimmick = nullptr;

					pGimmick = dynamic_cast<CGimmick*>(data.pObj);

					if (pGimmick)
						pGimmick->SetIsOnMap(true);
				}
				else if (data.type == CTask::HACKER_TASK_FIND_ENEMY)
				{
					pPolygon->LoadTex(nsMinimap::DEFAULT_ENEMY_TEXTURE);
					pPolygon->SetColor(nsMinimap::DEFAULT_ENEMY_COLOR);

					CEnemy* pEnemy = nullptr;

					pEnemy = dynamic_cast<CEnemy*>(data.pObj);

					if (pEnemy)
						pEnemy->SetIsOnMap(true);
				}
				else if (data.type == CTask::HACKER_TASK_FIND_PRISONER)
				{
					pPolygon->LoadTex(nsMinimap::DEFAULT_PRISONER_TEXTURE);
					pPolygon->SetColor(nsMinimap::DEFAULT_PRISONER_COLOR);

					CGimmick* pGimmick = nullptr;

					pGimmick = dynamic_cast<CGimmick*>(data.pObj);

					if (pGimmick)
						pGimmick->SetIsOnMap(true);
				}
				else
					pPolygon->SetColor(D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f));
			}

			//オブジェクトの情報を保存する
			SentObj obj;
			obj.nIdx = nCnt;
			obj.pIcon = pPolygon;

			m_vObj.push_back(obj);
		}
		else if(!data.bGot && !lastData.bSent)
		{
			if (data.type == CTask::HACKER_TASK_FIND_ENEMY)
			{
				CEnemy* pEnemy = nullptr;

				pEnemy = dynamic_cast<CEnemy*>(data.pObj);

				if (pEnemy && pEnemy->GetIsOnMap())
				{
					pManager->SentObjData(CApplication::GetInstance()->GetMap(), nCnt);
					pManager->FoundObj(CApplication::GetInstance()->GetMap(), nCnt);

					//アイコンを生成する
					CPolygon2D* pPolygon = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

					if (pPolygon)
					{//nullチェック
					 //アイコンサイズの設定
						if (!m_bOpen)
							pPolygon->SetSize(DEFAULT_ICON_SIZE);
						else
							pPolygon->SetSize(DEFAULT_BIG_ICON_SIZE);

						//アイコンテクスチャの設定
						pPolygon->LoadTex(nsMinimap::DEFAULT_ENEMY_TEXTURE);
						pPolygon->SetColor(nsMinimap::DEFAULT_ENEMY_COLOR);
					}

					//オブジェクトの情報を保存する
					SentObj obj;
					obj.nIdx = nCnt;
					obj.pIcon = pPolygon;

					m_vObj.push_back(obj);
				}
			}
			else if (data.type == CTask::HACKER_TASK_FIND_DESTROYABLE || data.type == CTask::HACKER_TASK_FIND_PRISONER)
			{
				CGimmick* pGimmick = nullptr;

				pGimmick = dynamic_cast<CGimmick*>(data.pObj);

				if (pGimmick && pGimmick->GetIsOnMap())
				{
					pManager->SentObjData(CApplication::GetInstance()->GetMap(), nCnt);
					pManager->FoundObj(CApplication::GetInstance()->GetMap(), nCnt);

					//アイコンを生成する
					CPolygon2D* pPolygon = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

					if (pPolygon)
					{//nullチェック
					 //アイコンサイズの設定
						if (!m_bOpen)
							pPolygon->SetSize(DEFAULT_ICON_SIZE);
						else
							pPolygon->SetSize(DEFAULT_BIG_ICON_SIZE);

						//アイコンテクスチャの設定
						if (data.type == CTask::HACKER_TASK_FIND_DESTROYABLE)
						{
							pPolygon->LoadTex(nsMinimap::DEFAULT_DESTROYABLE_TEXTURE);
							pPolygon->SetColor(nsMinimap::DEFAULT_DESTROYABLE_COLOR);
						}
						else if (data.type == CTask::HACKER_TASK_FIND_PRISONER)
						{
							pPolygon->LoadTex(nsMinimap::DEFAULT_PRISONER_TEXTURE);
							pPolygon->SetColor(nsMinimap::DEFAULT_PRISONER_COLOR);
						}
					}

					//オブジェクトの情報を保存する
					SentObj obj;
					obj.nIdx = nCnt;
					obj.pIcon = pPolygon;

					m_vObj.push_back(obj);
				}
			}
		}
	}

	for (int nCnt = 0; nCnt < (int)m_vObj.size(); nCnt++)
	{
		CMapDataManager::OBJ_TO_FIND_DATA data = vData.data()[m_vObj.data()[nCnt].nIdx], lastData = m_vLastObjList.data()[m_vObj.data()[nCnt].nIdx];

		if (data.bGot && !lastData.bGot)
		{//必要だったら、アイコンを消す
			m_vObj.data()[nCnt].pIcon->Uninit();
			m_vObj.erase(m_vObj.begin() + nCnt);
			m_vObj.shrink_to_fit();
		}
	}

	//ハッカーのターゲットを保存する
	m_vLastObjList = pManager->GetMapData(CApplication::GetInstance()->GetMap()).vToFindData;
}

//ハッカーが見つかったオブジェクトの位置の更新処理
void CMinimap::UpdateFoundObjPos()
{
	for (int nCnt = 0; nCnt < (int)m_vObj.size(); nCnt++)
	{
		//見つかれたオブジェクトを取得する
		CObject* pObj = m_vLastObjList.data()[m_vObj.data()[nCnt].nIdx].pObj;

		if (!pObj)
			return;

		D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		//アイコンの位置を計算し、設定
		pos.x = (-(GetSize().x * 0.5f) * pObj->GetPos().x) / DEFAULT_MAP_RADIUS;
		pos.y = ((GetSize().y * 0.5f) * pObj->GetPos().z) / DEFAULT_MAP_RADIUS;

		m_vObj.data()[nCnt].pIcon->SetPos(GetPos() + pos);
	}
}

//インプットの確認
void CMinimap::CheckInput()
{
	//インプットデバイスの取得
	CInput* pInput = CApplication::GetInstance()->GetInput();

	if (pInput->Trigger(DIK_M))
	{//Mキーを押したら
		m_bOpen = !m_bOpen;									//マップの大きさを切り替える

		if (m_bOpen)
		{//大きいマップ状態だったら
			SetPos(nsMinimap::SCREEN_CENTER);				//位置の設定
			CPolygon2D::SetSize(DEFAULT_OPEN_MAP_SIZE);		//サイズの設定

			//エージェントアイコンのサイズを変更する
			if (m_pPlayer)
				m_pPlayer->SetSize(DEFAULT_BIG_ICON_SIZE);

			//ドローンアイコンのサイズを変更する
			if (m_pDrone)
				m_pDrone->SetSize(DEFAULT_BIG_ICON_SIZE);

			//動けるオブジェクトのアイコンのサイズを変更する
			for (int nCnt = 0; nCnt < (int)m_vObj.size(); nCnt++)
			{
				if (m_vObj.data()[nCnt].pIcon)
					m_vObj.data()[nCnt].pIcon->SetSize(DEFAULT_BIG_ICON_SIZE);
			}

			//動けないオブジェクトのアイコンのサイズを変更し、位置を更新する
			for (int nCnt = 0; nCnt < (int)m_vStaticIcon.size(); nCnt++)
			{
				StaticIcon icon = m_vStaticIcon.data()[nCnt];

				if (icon.pIcon)
				{
					icon.pIcon->SetSize(DEFAULT_BIG_ICON_SIZE);

					D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

					pos.x = (-(GetSize().x * 0.5f) * icon.pos.x) / DEFAULT_MAP_RADIUS;
					pos.y = ((GetSize().y * 0.5f) * icon.pos.z) / DEFAULT_MAP_RADIUS;

					icon.pIcon->SetPos(GetPos() + pos);
				}
			}
		}
		else
		{//ミニマップ状態だったら
			SetPos(m_originalPos);							//位置の設定
			CPolygon2D::SetSize(m_originalSize);			//サイズの設定

			//エージェントアイコンのサイズを元に戻す
			if (m_pPlayer)
				m_pPlayer->SetSize(DEFAULT_ICON_SIZE);

			//ドローンアイコンのサイズを変更する
			if (m_pDrone)
				m_pDrone->SetSize(DEFAULT_ICON_SIZE);

			//動けるオブジェクトのアイコンのサイズを変更する
			for (int nCnt = 0; nCnt < (int)m_vObj.size(); nCnt++)
			{
				if (m_vObj.data()[nCnt].pIcon)
					m_vObj.data()[nCnt].pIcon->SetSize(DEFAULT_ICON_SIZE);
			}

			//動けないオブジェクトのアイコンのサイズを変更し、位置を更新する
			for (int nCnt = 0; nCnt < (int)m_vStaticIcon.size(); nCnt++)
			{
				StaticIcon icon = m_vStaticIcon.data()[nCnt];

				if (icon.pIcon)
				{
					icon.pIcon->SetSize(DEFAULT_ICON_SIZE);

					D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

					pos.x = (-(GetSize().x * 0.5f) * icon.pos.x) / DEFAULT_MAP_RADIUS;
					pos.y = ((GetSize().y * 0.5f) * icon.pos.z) / DEFAULT_MAP_RADIUS;

					icon.pIcon->SetPos(GetPos() + pos);
				}
			}
		}
	}
}

//エスケープポイントのアイコンの追加
void CMinimap::AddEscapeIcon()
{
	//マップデータマネージャーを取得する
	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (pManager)
	{//nullチェック

		//ギミックデータを取得する
		std::vector<CMapDataManager::GIMMICK_DATA> vData = pManager->GetMapData(CApplication::GetInstance()->GetMap()).vGimmickData;

		for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
		{
			if (vData.data()[nCnt].nType == CGimmick::GIMMICK_TYPE_ESCAPE_POINT)
			{//ギミックはエスケープポイントだったら、アイコンを生成する
				StaticIcon icon;

				icon.pos = vData.data()[nCnt].pos;								//エスケープポイントの位置を保存する
				icon.pIcon = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);		//アイコンを生成する

				if (icon.pIcon)
				{//nullチェック
					icon.pIcon->LoadTex(nsMinimap::DEFAULT_ESCAPE_TEXTURE);		//テクスチャの設定
					icon.pIcon->SetColor(D3DXCOLOR(0.25f, 1.0f, 0.5f, 1.0f));	//色の設定
					icon.pIcon->SetSize(DEFAULT_ICON_SIZE);						//サイズの設定

					D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

					//位置を計算し、設定する
					pos.x = (-(GetSize().x * 0.5f) * icon.pos.x) / DEFAULT_MAP_RADIUS;
					pos.y = ((GetSize().y * 0.5f) * icon.pos.z) / DEFAULT_MAP_RADIUS;

					icon.pIcon->SetPos(GetPos() + pos);

					m_vStaticIcon.push_back(icon);
				}
			}
		}
	}
}
