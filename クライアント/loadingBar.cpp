//=============================================================================
//
// loadingBar.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "loadingBar.h"
#include "renderer.h"



//=============================================================================
//								 静的変数の初期化
//=============================================================================
const int			CLoadingBar::DEFAULT_LOAD_TIME = 60;											//ディフォルトのロードに必要なフレーム数
const D3DXVECTOR3	CLoadingBar::DEFAULT_POS =
{ (float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f };			//ディフォルトの位置
const D3DXVECTOR3	CLoadingBar::DEFAULT_SIZE = { 500.0f, 60.0f, 0.0f };							//ディフォルトのサイズ
const D3DXCOLOR		CLoadingBar::DEFAULT_COLOR = { 0.0f, 0.0f, 0.0f, 1.0f };						//外側のバーの色
const D3DXCOLOR		CLoadingBar::DEFAULT_INNER_COLOR = { 0.0f, 1.0f, 0.25f, 1.0f };					//内側のバーの色



//コンストラクタ
CLoadingBar::CLoadingBar() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL4),
m_nCntTime(0),
m_nLoadTime(0),
m_innerBarPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_size(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_bEnd(false),
m_pInnerBar(nullptr)
{

}

//デストラクタ
CLoadingBar::~CLoadingBar()
{

}

//初期化
HRESULT CLoadingBar::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	//色の設定
	SetColor(DEFAULT_COLOR);

	//内側のバーの生成
	m_pInnerBar = CPolygon2D::Create(CSuper::PRIORITY_LEVEL4);

	if (m_pInnerBar)
	{//nullチェックと必要な変数の初期化

		m_pInnerBar->SetSize(D3DXVECTOR3(0.0f, 50.0f, 0.0f));
		m_pInnerBar->SetColor(DEFAULT_INNER_COLOR);
	}

	return S_OK;
}

//終了
void CLoadingBar::Uninit()
{
	//内側のバーの破棄
	if (m_pInnerBar)
	{
		m_pInnerBar->Uninit();
		m_pInnerBar = nullptr;
	}

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

//更新
void CLoadingBar::Update()
{
	//親クラスの更新処理
	CPolygon2D::Update();

	if (m_bEnd)
		return;

	if (m_nCntTime < m_nLoadTime)
		m_nCntTime++;			//ロード時間の更新
	else
		m_bEnd = true;

	if (m_pInnerBar && m_nLoadTime > 0)
	{//nullチェック

		//内側のバーのサイズ設定
		D3DXVECTOR3 size = D3DXVECTOR3(0.0f, m_size.y - (m_size.y * 0.2f), 0.0f);
		size.x = ((m_size.x - (m_size.x * 0.02f)) * m_nCntTime) / m_nLoadTime;

		D3DXVECTOR3 pos = m_innerBarPos;
		pos.x += size.x * 0.5f;

		m_pInnerBar->SetSize(size);
		m_pInnerBar->SetPos(pos);
	}
}

//描画
void CLoadingBar::Draw()
{
	//親クラスの描画処理
	CPolygon2D::Draw();
}

//位置のセッター
void CLoadingBar::SetPos(const D3DXVECTOR3 & pos)
{
	CPolygon2D::SetPos(pos);

	if (m_pInnerBar)
	{
		m_pInnerBar->SetPos(pos - D3DXVECTOR3((m_size.x * 0.5f - (5.0f * 0.01f)), 0.0f, 0.0f));
	}
}

// 大きさのセッター
void CLoadingBar::SetSize(const D3DXVECTOR3& size)
{
	m_size = size;

	CPolygon2D::SetSize(m_size);

	if (m_pInnerBar)
	{
		m_pInnerBar->SetSize(D3DXVECTOR3(0.0f, m_size.y, 0.0f));
	}
}

//ロードのリセット
void CLoadingBar::Reset()
{
	m_nCntTime = 0;
	m_bEnd = false;
}




//=============================================================================
//
//									静的関数
//
//=============================================================================




//生成
CLoadingBar* CLoadingBar::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int nLoadTime)
{
	CLoadingBar* pObj = new CLoadingBar;	//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理ができなかった場合
		return nullptr;
	}

	//内側のバーの相対位置の設定
	pObj->m_innerBarPos = pos - D3DXVECTOR3((size.x * 0.5f - (size.x * 0.01f)), 0.0f, 0.0f);

	pObj->SetPos(pos);						//位置の設定
	pObj->SetSize(size);					//サイズの設定

	if (nLoadTime <= 0)
		nLoadTime = DEFAULT_LOAD_TIME;

	pObj->m_nLoadTime = nLoadTime;			//ロード時間の設定

	return pObj;							//生成したインスタンスを返す
}