//=============================================================================
//
// jammingTool.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _JAMMING_TOOL_H
#define _JAMMING_TOOL_H


//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CModelObj;
class CPolygon2D;


class CJammingTool : public CGimmick
{
public:
	CJammingTool();					//コンストラクタ
	~CJammingTool() override;		//デストラクタ

	HRESULT Init() override;		//初期化
	void Uninit() override;			//終了
	void Update() override;			//更新
	void Draw() override;			//描画

	void SetPos(const D3DXVECTOR3 &pos) override;				//位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}	//過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;				//向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};		//大きさのセッター
	D3DXVECTOR3 GetPos() override;								//位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }	//過去位置のゲッター
	D3DXVECTOR3 GetRot()  override;								//向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	//大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void SetModelType(int nIdx);								//モデルの種類の設定

	static CJammingTool* Create(D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f));

private:

	void UpdateStaticNoise();							//スノーノイズの更新処理

private:

	static const int	DEFAULT_MODEL_IDX;				//ディフォルトのモデルインデックス
	static const float	DEFAULT_MAX_JAMMING_DISTANCE;	//ディフォルトの最大有効距離
	static const float	DEFAULT_MIN_JAMMING_DISTANCE;	//ディフォルトの最小有効距離

	float			m_fCoeffA;							//放物線のA係数(f(x) = ax^2 + bx + c)
	float			m_fCoeffB;							//放物線のB係数(f(x) = ax^2 + bx + c)
	float			m_fCoeffC;							//放物線のC係数(f(x) = ax^2 + bx + c)


	CModelObj*		m_pModel;							//モデルへのポインタ
	CPolygon2D*		m_pStatic;							//スノーノイズへのポインタ
};

#endif