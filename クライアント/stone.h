//=============================================================================
//
// stone.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _STONE_H_		// このマクロ定義がされてなかったら
#define _STONE_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CModelObj;
class CCollision_Rectangle3D;


class CStone : public CGimmick
{
public:

	CStone();						//コンストラクタ
	~CStone() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &posOld) override;			// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {}			// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};		// 大きさのセッター
	D3DXVECTOR3 GetPos() override;								// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override;							// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }	// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定

	static CStone* Create(D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f));		//生成

private:

	void UpdateState();		//状態によって更新する
	void NormalUpdate();	//普通状態の更新
	void PickedUpUpdate();	//拾われた状態の更新
	void ThrownUpdate();	//投げられた状態の更新

private:

	//状態の列挙型
	enum STATE
	{
		STATE_NORMAL = 0,	//普通
		STATE_PICKED_UP,	//拾われた
		STATE_THROWN,		//投げられた

		STATE_MAX
	};


	static const int			DEFAULT_MODEL_INDEX;	//ディフォルトのモデルインデック
	static const float			DEFAULT_SOUND_RADIUS;	//音が聞こえる範囲の半径
	static const D3DXVECTOR3	DEFAULT_HITBOX_SIZE;	//ディフォルトのヒットボックスサイズ
	static const int			DEFAULT_DELAY;			//ディフォルトのディレイ
	static const float			DEFAULT_THROW_SPEED;	//ディフォルトの投げる速度
	

	D3DXVECTOR3					m_move;					//移動量
	int							m_nCntDelay;			//ディレイのカウンター
	bool						m_bPickedUp;			//拾われたかどうかのフラグ
	STATE						m_state;				//状態

	CModelObj*					m_pModel;				//モデルへのポインタ
	CCollision_Rectangle3D*		m_pHitbox;				//ヒットボックスへのポインタ
};

#endif