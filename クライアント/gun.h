//=============================================================================
//
// eNX(gun.h)
// Author : ú±l
// Tv : fIuWFNg¶¬ðs¤
//
//=============================================================================
#ifndef _GUN_H_			// ±Ì}Nè`ª³êÄÈ©Á½ç
#define _GUN_H_			// ñdCN[hh~Ì}Nè`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "itemObj.h"

//=============================================================================
// Oûé¾
//=============================================================================
class CUiMessage;

//=============================================================================
// fIuWFNgNX
// Author : ú±l
// Tv : fIuWFNg¶¬ðs¤NX
//=============================================================================
class CGun : public CItemObj
{
public:

	static const D3DXVECTOR3	DEFAULT_BULLET_SPAWN;		//fBtHgÌeÌX|[ÌÎÊu

	//--------------------------------------------------------------------
	// ÃIoÖ
	//--------------------------------------------------------------------
	static CGun *Create();							// fIuWFNgÌ¶¬

	//--------------------------------------------------------------------
	// RXgN^ÆfXgN^
	//--------------------------------------------------------------------
	CGun();
	~CGun();

	//--------------------------------------------------------------------
	// oÖ
	//--------------------------------------------------------------------
	HRESULT Init() override;													// ú»
	void Uninit() override;														// I¹
	void Update() override;														// XV
	void Draw() override;														// `æ
	void SetParent(CObject *pParent) { m_pParent = pParent; }					// eÌÝè
	void SetParent() { m_pParent = nullptr; }									// eÌÝè
	void SetBulletMove(D3DXVECTOR3 bulletMove) { m_bulletMove = bulletMove; }	// eÌ­ËûüÌÝè
	void SetMaxBullet(int nMaxBullet) { m_nMaxBullet = nMaxBullet; }			// eJEgÌÅå
	void SetMaxInterval(int nMaxInterval) { m_nMaxInterval = nMaxInterval; }	// e­ËÜÅÌC^[oÌÅå
	void SetMaxReload(int nMaxReload) { m_nMaxReload = nMaxReload; }			// [hJEgÌÅå
	void SetReload(bool bReload);												// [htOÌÝè
	void SetShot(bool bShot);													// eÌ­ËtOÌÝè
	void SetRapidFire(bool bRapidFire) { m_bRapidFire = bRapidFire; }			// eÌ­ËtOÌÝè
	void SetAllReload(bool bAllReload) { m_bAllReload = bAllReload; }			// eðÜÆßÄ[h·é©Û©
	void SetSound(bool bAllSound) { m_bSound = bAllSound; }			// eðÜÆßÄ[h·é©Û©
	void SetBulletType(CObject::EObjectType type);								// eÌíÞÌÝè
	const int GetMaxBullet() { return m_nMaxBullet; }							//eJEgÌÅå
	const int GetLeftBullet() { return m_nCntBullet; }							//ceÌQb^[
	const D3DXVECTOR3 GetBulletSpawn();											//eÌX|[ÌÊuÌæ¾
	void SetHeldFlag(const bool bHeld) { m_bHeld = bHeld; }						//Eíê½©Ç¤©ÌtOZb^[


private:
	//--------------------------------------------------------------------
	// oÖ
	//--------------------------------------------------------------------
	void Shot();
	void Reload(bool isSound = false);
	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIbZ[WÌ¶¬
	void DestroyUiMessage();	//UIbZ[WÌjü

	//--------------------------------------------------------------------
	static const int		DEFAULT_GUN_MODEL;				//fBtHgÌf
	static const int		DEFAULT_MAX_BULLET;				//fBtHgÌeÌÅå
	static const int		DEFAULT_MAX_INTERVAL;	
	static const int		DEFAULT_MAX_RELOAD;				//fBtHgÌ[ht[

	//--------------------------------------------------------------------
	// oÏ
	//--------------------------------------------------------------------
	CObject *m_pParent;				// e
	D3DXVECTOR3 m_bulletMove;		// eÌ­Ëûü
	int m_nCntBullet;				// eJEg
	int m_nCntInterval;				// e­ËÜÅÌC^[o
	int m_nCntReload;				// [hJEg
	int m_nMaxBullet;				// eJEgÌÅå
	int m_nMaxInterval;				// e­ËÜÅÌC^[oÌÅå
	int m_nMaxReload;				// [hJEgÌÅå
	int m_Sin;						// ®­
	bool m_bReload;					// eÌ[hðs¤©Û©
	bool m_bShot;					// eÌ­Ëðs¤©Û©
	bool m_bRapidFire;				// eÌAËðs¤©Û©
	bool m_bAllReload;				// eðÜÆßÄ[h·é©Û©
	bool m_bSound;					// ¹ðg¤©Ç¤©
	bool m_bReloadSound;			// ¹ðg¤©Ç¤©
	bool m_bHeld;					// Eíê½©Ç¤©
	EObjectType m_bulletType;		// eÌíÞ
	CUiMessage*		 m_pUiMessage;	// bZ[WÌUI
};

#endif

