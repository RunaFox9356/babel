//=============================================================================
//
// ハッキング可能オブジェクトクラス(HackingObj.cpp)
// Author : 有田明玄
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>

#include "model3D.h"
#include "HackingObj.h"
#include "model_obj.h"
#include "renderer.h"
#include "application.h"
#include "collision_rectangle3D.h"
#include "MiniGameMNG.h"
#include "map.h"
#include "scene_mode.h"
#include "task.h"
#include "hacker.h"

//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
//CModelObj*					m_HackObj;				// モデル

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CHackObj * CHackObj::Create()
{
	// オブジェクトインスタンス
	CHackObj *pModelObj = nullptr;

	// メモリの解放
	pModelObj = new CHackObj;

	// メモリの確保ができなかった
	assert(pModelObj != nullptr);

	// 数値の初期化
	pModelObj->Init();

	// インスタンスを返す
	return pModelObj;
}

//=============================================================================
// ファイルを読み込み処理
// Author : 唐�ｱ結斗
// 概要 : ファイルを読み込みモデルを生成する
//=============================================================================
void CHackObj::LoadFile(const char *pFileName)
{
	CModelObj::LoadFile(pFileName);
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CHackObj::CHackObj() 
{
}

//=============================================================================
// デストラクタ
// Author : 有田明玄
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CHackObj::~CHackObj()
{

}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CHackObj::Init()
{
	SetObjType(OBJETYPE_HACKOBJ);

	m_HackObj = CModelObj::Create();
	assert(m_HackObj != nullptr);
	m_HackObj->SetType(53);
	m_HackObj->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_HackObj->SetObjType(CObject::OBJETYPE_GIMMICK);
	SetPosDefault(D3DXVECTOR3(150, 0.f, 100.0f));
	D3DXVECTOR3 modelDoorGimmick = m_HackObj->GetModel()->GetMyMaterial().size;
	m_pCollision = m_HackObj->GetCollision();
	m_pCollision->SetSize(modelDoorGimmick);
	m_pCollision->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y / 2.0f, 0.0f));
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_move.x = 5.0f;
	m_hitModle = CModelObj::Create();
	m_IsLeft = true;

	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));

	m_pCollision = m_hitModle->GetCollision();

	D3DXVECTOR3 pos = D3DXVECTOR3(0, 50.f, 0.0f);
	D3DXVECTOR3 size = D3DXVECTOR3(100, 100.f, 100.0f);
	if (m_hitModle)
	{
		m_hitModle->SetPos(D3DXVECTOR3(150, 0.f, 100.0f));
		m_hitModle->SetObjType(CObject::EObjectType::OBJETYPE_NON_SOLID_GIMMICK);
	}

	if (m_pCollision)
	{//nullチェック
		m_pCollision->SetPos(pos);					//コリジョンのサイズの位置の設定
		m_pCollision->SetSize(size);				//コリジョンのサイズの設定
	}
	m_HackState = STATE_NOT_HACK;

	CTask* pTask = CHacker::GetTask();			//タスクの取得

	if (pTask)
	{//nullチェック
		pTask->AddHackerTask(CTask::HACKER_TASK_HACK);	//タスクの更新
	}

	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CHackObj::Uninit()
{

}

//=============================================================================
// 更新
// Author : 有田明玄
// 概要 : 更新を行う
//=============================================================================
void CHackObj::Update()
{
}

//=============================================================================
// 描画
// Author : 有田明玄
// 概要 : 描画を行う
//=============================================================================
void CHackObj::Draw()
{
}

//=============================================================================
// ハッキング
// Author : 有田明玄
// 概要 :　sceneからハッキングを呼び出して判定
//=============================================================================
void CHackObj::Hacking()
{
	CMiniGameMNG* pMane = CApplication::GetInstance()->GetSceneMode()->GetMiniMng();
	pMane->LoadMinigame();
}



//エフェクトのインデックスの設定
void CHackObj::SetRenderMode(int mode)
{
	if (m_HackObj)
		m_HackObj->SetRenderMode(mode);
}

//向きの設定処理
void CHackObj::Rotate(const int nSwitchRot)
{
	//モデルがなかったら、何もせずに終わる
	if (!m_HackObj)
		return;

	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//向きの変数を宣言する

	//スイッチによって向きを設定する
	if (nSwitchRot == 1)
		rot.y = D3DX_PI * 0.5f;
	else if (nSwitchRot == 2)
		rot.y = D3DX_PI;
	else if (nSwitchRot == 3)
		rot.y = D3DX_PI * 1.5f;

	m_HackObj->SetRot(rot);				//モデルの向きを設定する

	//回転量が0、またはπだったら、このままで終わる
	if (nSwitchRot % 2 == 0)
		return;

	//当たり判定を取得する
	CCollision_Rectangle3D* pCollision = m_HackObj->GetCollision();

	if (pCollision)
	{//nullチェック

		D3DXVECTOR3 size = m_HackObj->GetModel()->GetMyMaterial().size;		//モデルのサイズを取得する

		pCollision->SetSize(D3DXVECTOR3(size.z, size.y, size.x));			//当たり判定のXとZ座標を逆にする
	}
}

void CHackObj::HackObjTP()
{
	m_HackObj->GetCollision()->SetPos(D3DXVECTOR3(m_HackObj->GetCollision()->GetPos().x, 1000000.0f, m_HackObj->GetCollision()->GetPos().z));
	m_pCollision->SetPos(D3DXVECTOR3(m_pCollision->GetPos().x, 1000000, m_pCollision->GetPos().z));
}

// モデルの種類の設定
void CHackObj::SetModelType(int nIdx)
{
	if (!m_HackObj)
		return;

	if (nIdx < 0 || nIdx >= CModel3D::GetMaxModel())
		nIdx = 53;

	m_HackObj->SetType(nIdx);

	CCollision_Rectangle3D* pCollision = m_HackObj->GetCollision();
	CModel3D* pModel = m_HackObj->GetModel();

	if (pCollision && pModel)
	{
		D3DXVECTOR3 size = pModel->GetMyMaterial().size, pos = D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f);

		pCollision->SetSize(size);
		pCollision->SetPos(pos);
	}
}

// 描画するかどうかのフラグセッター
void CHackObj::SetDraw(const bool bDraw)
{
	if (m_HackObj)
	{
		m_HackObj->SetDraw(bDraw);
	}
}