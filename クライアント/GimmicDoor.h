#ifndef _GimmicDoor_H_		// このマクロ定義がされてなかったら
#define _GimmicDoor_H_		// 二重インクルード防止のマクロ定義
//=============================================================================
//
// 運んで開けるドア.h
// Author: hamada
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "GimmicDoor.h"
#include "model_obj.h"
#include "gimmick.h"
//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CModelObj;
class CMove;
class CGimmick;
class CCollision_Rectangle3D;

class CGimmicDoor:public CGimmick
{
public:
	static CGimmicDoor *Create(const bool bOpenInXDirection = true);							// モデルオブジェクトの生成

	enum Type
	{
		Type_None,
		Type_Player,
		Type_Enemy,
		Type_Max
	};

	CGimmicDoor();
	~CGimmicDoor()override;

	HRESULT Init() override;		// 初期化
	void Uninit() override {}			// 終了
	void Update() override;			// 更新
	void Draw() override {}			// 描画

	void SetMove(D3DXVECTOR3 move) { m_move = move; }

	void SetLeft(bool move) { m_IsLeft = move; }

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosDefault(D3DXVECTOR3 pos);						// 位置の設定
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {}				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};			// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return door->GetPos(); }							// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }						// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void SetModelType(int nIdx, const bool bRot);				// モデルの種類の設定

	void IsOpeningInXDirection(const bool bOpenInXDir);			//X方向に開くかどうかの設定処理

	void bSetType(Type type) { m_type = type; }

private:

	void Animation();				//開く/閉まるアニメーション

private:

	D3DXVECTOR3 Move();


	static const float DEFAULT_OPEN_SPEED;		//ディフォルトの開くアニメーションのスピード
	static const float DEFAULT_OPEN_DISTANCE;	//ディフォルトの開くときの距離


	CMove *pMove;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_posDefault;
	D3DXVECTOR3 m_rot;				//回転角度
	bool m_IsLeft;
	bool m_bOpenInXDir;				//X方向に開くかどうか
	CModelObj* door;
	CModelObj* m_hitModel;
	CCollision_Rectangle3D *pCollisionDoorGimmick;
	CCollision_Rectangle3D *pCollisionDoor;

	Type m_type;
};


#endif
