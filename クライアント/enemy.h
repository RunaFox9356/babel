//=============================================================================
//
// エネミークラス(enemy.h)
// Author : 唐�ｱ結斗
// 概要 : エネミー生成を行う
//
//=============================================================================
#ifndef _ENEMY_H_			// このマクロ定義がされてなかったら
#define _ENEMY_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <Windows.h>
#include "d3dx9.h"							// 描画処理に必要
#include "motion_model3D.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMove;
class CCollision_Rectangle3D;
class CGun;
class CExclamationMark;

//=============================================================================
// プレイヤークラス
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行うクラス
//=============================================================================
class CEnemy : public CMotionModel3D
{
public:
	//--------------------------------------------------------------------
	// 定数定義
	//--------------------------------------------------------------------
	//敵の種類
	enum EEnemyType
	{
		ENEMY_TYPE_SLEEPING = 0,
		ENEMY_TYPE_STATIC,
		ENEMY_TYPE_PATROLLING,

		ENEMY_TYPE_MAX
	};

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CEnemy *Create();			// プレイヤーの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CEnemy();
	~CEnemy();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	virtual HRESULT Init();														// 初期化
	void Uninit() override;														// 終了
	void Update() override;														// 更新
	void Draw() override;														// 描画
	void SetRotDest(D3DXVECTOR3 rotDest) { m_rotDest = rotDest; }
	CMove *GetMove() { return m_pMove; }										// 移動情報の取得
	CCollision_Rectangle3D *GetCollision() { return m_pCollision; }				// 当たり判定の取得
	D3DXVECTOR3 GetRotDest() { return m_rotDest; }								// 移動方向の取得
	void SetId(int SendID) { m_SendId = SendID; }
	int GetId() { return m_SendId; }
	void Kill();																// 敵を殺す 
	void SetLife(const int nLife) { m_nLife = nLife; }							//ライフの設定
	void IncreaseLife() { m_nLife++; }											//ライフのインクリメント
	void DecreaseLife() { m_nLife--; }											//ライフのデクリメント
	const int GetLife() { return m_nLife; }										//ライフの取得
	void SetDeath(const bool bDeath) { m_bDeath = bDeath; }						// 死亡フラグの設定
	const bool GetDeath() { return m_bDeath; }									// 死亡フラグの取得
	void SetHandModelIdx(const int nHandIdx);									// 手のモデルのインデックスの設定
	CGun* GetGun() { return m_pGun; }											// 銃の取得
	void SetRotFriction(const float fFriction) { m_fRotFriction = fFriction; }	//回転の摩擦係数の設定
	virtual void SetSecurityLevel(const int nSecurity) { m_nSecurityLevel = nSecurity; }
	const int GetSecurityLevel() { return m_nSecurityLevel; }

	void SetFoundState(const bool bFound) { m_bFound = bFound; }		//ハッカーに確認されたかどうかの設定
	const bool HasBeenFound() { return m_bFound; }						//ハッカーに確認されたかどうかの取得

	void SetSoundPos(const D3DXVECTOR3 soundPos);						// 聞いた音の位置の設定
	const D3DXVECTOR3 GetSoundPos() { return m_soundPos; }				// 聞いた音の位置の取得

	const bool GetIsOnMap() { return m_bOnMap; }								//マップに表示されているかどうかの取得処理
	void SetIsOnMap(const bool bOnMap);									//マップに表示されているかどうかの設定処理
	virtual void Stun();
	bool GetStun() { return (m_nStun <= 0) ? true : false; };			//スタンしてるか判定
	void KillAnimation();
protected:

	enum SHOOT_MODEL
	{
		SHOOT_MODEL_SINGLE = 0,		//もう一度狙う前、弾を1個を撃つ
		SHOOT_MODEL_DOUBLE,			//もう一度狙う前、弾を2個を撃つ
		SHOOT_MODEL_TRIPLE,			//もう一度狙う前、弾を3個を撃つ

		SHOOT_MODEL_MAX
	};

	void CreateGun(const int nHandIdx);						// 銃の生成
	const bool Shoot();										// 弾を撃つ(ランダムなパターン)
	const bool Shoot(const int nShootModelIdx);				// 弾を撃つ
	void SoundPosReset();									// 音を聞いていない状態に戻す
	void IncreaseSecurity();								//セキュリティーをインクリメントする

protected:

	//--------------------------------------------------------------------
	// エネミーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_STATE
	{
		// 通常
		NEUTRAL_ACTION = 0,		// ニュートラル
		MOVE_ACTION,			// 移動
		DASH_ACTION,			// 高速移動
		JAMP_ACTION,			// ジャンプ
		LANDING_ACTION,			// 着地
		SQUAT_ACTION,			// しゃがむ
		AIM_ACTION,				// 構える
		STUN_ACTION,			// 気絶
		MAX_ACTION,				// 最大数
	};

	static const float	DEFAULT_ROTATION_FRICTION;			//ディフォルトの回転の摩擦係数


	void SetActionState(ACTION_STATE state);
	void Rotate();							// 回転
	void SetExclamationMarkDraw(const bool bDraw);						//UIの描画設定

	bool	m_bHeardSound;		// 音を聞いたかのフラグ

private:

	static const int	DEFAULT_AIM_DELAY;					//狙う時間
	static const int	DEFAULT_BURST_DELAY;				//
	static const int	DEFAULT_RECHARGE_DELAY;				//
	static const float	DEFAULT_BULLET_SPEED;				//ディフォルトの弾の速度

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	bool WasHitByBullet();					// 弾に当たったかどうかの判定
	void SetBulletSpeed();					// 弾の速度の設定
	const bool SingleShoot();				// 攻撃
	const bool DoubleShoot();				// 攻撃
	const bool TripleShoot();				// 攻撃

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	CMove						*m_pMove;						// 移動情報
	CCollision_Rectangle3D		*m_pCollision;					// 3D矩形の当たり判定
	D3DXVECTOR3					m_rotDest;						// 目的の向き
	D3DXVECTOR3					m_soundPos;						// 聞いた音の位置
	int							m_nNumMotion;					// 現在のモーション番号
	bool						m_isUse;						// 生きてるかどうか
	int							m_SendId;						// 送信するモデルのID
	int							m_nLife;						// ライフ
	int							m_nStun;						// ライフ

	int							m_nSecurityLevel;				// レベル
	float						m_fRotFriction;					// 回転の摩擦係数
	bool						m_bDeath;						// 死亡フラグ
	bool						m_bFound;						// ハッカーに確認されたかどうか
	bool						m_bOnMap;						// マップに表示されているかどうか
	bool						m_bDeathAnimation;						// マップに表示されているかどうか
	int							m_nAttackPattern;				// 攻撃パターン
	int							m_nNumHandPart;					// 手のモデルのインデックス
	int							m_nBurstBullet;					// はやく発生する弾
	int							m_nCntShoot;					// 弾の発生用の変数
	int							m_nBulletShot;					// 発生した弾の数
	int							m_nReloadTime;					// リロード時間
	CGun*						m_pGun;							// 銃へのポインタ
	CExclamationMark*			m_pAllertUi;					// ビックリマークのUI
};

#endif


