//=============================================================================
//
// task.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "task.h"
#include "taskUI.h"
#include "Score.h"
#include "game.h"
#include "AgentUI.h"
#include "hacker.h"
#include "HackerUI.h"



//コンストラクタ
CTask::CTask() : m_pUi(nullptr)
{
	for (int nCnt = 0; nCnt < AGENT_TASK_MAX; nCnt++)
	{
		m_nAgentObjectiveNum[nCnt] = 0;
		m_nAgentObjectiveMax[nCnt] = 0;
	}
	for (int nCnt = 0; nCnt < HACKER_TASK_MAX; nCnt++)
	{
		m_nHackerObjectiveNum[nCnt] = 0;
		m_nHackerObjectiveMax[nCnt] = 0;
	}
}

//デストラクタ
CTask::~CTask()
{

}

//初期化
HRESULT CTask::Init()
{
	m_pUi = CTaskUI::Create();		//UIの生成

	return S_OK;
}

//終了
void CTask::Uninit()
{
	//UIの破棄
	if (m_pUi)
	{
		m_pUi->Uninit();
		m_pUi = nullptr;
	}

	//メモリの解放
	CSuper::Release();
}

//更新
void CTask::Update()
{

}

//描画
void CTask::Draw()
{

}

//タスクの目的の追加
void CTask::AddTask(const AGENT_TASK_TYPE type)
{
	if (type >= (AGENT_TASK_TYPE)0 && type < AGENT_TASK_MAX)
	{
		m_nAgentObjectiveNum[(int)type]++;
		m_nAgentObjectiveMax[(int)type]++;
	}
}

//タスクの目的の削減
void CTask::SubtractTask(const AGENT_TASK_TYPE type)
{
	if (type >= (AGENT_TASK_TYPE)0 && type < AGENT_TASK_MAX && m_nAgentObjectiveNum[(int)type] > 0)
	{
		m_nAgentObjectiveNum[(int)type]--;

		if (m_pUi)
			m_pUi->UpdateTask(type);

		CAgentUI* pUiManager = CGame::GetUiManager();

		if (pUiManager)
		{
			pUiManager->AddScore(CScore::DEFAULT_AGENT_TASK_SCORE[type]);
		}
	}
}

//ハッカーのタスクの目的の追加
void CTask::AddHackerTask(const HACKER_TASK_TYPE type)
{
	if (type >= (HACKER_TASK_TYPE)0 && type < HACKER_TASK_MAX)
	{
		m_nHackerObjectiveNum[(int)type]++;
		m_nHackerObjectiveMax[(int)type]++;
	}
}

//ハッカーのタスクの目的の削減
void CTask::SubtractHackerTask(const HACKER_TASK_TYPE type)
{
	if (type >= (HACKER_TASK_TYPE)0 && type < HACKER_TASK_MAX && m_nHackerObjectiveNum[(int)type] > 0)
	{
		m_nHackerObjectiveNum[(int)type]--;

		if (m_pUi)
			m_pUi->UpdateTask(type);

		CHackerUI* pUiManager = CHacker::GetHackerUI();

		if (pUiManager)
		{
			pUiManager->AddScore(CScore::DEFAULT_HACKER_TASK_SCORE[type]);
		}
	}
}

//タスクの目的の最大数の取得
const int CTask::GetTaskObjectiveMax(const AGENT_TASK_TYPE type)
{
	int nMax = 0;

	if (type >= (AGENT_TASK_TYPE)0 && type < AGENT_TASK_MAX)
	{
		nMax = m_nAgentObjectiveMax[type];
	}

	return nMax;
}

//タスクの目的の最大数の取得(全部)
int* CTask::GetAllTaskObjectiveMax()
{
	return m_nAgentObjectiveMax;
}

//ハッカーのタスクの目的の最大数の取得
const int CTask::GetTaskObjectiveMax(const HACKER_TASK_TYPE type)
{
	int nMax = 0;

	if (type >= (HACKER_TASK_TYPE)0 && type < HACKER_TASK_MAX)
	{
		nMax = m_nHackerObjectiveMax[type];
	}

	return nMax;
}

//ハッカーのタスクの目的の最大数の取得(全部)
int* CTask::GetAllHackerTaskObjectiveMax()
{
	return m_nHackerObjectiveMax;
}

//タスクの目的の取得
const int CTask::GetTaskObjective(const AGENT_TASK_TYPE type)
{
	return m_nAgentObjectiveNum[type];
}

//タスクの目的の取得(全部)
int * CTask::GetAllTaskObjective()
{
	return m_nAgentObjectiveNum;
}

//ハッカーのタスクの目的の取得
const int CTask::GetTaskObjective(const HACKER_TASK_TYPE type)
{
	return m_nHackerObjectiveNum[type];
}

//ハッカーのタスクの目的の取得(全部)
int* CTask::GetHackerAllTaskObjective()
{
	return m_nHackerObjectiveNum;
}

//UIの生成処理
void CTask::CreateUI()
{
	if (m_pUi)
	{//nullチェック
		m_pUi->CreateText(this);
	}
}





//生成
CTask * CTask::Create()
{
	CTask* pTask = new CTask;	//インスタンスを生成する

	if (FAILED(pTask->Init()))
	{//初期化
		return nullptr;
	}

	return pTask;				//生成したインスタンスを返す
}
