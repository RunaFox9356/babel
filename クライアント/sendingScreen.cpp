//=============================================================================
//
// sendingScreen.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "sendingScreen.h"
#include "renderer.h"
#include "loadingBar.h"
#include "text.h"
#include "application.h"
#include "mapDataManager.h"
#include "task.h"
#include "hacker.h"
#include "drone.h"
#include "sound.h"


//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CSendingScreen::DEFAULT_POS =
{ (float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f };			//ディフォルトの位置
const D3DXVECTOR3	CSendingScreen::DEFAULT_SIZE =
{ (float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f };							//ディフォルトのサイズ
const D3DXCOLOR		CSendingScreen::DEFAULT_COLOR = { 0.2f, 0.2f, 0.2f, 0.5f };						//ディフォルトの色



//コンストラクタ
CSendingScreen::CSendingScreen() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL4),
m_nMaxLoad(0),
m_pLoadingBar(nullptr),
m_pText(nullptr),
m_pDrone(nullptr)
{

}

//デストラクタ
CSendingScreen::~CSendingScreen()
{

}

//初期化
HRESULT CSendingScreen::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	SetPos(DEFAULT_POS);			//位置の設定
	SetSize(DEFAULT_SIZE);			//サイズの設定
	SetColor(DEFAULT_COLOR);		//色の設定

	m_pLoadingBar = CLoadingBar::Create();		//ロードバーの生成

	{//テキスト

		//テキストの情報の設定
		D3DXVECTOR3 fontSize = D3DXVECTOR3(10.0f, 10.0f, 10.0f);				//フォントサイズ
		D3DXVECTOR3 size = D3DXVECTOR3(200.0f, 50.0f, 0.0f);					//サイズ
		D3DXVECTOR3 pos = D3DXVECTOR3(DEFAULT_POS.x - 100.0f, 530.0f, 0.0f);	//位置
		D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0, 0.2f, 1.0f);						//フォント色

		//テキストの生成
		m_pText = CText::Create(pos, size, CText::MAX, 1000, 5, "SENDING...", fontSize, color);	//テキストの生成
	}

	//ベクトルのクリア処理
	m_vSendDataIdx.clear();
	m_vSendDataIdx.shrink_to_fit();

	return S_OK;
}

//終了
void CSendingScreen::Uninit()
{
	//ロードバーの破棄
	if (m_pLoadingBar)
	{
		m_pLoadingBar->Uninit();
		m_pLoadingBar = nullptr;
	}

	//テキストの破棄
	if (m_pText)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	//ドローンへのポインタをnullに戻す
	if (m_pDrone)
		m_pDrone = nullptr;

	//ベクトルのクリア処理
	m_vSendDataIdx.clear();
	m_vSendDataIdx.shrink_to_fit();

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

//更新
void CSendingScreen::Update()
{
	//親クラスの更新処理
	CPolygon2D::Update();

	if (m_pLoadingBar && m_pLoadingBar->GetEnd())
	{
		//送信中のデータインデックスの更新
		m_nMaxLoad--;

		//現在のデータを確認する
		ControlObjIdx(m_nMaxLoad);

		if (m_nMaxLoad > 0)
			m_pLoadingBar->Reset();		//まだ送信できるデータがあったら、ロードアニメーションをリセットする
		else
		{
			//ドローンへのポインタがnullではなかったら、送信済み状態を設定する
			if (m_pDrone)
				m_pDrone->SetSendingState(false);

			//終了処理
			Uninit();
		}
	}
}

//描画
void CSendingScreen::Draw()
{
	//親クラスの描画処理
	CPolygon2D::Draw();
}




//=============================================================================
//
//									静的関数
//
//=============================================================================





//生成
CSendingScreen * CSendingScreen::Create(int nMaxLoad)
{
	CSendingScreen* pObj = new CSendingScreen;		//インスタンスを生成する

	//初期化処理ができなかった場合
	if (FAILED(pObj->Init()))
		return nullptr;

	pObj->m_nMaxLoad = nMaxLoad;					//ロードの回数の設定

	return pObj;									//生成したインスタンスを返
}

//生成
CSendingScreen * CSendingScreen::Create(std::vector<int> vData, CDrone* pDrone)
{
	CSendingScreen* pObj = new CSendingScreen;		//インスタンスを生成する

													//初期化処理ができなかった場合
	if (FAILED(pObj->Init()))
		return nullptr;

	pObj->m_nMaxLoad = (int)vData.size();			//ロードの回数の設定
	pObj->m_vSendDataIdx = vData;					//送信するデータのインデックスの設定
	pObj->m_pDrone = pDrone;						//ドローンへのポインタを保存する

	return pObj;									//生成したインスタンスを返
}




//=============================================================================
//
//								プライベート関数
//
//=============================================================================





//送信したデータのインデックスの確認処理
void CSendingScreen::ControlObjIdx(const int idx)
{
	if (idx < 0 || idx >= (int)m_vSendDataIdx.size())
		return;

	CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
	int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

	if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
	{//マップデータマネージャーのnullチェックとマップインデックスの確認

	 //ハッカーのターゲットのデータを取得する
		std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

		int nIdx = m_vSendDataIdx.data()[idx];

		if (nIdx >= 0 && nIdx < (int)vData.size() && vData.data()[nIdx].pObj && !vData.data()[nIdx].bGot)
		{//オブジェクトのポインタのnullチェックとエージェントにまだ見つかっていない場合

			pDataManager->SentObjData(CApplication::GetInstance()->GetMap(), m_vSendDataIdx.data()[idx]);

			CTask* pTask = nullptr;

			if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
				pTask = CHacker::GetTask();			//タスクの取得

			if (pTask)
			{//nullチェック
			 // サウンド情報の取得
				CSound *pSound = CApplication::GetInstance()->GetSound();
				pSound->PlaySound(CSound::SOUND_LABEL_SE_SEND);
				//タスクの更新
				pTask->SubtractHackerTask(vData.data()[nIdx].type);
			}
		}

	}
}
