//=============================================================================
//
// jammingTool.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "jammingTool.h"
#include "model_obj.h"
#include "polygon2D.h"
#include "application.h"
#include "renderer.h"
#include "hacker.h"
#include "drone.h"
#include "utility.h"


//=============================================================================
//								静的変数の初期化	
//=============================================================================
const int	CJammingTool::DEFAULT_MODEL_IDX = 59;					//ディフォルトのモデルインデックス
const float	CJammingTool::DEFAULT_MAX_JAMMING_DISTANCE = 500.0f;	//ディフォルトの最大有効距離
const float	CJammingTool::DEFAULT_MIN_JAMMING_DISTANCE = 100.0f;	//ディフォルトの最小有効距離


namespace
{
	float		s_fWidth = 1280.0f / 720.0f;		//スノーノイズのテクスチャの比率
	const int	DEFAULT_STATIC_TEXTURE_IDX = 54;	//ディフォルトのテクスチャインデックス
};




//コンストラクタ
CJammingTool::CJammingTool() : m_fCoeffA(0.0f),
m_fCoeffB(0.0f),
m_fCoeffC(0.0f),
m_pModel(nullptr),
m_pStatic(nullptr)
{

}

//デストラクタ
CJammingTool::~CJammingTool()
{

}

//初期化
HRESULT CJammingTool::Init()
{
	//モデルの生成
	m_pModel = CModelObj::Create();

	if (m_pModel)
	{//nullチェック

		m_pModel->SetType(DEFAULT_MODEL_IDX);		//モデルインデックの設定
	}

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{//ハッカーモードだったら、スノーノイズを生成する

		m_pStatic = CPolygon2D::Create(CSuper::PRIORITY_LEVEL4);		//スノーノイズの生成

		if (m_pStatic)
		{//nullチェック

			m_pStatic->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));	//位置の設定
			m_pStatic->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));					//サイズの設定
			m_pStatic->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));		//透明にする
			m_pStatic->LoadTex(DEFAULT_STATIC_TEXTURE_IDX);				//テクスチャインデックスの設定
		}
	}

	{//放物線の係数(f(x) = ax^2 + bx + c)を計算する

		//放物線の点
		D3DXVECTOR2 B = D3DXVECTOR2(DEFAULT_MAX_JAMMING_DISTANCE, 0.0f), A = D3DXVECTOR2(DEFAULT_MIN_JAMMING_DISTANCE, 1.0f);

		m_fCoeffA = (B.y - A.y) / ((B.x - A.x) * (B.x - A.x));			//A係数を計算する
		m_fCoeffB = -2.0f * m_fCoeffA * A.x;							//B係数を計算する
		m_fCoeffC = A.y + (m_fCoeffA * (A.x * A.x));					//C係数を計算する
	}

	return S_OK;
}

//終了
void CJammingTool::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}

	//スノーノイズの破棄
	if (m_pStatic)
	{
		m_pStatic->Uninit();
		m_pStatic = nullptr;
	}

	//メモリの解放
	Release();
}

//更新
void CJammingTool::Update()
{
	if (m_pStatic && CHacker::GetDrone())
	{//nullチェック

		//このオブジェクトとドローンの位置を取得する
		D3DXVECTOR3 pos = GetPos(), dronePos = CHacker::GetDrone()->GetPos();

		D3DXVECTOR3 distance = dronePos - pos;			//このオブジェクトからドローンまでの距離を計算する
		float fDist = D3DXVec3Length(&distance);		//上のベクトルの長さを計算する

		if (fDist < DEFAULT_MAX_JAMMING_DISTANCE)
		{//有効範囲内だったら

			if (fDist > DEFAULT_MIN_JAMMING_DISTANCE)
			{
				//α値を計算する
				float fAlpha = m_fCoeffA * (fDist * fDist) + m_fCoeffB * fDist + m_fCoeffC;

				//α値を計算する
				m_pStatic->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, fAlpha));
			}

			//スノーノイズのテクスチャ座標の更新
			UpdateStaticNoise();
		}
	}
}

//描画
void CJammingTool::Draw()
{

}

//位置のセッター
void CJammingTool::SetPos(const D3DXVECTOR3 &pos)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPos(pos);		//モデルの位置の設定
	}
}

//向きのセッター
void CJammingTool::SetRot(const D3DXVECTOR3 &rot)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetRot(rot);		//モデルの向きの設定
	}
}

//位置のゲッター
D3DXVECTOR3 CJammingTool::GetPos()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//nullチェック
		pos = m_pModel->GetPos();	//モデルの位置の取得
	}

	return pos;
}

//向きのゲッター
D3DXVECTOR3 CJammingTool::GetRot()
{
	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//nullチェック
		rot = m_pModel->GetRot();	//モデルの向きの取得
	}

	return rot;
}

// エフェクトのインデックスの設定
void CJammingTool::SetRenderMode(int mode)
{
	if (m_pModel)
	{
		m_pModel->SetRenderMode(mode);
	}
}

void CJammingTool::SetModelType(int nIdx)
{
	if (!m_pModel)
		return;

	if (nIdx < 0 || nIdx >= CModel3D::GetMaxModel())
		nIdx = 53;

	m_pModel->SetType(nIdx);
}




//=============================================================================
//
//								静的関数
//
//=============================================================================





//生成
CJammingTool* CJammingTool::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	CJammingTool* pObj = new CJammingTool;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);			//位置の設定
	pObj->SetRot(rot);			//向きの設定

	return pObj;				//生成したインスタンスを返
}




//=============================================================================
//
//							プライベート関数
//
//=============================================================================




//スノーノイズの更新
void CJammingTool::UpdateStaticNoise()
{
	if (m_pStatic)
	{//ハッカーに使われている場合

	 //ランダムでテクスチャ座標を設定する
		float fX = hmd::FloatRandam(0.0f, 1.0f), fY = hmd::FloatRandam(0.0f, 1.0f);

		m_pStatic->SetTex(0, D3DXVECTOR2(fX, fY), D3DXVECTOR2(fX + s_fWidth, fY + 1.0f));
	}
}