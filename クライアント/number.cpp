//=============================================================================
//
// 2Dio[(explosion2D.cpp)
// Author : ú±l
// Tv : 2Dio[¶¬ðs¤
//
//=============================================================================

//*****************************************************************************
// CN[h
//*****************************************************************************
#include <assert.h>

#include "number.h"
#include "renderer.h"
#include "application.h"



//=============================================================================
//								ÃIÏÌú»
//=============================================================================
const D3DXVECTOR3	CNumber::DEFAULT_DIGIT_SIZE = { 35.0f, 35.0f, 35.0f };		//fBtHgÌÌTCY



//=============================================================================
// CX^X¶¬
// Author : ú±l
// Tv : 2Dobgð¶¬·é
//=============================================================================
CNumber * CNumber::Create(int nPriority)
{
	// IuWFNgCX^X
	CNumber *pNumber = nullptr;

	pNumber = new CNumber(nPriority);

	if (pNumber != nullptr)
	{// lÌú»
		pNumber->Init();
	}
	else
	{// ÌmÛªÅ«È©Á½
		assert(false);
	}

	// CX^XðÔ·
	return pNumber;
}

//=============================================================================
// RXgN^
// Author : ú±l
// Tv : CX^X¶¬És¤
//=============================================================================
CNumber::CNumber(int nPriority) : CPolygon2D(nPriority),
m_digitSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
{
	m_nNumber = 0;
	m_nDigit = 1;
}

//=============================================================================
// fXgN^
// Author : ú±l
// Tv : CX^XI¹És¤
//=============================================================================
CNumber::~CNumber()
{

}

//=============================================================================
// ú»
// Author : ú±l
// Tv : eNX`ÌÝèµAoÏÌúlðÝè
//=============================================================================
HRESULT CNumber::Init()
{
	// IuWFNg2DÌú»
	CPolygon2D::Init();
	CPolygon2D::SetColor(D3DXCOLOR(0, 0, 0, 0));

	// eNX`ÌÝè
	/*std::vector<int> pTex;
	pTex.push_back(73);
	LoadTex(pTex);
	SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(0.1f, 1.0f));*/

	m_digitSize = DEFAULT_DIGIT_SIZE;

	for (int i = 0; i < NUMBER_MAX; i++)
	{
		m_pNumber[i] = CPolygon2D::Create(PRIORITY_LEVEL4);

		if (m_pNumber[i])
		{
			m_pNumber[i]->LoadTex(73);
			m_pNumber[i]->SetColor(D3DXCOLOR(1, 1, 1, 0));
			m_pNumber[i]->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(0.1f, 1.0f));
		}
	}

	return S_OK;
}

//=============================================================================
// I¹
// Author : ú±l
// Tv : eNX`Ì|C^Æ¸_obt@Ìðú
//=============================================================================
void CNumber::Uninit()
{// IuWFNg2DÌI¹
	CPolygon2D::Uninit();
}

//=============================================================================
// XV
// Author : ú±l
// Tv : 2DXVðs¤
//=============================================================================
void CNumber::Update()
{
	// IuWFNg2DÌXV
	CPolygon2D::Update();
}

//=============================================================================
// `æ
// Author : ú±l
// Tv : 2D`æðs¤
//=============================================================================
void CNumber::Draw()
{// vC[2DÌ`æ
	CPolygon2D::Draw();
}

//=============================================================================
// io[ÌÝè
// Author : ú±l
// Tv : io[ðÝèµÎ·éeNX`ÀWðÝè
//=============================================================================
void CNumber::SetNumber(int nNumber)
{
	// lÌÝè
	m_nNumber = nNumber;

	// eNX`ÀWÌ1ªÌlÌÝè
	float fTexX = 0.1f;

	// eNX`ÀWÌÝè
	m_pNumber[0]->SetTex(0,D3DXVECTOR2(0.0f + fTexX * (m_nNumber % 10), 0.01f), D3DXVECTOR2(fTexX + fTexX * (m_nNumber % 10), 1.0f));

	for (int i = 1; i < m_nDigit; i++)
	{//1ÌêÍ[v·çµÈ¢
		if (m_pNumber[i] != nullptr)
		{
			int number = (m_nNumber / (int)pow(10, i));
			m_pNumber[i]->SetTex(0,D3DXVECTOR2(0.0f + fTexX * number, 0.01f), D3DXVECTOR2(fTexX + fTexX * number, 1.0f));
			m_pNumber[i]->SetColor(D3DXCOLOR(0.0f,1.0f,1.0f,1.0f));
			m_pNumber[i]->SetShader(true, CPolygon2D::ERenderMode::Render_Blur);	// ¨µÅgÁÄÜ·@ÁµÄàOKÅ·
		}
	}
}

//=============================================================================
// ÌÝè
// Author : ú±l
// Tv : io[ðÝèµÎ·éeNX`ÀWðÝè
//=============================================================================
void CNumber::SetDigit(int nNumber)
{
	m_nDigit = nNumber;
	SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	SetPos(GetPos());
	SetSize(GetSize());
}

//=============================================================================
// ÌÝè
// Author : ú±l
// Tv : io[ðÝèµÎ·éeNX`ÀWðÝè
//=============================================================================
void CNumber::SetCol(D3DXCOLOR col)				// FÝè
{
	for (int i = 0; i < m_nDigit; i++)
	{//1ÌêÍ[v·çµÈ¢
		if (m_pNumber[i] != nullptr)
		{
			m_pNumber[i]->SetColor(col);
		}
	}
}

//=============================================================================
// ÌÝè
// Author : ú±l
// Tv : io[ðÝèµÎ·éeNX`ÀWðÝè
//=============================================================================
void CNumber::SetPos(const D3DXVECTOR3& pos)
{
	CPolygon2D::SetPos(pos);

	for (int i = 0; i < NUMBER_MAX; i++)
	{//1ÌêÍ[v·çµÈ¢
		if (m_pNumber[i] != nullptr)
		{
			m_pNumber[i]->SetPos(CPolygon2D::GetPos() - D3DXVECTOR3(GetSize().x + m_digitSize.x * (i), 0, 0));
		}
	}
}

//=============================================================================
// ÌÝè
// Author : ú±l
// Tv : io[ðÝèµÎ·éeNX`ÀWðÝè
//=============================================================================
void CNumber::SetSize(D3DXVECTOR3 siz)
{
	CPolygon2D::SetSize(siz);

	SetDigitSize(m_digitSize);
}

//ÌTCYÌZb^[
void CNumber::SetDigitSize(const D3DXVECTOR3 size)
{
	m_digitSize = size;

	for (int i = 0; i < NUMBER_MAX; i++)
	{//1ÌêÍ[v·çµÈ¢
		if (m_pNumber[i] != nullptr)
		{
			m_pNumber[i]->SetSize(m_digitSize);
		}
	}
}

// `ætOÌZb^[
void CNumber::SetDraw(const bool bDraw)
{
	for(int nCnt = 0; nCnt < NUMBER_MAX; nCnt++)
	{
		if (m_pNumber[nCnt])
		{
			m_pNumber[nCnt]->SetDraw(bDraw);
		}
	}
}
