#ifndef _FILEUTILITY_H_
#define _FILEUTILITY_H_

namespace fileutil
{
	// ssの状態をクリアし、読み取り位置を先頭に戻す処理
	void ssInitialize(std::stringstream& ss)
	{
		ss.clear();
		ss.seekg(0);
	}

	// JSONファイルのデータ読み込み関係
	void SafeLoad(nlohmann::json check, std::string str, int& value)
	{
		if (check[str].empty()) value = 0;	
		else value = check[str]; 
	}
	void SafeLoad(nlohmann::json check, std::string str, float& value) 
	{
		if (check[str].empty()) value = 0.0f;  
		else value = check[str]; 
	}
	void SafeLoad(nlohmann::json check, std::string str, bool& value)
	{ 
		if (check[str].empty()) value = false; 
		else value = check[str];
	}
	void SafeLoad(nlohmann::json check, std::string str, std::string& name)
	{ 
		if (check[str].empty()) name = "null";
		else name = check[str];
	}
	void SafeLoad(nlohmann::json check, std::string str, D3DXVECTOR3& value)
	{
		if (check[str].empty()) value = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		else value = D3DXVECTOR3(check[str]["X"], check[str]["Y"], check[str]["Z"]);
	}
	void SafeLoad(nlohmann::json check, std::string str, D3DXCOLOR& value)
	{
		if (check[str].empty()) value = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		else value = D3DXCOLOR(check[str]["R"], check[str]["G"], check[str]["B"], check[str]["A"]);
	}

	// テキストファイルのデータ読み込み関係
	void ReadVector3(std::stringstream& ss, std::string name, D3DXVECTOR3& vec)
	{
		std::vector<float> num;
		std::string str;
		std::string result;
		float value;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			ssInitialize(ss);

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			while (std::getline(ss, str, ','))
			{
				value = std::stof(str);
				num.push_back(value);
			}
		}

		if (num.size() == 3)
		{
			vec.x = num[0];
			vec.y = num[1];
			vec.z = num[2];
		}
	}

	void ReadColor(std::stringstream& ss, std::string name, D3DXCOLOR& color)
	{
		std::vector<float> num;
		std::string str;
		std::string result;
		float value;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			ssInitialize(ss);

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			while (std::getline(ss, str, ','))
			{
				value = std::stof(str);
				num.push_back(value);
			}
		}

		if (num.size() == 4)
		{
			color.r = num[0];
			color.g = num[1];
			color.b = num[2];
			color.a = num[3];
		}
	}
	bool ReadString(std::stringstream& ss, std::string name, std::string& tag)
	{
		std::vector<float> num;
		std::string str;
		std::string result;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			ssInitialize(ss);
			std::string str_tag;

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			if (ss >> str_tag)
			{
				tag = str_tag;
			}
		}
		else
		{
			return false;
		}

		return true;
	}

	bool ReadInt(std::stringstream& ss, std::string name, int& value)
	{
		std::string str;
		std::string result;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			ssInitialize(ss);

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			while (std::getline(ss, str, ','))
			{
				value = std::stoi(str);
			}
		}
		else
		{
			return false;
		}

		return true;
	}

	void ReadFloat(std::stringstream& ss, std::string name, float& value)
	{
		std::string str;
		std::string result;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			std::string str_random;

			ssInitialize(ss);

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			while (std::getline(ss, str, ','))
			{
				value = std::stof(str);
			}
		}
	}

	bool ReadBool(std::stringstream& ss, std::string name, bool& value)
	{
		std::string str;
		std::string result;

		// ：が見つかるまでの文字を抽出
		size_t pos = ss.str().find(":");
		if (pos != std::string::npos)
		{
			result = ss.str().substr(0, pos);
		}

		ssInitialize(ss);
		if (ss >> str && result == name)
		{
			ssInitialize(ss);
			std::string str_boolean;

			// 先頭から":"まで読み飛ばす
			std::getline(ss, str, ':');

			if (ss >> str_boolean)
			{
				value = (str_boolean == "true");
			}
		}
		else
		{
			return false;
		}
		return true;
	}
}

#endif