//=============================================================================
//
// respawnLoading.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _RESPAWN_LOADING_H
#define _RESPAWN_LOADING_H


//=============================================================================
// インクルード
//=============================================================================
#include "emptyObj.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CLoadingBar;
class CText;
class CDrone;


class CRespawnLoading : public CEmptyObj
{
public:
	CRespawnLoading();							//コンストラクタ
	~CRespawnLoading() override;				//デストラクタ

	HRESULT Init() override;					//初期化
	void Uninit() override;						//終了
	void Update() override;						//更新
	void Draw() override;						//描画

	void SetNewMessage(char* pText);			//新しいテキストの設定

	const bool GetEnd() { return m_bEnd; }		//終了したかどうかの取得

	static CRespawnLoading* Create(const int nRespawnTime);		//生成

private:

	void ChangeText();							//テキストの変更処理

private:

	static const int			DEFAULT_RESPAWN_TIME;		//ディフォルトのリスポーンフレーム数
	static const D3DXVECTOR3	DEFAULT_POS;				//ディフォルトの位置
	static const D3DXVECTOR3	DEFAULT_SIZE;				//ディフォルトのサイズ

	int				m_nTime;					//リスポーンフレーム数
	bool			m_bEnd;						//終了したかどうか
	bool			m_bChange;					//終了後、テキストを変えるかどうか

	char*			m_pNewMessage;				//新しいメッセージ
	CLoadingBar*	m_pBar;						//ロードバー
	CText*			m_pText;					//テキスト
};

#endif