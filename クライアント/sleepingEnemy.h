//=============================================================================
//
// sleepingEnemy.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SLEEPING_ENEMY_H
#define _SLEEPING_ENEMY_H


//=============================================================================
// インクルード
//=============================================================================
#include "enemy.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CViewField;
class CSoundSource;


class CSleepingEnemy : public CEnemy
{
public:
	CSleepingEnemy();								//コンストラクタ
	~CSleepingEnemy() override;						//デストラクタ

	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新

	void SetEyePos(const D3DXVECTOR3 eyePos);		//目の相対位置の設定
	void SetSecurityLevel(const int nSecurity) override;	//セキュリティーレベルのセッター
	void Stun() override;							//スタン

	static CSleepingEnemy* Create(const D3DXVECTOR3 pos, D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f), float fHearRadius = DEFAULT_HEARING_RADIUS);			//生成

private:

	void UpdateState();								//状態によって更新
	void Sleep();									//眠る
	void Wake();									//起きる
	void Search();									//音の方を確認する
	void LookAround();								//周りを見る
	void Attack(const bool isOnline = false);		//弾を撃つ
	void LookAtPlayer(bool isOnline = false);		//プレイヤーのほうに向かう
	bool Move(const D3DXVECTOR3 targetPos);			//移動
	void Chase();									//プレイヤーを追いかける
	void Return();									//スポーンの位置に戻る
	void HeardSound();								//音を聞いた

private:


	//挙動の状態
	enum EState
	{
		STATE_SLEEP = 0,

		STATE_WAKE,
		STATE_SEARCH,
		STATE_LOOK_AROUND,

		STATE_ATTACK,
		STATE_ONLINEATTACK,

		STATE_CHASE,
		STATE_RETURN,

		STATE_HEARD_SOUND,

		STATE_MAX
	};



	static const D3DXVECTOR3		EYE_RELATIVE_POS;			//目の相対位置
	static const float				DEFAULT_HEARING_RADIUS;		//ディフォルトのプレイヤー聞こえる範囲の半径
	static const D3DXVECTOR3		DEFAULT_COLLISION_POS;		//ディフォルトの当たり判定の相対位置
	static const D3DXVECTOR3		DEFAULT_COLLISION_SIZE;		//ディフォルトの当たり判定のサイズ
	static const int				DEFAULT_HAND_INDEX;			//ディフォルトの手のモデルのインデックス
	static const int				DEFAULT_WAKE_FRAMES;		//起きる前のフレーム数
	static const float				DEFAULT_ROTATION_FRICTION;	//ディフォルトの回転の摩擦係数
	static const float				ATTACK_ROTATION_FRICTION;	//攻撃の時の回転の摩擦係数
	static const int				DEFAULT_WAIT_FRAMES;		//ディフォルトの待つフレーム数
	static const float				LOOK_AROUND_ANGLE;			//周りを見る時のディフォルトの最大角度


	D3DXVECTOR3		m_startingPos;			//スポーンの位置
	D3DXVECTOR3		m_targetPos;			//音が聞こえた位置
	int				m_nCntAnim;				//アニメーション用の変数
	int				m_nCntPhase;			//アニメーション用の変数
	float			m_fLookAroundAngle;		//周りを見る時の最大角度
	float			m_fOriginalRot;			//元の向き
	float			m_fAnimRot;				//アニメーション用の変数
	float			m_fHearRadius;			//プレイヤー聞こえる範囲の半径
	bool			m_bSeen;				//プレイヤーを見たかどうか	
	bool			m_bChased;				//プレイヤーを追いかけたかどうか

	EState			m_state;				//現在の状態

	CViewField*		m_pView;				//視野へのポインタ
	CSoundSource*	m_pSoundSource;			//足音

};


#endif