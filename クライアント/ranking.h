//=============================================================================
//
// ranking.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _RANKING_H_		// このマクロ定義がされてなかったら
#define _RANKING_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "manager.h"

//=============================================================================
// 前方宣言
//=============================================================================



class CRanking : public CManager
{
public:

	CRanking();								//コンストラクタ
	~CRanking() override;					//デストラクタ

	HRESULT Init() override;				//初期化
	void Uninit() override;					//終了
	void Update() override;					//更新
	void Draw() override;					//描画

	const int GetRank(const int nScore);	//スコアの設定処理
	void GetLastScoreAndRank(int& nScore, int& nRank);		//前回のスコアとランクの取得
	void GetAllScores(std::vector<int>& vScore);			//全部のスコアの取得処理
	void SaveCompletedTask(std::vector<int> vTask, const bool bAgent);			//達成したタスクの保存処理
	void GetCompletedTask(std::vector<int>& vTask, bool& bAgent);			//達成したタスクの取得処理
	void ClearLastScoreAndRank();			//前回のスコアとランクのクリア

	static CRanking* Create();				//生成

private:

	void SaveRanking();						//ランキングを並び替える

private:

	static const char*	DEFAULT_RANKING_FILE_PATH;		//ディフォルトのランキングファイルのパス
	static const int	DEFAULT_RANKING_NUM = 5;		//保存するスコアの最大数

	int		m_aScore[2][DEFAULT_RANKING_NUM];			//保存するスコア
	int		m_nLastScore;								//前回のスコア
	int		m_nLastRank;								//前回のスコアのランキング

	std::vector<int>		m_vAgentTask;				//達成したエージェントタスク
	std::vector<int>		m_vHackerTask;				//達成したハッカータスク
	bool					m_bAgent;					//エージェントモードだったかどうか

};

#endif