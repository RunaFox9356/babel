//=============================================================================
//
// 3Dモデルクラス(model3D.h)
// Author : 唐�ｱ結斗
// 概要 : 3Dモデル生成を行う
//
//=============================================================================
#ifndef _MODEL_H_			// このマクロ定義がされてなかったら
#define _MODEL_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <vector>
#include "object.h"
#include "main.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CShadowVolume;
class CCollision_Sphere;

//=============================================================================
// 3Dモデルクラス
// Author : 唐�ｱ結斗
// 概要 : 3Dモデル生成を行うクラス
//=============================================================================
class CModel3D
{
public:
	// 頂点フォーマット
	const unsigned int	FVF_VERTEX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE);

	//*****************************************************************************
	// 構造体定義
	//*****************************************************************************
	// 頂点データ
	typedef struct
	{
		D3DXVECTOR3			pos;			// 頂点座標
		D3DXVECTOR3			nor;			// 法線ベクトル
		D3DCOLOR			col;			// 頂点カラー
	}VERTEX_3D;

	//--------------------------------------------------------------------
	// モデルのマテリアル情報
	// Author : 唐�ｱ結斗
	// 概要 : モデルのマテリアル情報の設定
	//--------------------------------------------------------------------
	struct MODEL_MATERIAL
	{
		LPD3DXMESH		pMesh;				// メッシュ情報へのポインタ
		LPD3DXBUFFER	pBuffer;			// マテリアル情報へのポインタ
		DWORD			nNumMat;			// マテリアル情報の数
		D3DXVECTOR3		size;				// モデルの大きさ
		std::vector<int>	pNumTex;			// テクスチャタイプ
		int				nindex;
		bool			hit;
		std::string		aFileName;	// Xファイルのパス
	};

	enum ERenderMode
	{
		Render_Default,
		Render_Highlight,
		Render_Hologram,
		Render_Glass,
		Render_MAX
	};

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CModel3D *Create();										// 3Dモデルの生成
	static void InitModel();										// モデルの初期化
	static void UninitModel();										// モデルの終了
	static void LoadModel(const char *pFileName);					// モデルの読み込み
	static void LoadModelAll(std::function<void()> func);
	static void LoadModelAllNo();
	static void SaveModelAll();
	static std::map<std::string, CModel3D::MODEL_MATERIAL, std::less<>> GetMaterial() { return m_ModelMap; }		// マテリアル情報の取得
	static int GetMaxModel() { return m_nMaxModel; }				// モデル数 
	static MODEL_MATERIAL LoadMaterial(std::string pass);								// 親モデルのゲッター
	static bool CollisonModel(CObject *pTarget, const D3DXVECTOR3 relativePos, bool& bLanding);
	static bool SearchCollison(CObject *pTarget, const float fTargetRange, const D3DXVECTOR3 relativePos, bool& bLanding);

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CModel3D();
	~CModel3D();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init();																			// 初期化
	void Uninit();																			// 終了
	void Update();																			// 更新
	void Draw();																			// 描画
	void Draw(D3DXMATRIX mtxParent);														// 描画(オーバーロード)
	void DrawMaterial();																	// マテリアル描画
	void SetPos(const D3DXVECTOR3 &pos);													// 位置のセッター
	void SetRot(const D3DXVECTOR3 &rot);													// 向きのセッター
	void SetDir(const D3DXVECTOR3 &dir) { m_dir = dir; }									// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size);													// 大きさのセッター
	void SetMtxWorld(D3DXMATRIX mtxWorld) { m_mtxWorld = mtxWorld; }						// ワールドマトリックスの設定
	void SetParent(CModel3D *pParent) { m_pParent = pParent; }								// 親モデルのセッター
	void SetModelID(const int nModelID); 													// モデルID
	void SetModelKEY(const std::string KEY);												// モデルID
	void SetColor(const D3DXCOLOR color);													// カラーの設定
	void SetColor(bool bColor) { m_bColor = bColor; }										// カラーの設定(オーバーロード)
	void SetShadow(bool bShadow) { m_bShadow = bShadow; }									// 影の使用状況の設定
	void SetLighting(bool bLighting) { m_bLighting = bLighting; }							// ライトを使用状況の設定
	void SetRenderMode(ERenderMode mode) { m_renderMode = mode; }
	void SetStencilShadowFlag(bool bStencilShadow) { m_bStencilShadow = bStencilShadow; }	// ステンシルシャドウの使用フラグの設定
	void SetIgnoreDistance(const bool bIgnore) { m_bIgnoreDistance = bIgnore; }				// 距離を無視するかどうかのセッター
	D3DXVECTOR3 GetPos() { return m_pos; }													// 位置のゲッター
	D3DXVECTOR3 GetRot() { return m_rot; }													// 向きのゲッター
	D3DXVECTOR3 GetSize() { return m_size; }												// 大きさのゲッター
	MODEL_MATERIAL GetMyMaterial(); 														// マテリアル情報の取得
	D3DXMATRIX GetMtxWorld() { return m_mtxWorld; }											// ワールドマトリックスの取得
	CModel3D *GetParent() { return m_pParent; }												// 親モデルのゲッター
	int GetModelID() { return m_nModelID; }
	bool Collison(CObject *pTarget, const D3DXVECTOR3 relativePos);							// 当たり判定
	bool Collison(CObject *pTarget, const D3DXVECTOR3 relativePos, bool& bLanding);			// 当たり判定

#ifdef _DEBUG
	void SetPolygonTestFlag(bool bPolygonTest) { m_bPolygonTest = bPolygonTest; }
#endif // _DEBUG

private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void Shadow();		// 影の描画
	void StencilShadow();
	void RenderDefault(LPD3DXEFFECT& effect, UINT pass = 0);
	void RenderHighLight(LPD3DXEFFECT& effect);
#ifdef _DEBUG
	void SetPolygonTest();
#endif // _DEBUG


	//--------------------------------------------------------------------
	// 静的メンバ変数
	//--------------------------------------------------------------------
	static int				m_nMaxModel;		// モデル数			
	static std::map<std::string, MODEL_MATERIAL, std::less<>> m_ModelMap; //TODO:スタティック
	static 	std::vector<std::string> m_KeyMap; //TODO:スタティック
	static std::vector<CModel3D*> m_pModelGroup;	// 使用されているモデルの総数
	static std::vector<CModel3D*> m_pSearchModel;	// 当たり判定可能なモデル

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	std::vector<CShadowVolume*>	m_pShadowVolume;	// シャドウボリューム
	CCollision_Sphere			*m_pCollisionSphere;
	CModel3D					*m_pParent;			// 親モデルの情報
	D3DXMATRIX					m_mtxWorld;			// ワールドマトリックス
	D3DXMATRIX					m_mtxWorldOld;		// ワールドマトリックス
	D3DXVECTOR3					m_pos;				// 位置
	D3DXVECTOR3					m_rot;				// 向き
	D3DXVECTOR3					m_dir;				// 向き
	D3DXVECTOR3					m_size;				// 大きさ
	D3DXCOLOR					m_color;			// カラー
	int							m_nModelID;			// モデルID
	int							m_nRenderTime;
	std::string					m_key;				// Key
	bool						m_isKeySet;			// keyをセットしてない時
	bool						m_bColor;			// カラーを使用する
	bool						m_bShadow;			// 影の使用状況
	bool						m_bStencilShadow;	// ステンシルシャドウの使用フラグ
	bool						m_bLighting;		// ライトの使用状況
	bool						m_bIgnoreDistance;	//距離を無視するかどうか
	MODEL_MATERIAL				*m_model;
	ERenderMode					m_renderMode;

#ifdef _DEBUG
	bool m_bPolygonTest;
#endif // _DEBUG

	//マップ

	//=========================================
	//ハンドル一覧
	//=========================================
	struct Handler
	{
		IDirect3DTexture9*	tex0;	// テクスチャ保存用
		D3DXHANDLE			WVP;
		D3DXHANDLE			vLightDir;	// ライトの方向
		D3DXHANDLE			vDiffuse;	// 頂点色
		D3DXHANDLE			vAmbient;	// 頂点色
		D3DXHANDLE			vCameraPos;
		D3DXHANDLE			Technique;	// テクニック
		D3DXHANDLE			Texture;		// テクスチャ
		D3DXHANDLE time;
		D3DXHANDLE isUseTexture;
	};
	Handler m_handler;
};

#endif