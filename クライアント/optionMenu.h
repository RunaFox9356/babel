//=============================================================================
//
// optionMenu.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _OPTION_MENU_H_		// このマクロ定義がされてなかったら
#define _OPTION_MENU_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CPolygon2D;
class CSlider;
class CText;
class CMenuButton;


class COptionMenu : public CObject
{
public:

	COptionMenu();													//コンストラクタ
	~COptionMenu() override;										//デストラクタ

	HRESULT Init() override;										//初期化
	void Uninit() override;											//終了
	void Update() override;											//更新
	void Draw() override;											//描画

	void SetPos(const D3DXVECTOR3 &/*pos*/) override {}				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッタ
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {}				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {}			// 大きさのセッター
	D3DXVECTOR3 GetPos() override;									// 位置のゲッター
	D3DXVECTOR3 GetPosOld() override { return D3DXVECTOR3(); }		// 過去位置のゲッタ
	D3DXVECTOR3 GetRot() override { return D3DXVECTOR3(); }			// 向きのゲッター
	D3DXVECTOR3 GetSize() override;									// 大きさのゲッター


	static COptionMenu*	Create();			//生成

private:

	void MouseSensibilityUpdate();			//マウス感度の更新
	void AimSensibilityUpdate();			//狙うモードのマウス感度の更新
	void FontUpdate();						//フォントの更新
	void ChangeTextFont();					//テキストのフォントの変更

	CText* ChangeText(CText* pText, std::string str);		//テキストの変更

	void SaveChanges();						//設定のセーブ処理
	void LoadOptions(float& fMouseSensibility, float& fAimSensibility);						//設定のロード処理

private:

	static const int			DEFAULT_BACKGROUND_TEXTURE_IDX;				//ディフォルトの背景のテクスチャインデックス
	static const D3DXCOLOR		DEFAULT_TEXT_COLOR;							//ディフォルトのテキストの色
	static const D3DXVECTOR3	DEFAULT_FONT_SIZE;							//ディフォルトのフォントサイズ
	static const int			DEFAULT_DELAY;								//ディフォルトのディレイ
	static const char*			DEFAULT_FILE_PATH;							//設定の外部ファイルのパス

	//========================================================================================================
	//										マウス感度の設定(普通)
	static const D3DXVECTOR3	DEFAULT_MOUSE_SENSIBILITY_SLIDER_POS;		//マウス感度のスライダーの位置
	static const D3DXVECTOR3	DEFAULT_MOUSE_SENSIBILITY_SLIDER_SIZE;		//マウス感度のスライダーのサイズ
	static const D3DXVECTOR3	DEFAULT_MOUSE_SENSIBILITY_TEXT_POS;			//マウス感度のスライダーのテキストの位置
	static const D3DXVECTOR3	DEFAULT_MOUSE_SENSIBILITY_VALUE_POS;		//マウス感度のスライダー値の位置
	static const float			DEFAULT_MOUSE_SENSIBILITY_MIN;				//ディフォルトのマウス感度の最小値
	static const float			DEFAULT_MOUSE_SENSIBILITY_MAX;				//ディフォルトのマウス感度の最大値
	//========================================================================================================
	//========================================================================================================
	//										マウス感度の設定(狙うモード)
	static const D3DXVECTOR3	AIM_MOUSE_SENSIBILITY_SLIDER_POS;			//マウス感度のスライダーの位置
	static const D3DXVECTOR3	AIM_MOUSE_SENSIBILITY_SLIDER_SIZE;			//マウス感度のスライダーのサイズ
	static const D3DXVECTOR3	AIM_MOUSE_SENSIBILITY_TEXT_POS;				//マウス感度のスライダーのテキストの位置
	static const D3DXVECTOR3	AIM_MOUSE_SENSIBILITY_VALUE_POS;			//マウス感度のスライダー値の位置
	static const float			AIM_MOUSE_SENSIBILITY_MIN;					//ディフォルトのマウス感度の最小値
	static const float			AIM_MOUSE_SENSIBILITY_MAX;					//ディフォルトのマウス感度の最大値
	//========================================================================================================
	//========================================================================================================
	//										フォントの設定
	static const D3DXVECTOR3	DEFAULT_FONT_TEXT_POS;						
	static const D3DXVECTOR3	DEFAULT_FONT_BUTTON_POS;					
	static const D3DXVECTOR3	DEFAULT_FONT_BUTTON_SIZE;					//フォントのテキストの位置
	//======================================================================//フォントのボタンの位置(相対(背景は親))==================================
																			//フォントのボタンのサイズ
	CPolygon2D*		m_pBg;								//背景へのポインタ
	CPolygon2D*		m_pLog;
	int				m_nDelay;							//ディレイのカウンター
	int				m_nFont;							//フォントの種類

	CSlider*		m_pMouseSensibility;				//マウス感度のスライダー
	CText*			m_pMouseSensibilityText;			//マウス感度のテキスト
	CText*			m_pMouseSensibilityValue;			//マウス感度の値のテキスト

	CSlider*		m_pAimMouseSensibility;				//マウス感度のスライダー
	CText*			m_pAimMouseSensibilityText;			//マウス感度のテキスト
	CText*			m_pAimMouseSensibilityValue;			//マウス感度の値のテキスト

	CText*			m_pFontText;						//フォントのテキスト
	CMenuButton*	m_pFontButton;						//フォントのボタン
};

#endif