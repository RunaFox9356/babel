#include "MiniRhythm.h"
#include"polygon2D.h"
#include "renderer.h"
#include "input.h"
#include "application.h"
#include "scene_mode.h"
#include "MiniGameMNG.h"
#include "sound.h"
#include "text.h"
namespace
{
	const float laneWidth = 200.0f;
	const float notesWidth = 160.0f;
}

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CMiniRhythm *CMiniRhythm::Create(int count)
{
	// オブジェクトインスタンス
	CMiniRhythm *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CMiniRhythm(count);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}


//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CMiniRhythm::CMiniRhythm(int nPriority):CMiniGame(nPriority),
m_pCommandUI(nullptr)
{
}

//=============================================================================
// デストラクタ
// Author : 有田明玄
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CMiniRhythm::~CMiniRhythm()
{
}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CMiniRhythm::Init()
{
	CMiniGame::Init();
	m_up = false;
	//モニター
	SetPos(D3DXVECTOR3(1280.0f * 0.5f, 720.0f * 0.5f, 0.0f));
	SetSize(D3DXVECTOR3(960.0f, 720.0f, 0.0f));
	SetColor(D3DXCOLOR(0.0f, 0.0f, 0.6f, 1.0f));
	for (int i = 0; i < 2; i++)
	{
		m_Lane[i] = CPolygon2D::Create();
		m_Lane[i]->SetPos(D3DXVECTOR3(640.0f - 100.0f + 200.0f * i, 360.0f, 0.0f));
		m_Lane[i]->SetSize(D3DXVECTOR3(laneWidth, 720.0f, 0));
		m_Lane[i]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_Lane[i]->LoadTex(69);
	}
	nCount = 0;	//タイマーリセット
	m_Notes = nullptr;

	//判定ライン
	m_Line = CPolygon2D::Create();
	m_Line->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT - 50.0f, 0.0f));
	m_Line->SetSize(D3DXVECTOR3(500.0f, 100.0f, 0));
	m_Line->SetColor(D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f));

	//操作のUIを生成する
	m_pCommandUI = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

	if (m_pCommandUI)
	{//nullチェック

		m_pCommandUI->SetPos(D3DXVECTOR3(300.0f, 450.0f, 0.0f));
		m_pCommandUI->SetSize(D3DXVECTOR3(300.0f, 100.0f, 0.0f));
		m_pCommandUI->SetColor(D3DXCOLOR(0.75f, 0.75f, 0.75f, 1.0f));
		m_pCommandUI->LoadTex(106);
	}

	m_pText = CText::Create(D3DXVECTOR3(420.0f, 80.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "壁に当たらずゴールを目指せ！！",
		D3DXVECTOR3(15.0f, 30.0f, 20.0f), D3DXCOLOR(1.0f, 0.1f, 0.1f, 1.0f), CFont::FONT_SOUEIKAKU);
	m_pText->TexChange("タイミングよくボタンを押せ！！");
	m_nScore = 0;//スコアリセット
	return S_OK;
}
//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CMiniRhythm::Uninit()
{

	CMiniGame::Uninit();

	if (m_Notes!=nullptr)
	{
		m_Notes->Uninit();	//ノーツ
	}

	if (m_Line != nullptr)
	{
		m_Line->Uninit();	//ノーツ
	}

	if (m_pText != nullptr)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	//UIを破棄する
	if (m_pCommandUI)
	{
		m_pCommandUI->Uninit();
		m_pCommandUI = nullptr;
	}
	
	for (int i = 0; i < 2; i++)
	{
		if (m_Lane[i] != nullptr)
		{
			m_Lane[i]->Uninit();	//ノーツ
		}
	}
}

//=============================================================================
// 更新
// Author : 有田明玄
// 概要 : 更新を行う
//=============================================================================
void CMiniRhythm::Update()
{
	CMiniGame::Update();
	if (m_Notes == nullptr)
	{
		m_Notes = CPolygon2D::Create();
		m_Notes->SetSize(D3DXVECTOR3(notesWidth, 16, 0));
		m_Notes->SetPos(D3DXVECTOR3(m_Lane[(rand() % 2)]->GetPos().x, 50, 0));
		m_Notes->SetColor(D3DXCOLOR(1.0f,1.0f, 1.0f,1.0f));
		m_Notes->LoadTex(70);
	}
	else if (m_Notes != nullptr)
	{
		m_Notes->SetPos(D3DXVECTOR3(m_Notes->GetPos().x, m_Notes->GetPos().y + 10, 0));	//移動
		
		if (m_Notes->GetPos().y >= CRenderer::SCREEN_HEIGHT)
		{
			m_Notes->Uninit();
			m_Notes = nullptr;
		}
		CInput *pInput = CInput::GetKey();
		if (pInput->Trigger(DIK_SPACE) && m_Notes != nullptr)
		{
			Judge();
		}
	}

	if (GetFlagDeath())
	{
		return;
	}

	D3DXCOLOR color = m_pCommandUI->GetColor();
	if (m_up)
	{
		color.a += 0.01f;
	}
	else
	{
		color.a -= 0.01f;
	}

	if (color.a >= 1.0f)
	{
		m_up = false;
	}
	else if (color.a <= 0.5f)
	{
		m_up = true;
	}
	m_pCommandUI->SetColor(color);

}

//=============================================================================
// 描画
// Author : 有田明玄
// 概要 : 描画を行う
//=============================================================================
void CMiniRhythm::Draw()
{
	CMiniGame::Draw();
}

//=============================================================================
// 判定
// Author : 有田明玄
// 概要 : ノーツとラインが重なっている時のみ成功判定を出す
//=============================================================================
void CMiniRhythm::Judge()
{
	if (m_Notes->GetPos().y - m_Notes->GetSize().y / 2 <= m_Line->GetPos().y + m_Line->GetSize().y / 2 &&
		m_Notes->GetPos().y + m_Notes->GetSize().y / 2 >= m_Line->GetPos().y - m_Line->GetSize().y / 2)
	{
		CSound *pSound = CApplication::GetInstance()->GetSound();
		pSound->PlaySound(CSound::SOUND_LABEL_SE_HIT);
		if (m_Notes != nullptr)
		{
			m_Notes->Uninit();
			m_Notes = nullptr;
		}
		if (++m_nScore >= CLEAR_SCORE)
		{//クリア
			CMiniGameMNG* pMng =  CApplication::GetInstance()->GetSceneMode()->GetMiniMng();
			pMng->SetClear(true);
			pSound->PlaySound(CSound::SOUND_LABEL_SE_OPEN);
			return Uninit();

		}
	}
}
