//============================
//
// マップチップヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MAP_H_
#define _MAP_H_

//**************************************************
// インクルード
//**************************************************
#include "manager.h"
#include "mapDataManager.h"


//**************************************************
// 前方宣言
//**************************************************
class CMiniGameMNG;
class CSceneMode;

//**************************************************
// クラス
//**************************************************
class CMap : public CManager
{
public:

	explicit CMap();									//コンストラクタ
	~CMap() override;									//デストラクタ
														
	virtual HRESULT Init() override;					//初期化
	virtual void Uninit() override;						//終了
	virtual void Update() override;						//更新
	virtual void Draw() override;						//描画

	void CreateMapObj();								//マップオブジェクトの生成
														
	//static CMap* CMap::Create();						
	static CMap* CMap::Create(int nMapIdx);				//生成
	int AddId() { m_itemId++; return m_itemId; }		//IDの追加
private:

	void CreateMeshfieldFromData(CMapDataManager::MESHFIELD_DATA data, const int nMapIdx);		//読み込んだデータからメッシュフィールドを生成する
	void CreateModelFromData(CMapDataManager::MODEL_DATA data, const int nMapIdx);				//読み込んだデータからモデルを生成する
	void CreateEnemyFromData(CMapDataManager::ENEMY_DATA data, const int nMapIdx);				//読み込んだデータからエネミーを生成する
	void CreateGimmickFromData(CMapDataManager::GIMMICK_DATA data, const int nMapIdx);			//読み込んだデータからギミックを生成する
	void CreateWallFromData(CMapDataManager::WALL_DATA data, const int nMapIdx);				//読み込んだデータから壁を生成する

private:
	int m_nMapIdx;			//マップのインデックス
	int m_itemId;			//オブジェクトのID
};



#endif	// _OBJECT2D_H_
