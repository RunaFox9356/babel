//=============================================================================
//
// menuButton.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "menuButton.h"
#include "input.h"
#include "application.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXCOLOR		CMenuButton::DEFAULT_NORMAL_COLOR = { 1.0f, 1.0f, 0.0f, 1.0f };			//ディフォルトの普通色
const D3DXCOLOR		CMenuButton::DEFAULT_MOUSE_OVER_COLOR = { 1.0f, 0.0f, 0.0f, 1.0f };		//ディフォルトのマウスカーソルと重なっている時の色
const D3DXVECTOR3	CMenuButton::DEFAULT_SIZE = { 400.0f, 75.0f, 0.0f };					//ディフォルトのサイズ



//コンストラクタ
CMenuButton::CMenuButton() : m_relativePos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_normalColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
m_mouseOverColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
m_bMouseOver(false),
m_bTrigger(false),
m_pParent(nullptr)
{

}

//デストラクタ
CMenuButton::~CMenuButton()
{

}

// 初期化
HRESULT CMenuButton::Init()
{
	CPolygon2D::Init();		//基本クラスの初期化処理

	m_normalColor = DEFAULT_NORMAL_COLOR;			//色の初期化
	m_mouseOverColor = DEFAULT_MOUSE_OVER_COLOR;	//色の初期化

	SetSize(DEFAULT_SIZE);				//サイズの初期化
	SetColor(m_normalColor);			//色の設定

	return S_OK;
}

// 終了
void CMenuButton::Uninit()
{
	CPolygon2D::Uninit();		//基本クラスの終了処理
}

// 更新
void CMenuButton::Update()
{
	CInput* pInput = CApplication::GetInstance()->GetInput();		//インプットの取得

	D3DXVECTOR3 mousePos = pInput->GetMouseCursor(), pos = m_relativePos, size = GetSize();

	if (m_pParent)
		pos += m_pParent->GetPos();

	//ボタンの辺の座標を計算する
	float fRight, fLeft, fTop, fBottom;
	fRight = pos.x + (size.x * 0.5f);
	fLeft = pos.x - (size.x * 0.5f);
	fTop = pos.y - (size.y * 0.5f);
	fBottom = pos.y + (size.y * 0.5f);
	
	if (!m_bMouseOver)
	{//前回のフレーム、マウスカーソルと重なっていなかった場合

		if (mousePos.x > fLeft && mousePos.x < fRight && mousePos.y < fBottom && mousePos.y > fTop)
		{//今回マウスカーソルと重なっている場合

			m_bMouseOver = true;		//フラグをtrueにする

			SetColor(m_mouseOverColor);	//色を変える
		}
	}
	else
	{//前回のフレーム、マウスカーソルと重なっていた場合

		if (mousePos.x < fLeft || mousePos.x > fRight || mousePos.y > fBottom || mousePos.y < fTop)
		{//今回マウスカーソルと重なっていない場合

			m_bMouseOver = false;		//フラグをfalseにする

			SetColor(m_normalColor);	//色を変える
		}
		else
		{//まだ重なっていて、マウスの左ボタンをクリックした場合

			if (pInput->Trigger(MOUSE_INPUT_LEFT))
			{
				m_bTrigger = true;		//トリガーのフラグをtrueにする
			}
		}
	}

	CPolygon2D::Update();				//基本クラスの更新処理
}

// 描画
void CMenuButton::Draw()
{
	CPolygon2D::Draw();					//基本クラスの描画処理
}

//親へのポインタの設定
void CMenuButton::SetParent(CObject * pParent)
{
	m_pParent = pParent;

	if (m_pParent)
		SetPos(m_pParent->GetPos() + m_relativePos);
	else
		SetPos(m_relativePos);
}

//相対位置の設定
void CMenuButton::SetRelativePos(const D3DXVECTOR3 pos)
{
	m_relativePos = pos;

	if (m_pParent)
		SetPos(m_pParent->GetPos() + m_relativePos);
	else
		SetPos(m_relativePos);
}

//相対位置の取得
const D3DXVECTOR3 CMenuButton::GetRelativePos()
{
	return m_relativePos;
}

//テクスチャの設定
void CMenuButton::SetTexture(const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();

	if (nTexIdx >= 0 && nTexIdx < nMax)
	{
		std::vector<int> pTex;
		pTex.push_back(nTexIdx);
		LoadTex(pTex);
	}
		
}

//生成
CMenuButton * CMenuButton::Create()
{
	// オブジェクトインスタンス
	CMenuButton *pObj = new CMenuButton;

	if (pObj != nullptr)
	{// 数値の初期化
		pObj->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pObj;
}

//生成
CMenuButton * CMenuButton::Create(const D3DXVECTOR3 pos)
{
	// オブジェクトインスタンス
	CMenuButton *pObj = new CMenuButton;

	if (pObj != nullptr)
	{// 数値の初期化
		pObj->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	pObj->SetRelativePos(pos);				//位置の設定

	// インスタンスを返す
	return pObj;
}

//生成
CMenuButton * CMenuButton::Create(const D3DXVECTOR3 pos, const D3DXCOLOR normalCol, const D3DXCOLOR triggerCol)
{
	// オブジェクトインスタンス
	CMenuButton *pObj = new CMenuButton;

	if (pObj != nullptr)
	{// 数値の初期化
		pObj->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	pObj->SetRelativePos(pos);				//位置の設定
	pObj->m_normalColor = normalCol;		//普通の色の設定
	pObj->m_mouseOverColor = triggerCol;	//トリガーの色の設定
	pObj->SetColor(normalCol);				//普通の色の設定

	// インスタンスを返す
	return pObj;
}
