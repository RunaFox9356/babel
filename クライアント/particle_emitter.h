//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
//ParticleEmitter.h
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
#ifndef _PARTICLE_EMITTER_H_
#define _PARTICLE_EMITTER_H_

#include "manager.h"
#include "particle.h"

class CParticle;

class CParticleEmitter : public CManager
{
public:
	enum EBehavior
	{
		Behavior_None,
		Behavior_Circle,
		Behavior_Sphere,
		Behavior_Rotate,
		Behavior_Max
	};

	enum EObjectType
	{
		OBJECT_BILLBOARD,
		OBJECT_MESH,
		OBJECT_2D,
		OBJECT_MAX
	};

	struct SEmitterInfo
	{
		SEmitterInfo()
		{
			behavior	   = Behavior_None;
			objType		   = OBJECT_BILLBOARD;
			modelTag	   = "CUBE";
			particleTag    = "Begin";
			texTag		   = "FLARE";
			maskTexTag	   = "null";
			radius		   = 360.0f;
			popDelay	   = 0;
			popParticleNum = 5;
			emitterIndex   = 0;
			enableShader   = false;
		}

		EBehavior	behavior;		// 挙動
		EObjectType objType;		// オブジェクトタイプ
		std::string modelTag;		// モデルのタグ
		std::string particleTag;	// パーティクルのタグ
		std::string texTag;			// テクスチャのタグ
		std::string maskTexTag;		// マスク用テクスチャのタグ
		float		radius;			// 半径
		int			texIndex;		// テクスチャのインデックス
		int			popDelay;		// 生成の遅延
		int			popParticleNum;	// 生成する数
		int			emitterIndex;	// パーティクルエミッタ用の番号
		bool		enableShader;	// シェーダーを使用するかどうか
	};

	CParticleEmitter();
	~CParticleEmitter();

	static CParticleEmitter* Create(const std::string& str);

	HRESULT Init();
	void Uninit();
	void Update();
	void Draw() { ; }
	void CreateParticle(int index);
	void AfterRelease() { m_afterRelease = true; }
	void Destroy() { m_isRelease = true; }

	void SetPos(D3DXVECTOR3 pos);

	D3DXVECTOR3 GetPos();
	bool GetDestroy() { return m_isRelease; }

private:
	void SetPosBillboard(D3DXVECTOR3 pos);

	std::vector<CParticle*>		  m_particle;
	std::vector<CParticle::SInfo> m_particleInfo;
	std::vector<SEmitterInfo>	  m_emitterInfo;
	D3DXVECTOR3					  m_popPos;
	D3DXVECTOR3					  m_move;
	int							  m_cntBillboard;
	int							  m_cntMesh;
	int							  m_time;
	int							  m_soundTime;
	bool						  m_soundOnce;
	bool						  m_afterRelease;
	bool						  m_isRelease;
};

#endif