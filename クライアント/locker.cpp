//=============================================================================
//
// locker.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "locker.h"
#include "collision_rectangle3D.h"
#include "input.h"
#include "agent.h"
#include "emptyObj.h"
#include "game.h"
#include "model_obj.h"
#include "motion.h"
#include "application.h"
#include "model_data.h"
#include "UImessage.h"
//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3 CLocker::DEFAULT_DOOR_POS = { 15.0f, 4.0f, -11.7f };					//ディフォルトドアの相対位置
const D3DXVECTOR3 CLocker::DEFAULT_COLLISION_POS = { 0.0f, 45.2f, 0.0f };				//ディフォルトコリジョンの相対位置
const D3DXVECTOR3 CLocker::DEFAULT_COLLISION_SIZE = { 36.2f, 90.4f, 23.4f };			//ディフォルトコリジョンのサイズ
const D3DXVECTOR3 CLocker::DEFAULT_INTERACT_HITBOX_POS = { 0.0f, 45.2f, -19.2f };		//ディフォルトのヒットボックスの相対位置
const D3DXVECTOR3 CLocker::DEFAULT_INTERACT_HITBOX_SIZE = { 36.2f, 90.4f, 15.0f };		//ディフォルトのヒットボックスのサイズ
const int		  CLocker::DEFAULT_DELAY_FRAMES = 30;									//ディフォルトのディレイフレーム数


namespace HLocker
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
	const int Q_BUTTON_TEXTURE_IDX = 46;			//Qボタンのテクスチャのインデックス
};


//コンストラクタ
CLocker::CLocker() : m_nDelayFrames(0),
m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_rot(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_bOpen(false),
m_bClose(false),
m_pModel(nullptr),
m_pHitboxObj(nullptr),
m_pDoor(nullptr),
m_pInteractionHitbox(nullptr),
m_pPlayer(nullptr),
m_pUiMessage(nullptr)
{

}

//デストラクタ
CLocker::~CLocker()
{

}

// 初期化
HRESULT CLocker::Init()
{
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));
	return S_OK;
}

// 終了
void CLocker::Uninit()
{
	//からのオブジェクトの破棄
	if (m_pHitboxObj)
	{
		m_pHitboxObj->Uninit();
		m_pHitboxObj = nullptr;
	}

	//ドアのモデルの破棄
	if (m_pDoor)
	{
		m_pDoor->Uninit();
		m_pDoor = nullptr;
	}

	//押し出さないヒットボックスの破棄
	if (m_pInteractionHitbox)
	{
		m_pInteractionHitbox->Uninit();
		m_pInteractionHitbox = nullptr;
	}

	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}

	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}

	//メモリの解放
	Release();
}

// 更新
void CLocker::Update()
{
	{
		SetAnimation(m_bOpen);
		// 入力デバイスの取得
		CInput *pInput = CInput::GetKey();

		if (pInput->Trigger(DIK_U))
		{
			m_bOpen = true;
			m_bClose = false;
		}
		else if (pInput->Trigger(DIK_Y))
		{
			m_bOpen = false;
			m_bClose = true;
		}
	}

	if (m_bOpen || CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
	{
		OpenDoor();		//ドアを開ける
	}
	else if (m_bClose ||!CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
	{
		CloseDoor();	//ドアを閉める
	}
	

	if (!GetInteraction())
	{//プレイヤーに使われていない場合

		if (m_pPlayer)
		{//前のフレームまでプレイヤーに使われていた場合

			m_pPlayer->SetPos(m_exitPos);		//プレイヤーの位置を設定する
			m_pPlayer = nullptr;				//ポインタをnullにする
			SetInteraction(false);				//プレイヤーに使われていない状態にする
			DestroyUiMessage();					//UIの破棄
		}

		if (m_pInteractionHitbox->Collision(CObject::OBJETYPE_PLAYER, false))
		{//プレイヤーが押し出さないヒットボックスと重なっている場合

			std::vector<CObject*> vpObj = m_pInteractionHitbox->GetCollidedObj();	//当たったオブジェクトのポインタを持っているベクトルの取得

			// 入力デバイスの取得
			CInput *pInput = CInput::GetKey();

			if ((int)vpObj.size() > 0 && pInput->Trigger(DIK_E))
			{//当たったプレイヤーがいて、Eボタンが押されている場合

				CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

				if (pPlayer)
				{//nullチェック

					pPlayer->SetGimmick(this);			//プレイヤーに使われているギミックとしてこのインスタンスを保存する
					pPlayer->SetHiddenState(true);		//プレイヤーを隠れている状態に設定する
					pPlayer->SetStopState(true);
					pPlayer->SetPos(m_pos);				//プレイヤーの位置の設定
					pPlayer->SetRot(m_rot);				//プレイヤーの回転角度の設定

					// モーション情報の取得
					CMotion *pMotion = pPlayer->GetMotion();

					if (pMotion)
					{//nullチェック
						pMotion->SetNumMotion(CAgent::ACTION_STATE::NEUTRAL_ACTION);	//モーションセットのインデックスの設定
					}

					SetInteraction(true);		//プレイヤーに使われている状態にする
					m_pPlayer = pPlayer;		//プレイヤーのポインタを保存する
					CreateUiMessage("Exit", HLocker::Q_BUTTON_TEXTURE_IDX);	//ExitというUIを生成する
				}
			}
			else
			{
				if (!m_pUiMessage)
					CreateUiMessage("Hide", HLocker::E_BUTTON_TEXTURE_IDX);	//HideというUIを生成する
			}
		}
		else
		{
			if (m_pUiMessage)
				DestroyUiMessage();				//UIの破棄
		}
	}
	
}

// 描画
void CLocker::Draw()
{

}

//位置の設定
void CLocker::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;

	if (m_pModel)
	{//モデルの位置の設定
		m_pModel->SetPos(m_pos);
	}
	if (m_pDoor)
	{//ドアのモデルの位置の設定
		m_pDoor->SetPos(m_pos + DEFAULT_INTERACT_HITBOX_POS);
	}
}

//回転角度の設定
void CLocker::SetRot(const D3DXVECTOR3 & rot)
{
	m_rot = rot;

	if (m_pModel)
	{//モデルの回転角度の設定
		m_pModel->SetRot(m_rot);
	}
	if (m_pDoor)
	{//ドアのモデルの回転角度の設定
		m_pDoor->SetRot(m_rot);
	}
}

//位置の取得
D3DXVECTOR3 CLocker::GetPos()
{
	return m_pos;
}

//回転角度の取得
D3DXVECTOR3 CLocker::GetRot()
{
	return m_rot;
}

//エフェクトのインデックスの設定
void CLocker::SetRenderMode(int mode)
{
	if (m_pModel)
		m_pModel->SetRenderMode(mode);

	if (m_pDoor)
		m_pDoor->SetRenderMode(mode);
}



//=============================================================================
//
//									静的関数
//
//=============================================================================

//生成
CLocker * CLocker::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot)
{
	CLocker* pObj = new CLocker;		//インスタンスを生成する

	if (FAILED(pObj->Init()))			//生成できなかった場合、nullを返す
		return nullptr;

	pObj->m_pModel = CModelObj::Create();
	pObj->m_pDoor = CModelObj::Create();	//ドアモデルの生成

	pObj->SetPos(pos);						//位置の設定
	pObj->SetRot(rot);						//回転角度の設定

	if (pObj->m_pDoor)
	{//生成出来たら

		pObj->m_pModel->SetPos(pos);						//位置の設定
		pObj->m_pModel->SetRot(rot);						//回転角度の設定
		pObj->m_pModel->SetType(51);						//モデルの種類の設定
	}

	if (pObj->m_pDoor)
	{//生成出来たら

		pObj->m_pDoor->SetPos(pos + DEFAULT_DOOR_POS);		//位置の設定
		pObj->m_pDoor->SetRot(rot);							//回転角度の設定
		pObj->m_pDoor->SetType(52);							//モデルの種類の設定
	}

	CCollision_Rectangle3D* pCollision = pObj->m_pModel->GetCollision();		//コリジョンの取得

	if (pCollision)
	{//nullチェック
		pCollision->SetPos(DEFAULT_COLLISION_POS);					//コリジョンのサイズの位置の設定
		pCollision->SetSize(DEFAULT_COLLISION_SIZE);				//コリジョンのサイズの設定
	}

	pObj->m_pHitboxObj = CEmptyObj::Create();

	if (pObj->m_pHitboxObj)
	{
		pObj->m_pHitboxObj->SetPos(pos);
		pObj->m_pHitboxObj->SetRot(rot);
		pObj->m_pHitboxObj->SetObjType(CObject::EObjectType::OBJETYPE_NON_SOLID_GIMMICK);
	}

	pObj->m_pInteractionHitbox = CCollision_Rectangle3D::Create();	//ヒットボックスの生成

	if (pObj->m_pInteractionHitbox)
	{//生成出来たら
		pObj->m_pInteractionHitbox->SetParent(pObj->m_pHitboxObj);			//親の設定
		pObj->m_pInteractionHitbox->SetPos(DEFAULT_INTERACT_HITBOX_POS);	//相対位置の設定
		pObj->m_pInteractionHitbox->SetSize(DEFAULT_INTERACT_HITBOX_SIZE);	//サイズの設定
	}

	pObj->CalcExitPos();
	

	return pObj;						//生成したインスタンスを返す
}

//出る時の位置を計算する
void CLocker::CalcExitPos()
{
	D3DXVECTOR3 vec = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	vec = m_pInteractionHitbox->GetPos();			//押し出さないヒットボックスの位置を取得する
	vec.y = 0;										//y座標を0にする
	D3DXVec3Normalize(&vec, &vec);					//ベクトルを正規化する
	vec.x *= 30.0f;									//ベクトルの長さを30にする
	vec.z *= 30.0f;									//ベクトルの長さを30にする

	m_exitPos = m_pos + vec;						//出口の位置の設定
}

//ドアを開ける
void CLocker::OpenDoor()
{
	float fMaxAngle = -D3DX_PI * (45.0f / 90.0f);	//限度の角度
	float fFrameAngle = -D3DX_PI * 0.01f;			//1フレームに加算されるの角度

	if (m_pDoor)
	{//ドアのモデルが存在したら

		D3DXVECTOR3 rot = m_pDoor->GetRot();		//現在の回転角度の取得

		if (rot.y > fMaxAngle)
		{//限度をまだ超えていない場合、回転させる

			rot.y += fFrameAngle;
		}
		else
		{//限度を超えたら、アニメーションを中止する
			rot.y = fMaxAngle;
			m_bOpen = false;
		}

		m_pDoor->SetRot(rot);		//角度の設定
	}
}

//ドアを閉める
void CLocker::CloseDoor()
{
	float fMaxAngle = 0.0f;						//限度の角度
	float fFrameAngle = D3DX_PI * 0.01f;		//1フレームに加算されるの角度

	if (m_pDoor)
	{
		D3DXVECTOR3 rot = m_pDoor->GetRot();	//現在の回転角度の取得

		if (rot.y < fMaxAngle)
		{//限度をまだ超えていない場合、回転させる

			rot.y += fFrameAngle;
		}
		else
		{//限度を超えたら、アニメーションを中止する
			rot.y = fMaxAngle;
			m_bClose = false;
		}

		m_pDoor->SetRot(rot);		//角度の設定
	}
}


//UIメッセージの生成処理
void CLocker::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage();

	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(85.0f, 85.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CLocker::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}