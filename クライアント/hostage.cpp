//=============================================================================
//
// hostage.cpp
// Author: 磯江 寿希亜
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "hostage.h"
#include "agent.h"
#include"collision_rectangle3D.h"
#include"model_obj.h"
#include"model3D.h"
#include "game.h"
#include "application.h"
#include "model_data.h"
#include "input.h"
#include "hacker.h"
#include "task.h"
#include "UImessage.h"
#include "motion_model3D.h"
#include "move.h"
#include "calculation.h"
#include "motion.h"
#include "EscapePoint.h"
#include "mapDataManager.h"


//=============================================================================
//									静的変数の初期化
//=============================================================================
const D3DXVECTOR3		CHostage::DEFAULT_POS = { -500.0f, 0.0f, 0.0f };						//ディフォルトのスポーンの位置
const D3DXVECTOR3		CHostage::DEFAULT_COLLISION_POS = D3DXVECTOR3(0.0f, 30.0f, 0.0f);		//ディフォルトのモデルの当たり判定の相対位置
const D3DXVECTOR3		CHostage::DEFAULT_COLLISION_SIZE = D3DXVECTOR3(30.0f, 60.0f, 30.0f);	//ディフォルトのモデルの当たり判定のサイズ
const float				CHostage::DEFAULT_INTERACTION_DISTANCE = 250.0f;						//ディフォルトの反応できる距離
const float				CHostage::DEFAULT_SPEED = 1.0f;											//ディフォルトの移動スピード
const char*				CHostage::DEFAULT_MOTION_FILE_PATH = "data/MOTION/motion.txt";			//ディフォルトのモーションのファイルパス


namespace
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
	const int Q_BUTTON_TEXTURE_IDX = 46;			//Qボタンのテクスチャのインデックス
	const float offsetHostagePos = 50.0f;
	const float selfPlayerDistance = 100.0f;
	const float stopTrackingDist = 750.0f;
	const float speed = 5.0f;
}

CHostage::CHostage() : m_rotDest(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_move(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_escapePos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_fDistance(0.0f),
m_bIsFollowing(false),
m_pUiMessage(nullptr),
m_pModelHitbox(nullptr),
m_pModel(nullptr),
m_pMove(nullptr),
m_action((ACTION_STATE)0),
m_state((STATE)0)
{
}

CHostage::~CHostage()
{
}

CHostage * CHostage::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	// オブジェクトインスタンス
	CHostage *pHostageObj = nullptr;

	// メモリの解放
	pHostageObj = new CHostage;

	// メモリの確保ができなかった
	assert(pHostageObj != nullptr);

	// 数値の初期化
	pHostageObj->Init();

	//位置の設定
	pHostageObj->SetPos(pos);

	//向きの設定
	pHostageObj->SetRot(rot);

	// インスタンスを返す
	return pHostageObj;
}

HRESULT CHostage::Init()
{
	//ギミックにはIDを絶対つけてね
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));
	SetAnimation(m_bIsFollowing);

	m_fDistance = 1000.0f;

	//モデルを生成する
	m_pModel = CMotionModel3D::Create();	

	if (m_pModel)
	{
		m_pModel->SetMotion(DEFAULT_MOTION_FILE_PATH);			//モーションをロードする
		m_pModel->SetPos(DEFAULT_POS);

		m_pModelHitbox = CCollision_Rectangle3D::Create();		//モデルの当たり判定を生成する

		if (m_pModelHitbox)
		{//nullチェック

			m_pModelHitbox->SetParent(m_pModel);				//当たり判定の親の設定
			m_pModelHitbox->SetSize(DEFAULT_COLLISION_SIZE);	//モデルの当たり判定のサイズ設定
			m_pModelHitbox->SetPos(DEFAULT_COLLISION_POS);		//モデルの当たり判定の位置の設定
		}

		m_action = CHostage::ACTION_STATE::NEUTRAL_ACTION;		//ニュートラルモーションを設定する

		SetMotion(m_action);										//ニュートラルモーションを再生する
	}

	m_pMove = new CMove;				//移動用のクラスを生成する

	if (m_pMove)
	{
		m_pMove->SetMoving(DEFAULT_SPEED, 1000.0f, 0.1f, 0.1f);
	}

	m_state = STATE_PRISONER;			

	CTask* pTask = nullptr;							//タスク情報へのポインタ

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
	{
		pTask = CGame::GetTask();			//エージェントタスクの取得
		SetRenderMode(0);					//レンダーエフェクトをつける
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pTask = CHacker::GetTask();			//ハッカータスクの取得
		SetRenderMode(1);					//レンダーエフェクトをつける
	}

	if (pTask)
	{//nullチェック

	 //タスクの更新
		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
			pTask->AddTask(CTask::AGENT_TASK_RESCUE);
		else if (mode == CApplication::MODE_HACKER)
			pTask->AddHackerTask(CTask::HACKER_TASK_FIND_PRISONER);
	}

	return S_OK;
}

// 終了
void CHostage::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}

	//当たり判定のポインタをnullに戻す
	if (m_pModelHitbox)
	{
		m_pModelHitbox->Uninit();
		m_pModelHitbox = nullptr;

	}
		
	//UIメッセージの破棄
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}

	//移動用のオブジェクトの破棄
	if (m_pMove)
	{
		delete m_pMove;
		m_pMove = nullptr;
	}

	//メモリの解放
	Release();
}

void CHostage::Update()
{
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
	if (mode == CApplication::MODE_HACKER)
	{
		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
		{
			SetFreeState();
			SetAnimation(true);
			m_bIsFollowing = true;			//追従するようにする
			m_state = STATE_FOLLOW;			//追従する状態にする
		}
		else
		{
			m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//移動を止める
			m_action = NEUTRAL_ACTION;				//ニュートラルアクションの設定
			SetMotion(m_action);					//アニメーションの設定
			m_state = STATE_WAITING;				//待機状態にする
			SetAnimation(false);
			m_bIsFollowing = false;					//追従しないようにする
		}
	}

	//プレイヤーまでの距離を確認する
	CheckPlayerDistance();

	UpdateState();

	MotionUpdate();

	//// プレイヤーに追従
	//Tracking();

	//if (CApplication::GetInstance()->GetPlayer(1)->GetCommu()->Player.m_isPopGimmick[GetId()])
	//{
	//	OnlineTracking();
	//}
}

//位置のセッター
void CHostage::SetPos(const D3DXVECTOR3 & pos)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPos(pos);
	}
}

//向きのセッター
void CHostage::SetRot(const D3DXVECTOR3& rot)
{
	if (m_pModel)
	{//モデルのnullチェック

		m_pModel->SetRot(rot);			//モデルの向きを設定する
	}
}

//位置のゲッター
D3DXVECTOR3 CHostage::GetPos()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//モデルが存在したら、モデルの位置を取得する
		pos = m_pModel->GetPos();
	}

	return pos;			//位置を返す
}

//向きのゲッター
D3DXVECTOR3 CHostage::GetRot()
{
	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//モデルのnullチェック

		rot = m_pModel->GetRot();		//モデルの向きを取得する
	}

	return rot;				//向きを返す
}

//エフェクトのインデックスの設定
void CHostage::SetRenderMode(int mode)
{
	//モードの確認
	if (mode < 0 || mode >= CModel3D::ERenderMode::Render_MAX)
		mode = 0;

	m_pModel->SetRenderMode((CModel3D::ERenderMode)mode);
}

//マップに表示されているかどうかの設定処理
void CHostage::SetIsOnMap(const bool bOnMap)
{
	CGimmick::SetIsOnMap(bOnMap);

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (!bOnMap)
		return;

	if (mode == CApplication::MODE_HACKER)
		SetRenderMode(0);
	else if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
		SetRenderMode(1);
}

//状態によっての更新処理
void CHostage::UpdateState()
{
	if (!GetIsOnMap() && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isMap)
		SetIsOnMap(true);

	switch (m_state)
	{
	case CHostage::STATE_PRISONER:

		//人質状態の更新処理
		PrisonerUpdate();

		break;

	case CHostage::STATE_WAITING:

		//待機状態の更新処理
		Wait();

		break;

	case CHostage::STATE_FOLLOW:

	{//追従状態の更新処理

		//プレイヤーまでの距離を確認する
		CheckPlayerDistance();

		//プレイヤーを追従する
		if (m_bIsFollowing)
			Follow();

		//モーションの更新処理
		MotionUpdate();

		//出口についたかどうかを判定する
		if (HasEscaped())
			m_state = STATE_ESCAPE;
	}

		break;
	case CHostage::STATE_ESCAPE:

		//逃亡状態の更新処理
		Escape();

		break;

	default:
		break;
	}
}

//人質状態の更新処理
void CHostage::PrisonerUpdate()
{
	//プレイヤーまでの距離を確認する
	CheckPlayerDistance();
}

//待機状態の更新処理
void CHostage::Wait()
{
	//プレイヤーまでの距離を確認する
	CheckPlayerDistance();
}

//逃亡状態の更新処理
void CHostage::Escape()
{
	//出口のほうに向かう
	FaceDirection(m_escapePos);

	//出口のほうに移動する
	Move(m_escapePos);
}

//回転処理
void CHostage::Rotate()
{
	// 向きの取得
	D3DXVECTOR3 rot = GetRot();

	// 向きの更新
	rot.y += (m_rotDest.y - rot.y) * 0.25f;

	// 向きの正規化
	rot.y = CCalculation::RotNormalization(rot.y);

	// 向きの設定
	SetRot(rot);
}

//プレイヤーの距離を計算し、プレイヤーに反応できるかどうかの判定
void CHostage::CheckPlayerDistance()
{
	if (m_bIsFollowing)
	{
		CInput* pInput = CApplication::GetInstance()->GetInput();		//インプットデバイスの取得

		if (pInput && pInput->Trigger(DIK_Q))
		{//Qボタンを押したら

			m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//移動を止める
			m_action = NEUTRAL_ACTION;				//ニュートラルアクションの設定
			SetMotion(m_action);					//アニメーションの設定
			m_state = STATE_WAITING;				//待機状態にする
			SetAnimation(false);
			m_bIsFollowing = false;					//追従しないようにする
			DestroyUiMessage();						//UIメッセージの破棄
			
			return;
		}
	}

	D3DXVECTOR3 playerPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//プレイヤーの位置の変数を宣言する
	D3DXVECTOR3 pos = GetPos();								//位置の取得

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
		playerPos = CGame::GetPlayer()->GetPos();			//プレイヤーの位置の取得


	if (mode == CApplication::MODE_HACKER)
		playerPos = CHacker::GetPlayer()->GetPos();		//プレイヤーの位置の取得

	D3DXVECTOR3 dist = playerPos - pos;						//プレイヤーまでのベクトルを計算する
	float fDist = D3DXVec3Length(&dist);					//プレイヤーまでの距離を計算する

	if (fDist < DEFAULT_INTERACTION_DISTANCE)
	{
		CInput* pInput = CApplication::GetInstance()->GetInput();		//インプットデバイスの取得

		if (!m_pUiMessage && (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME))
		{
			if (m_state == STATE_WAITING)
				CreateUiMessage("Make Follow", E_BUTTON_TEXTURE_IDX);	//Make FollowというUIを生成する
			else if(m_state == STATE_PRISONER)
				CreateUiMessage("Free", E_BUTTON_TEXTURE_IDX);			//FreeというUIを生成する
		}

		if (pInput && pInput->Trigger(DIK_E))
		{//Eボタンを押したら

			SetFreeState();
			SetAnimation(true);
			m_bIsFollowing = true;			//追従するようにする
			m_state = STATE_FOLLOW;			//追従する状態にする

			CreateUiMessage("Stop Follow", Q_BUTTON_TEXTURE_IDX);		//Stop FollowというUIを生成する
		}
	}
	else if (m_pUiMessage && m_state != STATE_FOLLOW)
		DestroyUiMessage();				//UIメッセージの破棄
	
	//人質状態と逃亡状態ではなかったら、プレイヤーが近い時プレイヤーのほうに向かう
	if(fDist < 1.5f * DEFAULT_INTERACTION_DISTANCE && (m_state != STATE_PRISONER && m_state != STATE_ESCAPE))
		FacePlayer();
}

//プレイヤーのほうに向かう
void CHostage::FacePlayer()
{
	CPlayer* pPlayer = nullptr;			//プレイヤーへのポインタ

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
		pPlayer = CGame::GetPlayer();			//プレイヤーの位置の取得
	
	if (mode == CApplication::MODE_HACKER)
		pPlayer = CHacker::GetPlayer();			//プレイヤーの位置の取得

	Rotate();


	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - pPlayer->GetPos();
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > pPlayer->GetPos().x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);				//目的の回転角度の設定
}

//ある方向に向かう
void CHostage::FaceDirection(D3DXVECTOR3 point)
{
	Rotate();

	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - point;
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > point.x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);				//目的の回転角度の設定
}

void CHostage::Follow()
{
	if (!m_bIsFollowing)
	{
		return;
	}
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	D3DXVECTOR3 targetPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT)
	{
		targetPos = CGame::GetPlayer()->GetPos();	//プレイヤーの位置を取得する
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		targetPos = CHacker::GetPlayer()->GetPos();	//プレイヤーの位置を取得する
	}

	D3DXVECTOR3 pos = GetPos();								//位置の取得
	D3DXVECTOR3 dist = targetPos - pos;						//プレイヤーまでのベクトルを計算する
	float fDist = D3DXVec3Length(&dist);					//プレイヤーまでの距離を計算する
	D3DXVec3Normalize(&dist, &dist);						//プレイヤーまでのベクトルを正規化する

	if (fDist > DEFAULT_INTERACTION_DISTANCE && m_pMove)
	{//移動用のクラスのnullチェック

		m_pMove->Moving(dist);								//移動量を計算する
		m_move = m_pMove->GetMove();						//移動量を取得する
	}
	else
	{
		m_move += (D3DXVECTOR3(0.0f, 0.0f, 0.0f) - m_move) * 0.1f;
	}

	SetPos(pos + m_move);					//位置を更新する
}

void CHostage::OnlineTracking()
{
	if (!CHacker::GetPlayer())
	{
		return;
	}
	Follow();
}

//出口に着いたかどうかの判定
bool CHostage::HasEscaped()
{
	bool bEsc = false;

	CPlayer* pPlayer = nullptr;					//プレイヤーへのポインタ

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
		pPlayer = CGame::GetPlayer();			//プレイヤーの位置の取得

	//プレイヤーが出口の近くについた場合
	if (!pPlayer || !pPlayer->GetNearEscape())
		return false;

	m_escapePos = pPlayer->GetNearEscape()->GetPos();	//出口の位置を保存する
	bEsc = true;										//エスケープできることを返す

	return bEsc;
}

//ポイントの方に移動
void CHostage::Move(const D3DXVECTOR3 point)
{
	if (m_action != MOVE_ACTION)
	{//移動アクションではなかったら、移動アニメーションを設定する

		m_action = MOVE_ACTION;
		SetMotion(m_action);
	}

	D3DXVECTOR3 targetPos = point;							//プレイヤーの位置を取得する
	D3DXVECTOR3 pos = GetPos();								//位置の取得
	D3DXVECTOR3 dist = targetPos - pos;						//プレイヤーまでのベクトルを計算する
	D3DXVec3Normalize(&dist, &dist);						//プレイヤーまでのベクトルを正規化する

	m_pMove->Moving(dist);									//移動量を計算する
	m_move = m_pMove->GetMove();							//移動量を取得する

	SetPos(pos + m_move);									//位置を更新する

	dist = m_escapePos - GetPos();							//出口までのベクトルを計算する
	float fDist = D3DXVec3Length(&dist);					//上のベクトルの長さを計算する

	if (fDist < 20.0f)
	{//距離が最大移動量より小さかったら

		CTask* pTask = nullptr;								//タスク情報へのポインタ

		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
			pTask = CGame::GetTask();			//エージェントタスクの取得

		if (pTask)
		{//nullチェック

		 //タスクの更新
			if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
			pTask->SubtractTask(CTask::AGENT_TASK_RESCUE);
		}

		//当たり判定の破棄
		if (m_pModelHitbox)
		{
			m_pModelHitbox->Uninit();
			m_pModelHitbox = nullptr;
		}

		DeleteData();

		//終了処理
		Uninit();
	}
}

//モーションの更新処理
void CHostage::MotionUpdate()
{
	if (m_action != MOVE_ACTION && D3DXVec3Length(&m_move) > 0.0f)
	{//移動していないのに、移動量が0.0fより大きいければ、移動アクションのアニメーションを再生する

		m_action = MOVE_ACTION;			//移動モーションを設定する
		SetMotion(m_action);			//現在のアニメーションを設定する
	}
	else if (m_action != NEUTRAL_ACTION && D3DXVec3Length(&m_move) < 0.1f)
	{//移動しているのに、移動量が小さくなってきた場合、ニュートラルアニメーションを再生する

		m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//移動を止める
		m_action = NEUTRAL_ACTION;				//ニュートラルモーションを設定する
		SetMotion(m_action);					//現在のアニメーションを設定する
	}
}

//モーションアニメーションの設定
void CHostage::SetMotion(ACTION_STATE state)
{
	//モデルのnullチェック
	if (!m_pModel)
		return;

	//モーションの取得
	CMotion* pMotion = m_pModel->GetMotion();

	//モーションのnullチェック
	if (!pMotion)
		return;

	pMotion->SetNumMotion(state);			//現在のアニメーションを設定する
}


//UIメッセージの生成処理
void CHostage::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();	//テクスチャの最大数の取得
	int nNum = nTexIdx;														

	//引数のテクスチャインデックスを確認する
	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	//UIメッセージの破棄
	DestroyUiMessage();

	//新しいUIメッセージを生成する
	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(50.0f, 50.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CHostage::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}

//解放状態の設定処理
void CHostage::SetFreeState()
{
	CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
	int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

	if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
	{//マップデータマネージャーのnullチェックとマップインデックスの確認

	 //ハッカーのターゲットのデータを取得する
		std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

		for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
		{//全部確認する

			if (vData.data()[nCnt].pObj == this)
			{
				vData.data()[nCnt].bGot = true;
				break;
			}
		}

	}
}

//データの破棄処理
void CHostage::DeleteData()
{
	CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
	int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

	if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
	{//マップデータマネージャーのnullチェックとマップインデックスの確認

	 //ハッカーのターゲットのデータを取得する
		std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

		for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
		{//全部確認する

			if (vData.data()[nCnt].pObj == this)
			{
				pDataManager->DeleteObj(nMapIdx, nCnt);
				break;
			}
		}

	}
}