//=============================================================================
//
// resultScoreUI.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _RESULT_SCORE_UI_H_		// このマクロ定義がされてなかったら
#define _RESULT_SCORE_UI_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"
#include "task.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CText;


class CResultScoreUI : public CPolygon2D
{
public:

	CResultScoreUI();				//コンストラクタ
	~CResultScoreUI() override;		//デストラクタ

	HRESULT Init() override;		//初期化
	void Uninit() override;			//終了
	void Update() override;			//更新
	void Draw() override;			//描画

	const bool GetEnd() { return m_bEnd; }			//終わったかどうかの取得処理

	static CResultScoreUI* Create(const bool bAgent, std::vector<int> vCompletedTask);	//生成

private:

	static const D3DXVECTOR3	DEFAULT_GB_POS;				//ディフォルトの背景の位置
	static const D3DXVECTOR3	DEFAULT_BG_SIZE;			//ディフォルトの背景のサイズ
	static const D3DXCOLOR		DEFAULT_BG_COLOR;			//ディフォルトの背景の色


	int		m_nCntAnim;				//アニメーションカウンター
	bool	m_bAgent;				//エージェントモードだったかどうか
	bool	m_bEnd;					//アニメーションが終わったかどうか

	CText*	m_pFirstText;			//最初のテキストへのポインタ
	CText*	m_pSecondText;			//第二のテキストへのポインタ

	std::vector<int>	m_vCompletedTask;	//達成したタスク
};

#endif