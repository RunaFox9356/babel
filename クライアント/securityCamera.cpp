//=============================================================================
//
// securityCamera.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "securityCamera.h"
#include "model_obj.h"
#include "application.h"
#include "collision_rectangle3D.h"
#include "game.h"
#include "camera.h"
#include "model3D.h"
#include "hacker.h"
#include "drone.h"
#include "polygon2D.h"
#include "renderer.h"
#include "utility.h"
#include "HackingObj.h"
#include "UImessage.h"
#include "input.h"
#include "application.h"
#include "hacker.h"
#include "scene_mode.h"
#include "MiniGameMNG.h"
#include "task.h"
#include "HackerUI.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CSecurityCamera::DEFAULT_MAP_POS_V = { 0.0f, 5000.0f, -1.0f };					//ディフォルトのマップカメラの視点
const D3DXVECTOR3	CSecurityCamera::DEFAULT_MAP_POS_R = { 0.0f, 0.0f, 0.0f };						//ディフォルトのマップカメラの注視点
const D3DXVECTOR3	CSecurityCamera::DEFAULT_POS_V = { 0.0f, 10.0f, -12.5f };							//ディフォルトのカメラの視点(相対)
const D3DXVECTOR3	CSecurityCamera::DEFAULT_POS_R = { 0.0f, 10.0f, -17.5f };						//ディフォルトのカメラの注視点(相対)
const float			CSecurityCamera::DEFAULT_MAX_ROT_Y = D3DX_PI * 0.25f;							//ディフォルトのY軸の最大回転角度
const D3DXVECTOR3	CSecurityCamera::DEFAULT_CAMERA_RELATIVE_POS = { 0.0f, 6.5f, 0.0f };			//カメラモデルの相対位置
const D3DXVECTOR3	CSecurityCamera::DEFAULT_CAMERA_RELATIVE_ROT = { D3DX_PI * -0.1f, 0.0f, 0.0f };	//カメラモデルの相対向き
const float			CSecurityCamera::DEFAULT_FRAME_MOVE = D3DX_PI * 0.0005f;						//ディフォルトの回転量
const int			CSecurityCamera::DEFAULT_ANIMATION_DELAY = 90;									//ディフォルトのアニメーションディレイ
D3DXVECTOR3			CSecurityCamera::m_lastRot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);						//元のドローンの向き(保存用)
std::vector<CSecurityCamera*> CSecurityCamera::m_vAllCameras;										//全部のカメラ
int					CSecurityCamera::m_nCameraIdx = 0;												//選択されているカメラインデックス

namespace
{
	float s_fWidth = 1280.0f / 720.0f;				//スノーノイズのテクスチャの比率
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
	const int	DEFAULT_STATIC_TEXTURE_IDX = 54;	//ディフォルトのテクスチャインデックス
};


//コンストラクタ
CSecurityCamera::CSecurityCamera() : m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_rot(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_posV(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_posR(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_fRotY(0.0f),
m_fMaxRotY(0.0f),
m_nDelay(0),
m_fFrameMove(0.0f),
m_bHacked(false),
m_bHackingPc(false),
m_pStaticNoise(nullptr),
m_pHackablePc(nullptr),
m_pUiMessage(nullptr)
{

}

//デストラクタ
CSecurityCamera::~CSecurityCamera()
{

}

// 初期化
HRESULT CSecurityCamera::Init()
{
	//カメラのベースの生成
	m_pSupportModel = CModel3D::Create();

	if (m_pSupportModel)
	{//nullチェック
		m_pSupportModel->SetModelID(57);		//モデルの種類の設定
		m_pSupportModel->SetShadow(false);		//影を描画しないように設定する
	}

	m_pCameraModel = CModel3D::Create();		//モデルの生成
	//SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));

	if (m_pCameraModel)
	{//生成出来たら

		m_pCameraModel->SetModelID(58);							//モデルの種類の設定
		m_pCameraModel->SetPos(DEFAULT_CAMERA_RELATIVE_POS);	//相対位置の設定
		m_pCameraModel->SetRot(DEFAULT_CAMERA_RELATIVE_ROT);	//向きの設定
		m_pCameraModel->SetParent(m_pSupportModel);				//親の設定
		m_pCameraModel->SetShadow(false);						//影を描画しないように設定する
	}

	m_posV = DEFAULT_POS_V;				//カメラの視点の設定
	m_posR = DEFAULT_POS_R;				//カメラの注視点の設定
	m_fMaxRotY = DEFAULT_MAX_ROT_Y;		//Y軸の最大回転角度の設定
	m_fFrameMove = DEFAULT_FRAME_MOVE;	//毎フレーム加算される角度の設定

	m_pStaticNoise = CPolygon2D::Create(PRIORITY_LEVEL3);		//スノーノイズの生成

	if (m_pStaticNoise)
	{//nullチェック

		//ポリゴンの位置の、サイズ、テクスチャの種類と色の設定
		m_pStaticNoise->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));
		m_pStaticNoise->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));
		std::vector<int> nNumTex;
		nNumTex.push_back(DEFAULT_STATIC_TEXTURE_IDX);
		m_pStaticNoise->LoadTex(nNumTex);
		m_pStaticNoise->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.25f));
		m_pStaticNoise->SetDraw(false);

		//ランダムでテクスチャ座標の設定
		float fX = hmd::FloatRandam(0.0f, 1.0f), fY = hmd::FloatRandam(0.0f, 1.0f);

		m_pStaticNoise->SetTex(0,D3DXVECTOR2(fX, fY), D3DXVECTOR2(fX + s_fWidth, fY + 1.0f));
	}

	return S_OK;
}

// 終了
void CSecurityCamera::Uninit()
{
	//モデルの破棄
	if (m_pCameraModel)
	{
		m_pCameraModel->Uninit();
		delete m_pCameraModel;
		m_pCameraModel = nullptr;
	}
	if (m_pSupportModel)
	{
		m_pSupportModel->Uninit();
		delete m_pSupportModel;
		m_pSupportModel = nullptr;
	}
	if (m_pHackablePc)
	{
		m_pHackablePc = nullptr;
	}

	//スノーノイズの破棄
	if (m_pStaticNoise)
	{
		m_pStaticNoise->Uninit();
		m_pStaticNoise = nullptr;
	}

	//メモリの解放
	Release();
}

// 更新
void CSecurityCamera::Update()
{
	//アニメーションの処理
	Animation();

	//現在のモードを取得する
	CApplication::SCENE_MODE mode =  CApplication::GetInstance()->GetMode();	

	//ハッカーモードではなく、ハッキングできるPCがない又は、ハッキング中ではなかったら、処理が終わる
	if (mode != CApplication::MODE_HACKER || !m_pHackablePc || !m_bHacked)
		return;

	HackPC();
}

// 描画
void CSecurityCamera::Draw()
{
	//ワールドマトリックスの宣言
	D3DXMATRIX mtxWorld;

	//マトリックスの初期化
	D3DXMatrixIdentity(&mtxWorld);

	if (m_pSupportModel)
	{//nullチェック

		m_pSupportModel->Draw();					//カメラベースの描画
		mtxWorld = m_pSupportModel->GetMtxWorld();	//ワールドマトリックスの取得
	}

	if (m_pCameraModel)
	{//nullチェック

		m_pCameraModel->Draw(mtxWorld);				//親のワールドマトリックスを使用して、カメラモデルを描画する
	}
}


// 位置のセッター
void CSecurityCamera::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;			//位置の設定

	//モデルの位置の設定
	if (m_pSupportModel)
	{//nullチェック
		m_pSupportModel->SetPos(pos);
	}
}

// 過去位置のセッター
void CSecurityCamera::SetPosOld(const D3DXVECTOR3 &/*posOld*/)
{

}

// 向きのセッター
void CSecurityCamera::SetRot(const D3DXVECTOR3 &rot)
{
	m_rot = rot;			//向きの設定

	//モデルの向きの設定
	if (m_pSupportModel)
	{//nullチェック
		m_pSupportModel->SetRot(rot);
	}
}

// 大きさのセッター
void CSecurityCamera::SetSize(const D3DXVECTOR3 &/*size*/)
{

}

//エフェクトのインデックスの設定
void CSecurityCamera::SetRenderMode(int mode)
{
	if (mode < 0 || mode >= CModel3D::ERenderMode::Render_MAX)
		mode = 0;

	if (m_pCameraModel)
		m_pCameraModel->SetRenderMode((CModel3D::ERenderMode)mode);

	if (m_pSupportModel)
		m_pSupportModel->SetRenderMode((CModel3D::ERenderMode)mode);
}

//ハッカーに使われているかどうかの設定処理
void CSecurityCamera::SetCameraHackedState(const bool bHacked)
{
	m_bHacked = bHacked;		//ハッカーに使われているかどうかの設定

	//UIメッセージを生成する又は、破棄する
	if (bHacked)
		CreateUiMessage();
	else
		DestroyUiMessage();

	if (!m_bHacked && m_pStaticNoise)
	{
		m_pStaticNoise->SetDraw(false);		//使用されていない場合、描画しないようにする
	}
}

//このカメラを使うようにする
void CSecurityCamera::ActivateCamera()
{
	// カメラの追従設定(目標 : 無)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	m_lastRot = pCamera->GetRot();
	pCamera->SetFollowTarget(false);
	pCamera->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetPosVOffset(m_posV);
	pCamera->SetPosROffset(m_posR);
	pCamera->IsFixed(true);
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetPosVOffset(DEFAULT_MAP_POS_V);
	pCamera->SetPosROffset(DEFAULT_MAP_POS_R);

	if (m_pStaticNoise)
	{//nullチェック
		m_pStaticNoise->SetDraw(true);		//使用されている場合、描画ようにする
	}

	m_bHacked = true;				//ハッカーに使われている状態にする

	CDrone* pDrone = nullptr;		//ドローンへのポインタ

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
		pDrone = CHacker::GetDrone();		//ドローン情報を取得する

	if (pDrone)
	{//nullチェック

		pDrone->SetHackingState(true);		//ハッキング状態を設定する
		pDrone->SetStopState(true);			//動ける状態にする
	}

	{
		CHackerUI* pUi = CHacker::GetHackerUI();

		if (pUi)
		{
			pUi->SetDroneCommandUi(true);
		}
	}

	//UIメッセージを生成する
	CreateUiMessage();
}

//カメラを使用されていない状態にする
void CSecurityCamera::DeactivateCamera()
{
	CDrone*pDrone = CHacker::GetDrone();		//ドローンの取得

	if (!pDrone)
		return;

	// カメラの追従設定(目標 : ドローン)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(pDrone, 1.0f);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(m_lastRot);
	pCamera->SetUseRoll(true, true);
	pCamera->SetRot(m_lastRot);
	m_lastRot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	pCamera->IsFixed(false);

	// カメラの追従設定(目標 : ドローン)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(pDrone, 1.0f);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));

	if (m_pStaticNoise)
	{//nullチェック

		m_pStaticNoise->SetDraw(false);		//使用されていない場合、描画しないようにする
	}

	m_bHacked = false;			//ハッカーに使われている状態にする	
	m_nCameraIdx = 0;			//選択されているカメラインデックスを0に戻す

	if (pDrone)
	{//nullチェック

		pDrone->SetHackingState(false);		//ハッキング状態を設定する
		pDrone->SetStopState(false);		//動けない状態にする
	}

	{
		CHackerUI* pUi = CHacker::GetHackerUI();

		if (pUi)
		{
			pUi->SetDroneCommandUi(false);
		}
	}

	//UIメッセージを破棄
	DestroyUiMessage();
}

//カメラを使用されていない状態にする
void CSecurityCamera::DeactivateCamera(CDrone* pDrone)
{
	if (!pDrone)
		return;

	// カメラの追従設定(目標 : ドローン)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(pDrone, 1.0f);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(m_lastRot);
	pCamera->SetUseRoll(true, true);
	pCamera->SetRot(m_lastRot);
	m_lastRot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	pCamera->IsFixed(false);

	// カメラの追従設定(目標 : ドローン)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(pDrone, 1.0f);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));

	if (m_pStaticNoise)
	{//nullチェック

		m_pStaticNoise->SetDraw(false);		//使用されていない場合、描画しないようにする
	}

	m_bHacked = false;			//ハッカーに使われている状態にする	
	m_nCameraIdx = 0;			//選択されているカメラインデックスを0に戻す

	if (pDrone)
	{//nullチェック

		pDrone->SetHackingState(false);		//ハッキング状態を設定する
		pDrone->SetStopState(false);		//動けない状態にする
	}

	//UIメッセージを破棄
	DestroyUiMessage();
}



//=============================================================================
//
//									静的関数
//
//=============================================================================


//生成
CSecurityCamera* CSecurityCamera::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot)
{
	CSecurityCamera* pCamera = new CSecurityCamera;		//インスタンスを生成する

	if (FAILED(pCamera->Init()))
	{//初期化処理
		return nullptr;
	}

	pCamera->SetPos(pos);				//位置の設定
	pCamera->SetRot(rot);				//回転角度の設定
	pCamera->CalcCameraInfo();			//カメラの情報の計算処理

	m_vAllCameras.push_back(pCamera);	//カメラを追加する

	return pCamera;						//生成したインスタンスを返す
}

//生成
CSecurityCamera * CSecurityCamera::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot, CHackObj * pHackablePc)
{
	CSecurityCamera* pCamera = new CSecurityCamera;		//インスタンスを生成する

	if (FAILED(pCamera->Init()))
	{//初期化処理
		return nullptr;
	}

	pCamera->SetPos(pos);					//位置の設定
	pCamera->SetRot(rot);					//回転角度の設定
	pCamera->CalcCameraInfo();				//カメラの情報の計算処理
	pCamera->m_pHackablePc = pHackablePc;	//ハッキングできるPCへのポインタの設定

	m_vAllCameras.push_back(pCamera);		//カメラを追加する

	return pCamera;							//生成したインスタンスを返す
}

//カメラのクリア処理
void CSecurityCamera::ClearCameras()
{
	m_vAllCameras.clear();				//ベクトルをクリアする
	m_vAllCameras.shrink_to_fit();		//ベクトルのサイズを0に戻す
	m_nCameraIdx = 0;					//選択されているカメラインデックスを0に戻す
}

//モデルアニメーション
void CSecurityCamera::Animation()
{
	if (m_nDelay <= 0)
	{//ディレイが0になったら

		m_fRotY += m_fFrameMove;		//カメラモデルの向きの更新

		if (m_bHacked)
			CalcCameraInfo();				//カメラの視点と注視点の計算処理

		if ((m_fFrameMove > 0.0f && m_fRotY > m_fMaxRotY) || (m_fFrameMove < 0.0f && m_fRotY < -m_fMaxRotY))
		{//限度を超えたら、回転を逆にする

			m_nDelay = DEFAULT_ANIMATION_DELAY;		//ディレイをしばらく止まるように設定する
			m_fFrameMove *= -1.0f;					
		}

		if (m_pCameraModel)
		{//nullチェック
			m_pCameraModel->SetRot(DEFAULT_CAMERA_RELATIVE_ROT + D3DXVECTOR3(0.0f, m_fRotY, 0.0f));	//カメラモデルの向きの設定
		}
	}
	else
		m_nDelay--;				//ディレイの更新

	UpdateStaticNoise();		//スノーノイズの更新
}

//カメラの視点と注視点の計算処理
void CSecurityCamera::CalcCameraInfo()
{
	{
		D3DXVECTOR3 pos = m_pos, rot = m_rot;		//位置と向きの取得
		D3DXMATRIX mtxOut, mtxTrans, mtxRot;		//計算用のマトリックス

		rot.y += m_fRotY;							//現在のカメラ向きにする

		//マトリックスの初期化
		D3DXMatrixIdentity(&mtxOut);

		//カメラの回転を反映する
		D3DXMatrixRotationYawPitchRoll(&mtxRot, DEFAULT_CAMERA_RELATIVE_ROT.y, DEFAULT_CAMERA_RELATIVE_ROT.x, DEFAULT_CAMERA_RELATIVE_ROT.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxRot);

		//マトリックスの初期化
		D3DXMatrixIdentity(&mtxRot);

		//親の回転を反映する
		D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxRot);

		//位置を反映する
		D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
		D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxTrans);

		//視点と注視点の取得
		D3DXVECTOR3 posV = DEFAULT_POS_V, posR = DEFAULT_POS_R;

		//視点と注視点の位置を変換する(相対位置から絶対位置に変換)
		D3DXVec3TransformCoord(&posV, &posV, &mtxOut);
		D3DXVec3TransformCoord(&posR, &posR, &mtxOut);

		//視点と注視点の設定
		m_posV = posV;
		m_posR = posR;

		if (m_bHacked)
		{//カメラがハッカーに使われている場合

			//カメラの視点と注視点を設定する
			CCamera *pCamera = CApplication::GetInstance()->GetCamera();
			pCamera->SetFollowTarget(false);
			pCamera->SetPosVOffset(m_posV);
			pCamera->SetPosROffset(m_posR);
			pCamera = CApplication::GetInstance()->GetMapCamera();
			pCamera->SetPosVOffset(DEFAULT_MAP_POS_V);
			pCamera->SetPosROffset(DEFAULT_MAP_POS_R);
		}
	}
}

//スノーノイズの更新
void CSecurityCamera::UpdateStaticNoise()
{
	if (m_bHacked && m_pStaticNoise)
	{//ハッカーに使われている場合

		//ランダムでテクスチャ座標を設定する
		float fX = hmd::FloatRandam(0.0f, 1.0f), fY = hmd::FloatRandam(0.0f, 1.0f);

		m_pStaticNoise->SetTex(0, D3DXVECTOR2(fX, fY), D3DXVECTOR2(fX + s_fWidth, fY + 1.0f));
	}
}


//UIメッセージの生成処理
void CSecurityCamera::CreateUiMessage()
{
	//ハッキングできるPCがなかったら、何もせずに終わる
	if (!m_pHackablePc)
		return;

	//メッセージの破棄
	DestroyUiMessage();

	//メッセージの生成
	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(50.0f, 50.0f, 0.0f), E_BUTTON_TEXTURE_IDX, "Hack Pc", D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CSecurityCamera::DestroyUiMessage()
{
	//メッセージの破棄
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}

//カメラの切り替え処理
void CSecurityCamera::ChangeCamera()
{
	//カメラが存在するかどうかを確認する。それにPCハッキング中だったら、処理が終わる
	if ((int)m_vAllCameras.size() <= 0 || m_vAllCameras.data()[m_nCameraIdx]->m_bHackingPc)
		return;

	CInput* pInput = CApplication::GetInstance()->GetInput();		//インプットの取得

	if (pInput->Trigger(DIK_RIGHT))
	{//右ボタンを押したら

		m_vAllCameras.data()[m_nCameraIdx]->SetCameraHackedState(false);								//このカメラをハッカーに使われていない状態に戻す

		m_nCameraIdx++;		//選択されているカメラインデックスをインクリメントする

		if (m_nCameraIdx >= (int)m_vAllCameras.size())
		{//ベクトルサイズ以上だったら、0に戻す
			m_nCameraIdx = 0;
		}

		m_vAllCameras.data()[m_nCameraIdx]->ActivateCamera();		//選択されているカメラをハッカーに使われていない状態にする
	}
	else if (pInput->Trigger(DIK_LEFT))
	{//左ボタンを押したら

		m_vAllCameras.data()[m_nCameraIdx]->SetCameraHackedState(false);								//このカメラをハッカーに使われていない状態に戻す

		m_nCameraIdx--;		//選択されているカメラインデックスをデクリメントする

		if (m_nCameraIdx < 0)
		{//0より小さくなったら、ベクトルサイズ-1にする
			m_nCameraIdx = (int)m_vAllCameras.size() - 1;
		}
				
		m_vAllCameras.data()[m_nCameraIdx]->ActivateCamera();		//選択されているカメラをハッカーに使われていない状態にする
	}
	else if (pInput->Trigger(DIK_Q))
	{//0ボタンを押したら

		CDrone* pDrone = CHacker::GetDrone();

		if (pDrone && pDrone->GetLife() > 0)
			m_vAllCameras.data()[m_nCameraIdx]->DeactivateCamera();			//ドローンモードに切り替える
	}
}


//PCのハッキング
void CSecurityCamera::HackPC()
{
	CInput *pInput = CInput::GetKey();	//キーボードよびだし
	//CSceneMode* pMode = CApplication::GetInstance()->GetSceneMode();
	CHacker* pHacker = GetHackerMode();

	if(pHacker)
	{
		if (pInput->Trigger(DIK_E) && m_pHackablePc->GetHackState() == CHackObj::STATE_NOT_HACK)
		{
			m_pHackablePc->Hacking();
			m_pHackablePc->SetHackState(CHackObj::STATE_NOW_HACKING);

			m_bHackingPc = true;
			DestroyUiMessage();				//UIメッセージの破棄
		}

		if (pHacker->GetMiniMng()->GetClear() == true)
		{
			m_pHackablePc->SetHackState(CHackObj::STATE_HACKED);	//ハッキング完了
			pHacker->GetMiniMng()->SetClear(false);					//リセット
			m_pHackablePc = nullptr;								//ハッキングできるPCのポインタをnullにする(これから同じPCをハッキングできなくようになる)
			m_bHackingPc = false;

			CTask* pTask = CHacker::GetTask();			//タスクの取得

			if (pTask)
			{//nullチェック
				pTask->SubtractHackerTask(CTask::HACKER_TASK_HACK);	//タスクの更新
			}
		}
	}
}

//ハッカーモードの取得
CHacker* CSecurityCamera::GetHackerMode()
{
	//現在のモードを取得する
	CSceneMode* pMode = CApplication::GetInstance()->GetSceneMode();		
	CHacker* pHacker = nullptr;												

	//モードを取得できなかった場合、何もせずに終わる
	if (!pMode)
		return nullptr;

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{//ハッカーモードだけの場合、ダイナミックキャストをしてから、ポインタを返す

		pHacker = dynamic_cast<CHacker*>(pMode);
	}

	return pHacker;
}
