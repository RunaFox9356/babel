#include "shader_manager.h"
#include "application.h"
#include "renderer.h"
#include "file.h"
#include "utility.h"

namespace
{
	D3DVERTEXELEMENT9 decl_Default[] = {
		{ 0 , 0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 12, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
		{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,    0 },
		{ 0, 28, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		D3DDECL_END()
	};

	D3DVERTEXELEMENT9 decl_SkinMesh[] =	{
		{ 0, 0,  D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,     0 },
		{ 0, 12, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,       0 },
		{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,        0 },
		{ 0, 28, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,     0 },
		{ 0, 36, D3DDECLTYPE_FLOAT4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,  0 },
		{ 0, 52, D3DDECLTYPE_UBYTE4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
		D3DDECL_END()
	};
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーマネージャのコンストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CShaderManager::CShaderManager()
{
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// 頂点宣言オブジェクト
	m_decl["Default"]  = decl_Default;
	m_decl["SkinMesh"] = decl_SkinMesh;

	// 頂点宣言オブジェクトの生成
	pDevice->CreateVertexDeclaration(decl_Default, &m_vtxDecl["Default"]);
	pDevice->CreateVertexDeclaration(decl_SkinMesh, &m_vtxDecl["SkinMesh"]);
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーマネージャのデストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CShaderManager::~CShaderManager()
{
	assert(m_effect.empty());
	assert(m_decl.empty());
	assert(m_vtxDecl.empty());
	assert(m_techniqueCache.empty());
	assert(m_parameterCache.empty());
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// エフェクト、ハンドルの読み込み
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::Load(nlohmann::json& list)
{
	LPDIRECT3DDEVICE9 pDevice	  = CApplication::GetInstance()->GetRenderer()->GetDevice();
	LPD3DXBUFFER	  errorBuffer = nullptr;
	std::string		  label		  = list["Label"];
	std::string		  path		  = list["Path"];

	// エフェクトファイルの読み込み
	LPD3DXEFFECT effect = nullptr;
	if (FAILED(D3DXCreateEffectFromFile(pDevice,
		_T(path.c_str()),
		nullptr,
		nullptr,
		0,
		nullptr,
		&effect,
		&errorBuffer)))
	{
		if (errorBuffer != nullptr)
		{
			const char* errorMessage = static_cast<const char*>(errorBuffer->GetBufferPointer());
			errorBuffer->Release(); // リソースの解放
		}

		assert(false);
	}
	m_effect.insert(std::make_pair(label, effect));

	// テクニックのハンドルを保存
	size_t techniqueSize = list["Technique"].size();
	for (size_t i = 0; i < techniqueSize; i++)
	{
		std::string techName = "Technique" + std::to_string(i);
		AddTechniqueCache(label, list["Technique"][techName]);
	}

	// パラメータのハンドルを保存
	size_t parameterSize = list["Parameter"].size();
	for (size_t i = 0; i < parameterSize; i++)
	{
		std::string paramName = "Parameter" + std::to_string(i);
		AddParameterCache(label, list["Parameter"][paramName]);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// Jsonからシェーダーを使用するための情報の読み取り
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::LoadAll()
{
	nlohmann::json list = CFile::LoadJson(L"Data/FILE/shader.json");

	size_t size = list["SHADER"].size();
	for (size_t i = 0; i < size; i++)
	{
		Load(list["SHADER"].at(i));
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーマネージャの破棄
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::ReleaseAll()
{
	for (auto& p : m_effect)
	{
		if (p.second != nullptr)
		{
			p.second->Release();
			p.second = nullptr;
		}
	}
	m_effect.clear();

	for (auto& p : m_techniqueCache)
	{	// テクニックのキャッシュを破棄
		p.second = nullptr;
	}
	m_techniqueCache.clear();

	for (auto& p : m_parameterCache)
	{	// パラメータのキャッシュを破棄
		p.second = nullptr;
	}
	m_parameterCache.clear();

	// 頂点宣言オブジェクトの破棄
	if (!m_decl.empty())
	{
		m_decl.clear();
	}
	if (!m_vtxDecl.empty())
	{
		m_vtxDecl.clear();
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーの適用処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::Apply(std::string label, unsigned int pass)
{
	LPD3DXEFFECT pShader = m_effect[label];
	if (pShader != nullptr)
	{
		pShader->Begin(NULL, 0);
		pShader->BeginPass(pass);
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// シェーダーの終了処理
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::End(std::string label)
{
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();
	LPD3DXEFFECT pShader	  = m_effect[label];
	if (pShader != nullptr)
	{
		pShader->EndPass();
		pShader->End();
	}

	// 固定機能パイプラインに戻す
	pDevice->SetVertexShader(NULL);
	pDevice->SetPixelShader(NULL);
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// テクニックのキャッシュに要素を追加
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::AddTechniqueCache(const std::string& label, const std::string& handleName)
{
	std::string key = label + "_" + handleName;

	if (m_techniqueCache.count(key) > 0)
	{
		assert(false);
	}
	m_techniqueCache[key] = m_effect[label]->GetTechniqueByName(handleName.c_str());
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パラメータのキャッシュに要素を追加
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CShaderManager::AddParameterCache(const std::string& label, const std::string& handleName)
{
	std::string key = label + "_" + handleName;

	if (m_parameterCache.count(key) > 0)
	{
		assert(false);
	}
	m_parameterCache[key] = m_effect[label]->GetParameterByName(NULL, handleName.c_str());
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// テクニックのキャッシュを取得
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
D3DXHANDLE CShaderManager::GetTechniqueCache(const std::string& label, const std::string& handleName)
{
	std::string key = label + "_" + handleName;

	// キャッシュからハンドルを取得
	if (m_techniqueCache.count(key) < 0)
	{
		return nullptr;
	}
	return m_techniqueCache[key];
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パラメータのキャッシュを取得
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
D3DXHANDLE CShaderManager::GetParameterCache(const std::string& label, const std::string& handleName)
{
	std::string key = label + "_" + handleName;

	// キャッシュからハンドルを取得
	if (m_parameterCache.count(key) < 0)
	{
		return nullptr;
	}
	return m_parameterCache[key];
}