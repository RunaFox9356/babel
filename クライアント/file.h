//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================
#ifndef _FILE_H_			// このマクロ定義がされてなかったら
#define _FILE_H_			// 二重インクルード防止のマクロ定義

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "renderer.h"
#include "object.h"


class CFile
{
public:
	static nl::json LoadJson(const wchar_t* fileName);

	void Savefile(const char * pFileName);

	nl::json GetLoadJson() { return m_loadJson; }
	nl::json GetSaveJson() { return m_savejson; }
	
	void SaveJson(nl::json Save, const char * pPassName);
private:
	nl::json m_savejson;//リストの生成
	nl::json m_loadJson;//リストの生成

};
#endif