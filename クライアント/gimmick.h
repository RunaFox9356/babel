//=============================================================================
//
// gimmick.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _GIMMICK_H
#define _GIMMICK_H


//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

class CGimmick : public CObject
{
public:

	//ギミックの種類(マップツールと同じなので、変更する前、マップツールも確認してください。)
	enum GIMMICK_TYPE
	{
		GIMMICK_TYPE_DOOR = 0,
		GIMMICK_TYPE_PC,
		GIMMICK_TYPE_LOCKER,
		GIMMICK_TYPE_LOCKED_DOOR,
		GIMMICK_TYPE_HACK_PC,
		GIMMICK_TYPE_ESCAPE_POINT,
		GIMMICK_TYPE_HOSTAGE,
		GIMMICK_TYPE_GUN,
		GIMMICK_TYPE_JAMMING_TOOL,
		GIMMICK_TYPE_HACKABLE_DOOR,
		GIMMICK_TYPE_STEAL_OBJ,

		GIMMICK_TYPE_MAX
	};

	enum EHackState
	{
		STATE_NOT_HACK = 0,		//未ハッキング
		STATE_NOW_HACKING,		//ハッキング中
		STATE_HACKED,			//ハッキング済み
	};


	CGimmick();																//コンストラクタ
	~CGimmick() override;													//デストラクタ

	virtual void SetPos(const D3DXVECTOR3 &pos) override = 0;				// 位置のセッター
	virtual void SetPosOld(const D3DXVECTOR3 &posOld) override = 0;			// 過去位置のセッター
	virtual void SetRot(const D3DXVECTOR3 &rot) override = 0;				// 向きのセッター
	virtual void SetSize(const D3DXVECTOR3 &size) override = 0;				// 大きさのセッター
	virtual D3DXVECTOR3 GetPos() override = 0;								// 位置のゲッター
	virtual D3DXVECTOR3 GetPosOld()  override = 0;							// 過去位置のゲッター
	virtual D3DXVECTOR3 GetRot()  override = 0;								// 向きのゲッター
	virtual D3DXVECTOR3 GetSize()  override = 0;							// 大きさのゲッター
	virtual void SetRenderMode(int mode) = 0;								// エフェクトのインデックスの設定

	//プレイヤーがこのギミックを使ったかどうかの設定
	void SetInteraction(const bool bInteraction) { m_bInteraction = bInteraction; }

	//プレイヤーがこのギミックを使ったかどうかの取得
	const bool GetInteraction() { return m_bInteraction; }

	void SetAnimation(const bool isAnimation) { m_isAnimation = isAnimation; }	//アニメーションフラグの設定
	const bool GetAnimation() { return m_isAnimation; }							//アニメーションフラグの取得
	void SetId(int GimmickID) { m_GimmickID = GimmickID; }						//IDの設定
	int GetId() { return m_GimmickID; }											//IDの取得
	const bool GetIsOnMap() { return m_bOnMap; }								//マップに表示されているかどうかの取得処理
	virtual void SetIsOnMap(const bool bOnMap) { m_bOnMap = bOnMap; }			//マップに表示されているかどうかの設定処理

	void SetFoundState(const bool bFound) { m_bFound = bFound; }				//ハッカーに確認されたかどうかの設定
	const bool HasBeenFound() { return m_bFound; }								//ハッカーに確認されたかどうかの取得
	void SetHackState(EHackState State) { m_HackState= State; };
	EHackState GetHackState() { return m_HackState; };
private:
	bool	m_isAnimation;			//アニメーションフラグ
	bool	m_bInteraction;			//使われているかどうか
	int		m_GimmickID;			//ID
	bool	m_bFound;				//ハッカーに確認されたかどうか
	bool	m_bOnMap;				//マップに表示されているかどうか
	EHackState m_HackState;			

};



#endif