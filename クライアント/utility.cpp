﻿//==================================================
// utility.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "utility.h"
#include "manager.h"
#include "camera.h"
#include "boundingBox3D.h"
#include "boundingSphere3D.h"

namespace hmd
{
	bool CollisionCircle(const D3DXVECTOR3& pos1, float radius1, const D3DXVECTOR3& pos2, float radius2);
	D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard);
	D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard);
	float easeInSine(float X);
	float easeInQuad(float X);
	bool is_sjis_lead_byte(int c);
	//--------------------------------------------------
	// 目的の角度の正規化
	//--------------------------------------------------
	void NormalizeAngleDest(float *pAngle, float *pAngleDest)
	{
		// 目的の角度の正規化
		if (*pAngleDest - *pAngle > D3DX_PI)
		{
			*pAngleDest -= D3DX_PI * 2.0f;
		}
		else if (*pAngleDest - *pAngle < -D3DX_PI)
		{
			*pAngleDest += D3DX_PI * 2.0f;
		}
	}


	//=============================================================================
	// ワールド座標をスクリーン座標へのキャスト
	//=============================================================================
	D3DXVECTOR3 WorldCastScreen(D3DXVECTOR3 *screenPos,			// スクリーン座標
		const D3DXVECTOR3& screenSize,									// スクリーンサイズ
		const D3DXMATRIX& mtxView,									// ビューマトリックス
		const D3DXMATRIX& mtxProjection)								// プロジェクションマトリックス
	{
		// 変数宣言
		D3DXVECTOR3 ScreenPos;

		// 計算用マトリックスの宣言
		D3DXMATRIX InvView, InvPrj, VP, InvViewport;

		// 各行列の逆行列を算出
		D3DXMatrixInverse(&InvView, NULL, &mtxView);
		D3DXMatrixInverse(&InvPrj, NULL, &mtxProjection);
		D3DXMatrixIdentity(&VP);
		VP._11 = screenSize.x / 2.0f; VP._22 = -screenSize.y / 2.0f;
		VP._41 = screenSize.x / 2.0f; VP._42 = screenSize.y / 2.0f;
		D3DXMatrixInverse(&InvViewport, NULL, &VP);

		// ワールド座標へのキャスト
		D3DXMATRIX mtxWorld = InvViewport * InvPrj * InvView;

		D3DXVec3TransformCoord(&ScreenPos, screenPos, &mtxWorld);

		return ScreenPos;
	}

	//--------------------------------------------------
	// 角度の正規化
	//--------------------------------------------------
	float NormalizeAngle(float *pAngle)
	{
		if (*pAngle >= D3DX_PI)
		{// 3.14より大きい
			*pAngle -= D3DX_PI * 2.0f;
		}
		else if (*pAngle <= -D3DX_PI)
		{// -3.14より小さい
			*pAngle += D3DX_PI * 2.0f;
		}
		return *pAngle;
	}

	//--------------------------------------------------
	// 2Dベクトルの外積
	//--------------------------------------------------
	float Vec2Cross(D3DXVECTOR3* v1, D3DXVECTOR3* v2)
	{
		return v1->x * v2->z - v1->z * v2->x;
	}

	//--------------------------------------------------
	// 2Dベクトルの内積
	//--------------------------------------------------
	float D3DXVec2Dot(D3DXVECTOR3* v1, D3DXVECTOR3* v2)
	{
		return v1->x * v2->x + v1->z * v2->z;
	}

	//---------------------------------------------------------------------------
	// 小数点のランダム
	//---------------------------------------------------------------------------
	float FloatRandam(float fMax, float fMin)
	{
		return ((rand() / (float)RAND_MAX) * (fMax - fMin)) + fMin;
	}
	//--------------------------------------------------
	// 整数のランダム
	//--------------------------------------------------
	int IntRandom(int nMax, int nMin)
	{
		return (int)((rand() / (float)RAND_MAX) * (nMax - nMin)) + nMin;
	}


	float easeInOutQuint(float x)
	{
		return x < 0.5 ? 16 * x * x * x * x * x : 1 - ((-2 * x + 2, 5) * (-2 * x + 2, 5)) / 2;
	}

	float easeOutQuint(float x)
	{
		return 1 - powf(1 - x, 5);
	}

	//--------------------------------------------------
	// スカラ三重積
	//--------------------------------------------------
	float ScalarTriple(const D3DXVECTOR3 & a, const D3DXVECTOR3 & b, const D3DXVECTOR3 & c)
	{
		// aとbの外積
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &a, &b);

		// スカラ三重積の結果
		float fScalarTriple = D3DXVec3Dot(&cross, &c);

		return fScalarTriple;
	}

	float SearchLargest(std::vector<float>& fSearchGroup)
	{
		// 返り値
		float fLargest = 0.0f;

		for (int nCnt = 0; nCnt < fSearchGroup.size(); nCnt++)
		{
			if (fLargest <= fSearchGroup[nCnt])
			{
				fLargest = fSearchGroup[nCnt];
			}
		}

		return fLargest;
	}
}

float hmd::easeInSine(float X)
{
	return 1 - cos((X * D3DX_PI) / 2);
}

float hmd::easeInQuad(float X)
{
	return X * X;
}

//=============================================================================
//1バイト文字をシフトJISかどうか判定する関数
//=============================================================================
bool hmd::is_sjis_lead_byte(int c)
{
	return (((c & 0xffu) ^ 0x20u) - 0xa1) < 94u / 2;
}

//=============================================================================
// 平面と線分の交点を算出
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
bool hmd::IntersectionLineToPlane(const D3DXVECTOR3 & start, const D3DXVECTOR3 & goal, const D3DXVECTOR3 & vector0, const D3DXVECTOR3 & vector1, const D3DXVECTOR3 & vector2 ,D3DXVECTOR3 &OutPos)
{
	// 返り値用の変数の定義
	D3DXVECTOR3 crossPos;

	// 面の辺の算出
	D3DXVECTOR3 side0 = vector1 - vector0;
	D3DXVECTOR3 side1 = vector2 - vector1;

	// 面法線ベクトルの算出
	D3DXVECTOR3 norPlane;
	D3DXVec3Cross(&norPlane, &side0, &side1);
	D3DXVec3Normalize(&norPlane, &norPlane);

	// ラインの法線ベクトル
	D3DXVECTOR3 norLine = goal - start;
	D3DXVec3Normalize(&norLine, &norLine);

	// 面ベクトルの算出
	float fVtxPlane = -(norPlane.x * vector0.x + norPlane.y * vector0.y + norPlane.z * vector0.z);

	if (D3DXVec3Dot(&norPlane, &norLine) == 0)
	{
		int hoge = 0;
		OutPos = start;
		return false;
	}
	// 交点までの距離を算出
	float fCrossPoint = D3DXVec3Dot(&(vector0 - start), &norPlane) / D3DXVec3Dot(&norLine, &norPlane);
	
	// 交点の算出
	crossPos = start + (fCrossPoint * norLine);

	if (D3DXVec3Length(&(crossPos - start)) > D3DXVec3Length(&(goal - start)))
	{
		OutPos = start;
		return false;
	}

	if (!hmd::IsPointInsideTriangle(crossPos, vector0, vector1, vector2))
	{
		OutPos = start;
		return false;
	}

	//if (((norPlane.x * crossPos.x + norPlane.y * crossPos.y + norPlane.z * crossPos.z + fCrossPoint) * (norPlane.x * crossPos.x + norPlane.y * crossPos.y + norPlane.z * crossPos.z + fCrossPoint)) > 1.0f)
	//{//ここに入ると平面じゃない
 //		int a = 0;
	//}

	OutPos = crossPos;
	return true;
}

//=============================================================================
// 三角形と線分の交点を算出
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
bool hmd::IntersectionLineToTriangle(const D3DXVECTOR3 & start, const D3DXVECTOR3 & goal, const D3DXVECTOR3 & vector0, const D3DXVECTOR3 & vector1, const D3DXVECTOR3 & vector2, D3DXVECTOR3 & OutPos)
{
	// 交点の位置
	D3DXVECTOR3 crossPos;

	// 面の辺の算出
	D3DXVECTOR3 side0 = vector1 - vector0;
	D3DXVECTOR3 side1 = vector2 - vector1;

	// 線分の算出
	D3DXVECTOR3 line = goal - start;
	D3DXVECTOR3 norLine;
	D3DXVec3Normalize(&norLine, &line);

	// 法線ベクトル
	D3DXVECTOR3 norTriangle;
	D3DXVec3Cross(&norTriangle, &side0, &side1);
	D3DXVec3Normalize(&norTriangle, &norTriangle);

	// 線分ベクトルと法線ベクトルディレクションチェック
	float direction = D3DXVec3Dot(&norLine, &norTriangle);
	if (direction <= 0) return false;

	// 線分が貫通するかチェックする
	D3DXVECTOR3 lineChecker = start - vector0;

	float t = D3DXVec3Dot(&lineChecker, &norTriangle);
	if (t < 0.0f) return false;
	if (t > direction) return false;

	// 重心座標を計算し範囲内にあるかチェック
	D3DXVECTOR3 e;
	D3DXVec3Cross(&e, &line, &lineChecker);

	float v = D3DXVec3Dot(&side1, &e);
	if (v < 0.0f || v > direction) return false;

	float w = -D3DXVec3Dot(&side0, &e);
	if (w < 0.0f || v + w > direction) return false;

	// 重心座標の算出
	float ood = 1.0f / direction;
	t *= ood;
	v *= ood;
	w *= ood;
	float u = 1.0f - v - w;

	return true;
}

//=============================================================================
// pointが三点の内にあるかを算出
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
bool hmd::IsPointInsideTriangle(const D3DXVECTOR3 & point, const D3DXVECTOR3 & vector0, const D3DXVECTOR3 & vector1, const D3DXVECTOR3 & vector2)
{
	//// 三角形の３つの辺を求める
	//D3DXVECTOR3 edge0 = vector1 - vector0;
	//D3DXVECTOR3 edge1 = vector2 - vector1;
	//D3DXVECTOR3 edge2 = vector0 - vector2;

	//// 三角形の３つの法線ベクトルを求める
	//D3DXVECTOR3 normal0, normal1, normal2, cross0, cross1, cross2;
	///*D3DXVec3Cross(&normal0, &edge0, &edge1);
	//D3DXVec3Cross(&normal1, &edge1, &edge2);
	//D3DXVec3Cross(&normal2, &edge2, &edge0);*/
	//D3DXVec3Normalize(&normal0, &edge0);
	//D3DXVec3Normalize(&normal1, &edge1);
	//D3DXVec3Normalize(&normal2, &edge2);

	//// 点と三角形の各頂点を結ぶベクトル
	//D3DXVECTOR3 toPoint0 = point - vector0;
	//D3DXVECTOR3 toPoint1 = point - vector1;
	//D3DXVECTOR3 toPoint2 = point - vector2;

	//D3DXVec3Normalize(&toPoint0, &toPoint0);
	//D3DXVec3Normalize(&toPoint1, &toPoint1);
	//D3DXVec3Normalize(&toPoint2, &toPoint2);

	//D3DXVec3Cross(&cross0, &edge0, &toPoint0);
	//D3DXVec3Cross(&cross1, &edge1, &toPoint1);
	//D3DXVec3Cross(&cross2, &edge2, &toPoint2);

	//// 法線ベクトルとtoPointベクトルの内積
	//float dot0 = D3DXVec3Dot(&cross0, &cross1);
	//float dot1 = D3DXVec3Dot(&cross0, &cross2);
	//float dot2 = D3DXVec3Dot(&cross1, &cross2);

	//// 法線ベクトルが同じ方向を向いているかどうかを確認
	//if ((dot0 >= 0.0f && dot1 >= 0.0f && dot2 >= 0.0f) 
	//	|| (dot0 <= 0.0f && dot1 <= 0.0f && dot2 <= 0.0f))
	//{// 点は三角形の内部にある
	//	return true;  
	//}

	//// 点は三角形の外部にある
	//return false;  

	D3DXVECTOR3 edge0 = vector1 - vector0, edge1 = vector2 - vector0;
	D3DXVECTOR3 pointToA = vector0 - point, pointToB = vector1 - point, pointToC = vector2 - point;
	D3DXVECTOR3 cross = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	//三角形の面積を計算する
	D3DXVec3Cross(&cross, &edge0, &edge1);
	float fTotalArea = (D3DXVec3Length(&cross)) * 0.5f;

	if (fTotalArea == 0.0f)
		return false;

	float fAreaA = 0.0f, fAreaB = 0.0f, fAreaC = 0.0f;

	D3DXVec3Cross(&cross,  &pointToA, &pointToB);
	fAreaA = (D3DXVec3Length(&cross)) / (fTotalArea * 2.0f);

	D3DXVec3Cross(&cross, &pointToB, &pointToC);
	fAreaB = (D3DXVec3Length(&cross)) / (fTotalArea * 2.0f);

	D3DXVec3Cross(&cross, &pointToC, &pointToA);
	fAreaC = (D3DXVec3Length(&cross)) / (fTotalArea * 2.0f);

	bool bA = (fAreaA >= 0.0f && fAreaA <= 1.0f);
	bool bB = (fAreaB >= 0.0f && fAreaB <= 1.0f);
	bool bC = (fAreaC >= 0.0f && fAreaC <= 1.0f);
	bool bD = fAreaA + fAreaB + fAreaC > 1.0f - 0.1f && fAreaA + fAreaB + fAreaC < 1.0f + 0.1f;

	if (bA && bB && bC && bD)
		return true;

	return false;
}

//=============================================================================
// 特定の三点を平面に変換する
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
void hmd::CreatePlaneFromPoints(const D3DXVECTOR3 & vector0, const D3DXVECTOR3 & vector1, const D3DXVECTOR3 & vector2, D3DXPLANE & plane)
{
	// ベクトル1とベクトル2を元に法線ベクトルを求める
	D3DXVECTOR3 v1 = vector1 - vector0;
	D3DXVECTOR3 v2 = vector2 - vector0;
	D3DXVECTOR3 normal;
	D3DXVec3Cross(&normal, &v1, &v2);
	D3DXVec3Normalize(&normal, &normal);

	// 法線ベクトルと点1を使用して平面を初期化する
	D3DXPlaneFromPointNormal(&plane, &vector0, &normal);
}

//=============================================================================
// 壁ずりベクトルの算出
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
D3DXVECTOR3 * hmd::CalcWallScratchVector(D3DXVECTOR3 * out, const D3DXVECTOR3 & front, const D3DXVECTOR3 & normal)
{
	D3DXVECTOR3 normal_n;
	D3DXVec3Normalize(&normal_n, &normal);
	return D3DXVec3Normalize(out, &(front - D3DXVec3Dot(&front, &normal_n) * normal_n));
}

//=============================================================================
// 円の当たり判定
//=============================================================================
bool hmd::CollisionCircle(const D3DXVECTOR3& pos1, float radius1, const D3DXVECTOR3& pos2, float radius2)
{
	// ２つの物体の半径同士の和
	float radius = radius1 + radius2;

	// Xの差分
	float diffX = pos1.x - pos2.x;

	// Yの差分
	float diffY = pos1.y - pos2.y;

	// 現在の２点の距離
	float length = sqrtf((diffX * diffX) + (diffY * diffY));

	if (radius >= length)
	{// ２つの物体の半径同士の和より現在の２点の距離が小さいかどうか
	 // 当たった
		return true;
	}

	// 当たってない
	return false;
}

//=============================================================================
// 当たり判定
// Author : 浜田琉雅
// 概要 : リストの解除と破棄をする処理
//=============================================================================
SHitData hmd::IsBoundingBoxCollision(const CBoundingBox3D& box1, const CBoundingBox3D& box2)
{
	SHitData data;
	data.isUse = false;
	data.isHitType = 0;

	// 1つの境界ボックスの右側がもう一つの境界ボックスの左側より左にある場合、衝突していない
	if (box1.m_right < box2.m_left || box2.m_right < box1.m_left)
	{
		return data;
	}

	// 1つの境界ボックスの下側がもう一つの境界ボックスの上側より上にある場合、衝突していない
	if (box1.m_bottom < box2.m_top || box2.m_bottom < box1.m_top)
	{
		return data;
	}

	// 1つの境界ボックスの後ろ側がもう一つの境界ボックスの前側より前にある場合、衝突していない
	if (box1.m_back < box2.m_front || box2.m_back < box1.m_front)
	{
		return data;
	}

	data.isUse = true;
	// 上記の条件を満たさない場合、境界ボックスは衝突している
	return data;
}
//=============================================================================
// 当たり判定
// Author : 浜田琉雅
// 概要 : リストの解除と破棄をする処理
//=============================================================================
SHitData hmd::IsBoxCollision(const CBoundingBox3D& box1, const CBoundingBox3D& box2)
{
	SHitData data;
	data.isUse = true;
	data.isHitType = 5;


	if ((box1.m_Pos.y - box1.m_Size.y) <= (box2.m_Pos.y + box2.m_Size.y))
	{
		data.isHitType = 1;
	}
	if ((box1.m_Pos.y + box1.m_Size.y) >= (box2.m_Pos.y - box2.m_Size.y))
	{
		data.isHitType = 2;
	}
	if ((box1.m_Pos.x - box1.m_Size.x) <= (box2.m_Pos.x + box2.m_Size.x))
	{
		data.isHitType = 3;
	}
	//当たり判定
	if ((box1.m_Pos.x + box1.m_Size.x) >= (box2.m_Pos.x - box2.m_Size.x) )
	{
		data.isHitType = 4;
	}
	if (data.isHitType == 5)
	{
		data.isUse = false;
	}
	
	// 上記の条件を満たさない場合、境界ボックスは衝突している
	return data;
}

SHitData hmd::IsSphereCollision(const CBoundingSphere3D & sphere1, const CBoundingSphere3D & sphere2)
{
	SHitData data;
	data.isUse = true;
	data.isHitType = 5;

	// 自分の情報を取得する
	D3DXVECTOR3 pos = sphere2.m_Pos;
	float fRadius = sphere2.m_fRadius;

	// 目標の情報取得
	D3DXVECTOR3 posTarget = sphere1.m_Pos;
	float fRadiusTarget = sphere2.m_fRadius;

	// 判定を行う距離を算出
	float fJudgeDistance = fRadius + fRadiusTarget;

	// お互いの位置の差を算出
	D3DXVECTOR3 distance = posTarget - pos;
	float fDistance = sqrtf((distance.x * distance.x) + (distance.y * distance.y) + (distance.z * distance.z));

	if (fDistance <= fJudgeDistance)
	{// 位置の差が判定を行う距離より短い場合
		data.isHitType = 4;
	}

	//当たり判定
	if (data.isHitType == 5)
	{
		data.isUse = false;
	}

	// 上記の条件を満たさない場合、境界ボックスは衝突している
	return data;
}
