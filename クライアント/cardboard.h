//=============================================================================
//
// hostage.h
// Author: 磯江 寿希亜
// Author: Ricci Alex
//
//=============================================================================

#ifndef _CARDBOARD_H_
#define _CARDBOARD_H_

#include "model_obj.h"
#include "gimmick.h"

class CMove;
class CCollision_Rectangle3D;
class CUiMessage;
class CModelObj;
class CAgent;
class CPlayer;
class CCardboard :public CGimmick
{
public:

public:
	static CCardboard *Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f));		// モデルオブジェクトの生成


	CCardboard();						//コンストラクタ
	~CCardboard()override;			//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override {}			// 描画


	void SetPos(const D3DXVECTOR3 &pos) override;							//位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}				//前回の位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;							//向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};					//サイズのセッター

	D3DXVECTOR3 GetPos() override;											//位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }				//前回の位置のゲッター
	D3DXVECTOR3 GetRot()  override;											//向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }				//サイズのゲッター

	void SetRenderMode(int mode) override;									// エフェクトのインデックスの設定



private:

	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIメッセージの生成処理
	void DestroyUiMessage();											//UIメッセージの破棄処理


private:

	CUiMessage*						m_pUiMessage;					//メッセージのUI
	CPlayer*						m_pearentu;
	CModelObj*						m_pModel;						//人質のモデル
	CCollision_Rectangle3D*			m_pCollision;
};


#endif
