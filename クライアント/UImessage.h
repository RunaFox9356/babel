//=============================================================================
//
// UImessage.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _UI_MESSAGE_H
#define _UI_MESSAGE_H


//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CText;


class CUiMessage : public CPolygon2D
{
public:
	CUiMessage();
	~CUiMessage() override;

	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新
	void Draw() override;							// 描画


	static CUiMessage* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const int nTextureIdx, const char* pMessageText, const D3DXCOLOR col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));	//生成

private:

	void CreateText(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const char* pMessageText, const D3DXCOLOR col);		//テキストの生成

private:


	CText* m_pText;					//テキストへのポインタ
};


#endif