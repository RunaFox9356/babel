//=============================================================================
//
// 3Dモデルクラス(model3D.h)
// Author : 唐�ｱ結斗
// 概要 : 3Dモデル生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <algorithm>

#include "model3D.h"
#include "renderer.h"
#include "application.h"
#include "texture.h"
#include "light.h"
#include "camera.h"
#include "shader_manager.h"
#include "file.h"
#include "shadow_volume.h"
#include "calculation.h"
#include "boundingSphere3D.h"
#include "polygon3D.h"

#include "utility.h"

namespace
{
	const std::string effectLabel = "Effect";
	const float eraseShadowDist = 1200.0f;
	const float eraseModelDist = 2000.0f;
}

//--------------------------------------------------------------------
// 静的メンバ変数定義
//--------------------------------------------------------------------
int CModel3D::m_nMaxModel = 0;									// モデル数		
std::map<std::string, CModel3D::MODEL_MATERIAL, std::less<>> CModel3D::m_ModelMap; //TODO:スタティック
std::vector<std::string> CModel3D::m_KeyMap;
std::vector<CModel3D*> CModel3D::m_pModelGroup;
std::vector<CModel3D*> CModel3D::m_pSearchModel;

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 3Dモデルを生成する
//=============================================================================
CModel3D * CModel3D::Create()
{
	// オブジェクトインスタンス
	CModel3D *pModel3D = nullptr;

	// メモリの解放
	pModel3D = new CModel3D;

	// メモリの確保ができなかった
	assert(pModel3D != nullptr);

	// 数値の初期化
	pModel3D->Init();

	// インスタンスを返す
	return pModel3D;
}

//=============================================================================
// モデル情報の初期化
// Author : 唐�ｱ結斗
// 概要 : 読み込んだモデル情報を元に3Dモデルを生成する
//=============================================================================
void CModel3D::InitModel()
{

}

//=============================================================================
// モデル情報の終了
// Author : 唐�ｱ結斗
// 概要 : モデル情報の終了
//=============================================================================
void CModel3D::UninitModel()
{
	for (const auto &pair : m_ModelMap)
	{
		// メッシュの破棄
		if (m_ModelMap[pair.first].pMesh != nullptr)
		{
			m_ModelMap[pair.first].pMesh->Release();
			m_ModelMap[pair.first].pMesh = nullptr;
		}

		// マテリアルの破棄
		if (m_ModelMap[pair.first].pBuffer != nullptr)
		{
			m_ModelMap[pair.first].pBuffer->Release();
			m_ModelMap[pair.first].pBuffer = nullptr;
		}
	}
}

//=============================================================================
// Xファイルの読み込み
// Author : 唐�ｱ結斗
// 概要 : Xファイルの読み込みを行う
//=============================================================================
void CModel3D::LoadModel(const char *pFileName)
{
	// 変数宣言
	char aStr[128];
	int nCntModel = 0;

	// ファイルの読み込み
	FILE *pFile = fopen(pFileName, "r");

	if (pFile != nullptr)
	{
		while (fscanf(pFile, "%s", &aStr[0]) != EOF)
		{// "EOF"を読み込むまで 
			if (strncmp(&aStr[0], "#", 1) == 0)
			{// 一列読み込む
				fgets(&aStr[0], sizeof(aStr), pFile);
			}

			if (strstr(&aStr[0], "MAX_TYPE") != NULL)
			{
				fscanf(pFile, "%s", &aStr[0]);
				fscanf(pFile, "%d", &m_nMaxModel);

			}

			if (strstr(&aStr[0], "MODEL_FILENAME") != NULL)
			{
				fscanf(pFile, "%s", &aStr[0]);
				nCntModel++;
			}
		}
	}
	else
	{
		assert(false);
	}
}

//=============================================================================
// Xファイルの読み込み
// Author : 唐�ｱ結斗
// 概要 : Xファイルの読み込みを行う
//=============================================================================
void CModel3D::LoadModelAll(std::function<void()> func)
{
	nl::json m_Loadjson;

	CFile*Json = new CFile;

	m_Loadjson = Json->LoadJson(L"data/FILE/Model.json");

	m_nMaxModel = m_Loadjson["MODEL"].size();

	// デバイスの取得
	for (int nCnt = 0; nCnt < m_nMaxModel; nCnt++)
	{	
		int ID = m_Loadjson["MODEL"][nCnt][0];
		std::string PASS = m_Loadjson["MODEL"][nCnt][1];
		std::string KEY = m_Loadjson["MODEL"][nCnt][2];

		m_KeyMap.push_back(KEY);

		CRenderer *pRenderer = CApplication::GetInstance()->GetInstance()->GetRenderer();

		LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

		CTexture *pTexture = CApplication::GetInstance()->GetInstance()->GetTexture();

		// テクスチャ情報の取得
		std::map<std::string, CTexture::TEXTURE, std::less<>> pTextureData = pTexture->GetTextureData();
		CModel3D::MODEL_MATERIAL material;
		// テクスチャの使用数のゲット
		int nMaxTex = pTexture->GetMaxTexture();

		material.aFileName = PASS;
		material.hit = m_Loadjson["MODEL"][nCnt][3];
		D3DXLoadMeshFromX(material.aFileName.c_str(),
			D3DXMESH_SYSTEMMEM,
			pDevice,
			NULL,
			&material.pBuffer,
			NULL,
			&material.nNumMat,
			&material.pMesh);

		// マテリアルのテクスチャ情報のメモリ確保
		material.pNumTex.resize(material.nNumMat);
		//assert(material.pNumTex != nullptr);

		// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
		D3DXMATERIAL *pMat = (D3DXMATERIAL*)material.pBuffer->GetBufferPointer();
		
		// 各メッシュのマテリアル情報を取得する
		for (int nCntMat = 0; nCntMat < (int)material.nNumMat; nCntMat++)
		{
		
			// マテリアルのテクスチャ情報の初期化
			material.pNumTex[nCntMat] = -1;

			if (pMat[nCntMat].pTextureFilename != NULL)
			{
				bool isTex = false;
				for (const auto &pair : pTextureData)
				{
					std::string pass = pMat[nCntMat].pTextureFilename;
					if (pTextureData[pair.first].aFileName == pass)
					{
						material.pNumTex[nCntMat] = pTextureData[pair.first].nindex;
						isTex = true;
						break;
					}
				}
				if (!isTex)
				{
					pTexture;
				}
			}
		}

		// 頂点座標の最小値・最大値の算出
		int		nNumVtx;	// 頂点数
		DWORD	sizeFVF;	// 頂点フォーマットのサイズ
		BYTE	*pVtxBuff;	// 頂点バッファへのポインタ

		// 頂点数の取得
		nNumVtx = material.pMesh->GetNumVertices();

		// 頂点フォーマットのサイズの取得
		sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

		// 頂点バッファのロック
		material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

		// 最も大きな頂点
		D3DXVECTOR3 vtxMax = D3DXVECTOR3(-100000.0f, -100000.0f, -100000.0f);
		D3DXVECTOR3 vtxMin = D3DXVECTOR3(100000.0f, 100000.0f, 100000.0f);

		for (int nCntVtx = 0; nCntVtx < nNumVtx; nCntVtx++)
		{
			// 頂点座標の代入
			D3DXVECTOR3 vtx = *(D3DXVECTOR3*)pVtxBuff;

			if (vtx.x < vtxMin.x)
			{// 比較対象が現在の頂点座標(X)の最小値より小さい
				vtxMin.x = vtx.x;
			}
			if (vtx.y < vtxMin.y)
			{// 比較対象が現在の頂点座標(Y)の最小値より小さい
				vtxMin.y = vtx.y;
			}
			if (vtx.z < vtxMin.z)
			{// 比較対象が現在の頂点座標(Z)の最小値より小さい
				vtxMin.z = vtx.z;
			}

			if (vtx.x > vtxMax.x)
			{// 比較対象が現在の頂点座標(X)の最大値より大きい
				vtxMax.x = vtx.x;
			}
			if (vtx.y > vtxMax.y)
			{// 比較対象が現在の頂点座標(Y)の最大値より大きい
				vtxMax.y = vtx.y;
			}
			if (vtx.z > vtxMax.z)
			{// 比較対象が現在の頂点座標(Z)の最大値より大きい
				vtxMax.z = vtx.z;
			}

			// 頂点フォーマットのサイズ分ポインタを進める
			pVtxBuff += sizeFVF;
		}

		// 頂点バッファのアンロック
		material.pMesh->UnlockVertexBuffer();

		// 大きさの設定
		material.size = vtxMax - vtxMin;

		material.nindex = nCnt;

		//値をマップに保存
		m_ModelMap[KEY] = material;
	}
	delete Json;
	func();
}
void CModel3D::LoadModelAllNo()
{
	nl::json m_Loadjson;

	CFile*Json = new CFile;

	m_Loadjson = Json->LoadJson(L"data/FILE/Model.json");

	m_nMaxModel = m_Loadjson["MODEL"].size();

	// デバイスの取得
	for (int nCnt = 0; nCnt < m_nMaxModel; nCnt++)
	{
		int ID = m_Loadjson["MODEL"][nCnt][0];
		std::string PASS = m_Loadjson["MODEL"][nCnt][1];
		std::string KEY = m_Loadjson["MODEL"][nCnt][2];

		m_KeyMap.push_back(KEY);

		CRenderer *pRenderer = CApplication::GetInstance()->GetInstance()->GetRenderer();

		LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

		CTexture *pTexture = CApplication::GetInstance()->GetInstance()->GetTexture();

		// テクスチャ情報の取得
		std::map<std::string, CTexture::TEXTURE, std::less<>> pTextureData = pTexture->GetTextureData();
		CModel3D::MODEL_MATERIAL material;
		// テクスチャの使用数のゲット
		int nMaxTex = pTexture->GetMaxTexture();

		material.aFileName = PASS;
		D3DXLoadMeshFromX(material.aFileName.c_str(),
			D3DXMESH_SYSTEMMEM,
			pDevice,
			NULL,
			&material.pBuffer,
			NULL,
			&material.nNumMat,
			&material.pMesh);

		// マテリアルのテクスチャ情報のメモリ確保
		material.pNumTex.resize(material.nNumMat);
		//assert(material.pNumTex != nullptr);

		// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
		D3DXMATERIAL *pMat = (D3DXMATERIAL*)material.pBuffer->GetBufferPointer();

		// 各メッシュのマテリアル情報を取得する
		for (int nCntMat = 0; nCntMat < (int)material.nNumMat; nCntMat++)
		{

			// マテリアルのテクスチャ情報の初期化
			material.pNumTex[nCntMat] = -1;

			if (pMat[nCntMat].pTextureFilename != NULL)
			{
				bool isTex = false;
				for (const auto &pair : pTextureData)
				{
					std::string pass = pMat[nCntMat].pTextureFilename;
					if (pTextureData[pair.first].aFileName == pass)
					{
						material.pNumTex[nCntMat] = pTextureData[pair.first].nindex;
						isTex = true;
						break;
					}
				}
				if (!isTex)
				{
					pTexture;
				}
			}
		}

		// 頂点座標の最小値・最大値の算出
		int		nNumVtx;	// 頂点数
		DWORD	sizeFVF;	// 頂点フォーマットのサイズ
		BYTE	*pVtxBuff;	// 頂点バッファへのポインタ

							// 頂点数の取得
		nNumVtx = material.pMesh->GetNumVertices();

		// 頂点フォーマットのサイズの取得
		sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

		// 頂点バッファのロック
		material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

		// 最も大きな頂点
		D3DXVECTOR3 vtxMax = D3DXVECTOR3(-100000.0f, -100000.0f, -100000.0f);
		D3DXVECTOR3 vtxMin = D3DXVECTOR3(100000.0f, 100000.0f, 100000.0f);

		for (int nCntVtx = 0; nCntVtx < nNumVtx; nCntVtx++)
		{
			// 頂点座標の代入
			D3DXVECTOR3 vtx = *(D3DXVECTOR3*)pVtxBuff;

			if (vtx.x < vtxMin.x)
			{// 比較対象が現在の頂点座標(X)の最小値より小さい
				vtxMin.x = vtx.x;
			}
			if (vtx.y < vtxMin.y)
			{// 比較対象が現在の頂点座標(Y)の最小値より小さい
				vtxMin.y = vtx.y;
			}
			if (vtx.z < vtxMin.z)
			{// 比較対象が現在の頂点座標(Z)の最小値より小さい
				vtxMin.z = vtx.z;
			}

			if (vtx.x > vtxMax.x)
			{// 比較対象が現在の頂点座標(X)の最大値より大きい
				vtxMax.x = vtx.x;
			}
			if (vtx.y > vtxMax.y)
			{// 比較対象が現在の頂点座標(Y)の最大値より大きい
				vtxMax.y = vtx.y;
			}
			if (vtx.z > vtxMax.z)
			{// 比較対象が現在の頂点座標(Z)の最大値より大きい
				vtxMax.z = vtx.z;
			}

			// 頂点フォーマットのサイズ分ポインタを進める
			pVtxBuff += sizeFVF;
		}

		// 頂点バッファのアンロック
		material.pMesh->UnlockVertexBuffer();

		// 大きさの設定
		material.size = vtxMax - vtxMin;

		material.nindex = nCnt;
		//値をマップに保存
		m_ModelMap[KEY] = material;
	}
	SaveModelAll();
	delete Json;
}
//=============================================================================
// Xファイルのsave
// Author : 唐�ｱ結斗
// 概要 : Xファイルの読み込みを行う
//=============================================================================
void CModel3D::SaveModelAll()
{
	nl::json m_savejson;

	CFile*Json = new CFile;

	m_savejson["INDEX"] = m_nMaxModel;

	for (int i = 0; i < m_nMaxModel; i++)
	{
		std::string name = "MODEL";

		std::string KEY = m_KeyMap[i];
		std::string Pass;
		Pass = m_ModelMap[KEY].aFileName;
		int data = Pass.rfind("/");
		int hoge = Pass.find(".x");
		int size = Pass.size();
		data += 1;
		size -= data + 2;
		std::string key = Pass.substr(data, size);


		m_savejson[name.c_str()][i] = { i,Pass.c_str(),key.c_str(),false };
	}
	
	Json->SaveJson(m_savejson, "data/FILE/Modeltest.json");
	
	delete Json;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CModel3D::CModel3D()
{
	m_pShadowVolume.clear();									// シャドウボリューム
	m_pParent = nullptr;										// 親モデルの情報
	m_mtxWorld = {};											// ワールドマトリックス
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);						// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);						// 向き
	m_dir = D3DXVECTOR3(0.0f, 0.0f, 0.0f);						// 向き
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);						// 大きさ
	m_color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);				// カラー
	m_renderMode = Render_Default;
	m_nModelID = -1;											// モデルID
	m_nRenderTime = 0;
	m_bColor = false;											// カラーを使用する
	m_bShadow = true;											// 影の使用状況
	m_bLighting = true;											// ライトの使用状況
	m_isKeySet = false;
	m_bStencilShadow = false;
	m_bIgnoreDistance = false;

#ifdef _DEBUG
	m_bPolygonTest = false;
#endif // _DEBUG
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CModel3D::~CModel3D()
{
	// std::vectorのeraseメソッドを使用して要素を削除
	auto it = std::find(m_pModelGroup.begin(), m_pModelGroup.end(), this);

	if (it != m_pModelGroup.end())
	{
		m_pModelGroup.erase(it);
	}

	it = std::find(m_pModelGroup.begin(), m_pModelGroup.end(), this);
}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// Author : 田中
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CModel3D::Init()
{
	// メンバ変数の初期化
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);					// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);					// 向き
	m_size = D3DXVECTOR3(1.0f, 1.0f, 1.0f);					// 大きさ
	m_nModelID = -1;										// モデルID

	CShaderManager* pEffect = CApplication::GetInstance()->GetShaderManager();

	// ハンドルの初期化
	m_handler.Technique = pEffect->GetTechniqueCache(effectLabel, "Diffuse");			// エフェクト
	m_handler.Texture = pEffect->GetParameterCache(effectLabel, "Tex");			// テクスチャ
	m_handler.vLightDir = pEffect->GetParameterCache(effectLabel, "vLightDir");	// ライトの方向
	m_handler.vDiffuse = pEffect->GetParameterCache(effectLabel, "vDiffuse");	// 頂点カラー
	m_handler.vAmbient = pEffect->GetParameterCache(effectLabel, "vAmbient");	// 頂点カラー
	m_handler.WVP = pEffect->GetParameterCache(effectLabel, "wvp");
	m_handler.vCameraPos = pEffect->GetParameterCache(effectLabel, "vCameraPos");
	m_handler.time = pEffect->GetParameterCache(effectLabel, "renderTime");
	m_handler.isUseTexture = pEffect->GetParameterCache(effectLabel, "isUseTex");
	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CModel3D::Uninit()
{
	
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CModel3D::Update()
{
#ifdef _DEBUG
	if (m_nModelID != -1
		&& m_bPolygonTest)
	{
		SetPolygonTest();
	}
#endif // _DEBUG
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 親モデルが指定されてる場合、親のワールドマトリックス元に描画する
//=============================================================================
void CModel3D::Draw()
{
	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (m_nModelID >= 0
		&& m_nModelID < m_nMaxModel)
	{
		// レンダラーの取得
		CRenderer *pRenderer = CApplication::GetInstance()->GetInstance()->GetRenderer();

		// デバイスの取得
		LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

		// 親のワールドマトリックス
		D3DXMATRIX mtxParent = {};

		// 計算用マトリックス
		D3DXMATRIX mtxRot, mtxTrans, mtxScaling;

		// ワールドマトリックスの初期化
		D3DXMatrixIdentity(&m_mtxWorld);											// 行列初期化関数

		// 向きの反映
		D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);			// 行列回転関数
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);						// 行列掛け算関数 

		// 位置を反映
		D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);				// 行列移動関数
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);					// 行列掛け算関数

		if (m_pParent != nullptr)
		{
			mtxParent = m_pParent->GetMtxWorld();

			// 行列掛け算関数
			D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxParent);
		}

		if (m_bShadow && (pCamera->Distance(utility::GetWorldPosMatrix(&m_mtxWorld)) <= eraseShadowDist))
		{// 影の生成
			if (m_bStencilShadow)
			{
				StencilShadow();
			}
			else
			{
				Shadow();
			}
		}
		
		// ワールドマトリックスの設定
		pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

		//// ステンシルバッファの設定
		//pRenderer->SetStencil(1, D3DCMP_EQUAL);

		//// ステンシルテストの結果を反映
		//pRenderer->SetStencilReflection(D3DSTENCILOP_KEEP, D3DSTENCILOP_KEEP, D3DSTENCILOP_KEEP);

		// マテリアル描画
		DrawMaterial();

		// ステンシルバッファの終了設定
	pRenderer->ClearStencil();
	}
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 描画を行う
//=============================================================================
void CModel3D::Draw(D3DXMATRIX mtxParent)
{
	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (m_nModelID >= 0
		&& m_nModelID < m_nMaxModel)
	{
		// 計算用マトリックス
		D3DXMATRIX mtxRot, mtxTrans, mtxScaling;

		// ワールドマトリックスの初期化
		D3DXMatrixIdentity(&m_mtxWorld);											// 行列初期化関数

		// サイズの反映
		D3DXMatrixScaling(&mtxScaling, m_size.x, m_size.y, m_size.z);
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScaling);					// 行列掛け算関数

		// 向きの反映
		D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);			// 行列回転関数
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);						// 行列掛け算関数 

		// 位置を反映
		D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);				// 行列移動関数
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);					// 行列掛け算関数

		// 行列掛け算関数
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxParent);

		if (m_bShadow && (pCamera->Distance(utility::GetWorldPosMatrix(&m_mtxWorld)) <= eraseShadowDist))
		{// 影の生成
			if (m_bStencilShadow)
			{
				StencilShadow();
			}
			else
			{
				Shadow();
			}
		}

		// マテリアル描画
		DrawMaterial();
	}
}

//=============================================================================
// マテリアル描画
// Author : 唐�ｱ結斗
// 概要 : マテリアル描画
//=============================================================================
void CModel3D::DrawMaterial()
{
	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (!m_bIgnoreDistance)
	{
		if (pCamera->Distance2D(utility::GetWorldPosMatrix(&m_mtxWorld)) > eraseModelDist/* ||
			pCamera->Behind(utility::GetWorldPosMatrix(&m_mtxWorld), -100.0f)*/)
		{
			// std::vectorのeraseメソッドを使用して要素を削除
			auto it = std::find(m_pModelGroup.begin(), m_pModelGroup.end(), this);

			if (it != m_pModelGroup.end())
			{
				m_pModelGroup.erase(it);
			}

			it = std::find(m_pModelGroup.begin(), m_pModelGroup.end(), this);

			return;
		}
	}

	// std::vectorのeraseメソッドを使用して要素を削除
	auto it = std::find(m_pModelGroup.begin(), m_pModelGroup.end(), this);

	if (it == m_pModelGroup.end()
		&& GetMyMaterial().hit)
	{
		m_pModelGroup.push_back(this);
	}


	CShaderManager* pShaderManager = CApplication::GetInstance()->GetShaderManager();
	LPD3DXEFFECT pEffect = pShaderManager->GetEffect(effectLabel);
	if (pEffect == nullptr)
	{
		assert(false);
		return;
	}

	/* g_pEffectに値が入ってる */

	D3DXMATRIX viewMatrix = pCamera->GetMtxView();
	D3DXMATRIX projMatrix = pCamera->GetMtxProj();

	//-------------------------------------------------
	// シェーダの設定
	//-------------------------------------------------
	pEffect->SetTechnique(m_handler.Technique);
	pEffect->Begin(NULL, 0);

	CShaderManager::WVP wvp = { m_mtxWorld, viewMatrix, projMatrix };
	pEffect->SetValue(m_handler.WVP, &wvp, sizeof(CShaderManager::WVP));
	pEffect->SetVector(m_handler.vCameraPos, &static_cast<D3DXVECTOR4>(pCamera->GetPosV()));

	// ライト情報
	CLight* lightClass = CApplication::GetInstance()->GetLight();

	// ライトの方向
	D3DXVECTOR3 vecLight = lightClass->GetLightVec(); // ライトの取得
	D3DXVECTOR4 lightDir = D3DXVECTOR4(vecLight.x, vecLight.y, vecLight.z, 0.0f);
	pEffect->SetVector(m_handler.vLightDir, &lightDir);

	pEffect->SetInt(m_handler.time, m_nRenderTime);

	switch (m_renderMode)
	{
	case ERenderMode::Render_Default:
		RenderDefault(pEffect, 0);
		break;
	case ERenderMode::Render_Highlight:
		RenderHighLight(pEffect);
		break;
	case ERenderMode::Render_Hologram:
		RenderDefault(pEffect, 2);
		break;
	case ERenderMode::Render_Glass:
		RenderDefault(pEffect, 3);
		break;
	default:
		assert(false);
		break;
	}

	pEffect->End();

	m_nRenderTime++;
}

void CModel3D::RenderDefault(LPD3DXEFFECT& effect, UINT pass)
{
	if (m_model->pBuffer == nullptr)
	{
		return;
	}
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();
	CTexture *pTexture = CApplication::GetInstance()->GetTexture();

	// マテリアルデータのポインタを取得する
	D3DXMATERIAL* pMat = (D3DXMATERIAL*)m_model->pBuffer->GetBufferPointer();

	for (int nCntMat = 0; nCntMat < (int)m_model->nNumMat; nCntMat++)
	{
		// モデルの色の設定 
		effect->SetValue(m_handler.vDiffuse, &pMat[nCntMat].MatD3D.Diffuse, sizeof(D3DCOLORVALUE));
		effect->SetValue(m_handler.vAmbient, &pMat[nCntMat].MatD3D.Ambient, sizeof(D3DCOLORVALUE));

		pDevice->SetTexture(0, pTexture->GetTexture(m_model->pNumTex[nCntMat]));
		effect->SetBool(m_handler.isUseTexture, (pTexture->GetTexture(m_model->pNumTex[nCntMat]) != nullptr));

		effect->BeginPass(pass);
		m_model->pMesh->DrawSubset(nCntMat);	//モデルパーツの描画
		effect->EndPass();
	}
}

void CModel3D::RenderHighLight(LPD3DXEFFECT& effect)
{
	/* 強調表示する場合はオブジェクトをリストの末尾に移動 */

	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

	// ステンシルテストの設定
	pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);
	pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);
	pDevice->SetRenderState(D3DRS_STENCILWRITEMASK, 0xff);
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);

	pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);
	pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
	pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_REPLACE);

	RenderDefault(effect);

	pDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_GREATER);

	for (int nCntMat = 0; nCntMat < (int)m_model->nNumMat; nCntMat++)
	{
		effect->BeginPass(1);
		m_model->pMesh->DrawSubset(nCntMat);	//モデルパーツの描画
		effect->EndPass();
	}

	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
}

#ifdef _DEBUG
void CModel3D::SetPolygonTest()
{
	for (int nCntShadow = 0; nCntShadow < (int)m_pShadowVolume.size(); nCntShadow++)
	{
		if (m_pShadowVolume[nCntShadow] != nullptr)
		{
			m_pShadowVolume[nCntShadow]->Uninit();
			m_pShadowVolume[nCntShadow] = nullptr;
		}
	}

	m_pShadowVolume.clear();

	// マテリアル情報
	MODEL_MATERIAL material = GetMyMaterial();
	IDirect3DIndexBuffer9* indexBuffer = NULL;

	material.pMesh->GetIndexBuffer(&indexBuffer);

	// インデックスバッファをロック
	WORD *pIdx;
	assert(indexBuffer != nullptr);
	indexBuffer->Lock(0, 0, (void**)&pIdx, 0);

	if (m_nModelID == -1)
	{
		return;
	}

	int		nNumVtx;	// 頂点数
	int		nNumPol;	// ポリゴン数
	DWORD	sizeFVF;	// 頂点フォーマットのサイズ
	BYTE	*pVtxBuff;	// 頂点バッファへのポインタ

						// 頂点数の取得
	nNumVtx = material.pMesh->GetNumVertices();

	// ポリゴン数の取得
	nNumPol = material.pMesh->GetNumFaces();

	// 頂点フォーマットのサイズの取得
	sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

	// 頂点バッファのロック
	material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

	// 計算用変数
	D3DXVECTOR3 aVtx[3] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };
	D3DXVECTOR3 aLine[2] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };

	for (int nCntPol = 0; nCntPol < nNumPol; nCntPol++)
	{
		for (int nCntVtx = 0; nCntVtx < 3; nCntVtx++)
		{
			aVtx[nCntVtx] = *(D3DXVECTOR3*)(pVtxBuff + (sizeFVF * pIdx[nCntPol * 3 + nCntVtx]));
			D3DXVec3TransformCoord(&aVtx[nCntVtx], &aVtx[nCntVtx], &m_mtxWorld);
		}

		if (aVtx[0] == aVtx[1]
			|| aVtx[0] == aVtx[2]
			|| aVtx[1] == aVtx[2])
		{// 縮退ポリゴンを飛ばす
			continue;
		}

		CShadowVolume *pPolygon3D = CShadowVolume::Create();
		std::vector<D3DXVECTOR3> vtx;
		vtx.clear();
		vtx.resize(4);

		vtx[0] = aVtx[0];
		vtx[1] = aVtx[1];
		vtx[2] = aVtx[2];
		vtx[3] = aVtx[0];

		pPolygon3D->SetVtxPolygon(vtx);
		pPolygon3D->SetColor(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
		m_pShadowVolume.push_back(pPolygon3D);
	}

	// 頂点バッファのアンロック
	material.pMesh->UnlockVertexBuffer();

	// インデックスバッファのアンロック
	indexBuffer->Unlock();
}
#endif // _DEBUG

//=============================================================================
// 位置のセッター
// Author : 唐�ｱ結斗
// 概要 : 位置のメンバ変数に引数を代入
//=============================================================================
void CModel3D::SetPos(const D3DXVECTOR3 &pos)
{
	// 位置の設定
	m_pos = pos;
}

//=============================================================================
// 向きのセッター
// Author : 唐�ｱ結斗
// 概要 : 向きのメンバ変数に引数を代入
//=============================================================================
void CModel3D::SetRot(const D3DXVECTOR3 &rot)
{
	// 位置の設定
	m_rot = rot;
}

//=============================================================================
// 大きさのセッター
// Author : 唐�ｱ結斗
// 概要 : 大きさのメンバ変数に引数を代入
//=============================================================================
void CModel3D::SetSize(const D3DXVECTOR3 & size)
{
	// 大きさの設定
	m_size = size;
}

//=============================================================================
// setID
// Author : 唐�ｱ結斗
// 概要 : 大きさのメンバ変数に引数を代入
//=============================================================================
void CModel3D::SetModelID(const int nModelID)
{
	m_nModelID = nModelID;
	if (m_nModelID != -1)
	{
		m_key = m_KeyMap[m_nModelID];
		m_model = &m_ModelMap[m_key];
		m_isKeySet = true;
	}
}

//=============================================================================
// setID
// Author : 唐�ｱ結斗
// 概要 : 大きさのメンバ変数に引数を代入
//=============================================================================
void CModel3D::SetModelKEY(const std::string KEY)
{
	m_key = KEY;
	m_isKeySet = true;
	m_nModelID = 0;
	m_model = &m_ModelMap[m_key];
}

//=============================================================================
// カラーの設定
// Author : 唐�ｱ結斗
// 概要 : カラーを設定し、カラー使用を
//=============================================================================
void CModel3D::SetColor(const D3DXCOLOR color)
{
	// 色の設定
	m_color = color;
	m_bColor = true;
}

//=============================================================================
// KEYの設定
// Author : 唐�ｱ結斗
// 概要 : カラーを設定し、カラー使用を
//=============================================================================
CModel3D::MODEL_MATERIAL CModel3D::GetMyMaterial()
{
	if (!m_isKeySet)
	{
		m_key = m_KeyMap[m_nModelID];
		m_model = &m_ModelMap[m_key];
		m_isKeySet = true;
	}
	
	return *m_model;
	
}

//=============================================================================
// 当たり判定
// Author : 唐�ｱ結斗
// 概要 : モデルとオブジェクトの当たり判定
//=============================================================================
bool CModel3D::Collison(CObject *pTarget, const D3DXVECTOR3 relativePos)
{
	// 当たり判定
	bool bCollison = false;

	if (!pTarget->GetFlagDeath())
	{
		// マテリアル情報
		MODEL_MATERIAL material = GetMyMaterial();
		IDirect3DIndexBuffer9* indexBuffer = NULL;

		material.pMesh->GetIndexBuffer(&indexBuffer);

		// インデックスバッファをロック
		WORD *pIdx;
		assert(indexBuffer != nullptr);
		indexBuffer->Lock(0, 0, (void**)&pIdx, 0);

		// ポリゴン数の取得
		int		nNumPol;	// ポリゴン数
		nNumPol = material.pMesh->GetNumFaces();

		// ターゲット情報の宣言
		D3DXVECTOR3 posTarget = pTarget->GetPos() + relativePos;
		D3DXVECTOR3 posOldTarget = pTarget->GetPosOld() + relativePos;
		D3DXVECTOR3 Size{ D3DXVECTOR3(500.0f,500.0f,500.0f) };

		//逆行列を計算し、現在の位置と過去の位置を相対座標に変換する
		D3DXMATRIX mtxInv;
		D3DXMatrixInverse(&mtxInv, nullptr, &m_mtxWorld);
		D3DXVec3TransformCoord(&posTarget, &posTarget, &mtxInv);
		D3DXVec3TransformCoord(&posOldTarget, &posOldTarget, &mtxInv);

		// 計算用変数
		D3DXVECTOR3 aVtx[3] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };

		// 頂点フォーマットのサイズの取得
		DWORD sizeFVF;
		sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

		// 頂点バッファのロック
		BYTE *pVtxBuff;
		material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

		for (int nCntPolygon = 0; nCntPolygon < nNumPol; nCntPolygon++)
		{//モデルの全部の三角形を確認する

		 //面積の法線ベクトルを宣言する
			D3DXVECTOR3 nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			for (int nCntVtx = 0; nCntVtx < 3; nCntVtx++)
			{//三角形の頂点の位置と法線を取得する

				D3DXVECTOR3 n = *(D3DXVECTOR3*)(pVtxBuff + sizeof(D3DXVECTOR3) + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
				aVtx[nCntVtx] = *(D3DXVECTOR3*)(pVtxBuff + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
				nor += *(D3DXVECTOR3*)(pVtxBuff + sizeof(D3DXVECTOR3) + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
			}

			D3DXVECTOR3 vec[3] = { D3DXVECTOR3(0.f,0.f,0.f) };

			vec[0] = aVtx[0];
			vec[1] = aVtx[1];
			vec[2] = aVtx[2];

			{//法線を正規化し、確認する

				D3DXVec3Normalize(&nor, &nor);
				D3DXVECTOR3 n = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				D3DXVec3Cross(&n, &(vec[1] - vec[0]), &(vec[2] - vec[1]));
				D3DXVec3Normalize(&n, &n);

				if (D3DXVec3Length(&(nor - n)) > 0.01f)
				{
					nor = n;
				}
			}

			//面積のdパラメーターを計算する
			float fd = -(nor.x * vec[0].x + nor.y * vec[0].y + nor.z * vec[0].z);

			// 三点を平面に変換する
			D3DXPLANE plane = D3DXPLANE(nor.x, nor.y, nor.z, fd);

			{//過去の位置は面積の法線の向きのほうにあり、現在の位置が面積の後ろになかったら、判定を中断し、次の三角形へ進む
				float fDot = D3DXVec3Dot(&nor, &(posTarget - vec[0])), fDotOld = D3DXVec3Dot(&nor, &(posOldTarget - vec[0]));

				if (fDot >= 0.0f || fDotOld < 0.0f)
				{
					continue;
				}
			}

			D3DXVECTOR3 crossPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			if (!D3DXPlaneIntersectLine(&crossPos, &plane, &posOldTarget, &posTarget))
			{//移動と面積が平行だったら、判定を中断し、次の三角形へ進む
				continue;
			}

			if (!hmd::IsPointInsideTriangle(crossPos, vec[0], vec[1], vec[2]))
			{//移動のベクトルと面積の交点が三角形外だったら、判定を中断し、次の三角形へ進む
				continue;
			}

			// 判定に必要な変数
			D3DXVECTOR3 normal = D3DXVECTOR3(plane.a, plane.b, plane.c), endPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), d = posTarget + normal;
			D3DXVec3Normalize(&normal, &normal);

			//面積の法線のほうに押し出す(交点より少し法線のほうにずらす)
			D3DXPlaneIntersectLine(&endPos, &plane, &posTarget, &d);
			endPos += normal * 0.01f;
			posTarget = endPos;

			//位置を絶対座標に変換する
			D3DXVec3TransformCoord(&endPos, &endPos, &m_mtxWorld);

			//オブジェクトの位置を設定し、当たったということにする
			pTarget->SetPos(endPos - relativePos);
			bCollison = true;
		}

		// インデックスバッファのアンロック
		indexBuffer->Unlock();

		// 頂点バッファのアンロック
		material.pMesh->UnlockVertexBuffer();
	}

	return bCollison;
}

bool CModel3D::Collison(CObject * pTarget, const D3DXVECTOR3 relativePos, bool& bLanding)
{
	// 当たり判定
	bool bCollison = false;

	if (!pTarget->GetFlagDeath())
	{
		// マテリアル情報
		MODEL_MATERIAL material = GetMyMaterial();
		IDirect3DIndexBuffer9* indexBuffer = NULL;

		material.pMesh->GetIndexBuffer(&indexBuffer);

		// インデックスバッファをロック
		WORD *pIdx;
		assert(indexBuffer != nullptr);
		indexBuffer->Lock(0, 0, (void**)&pIdx, 0);

		// ポリゴン数の取得
		int		nNumPol;	// ポリゴン数
		nNumPol = material.pMesh->GetNumFaces();

		// ターゲット情報の宣言
		D3DXVECTOR3 posTarget = pTarget->GetPos() + relativePos;
		D3DXVECTOR3 posOldTarget = pTarget->GetPosOld() + relativePos;
		D3DXVECTOR3 Size{ D3DXVECTOR3(500.0f,500.0f,500.0f) };

		//逆行列を計算し、現在の位置と過去の位置を相対座標に変換する
		D3DXMATRIX mtxInv;
		D3DXMatrixInverse(&mtxInv, nullptr, &m_mtxWorld);
		D3DXVec3TransformCoord(&posTarget, &posTarget, &mtxInv);
		D3DXVec3TransformCoord(&posOldTarget, &posOldTarget, &mtxInv);

		// 計算用変数
		D3DXVECTOR3 aVtx[3] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };

		// 頂点フォーマットのサイズの取得
		DWORD sizeFVF;
		sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

		// 頂点バッファのロック
		BYTE *pVtxBuff;
		material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

		for (int nCntPolygon = 0; nCntPolygon < nNumPol; nCntPolygon++)
		{//モデルの全部の三角形を確認する

			//面積の法線ベクトルを宣言する
			D3DXVECTOR3 nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			for (int nCntVtx = 0; nCntVtx < 3; nCntVtx++)
			{//三角形の頂点の位置と法線を取得する

				D3DXVECTOR3 n = *(D3DXVECTOR3*)(pVtxBuff + sizeof(D3DXVECTOR3) + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
				aVtx[nCntVtx] = *(D3DXVECTOR3*)(pVtxBuff + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
				nor += *(D3DXVECTOR3*)(pVtxBuff + sizeof(D3DXVECTOR3) + (sizeFVF * pIdx[nCntPolygon * 3 + nCntVtx]));
			}

			D3DXVECTOR3 vec[3] = { D3DXVECTOR3(0.f,0.f,0.f) };

			vec[0] = aVtx[0];
			vec[1] = aVtx[1];
			vec[2] = aVtx[2];

			{//法線を正規化し、確認する

				D3DXVec3Normalize(&nor, &nor);
				D3DXVECTOR3 n = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				D3DXVec3Cross(&n, &(vec[1] - vec[0]), &(vec[2] - vec[1]));
				D3DXVec3Normalize(&n, &n);

				if (D3DXVec3Length(&(nor - n)) > 0.01f)
				{
					nor = n;
				}
			}

			//面積のdパラメーターを計算する
			float fd = -(nor.x * vec[0].x + nor.y * vec[0].y + nor.z * vec[0].z);

			// 三点を平面に変換する
			D3DXPLANE plane = D3DXPLANE(nor.x, nor.y, nor.z, fd);

			{//過去の位置は面積の法線の向きのほうにあり、現在の位置が面積の後ろになかったら、判定を中断し、次の三角形へ進む
				float fDot = D3DXVec3Dot(&nor, &(posTarget - vec[0])), fDotOld = D3DXVec3Dot(&nor, &(posOldTarget - vec[0]));

				if (fDot >= 0.0f || fDotOld < 0.0f)
				{
					continue;
				}
			}

			D3DXVECTOR3 crossPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			if (!D3DXPlaneIntersectLine(&crossPos, &plane, &posOldTarget, &posTarget))
			{//移動と面積が平行だったら、判定を中断し、次の三角形へ進む
				continue;
			}

			if (!hmd::IsPointInsideTriangle(crossPos, vec[0], vec[1], vec[2]))
			{//移動のベクトルと面積の交点が三角形外だったら、判定を中断し、次の三角形へ進む
				continue;
			}

			// 判定に必要な変数
			D3DXVECTOR3 normal = D3DXVECTOR3(plane.a, plane.b, plane.c), endPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), d = posTarget + normal;
			D3DXVec3Normalize(&normal, &normal);

			//斜めの面積に乗ったら、着地したことにする
			if (normal.y > sinf(D3DX_PI * 0.2f))
				bLanding = true;

			//面積の法線のほうに押し出す(交点より少し法線のほうにずらす)
			D3DXPlaneIntersectLine(&endPos, &plane, &posTarget, &d);
			endPos += normal * 0.01f;
			posTarget = endPos;

			//位置を絶対座標に変換する
			D3DXVec3TransformCoord(&endPos, &endPos, &m_mtxWorld);

			//オブジェクトの位置を設定し、当たったということにする
			pTarget->SetPos(endPos - relativePos);
			bCollison = true;
		}

		// インデックスバッファのアンロック
		indexBuffer->Unlock();

		// 頂点バッファのアンロック
		material.pMesh->UnlockVertexBuffer();
	}

	return bCollison;
}

//=============================================================================
// マテリアルの取得
// Author : 唐�ｱ結斗
// 概要 : カラーを設定し、カラー使用を
//=============================================================================
CModel3D::MODEL_MATERIAL  CModel3D::LoadMaterial(std::string pass)
{
	CRenderer *pRenderer = CApplication::GetInstance()->GetInstance()->GetRenderer();

	LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

	CTexture *pTexture = CApplication::GetInstance()->GetInstance()->GetTexture();

	// テクスチャ情報の取得
	std::map<std::string, CTexture::TEXTURE, std::less<>> pTextureData = pTexture->GetTextureData();
	CModel3D::MODEL_MATERIAL material;
	// テクスチャの使用数のゲット
	int nMaxTex = pTexture->GetMaxTexture();

	material.aFileName = pass;
	D3DXLoadMeshFromX(material.aFileName.c_str(),
		D3DXMESH_SYSTEMMEM,
		pDevice,
		NULL,
		&material.pBuffer,
		NULL,
		&material.nNumMat,
		&material.pMesh);

	// マテリアルのテクスチャ情報のメモリ確保
	material.pNumTex.resize(material.nNumMat);

	// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
	D3DXMATERIAL *pMat = (D3DXMATERIAL*)material.pBuffer->GetBufferPointer();
	int Tex = 0;
	// 各メッシュのマテリアル情報を取得する
	for (int nCntMat = 0; nCntMat < (int)material.nNumMat; nCntMat++)
	{
		Tex = 0;
		// マテリアルのテクスチャ情報の初期化
		material.pNumTex[nCntMat] = -1;

		if (pMat[nCntMat].pTextureFilename != NULL)
		{
			bool isTex = false;
			for (const auto &pair : pTextureData)
			{
				std::string modelpass = pMat[nCntMat].pTextureFilename;
				if (pTextureData[pair.first].aFileName == modelpass)
				{
					material.pNumTex[nCntMat] = pTextureData[pair.first].nindex;
					isTex = true;
					break;
				}
			}
			if (!isTex)
			{
				pTexture->SetTex(material.aFileName);
			}
		}
	}

	// 頂点座標の最小値・最大値の算出
	int		nNumVtx;	// 頂点数
	DWORD	sizeFVF;	// 頂点フォーマットのサイズ
	BYTE	*pVtxBuff;	// 頂点バッファへのポインタ

	// 頂点数の取得
	nNumVtx = material.pMesh->GetNumVertices();

	// 頂点フォーマットのサイズの取得
	sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

	// 頂点バッファのロック
	material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

	// 最も大きな頂点
	D3DXVECTOR3 vtxMax = D3DXVECTOR3(-100000.0f, -100000.0f, -100000.0f);
	D3DXVECTOR3 vtxMin = D3DXVECTOR3(100000.0f, 100000.0f, 100000.0f);

	for (int nCntVtx = 0; nCntVtx < nNumVtx; nCntVtx++)
	{
		// 頂点座標の代入
		D3DXVECTOR3 vtx = *(D3DXVECTOR3*)pVtxBuff;

		if (vtx.x < vtxMin.x)
		{// 比較対象が現在の頂点座標(X)の最小値より小さい
			vtxMin.x = vtx.x;
		}
		if (vtx.y < vtxMin.y)
		{// 比較対象が現在の頂点座標(Y)の最小値より小さい
			vtxMin.y = vtx.y;
		}
		if (vtx.z < vtxMin.z)
		{// 比較対象が現在の頂点座標(Z)の最小値より小さい
			vtxMin.z = vtx.z;
		}

		if (vtx.x > vtxMax.x)
		{// 比較対象が現在の頂点座標(X)の最大値より大きい
			vtxMax.x = vtx.x;
		}
		if (vtx.y > vtxMax.y)
		{// 比較対象が現在の頂点座標(Y)の最大値より大きい
			vtxMax.y = vtx.y;
		}
		if (vtx.z > vtxMax.z)
		{// 比較対象が現在の頂点座標(Z)の最大値より大きい
			vtxMax.z = vtx.z;
		}

		// 頂点フォーマットのサイズ分ポインタを進める
		pVtxBuff += sizeFVF;
	}

	// 頂点バッファのアンロック
	material.pMesh->UnlockVertexBuffer();

	// 大きさの設定
	material.size = vtxMax - vtxMin;

	return material;
}

bool CModel3D::CollisonModel(CObject * pTarget, const D3DXVECTOR3 relativePos, bool& bLanding)
{
	bool bCollison = false;

	for (int nCnt = 0; nCnt < (int)m_pSearchModel.size(); nCnt++)
	{
		if (m_pSearchModel[nCnt]->Collison(pTarget, relativePos, bLanding))
		{
			bCollison = true;
		}
	}

	return bCollison;
}

bool CModel3D::SearchCollison(CObject *pTarget, const float fTargetRange, const D3DXVECTOR3 relativePos, bool& bLanding)
{
	bool bCollison = false;

	m_pSearchModel.clear();
	CBoundingSphere3D target = CBoundingSphere3D(pTarget->GetPos(), fTargetRange);

	for (int nCnt = 0; nCnt < (int)m_pModelGroup.size(); nCnt++)
	{
		CModel3D *pModel = m_pModelGroup[nCnt];

		if (pModel->GetModelID() == -1)
		{
			continue;
		}

		D3DXVECTOR3 pos = D3DXVECTOR3(0.f, 0.f, 0.f);
		D3DXVECTOR3 size = m_pModelGroup[nCnt]->GetMyMaterial().size;

		float fCollisionRange = 0.0f;
		std::vector<float> fSearchLongest;
		fSearchLongest.push_back(size.x);
		fSearchLongest.push_back(size.y);
		fSearchLongest.push_back(size.z);
		fCollisionRange = hmd::SearchLargest(fSearchLongest);
		D3DXVec3TransformCoord(&pos, &pos, &pModel->GetMtxWorld());

		CBoundingSphere3D model = CBoundingSphere3D(pos, fCollisionRange);

		if (hmd::IsSphereCollision(target, model).isUse)
		{
			m_pSearchModel.push_back(m_pModelGroup[nCnt]);
		}
	}

	bCollison = CModel3D::CollisonModel(pTarget, relativePos, bLanding);

	m_pSearchModel.clear();

	return bCollison;
}

//=============================================================================
// 影の生成
// Author : 唐�ｱ結斗
// 概要 : モデルの影を生成する
//=============================================================================
void CModel3D::Shadow()
{
	// 変数宣言
	D3DXMATRIX		mtxShadow = {};													// シャドウマトリックス
	D3DXPLANE		planeField = {};												// 平面化用変数
	D3DXVECTOR4		vecLight = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);					// ライト方向の逆ベクトル
	D3DXVECTOR3		posShadow = D3DXVECTOR3(0.0f, 1.0f, 0.0f);;						// 影の位置
	D3DXVECTOR3		norShadow = D3DXVECTOR3(0.0f, 1.0f, 0.0f);;						// 影の法線
	D3DXVECTOR3		lightVec = CApplication::GetInstance()->GetLight()->GetLightVec()  * -1;		// ライト方向

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;

	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;

	// シャドウマトリックスの初期化
	D3DXMatrixIdentity(&mtxShadow);	// 行列初期化関数

	// ライト方向の逆ベクトルの設定
	vecLight = D3DXVECTOR4(lightVec.x, lightVec.y, lightVec.z, 0.0f);

	// 平面化用変数の設定
	D3DXPlaneFromPointNormal(&planeField, &posShadow, &norShadow);

	// シャドウマトリックスの設定
	D3DXMatrixShadow(&mtxShadow, &vecLight, &planeField);

	// 行列掛け算関数
	D3DXMatrixMultiply(&mtxShadow, &m_mtxWorld, &mtxShadow);

	// シャドウマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &mtxShadow);

	// 現在のマテリアルを保持
	pDevice->GetMaterial(&matDef);
	
	if (m_model->pBuffer != nullptr)
	{// マテリアルデータへのポインタを取得
		pMat = (D3DXMATERIAL*)m_model->pBuffer->GetBufferPointer();

		for (int nCntMat = 0; nCntMat < (int)m_model->nNumMat; nCntMat++)
		{// マテリアル情報の設定
			D3DMATERIAL9  matD3D = pMat[nCntMat].MatD3D;

			// 引数を色に設定
			matD3D.Diffuse = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.5f);
			matD3D.Emissive = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.5f);

			// マテリアルの設定
			pDevice->SetMaterial(&matD3D);

			// テクスチャの設定
			pDevice->SetTexture(0, nullptr);

			// モデルパーツの描画
			m_model->pMesh->DrawSubset(nCntMat);
		}
	}

	// 保持していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);
}

void CModel3D::StencilShadow()
{
	for (int nCntShadow = 0; nCntShadow < (int)m_pShadowVolume.size(); nCntShadow++)
	{
		if (m_pShadowVolume[nCntShadow] != nullptr)
		{
			m_pShadowVolume[nCntShadow]->Uninit();
			m_pShadowVolume[nCntShadow] = nullptr;
		}
	}

	m_pShadowVolume.clear();

	// マテリアル情報
	MODEL_MATERIAL material = GetMyMaterial();	
	IDirect3DIndexBuffer9* indexBuffer = NULL;

	material.pMesh->GetIndexBuffer(&indexBuffer);

	// インデックスバッファをロック
	WORD *pIdx;
	assert(indexBuffer != nullptr);
	indexBuffer->Lock(0, 0, (void**)&pIdx, 0);

	if (m_nModelID == -1)
	{
		return;
	}

	// ライト方向
	D3DXVECTOR3 lightVec = CApplication::GetInstance()->GetLight()->GetLightVec()  * -1;		

	int		nNumVtx;	// 頂点数
	int		nNumPol;	// ポリゴン数
	DWORD	sizeFVF;	// 頂点フォーマットのサイズ
	BYTE	*pVtxBuff;	// 頂点バッファへのポインタ

	// 頂点数の取得
	nNumVtx = material.pMesh->GetNumVertices();

	// ポリゴン数の取得
	nNumPol = material.pMesh->GetNumFaces();

	// 頂点フォーマットのサイズの取得
	sizeFVF = D3DXGetFVFVertexSize(material.pMesh->GetFVF());

	// 頂点バッファのロック
	material.pMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

	// 計算用変数
	D3DXVECTOR3 aVtx[3] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };
	D3DXVECTOR3 aLine[2] = { D3DXVECTOR3(0.0f,0.0f,0.0f) };

	for (int nCntPol = 0; nCntPol < nNumPol; nCntPol++)
	{
		for (int nCntVtx = 0; nCntVtx < 3; nCntVtx++)
		{
			aVtx[nCntVtx] = *(D3DXVECTOR3*)(pVtxBuff + (sizeFVF * pIdx[nCntPol * 3 + nCntVtx]));
			D3DXVec3TransformCoord(&aVtx[nCntVtx], &aVtx[nCntVtx], &m_mtxWorld);
		}

		if (aVtx[0] == aVtx[1]
			|| aVtx[0] == aVtx[2]
			|| aVtx[1] == aVtx[2])
		{// 縮退ポリゴンを飛ばす
			continue;
		}

		// ポリゴンの辺ベクトル
		aLine[0] = aVtx[1] - aVtx[0];
		aLine[1] = aVtx[2] - aVtx[0];

		// 面法線ベクトル
		D3DXVECTOR3 norVec = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		// 面法線ベクトル
		D3DXVec3Cross(&norVec, &aLine[0], &aLine[1]);

		// 面法線ベクトルの正規化
		D3DXVec3Normalize(&norVec, &norVec);

		float fInnerProduct = D3DXVec3Dot(&norVec, &lightVec);

		if (fInnerProduct <= 0.f)
		{
			for (int nCnt = 0; nCnt < 2; nCnt++)
			{
				CShadowVolume *pPolygon3D = CShadowVolume::Create();
				std::vector<D3DXVECTOR3> vtx;
				vtx.clear();
				vtx.resize(4);

				if (nCntPol % 2 == 0)
				{
					vtx[0] = aVtx[0];
					vtx[1] = aVtx[1];
					vtx[2] = vtx[0] - (lightVec * 5000.0f);
					vtx[3] = vtx[1] - (lightVec * 5000.0f);

					if (nCnt != 0)
					{
						vtx[0] = aVtx[1];
						vtx[1] = aVtx[2];
						vtx[2] = vtx[1] - (lightVec * 5000.0f);
						vtx[3] = vtx[2] - (lightVec * 5000.0f);
					}
				}
				else
				{
					vtx[0] = aVtx[2];
					vtx[1] = aVtx[0];
					vtx[2] = vtx[0] - (lightVec * 5000.0f);
					vtx[3] = vtx[1] - (lightVec * 5000.0f);

					if (nCnt != 0)
					{
						vtx[0] = aVtx[0];
						vtx[1] = aVtx[1];
						vtx[2] = vtx[0] - (lightVec * 5000.0f);
						vtx[3] = vtx[1] - (lightVec * 5000.0f);
					}
				}

				pPolygon3D->SetVtxPolygon(vtx);
				pPolygon3D->SetColor(D3DXCOLOR(1.f, 0.f, 0.f, 1.f));
				m_pShadowVolume.push_back(pPolygon3D);
			}
		}
	}

	// 頂点バッファのアンロック
	material.pMesh->UnlockVertexBuffer();

	// インデックスバッファのアンロック
	indexBuffer->Unlock();
}

