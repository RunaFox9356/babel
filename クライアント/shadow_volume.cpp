//=============================================================================
//
// シャドウボリュームクラス(shadow_volume.cpp)
// Author : 唐�ｱ結斗
// 概要 : ポリゴン生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "shadow_volume.h"
#include "object.h"
#include "renderer.h"
#include "application.h"

//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
unsigned int CShadowVolume::nCntMyObject = 0;						// オブジェクトのカウント

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CShadowVolume * CShadowVolume::Create(void)
{
	// オブジェクトインスタンス
	CShadowVolume *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CShadowVolume;

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CShadowVolume::CShadowVolume()
{
	nCntMyObject++;
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CShadowVolume::~CShadowVolume()
{
	nCntMyObject--;
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CShadowVolume::Draw()
{// レンダラーのゲット
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	// テクスチャポインタの取得
	//CTexture *pTexture = CApplication::GetTexture();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

	D3DXMATRIX mtxWorld = GetMtxWorld();

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&mtxWorld);

	// ライトを無効
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// カリングを無効にする
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマット
	pDevice->SetFVF(FVF_MULTI_TEX_VTX_3D);

	//// ステンシルバッファの設定
	//pRenderer->SetStencil(1, D3DCMP_GREATER);

	//// ステンシルテストの結果を反映
	//pRenderer->SetStencilReflection(D3DSTENCILOP_INCRSAT, D3DSTENCILOP_DECRSAT, D3DSTENCILOP_INVERT);

	// 描画
	CPolygon::Draw();

	// ステンシルバッファの終了設定
	pRenderer->ClearStencil();

	// カリングを有効にする
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	// ライトを有効	
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// Zテストの終了
	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

	// αテストの終了
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CShadowVolume::SetVtx()
{
	//頂点情報へのポインタを生成
	VERTEX_3D *pVtx;

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	D3DXVECTOR3 vtx[4] = { D3DXVECTOR3(0.f,0.f,0.f) };

	if (m_vtx.size() >= 4)
	{
		for (int nCnt = 0; nCnt < 4; nCnt++)
		{
			vtx[nCnt] = m_vtx[nCnt];
		}
	}

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		pVtx[nCnt].pos = vtx[nCnt];
	}

	// 各頂点の法線の設定(*ベクトルの大きさは1にする必要がある)
	pVtx[0].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[1].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[2].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[3].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}
