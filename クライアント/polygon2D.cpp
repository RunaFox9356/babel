//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "polygon2D.h"
#include "object.h"
#include "renderer.h"
#include "application.h"
#include "shader_manager.h"

namespace
{
	std::string effectLabel = "2DEffect";
}

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CPolygon2D * CPolygon2D::Create(void)
{
	// オブジェクトインスタンス
	CPolygon2D *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CPolygon2D;

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CPolygon2D * CPolygon2D::Create(int nPriority)
{
	// オブジェクトインスタンス
	CPolygon2D *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CPolygon2D(nPriority);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CPolygon2D::CPolygon2D(int nPriority/* = PRIORITY_LEVEL0*/) : CPolygon(nPriority), m_useShader(false),
m_bDraw(false)
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CPolygon2D::~CPolygon2D()
{
	
}

//=============================================================================
// ポリゴンの初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CPolygon2D::Init()
{// レンダラーのゲット
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	//頂点バッファの生成
	pRenderer->GetDevice()->CreateVertexBuffer(sizeof(VERTEX_2D) * 4,		// 確保するバッファサイズ
		D3DUSAGE_WRITEONLY,
		FVF_MULTI_TEX_VTX_2D,												// 頂点ファーマット
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	// ポリゴンの初期化
	CPolygon::Init();

	m_bDraw = true;

	return S_OK;
}

//=============================================================================
// ポリゴンの終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CPolygon2D::Uninit()
{ 
	// 終了
	CPolygon::Uninit();

	//頂点バッファを破棄
	if (m_pVtxBuff != nullptr)
	{
		m_pVtxBuff->Release();

		m_pVtxBuff = nullptr;
	}

	// オブジェクト2Dの解放
	Release();
}

//=============================================================================
// ポリゴンの更新
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの更新を行う
//=============================================================================
void CPolygon2D::Update()
{
	
}

//=============================================================================
// ポリゴンの描画
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの描画を行う
//=============================================================================
void CPolygon2D::Draw()
{// レンダラーのゲット

	if (!m_bDraw)
		return;

	if (m_useShader)
	{
		DrawShader();
		return;
	}

	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();
	
	// テクスチャポインタの取得
	//CTexture *pTexture = CApplication::GetTexture();

	//頂点バッファをデータストリームに設定
	pRenderer->GetDevice()->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_2D));

	// 頂点フォーマットの設定
	pRenderer->GetDevice()->SetFVF(FVF_MULTI_TEX_VTX_2D);

	// αテストを使用する
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);

	// αテストの設定
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHAREF, 32);
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	// ポリゴンの描画
	CPolygon::Draw();

	// αテストの終了
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

//=============================================================================
// シェーダーによる描画
//=============================================================================
void CPolygon2D::DrawShader()
{
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	CShaderManager* pShaderManager = CApplication::GetInstance()->GetShaderManager();
	LPD3DXEFFECT pEffect = pShaderManager->GetEffect(effectLabel);
	if (pEffect == nullptr)
	{
		assert(false);
		return;
	}

	//頂点バッファをデータストリームに設定
	pRenderer->GetDevice()->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_2D));

	// 頂点フォーマットの設定
	pRenderer->GetDevice()->SetFVF(FVF_VERTEX_2D);

	// αテストを使用する
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);

	// αテストの設定
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHAREF, 0);
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	pEffect->SetTechnique(m_handler.Technique);
	pEffect->Begin(NULL, 0);

	for (size_t i = 0; i < GetTexData().size(); i++)
	{
		if (i == 0)
		{
			pEffect->SetBool(m_handler.useTexture, (GetTexData().at(0) != -1));
		}
		else if (i == 1)
		{
			pEffect->SetBool(m_handler.useSubTexture, (GetTexData().at(1) != -1));
		}
	}

	switch (m_renderMode)
	{
	case CPolygon2D::Render_Default:
		RenderDefault(pEffect, 0);
		break;
	case CPolygon2D::Render_Blur:
		RenderBlur(pEffect);
		break;
	case CPolygon2D::Render_Monochrome:
		RenderDefault(pEffect, 2);
		break;
	default:
		break;
	}

	pEffect->End();

	// αテストの終了
	pRenderer->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

void CPolygon2D::RenderDefault(LPD3DXEFFECT effect, const int pass)
{
	effect->BeginPass(pass);

	// ポリゴンの描画
	CPolygon::Draw();

	effect->EndPass();
}

void CPolygon2D::RenderBlur(LPD3DXEFFECT effect)
{
	// ぼやっとしたやつ上から貼った方がそれっぽい
	effect->BeginPass(1);

	// ポリゴンの描画
	CPolygon::Draw();

	effect->EndPass();

	effect->BeginPass(0);

	CPolygon::Draw();

	effect->EndPass();
}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CPolygon2D::SetVtx()
{
	//頂点情報へのポインタを生成
	VERTEX_2D *pVtx;

	// 情報の取得
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();
	D3DXVECTOR3 size = GetSize();

	//対角線の長さを算出する
	m_fLength = sqrtf((size.x * size.x) + (size.y * size.y)) / 2.0f;

	//対角線の角度を算出
	m_fAngle = atan2f(size.x, size.y);

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// 頂点情報を設定
	pVtx[0].pos.x = pos.x + sinf(rot.z + (D3DX_PI + m_fAngle)) * m_fLength;
	pVtx[0].pos.y = pos.y + cosf(rot.z + (D3DX_PI + m_fAngle)) * m_fLength;
	pVtx[0].pos.z = 0.0f;

	pVtx[1].pos.x = pos.x + sinf(rot.z + (D3DX_PI - m_fAngle)) *  m_fLength;
	pVtx[1].pos.y = pos.y + cosf(rot.z + (D3DX_PI - m_fAngle)) *  m_fLength;
	pVtx[1].pos.z = 0.0f;

	pVtx[2].pos.x = pos.x + sinf(rot.z - (0 + m_fAngle)) * m_fLength;
	pVtx[2].pos.y = pos.y + cosf(rot.z - (0 + m_fAngle)) * m_fLength;
	pVtx[2].pos.z = 0.0f;

	pVtx[3].pos.x = pos.x + sinf(rot.z - (0 - m_fAngle)) *  m_fLength;
	pVtx[3].pos.y = pos.y + cosf(rot.z - (0 - m_fAngle)) *  m_fLength;
	pVtx[3].pos.z = 0.0f;

	// rhwの設定
	pVtx[0].rhw = 1.0f;
	pVtx[1].rhw = 1.0f;
	pVtx[2].rhw = 1.0f;
	pVtx[3].rhw = 1.0f;

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// 色の設定
// Author : 唐�ｱ結斗
// 概要 : 頂点カラーを設定する
//=============================================================================
void CPolygon2D::SetColor(const D3DXCOLOR & color)
{
	CPolygon::SetColor(color);

	// 色の取得
	D3DXCOLOR col = GetColor();

	//頂点情報へのポインタを生成
	VERTEX_2D *pVtx;
	if (m_pVtxBuff != nullptr)
	{
		//頂点バッファをロックし、頂点情報へのポインタを取得
		m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

		// 頂点カラーの設定
		pVtx[0].col = col;
		pVtx[1].col = col;
		pVtx[2].col = col;
		pVtx[3].col = col;

		//頂点バッファをアンロック
		m_pVtxBuff->Unlock();
	}
}

//=============================================================================
// テクスチャ座標の設定
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトのテクスチャ座標を設定する
//=============================================================================
void CPolygon2D::SetTex(int nTex, const D3DXVECTOR2 &minTex, const D3DXVECTOR2 &maxTex)
{
	//頂点情報へのポインタを生成
	VERTEX_2D *pVtx;

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// テクスチャ座標の設定
	pVtx[0].tex[nTex] = D3DXVECTOR2(minTex.x, minTex.y);
	pVtx[1].tex[nTex] = D3DXVECTOR2(maxTex.x, minTex.y);
	pVtx[2].tex[nTex] = D3DXVECTOR2(minTex.x, maxTex.y);
	pVtx[3].tex[nTex] = D3DXVECTOR2(maxTex.x, maxTex.y);

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}


void CPolygon2D::SetShader(const bool is, const ERenderMode mode)
{
	m_useShader = is;
	m_renderMode = mode;
	if (!is || !m_handler.isEmpty)
	{
		return;
	}

	CShaderManager* pEffect = CApplication::GetInstance()->GetShaderManager();

	// ハンドルの初期化
	m_handler.Technique = pEffect->GetTechniqueCache(effectLabel, "TechShader");
	m_handler.useTexture = pEffect->GetParameterCache(effectLabel, "isUseTex");
	m_handler.useSubTexture = pEffect->GetParameterCache(effectLabel, "useSubTex");

	m_handler.isEmpty = false;
}