//=============================================================================
//
// PC.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _PC_GIMMICK_H
#define _PC_GIMMICK_H


//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CModelObj;
class CCollision_Rectangle3D;
class CUiMessage;
class CBomb;


class CGimmickPC : public CGimmick
{
public:
	CGimmickPC();									//コンストラクタ
	~CGimmickPC() override;							//デストラクタ

	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新
	void Draw() override;							// 描画

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &posOld) override;			// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size) override;				// 大きさのセッター

	D3DXVECTOR3 GetPos() override { return m_pos; }				// 位置のゲッター
	D3DXVECTOR3 GetPosOld() override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot() override { return m_rot; }				// 向きのゲッター
	D3DXVECTOR3 GetSize() override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void ChangeModel(int idx);									// モデルインデックスの設定処理
	void SetIsOnMap(const bool bOnMap) override;				//マップに表示されているかどうかの設定処理



	static CGimmickPC* Create(const D3DXVECTOR3 pos);								//生成
	static CGimmickPC* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot);		//生成

private:

	void CreateUiMessage();				//UIメッセージの生成処理
	void DestroyUiMessage();			//UIメッセージの破棄処理
	void DeleteData();					//このオブジェクトの保存したデータを消す

private:

	static const D3DXVECTOR3 DEFAULT_HITBOX_POS;			//ディフォルトのヒットボックスの相対位置
	static const D3DXVECTOR3 DEFAULT_SIZE;					//ディフォルトのサイズ
	static const D3DXVECTOR3 DEFAULT_ACTIVATION_HITBOX_POS;	//ディフォルトのヒットボックスの相対位置
	static const D3DXVECTOR3 DEFAULT_ACTIVATION_SIZE;		//ディフォルト当たり判定のサイズ


	D3DXVECTOR3 m_pos;				//位置
	D3DXVECTOR3 m_rot;				//回転角度

	CModelObj* m_pModel;			//モデル情報へのポインタ
	CCollision_Rectangle3D* m_pActivationArea;	//爆弾がおけるエリアへのポインタ
	CUiMessage*	m_pUiMessage;		//メッセージのUI
	CBomb*		m_pBomb;			//爆弾へのポインタ
};



#endif