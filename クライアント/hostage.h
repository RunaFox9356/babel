//=============================================================================
//
// hostage.h
// Author: 磯江 寿希亜
// Author: Ricci Alex
//
//=============================================================================

#ifndef _hostage_H_
#define _hostage_H_

#include "model_obj.h"
#include "gimmick.h"

class CMove;
class CCollision_Rectangle3D;
class CUiMessage;
class CMotionModel3D;
class CPlayer;
class CHostage : public CGimmick
{
public:

	//--------------------------------------------------------------------
	// アクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_STATE
	{
		// 通常
		NEUTRAL_ACTION = 0,		// ニュートラル
		MOVE_ACTION,			// 移動
		DASH_ACTION,			// 高速移動
		JAMP_ACTION,			// ジャンプ
		LANDING_ACTION,			// 着地
		SQUAT_ACTION,			// しゃがむ
		MAX_ACTION,				// 最大数
	};

public:
	static CHostage *Create(D3DXVECTOR3 pos = DEFAULT_POS, D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f));		// モデルオブジェクトの生成


	CHostage();						//コンストラクタ
	~CHostage()override;			//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override {}			// 描画

	void SetMove(D3DXVECTOR3 move) { m_move = move; }						//移動量のセッター
	void SetPos(const D3DXVECTOR3 &pos) override;							//位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}				//前回の位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;							//向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};					//サイズのセッター

	D3DXVECTOR3 GetPos() override;											//位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }				//前回の位置のゲッター
	D3DXVECTOR3 GetRot()  override;											//向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }				//サイズのゲッター

	void SetRenderMode(int mode) override;									// エフェクトのインデックスの設定
	void SetIsOnMap(const bool bOnMap) override;							//マップに表示されているかどうかの設定処理

	void SetRotDest(const D3DXVECTOR3 rotDest) { m_rotDest = rotDest; }		//目的の向きのセッター

private:

	void UpdateState();							//状態によっての更新処理

	void PrisonerUpdate();						//
	void Wait();								//待機
	void Escape();								//エスケープ

	void Rotate();								//回転処理
	void CheckPlayerDistance();					//プレイヤーの距離を計算し、プレイヤーに反応できるかどうかの判定
	void FacePlayer();							//プレイヤーのほうに向かう
	void FaceDirection(D3DXVECTOR3 point);		//ある方向に向かう
	void Follow();								//プレイヤーを追従する
	void OnlineTracking();						//プレイヤーを追従する(ハッカー側)
	bool HasEscaped();							//出口に着いたかどうかの判定
	void Move(const D3DXVECTOR3 point);			//ポイントの方に移動

	void MotionUpdate();						//モーションの更新処理
	void SetMotion(ACTION_STATE state);			//モーションアニメーションの設定

	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIメッセージの生成処理
	void DestroyUiMessage();											//UIメッセージの破棄処理
	void SetFreeState();						//解放状態の設定処理
	void DeleteData();							//データの破棄処理


private:

	//挙動の状態
	enum STATE
	{
		STATE_PRISONER = 0,		//人質
		STATE_WAITING,			//待機
		STATE_FOLLOW,			//追従
		STATE_ESCAPE,			//逃亡

		STATE_MAX
	};


	static const D3DXVECTOR3		DEFAULT_POS;					//ディフォルトのスポーンの位置
	static const D3DXVECTOR3		DEFAULT_COLLISION_POS;			//モデルの当たり判定の相対位置
	static const D3DXVECTOR3		DEFAULT_COLLISION_SIZE;			//モデルの当たり判定のサイズ
	static const float				DEFAULT_INTERACTION_DISTANCE;	//ディフォルトの反応できる距離
	static const float				DEFAULT_SPEED;					//ディフォルトの移動スピード
	static const char*				DEFAULT_MOTION_FILE_PATH;		//ディフォルトのモーションのファイルパス

	D3DXVECTOR3						m_rotDest;						//目的の向き
	D3DXVECTOR3						m_move;							//移動量
	D3DXVECTOR3						m_escapePos;					//逃亡の方向
	CMove*							m_pMove;						//移動用のクラスへのポインタ
	CMotionModel3D*					m_pModel;						//人質のモデル
	CCollision_Rectangle3D*			m_pModelHitbox;					//モデルのヒットボックスへのポインタ
	float							m_fDistance;					//プレイヤーまでの距離
	bool							m_bIsFollowing;					//プレイヤーを追従しているかどうかのフラグ
	CUiMessage*						m_pUiMessage;					//メッセージのUI
	ACTION_STATE					m_action;						//現在のモーションアニメーション
	STATE							m_state;						//現在の挙動
	CPlayer*						m_pearentu;
};


#endif
