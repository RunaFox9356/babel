//=============================================================================
//
// 3D��`�̏Փ˔���N���X(collision_rectangle3D.cpp)
// Author : �������l
// �T�v : 3D��`�̏Փ˔��萶�����s��
//
//=============================================================================

//*****************************************************************************
// �C���N���[�h
//*****************************************************************************
#include <assert.h>
#include <vector>

#include "collision_rectangle2D.h"
#include "application.h"
#include "renderer.h"
#include "object.h"
#include "line.h"
#include "boundingBox3D.h"
#include "utility.h"

//=============================================================================
// �C���X�^���X����
// Author : �������l
// �T�v : ���[�V�����L�����N�^�[3D�𐶐�����
//=============================================================================
CCollision_Rectangle2D * CCollision_Rectangle2D::Create()
{
	// �I�u�W�F�N�g�C���X�^���X
	CCollision_Rectangle2D *pCollisionRectangle3D = nullptr;

	// �������̉��
	pCollisionRectangle3D = new CCollision_Rectangle2D;

	// �������̊m�ۂ��ł��Ȃ�����
	assert(pCollisionRectangle3D != nullptr);

	// ���l�̏�����
	pCollisionRectangle3D->Init();

	// �C���X�^���X��Ԃ�
	return pCollisionRectangle3D;
}

//=============================================================================
// �R���X�g���N�^
// Author : �������l
// �T�v : �C���X�^���X�������ɍs������
//=============================================================================
CCollision_Rectangle2D::CCollision_Rectangle2D()
{
	m_state = STATE_NONE;					// ���������ꏊ
	m_bPlusMinus = true;					// �����̔���
}

//=============================================================================
// �f�X�g���N�^
// Author : �������l
// �T�v : �C���X�^���X�I�����ɍs������
//=============================================================================
CCollision_Rectangle2D::~CCollision_Rectangle2D()
{

}

//=============================================================================
// ������
// Author : �������l
// �T�v : ���_�o�b�t�@�𐶐����A�����o�ϐ��̏����l��ݒ�
//=============================================================================
HRESULT CCollision_Rectangle2D::Init()
{
	// �����蔻��̎擾
	SetType(TYPE_RECTANGLE2D);

	// �ʒu�̎擾
	SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// �T�C�Y�̎擾
	SetSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	// �Փ˂����I�u�W�F�N�g�̏�����
	GetCollidedObj().clear();

	return S_OK;
}

//=============================================================================
// �I��
// Author : �������l
// �T�v : �e�N�X�`���̃|�C���^�ƒ��_�o�b�t�@�̉��
//=============================================================================
void CCollision_Rectangle2D::Uninit()
{
	// �X�R�A�̉��
	Release();
}

//=============================================================================
// �X�V
// Author : �������l
// �T�v : �X�V���s��
//=============================================================================
void CCollision_Rectangle2D::Update()
{

}

//=============================================================================
// �`��
// Author : �������l
// �T�v : �`����s��
//=============================================================================
void CCollision_Rectangle2D::Draw()
{

}

//=============================================================================
// �����蔻��
// Author : �������l
// �T�v : �����蔻��
//=============================================================================
bool CCollision_Rectangle2D::Collision(CObject::EObjectType objType, bool bExtrude)
{
	// �Փ˂����I�u�W�F�N�g�̏�����
	std::vector<CObject*> pCollided = GetCollidedObj();
	pCollided.clear();

	// �T�C�Y�̎擾
	int nSize = pCollided.size();

	// �Փ˂����I�u�W�F�N�g�̃J�E���g
	int nCntCollided = 0;
	CBoundingBox3D myBoundingBox = GetBoundingBox();
	// �擪�C���X�^���X�̎擾
	CCollision *pCollision = CCollision::GetTop();
	bool bCollision = false;

	if (pCollision != nullptr)
	{
		while (pCollision)
		{// ���݂̃I�u�W�F�N�g�̎��̃I�u�W�F�N�g��ۊ�
			CCollision *pCollisionNext = pCollision->GetNext();
			CObject::EObjectType myObjeType = pCollision->GetParent()->GetObjType();

			if ((myObjeType == objType
				|| CObject::OBJTYPE_NONE == objType)
				&& pCollision != this
				&& pCollision->GetUseFlag()
				&& !pCollision->GetDeathFlag())
			{
				switch (pCollision->GetType())
				{
				case CCollision::TYPE_RECTANGLE3D:
				case CCollision::TYPE_SPHERE:
					break;

				case CCollision::TYPE_RECTANGLE2D:
					if (hmd::IsBoundingBoxCollision(myBoundingBox, pCollision->GetBoundingBox()).isUse)
					{
						if (ToRectangle(pCollision, bExtrude))
						{
							bCollision = true;

							// �����蔻��̒ǉ�
							pCollided.push_back(pCollision->GetParent());
							nCntCollided++;
						}
					}
					break;

				default:
					assert(false);
					break;
				}
			}

			// ���݂̃I�u�W�F�N�g�̎��̃I�u�W�F�N�g���X�V
			pCollision = pCollisionNext;
		}

		// �Փ˂����I�u�W�F�N�g�̃Z�b�g
		SetCollidedObj(pCollided);
	}

	return bCollision;
}

//=============================================================================
// ��`�Ƃ̓����蔻��
// Author : �������l
// �T�v : ��`�Ƃ̓����蔻��
//=============================================================================
bool CCollision_Rectangle2D::ToRectangle(CCollision *pTarget, bool bExtrude)
{
	// �Ԃ�l�p�̕ϐ�
	bool bColision = false;
	m_bPlusMinus = true;

	// �����̏����擾����
	D3DXVECTOR3 pos = GetParent()->GetPos() + GetPos();
	D3DXVECTOR3 posOld = GetParent()->GetPosOld() + GetPos();
	D3DXVECTOR3 size = GetSize() / 2.0f;

	// �ڕW�̏��擾
	D3DXVECTOR3 posTarget = pTarget->GetParent()->GetPos() + pTarget->GetPos();
	D3DXVECTOR3 sizeTarget = pTarget->GetSize() / 2.0f;
	m_state = STATE_NONE;

	if ((pos.x - size.x) < (posTarget.x + sizeTarget.x)
		&& (pos.x + size.x) > (posTarget.x - sizeTarget.x)
		&& (pos.y - size.y) < (posTarget.y + sizeTarget.y)
		&& (pos.y + size.y) > (posTarget.y - sizeTarget.y))
	{
		bColision = true;
	}

	if (pos.y - size.y  < posTarget.y + sizeTarget.y
		&& pos.y + size.y  > posTarget.y - sizeTarget.y)
	{// ���f�����ɂ���(Y��)
		if (posOld.y + size.y <= posTarget.y - sizeTarget.y
			&& pos.y + size.y  > posTarget.y - sizeTarget.y)
		{
			m_state = STATE_Y;
			if (bExtrude)
			{// �����o�����g�p����
				m_bPlusMinus = false;
				pos.y = posTarget.y - sizeTarget.y - size.y;
			}
		}
		if (posOld.y - size.y >= posTarget.y + sizeTarget.y
			&& pos.y - size.y  < posTarget.y + sizeTarget.y)
		{
			m_state = STATE_Y;
			if (bExtrude)
			{// �����o�����g�p����
				pos.y = posTarget.y + sizeTarget.y + size.y;
			}
		}
	}
	if (pos.x - size.x  < posTarget.x + sizeTarget.x
		&& pos.x + size.x  > posTarget.x - sizeTarget.x)
	{// ���f�����ɂ���(X��)
		if (posOld.x + size.x <= posTarget.x - sizeTarget.x
			&& pos.x + size.x  > posTarget.x - sizeTarget.x)
		{
			m_state = STATE_X;
			if (bExtrude)
			{// �����o�����g�p����
				m_bPlusMinus = false;
				pos.x = posTarget.x - sizeTarget.x - size.x;
			}
		}
		if (posOld.x- size.x >= posTarget.x + sizeTarget.x
			&& pos.x- size.x  < posTarget.x + sizeTarget.x)
		{
			m_state = STATE_X;
			if (bExtrude)
			{// �����o�����g�p����
				pos.x = posTarget.x + sizeTarget.x + size.x;
			}
		}
	}

	if (bColision)
	{// �ʒu�̐ݒ�
		CObject *pParent = GetParent();
		pParent->SetPos(pos - GetPos());
	}

	return bColision;
}


