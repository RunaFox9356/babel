//=============================================================================
//
// シーンモードクラス(scene_mode.h)
// Author : 唐�ｱ結斗
// 概要 : シーンモードの派生を行う
//
//=============================================================================
#ifndef _SCENE_MODE_H_		// このマクロ定義がされてなかったら
#define _SCENE_MODE_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "super.h"
#include "model_data.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CGimmick;
class CMiniGameMNG;
class CMap;

//=============================================================================
// シーンモードクラス
// Author : 唐�ｱ結斗
// 概要 : シーンモード生成を行うクラス
//=============================================================================
class CSceneMode : public CSuper
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CSceneMode();
	~CSceneMode() override;

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新
	void Draw() override;		// 描画

	void InitPosh();
	void SetFog(const LPDIRECT3DDEVICE9& pDev, float start, float end, float density, D3DXCOLOR color);

	//サーバーに送るデータの関数
	int GetGimmickSize() { return m_NumGimmick; };
	int PoshGimmick(CGimmick * SetModel) { m_popGimmick[m_NumGimmick] = SetModel;  m_NumGimmick++; return m_NumGimmick-1; };
	void SetGimmick(CGimmick * SetModel, int Num) { m_popGimmick[Num] = SetModel; };
	CGimmick * GetGimmick(int Num) { return m_popGimmick[Num]; };
	CGimmick * m_popGimmick[MaxModel];
	
	int GetEnemySize() { return m_NumEnemy; };
	int PoshEnemyUse(bool SetModel) { m_popEnemy[m_NumGimmick].isUse = SetModel; m_popEnemy[m_NumGimmick].isDiscovery = false; m_NumEnemy++; return m_NumEnemy - 1; };
	void SetEnemyUse(bool SetModel, int Num) { m_popEnemy[Num].isUse = SetModel; };
	void SetEnemyDiscovery(bool SetModel, int Num) { m_popEnemy[Num].isDiscovery = SetModel; };
	void SetEnemyOnMap(bool bMap, int Num) { m_popEnemy[Num].isMap = bMap; };
	void SetEnemyPos(D3DXVECTOR3 SetModel, int Num) { m_popEnemy[Num].Pos = SetModel; };
	void SetEnemyDes(bool bDes, int Num) { m_popEnemy[Num].isDes = bDes; };
	void SetEnemyAnime(int bDes, int Num) { m_popEnemy[Num].isMotion = bDes; };
	CModelData::SEnemyData GetEnemy(int Num) { return m_popEnemy[Num]; };
	CModelData::SEnemyData m_popEnemy[MaxModel];
	int m_NumGimmick;

	CMiniGameMNG* GetMiniMng() { return m_MiniMng; };

	CMap*GetMap() { return m_map; }
	void SetMap(CMap*Map) { m_map = Map; }
	void SetEnd(const bool bEnd) { m_bEnd = bEnd; }				//終わったかどうかのフラグの設定処理
	const bool GetEnd() { return m_bEnd; }						//終わったかどうかのフラグの取得処理

	void SetItem() { m_NumItemId++; }
	int GetItem() { return m_NumItemId; }
private:
	CMap*m_map;
	CMiniGameMNG* m_MiniMng;	//ミニゲームマネージャー
	int m_NumEnemy;
	int m_NumItemId;
	bool m_bEnd;

};

#endif

