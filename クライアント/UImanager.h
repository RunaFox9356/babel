//=============================================================================
//
// マネージャークラス(manager.h)
// Author : 唐�ｱ結斗
// 概要 : マネージャーの派生を行う
//
//=============================================================================
#ifndef _UIMANAGER_H_		// このマクロ定義がされてなかったら
#define _UIMANAGER_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "manager.h"

class CHackingObj;
class CPolygon2D;
class CPolygon3D;

//=============================================================================
// マネージャークラス
// Author : 唐�ｱ結斗
// 概要 : マネージャー生成を行うクラス
//=============================================================================
class CUIManager:public CManager
{
public:
	enum EUIType
	{
		UI_TYPE_UI = 0,		//画像のみ
		UI_TYPE_TIMER,		//タイマー
		UI_TYPE_SCORE,		//スコア
		UI_TYPE_NUMBER,		//数字
		UI_TYPE_TASK,		//達成目標
		UI_TYPE_BILLBOARD,	//ビルボード
		UI_TYPE_MINIMAP,	//ミニマップ
		UI_TYPE_MAX
	};
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CUIManager *Create();	// UIの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CUIManager();
	~CUIManager();
	HRESULT Init()override;
	void Uninit()override;
	void Update()override;
	void Draw()override;
	CPolygon2D* LoadUI(EUIType type,D3DXVECTOR3 pos ,D3DXVECTOR3 size,int texnumber);	//UI生成
	CPolygon3D* Load3DUI(EUIType type, D3DXVECTOR3 pos, D3DXVECTOR3 size, int texnumber);	//UI生成

private:

};

#endif
