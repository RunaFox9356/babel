//==================================================
// utility.h
// Author: Buriya Kota
//==================================================
#ifndef _UTILITY_H_
#define _UTILITY_H_

//**************************************************
// インクルード
//**************************************************
#include <random>
#include <chrono>
#include <vector>
//#include <math.h>

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************
class CBoundingBox3D;
class CBoundingSphere3D;

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// グローバル関数
//**************************************************

struct SHitData
{
	bool isUse;
	int isHitType;
};
namespace hmd
{
	
	// 座標変換
	SHitData IsBoundingBoxCollision(const CBoundingBox3D& box1, const CBoundingBox3D& box2);
	SHitData IsBoxCollision(const CBoundingBox3D& box1, const CBoundingBox3D& box2);
	SHitData IsSphereCollision(const CBoundingSphere3D& sphere1, const CBoundingSphere3D& sphere2);
	float Vec2Cross(D3DXVECTOR3* v1, D3DXVECTOR3* v2);
	float D3DXVec2Dot(D3DXVECTOR3* v1, D3DXVECTOR3* v2);
	float FloatRandam(float fMax, float fMin);
	int IntRandom(int nMax, int nMin);
	float NormalizeAngle(float *pAngle);
	void NormalizeAngleDest(float *pAngle, float *pAngleDest);
	float easeInOutQuint(float x);
	float easeOutQuint(float x);
	float ScalarTriple(const D3DXVECTOR3& a, const D3DXVECTOR3& b, const D3DXVECTOR3& c);
	float SearchLargest(std::vector<float> &fSearchGroup);

	D3DXVECTOR3 WorldCastScreen(D3DXVECTOR3 *screenPos,			// スクリーン座標
		const D3DXVECTOR3& screenSize,									// スクリーンサイズ
		const D3DXMATRIX& mtxView,									// ビューマトリックス
		const D3DXMATRIX& mtxProjection);								// プロジェクションマトリックス
	bool CollisionCircle(const D3DXVECTOR3& pos1, float radius1, const D3DXVECTOR3& pos2, float radius2);
	//D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard = false);
	//D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard = false);
	float easeInSine(float X);
	float easeInQuad(float X);
	bool is_sjis_lead_byte(int c);

	bool IntersectionLineToPlane(const D3DXVECTOR3 &start, const D3DXVECTOR3 &goal,
		const D3DXVECTOR3 &vector0, const D3DXVECTOR3 &vector1, const D3DXVECTOR3 &vector2, D3DXVECTOR3 &OutPos);
	bool IntersectionLineToTriangle(const D3DXVECTOR3 &start, const D3DXVECTOR3 &goal,
		const D3DXVECTOR3 &vector0, const D3DXVECTOR3 &vector1, const D3DXVECTOR3 &vector2, D3DXVECTOR3 &OutPos);
	bool IsPointInsideTriangle(const D3DXVECTOR3& point, const D3DXVECTOR3& vector0, const D3DXVECTOR3& vector1, const D3DXVECTOR3& vector2);
	void CreatePlaneFromPoints(const D3DXVECTOR3& vector0, const D3DXVECTOR3& vector1, const D3DXVECTOR3& vector2, D3DXPLANE& plane);
	D3DXVECTOR3* CalcWallScratchVector(D3DXVECTOR3* out, const D3DXVECTOR3& front, const D3DXVECTOR3& normal);
}



namespace utility
{
	// enumからInt型に変換する処理
	template<typename T>
	inline typename std::underlying_type<T>::type EnumToInt(T value)
	{
		return static_cast<typename std::underlying_type<T>::type>(value);
	}

	// Int型からenumに変換する処理
	template <typename T>
	inline T IntToEnum(typename std::underlying_type<T>::type value)
	{
		return static_cast<T>(value);
	}

	// 数値をランダムにする処理
	template<typename T>
	inline T Random(T max, T min)
	{
		std::random_device seed_gen;
		std::mt19937 gen(seed_gen());
		std::uniform_int_distribution<> distribution(0, 32767);
		int randomValue = distribution(gen);
		return ((randomValue * (1.0f / 32767.0f)) * (max - min)) + min;
	}

	inline D3DXVECTOR3 GetWorldPosMatrix(D3DXMATRIX* mtx)
	{
		D3DXVECTOR3 result;
		result.x = mtx->_41;
		result.y = mtx->_42;
		result.z = mtx->_43;
		return result;
	}

	template<typename T>
	inline void CopyBuffer(unsigned size, void* src, T* buf)
	{
		void *p = 0;
		buf->Lock(0, 0, &p, 0);
		memcpy(p, src, size);
		buf->Unlock();
	}
}

#endif	// _UTILITY_H_