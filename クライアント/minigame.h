//=============================================================================
//
// ミニゲーム.h
// Author: hamada
//
//=============================================================================

#ifndef _MINGAME_H_		// このマクロ定義がされてなかったら
#define _MINGAME_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include"polygon2D.h"

class CMiniGame : public CPolygon2D
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CMiniGame(int nPriority = PRIORITY_LEVEL0);
	~CMiniGame();

	HRESULT Init();														// 初期化
	void Uninit();														// 終了
	void Update();														// 更新
	void Draw();														// 描画

	static CMiniGame *Create(int count);

protected:

private:


};
#endif
