//=============================================================================
//
// プレイヤークラス(player.cpp)
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "drone.h"
#include "game.h"
#include "mesh.h"
#include "motion.h"
#include "renderer.h"
#include "application.h"
#include "camera.h"
#include "input.h"
#include "calculation.h"
#include "move.h"
#include "debug_proc.h"
#include "parts.h"
#include "sound.h"
#include "scene_mode.h"
#include "client.h"
#include "bullet.h"
#include "collision_rectangle3D.h"
#include "enemy.h"
#include "polygon2D.h"
#include "model_data.h"
#include "tcp_client.h"
#include "itemObj.h"
#include "hacker.h"
#include "viewField.h"
#include "mapDataManager.h"
#include "gimmick.h"
#include "task.h"
#include "sendingScreen.h"
#include "text.h"
#include "HackerUI.h"
#include "Score.h"
#include "securityCamera.h"
#include "respawnLoading.h"
#include "ranking.h"
#include "hacker.h"
#include "HackerUI.h"
#include "Timer.h"
#include "renderer.h"
#include "utility.h"
#include "soundSource.h"

// ---------------------------------------------------------------------------- 
// 定数
//-----------------------------------------------------------------------------
const int		CDrone::HACKING_STROKE = 400;				// ハッキング可能距離
const float		CDrone::fSPEED = 0.175f;						//ドローンの速度
const int		CDrone::DEFAULT_LIFE = 5;					// ディフォルトのライフ
const int		CDrone::DEFAULT_INVULNERABILITY = 90;		//ディフォルトの無敵状態のフレーム数
const int		CDrone::DEFAULT_RESPAWN_FRAMES = 900;		//リスポーンに必要な時間(15秒)
const float		CDrone::DEFAULT_MAX_HEIGHT = 1200.0f;		//最大のY座標

//モデルパーツの相対位置
const D3DXVECTOR3	CDrone::DEFAULT_PARTS_RELATiVE_POS[4] =
{
	{ +15.0f, 9.0f, +11.6f },
	{ -15.0f, 9.0f, +11.6f },
	{ +15.0f, 9.0f, -11.6f },
	{ -15.0f, 9.0f, -11.6f }
};


namespace nsDrone
{
	float	s_fWidth = 1280.0f / 720.0f;				//スノーノイズのテクスチャの比率
	float	s_fGravity = -0.1f;							//ディフォルトの重力加速度
	float	s_fMaxInterferenceHeight = 2000.0f;			
	float	s_fSlope = 0.0f;
	float	s_fIntercept = 0.0f;
};

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CDrone * CDrone::Create()
{
	// オブジェクトインスタンス
	CDrone *pPlayer = nullptr;

	// メモリの解放
	pPlayer = new CDrone;

	// メモリの確保ができなかった
	assert(pPlayer != nullptr);

	// 数値の初期化
	pPlayer->Init();

	// インスタンスを返す
	return pPlayer;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CDrone::CDrone() : m_EAction(NEUTRAL_ACTION),
m_nNumMotion(0),
m_nLife(0),
m_nInvulnerability(0),
m_fSpeedY(0.0f),
m_bSendingData(false),
m_bHacking(false),
m_pMessage(nullptr),
m_pRespawnUi(nullptr),
m_pStaticUi(nullptr),
m_pCommandUI(nullptr)
{
	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		m_pParts[nCnt] = nullptr;
	}
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CDrone::~CDrone()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗　有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CDrone::Init()
{
	CPlayer::Init();

	//m_socket = new CSocket;
	//m_socket->Init();
	//m_socket->PoPList();

	// 移動クラスのメモリ確保
	GetMove()->SetMoving(fSPEED, 10.0f, 0.0f, 0.02f);

	if (GetCollision())
	{
		GetCollision()->SetSize(D3DXVECTOR3(30.0f, 25.0f, 30.0f));
		GetCollision()->SetPos(D3DXVECTOR3(0.0f, 10.0f, 0.0f));
	}

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		m_pParts[nCnt] = CModelObj::Create();

		if (m_pParts[nCnt])
		{
			m_pParts[nCnt]->SetType(237);
		}
	}

	m_nLife = DEFAULT_LIFE;				//体力設定

	SetObjType(OBJETYPE_DRONE);	//種類をドローンに設定
	/*SetHaveItem(CItemObj::ITEM_NON);*/

	//ドローン用サイトui
	m_pSite = CPolygon2D::Create();
	m_pSite->SetSize(D3DXVECTOR3(50.0f, 50.0f, 50.0f));
	m_pSite->SetPos(D3DXVECTOR3(1280.0f * 0.5f, 720.0f * 0.5f,  0.0f));
	std::vector<int> pTex;
	pTex.push_back(41);
	m_pSite->LoadTex(pTex);
	SetBomCommand(false);
	m_bControl = true;

	//スノーノイズのUIの生成
	m_pStaticUi = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

	if (m_pStaticUi)
	{//nullチェック

		m_pStaticUi->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));
		m_pStaticUi->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));
		m_pStaticUi->LoadTex(54);
		m_pStaticUi->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		m_pStaticUi->SetDraw(false);
	}

	//視野
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_View = CViewField::Create(this);
	m_View->SetViewAngle(D3DX_PI * 0.125f);
	m_View->SetViewDistance((float)HACKING_STROKE);
	m_View->SetPosV(pos);

	//ベクトルのクリア処理
	m_vDataToSend.clear();
	m_vDataToSend.shrink_to_fit();

	nsDrone::s_fSlope = 1.0f / (nsDrone::s_fMaxInterferenceHeight);
	nsDrone::s_fIntercept = - nsDrone::s_fSlope * DEFAULT_MAX_HEIGHT;
	CModelData* Model = CApplication::GetInstance()->GetPlayer(1);
	Model->GetPlayerData()->Player.m_MyId = P1;

	CreateSound(CSound::SOUND_LABEL_SE_DRONE, 900);

	m_pCommandUI = CPolygon2D::Create();

	if (m_pCommandUI)
	{
		m_pCommandUI->SetPos(D3DXVECTOR3(350.0f, 680.0f, 0.0f));
		m_pCommandUI->SetSize(D3DXVECTOR3(80.0f, 80.0f, 0.0f));
		m_pCommandUI->LoadTex(138);
		m_pCommandUI->SetDraw(false);
	}

	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CDrone::Uninit()
{

	if (CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{
		CRanking* pRanking = CApplication::GetInstance()->GetRanking();
		CHackerUI* pUi = CHacker::GetHackerUI();

		if (pRanking && pUi)
		{
			pRanking->GetRank(pUi->GetScore()->GetScore());
		}
	}

	CHackerUI* pUiManager = CHacker::GetHackerUI();

	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);
	CModelData::SSendPack data;
	data = Model->GetPlayerData()->Player;
	data.m_PlayData.m_pos = GetPos();
	data.m_PlayData.m_rot = GetRotDest();
	//data.m_haveItemLeftId = GetHaveItemLeft();
	//data.m_haveItemRightId = GetHaveItemRight();
	if (CHacker::GetHackerUI() != nullptr)
	{
		data.m_PlayData.m_Timer = CHacker::GetHackerUI()->GetTimer()->GetTimer();
	}
	else
	{
		data.m_PlayData.m_Timer = 0;
	}
	data.m_PlayData.m_motion = GetMotion()->GetNumMotion();
	if (Model->GetPlayerData()->Player.m_log + 1 >= 9999999999)
	{
		Model->GetPlayerData()->Player.m_log = 0;
	}
	data.m_PlayData.m_pushBomComands = GetBomCommand();
	Model->GetPlayerData()->SetPlayerCast((const char*)&data);
	data.m_IsGame = false;
	data.m_MyId = P2;
	if (pUiManager)
	{
		data.m_addscore = pUiManager->GetScore()->GetSendScore();
	}
	CApplication::GetInstance()->GetClient()->Send((const char*)&data, sizeof(CModelData::SSendPack), CClient::TYPE_UDP);

	//UIの破棄
	if (m_pStaticUi)
	{
		m_pStaticUi->Uninit();
		m_pStaticUi = nullptr;
	}

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{
		if (m_pParts[nCnt])
		{
			m_pParts[nCnt]->Uninit();
			m_pParts[nCnt] = nullptr;
		}
	}
	if (m_pCommandUI)
	{
		m_pCommandUI->Uninit();
		m_pCommandUI = nullptr;
	}

	//ベクトルのクリア処理
	m_vDataToSend.clear();
	m_vDataToSend.shrink_to_fit();

	//UIメッセージの破棄
	DestroyMessage();

	CPlayer::Uninit();
	//if (m_socket != nullptr)
	//{
	//	m_socket->Uninit();
	//	delete m_socket;
	//}

}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗　有田明玄
// 概要 : 2D更新を行う
//=============================================================================
void CDrone::Update()
{
	if (!Respawn())
		return;

	if (m_bControl)	//操作可能かどうか
	{
		//起爆使ってないとき初期化
		SetBomCommand(false);

		// 位置の取得
		D3DXVECTOR3 pos = GetPos(), p = pos;

		// 過去位置の更新
		SetPosOld(pos);

		// 移動
		pos += Flying();

		// 位置の設定
		SetPos(pos);

		//親クラスの更新処理
		CPlayer::Update();

		//カメラの向き
		CCamera *pCamera = CApplication::GetInstance()->GetCamera();
		D3DXVECTOR3 rot = GetRotDest();
		D3DXVECTOR3 rotCamera = pCamera->GetRot();
		rot.y = rotCamera.y - D3DX_PI;
		rot.y = CCalculation::RotNormalization(rot.y);
		SetRotDest(rot);
		D3DXVECTOR3 rotView = D3DXVECTOR3(rotCamera.x, -D3DX_PI, 0);
		rotView.x = CCalculation::RotNormalization(rotView.x);
		rotView.y = CCalculation::RotNormalization(rotView.y);
		m_View->SetRot(rotView);

		UpdateStaticNoise();

		//タスクのターゲットが見えるかどうかの判定処理
		LookForTargets();

		int nMap = CApplication::GetInstance()->GetMap();

		if (CApplication::GetInstance()->GetMapDataManager()->GetMapData(nMap).vMeshData[0].pMesh->MeshtoSelfCollison(this))
		{
			//Landing();
		}

		CollisionCheck();

		// 入力デバイスの取得
		CInput *pInput = CInput::GetKey();

		if (pInput->Press(DIK_RETURN))
		{//エンターキーを押したら

			//ばくだん起爆
			SetBomCommand(true);
		}
	}

	// 当たり判定
	CCollision_Rectangle3D *pCollision = GetCollision();

	//当たり判定のクラスを取得できなかったら、中断する
	if (!pCollision)
		return;

	if (pCollision->Collision(OBJTYPE_TEST, false))
	{
		std::vector<CObject*> pCollidedObj = pCollision->GetCollidedObj();
		int nSize = pCollidedObj.size();

		for (int nCntObj = 0; nCntObj < nSize; nCntObj++)
		{
			CModelObj *pModelObj = (CModelObj*)pCollidedObj.at(nCntObj);
			CModel3D *pModel = pModelObj->GetModel();
			bool bCollison = pModel->Collison(this, D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		}
	}

	{
		D3DXMATRIX mtxWorld, mtxRot, mtxTrans, mtxOut;
		D3DXVECTOR3 pos = GetPos(), rot = GetRot();

		D3DXMatrixIdentity(&mtxWorld);
		D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
		D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxRot);
		D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
		D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxTrans);

		for (int nCnt = 0; nCnt < 4; nCnt++)
		{
			if (m_pParts[nCnt])
			{
				D3DXMatrixIdentity(&mtxOut);
				D3DXMatrixTranslation(&mtxTrans, DEFAULT_PARTS_RELATiVE_POS[nCnt].x, DEFAULT_PARTS_RELATiVE_POS[nCnt].y, DEFAULT_PARTS_RELATiVE_POS[nCnt].z);
				D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxTrans);
				D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxWorld);
				D3DXVec3TransformCoord(&pos, &DEFAULT_PARTS_RELATiVE_POS[nCnt], &mtxOut);

				m_pParts[nCnt]->SetPos(pos);

				rot = m_pParts[nCnt]->GetRot() + D3DXVECTOR3(0.0f, D3DX_PI * 0.125f, 0.0f);

				if (rot.y > 2.0f * D3DX_PI)
					rot.y -= 2.0f * D3DX_PI;

				m_pParts[nCnt]->SetRot(rot);
			}
		}
	}

	CHackerUI* pUiManager = CHacker::GetHackerUI();

	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);
	CModelData::SSendPack data;
	data = Model->GetPlayerData()->Player;
	data.m_PlayData.m_pos = GetPos();
	data.m_PlayData.m_rot = GetRotDest();
	//data.m_haveItemLeftId = GetHaveItemLeft();
	//data.m_haveItemRightId = GetHaveItemRight();
	data.m_PlayData.m_motion = GetMotion()->GetNumMotion();
	data.m_log = Model->GetPlayerData()->Player.m_log+1;
	data.m_PlayData.m_pushBomComands = GetBomCommand();
	data.m_PlayData.m_Life = m_nLife;
	Model->GetPlayerData()->SetPlayerCast((const char*)&data);
	data.m_IsGame = true;
	data.m_MyId = P2;
	if (pUiManager)
	{
		data.m_addscore = pUiManager->GetScore()->GetSendScore();
	}
	//CApplication::GetInstance()->GetTcpClient()->Send((const char*)&data, sizeof(CModelData::SSendPack));

	CApplication::GetInstance()->GetClient()->Send((const char*)&data, sizeof(CModelData::SSendPack),CClient::TYPE_UDP);

}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CDrone::Draw()
{
	if (m_nInvulnerability % 10 <= 5)
		CPlayer::Draw();
}

//データを送信済みであるかどうかのセッター
void CDrone::SetSendingState(const bool bSend)
{
	m_bSendingData = bSend;
	SetStopState(bSend);
}

//=============================================================================
// 移動
// Author : 唐�ｱ結斗　有田明玄
// 概要 : キー入力で方向を決めて、移動ベクトルを算出する
//=============================================================================
D3DXVECTOR3 CDrone::Flying()
{
	// 変数宣言
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_bSendingData || m_bHacking)
		return move;

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	if (GetPos().y > DEFAULT_MAX_HEIGHT + ((nsDrone::s_fMaxInterferenceHeight - DEFAULT_MAX_HEIGHT) * 0.5f))
	{
		CCamera* pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
		{
			D3DXVECTOR3 dir = pCamera->GetPosR() - pCamera->GetPosV();
			dir.y = 0.0f;
			D3DXVec3Normalize(&dir, &dir);

			move = dir;
		}

		if (pInput->Press(DIK_SPACE))
		{// スペースキーが押された時
		 // 移動方向の更新
			move.y = 1.0f;

			if (m_fSpeedY < 0.0f)
			{
				m_fSpeedY += 1.0f;

				if (m_fSpeedY > 0.0f)
					m_fSpeedY = 0.0f;
			}
		}
		if (pInput->Press(DIK_LSHIFT))
		{// 左シフトが押された時
		 // 移動方向の更新
			move.y = -1.0f;
		}

		return move;
	}

	// 移動クラスのメモリ確保
	CMove *pMove = GetMove();

	if (pInput->Press(DIK_SPACE) ||
		pInput->Press(DIK_LSHIFT) ||
		pInput->Press(MOUSE_INPUT_LEFT) ||
		pInput->Press(MOUSE_INPUT_RIGHT))
	{// 移動キーが押された
		if (pInput->Press(DIK_SPACE))
		{// スペースキーが押された時
			// 移動方向の更新
			move.y = 1.0f;

			if (m_fSpeedY < 0.0f)
			{
				m_fSpeedY += 1.0f;

				if (m_fSpeedY > 0.0f)
					m_fSpeedY = 0.0f;
			}
		}
		if (pInput->Press(DIK_LSHIFT))
		{// 左シフトが押された時
		 // 移動方向の更新
			move.y = -1.0f;
		}
		//if (pInput->Trigger(MOUSE_INPUT_LEFT) && m_nHealTimer <= 0)
		//{// スペースキーが押された時
		// // 移動方向の更新
		//	Shot(3);
		//	m_nHealTimer = 600;
		//	CHacker::GetHackerUI()->GetHealTimer()->SetTimer(m_nHealTimer);
		//}
		//if (pInput->Trigger(MOUSE_INPUT_RIGHT) && m_nShockTimer <= 0)
		//{// スペースキーが押された時
		//	// 移動方向の更新
		//	Shot(2);
		//}
	}

#ifdef _DEBUG
	if (pInput->Trigger(DIK_E))
	{// スペースキーが押された時
	 // 移動方向の更新
		m_nShockTimer = 0;
		m_nHealTimer = 0;
	}
#endif // _DEBUG

	if (!m_bSendingData && pInput->Trigger(DIK_T))
	{// Tキーが押された時
		SendData();
	}

	m_nHealTimer--; m_nShockTimer--;
	
	// 移動情報の計算
	pMove->Moving(move);

	// 移動情報の取得
	D3DXVECTOR3 moveing = GetMove()->GetMove();
	
	return moveing;
}

//=============================================================================
// 射撃
// Author : 有田明玄
// 概要 : ドローンから弾を飛ばす
//=============================================================================
void CDrone::Shot(int /*nType*/)
{
	// カメラ情報の取得
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	CBullet*pBul = CBullet::Create();				//生成
	pBul->SetPos(GetPos());							//位置設定

	D3DXVECTOR3 dir = pCamera->GetPosR() - pCamera->GetPosV();
	D3DXVec3Normalize(&dir, &dir);
	pBul->SetMoveRot(dir);
	pBul->SetParent(this);
	pBul->SetObjType(CObject::OBJTYPE_PLAYER_BULLET);

	//当たり判定設定
	CCollision_Rectangle3D* pCollision = pBul->GetCollision();
	D3DXVECTOR3 modelDoorGimmick = pBul->GetModel()->GetMyMaterial().size;
	pCollision = pBul->GetCollision();
	pCollision->SetSize(modelDoorGimmick);
	pCollision->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y * 0.5f, 0.0f));
}

//破壊できるPCを探す処理
void CDrone::LookForTargets()
{
	if (m_View && !m_bSendingData)
	{//視野のnullチェック

		CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
		int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

		if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
		{//マップデータマネージャーのnullチェックとマップインデックスの確認

			//ハッカーのターゲットのデータを取得する
			std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

			for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
			{//全部確認する

				if (vData.data()[nCnt].pObj && !vData.data()[nCnt].bFound && !vData.data()[nCnt].bGot)
				{//オブジェクトのポインタのnullチェックとエージェントとハッカーにまだ見つかっていない場合

					//位置の取得
					D3DXVECTOR3 pos = vData.data()[nCnt].pObj->GetPos();

					if (m_View->IsPointInFront(pos))
					{//前にあって、遠すぎない場合

						//オブジェクトを見つかったフラグをtrueにする
						pDataManager->FoundObj(nMapIdx, nCnt);

						CTask* pTask = nullptr;		//タスクマネージャーを取得する

						CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

						//ハッカーモードではなかったら、中断する
						if (mode != CApplication::MODE_HACKER)
							return;

						pTask = CHacker::GetTask();			//タスクの取得

						if (pTask)
						{//nullチェック

							//送信できるデータを追加する
							m_vDataToSend.push_back(nCnt);

							//UIメッセージの生成
							CreateMessage();

							//タスクの更新
							//pTask->SubtractHackerTask(vData.data()[nCnt].type);
						}
					}
				}
			}
		}
	}
}

//データの送信処理
void CDrone::SendData()
{
	//送信できるデータがなかったら、中断する
	if ((int)m_vDataToSend.size() <= 0)
		return;

	//送信画面の生成
	CSendingScreen::Create(m_vDataToSend, this);	
	
	//送信できるデータのベクトルをクリアする
	m_vDataToSend.clear();
	m_vDataToSend.shrink_to_fit();

	//送信中フラグをtrueにする
	SetSendingState(true);

	//UIメッセージの破棄
	DestroyMessage();
}

//当たり判定
void CDrone::CollisionCheck()
{
	//当たり判定用のクラスの取得
	CCollision_Rectangle3D* pCollision = GetCollision();

	//当たり判定用のクラスを取得できなかったら、中断する
	if (!pCollision)
		return;

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	//モデルの当たり判定
	pCollision->Collision(CObject::OBJTYPE_3DMODEL, true);

	//モデルの当たり判定
	pCollision->Collision(CObject::OBJETYPE_GIMMICK, true);

	if (m_nInvulnerability <= 0 && pCollision->Collision(CObject::OBJTYPE_ENEMY_BULLET, false)/* || pInput->Trigger(DIK_J)*/)
	{//無敵状態ではなく、弾と当たった場合

		m_nLife--;			//ライフの更新

		m_nInvulnerability = DEFAULT_INVULNERABILITY;		//無敵状態の設定

		if (m_nLife <= 0)
		{//ライフが0になったら、画面遷移

			m_nInvulnerability--;		//描画しないように設定する

			{//死
			 // カメラの追従解除
				CCamera *pCamera = CApplication::GetInstance()->GetCamera();
				pCamera->SetFollowTarget(false);

				// カメラの追従解除
				pCamera = CApplication::GetInstance()->GetMapCamera();
				pCamera->SetFollowTarget(false);

				std::vector<CSecurityCamera*> vCamera;
				vCamera.clear();

				CSecurityCamera::GetAllCameras(vCamera);

				if ((int)vCamera.size() > 0)
				{
					vCamera.data()[0]->ActivateCamera();
				}

				//リスポーンのUIを生成する
				if (!m_pRespawnUi)
				{
					m_pRespawnUi = CRespawnLoading::Create(DEFAULT_RESPAWN_FRAMES);

					if (m_pRespawnUi)
					{
						m_pRespawnUi->SetNewMessage("PRESS R TO RESPAWN");
					}
				}
			}

			return;
		}
	}
	else if (m_nInvulnerability > 0)
	{//無敵状態カウンターが0より大きければ

		m_nInvulnerability--;		//無敵状態カウンターの更新
	}
}

//UIメッセージの生成
void CDrone::CreateMessage()
{
	//メッセージの破棄
	DestroyMessage();

	if (!m_pMessage)
	{//nullだったら

		//テキストの情報の設定
		D3DXVECTOR3 pos = D3DXVECTOR3(400.0f, 680.0f, 0.0f);					//位置の設定
		D3DXVECTOR3 size = D3DXVECTOR3(300.0f, 20.0f, 0.0f);					//サイズの設定
		D3DXVECTOR3 fontSize = D3DXVECTOR3(10.0f, 10.0f, 10.0f);				//フォントサイズ
		D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0, 0.2f, 1.0f);						//フォント色
		std::string str = "SENDABLE DATA: " + std::to_string((int)m_vDataToSend.size());	//メッセージの内容

		if (m_pCommandUI)
		{
			m_pCommandUI->SetDraw(true);
		}

		//テキストの生成
		m_pMessage = CText::Create(pos, size, CText::MAX, 1000, 1, str.c_str(), fontSize, color);	//テキストの生成
	}
}

//UIメッセージの破棄
void CDrone::DestroyMessage()
{
	if (m_pMessage)
	{
		m_pMessage->Uninit();
		m_pMessage = nullptr;
	}

	if (m_pCommandUI)
	{
		m_pCommandUI->SetDraw(false);
	}
}

//リスポーンの処理
bool CDrone::Respawn()
{
	if (m_nLife > 0)
		return true;

	//インプット情報の取得
	CInput* pInput = CInput::GetKey();

	if (m_pRespawnUi && m_pRespawnUi->GetEnd() && pInput && pInput->Trigger(DIK_R))
	{
		m_nLife = DEFAULT_LIFE;							//ライフの設定
														
		m_pRespawnUi->Uninit();							//リスポーンUIを破棄する
		m_pRespawnUi = nullptr;							//ポインタをnullに戻す

		CPlayer* pPlayer = CHacker::GetPlayer();		//プレイヤー情報を取得する

		if (pPlayer)
		{//nullチェック

			SetPos(pPlayer->GetPos());					//ドローンの位置を設定する
		}

		//監視カメラを全部取得する
		std::vector<CSecurityCamera*> vCamera;
		vCamera.clear();
		CSecurityCamera::GetAllCameras(vCamera);

		if ((int)vCamera.size() > 0)
		{//監視カメラがあったら、全部のカメラを確認する

			for (int nCnt = 0; nCnt < (int)vCamera.size(); nCnt++)
			{
				if (vCamera.data()[nCnt]->GetHackedState())
				{//ハッキング中だったら、ドローンモードに切り替える

					vCamera.data()[nCnt]->DeactivateCamera();
					break;
				}
			}
		}

		return true;
	}

	return false;
}

//スノーノイズの更新
void CDrone::UpdateStaticNoise()
{
	D3DXVECTOR3 pos = GetPos();

	if (m_pStaticUi && pos.y > DEFAULT_MAX_HEIGHT)
	{
		m_pStaticUi->SetDraw(true);
		m_fSpeedY += nsDrone::s_fGravity;

		pos.y += m_fSpeedY;

		if (pos.y > DEFAULT_MAX_HEIGHT)
		{
			m_pStaticUi->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, nsDrone::s_fSlope * pos.y + nsDrone::s_fIntercept));

			if (GetSoundSource()->GetSoundLabel() != CSound::SOUND_LABEL_SE_STATIC_NOISE)
			{
				GetSoundSource()->SetSoundMax(0.5f);
				GetSoundSource()->SetDelay(480);
				GetSoundSource()->SetSoudLabel(CSound::SOUND_LABEL_SE_STATIC_NOISE);
			}
		}
		else
		{
			m_pStaticUi->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			m_pStaticUi->SetDraw(false);

			GetSoundSource()->SetSoundMax(1.0f);
			GetSoundSource()->SetDelay(900);
			GetSoundSource()->SetSoudLabel(CSound::SOUND_LABEL_SE_DRONE);
		}

		//ランダムでテクスチャ座標を設定する
		float fX = hmd::FloatRandam(0.0f, 1.0f), fY = hmd::FloatRandam(0.0f, 1.0f);

		m_pStaticUi->SetTex(0, D3DXVECTOR2(fX, fY), D3DXVECTOR2(fX + nsDrone::s_fWidth, fY + 1.0f));

		SetPos(pos);
	}
	else
	{
		if (m_pStaticUi)
			m_pStaticUi->SetDraw(false);

		if (m_fSpeedY < 0.0f)
		{
			m_fSpeedY += -nsDrone::s_fGravity * 2.0f;

			if (m_fSpeedY > 0.0f)
				m_fSpeedY = 0.0f;
		}

		if (GetSoundSource()->GetSoundLabel() != CSound::SOUND_LABEL_SE_DRONE)
		{
			m_pStaticUi->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			m_pStaticUi->SetDraw(false);

			GetSoundSource()->SetSoundMax(1.0f);
			GetSoundSource()->SetDelay(900);
			GetSoundSource()->SetSoudLabel(CSound::SOUND_LABEL_SE_DRONE);
		}

		pos.y += m_fSpeedY;
		SetPos(pos);
	}
}
