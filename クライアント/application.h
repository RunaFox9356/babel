//=============================================================================
//
// アプリケーションクラス(application.h)
// Author : 唐�ｱ結斗
// 概要 : 各クラスの呼び出しを行うクラス
//
//=============================================================================
#ifndef _APPLICATION_H_		// このマクロ定義がされてなかったら
#define _APPLICATION_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <Windows.h>
#include "d3dx9.h"							// 描画処理に必要
#include "model_data.h"
//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CRenderer;
class CDebugProc;
class CInput;
class CTexture;
class CCamera;
class CSceneMode;
class CFade;
class CLight;
class CSound;
class CPause;
class CClient;
class CModelData;
class CMapDataManager;
class CPlayer;
class CFont;
class CParticleManager;
class CShaderManager;
class CRanking;
class CReceiveData;

//=============================================================================
// アプリケーションクラス
// Author : 唐�ｱ結斗
// 概要 : 各クラスの呼び出しを行うクラス
//=============================================================================
class CApplication
{
public:

	struct SConnectCheck
	{	
		bool myConnect;
		bool enemyConnect;
	};
	struct SMapList
	{
		int enemyCount;
		int gimmickConnect;

		void ReSetConnect() { enemyCount = 0; gimmickConnect = 0; }
	};
	//--------------------------------------------------------------------
	// シーンモードの列挙型
	// Author : 唐�ｱ結斗
	// 概要 : シーンモードを識別する
	//--------------------------------------------------------------------
	enum SCENE_MODE
	{
		MODE_TITLE = 0,			// タイトル
		MODE_GAME,				// ゲーム
		MODE_AGENT,				// エージェントモード
		MODE_HACKER,			// ハッカーモード
		MODE_RESULT,			// リザルト
		MODE_TUTORIAL,			// チュートリアル
		MODE_DEBUG,				// デバック
		MODE_CONNECT,			// コネクタ
		MODE_SELECT,			// 選択画面
		MODE_MAPSELECT,			// 選択画面
		MODE_AGENT_TUTORIAL,	// エージェントのチュートリアル
		MODE_GAMEOVER,
		MODE_LOADING,	// エージェントのチュートリアル
		MAX_MODE,				// シーンの最大数
		MODE_NONE,				// シーン無し
	};

	
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CApplication* GetInstance();
	HWND GetWnd() { return m_hWnd; }
	CRenderer *GetRenderer() { return m_pRenderer; }								// レンダラーのゲッター
	CInput *GetInput() { return m_pInput; }										// 外部デバイスの取得
	CTexture *GetTexture() { return m_pTexture; }								// テクスチャのゲッター
	CCamera *GetCamera() { return m_pCamera; }									// カメラのゲッター
	CCamera *GetMapCamera() { return m_pMapCamera; }								// マップカメラのゲッター
	CFade *GetFade() { return m_pFade; }											// フェードのゲッター
	CLight *GetLight() { return m_pLight; }										// ライトの取得
	CSound *GetSound() { return m_pSound; }										// サウンドのゲッター
	CPause *GetPause() { return m_pPause; }										// ポーズの取得
	CSceneMode *GetSceneMode() { return m_pSceneMode; }							// シーンモードの取得
	D3DXVECTOR3 ScreenCastWorld(const D3DXVECTOR3 &pos);							// ワールド座標をスクリーン座標にキャストする
	D3DXVECTOR3 WorldCastScreen(const D3DXVECTOR3 &pos);							// ワールド座標をスクリーン座標にキャストする
	float RotNormalization(float fRot);											// 角度の設定
	float RotNormalization(float fRot,float fMin,float fMax);					// 角度の設定
	void SetNextMode(SCENE_MODE mode) { m_nextMode = mode; }						// 次のモードの設定
	void SetMode(SCENE_MODE mode);												// モードの設定
	SCENE_MODE GetMode() { return m_mode; }										// モードの取得
	void SetSelectMode(SCENE_MODE mode) { m_selectMode = mode; };				// 選んだモードの設定
	SCENE_MODE GetSelectMode() { return m_selectMode; }							// 選んだモードの取得
	CClient *GetClient() { return m_Client; }						// 通信クラスの取得
	CMapDataManager* GetMapDataManager() { return m_pMapDataManager; }			// マップデータマネージャーの取得
	CParticleManager* GetParticleManager() { return m_pParticleManager; }
	CShaderManager* GetShaderManager() { return m_pShaderManager; }

	CModelData *GetPlayer(int Num) { return m_Player[Num]; };
	void SetPlayer(int Num, CModelData * player) { m_Player[Num] = player; };

	CReceiveData::SReceiveList GetReceivePlayer() { return m_receive; };
	void SetReceivePlayer(CReceiveData::SReceiveList  player) { m_receive = player; };

	void SetReceivePlayerLog(CReceiveData::SReceiveList  player);

	void SetPlayerLog(CModelData * player);
	
	CPlayer *GetMoveModel() { return m_MoveModel; };
	void SetMoveModel(CPlayer*Player) { m_MoveModel = Player; };
	
	CFont *GetFont() { return m_pFont; };

	CRanking* GetRanking() { return m_pRanking; }			//ランキングマネージャーの取得処理

	bool GetConnect() { return m_IsbConnect; }
	void SetConnect(bool SetConnect) { m_IsbConnect = SetConnect; }
	
	bool GetEnemyConnect() { return m_IsEnemybConnect; }
	void SetEnemyConnect(bool SetConnect) { m_IsEnemybConnect = SetConnect; }

	int GetMap() { return m_map; }
	void SetMap(int map) { m_map = map; }

	//マップの設定してるかどうか
	void SetIsMap(bool data) { m_isMap = data; }												// 当たり判定
	bool GetIsMap() {return m_isMap; }															// 当たり判定

	//モデルの設定
	void SetModelSet(bool data) { m_isModelSet = data; }										// モデルセット
	bool GetModelSet() { return m_isModelSet; }													// モデルセット

	//Loop																		
	void SetLoop(bool data) { m_isLoop = data; }												// Loop設定
	bool GetLoop() { return m_isLoop; }															// Loop設定

	void SetLog(const int data) { m_Log = data; }												// Log
	int GetLog() { return m_Log; }																// Log

	void SetList(SMapList data) { m_list = data; }												// 判定
	SMapList GetList() { return m_list; }														// 判定

	void SetActiveWindowThis(bool data) { m_isActiveWindowThis = data; }						// モデルセット
	bool GetActiveWindowThis() { return m_isActiveWindowThis; }									// モデルセット

	void SetMyPlayer(int data) { m_myPlayer = data; }						// モデルセット
	int GetMyPlayer() { return m_myPlayer; }								// モデルセット
	
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CApplication();
	~CApplication();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init(HINSTANCE hInstance, HWND hWnd);		// 初期化
	void Uninit();										// 終了
	void Update();										// 更新
	void Draw();										// 描画
	SConnectCheck ConnectTh(int Game);
	static void Recv(int Num);

	void LoodIp();
	std::string GetConfigString(const std::string& filePath, const char* pSectionName, const char* pKeyName);
	std::string m_IPaddress;
	std::string m_IPoot;
	

private:
	//--------------------------------------------------------------------
	// 静的メンバ変数
	//--------------------------------------------------------------------
	static CApplication* m_pApplication;

	CDebugProc*			m_pDebugProc;		// デバック表示
	CRenderer*			m_pRenderer;		// rendererクラス
	CInput*				m_pInput;			// 外部デバイスクラス
	CTexture*			m_pTexture;			// テクスチャクラス
	CCamera*			m_pCamera;			// ゲームカメラクラス
	CCamera*			m_pMapCamera;		// マップカメラ
	CSceneMode*			m_pSceneMode;		// シーンモードを格納
	CFade*				m_pFade;			// フェードクラス
	CLight*				m_pLight;			// ライトクラス
	CSound*				m_pSound;			// サウンドクラス
	CPause*				m_pPause;			// ポーズクラス
	CFont*				m_pFont;			// フォントクラス
	CMapDataManager*	m_pMapDataManager;	// マップデータマネージャー
	CParticleManager*	m_pParticleManager; // パーティクルマネージャー
	CShaderManager*		m_pShaderManager;	// シェーダーマネージャー
	CRanking*			m_pRanking;			// ランキングマネージャー
	int					m_nPriority;		// プライオリティ番号
	bool				m_bWireFrame;		// ワイヤーフレームを使うか

	SCENE_MODE m_mode;				// 現在のモードの格納
	SCENE_MODE m_nextMode;			// 次に設定するモード
	SCENE_MODE m_selectMode;		// 次に設定するモード
	HWND	   m_hWnd;				// ウィンドウ

	// 現在の最前面を保存
	HWND m_activeWindowHandle;
	bool m_isActiveWindowThis;

	//サーバーコントローラー
	static const int MAX_PLAYER = 2;
	static const int MAX_LOG = 10;

	CClient *m_Client;
	bool m_IsbConnect;
	bool m_IsEnemybConnect;
	CModelData*m_Player[MAX_PLAYER];
	CReceiveData::SReceiveList m_receive;
	CPlayer *m_MoveModel;					// 
	std::vector<CModelData>m_PlayerLog;

	int		 m_myPlayer;					//自分プレイヤー番号
	bool	 m_isMap;
	bool	 m_isMainSocket;
	bool	 m_isModelSet;
	bool	 m_isLoop;
	int		 m_map;								//map
	int		 m_Log;
	SMapList m_list;
};

#endif

