//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "gauge2D.h"
#include "object.h"
#include "renderer.h"
#include "application.h"

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CGauge2D * CGauge2D::Create(void)
{
	// オブジェクトインスタンス
	CGauge2D *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CGauge2D;

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CGauge2D * CGauge2D::Create(int nPriority)
{
	// オブジェクトインスタンス
	CGauge2D *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CGauge2D(nPriority);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CGauge2D::CGauge2D(int nPriority/* = PRIORITY_LEVEL0*/) : CPolygon2D(nPriority),
m_maxSize(D3DXVECTOR3(0.f,0.f,0.f)),
m_fMaxNumber(0.f),
m_fNumber(0.f),
m_fDestNumber(0.f),
m_fCoefficient(0.f)
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CGauge2D::~CGauge2D()
{
	
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CGauge2D::Update()
{
	AddGauge();
}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CGauge2D::SetVtx()
{
	//頂点情報へのポインタを生成
	VERTEX_2D *pVtx;

	// 情報の取得
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();
	D3DXVECTOR3 size = GetSize();

	//対角線の長さを算出する
	float fLength = sqrtf((size.x * size.x) + ((size.y * 2.0f) * (size.y * 2.0f))) / 2.0f;
	float fLengthOrigin = sqrtf(size.x * size.x) / 2.0f;

	//対角線の角度を算出
	float fAngle = atan2f(size.x, size.y * 2.0f);
	float fAngleOrigin = atan2f(size.x, 0.0f);

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// 頂点情報を設定
	pVtx[0].pos.x = pos.x + sinf(rot.z + (D3DX_PI + fAngle)) * fLength;
	pVtx[0].pos.y = pos.y + cosf(rot.z + (D3DX_PI + fAngle)) * fLength;
	pVtx[0].pos.z = 0.0f;

	pVtx[1].pos.x = pos.x + sinf(rot.z + (D3DX_PI - fAngle)) *  fLength;
	pVtx[1].pos.y = pos.y + cosf(rot.z + (D3DX_PI - fAngle)) *  fLength;
	pVtx[1].pos.z = 0.0f;

	pVtx[2].pos.x = pos.x + sinf(rot.z - (0 + fAngleOrigin)) * fLengthOrigin;
	pVtx[2].pos.y = pos.y + cosf(rot.z - (0 + fAngleOrigin)) * fLengthOrigin;
	pVtx[2].pos.z = 0.0f;

	pVtx[3].pos.x = pos.x + sinf(rot.z - (0 - fAngleOrigin)) *  fLengthOrigin;
	pVtx[3].pos.y = pos.y + cosf(rot.z - (0 - fAngleOrigin)) *  fLengthOrigin;
	pVtx[3].pos.z = 0.0f;

	// rhwの設定
	pVtx[0].rhw = 1.0f;
	pVtx[1].rhw = 1.0f;
	pVtx[2].rhw = 1.0f;
	pVtx[3].rhw = 1.0f;

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// 大きさの最大値の設定
// Author : 唐�ｱ結斗
// 概要 : 大きさの最大値の設定を行う
//=============================================================================
void CGauge2D::SetMaxSize(const D3DXVECTOR3 maxSize)
{
	m_maxSize = maxSize;
	SetSize(maxSize);
}

//=============================================================================
// ゲージの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dゲージを設定する
//=============================================================================
void CGauge2D::SetGauge()
{
	D3DXVECTOR3 size = GetSize();

	size.y = (m_maxSize.y / m_fMaxNumber) * m_fNumber;

	if (size.y <= 0.0f)
	{
		size.y = 0.0f;
	}
	else if (size.y >= m_maxSize.y)
	{
		size.y = m_maxSize.y;
	}

	SetSize(size);
}

//=============================================================================
// 数値の最大値の設定
// Author : 唐�ｱ結斗
// 概要 : 数値の最大値を設定する
//=============================================================================
void CGauge2D::SetMaxNumber(const float fMaxNumber)
{
	m_fMaxNumber = fMaxNumber;

	// ゲージの設定
	SetGauge();
}

//=============================================================================
// ゲージのリセット
// Author : 唐�ｱ結斗
// 概要 : ゲージのリセットを行う
//=============================================================================
void CGauge2D::GaugeReset(const float fMaxNumber)
{
	m_fMaxNumber = fMaxNumber;
	m_fDestNumber = fMaxNumber;
	m_fNumber = fMaxNumber;
}

//=============================================================================
// ゲージの加算
// Author : 唐�ｱ結斗
// 概要 : ゲージの加算管理を行う
//=============================================================================
void CGauge2D::AddGauge()
{
	if (m_fDestNumber != m_fNumber)
	{// 数値の設定
		float fAdd = (m_fDestNumber - m_fNumber) * m_fCoefficient;

		m_fNumber += fAdd;

		if (fAdd > 0)
		{
			if (m_fNumber >= m_fDestNumber)
			{
				m_fNumber = m_fDestNumber;
			}
		}
		else if (fAdd < 0)
		{
			if (m_fNumber <= m_fDestNumber)
			{
				m_fNumber = m_fDestNumber;
			}
		}

		// ゲージの設定
		SetGauge();
	}
}
