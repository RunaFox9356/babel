//=============================================================================
//
// taskUI.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "taskUI.h"
#include "text.h"
#include "words.h"
#include "game.h"
#include "hacker.h"
#include "application.h"
#include <vector>

//=============================================================================
//									静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CTaskUI::DEFAULT_POS = { 100.0f, 100.0f, 0.0f };				//ディフォルトの位置
const D3DXVECTOR3	CTaskUI::DEFAULT_SIZE = { 360.0f, 200.0f, 0.0f };				//ディフォルトのサイズ
const D3DXCOLOR		CTaskUI::DEFAULT_COLOR = { 1.0f, 0.0f, 0.0f, 0.4f};			//ディフォルトの色
const int			CTaskUI::DEFAULT_TEXTURE = -1;								//ディフォルトのテクスチャ

const D3DXCOLOR		CTaskUI::DEFAULT_TEXT_COLOR = { 0.0f, 1.0f, 1.0f, 0.5f };		//ディフォルトテキストの色
const D3DXVECTOR3	CTaskUI::DEFAULT_LETTER_SIZE = { 10.0f, 10.0f, 10.0f };		//ディフォルト文字のサイズ
const D3DXVECTOR3	CTaskUI::DEFAULT_TEXT_POS = { -105.0f, -60.0f, 0.0f };			//ディフォルト文字の相対位置
const D3DXVECTOR3	CTaskUI::DEFAULT_NUMBER_POS = { 175.0f, 0.0f, 0.0f };			//ディフォルト数値の相対位置

//ディフォルトのテキスト
const char			CTaskUI::DEFAULT_AGENT_TEXT[CTask::AGENT_TASK_MAX][TEXT_MAX_CHARACTER] =
{
	"RESCUE",
	"DESTROY",
	"KILL",
	"STEAL"
};

const char			CTaskUI::DEFAULT_HACKER_TEXT[CTask::HACKER_TASK_MAX][TEXT_MAX_CHARACTER] =
{
	"HACK",
	"PRISONER",
	"DESTROY",
	"ENEMY"
};



//コンストラクタ
CTaskUI::CTaskUI() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL4),
m_textPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_textCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
m_bText(false)
{
	for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
	{
		m_pText[nCnt] = nullptr;
		m_pNum[nCnt] = nullptr;
	}

	for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
	{
		m_pHackerText[nCnt] = nullptr;
		m_pHackerNum[nCnt] = nullptr;
	}
}

//デストラクタ
CTaskUI::~CTaskUI()
{

}

// 初期化
HRESULT CTaskUI::Init()
{
	//基本クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	CPolygon2D::SetPos(DEFAULT_POS);		//ディフォルトの位置の設定
	CPolygon2D::SetSize(DEFAULT_SIZE);		//ディフォルトのサイズの設定
	CPolygon2D::SetColor(DEFAULT_COLOR);	//ディフォルトの色の設定
	std::vector<int> nNumTex;
	nNumTex.push_back(DEFAULT_TEXTURE);
	CPolygon2D::LoadTex(nNumTex);	//ディフォルトのテクスチャの設定
	m_textCol = DEFAULT_TEXT_COLOR;			//ディフォルトのテキストの色の設定

	return S_OK;
}

// 終了
void CTaskUI::Uninit()
{
	for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
	{
		//テキストの破棄
		if (m_pText[nCnt])
		{
			//m_pText[nCnt]->Release();
			m_pText[nCnt] = nullptr;
		}
		if (m_pNum[nCnt])
		{
			m_pNum[nCnt]->Uninit();
			m_pNum[nCnt] = nullptr;
		}
	}

	for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
	{
		//テキストの破棄
		if (m_pHackerText[nCnt])
		{
			//m_pText[nCnt]->Release();
			m_pHackerText[nCnt] = nullptr;
		}
		if (m_pHackerNum[nCnt])
		{
			m_pHackerNum[nCnt]->Uninit();
			m_pHackerNum[nCnt] = nullptr;
		}
	}

	//基本クラスの終了処理
	CPolygon2D::Uninit();
}

// 更新
void CTaskUI::Update()
{
	//基本クラスの更新処理
	CPolygon2D::Update();
}

// 描画
void CTaskUI::Draw()
{
	//基本クラスの描画処理
	CPolygon2D::Draw();
}

//タスクのテキストの更新
void CTaskUI::UpdateTask(CTask::AGENT_TASK_TYPE type)
{
	//テキストの終了処理
	if (m_pNum[type])
	{
		m_pNum[type]->Uninit();
		m_pNum[type] = nullptr;
	}

	if (!m_pNum[type])
	{//nullチェック

		CTask* pTask = CGame::GetTask();			//タスクマネージャーの取得
		D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * (int)type, 0.0f);
		D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + DEFAULT_NUMBER_POS + delta;	//テキストの位置の設定
		int nMax = 0, nNum = 0;						//タスクの目的の現在の数と最大数
		std::string str;					

		if (pTask)
		{//タスクマネージャーを取得出来たら

			nMax = pTask->GetTaskObjectiveMax(type);			//タスクの目的の最大数
			nNum = nMax - pTask->GetTaskObjective(type);		//タスクの残っている目的
		}

		str = std::to_string(nNum) + "/" + std::to_string(nMax);	//ストリングの設定

		//テキストの生成
		m_pNum[type] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 3, str.c_str(), DEFAULT_LETTER_SIZE, m_textCol);
	}
}

//ハッカーのタスクのテキストの更新
void CTaskUI::UpdateTask(CTask::HACKER_TASK_TYPE type)
{
	//テキストの終了処理
	if (m_pHackerNum[type])
	{
		m_pHackerNum[type]->Uninit();
		m_pHackerNum[type] = nullptr;
	}

	if (!m_pHackerNum[type])
	{//nullチェック

		CTask* pTask = CHacker::GetTask();

		D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * (int)type, 0.0f);
		D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + DEFAULT_NUMBER_POS + delta;	//テキストの位置の設定
		int nMax = 0, nNum = 0;						//タスクの目的の現在の数と最大数
		std::string str;

		if (pTask)
		{//タスクマネージャーを取得出来たら

			nMax = pTask->GetTaskObjectiveMax(type);			//タスクの目的の最大数
			nNum = nMax - pTask->GetTaskObjective(type);		//タスクの残っている目的
		}

		str = std::to_string(nNum) + "/" + std::to_string(nMax);	//ストリングの設定

																	//テキストの生成
		m_pHackerNum[type] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 3, str.c_str(), DEFAULT_LETTER_SIZE, m_textCol);
	}
}

//テキストの生成
void CTaskUI::CreateText(CTask* pTask)
{
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
	{
		CreateAgentUI(pTask);
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		CreateHackerUI(pTask);
	}
}

//生成
CTaskUI * CTaskUI::Create()
{
	CTaskUI* pUi = new CTaskUI;		//インスタンスを生成する

	if (FAILED(pUi->Init()))
	{//初期化処理
		return nullptr;
	}

	return pUi;						//生成したインスタンスを返す
}

//エージェントのUIの生成
void CTaskUI::CreateAgentUI(CTask* pTask)
{
	for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
	{
		if (!m_pText[nCnt])
		{//nullチェック
			D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * nCnt, 0.0f);
			D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + delta;

			//テキストの生成
			m_pText[nCnt] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 1, DEFAULT_AGENT_TEXT[nCnt], DEFAULT_LETTER_SIZE, m_textCol);
		}
		if (!m_pNum[nCnt])
		{//nullチェック

			D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * nCnt, 0.0f);
			D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + DEFAULT_NUMBER_POS + delta;	//テキストの位置の設定
			int* pMax = nullptr;		//タスクの目的の最大数（全部）へのポインタ
			int nMax = 0;				//タスクの残っている目的
			std::string str;

			if (pTask)
			{//nullチェック
				pMax = pTask->GetAllTaskObjectiveMax();	//タスクの目的の最大数（全部）を取得する
			}
			if (pMax)
				nMax = pMax[nCnt];						//タスクの目的の最大数の設定

			str = "0/" + std::to_string(nMax);			//ストリングの設定

														//テキストの生成
			m_pNum[nCnt] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 3, str.c_str(), DEFAULT_LETTER_SIZE, m_textCol);
		}
	}

	m_bText = true;		//テキストを更新できるようにする
}

//ハッカーのUIの生成
void CTaskUI::CreateHackerUI(CTask* pTask)
{
	for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
	{
		if (!m_pHackerText[nCnt])
		{//nullチェック
			D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * nCnt, 0.0f);
			D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + delta;

			//テキストの生成
			m_pHackerText[nCnt] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 1, DEFAULT_HACKER_TEXT[nCnt], DEFAULT_LETTER_SIZE, m_textCol);
		}
		if (!m_pHackerNum[nCnt])
		{//nullチェック

			D3DXVECTOR3 delta = D3DXVECTOR3(0.0f, DEFAULT_LETTER_SIZE.y * 4.0f * nCnt, 0.0f);
			D3DXVECTOR3 pos = GetPos() + DEFAULT_TEXT_POS + DEFAULT_NUMBER_POS + delta;	//テキストの位置の設定
			int* pMax = nullptr;		//タスクの目的の最大数（全部）へのポインタ
			int nMax = 0;				//タスクの残っている目的
			std::string str;

			if (pTask)
			{//nullチェック
				pMax = pTask->GetAllHackerTaskObjectiveMax();	//タスクの目的の最大数（全部）を取得する
			}
			if (pMax)
				nMax = pMax[nCnt];						//タスクの目的の最大数の設定

			str = "0/" + std::to_string(nMax);			//ストリングの設定

														//テキストの生成
			m_pHackerNum[nCnt] = CText::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 3, str.c_str(), DEFAULT_LETTER_SIZE, m_textCol);
		}
	}

	m_bText = true;		//テキストを更新できるようにする
}
