//============================
//
// TEXT
// Author:hamada ryuuga
//
//============================

#ifndef _TEXT_H_
#define _TEXT_H_

#include "polygon2D.h"
#include "words.h"

class CText : public CPolygon2D
{
public:

	enum Type
	{
		GON = 0,
		MASUO,
		MAX
	};

	CText(int list);
	~CText();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CText* Create(D3DXVECTOR3 setPos, D3DXVECTOR3 SetSize, Type talkType, int DeleteTime, int SpeedText, const char * Text, D3DXVECTOR3 fontSize = D3DXVECTOR3(20.0f, 20.0f, 20.0f), D3DXCOLOR col = D3DXCOLOR(1.0f, 1.0f, 1.0f,1.0f),CFont::FONT Type = CFont::FONT_GON, int wordsPopCountX = 0,  bool Nottimerdelete = false);
	
	void Releasetimer(int nTimar);
	void CText::Setfunc(std::function<void(void) > func);

	void SetCountXfast(int fast) { m_wordsPopCountXfast = fast; m_wordsPopCountX = m_wordsPopCountXfast; };
	void SetRelease(bool Nottimerdelete) { m_isRelease = Nottimerdelete; };

	void SetFontSize(D3DXVECTOR3 fontSize) { m_fontSize = fontSize;};
	D3DXVECTOR3 GetFontSize() {return m_fontSize; };
	void SetCol(D3DXCOLOR col) { m_col = col; };
	void SetTextCol(D3DXCOLOR col);

	static void SetDefaultFont(CFont::FONT type) { m_font = type; }		//ディフォルトのフォントのセッター
	void TexChange(const char * Text);
private:

	static CFont::FONT	m_font;			//ディフォルトのフォント

	D3DXCOLOR m_col;
	int m_DesTimarMax;//消える最大時間
	int m_DesTimar;//消える時間

	int m_TextSize;//文字のサイズ
	int m_Addnumber;//1つの文字が加算されるまでの時間
	int m_AddCount;//文字が加算されるまでのカウント
	int m_AddLetter;//何文字目入れるか指定
	int m_wordsPopCount;//文字を出した最大数
	int m_wordsPopCountX;//横に文字を出した数
	int m_newlineCount;//改行の数
	int m_wordsPopCountXfast;
	bool m_isRelease;

	LPD3DXFONT m_pFont = nullptr;
	D3DXVECTOR3 m_fontSize;
	std::string m_Text;
	std::string m_ALLText;
	CFont::FONT m_FontType;
	std::vector<CWords*> m_words;
	std::function<void(void)> m_func;
	void TextLetter(const char * Text, int SpeedText);
	void WordList();
	void SetFont(CFont::FONT FontType) { m_FontType = FontType; };
};
#endif
