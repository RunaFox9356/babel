//=============================================================================
//
// タイマークラス(Timer.h)
// Author : 唐�ｱ結斗
// 概要 : タイマー生成を行う
//
//=============================================================================
#ifndef _TIMER_H_		// このマクロ定義がされてなかったら
#define _TIMER_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include "polygon2D.h"
#include "number.h"
#include "manager.h"
//=============================================================================
// 2Dポリゴンクラス
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴン生成を行うクラス
//=============================================================================
class  CTimer : public CPolygon2D
{
private:
	enum ETime
	{
		//タイマー
		TIME_60,		//60進数(分・時間刻み)
		TIME_10
	};
	static const int TIMERMAX = 2;
public:

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CTimer *Create(int nPriority);	// 2Dポリゴンの生成(オーバーロード)

//--------------------------------------------------------------------
// コンストラクタとデストラクタ
//--------------------------------------------------------------------
	explicit CTimer(int nPriority = PRIORITY_LEVEL0);
	~CTimer() override;

	//--------------------------------------------------------------------
	// オーバーライド関数
	//--------------------------------------------------------------------
	HRESULT Init() override;																	// 初期化
	void Uninit() override;																		// 終了
	void Update() override;																		// 更新
	void Draw() override;																		// 色の設定

	void SetPos(const D3DXVECTOR3& pos) override;												//位置のセッター

	void TimeCount();																			// タイマーカウント用
	void TimeCount60();																			// 分秒刻み
	void SetTimer(int time) { m_nTimer = time; SetDraw(true); };								//	タイマーセット
	int GetTimer() { return m_nTimer; };														//　タイマー取得
	void SetMode(int mode);
	void SetDigitSize(const D3DXVECTOR3 digitSize);												//　桁のサイズのセッター
	void SetDraw(const bool bDraw) override;													//描画フラグのセッター
private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	int m_nTimer;		//タイマー
	int m_nMinutes;
	int m_nSecond;
	CNumber* m_pNumber[2];
	ETime m_Mode;	
};
#endif




