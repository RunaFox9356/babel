//=============================================================================
//
// resultScoreUI.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "resultScoreUI.h"
#include "renderer.h"
#include "task.h"
#include "text.h"
#include "input.h"
#include "Score.h"
#include "application.h"
#include "ranking.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3	CResultScoreUI::DEFAULT_GB_POS = { (float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f };				//ディフォルトの背景の位置
const D3DXVECTOR3	CResultScoreUI::DEFAULT_BG_SIZE = { 800.0f, (float)CRenderer::SCREEN_HEIGHT, 0.0f };	//ディフォルトの背景のサイズ
const D3DXCOLOR		CResultScoreUI::DEFAULT_BG_COLOR = { 0.0f, 0.0f, 0.0f, 0.5f };						//ディフォルトの背景の色


namespace
{
	char aAgentText[CTask::AGENT_TASK_MAX][64]  = { "Prisoner Rescued : ", "Target Destroyed : ", "Enemies Killed : ", "Object Stolen : " };
	char aHackerText[CTask::HACKER_TASK_MAX][64] = { "PC Hacked : ", "Prisoner Found : ", "Object To Destroy Found : ", "Enemies Found : " };
};




//コンストラクタ
CResultScoreUI::CResultScoreUI() : CPolygon2D(CSuper::PRIORITY_LEVEL3),
m_nCntAnim(0),
m_pFirstText(nullptr),
m_pSecondText(nullptr),
m_bAgent(true),
m_bEnd(false)
{
	m_vCompletedTask.clear();
	m_vCompletedTask.shrink_to_fit();
}

//デストラクタ
CResultScoreUI::~CResultScoreUI()
{

}

//初期化
HRESULT CResultScoreUI::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	SetPos(DEFAULT_GB_POS);				//ディフォルトの背景の位置の設定
	SetSize(DEFAULT_BG_SIZE);			//ディフォルトの背景のサイズの設定
	SetColor(DEFAULT_BG_COLOR);			//ディフォルトの背景の色の設定

	return S_OK;
}

//終了
void CResultScoreUI::Uninit()
{
	//テキストの破棄
	if (m_pFirstText)
	{
		m_pFirstText->Uninit();
		m_pFirstText = nullptr;
	}
	if (m_pSecondText)
	{
		m_pSecondText->Uninit();
		m_pSecondText = nullptr;
	}

	//達成したタスクのベクトルのクリア処理
	m_vCompletedTask.clear();
	m_vCompletedTask.shrink_to_fit();

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

//更新
void CResultScoreUI::Update()
{
	CInput* pInput = CApplication::GetInstance()->GetInput();
	m_nCntAnim++;

	if (m_nCntAnim == 90)
	{
		std::string str;

		if (m_bAgent)
		{
			for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
			{
				str += aAgentText[nCnt];
				str += std::to_string(m_vCompletedTask[nCnt] * CScore::DEFAULT_AGENT_TASK_SCORE[nCnt]);
				str += "\n\n";
			}
		}
		else
		{
			for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
			{
				str += aHackerText[nCnt];
				str += std::to_string(m_vCompletedTask[nCnt] * CScore::DEFAULT_HACKER_TASK_SCORE[nCnt]);
				str += "\n\n";
			}
		}

		{
			D3DXVECTOR3 fontSize = D3DXVECTOR3(15.0f, 15.0f, 15.0f);							//フォントサイズ
			D3DXVECTOR3 P = D3DXVECTOR3(250.0f, 150.0f, 0.0f);									//絶対位置の設定
			m_pFirstText = CText::Create(P, fontSize, CText::MAX, 1000, 3, str.c_str(), fontSize, D3DXCOLOR(1.0f, 1.0f, 0.25f, 1.0f));	//テキストの生成
		}
	}
	else if (m_nCntAnim == 360)
	{
		std::string str;

		int nScore = 0, nPartialScore = 0, nRank = 0;
		CApplication::GetInstance()->GetRanking()->GetLastScoreAndRank(nScore, nRank);

		if (m_bAgent)
		{
			for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
			{
				nPartialScore += m_vCompletedTask[nCnt] * CScore::DEFAULT_AGENT_TASK_SCORE[nCnt];
			}
		}
		else
		{
			for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
			{
				nPartialScore += m_vCompletedTask[nCnt] * CScore::DEFAULT_HACKER_TASK_SCORE[nCnt];
			}
		}

		if (m_bAgent)
			str += "Hacker Total Score : ";
		else
			str += "Agent Total Score : ";

		str += std::to_string(nScore - nPartialScore);

		str += "\n\n\n\n\n\n";
		str += "Total : ";
		str += std::to_string(nScore);

		{
			D3DXVECTOR3 fontSize = D3DXVECTOR3(15.0f, 15.0f, 15.0f);							//フォントサイズ
			D3DXVECTOR3 P = D3DXVECTOR3(250.0f, 400.0f, 0.0f);									//絶対位置の設定
			m_pSecondText = CText::Create(P, fontSize, CText::MAX, 1000, 3, str.c_str(), fontSize, D3DXCOLOR(1.0f, 1.0f, 0.25f, 1.0f));	//テキストの生成
		}
	}
	else if (m_nCntAnim >= 1200 || (pInput && (pInput->Trigger(DIK_RETURN) || pInput->Trigger(DIK_SPACE))))
	{
		m_bEnd = true;
	}

	//親クラスの更新処理
	CPolygon2D::Update();
}

//描画
void CResultScoreUI::Draw()
{
	//親クラスの描画処理
	CPolygon2D::Draw();
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
CResultScoreUI* CResultScoreUI::Create(const bool bAgent, std::vector<int> vCompletedTask)
{
	CResultScoreUI* pObj = new CResultScoreUI;			//新しいインスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->m_bAgent = bAgent;
	pObj->m_vCompletedTask = vCompletedTask;

	return pObj;				//生成したインスタンスを返す
}