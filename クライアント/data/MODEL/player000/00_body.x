xof 0302txt 0064
template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
 <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
 <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
 <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
 <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template IndexedColor {
 <1630B820-7842-11cf-8F52-0040333594A3>
 DWORD index;
 ColorRGBA indexColor;
}

template Boolean {
 <4885AE61-78E8-11cf-8F52-0040333594A3>
 WORD truefalse;
}

template Boolean2d {
 <4885AE63-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template MaterialWrap {
 <4885AE60-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshFaceWraps {
 <4885AE62-78E8-11cf-8F52-0040333594A3>
 DWORD nFaceWrapValues;
 Boolean2d faceWrapValues;
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template MeshVertexColors {
 <1630B821-7842-11cf-8F52-0040333594A3>
 DWORD nVertexColors;
 array IndexedColor vertexColors[nVertexColors];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

Header{
1;
0;
1;
}

Mesh {
 25;
 -23.49546;1.94886;-7.65789;,
 23.49546;1.94886;-7.65789;,
 23.49546;1.94886;7.33845;,
 -23.49546;1.94886;7.33845;,
 -23.36379;72.33138;-7.65789;,
 -23.49546;72.33138;-7.65789;,
 -23.49546;72.33138;-3.74616;,
 23.49546;72.33138;7.33845;,
 23.49546;72.33138;-7.65789;,
 23.49546;27.17163;7.33845;,
 23.49546;25.13652;-7.65789;,
 -23.49546;72.33138;-3.48996;,
 -23.49546;72.32169;-3.56175;,
 -23.49546;71.74854;7.33845;,
 -23.49546;72.33138;7.33845;,
 -23.49546;71.76567;-7.65789;,
 -22.01466;72.33138;7.33845;,
 -23.49546;28.25955;7.33845;,
 -23.49546;28.10643;-2.03799;,
 -23.49546;28.01466;-7.65789;,
 -0.76032;49.20555;-7.65789;,
 23.49546;71.81379;-7.65789;,
 23.49546;72.05865;7.33845;,
 0.16998;50.31753;7.33845;,
 -23.49546;28.25955;7.33845;;
 
 28;
 4;0,1,2,3;,
 3;4,5,6;,
 3;4,7,8;,
 4;9,2,1,10;,
 3;9,3,2;,
 3;11,6,12;,
 4;11,12,13,14;,
 4;12,6,5,15;,
 3;11,14,16;,
 4;11,16,7,4;,
 3;11,4,6;,
 3;17,13,18;,
 4;17,18,0,3;,
 3;19,15,20;,
 4;19,20,10,1;,
 3;19,1,0;,
 3;21,10,20;,
 4;21,20,15,5;,
 3;21,5,8;,
 4;21,8,7,22;,
 4;21,22,9,10;,
 3;23,9,22;,
 4;23,22,7,16;,
 4;23,16,14,24;,
 4;23,24,3,9;,
 3;19,0,18;,
 4;19,18,13,12;,
 3;19,12,15;;
 
 MeshMaterialList {
  2;
  28;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;;
  Material {
   0.800000;0.301176;0.737255;1.000000;;
   5.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
  }
  Material {
   0.800000;0.800000;0.800000;1.000000;;
   5.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
  }
 }
 MeshNormals {
  7;
  0.000000;1.000000;0.000000;,
  0.000000;-1.000000;-0.000000;,
  -1.000000;0.000000;0.000000;,
  1.000000;0.000000;0.000000;,
  0.000000;0.000000;-1.000000;,
  0.000000;0.000000;1.000000;,
  0.000000;0.000000;-1.000000;;
  28;
  4;1,1,1,1;,
  3;0,0,0;,
  3;0,0,0;,
  4;3,3,3,3;,
  3;5,5,5;,
  3;2,2,2;,
  4;2,2,2,2;,
  4;2,2,2,2;,
  3;0,0,0;,
  4;0,0,0,0;,
  3;0,0,0;,
  3;2,2,2;,
  4;2,2,2,2;,
  3;4,4,4;,
  4;4,4,4,4;,
  3;4,4,4;,
  3;4,4,4;,
  4;4,4,4,4;,
  3;4,4,6;,
  4;3,3,3,3;,
  4;3,3,3,3;,
  3;5,5,5;,
  4;5,5,5,5;,
  4;5,5,5,5;,
  4;5,5,5,5;,
  3;2,2,2;,
  4;2,2,2,2;,
  3;2,2,2;;
 }
}
