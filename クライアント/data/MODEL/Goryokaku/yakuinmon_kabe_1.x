xof 0302txt 0064
template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
 <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
 <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
 <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
 <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template IndexedColor {
 <1630B820-7842-11cf-8F52-0040333594A3>
 DWORD index;
 ColorRGBA indexColor;
}

template Boolean {
 <4885AE61-78E8-11cf-8F52-0040333594A3>
 WORD truefalse;
}

template Boolean2d {
 <4885AE63-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template MaterialWrap {
 <4885AE60-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshFaceWraps {
 <4885AE62-78E8-11cf-8F52-0040333594A3>
 DWORD nFaceWrapValues;
 Boolean2d faceWrapValues;
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template MeshVertexColors {
 <1630B821-7842-11cf-8F52-0040333594A3>
 DWORD nVertexColors;
 array IndexedColor vertexColors[nVertexColors];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

Header{
1;
0;
1;
}

Mesh {
 20;
 -250.37927;175.49857;-54.42422;,
 250.31458;175.49857;-54.42422;,
 250.31458;0.00876;-54.42422;,
 -250.37927;0.00876;-54.42422;,
 250.31458;175.49857;54.42422;,
 250.31458;0.00876;54.42422;,
 -250.37927;175.49857;54.42422;,
 -250.37927;0.00876;54.42422;,
 -249.50876;256.96481;-0.00255;,
 249.44403;256.96481;-0.00255;,
 250.78030;212.18280;-30.67240;,
 -250.26434;212.18280;-30.67240;,
 250.37453;175.00000;-74.89850;,
 -250.14886;175.00000;-74.89850;,
 249.44403;256.96481;0.00255;,
 250.78030;212.18280;30.67240;,
 250.37453;175.00000;74.89850;,
 -249.50876;256.96481;0.00255;,
 -250.26434;212.18280;30.67240;,
 -250.14886;175.00000;74.89850;;
 
 16;
 4;0,1,2,3;,
 4;1,4,5,2;,
 4;4,6,7,5;,
 4;6,0,3,7;,
 4;6,4,1,0;,
 4;3,2,5,7;,
 4;8,9,10,11;,
 4;11,10,12,13;,
 4;9,14,15,10;,
 4;10,15,16,12;,
 4;14,17,18,15;,
 4;15,18,19,16;,
 4;17,8,11,18;,
 4;18,11,13,19;,
 4;17,14,9,8;,
 4;13,12,16,19;;
 
 MeshMaterialList {
  3;
  16;
  0,
  0,
  0,
  0,
  0,
  0,
  2,
  2,
  2,
  2,
  2,
  2,
  2,
  2,
  2,
  2;;
  Material {
   0.552000;0.552000;0.552000;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
   TextureFilename {
    "data/TEXTURE/flagstone.jpg";
   }
  }
  Material {
   0.432800;0.432800;0.432800;1.000000;;
   0.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
  }
  Material {
   0.800000;0.800000;0.800000;1.000000;;
   5.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
   TextureFilename {
    "data/TEXTURE/yakuinmon_parts/kawara.jpg";
   }
  }
 }
 MeshNormals {
  19;
  0.000000;0.000000;-1.000000;,
  1.000000;0.000000;0.000000;,
  0.000000;0.000000;1.000000;,
  -1.000000;0.000000;0.000000;,
  0.000000;1.000000;0.000000;,
  0.000000;-1.000000;-0.000000;,
  0.000000;0.884606;-0.466340;,
  0.000000;0.671403;-0.741093;,
  0.000000;0.765425;-0.643526;,
  0.999555;0.029826;0.000000;,
  0.999955;0.009459;0.000000;,
  0.999941;-0.010912;-0.000000;,
  0.000000;0.884606;0.466340;,
  0.000000;0.671403;0.741093;,
  0.000000;0.765425;0.643526;,
  -0.999858;0.016870;0.000000;,
  -0.999976;0.006883;0.000000;,
  -0.999995;-0.003106;0.000000;,
  0.000000;-1.000000;-0.000000;;
  16;
  4;0,0,0,0;,
  4;1,1,1,1;,
  4;2,2,2,2;,
  4;3,3,3,3;,
  4;4,4,4,4;,
  4;5,5,5,5;,
  4;6,6,7,7;,
  4;7,7,8,8;,
  4;9,9,10,10;,
  4;10,10,11,11;,
  4;12,12,13,13;,
  4;13,13,14,14;,
  4;15,15,16,16;,
  4;16,16,17,17;,
  4;12,12,6,6;,
  4;18,18,18,18;;
 }
 MeshTextureCoords {
  20;
  -0.751879;1.504799;,
  1.751565;1.504799;,
  1.751565;0.523276;,
  -0.751879;0.523276;,
  1.751565;1.435436;,
  1.751565;0.453913;,
  -0.751879;1.435436;,
  -0.751879;0.453913;,
  -1.995088;-0.933658;,
  2.994440;-0.933658;,
  3.007803;-0.683811;,
  -2.002643;-0.683811;,
  3.003745;-0.476360;,
  -2.001489;-0.476360;,
  2.994440;-0.933658;,
  3.007803;-0.683811;,
  3.003745;-0.476360;,
  -1.995088;-0.933658;,
  -2.002643;-0.683811;,
  -2.001489;-0.476360;;
 }
}
