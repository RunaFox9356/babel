bool isUseTex;
bool useSubTex;
int renderTime;

float2 screenDimensions = float2(1280, 720);
sampler textureSampler : register(s0);	// pDevice->SetTexture()Éçê½eNX`
sampler multiTextureSamplerA : register(s1);

// -------------------------------------------------------------
// ¸_VF[_©çsNZVF[_Én·f[^
// -------------------------------------------------------------
struct VS_OUTPUT
{
	float4 Pos			: SV_POSITION;			// Êu
	float4 Color		: COLOR0;			// F
	float2 Tex			: TEXCOORD0;		// eNX`
};

//
// Gg|Cg
//
float4 main(float4 pos : POSITION) : SV_POSITION
{
	return pos;
}

//
// ÊíÌ¸_VF[_[
//
VS_OUTPUT VS(
	float4 Pos    : POSITION0,	// [JÊuÀW
	float Rhw : RHW,		// @üxNg
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// eNX`Ì@üxNg
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// oÍf[^

	// ÀWÏ·
	float4 pos = Pos;
	Out.Pos = pos;

	Out.Tex = Tex;
	Out.Color = Color;
	return Out;
}

//
// ÊíÌsNZVF[_[
//
float4 PS(VS_OUTPUT In) : COLOR0
{
	float4 result;

	// eNX`TvO
	float4 texColor = tex2D(textureSampler, In.Tex);
	result = isUseTex ? (In.Color * texColor) : In.Color;

	float4 subTexColor = tex2D(multiTextureSamplerA, In.Tex);
	result *= useSubTex ? subTexColor : float4(1.0f, 1.0f, 1.0f, 1.0f);

	return result;
}

//
// mNÌsNZVF[_[
//
float4 PS_Monochrome(VS_OUTPUT In) : COLOR0
{
	float4 result;

	// eNX`TvO
	float4 texColor = tex2D(textureSampler, In.Tex);
	result = isUseTex ? (In.Color * texColor) : In.Color;

	float4 subTexColor = tex2D(multiTextureSamplerA, In.Tex);
	result *= useSubTex ? subTexColor : float4(1.0f, 1.0f, 1.0f, 1.0f);

	// rgbðO[XP[ÉÏ·
	float lumina = dot(result.rgb, float3(0.4f, 0.7f, 0.3f));
	float4 monochrome = float4(lumina, lumina, lumina, result.a);

	return monochrome;
}

//
// u[ÌsNZVF[_[
//
float4 PS_Blur(VS_OUTPUT In) : COLOR0
{
	float4 blurColor = (float4)0;
	const float radius = 1.0f;
	const float strong = 0.0085f;
	float4 orgColor = tex2D(textureSampler, In.Tex);

	// ¡ûüÌu[
	for (float i = -radius; i <= radius; i++)
	{
		float2 offset = float2(i, 0) * strong;
		blurColor += tex2D(textureSampler, In.Tex + offset);
	}

	// cûüÌu[
	for (float i = -radius; i <= radius; i++)
	{
		float2 offset = float2(0, i) * strong;
		blurColor += tex2D(textureSampler, In.Tex + offset);
	}

	blurColor = (blurColor / ((radius * 2 + 1))) * float4(In.Color.rgb, 1.0f);
	return blurColor;
}

//
// eNjbN
//
technique TechShader
{
	pass P0
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	}
	pass P1
	{
		VertexShader = NULL;
		PixelShader = compile ps_2_0 PS_Blur();
	}
	pass P2
	{
		VertexShader = NULL;
		PixelShader = compile ps_2_0 PS_Monochrome();
	}
}