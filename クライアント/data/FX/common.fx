// ���[���h�r���[�v���W�F�N�V�����̍\����
struct ViewProjection
{
	float4x4 mtxWorld;
	float4x4 cameraView;
	float4x4 cameraProjection;
};
ViewProjection vp;

struct MtxWorld
{
	float3 pos;
	float3 rot;
	float3 scale;
};
MtxWorld mtxWorld;

sampler textureSampler : register(s0);		// ���C���̃e�N�X�`���T���v���[

struct VS_INPUT
{	// ���_�V�F�[�_�[���͗p
	float4 pos : POSITION;
	float3 nor : NORMAL;
	float4 col : COLOR0;
	float2 texCoord : TEXCOORD0;
};
struct VS_OUTPUT
{	// ���_�V�F�[�_�[�o�͗p
	float4 pos : SV_POSITION;
	float4 col : COLOR0;
	float2 texCoord : TEXCOORD0;
};

//��������������������������������������������������������������
// �P�ʍs��
//��������������������������������������������������������������
float4x4 MatrixIdentity(float4x4 mtxWorld)
{
	return mtxWorld = float4x4( 1.0f, 0.0f, 0.0f, 0.0f,
				 0.0f, 1.0f, 0.0f, 0.0f,
				 0.0f, 0.0f, 1.0f, 0.0f,
				 0.0f, 0.0f, 0.0f, 1.0f );
}

//��������������������������������������������������������������
// �}�g���b�N�X�Ɋg�k�𔽉f
//��������������������������������������������������������������
float4x4 MatrixScaling(float3 scale)
{
	float4x4 scaling;
	scaling[0] = float4(scale.x, 0.0f, 0.0f, 0.0f);
	scaling[1] = float4(0.0f, scale.y, 0.0f, 0.0f);
	scaling[2] = float4(0.0f, 0.0f, scale.z, 0.0f);
	scaling[3] = float4(0.0f, 0.0f, 0.0f, 1.0f);

	return scaling;
}

//��������������������������������������������������������������
// �}�g���b�N�X�Ɍ����𔽉f
//��������������������������������������������������������������
float4x4 MatrixRotation(float3 rot)
{
	float cy = cos(rot.x);
	float cp = cos(rot.y);
	float cr = cos(rot.z);
	float sy = sin(rot.x);
	float sp = sin(rot.y);
	float sr = sin(rot.z);

	float4x4 rotate;
	rotate[0] = float4(cp * cy, cp * sy, -sp, 0.0f);
	rotate[1] = float4(cy * sp * sr - cr * sy, sy * sp * sr + cr * cy, cp * sr, 0.0f);
	rotate[2] = float4(cy * sp * cr + sr * sy, cr * cy * sp - sr * sy, cp * cr, 0.0f);
	rotate[3] = float4(0.0f, 0.0f, 0.0f, 1.0f);

	return rotate;
}

//��������������������������������������������������������������
// �}�g���b�N�X�Ɉʒu�𔽉f
//��������������������������������������������������������������
float4x4 MatrixTrans(float3 pos)
{
	float4x4 translation;
	translation[0] = float4(1.0f, 0.0f, 0.0f, 0.0f);
	translation[1] = float4(0.0f, 1.0f, 0.0f, 0.0f);
	translation[2] = float4(0.0f, 0.0f, 1.0f, 0.0f);
	translation[3] = float4(pos.xyz, 1.0f);

	return translation;
}

//��������������������������������������������������������������
// ���_���ˉe��Ԃֈړ�
//��������������������������������������������������������������
float4 TransVertex(float4 vtx/*, float4x4 mtxWorld*/)
{
	vtx = mul(vtx, vp.mtxWorld);
	vtx = mul(vtx, vp.cameraView);
	vtx = mul(vtx, vp.cameraProjection);
	return vtx;
}

//��������������������������������������������������������������
// �e�N�X�`�����W���K��
//��������������������������������������������������������������
float2 NormalizeTexCoord(float2 texCoord)
{
	float2 normalizedTexCoord;
	normalizedTexCoord.x = texCoord.x - floor(texCoord.x);
	normalizedTexCoord.y = texCoord.y - floor(texCoord.y);
	return normalizedTexCoord;
}

//��������������������������������������������������������������
// �G���g���|�C���g
//��������������������������������������������������������������
float4 Entry(float4 pos : POSITION) : SV_POSITION
{
	return pos;
}

//��������������������������������������������������������������
// ���_�V�F�[�_�[
//��������������������������������������������������������������
VS_OUTPUT VS_Main(VS_INPUT inVS)
{
	VS_OUTPUT outVS;

	float4 vtx;
	//float4x4 mtxScale, mtxTrans, mtxRot;

	//float4x4 matrixWorld;
	//matrixWorld = MatrixIdentity(matrixWorld);

	//// ��]�𔽉f
	//mtxRot = MatrixRotation(mtxWorld.rot);
	//matrixWorld = mul(matrixWorld, mtxRot);

	//// �ʒu�𔽉f
	//mtxTrans = MatrixTrans(mtxWorld.pos);
	//matrixWorld = mul(matrixWorld, mtxTrans);

	//// �g�k�𔽉f
	//mtxScale = MatrixScaling(mtxWorld.scale);
	//matrixWorld = mul(matrixWorld, mtxScale);

	outVS.pos = inVS.pos;
	outVS.col = inVS.col;
	outVS.texCoord = inVS.texCoord;
	return outVS;
}

struct PS_OUTPUT
{	// �s�N�Z���V�F�[�_�[�o�͗p
	float4 col : SV_Target;
};

//��������������������������������������������������������������
// �s�N�Z���V�F�[�_�[
//��������������������������������������������������������������
PS_OUTPUT PS_Main(VS_OUTPUT inVS)
{
	PS_OUTPUT outPS;
	float2 normalizedTexCoord = NormalizeTexCoord(inVS.texCoord);

	// �e�N�X�`���T���v�����O
	outPS.col = inVS.col;
	return outPS;
}

//��������������������������������������������������������������
// �e�N�j�b�N
//��������������������������������������������������������������
technique TechShader
{
	pass P0
	{
		VertexShader = compile vs_2_0 VS_Main();
		PixelShader = compile ps_2_0 PS_Main();
	}
}