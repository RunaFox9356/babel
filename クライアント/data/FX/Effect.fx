// -------------------------------------------------------------
// グローバル変数
// -------------------------------------------------------------
struct WVP
{
	float4x4 world;
	float4x4 view : VIEW;
	float4x4 proj : PROJECTION;
};
WVP wvp;

float4x4 mWVP;
float4 vLightDir;	// ライトの方向
float4 vDiffuse;	// ライト＊メッシュの色
float4 vAmbient;	// 色
float4 vCameraPos;
float4 diff;
bool isUseTex;
int renderTime;

int numBlendMatrix;
extern int NumVertInfluences = 2;
extern float4x4 skinMatrix[16] : WORLDMATRIXARRAY;
extern float4x4 skinOffsetMatrix[16];

// test
extern int skinIndex[16];

sampler textureSampler : register(s0);	// pDevice->SetTexture()に送られたテクスチャ

// -------------------------------------------------------------
// 頂点シェーダからピクセルシェーダに渡すデータ
// -------------------------------------------------------------
struct VS_OUTPUT
{
	float4 Pos			: SV_POSITION;			// 位置
	float4 Color		: COLOR0;			// 色
	float2 Tex			: TEXCOORD0;		// テクスチャ
	float3 Normal		: TEXCOORD1;
};

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// エントリポイント
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float4 main(float4 pos : POSITION) : SV_POSITION
{
	return pos;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 通常の頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// テクスチャの法線ベクトル
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 WVP = mul(wvp.world, wvp.view);
	WVP = mul(WVP, wvp.proj);

	float4 worldPos = wvp.world[3];

	// 座標変換
	float4 pos = mul(Pos, WVP);
	Out.Pos = pos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// 関数メモ
	// saturate : 正規化に近い？
	// transpose : 転置行列が行われる
	// smoothstep : 補間

	// ローカル座標に変換
	float3 localL = mul(L, (float3x3)transpose(wvp.world));

	// 法線ベクトル
	float3 N = normalize( Normal.xyz );
	float factor = dot(N, localL);

	// 反射の計算
	float4 vRefColor = float4(0.75f, 0.75f, 0.75f, 1.0f);
	float4 refColor = vRefColor * smoothstep(1.0f - 0.1f, 1.0f, factor);

	// 影の色変更
	float shadowStrength = 0.25f;
	float4 lightColor = float4(0.0f, 0.0f, 0.0f,1.0f);
	float4 shadowColor = float4(shadowStrength, shadowStrength, shadowStrength, 1.0f) * vDiffuse;
	float4 shadingColor = lerp(shadowColor, lightColor, factor);

	// 拡散光＋環境光
	Out.Normal = N;
	Out.Color = (vDiffuse * max(vAmbient, factor) + refColor);
	Out.Color += shadingColor;

	return Out;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// アウトラインの頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS_Outline(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// テクスチャの法線ベクトル
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 mtxWorld = wvp.world;

	float scale = 1.75f;
	float3 normalDir = normalize(Pos.xyz - Normal.xyz);
	float4 outline = Pos + float4(normalDir * scale, 0.0f);

	float4x4 WVP = mul(wvp.world, wvp.view);
	WVP = mul(WVP, wvp.proj);

	// 座標変換
	float4 pos = mul(outline, WVP);
	Out.Pos = pos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// 法線ベクトル
	float3 N = normalize(Normal.xyz);

	// 拡散光＋環境光
	Out.Color = Color;

	return Out;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// グリッチの頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS_Glitch(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// テクスチャの法線ベクトル
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 WVP = mul(wvp.world, wvp.view);
	WVP = mul(WVP, wvp.proj);

	// グリッチの設定
	float glitchStrong = 0.2f;
	float moveFactor = renderTime * 0.01f;
	float2 displacement = (sin(Pos.y * 200.0f * moveFactor) * glitchStrong) + (sin(Pos.x * 100.0f * moveFactor) * glitchStrong);

	// 頂点を動かす
	float4 resultPos = mul(Pos, WVP) + float4(displacement.xy, 0.0f, 0.0f);
	Out.Pos = resultPos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// ローカル座標に変換
	float3 localL = mul(L, (float3x3)transpose(wvp.world));

	// 法線ベクトル
	float3 N = normalize(Normal.xyz);
	float factor = dot(N, localL);

	// 影の色変更
	float shadowStrength = 0.25f;
	float4 lightColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
	float4 shadowColor = float4(shadowStrength, shadowStrength, shadowStrength, 1.0f) * vDiffuse;
	float4 shadingColor = lerp(shadowColor, lightColor, factor);

	// 拡散光＋環境光
	Out.Normal = N;
	Out.Color = vDiffuse * max(vAmbient, factor);
	Out.Color += shadingColor;

	return Out;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ガラスの頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS_Glass(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// テクスチャの法線ベクトル
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 WVP = mul(wvp.world, wvp.view);
	WVP = mul(WVP, wvp.proj);

	float4 worldPos = wvp.world[3];

	// 座標変換
	float4 pos = mul(Pos, WVP);
	Out.Pos = pos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// ローカル座標に変換
	float3 localL = mul(L, (float3x3)transpose(wvp.world));

	// 法線ベクトル
	float3 N = normalize(Normal.xyz);
	float factor = dot(N, localL);

	// 反射
	float4 vRefColor = float4(0.75f, 0.75f, 0.75f, 1.0f);
	float4 refColor = vRefColor * smoothstep(1.0f - 0.1f, 1.0f, factor);

	// 拡散光＋環境光
	Out.Normal = N;
	Out.Color = (float4(1.0f,1.0f,1.0f,1.0f) * max(0.3f, factor) + refColor);

	return Out;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// テクスチャ座標正規化
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float2 NormalizeTexCoord(float2 texCoord)
{
	float2 normalizedTexCoord;
	normalizedTexCoord.x = texCoord.x - floor(texCoord.x);
	normalizedTexCoord.y = texCoord.y - floor(texCoord.y);
	return normalizedTexCoord;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// 通常のピクセルシェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float4 PS(VS_OUTPUT In) : COLOR0
{
	float4 result;
	float2 normalizedTexCoord = NormalizeTexCoord(In.Tex);

	// テクスチャサンプリング
	float4 texColor = tex2D(textureSampler, normalizedTexCoord);
	result = isUseTex ? (In.Color * texColor) : In.Color;
	return result;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// アウトラインのピクセルシェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float4 PS_Outline(VS_OUTPUT In) : COLOR0
{
	float4 outline = float4(1.0f, 0.5f, 0.0f, 1.0f);
	return outline;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// ホログラムのピクセルシェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float4 PS_Hologram(VS_OUTPUT In) : COLOR0
{
	float4 result;

	// テクスチャサンプリング
	float4 texColor = tex2D(textureSampler, In.Tex);
	result = isUseTex ? (In.Color * texColor) : In.Color;

	// ホログラム効果
	float texPos = In.Tex.y + renderTime;
	float hologramTex = sin(texPos * 300.0f) * 0.2f + 0.8f;
	float4 hologramEffect = float4(hologramTex, hologramTex * 2.5f, hologramTex * 3.0f, result.a * 0.75f);

	result = result * hologramEffect;
	return result;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// テクニック
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
technique Diffuse
{
	pass P0
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	}
	pass P1
	{
		VertexShader = compile vs_2_0 VS_Outline();
		PixelShader = compile ps_2_0 PS_Outline();
		CullMode = CCW;
	}
	pass P2
	{
		VertexShader = compile vs_2_0 VS_Glitch();
		PixelShader = compile ps_2_0 PS_Hologram();
	}
	pass P3
	{
		VertexShader = compile vs_2_0 VS_Glass();
		PixelShader = compile ps_2_0 PS();
	}
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// メッシュフィールド用の頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS_MeshField(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float4 Color : COLOR0,
	float2 Tex : TEXCOORD		// テクスチャの法線ベクトル
)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 WVP = mul(wvp.world, wvp.view);
	WVP = mul(WVP, wvp.proj);

	// 座標変換
	float4 pos = mul(Pos, WVP);
	Out.Pos = pos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// 法線ベクトル
	float3 N = normalize(Normal.xyz);

	Out.Normal = N;
	Out.Color = Color;

	return Out;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// メッシュフィールド用のピクセルシェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
float4 PS_MeshField(VS_OUTPUT In) : COLOR0
{
	float4 result;
	float2 normalizedTexCoord = NormalizeTexCoord(In.Tex);
	
	// テクスチャサンプリング
	float4 texColor = tex2D(textureSampler, normalizedTexCoord);
	result = isUseTex ? (In.Color * texColor) : In.Color;
	result = result * 1.5f;
	return result;
}

technique MeshField
{
	pass P0
	{
		VertexShader = compile vs_2_0 VS_MeshField();
		PixelShader = compile ps_2_0 PS_MeshField();
	}
};

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// スキンメッシュ用の頂点シェーダー
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
VS_OUTPUT VS_Skin(
	float4 Pos    : POSITION0,	// ローカル位置座標
	float4 Normal : NORMAL,		// 法線ベクトル
	float2 Tex : TEXCOORD0,
	float4 BlendWeights : BLENDWEIGHT0,
	float4 BlendIndicies : BLENDINDICES0)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;		// 出力データ

	float4x4 result = (float4x4)0;
	for (int i = 0; i <= numBlendMatrix; i++)
	{
		result += skinMatrix[skinIndex[i]] * BlendWeights[i];
	}

	float4x4 WVP = mul(result, wvp.world);
	WVP = mul(WVP, wvp.view);
	WVP = mul(WVP, wvp.proj);

	// 座標変換
	float4 pos = mul(Pos, WVP);
	Out.Pos = pos;

	// テクスチャ座標
	Out.Tex = Tex;

	// 拡散光＋環境光
	float3 L = -(vLightDir.xyz);

	// ローカル座標に変換
	float3 localL = mul(L, (float3x3)transpose(wvp.world));

	// 法線ベクトル
	float3 srcN = Normal.xyz;
	srcN = mul(srcN, result);
	float3 N = normalize(srcN);
	float factor = dot(N, localL);

	// 反射の計算
	float4 vRefColor = float4(0.75f, 0.75f, 0.75f, 1.0f);
	float4 refColor = vRefColor * smoothstep(1.0f - 0.1f, 1.0f, factor);

	// 影の色変更
	float shadowStrength = 0.25f;
	float4 lightColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
	float4 shadowColor = float4(shadowStrength, shadowStrength, shadowStrength, 1.0f) * vDiffuse;
	float4 shadingColor = lerp(shadowColor, lightColor, factor);

	// 拡散光＋環境光
	Out.Normal = N;
	Out.Color = (vDiffuse * max(vAmbient, factor) + refColor);
	Out.Color += shadingColor;
	return Out;
}

technique Skin
{
	pass P0
	{
		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS();
	}
}
