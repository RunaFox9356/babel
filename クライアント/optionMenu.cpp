//=============================================================================
//
// optionMenu.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "optionMenu.h"
#include "application.h"
#include "polygon2D.h"
#include "text.h"
#include "slider.h"
#include "renderer.h"
#include "camera.h"
#include "menuButton.h"
#include "agent.h"


//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			COptionMenu::DEFAULT_BACKGROUND_TEXTURE_IDX = 9;									//ディフォルトの背景のテクスチャインデックス
const D3DXCOLOR		COptionMenu::DEFAULT_TEXT_COLOR = { 1.0f, 0.25f, 0.25f, 1.0f };						//ディフォルトのテキストの色
const D3DXVECTOR3	COptionMenu::DEFAULT_FONT_SIZE = { 7.0f, 7.0f, 7.0f };								//ディフォルトのフォントサイズ
const int			COptionMenu::DEFAULT_DELAY = 15;													//ディフォルトのディレイ
const char*			COptionMenu::DEFAULT_FILE_PATH = "data/INPUT/Settings.txt";							//設定の外部ファイルのパス

//マウス感度の設定(普通)
const D3DXVECTOR3	COptionMenu::DEFAULT_MOUSE_SENSIBILITY_SLIDER_POS = { 250.0f, 60.0f, 0.0f };		// マウス感度のスライダーの位置
const D3DXVECTOR3	COptionMenu::DEFAULT_MOUSE_SENSIBILITY_SLIDER_SIZE = { 300.0f, 30.0f, 0.0f };		//マウス感度のスライダーのサイズ
const D3DXVECTOR3	COptionMenu::DEFAULT_MOUSE_SENSIBILITY_TEXT_POS = { 500.0f, 60.0f, 0.0f };			// マウス感度のスライダーのテキスト位置
const D3DXVECTOR3	COptionMenu::DEFAULT_MOUSE_SENSIBILITY_VALUE_POS = { 1000.0f, 60.0f, 0.0f };		//マウス感度のスライダー値の位置
const float			COptionMenu::DEFAULT_MOUSE_SENSIBILITY_MIN = 0.1f;									//ディフォルトのマウス感度の最小値
const float			COptionMenu::DEFAULT_MOUSE_SENSIBILITY_MAX = 1.0f;									//ディフォルトのマウス感度の最大値

//マウス感度の設定(狙うモード)
const D3DXVECTOR3	COptionMenu::AIM_MOUSE_SENSIBILITY_SLIDER_POS = { 250.0f, 120.0f, 0.0f };			// マウス感度のスライダーの位置
const D3DXVECTOR3	COptionMenu::AIM_MOUSE_SENSIBILITY_SLIDER_SIZE = { 300.0f, 30.0f, 0.0f };			//マウス感度のスライダーのサイズ
const D3DXVECTOR3	COptionMenu::AIM_MOUSE_SENSIBILITY_TEXT_POS = { 500.0f, 120.0f, 0.0f };				// マウス感度のスライダーのテキスト位置
const D3DXVECTOR3	COptionMenu::AIM_MOUSE_SENSIBILITY_VALUE_POS = { 1000.0f, 120.0f, 0.0f };			//マウス感度のスライダー値の位置
const float			COptionMenu::AIM_MOUSE_SENSIBILITY_MIN = 0.1f;										//ディフォルトのマウス感度の最小値
const float			COptionMenu::AIM_MOUSE_SENSIBILITY_MAX = 0.75f;										//ディフォルトのマウス感度の最大値

//フォントの設定
const D3DXVECTOR3	COptionMenu::DEFAULT_FONT_TEXT_POS = { 100.0f, 180.0f, 0.0f };						//フォントのテキストの位置
const D3DXVECTOR3	COptionMenu::DEFAULT_FONT_BUTTON_POS = { -100.0f, -180.0f, 0.0f };					//フォントのボタンの位置(相対(背景は親))
const D3DXVECTOR3	COptionMenu::DEFAULT_FONT_BUTTON_SIZE = { 30.0f, 30.0f, 30.0f };					//フォントのボタンのサイズ


namespace nsOption
{
	const char* pFontNames[CFont::FONT::FONT_MAX] = { "GON", "MASUO", "SOUEIKAKU" };
};


//コンストラクタ
COptionMenu::COptionMenu() : m_nFont(0),
m_nDelay(0),
m_pMouseSensibility(nullptr),
m_pMouseSensibilityText(nullptr),
m_pMouseSensibilityValue(nullptr),
m_pAimMouseSensibility(nullptr),
m_pAimMouseSensibilityText(nullptr),
m_pAimMouseSensibilityValue(nullptr),
m_pBg(nullptr),
m_pFontText(nullptr),
m_pFontButton(nullptr)
{

}

//デストラクタ
COptionMenu::~COptionMenu()
{

}

//初期化
HRESULT COptionMenu::Init()
{
	m_nDelay = DEFAULT_DELAY;

	float fMouseSensibility = DEFAULT_MOUSE_SENSIBILITY_MAX;
	float fAimSensibility = AIM_MOUSE_SENSIBILITY_MAX;

	LoadOptions(fMouseSensibility, fAimSensibility);

	//背景の生成
	m_pBg = CPolygon2D::Create();

	// タイトルロゴ
	m_pLog = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);
	m_pLog->SetPos(D3DXVECTOR3(240.0f, 580.0f, 0.0f));
	m_pLog->SetSize(D3DXVECTOR3(1280.0f*0.5f, 720.0f*0.5f, 0.0f));
	m_pLog->LoadTex(75);

	if (m_pBg)
	{//nullチェック
		m_pBg->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));	//位置の設定
		m_pBg->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));				//サイズの設定
		m_pBg->LoadTex(DEFAULT_BACKGROUND_TEXTURE_IDX);														//テクスチャの設定
	}

	//マウス感度のスライダーの生成
	m_pMouseSensibility = CSlider::Create(DEFAULT_MOUSE_SENSIBILITY_SLIDER_POS, DEFAULT_MOUSE_SENSIBILITY_SLIDER_SIZE, DEFAULT_MOUSE_SENSIBILITY_MAX, DEFAULT_MOUSE_SENSIBILITY_MIN);

	if (m_pMouseSensibility)
	{
		m_pMouseSensibility->SetSliderDefault(fMouseSensibility);
	}


	D3DXVECTOR3 P = DEFAULT_MOUSE_SENSIBILITY_TEXT_POS;			//絶対位置の設定

	//マウス感度のスライダーのテキストの生成
	m_pMouseSensibilityText = CText::Create(P, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, "Mouse Sensibility (Normal)", DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

	{
		std::string str = std::to_string(fMouseSensibility);
		P = DEFAULT_MOUSE_SENSIBILITY_VALUE_POS;

		m_pMouseSensibilityValue = CText::Create(P, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成
	}

	{
		//マウス感度のスライダーの生成
		m_pAimMouseSensibility = CSlider::Create(AIM_MOUSE_SENSIBILITY_SLIDER_POS, AIM_MOUSE_SENSIBILITY_SLIDER_SIZE, AIM_MOUSE_SENSIBILITY_MAX, AIM_MOUSE_SENSIBILITY_MIN);

		if (m_pAimMouseSensibility)
		{
			m_pAimMouseSensibility->SetSliderDefault(fAimSensibility);
		}


		P = AIM_MOUSE_SENSIBILITY_TEXT_POS;			//絶対位置の設定

																	//マウス感度のスライダーのテキストの生成
		m_pAimMouseSensibilityText = CText::Create(P, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, "Mouse Sensibility (Aim)", DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

		{
			std::string str = std::to_string(fAimSensibility);
			P = AIM_MOUSE_SENSIBILITY_VALUE_POS;

			m_pAimMouseSensibilityValue = CText::Create(P, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成
		}
	}

	{
		std::string str = "Current Font: ";
		str += nsOption::pFontNames[m_nFont];
		P = DEFAULT_FONT_TEXT_POS;

		m_pFontText = CText::Create(P, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

		m_pFontButton = CMenuButton::Create(DEFAULT_FONT_BUTTON_POS);

		if (m_pFontButton)
		{
			m_pFontButton->SetParent(this);
			m_pFontButton->SetSize(DEFAULT_FONT_BUTTON_SIZE);
		}
	}

	return S_OK;
}

//終了
void COptionMenu::Uninit()
{
	//設定の保存処理
	SaveChanges();

	//スライダーの破棄
	if (m_pMouseSensibility)
	{
		m_pMouseSensibility->Uninit();
		m_pMouseSensibility = nullptr;
	}
	//スライダーの破棄
	if (m_pAimMouseSensibility)
	{
		m_pAimMouseSensibility->Uninit();
		m_pAimMouseSensibility = nullptr;
	}

	//テキストの破棄
	if (m_pMouseSensibilityText)
	{
		m_pMouseSensibilityText->Uninit();
		m_pMouseSensibilityText = nullptr;
	}
	if (m_pMouseSensibilityValue)
	{
		m_pMouseSensibilityValue->Uninit();
		m_pMouseSensibilityValue = nullptr;
	}
	if (m_pAimMouseSensibilityText)
	{
		m_pAimMouseSensibilityText->Uninit();
		m_pAimMouseSensibilityText = nullptr;
	}
	if (m_pAimMouseSensibilityValue)
	{
		m_pAimMouseSensibilityValue->Uninit();
		m_pAimMouseSensibilityValue = nullptr;
	}
	if (m_pFontText)
	{
		m_pFontText->Uninit();
		m_pFontText = nullptr;
	}

	//ボタンの破棄
	if (m_pFontButton)
	{
		m_pFontButton->Uninit();
		m_pFontButton = nullptr;
	}

	//背景の破棄
	if (m_pBg)
	{
		m_pBg->Uninit();
		m_pBg = nullptr;
	}
	//背景の破棄
	if (m_pLog)
	{
		m_pLog->Uninit();
		m_pLog = nullptr;
	}
	
	CSuper::Release();
}

//更新
void COptionMenu::Update()
{
	if (m_nDelay > 0)
		m_nDelay--;

	MouseSensibilityUpdate();

	AimSensibilityUpdate();

	FontUpdate();
}

//描画
void COptionMenu::Draw()
{

}

// 位置のゲッター
D3DXVECTOR3 COptionMenu::GetPos()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pBg)
		pos = m_pBg->GetPos();

	return pos;
}

// 大きさのゲッター
D3DXVECTOR3 COptionMenu::GetSize()
{
	D3DXVECTOR3 size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pBg)
		size = m_pBg->GetSize();

	return size;
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
COptionMenu * COptionMenu::Create()
{
	COptionMenu* pObj = new COptionMenu;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
		return nullptr;

	return pObj;								//生成したインスタンスを返
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



//マウス感度の更新
void COptionMenu::MouseSensibilityUpdate()
{
	if (m_pMouseSensibility)
	{
		if (!m_pMouseSensibility->IsBeingChanged())
		{
			CCamera* pCamera = CApplication::GetInstance()->GetCamera();

			if (pCamera && m_pMouseSensibility->GetSliderValue() != pCamera->GetMouseSensibility())
			{
				pCamera->SetMouseSensibility(m_pMouseSensibility->GetSliderValue());
				m_pMouseSensibilityValue = ChangeText(m_pMouseSensibilityValue, std::to_string(m_pMouseSensibility->GetSliderValue()));
				CAgent::SetNormalMouseSensibility(m_pMouseSensibility->GetSliderValue());
			}
		}
	}
}

//狙うモードのマウス感度の更新
void COptionMenu::AimSensibilityUpdate()
{
	if (m_pAimMouseSensibility)
	{
		if (!m_pAimMouseSensibility->IsBeingChanged())
		{
			CCamera* pCamera = CApplication::GetInstance()->GetCamera();

			if (pCamera && m_pAimMouseSensibility->GetSliderValue() != CAgent::GetAimMouseSensibility())
			{
				m_pAimMouseSensibilityValue = ChangeText(m_pAimMouseSensibilityValue, std::to_string(m_pAimMouseSensibility->GetSliderValue()));
				CAgent::SetAimMouseSensibility(m_pAimMouseSensibility->GetSliderValue());
			}
		}
	}
}

//フォントの更新
void COptionMenu::FontUpdate()
{
	if (m_pFontButton && m_pFontText && m_pFontButton->GetTriggerState() && m_nDelay <= 0)
	{
		m_nDelay = DEFAULT_DELAY;
		m_pFontButton->SetTriggerState(false);

		m_nFont++;

		if (m_nFont >= CFont::FONT::FONT_MAX)
			m_nFont = (CFont::FONT)0;

		CText::SetDefaultFont((CFont::FONT)m_nFont);

		std::string str = "Current Font: ";
		str += nsOption::pFontNames[m_nFont];

		m_pFontText->Uninit();
		m_pFontText = nullptr;

		m_pFontText = CText::Create(DEFAULT_FONT_TEXT_POS, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

		ChangeTextFont();
	}
}

//テキストのフォントの変更
void COptionMenu::ChangeTextFont()
{
	if (m_pMouseSensibilityText)
	{
		m_pMouseSensibilityText->Uninit();
		m_pMouseSensibilityText = nullptr;
	}
	if (m_pMouseSensibilityValue)
	{
		m_pMouseSensibilityValue->Uninit();
		m_pMouseSensibilityValue = nullptr;
	}
	if (m_pAimMouseSensibilityText)
	{
		m_pAimMouseSensibilityText->Uninit();
		m_pAimMouseSensibilityText = nullptr;
	}
	if (m_pAimMouseSensibilityValue)
	{
		m_pAimMouseSensibilityValue->Uninit();
		m_pAimMouseSensibilityValue = nullptr;
	}

	m_pMouseSensibilityText = CText::Create(DEFAULT_MOUSE_SENSIBILITY_TEXT_POS, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, "Mouse Sensibility", DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

	if(m_pMouseSensibility)
	{
		float fValue = m_pMouseSensibility->GetSliderValue();

		std::string str = std::to_string(fValue);

		m_pMouseSensibilityValue = CText::Create(DEFAULT_MOUSE_SENSIBILITY_VALUE_POS, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成
	}

	m_pAimMouseSensibilityText = CText::Create(AIM_MOUSE_SENSIBILITY_TEXT_POS, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, "Mouse Sensibility (Aim)", DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

	if(m_pAimMouseSensibility)
	{
		float fValue = m_pAimMouseSensibility->GetSliderValue();

		std::string str = std::to_string(fValue);

		m_pAimMouseSensibilityValue = CText::Create(AIM_MOUSE_SENSIBILITY_VALUE_POS, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成
	}
}

//テキストの変更
CText* COptionMenu::ChangeText(CText* pText, std::string str)
{
	if (!pText || str.size() <= 0)
		return pText;

	D3DXVECTOR3 pos = pText->GetPos();
	pText->Uninit();

	pText = CText::Create(pos, D3DXVECTOR3(100.0f, 30.0f, 0.0f), CText::MAX, 1000, 1, str.c_str(), DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOR);	//テキストの生成

	return pText;
}

//設定のセーブ処理
void COptionMenu::SaveChanges()
{
	std::ofstream strm(DEFAULT_FILE_PATH);

	if (strm && strm.good())
	{
		float fMouseSensibility = DEFAULT_MOUSE_SENSIBILITY_MAX;
		float fAimSensibility = AIM_MOUSE_SENSIBILITY_MAX;

		if (m_pMouseSensibility)
			fMouseSensibility = m_pMouseSensibility->GetSliderValue();

		if (m_pAimMouseSensibility)
			fAimSensibility = m_pAimMouseSensibility->GetSliderValue();

		strm << "#==========================================================#\n";
		strm << "#                         Settings                         #\n";
		strm << "#==========================================================#\n\n";

		strm << "MOUSE_SENSIBILITY " << fMouseSensibility << "\n\n";

		strm << "AIM_MOUSE_SENSIBILITY " << fAimSensibility << "\n\n";

		strm << "FONT_TYPE " << m_nFont << "\n\n";

		strm.close();
	}
}

//設定のロード処理
void COptionMenu::LoadOptions(float& fMouseSensibility, float& fAimSensibility)
{
	std::ifstream strm(DEFAULT_FILE_PATH);

	if (strm && strm.good())
	{
		char aStr[256] = {};

		while (!strm.eof())
		{
			strm >> aStr;

			if (strcmp(aStr, "MOUSE_SENSIBILITY") == 0)
			{
				strm >> fMouseSensibility;

				if (fMouseSensibility < DEFAULT_MOUSE_SENSIBILITY_MIN)
					fMouseSensibility = DEFAULT_MOUSE_SENSIBILITY_MIN;
				else if (fMouseSensibility > DEFAULT_MOUSE_SENSIBILITY_MAX)
					fMouseSensibility = DEFAULT_MOUSE_SENSIBILITY_MAX;
			}
			else if (strcmp(aStr, "AIM_MOUSE_SENSIBILITY") == 0)
			{
				strm >> fAimSensibility;

				if (fAimSensibility < AIM_MOUSE_SENSIBILITY_MIN)
					fAimSensibility = AIM_MOUSE_SENSIBILITY_MIN;
				else if (fAimSensibility > AIM_MOUSE_SENSIBILITY_MAX)
					fAimSensibility = AIM_MOUSE_SENSIBILITY_MAX;
			}
			else if (strcmp(aStr, "FONT_TYPE") == 0)
			{
				strm >> m_nFont;

				if (m_nFont < 0)
					m_nFont = 0;
				else if (m_nFont >= CFont::FONT::FONT_MAX)
					m_nFont = CFont::FONT::FONT_MAX - 1;

				CText::SetDefaultFont((CFont::FONT)m_nFont);
			}
		}
	}
}
