//=============================================================================
//
// exclamationMark.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "exclamationMark.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			CExclamationMark::DEFAULT_TEXTURE_IDX = 95;								//ディフォルトのテクスチャインデックス
const D3DXVECTOR3	CExclamationMark::DEFAULT_RELATIVE_POS = { 0.0f, 150.0f, 0.0f };		//ディフォルトの相対位置
const D3DXVECTOR3	CExclamationMark::DEFAULT_SIZE = { 50.0f, 50.0f, 0.0f };				//ディフォルトのサイズ



//コンストラクタ
CExclamationMark::CExclamationMark() : m_relativePos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_pParent(nullptr)
{

}

//デストラクタ
CExclamationMark::~CExclamationMark()
{

}

// 初期化
HRESULT CExclamationMark::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon3D::Init()))
		return E_FAIL;

	LoadTex(DEFAULT_TEXTURE_IDX);
	SetBillboard(true);

	return S_OK;
}

// 終了
void CExclamationMark::Uninit()
{
	//親クラスの終了処理
	CPolygon3D::Uninit();
}

// 更新
void CExclamationMark::Update()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pParent)
	{
		pos = m_pParent->GetPos() + m_relativePos;
	}

	SetPos(pos);

	//親クラスの更新処理
	CPolygon3D::Update();
}

// 描画
void CExclamationMark::Draw()
{
	//親クラスの描画処理
	CPolygon3D::Draw();
}






//生成
CExclamationMark* CExclamationMark::Create(CObject* pParent, D3DXVECTOR3 relativePos, D3DXVECTOR3 size)
{
	CExclamationMark* pObj = new CExclamationMark;		//インスタンスを生成する

	//初期化
	if (FAILED(pObj->Init()))
		return nullptr;

	pObj->SetParent(pParent);	//親の設定
	pObj->m_relativePos = relativePos;		//相対位置の設定
	pObj->SetSize(size);		//サイズの設定

	return pObj;				//生成したインスタンスを返
}