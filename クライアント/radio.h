//=============================================================================
//
//  radioチャット.h
// Author: 浜田琉雅
//
//=============================================================================
#ifndef _RADIO_H
#define _RADIO_H


//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CText;


class CRadio : public CPolygon2D
{
public:
	CRadio();
	~CRadio() override;

	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新
	void Draw() override;							// 描画


	static CRadio* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const int nTextureIdx, const char* pMessageText, const D3DXCOLOR col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));	//生成
	void CreateText(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, const char* pMessageText, const D3DXCOLOR col);		//テキストの生成

	void SetTop(const D3DXVECTOR3 pos) { m_TopPos = pos; }
private:

	
private:
	D3DXVECTOR3 m_TopPos;
	int m_Top;
	static const int MaxSize = 5;
	int m_TextSize;
	CText* m_pText[MaxSize];					//テキストへのポインタ

};


#endif