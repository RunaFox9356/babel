//=============================================================================
//
// staticEnemy.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "staticEnemy.h"
#include "viewField.h"
#include "application.h"
#include "scene_mode.h"
#include "collision_rectangle3D.h"
#include "hacker.h"
#include "agent.h"
#include "motion.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3		CStaticEnemy::EYE_RELATIVE_POS = D3DXVECTOR3(0.0f, 42.0f, 0.0f);			//目の相対位置
const float				CStaticEnemy::LOOK_AROUND_ANGLE = D3DX_PI * 0.25f;							//周りを見る時の加算される角度
const D3DXVECTOR3		CStaticEnemy::DEFAULT_COLLISION_POS = D3DXVECTOR3(0.0f, 70.0f, 0.0f);		//ディフォルトの当たり判定の相対位置
const D3DXVECTOR3		CStaticEnemy::DEFAULT_COLLISION_SIZE = D3DXVECTOR3(40.0f, 140.0f, 40.0f);	//ディフォルトの当たり判定のサイズ
const int				CStaticEnemy::DEFAULT_HAND_INDEX = 3;										//ディフォルトの手のモデルのインデックス
const float				CStaticEnemy::DEFAULT_ROTATION_FRICTION = 0.02f;							//ディフォルトの回転の摩擦係数
const float				CStaticEnemy::ATTACK_ROTATION_FRICTION = 0.1f;								//攻撃の時の回転の摩擦係数
const int				CStaticEnemy::DEFAULT_WAIT_FRAMES = 120;									//ディフォルトの待つフレーム数


//コンストラクタ
CStaticEnemy::CStaticEnemy() : m_nCntAnim(0),
m_nCntPhase(0),
m_fLookAroundAngle(0.0f),
m_fOriginalRot(0.0f),
m_fTargetRot(0.0f),
m_bSeen(false),
m_state((EState)0),
m_pViewField(nullptr)
{

}

//デストラクタ
CStaticEnemy::~CStaticEnemy()
{

}

// 初期化
HRESULT CStaticEnemy::Init()
{
	//親クラスの初期化処理
	CEnemy::Init();

	CApplication::GetInstance()->GetSceneMode()->SetEnemyUse(true, GetId());

	SetMotion("data/MOTION/motion001.txt");		//アニメーションの設定

	m_pViewField = CViewField::Create(this);		//視野の生成

	if (m_pViewField)
	{//nullチェック

		m_pViewField->SetPosV(EYE_RELATIVE_POS);					//視野の相対位置の設定
		m_pViewField->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));		//視野の相対回転の設定
	}

	m_fLookAroundAngle = LOOK_AROUND_ANGLE;		//周りを見る時の最大角度の設定
	m_state = STATE_LOOK_AROUND;				//現在の状態の設定

	CCollision_Rectangle3D* pCollision = GetCollision();		//当たり判定の取得
	CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());

	if (pCollision)
	{//当たり判定のサイズと相対位置の設定

		GetCollision()->SetSize(DEFAULT_COLLISION_SIZE);
		GetCollision()->SetPos(DEFAULT_COLLISION_POS);
	}

	SetRotFriction(DEFAULT_ROTATION_FRICTION);			//回転の摩擦係数の設定
	SetActionState(CEnemy::ACTION_STATE::NEUTRAL_ACTION);

	return S_OK;
}

// 終了
void CStaticEnemy::Uninit()
{
	//視野の破棄
	if (m_pViewField)
	{
		m_pViewField->Uninit();
		m_pViewField = nullptr;
	}

	//親クラスの終了処理
	CEnemy::Uninit();
}

// 更新
void CStaticEnemy::Update()
{
	if (CApplication::GetInstance()->GetSceneMode()->GetEnd())
		return;

	if (!GetDeath() && !GetStun())
	{
		CreateGun(DEFAULT_HAND_INDEX);							//銃の生成処理

		if (m_pViewField)
		{//視野のnullチェック	
			CPlayer* checkPlayer = CHacker::GetPlayer();		//プレイヤーの取得
			if (checkPlayer && CApplication::GetInstance()->GetEnemyConnect())
			{//ハッカーでは接続されてないと動かなくなるぞ！	
				//SetPos(CApplication::GetInstance()->GetPlayer(1)->GetCommu()->Player.m_isPopEnemy[GetId()].Pos);
			}
			if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDiscovery)
			{
				m_bSeen = true;
				m_state = STATE_ONLINEATTACK;
				SetActionState(CEnemy::ACTION_STATE::AIM_ACTION);
			}
			else
			{

				m_bSeen = false;
			}
			CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

			if (pPlayer && !pPlayer->GetHiddenState())
			{//プレイヤーが見える状態だったら

			 //プレイヤーが見えるかどうかを確認する
				D3DXVECTOR3 pos = pPlayer->GetPos();
				D3DXVECTOR3 dist = pos - GetPos();

				if (m_pViewField->IsPontInView(pos) ||
					m_pViewField->IsPontInView(pPlayer->GetPlayerBody()) ||
					m_pViewField->IsPontInView(pPlayer->GetPlayerHead()) ||
					(!m_bSeen && pPlayer->GetDash() && D3DXVec3Length(&dist) < 500.0f))
				{
					m_bSeen = true;
					CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(true, GetId());
					m_state = STATE_ATTACK;
					SetRotFriction(ATTACK_ROTATION_FRICTION);
					SoundPosReset();
					IncreaseSecurity();
					SetExclamationMarkDraw(true);
					SetActionState(CEnemy::ACTION_STATE::AIM_ACTION);
				}
				else if (m_bSeen)
				{
					CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
					m_bSeen = false;
					SetRotFriction(DEFAULT_ROTATION_FRICTION);
					SetExclamationMarkDraw(false);
					SetActionState(CEnemy::ACTION_STATE::NEUTRAL_ACTION);
				}
			}
		}

		UpdateState();					//現在の状態によっての更新処理

		CEnemy::Update();				//親クラスの更新処理

		if (GetDeath())
		{//死亡フラグがtrueだったら

			Kill();			//死ぬように設定する

			return;
		}
	}
	else
	{
		//親クラスの更新処理
		CEnemy::Update();
	}
	CApplication::GetInstance()->GetSceneMode()->SetEnemyPos(GetPos(), GetId());

}

//目の相対位置の設定
void CStaticEnemy::SetEyePos(const D3DXVECTOR3 eyePos)
{
	if (m_pViewField)
	{
		m_pViewField->SetPosV(eyePos);
	}
}

//セキュリティーレベルのセッター
void CStaticEnemy::SetSecurityLevel(const int nSecurity)
{
	CEnemy::SetSecurityLevel(nSecurity);

	if (m_pViewField)
	{
		m_pViewField->SetViewDistance(CViewField::DEFAULT_VIEW_DISTANCE + (CViewField::DEFAULT_VIEW_DISTANCE * 0.5f * nSecurity));
		m_pViewField->SetViewAngle(CViewField::DEFAULT_VIEW_ANGLE + (CViewField::DEFAULT_VIEW_ANGLE * 0.5f * nSecurity));
	}
}




//=============================================================================
//
//									静的関数
//
//=============================================================================




//生成
CStaticEnemy* CStaticEnemy::Create(const D3DXVECTOR3 pos, D3DXVECTOR3 rot, const float fLookAroundAngle)
{
	CStaticEnemy* pEnemy = new CStaticEnemy;		//インスタンスを生成する

													//初期化処理
	if (FAILED(pEnemy->Init()))
		return nullptr;

	pEnemy->SetPos(pos);							//位置の設定
	pEnemy->SetRot(rot);							//向きの設定
	pEnemy->m_fOriginalRot = rot.y;					//初めの向きの設定
	pEnemy->m_fLookAroundAngle = fLookAroundAngle;	//周りを見る最大の角度の設定

	return pEnemy;									//生成したインスタンスを返す
}




//=============================================================================
//
//								プライベート関数
//
//=============================================================================




//状態によって更新
void CStaticEnemy::UpdateState()
{
	switch (m_state)
	{
	case CStaticEnemy::STATE_LOOK_AROUND:

		//周りを見る
		LookAround();

		if (m_bHeardSound)
		{
			m_nCntAnim = 0;
			m_nCntPhase = 0;
			m_state = STATE_HEARD_SOUND;
		}

		break;

	case CStaticEnemy::STATE_ATTACK:

		//攻撃
		Attack();

		break;

	case CStaticEnemy::STATE_ONLINEATTACK:

		//オンライン攻撃
		Attack(true);

		break;

	case CStaticEnemy::STATE_HEARD_SOUND:

		LookAroundAPoint(GetSoundPos());

		break;

	default:
		break;
	}
}

//周りを見る
void CStaticEnemy::LookAround()
{
	D3DXVECTOR3 rotDest = GetRotDest();			//目的の回転角度の取得

	if (GetSecurityLevel() > 0)
		CEnemy::Rotate();

	switch (m_nCntPhase)
	{
	case 0:

	{
		rotDest.y = m_fOriginalRot + LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);	//回転角度の設定
		m_nCntAnim++;			//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{//90フレームが経ったら

			m_nCntPhase++;		//次へ進む
			m_nCntAnim = 0;		//フレーム数を0に戻す
		}
	}

	break;

	case 1:

	{
		m_nCntAnim++;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			m_nCntAnim = 0;
			m_nCntPhase++;
		}
	}

	break;

	case 2:

	{
		rotDest.y = m_fOriginalRot - LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);		//回転角度の設定
		m_nCntAnim++;				//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{//90フレームが経ったら
			m_nCntPhase++;			//次へ進む
			m_nCntAnim = 0;			//フレーム数を0に戻す
		}
	}

	break;

	case 3:

	{
		m_nCntAnim++;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			m_nCntPhase = 0;
			m_nCntAnim = 0;
		}
	}

	break;

	default:
		break;
	}
}

//位置の周りを見る
void CStaticEnemy::LookAroundAPoint(const D3DXVECTOR3 point)
{
	D3DXVECTOR3 rotDest = GetRotDest();			//目的の回転角度の取得

	switch (m_nCntPhase)
	{
	case 0:

	{
		if (LookAtPos(point))
			m_nCntPhase++;
	}

	break;

	case 1:

	{
		m_nCntAnim++;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			m_nCntAnim = 0;
			m_nCntPhase++;
		}
	}

	break;

	case 2:

	{
		D3DXVECTOR3 angle = D3DXVECTOR3(0.0f, 0.0f, 0.0f), pos = point, unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);


		rotDest.y = m_fTargetRot + LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);	//回転角度の設定
		m_nCntAnim++;			//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{//90フレームが経ったら

			m_nCntPhase++;		//次へ進む
			m_nCntAnim = 0;		//フレーム数を0に戻す
		}
	}

	break;

	case 3:

	{
		m_nCntAnim++;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			m_nCntAnim = 0;
			m_nCntPhase++;
		}
	}

	break;

	case 4:

	{
		rotDest.y = m_fTargetRot - LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);		//回転角度の設定
		m_nCntAnim++;				//フレーム数の更新

		float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

		if (/*m_nCntAnim >= m_nAnimationFrames*/fNum < 0.005f)
		{//90フレームが経ったら
			m_nCntPhase++;			//次へ進む
			m_nCntAnim = 0;			//フレーム数を0に戻す
		}
	}

	break;

	case 5:

	{
		m_nCntAnim++;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			m_nCntPhase = 0;
			m_nCntAnim = 0;
			m_state = STATE_LOOK_AROUND;

			SoundPosReset();
		}
	}

	break;

	default:
		break;
	}
}

//弾を撃つ
void CStaticEnemy::Attack(const bool isOnline)
{
	//プレイヤーのほうに向かう
	LookAtPlayer(isOnline);

	if (Shoot() && !m_bSeen)
	{//攻撃が終わった時、プレイヤーが見えない場合、歩く状態に戻す
		
		m_state = STATE_LOOK_AROUND;
		m_nCntPhase = 0;
		m_nCntAnim = 0;

		if (m_nCntAnim > DEFAULT_WAIT_FRAMES)
		{
			SoundPosReset();
		}
	}
}

//プレイヤーのほうに向かう
void CStaticEnemy::LookAtPlayer(bool isOnline)
{
	CGun* pGun = GetGun();					//銃の取得
	CPlayer* pPlayer = nullptr;//プレイヤーの取得
	if (isOnline)
	{
		pPlayer = CHacker::GetPlayer();	//プレイヤーの取得
	}
	else
	{
		pPlayer = CGame::GetPlayer();	//プレイヤーの取得
	}

	if (!pGun || !pPlayer)
	{
		m_state = STATE_LOOK_AROUND;
		return;
	}

	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - pPlayer->GetPos();
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > pPlayer->GetPos().x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);			//目的の回転角度の設定
}

//ポイントの方に向かう
bool CStaticEnemy::LookAtPos(const D3DXVECTOR3 pos)
{
	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - pos;
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > pos.x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);			//目的の回転角度の設定
	m_fTargetRot = rotDest.y;		//目的の向きを保存する

	float fNum = (GetRot().y - rotDest.y) * (GetRot().y - rotDest.y);

	if (fNum < 0.005f)
	{
		return true;
	}

	return false;
}
