//=============================================================================
//
// swipingImage.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SWIPING_IMAGES_H_		// このマクロ定義がされてなかったら
#define _SWIPING_IMAGES_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CPolygon2D;


class CSwipingImage : public CObject
{
public:
	CSwipingImage();				// コンストラクタ
	~CSwipingImage();				// デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &/*pos*/) override {}					// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}			// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {}					// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {}				// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return D3DXVECTOR3(); }			// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }		// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }		// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }		// 大きさのゲッター

	void SetStartingPos(const D3DXVECTOR3 pos);						//画像の始点のセッター
	void SettargetPos(const D3DXVECTOR3 pos);						//画像の目的の位置のセッター
	void SetEndPos(const D3DXVECTOR3 pos);							//画像の終点のセッター
	void SetImageSize(const D3DXVECTOR3 size);						//画像のサイズのセッター
	void SetImageSpeedIn(const float fSpeed);						//入る時の画像のスピードのセッター
	void SetImageSpeedOut(const float fSpeed);						//出る時の画像のスピードのセッター

	const bool IsUpdating() { return m_bUpdating; }					//更新しているかどうか
																	
	void ChangeImage(const int nNewIdx);							//画像を変更する(引数は新しい画像のインデックスです)

	CPolygon2D* AddImage(int nTextureIdx = DEFAULT_IMAGE_IDX, D3DXCOLOR col = DEFAULT_IMAGE_COLOR);		//新しい画像の生成

	static CSwipingImage* Create(D3DXVECTOR3 startPos = DEFAULT_IMAGE_SPAWN_POS, D3DXVECTOR3 targetPos = DEFAULT_IMAGE_TARGET_POS, D3DXVECTOR3 endPos = DEFAULT_IMAGE_END_POS);		//生成
	
private:

	void CalcSpeed();											//スピードを計算する
	void HasArrived(bool& bIn, bool& bOut);						//画像が着いたかどうかの判定

private:

	static const int			DEFAULT_IMAGE_IDX;				//ディフォルトのテクスチャインデックス
	static const D3DXVECTOR3	DEFAULT_IMAGE_SPAWN_POS;		//ディフォルトのマップの画像のスポーンの位置
	static const D3DXVECTOR3	DEFAULT_IMAGE_TARGET_POS;		//ディフォルトのマップの画像の目的の位置
	static const D3DXVECTOR3	DEFAULT_IMAGE_END_POS;			//ディフォルトのマップの画像の終点
	static const D3DXVECTOR3	DEFAULT_IMAGE_SIZE;				//ディフォルトのマップの画像のサイズ
	static const D3DXCOLOR		DEFAULT_IMAGE_COLOR;			//ディフォルトのマップの画像の色
	static const float			DEFAULT_SWIPE_SPEED_IN;			//ディフォルトの入る時の画像のスピード
	static const float			DEFAULT_SWIPE_SPEED_OUT;		//ディフォルトの出る時の画像のスピード

	D3DXVECTOR3		m_startingPos;				//始点
	D3DXVECTOR3		m_imageSize;				//画像のサイズ
	D3DXVECTOR3		m_targetPos;				//目的の位置
	D3DXVECTOR3		m_endPos;					//終点
	D3DXVECTOR3		m_moveIn;					//入る時の移動量
	D3DXVECTOR3		m_moveOut;					//出る時の移動量
	int				m_nIdxIn;					//選択された画像のインデックス
	int				m_nIdxOut;					//更新される画像のインデックス
	float			m_fSwipeSpeedIn;			//入る時の画像のスピード
	float			m_fSwipeSpeedOut;			//出る時の画像のスピード

	bool			m_bUpdating;				//更新中であるかどうか


	std::vector <CPolygon2D*>	m_vImages;		//画像へのポインタ
};


#endif