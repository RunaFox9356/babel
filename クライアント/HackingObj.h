//=============================================================================
//
// ハッキング可能オブジェクト(HackingObj.h)
// Author : 有田明玄
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================
#ifndef _HACKINGOBJ_H_			// このマクロ定義がされてなかったら
#define _HACKINGOBJ_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include"model_obj.h"
#include"gimmick.h"
#include "collision_rectangle3D.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CModel3D;
class CCollision_Rectangle3D;
class CGimmick;

//=============================================================================
// モデルオブジェクトクラス
// Author : 唐�ｱ結斗
// 概要 : モデルオブジェクト生成を行うクラス
//=============================================================================
class CHackObj : public CGimmick
{	
public:
	//--------------------------------------------------------------------
	// 列挙型
	//--------------------------------------------------------------------
	enum EHackState
	{
		STATE_NOT_HACK = 0,		//未ハッキング
		STATE_NOW_HACKING,		//ハッキング中
		STATE_HACKED,			//ハッキング済み
	};

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CHackObj *Create();							// モデルオブジェクトの生成
	static void LoadFile(const char *pFileName);		// ファイルの読み込み

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CHackObj();
	~CHackObj();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;	// 初期化
	void Uninit() override;		// 終了
	void Update() override;		// 更新
	void Draw() override;		// 描画
	void Hacking();				// ハッキング
	void SetPos(const D3DXVECTOR3 &pos) override { m_HackObj->SetPos(pos); }				// 位置のセッター
	void SetPosDefault(D3DXVECTOR3 pos) { m_posDefault = pos; }																// 位置の設定
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {};				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};			// 大きさのセッター
	void SetHackState(EHackState state) { m_HackState = state; };			//ハッキングの状態
	void SetObj(CModelObj*Obj){ m_HackObj = Obj; };				//ハッキングオブジェクトの取得

	D3DXVECTOR3 GetPos() override { return m_HackObj->GetPos(); }							// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return m_HackObj->GetPosOld(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return m_HackObj->GetRot(); }						// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return m_HackObj->GetSize(); }	// 大きさのゲッター
	CModelObj*	GetObj() { return m_HackObj; };				//ハッキングオブジェクトの取得
	EHackState GetHackState() { return m_HackState; };			//ハッキングの状態
	CCollision_Rectangle3D*	GetCollision() { return m_pCollision; };			//ハッキングの状態

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void SetModelType(int nIdx);								// モデルの種類の設定
	void SetDraw(const bool bDraw);								// 描画するかどうかのフラグセッター


	void Rotate(const int nSwitchRot);						//向きの設定処理
	void HackObjTP();

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	CCollision_Rectangle3D*		m_pCollision;			// 当たり判定
	CModelObj*					m_HackObj;				// モデル
	D3DXVECTOR3					m_posDefault;			// 
	D3DXVECTOR3 m_move;
	bool m_IsLeft;
	CModelObj* m_hitModle;
	EHackState m_HackState;	 //ハッキング周りの状態


};

#endif
