//=============================================================================
//
// watch.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _WATCH_H_		// このマクロ定義がされてなかったら
#define _WATCH_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CScore;
class CTimer;
class CHpGauge;


class CWatch : public CPolygon2D
{
public:

	struct WatchObj
	{
		CPolygon2D* pObj;				//オブジェクトへのポインタ
		D3DXVECTOR3	relativePos;		//相対位置
	};

	CWatch();						//コンストラクタ
	~CWatch() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新

	void AddObj(CScore* pScore, const D3DXVECTOR3 relativePos);	//オブジェクトを追加する
	void AddObj(CTimer* pTimer, const D3DXVECTOR3 relativePos);	//オブジェクトを追加する
	void AddObj(CHpGauge* pGauge, const D3DXVECTOR3 relativePos);	//オブジェクトを追加する

	void SetTextureType(int nType);	//テクスチャの種類の設定

	static CWatch* Create();		//生成処理

private:

	void UpdateObjectPos();			//オブジェクトの位置の更新
	void CheckKey();				//キーボード入力の確認

private:

	enum TextureType
	{
		TEXTURE_TYPE_WATCH = 0,
		TEXTURE_TYPE_TABLET,

		TEXTURE_TYPE_MAX
	};

	static const D3DXVECTOR3		DEFAULT_INACTIVE_POS;		//ディフォルトの画面外の位置
	static const D3DXVECTOR3		DEFAULT_ACTIVE_POS;			//ディフォルトの画面内の位置
	static const D3DXVECTOR3		DEFAULT_SIZE;				//ディフォルトのサイズ
	static const float				DEFAULT_ANIMATION_SPEED;	//ディフォルトのアニメーションの速度
	static const int				DEFAULT_TEXTURE_IDX[TEXTURE_TYPE_MAX];		//テクスチャのインデックス
	static const D3DXVECTOR3		DEFAULT_COMMAND_POS;		//ディフォルトの操作UIの相対位置
	static const D3DXVECTOR3		DEFAULT_COMMAND_SIZE;		//ディフォルトの操作UIのサイズ

	int			m_nState;				//現在の状態(-1 = 画面外、0 = 移動中、+1 = 画面内)
	float		m_fDir;					//移動向き
	D3DXVECTOR3	m_scoreRelativePos;		//スコアの相対位置
	D3DXVECTOR3	m_timerRelativePos;		//タイマーの相対位置
	D3DXVECTOR3	m_GaugeRelativePos;		//HPゲージの相対位置

	CScore*		m_pScore;				//スコアへのポインタ
	CTimer*		m_pTimer;				//タイマーへのポインタ
	CHpGauge*	m_pHpGauge;				//プレイヤーのHPゲージへのポインタ
	CPolygon2D* m_pCommand;				//操作のUIへのポインタ
};

#endif