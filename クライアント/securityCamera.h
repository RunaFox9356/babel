//=============================================================================
//
// securityCamera.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SECURITY_CAMERA_H
#define _SECURITY_CAMERA_H


//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CModelObj;
class CModel3D;
class CPolygon2D;
class CHackObj;
class CUiMessage;
class CHacker;
class CDrone;


class CSecurityCamera : public CGimmick
{
public:
	CSecurityCamera();					//コンストラクタ
	~CSecurityCamera() override;		//デストラクタ


	HRESULT Init() override;						// 初期化
	void Uninit() override;							// 終了
	void Update() override;							// 更新
	void Draw() override;							// 描画

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &posOld) override;			// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &size) override;				// 大きさのセッター

	void SetHackablePc(CHackObj* pPc) { m_pHackablePc = pPc; }	// ハッキングできるPCへのポインタのセッター

	D3DXVECTOR3 GetPos() override { return m_pos; }				// 位置のゲッター
	D3DXVECTOR3 GetPosOld() override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot() override { return m_rot; }				// 向きのゲッター
	D3DXVECTOR3 GetSize() override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	const bool GetHackedState() { return m_bHacked; }			// ハッキング状態であるかどうか

	void SetCameraHackedState(const bool bHacked);				//ハッカーに使われているかどうかの設定処理
	void ActivateCamera();										//このカメラを使うようにする
	void DeactivateCamera();									//カメラを使用されていない状態にする
	void DeactivateCamera(CDrone* pDrone);						//カメラを使用されていない状態にする


	static CSecurityCamera* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot);							//生成
	static CSecurityCamera* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot, CHackObj* pHackablePc);	//生成

	static void GetAllCameras(std::vector<CSecurityCamera*>& vCamera) { vCamera = m_vAllCameras; }			//全部のカメラの取得処理
	static void ClearCameras();				//カメラのクリア処理
	static void ChangeCamera();				//カメラの切り替え処理

private:

	void Animation();			//モデルアニメーション
	void CalcCameraInfo();		//カメラの視点と注視点の計算処理
	void UpdateStaticNoise();	//スノーノイズの更新
	void CreateUiMessage();		//UIメッセージの生成処理
	void DestroyUiMessage();	//UIメッセージの破棄処理

	void HackPC();				//PCのハッキング

	CHacker* GetHackerMode();	//ハッカーモードの取得

private:

	static const D3DXVECTOR3	DEFAULT_MAP_POS_V;				//ディフォルトのマップカメラの視点
	static const D3DXVECTOR3	DEFAULT_MAP_POS_R;				//ディフォルトのマップカメラの注視点
	static const D3DXVECTOR3	DEFAULT_POS_V;					//ディフォルトのカメラの視点(相対)
	static const D3DXVECTOR3	DEFAULT_POS_R;					//ディフォルトのカメラの注視点(相対)
	static const float			DEFAULT_MAX_ROT_Y;				//ディフォルトのY軸の最大回転角度
	static const D3DXVECTOR3	DEFAULT_CAMERA_RELATIVE_POS;	//カメラモデルの相対位置
	static const D3DXVECTOR3	DEFAULT_CAMERA_RELATIVE_ROT;	//カメラモデルの相対向き
	static const float			DEFAULT_FRAME_MOVE;				//ディフォルトの回転量
	static const int			DEFAULT_ANIMATION_DELAY;		//ディフォルトのアニメーションディレイ

	D3DXVECTOR3 m_pos;				//位置
	D3DXVECTOR3 m_rot;				//回転角度

	D3DXVECTOR3 m_posV;				//カメラの視点(相対)
	D3DXVECTOR3 m_posR;				//カメラの注視点(相対)

	float		m_fRotY;			//Y軸の回転角度
	float		m_fMaxRotY;			//Y軸の最大回転角度

	int			m_nDelay;			//動く後のディレイ
	float		m_fFrameMove;		//毎フレーム加算される角度

	bool		m_bHacked;			//ハッカーに使われているかどうか
	bool		m_bHackingPc;		//PCをハッキングしているかどうか

	CModel3D* m_pCameraModel;		//モデル情報へのポインタ
	CModel3D* m_pSupportModel;		//モデル情報へのポインタ

	CPolygon2D* m_pStaticNoise;		//スノーノイズのポリゴンへのポインタ
	CHackObj*	m_pHackablePc;		//ハッキングできるPCへのポインタ(nullだったら、PCがハッキング済み又は、このカメラからハッキングできるPCがない)
	CUiMessage*	m_pUiMessage;		//メッセージのUI

	static D3DXVECTOR3 m_lastRot;	//元のドローンの向き(保存用)

	static std::vector<CSecurityCamera*>	m_vAllCameras;		//全部のカメラ
	static int								m_nCameraIdx;		//選択されているカメラインデックス
};

#endif