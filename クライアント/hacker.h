//=============================================================================
//
// Q[NX(game.h)
// Author : ϊ±l
// Tv : Q[NXΜΗπs€
//
//=============================================================================
#ifndef _HACKER_H_		// ±Μ}Nθ`ͺ³κΔΘ©Α½η
#define _HACKER_H_		// ρdCN[hh~Μ}Nθ`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "game.h"

//*****************************************************************************
// OϋιΎ
//*****************************************************************************
class CPlayer;
class CMesh3D;
class CDrone;
class CPolygon2D;
class CPolygon3D;
class CHackObj;
class CAgent;
class CLine;
class CModelObj;
class CEnemy_Socket;
class CCollision_Rectangle3D;
class CGimmicDoor;
class CViewField;
class CSecurityCamera;
class CTask;
class CModelObj;
class CHackerUI;
class CUiMessage;
//--------------------------------------------------------------------
// JXg
// Author : LcΎΊ
// Tv : JΜ\’Μ
//--------------------------------------------------------------------
class CCameraList
{
public:
	D3DXVECTOR3 PosV;		//JΐW
	D3DXVECTOR3 PosR;
	D3DXVECTOR3 MapPosV;	//JΐW
	D3DXVECTOR3 MapPosR;
	D3DXVECTOR3 rot;		//Jό«
	int number;				//JΤ
	CModelObj*	obj;		//σΜIuWFNg(^[Qbgp)
};
//=============================================================================
// Q[NX
// Author : ϊ±l
// Tv : Q[Ά¬πs€NX
//=============================================================================
class CHacker : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// ΓIoΦ
	//--------------------------------------------------------------------
	static CPlayer *GetPlayer() { return m_pPlayer; }				// vC[
	static void SetGame(const bool bGame) { m_bGame = bGame; }		// Q[Μσ΅Μέθ
	static CDrone *GetDrone() { return m_pDrone; }					// h[
	static CMesh3D* GetMesh() { return m_Mesh; }					// ^XNΜζΎ
	static CTask* GetTask() { return m_pTask; }						// ^XNΜζΎ
	static CHackerUI* GetHackerUI() { return m_pHackerUI; };		//UIζΎ
	
private:
	//--------------------------------------------------------------------
	// θ
	//--------------------------------------------------------------------
	static const int   MAX_ITEM=3;				// g¦ιACeΜ
	static const int   MAX_CAMERA = 5;			// g¦ιJEh[Μ
	static const float ITEM_NORMAL_SIZE;		// ACeκΜUIΜIΞκΔ’Θ’ΜTCY
	static const float ITEM_WIDTH;				// ACeκΜUIzuΤu
	static const float ITEM_DISTANCE_FROM_EDGE;	// [©ηΜ£
	static const int HEAL_RECAST_TIME;			// ρΜLXgΤ
	static const float TIMER_SIZE;			// TEXTUREΜΜ0Μκ
	static const int MAX_SAVE_HACJOBJ = 5;		// ΝΝΰΕnbLOΒ\IuWFNg
	static const int MINIGAME_CLEAR_SCORE;		//~jQ[NAΜXRA



public:
	//--------------------------------------------------------------------
	// ΓIoΦ
	//--------------------------------------------------------------------
	static CPlayer *m_pPlayer;						// vC[NX
	static CDrone *m_pDrone;						// h[
	static bool m_bGame;							// Q[Μσ΅
	static CMesh3D*m_Mesh;
	static CTask* m_pTask;							// ^XN}l[W[
	//--------------------------------------------------------------------
	// RXgN^ΖfXgN^
	//--------------------------------------------------------------------
	CHacker();
	~CHacker() override;

	//--------------------------------------------------------------------
	// ΓIoΟ
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// oΦ
	//--------------------------------------------------------------------
	HRESULT Init() override;					// ϊ»
	void Uninit() override;						// IΉ
	void Update() override;						// XV

private:
	//--------------------------------------------------------------------
	// oΦ
	//--------------------------------------------------------------------
	void ChangeCamera(int CameraNumber);					// JΟX
	void ChangeDrone();										// h[ΙΨθΦ¦
	CHackObj* Hacking();											// nbLO
	void CameraSwitching();									// JΨθΦ¦
	void Hack();
	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIbZ[WΜΆ¬
	void DestroyUiMessage();	//UIbZ[WΜjό

	//--------------------------------------------------------------------
	// oΟ
	//--------------------------------------------------------------------
	CHackObj* m_pHackTg;						//	nbLOΜ^[Qbg
	CHackObj* m_pHackSelectTg[5];				//	ΝΝΰΜ^[Qbg
	int m_Hacknumber;							//	ΝΝΰΜ^[QbgΜ5ͺγΐ
	int m_nNowCamera;							// »έΜJΤ
	
	CLine*		m_pLine;						//	nbNΞΫΖ©ͺΜΤΜC
	CCollision_Rectangle3D* m_pHackRange;		//	nbLOΒ\ΝΝ

	//κI
	static const int SECURITY_CAMERA_NUM = 2;

	CSecurityCamera* m_pSecurityCamera[SECURITY_CAMERA_NUM];

	static CHackerUI*	m_pHackerUI;			//@nbJ[ΜUI
	static CCameraList* m_pCameraList[5];		//@JXg
	CDrone* m_pDroneList[5];					//gpΒ\Θh[ΜXg
	int m_nSelectHackObj;
	int m_Map;
	CPolygon3D*		m_pFloor;					//nΚΦΜ|C^
	CUiMessage*		 m_pUiMessage;								//bZ[WΜUI
};

#endif




