//=============================================================================
//
// ポリゴンクラス(polygon.cpp)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "polygon.h"
#include "renderer.h"
#include "application.h"

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CPolygon::CPolygon(int nPriority/* = PRIORITY_LEVEL0*/) : CObject(nPriority)
{
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 向き
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 大きさ
	m_color = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);		// カラー
	m_nNumTex.clear();									// テクスチャタイプ
	m_pTexture.clear();
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CPolygon::~CPolygon()
{
	
}

//=============================================================================
// ポリゴンの初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CPolygon::Init()
{
	// ポリゴン情報の設定
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 向き
	m_size = D3DXVECTOR3(100.0f, 100.0f, 0.0f);		// 大きさ
	m_nNumTex.clear();								// テクスチャタイプ

	// 頂点バッファの設定
	SetVtx();

	// 色の設定
	SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	// テクスチャ座標の設定
	SetTex(0,D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f));
	SetTex(1,D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f));

	return S_OK;
}

//=============================================================================
// ポリゴンの終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CPolygon::Uninit()
{ 
	// オブジェクト2Dの解放
	Release();
}

//=============================================================================
// ポリゴンの更新
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの更新を行う
//=============================================================================
void CPolygon::Update()
{
	
}

//=============================================================================
// ポリゴンの描画
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの描画を行う
//=============================================================================
void CPolygon::Draw()
{// レンダラーのゲット
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();
	
	// テクスチャポインタの取得
	CTexture *pTexture = CApplication::GetInstance()->GetTexture();
	std::vector<LPDIRECT3DTEXTURE9> pTex;
	pTex.clear();

	//テクスチャの設定
	pRenderer->GetDevice()->SetTexture(0, nullptr);

	int nMaxSize = 0;
	bool bTex = false;

	if ((int)m_pTexture.size() > 0)
	{
		bTex = true;
		nMaxSize = (int)m_pTexture.size();
	}
	else
	{
		nMaxSize = (int)m_nNumTex.size();
	}

	pTex.resize(nMaxSize);

	if (nMaxSize > 0)
	{
		for (int nCnt = 0; nCnt < nMaxSize; nCnt++)
		{
			if (bTex)
			{
				pTex.at(nCnt) = m_pTexture.at(nCnt);
			}
			else
			{
				pTex.at(nCnt) = pTexture->GetTexture(m_nNumTex.at(nCnt));
			}

			pRenderer->GetDevice()->SetTexture(nCnt, pTex.at(nCnt));
		}
	}
	

	//ポリゴン描画
	pRenderer->GetDevice()->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	//テクスチャの設定
	pRenderer->GetDevice()->SetTexture(0, nullptr);
	pRenderer->GetDevice()->SetTexture(1, nullptr);
}

//=============================================================================
// 位置のセッター
// Author : 唐�ｱ結斗
// 概要 : 位置のメンバ変数に引数を代入
//=============================================================================
void CPolygon::SetPos(const D3DXVECTOR3 &pos)
{
	// 位置の設定
	m_pos = pos;

	// 頂点座標などの設定
	SetVtx();
}

//=============================================================================
// 向きのセッター
// Author : 唐�ｱ結斗
// 概要 : 向きのメンバ変数に引数を代入
//=============================================================================
void CPolygon::SetRot(const D3DXVECTOR3 &rot)
{
	// 向きの設定
	m_rot = rot;

	// 頂点座標などの設定
	SetVtx();
}

//=============================================================================
// 大きさのセッター
// Author : 唐�ｱ結斗
// 概要 : 大きさのメンバ変数に引数を代入
//=============================================================================
void CPolygon::SetSize(const D3DXVECTOR3 & size)
{
	// 大きさの設定
	m_size = size;

	// 頂点座標などの設定
	SetVtx();
}

//=============================================================================
// 色の設定
// Author : 唐�ｱ結斗
// 概要 : 頂点カラーを設定する
//=============================================================================
void CPolygon::SetColor(const D3DXCOLOR & color)
{// カラー
	m_color = color;
}

//=============================================================================
// テクスチャの読み込み
// Author : 唐�ｱ結斗
// 概要 : テクスチャの読み込みを行う
//=============================================================================
void CPolygon::LoadTex(const int nNumTex1, const int nNumTex2)
{
	m_nNumTex.clear();

	if (nNumTex1 != -1)
	{
		m_nNumTex.push_back(nNumTex1);
	}

	if (nNumTex2 != -1)
	{
		m_nNumTex.push_back(nNumTex2);
	}
}

//=============================================================================
// テクスチャの読み込み
// Author : 唐�ｱ結斗
// 概要 : テクスチャの読み込みを行う
//=============================================================================
void CPolygon::SetTexture(LPDIRECT3DTEXTURE9 tex1, LPDIRECT3DTEXTURE9 tex2)
{
	m_pTexture.clear();

	if (tex1 != nullptr)
	{
		m_pTexture.push_back(tex1);
	}

	if (tex2 != nullptr)
	{
		m_pTexture.push_back(tex2);
	}
}

