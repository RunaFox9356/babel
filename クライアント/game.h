//=============================================================================
//
// Q[NX(game.h)
// Author : ú±l
// Tv : Q[NXÌÇðs¤
//
//=============================================================================
#ifndef _GAME_H_		// ±Ì}Nè`ª³êÄÈ©Á½ç
#define _GAME_H_		// ñdCN[hh~Ì}Nè`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "scene_mode.h"
#include "collision.h"

//*****************************************************************************
// Oûé¾
//*****************************************************************************
class CAgent;
class CDrone;
class CMesh3D;
class CLine;
class CModelObj;
class CCollision_Rectangle3D;
class CGimmicDoor;
class CPlayer;
class CTask;
class CMap;
class CRadio;
class CAgentUI;
class CStencilCanvas;
class CSecurityTimer;
class CPolygon3D;

//=============================================================================
// Q[NX
// Author : ú±l
// Tv : Q[¶¬ðs¤NX
//=============================================================================
class CGame : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// ÃIoÖ
	//--------------------------------------------------------------------
	static CAgent *GetPlayer() { return m_pPlayer; }				// vC[
	static void SetGame(const bool bGame) { m_bGame = bGame; }		// Q[ÌóµÌÝè
	static CPlayer *GetDrone() { return m_pDrone; }					// h[
	static CTask* GetTask() { return m_pTask; }						// ^XNÌæ¾
	static CMesh3D* GetMesh() { return m_Mesh; }					// ^XNÌæ¾
	static CRadio* GetRadio() { return m_data; }
	static CAgentUI* GetUiManager() { return m_pUiManager; }		//UI}l[W[Ìæ¾
	//--------------------------------------------------------------------
	// RXgN^ÆfXgN^
	//--------------------------------------------------------------------
	CGame();
	~CGame() override;

	//--------------------------------------------------------------------
	// ÃIoÏ
	//--------------------------------------------------------------------
	static CAgent *m_pPlayer;						// vC[NX
	static CPlayer *m_pDrone;						// h[
	static bool m_bGame;							// Q[Ìóµ
	static CTask* m_pTask;							// ^XN}l[W[
	static CAgentUI*	m_pUiManager;				// UI}l[W[
	static CMesh3D*m_Mesh;
	static CRadio* m_data;

	//CEnemy_Socket* m_EnemySocket;


	//---------------
	//JÂM~bN
	//---------------
	bool bOpenflg;
	//---------------


	//--------------------------------------------------------------------
	// oÖ
	//--------------------------------------------------------------------
	HRESULT Init() override;					// ú»
	void Uninit() override;						// I¹
	void Update() override;						// XV

	const int GetSecurityLevel() { return m_nSecurityLevel; }
	void IncreaseSecurity();					

protected:

	void SetStencilCanvas(CStencilCanvas* pObj);	//

private:

	static const int		DEFAULT_ALLERT_TIME;

	void ResultScore();							// XRAXV
	void UpdateSecurityLevel();					// ZLeB[xÌXV


	CStencilCanvas *m_pStencilCanvas;
	int m_Map;
	int m_nSecurityLevel;						//ZLeB[x
	//int m_nAllertTime;							//ZLeB[xÌJE^[
	CSecurityTimer*	m_pSecurityTimer;			//ZLeB^C}[ÌUI
	CPolygon3D*		m_pFloor;					//nÊÖÌ|C^
};

#endif



