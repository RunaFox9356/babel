//=============================================================================
//
// マネージャークラス(manager.h)
// Author : 唐�ｱ結斗
// 概要 : マネージャーの派生を行う
//
//=============================================================================
#ifndef _MINI_MNG_H_		// このマクロ定義がされてなかったら
#define _MINI_MNG_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "manager.h"

class CPolygon2D;
//=============================================================================
// マネージャークラス
// Author : 唐�ｱ結斗
// 概要 : マネージャー生成を行うクラス
//=============================================================================
class CMiniGameMNG:public CManager
{
private:
	enum EGameType
	{
		GAME_TYPE_0 = 0,
		GAME_TYPE_1,
		GAME_TYPE_2,
		GAME_TYPE_MAX
	};

public:
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CMiniGameMNG *Create();	// ミニゲームの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CMiniGameMNG();
	~CMiniGameMNG();
	HRESULT Init()override;
	void Uninit()override;
	void Update()override;
	void Draw()override;
	void LoadMinigame();	//クリア
	void SetClear(bool bClr) { m_bClear = bClr; };
	CPolygon2D* GetMinigame() { return m_Minigame; };	//読み込み
	bool GetClear() { return m_bClear; };	//クリア
	void Cancel();							//ミニゲームキャンセル
private:
	CPolygon2D* m_Minigame;		//ミニゲーム
	bool m_bClear;

};

#endif