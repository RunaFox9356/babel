//=============================================================================
//
// 2Dポリゴンクラス(polygon2D.h)
// Author : 唐�ｱ結斗
// 概要 : ポリゴン生成を行う
//
//=============================================================================
#ifndef _SCORE_H_		// このマクロ定義がされてなかったら
#define _SCORE_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include "number.h"
#include "task.h"
#include "manager.h"

//=============================================================================
// 2Dポリゴンクラス
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴン生成を行うクラス
//=============================================================================
class  CScore : public CPolygon2D
{
public:
	static const int	DEFAULT_AGENT_TASK_SCORE[CTask::AGENT_TASK_MAX];			//エージェントのディフォルトタスクスコア
	static const int	DEFAULT_HACKER_TASK_SCORE[CTask::HACKER_TASK_MAX];			//ハッカーのディフォルトタスクスコア
	static const int	MAX_HACKER_DIGIT = 10;										//最大桁数
	
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CScore *Create(int ndigit);				// 2Dポリゴンの生成
	static CScore *Create(int nPriority,int ndigit);		// 2Dポリゴンの生成(オーバーロード)

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CScore(int nPriority = PRIORITY_LEVEL4);
	~CScore() override;

	//--------------------------------------------------------------------
	// オーバーライド関数
	//--------------------------------------------------------------------
	HRESULT Init() override;																	// 初期化
	void Uninit() override;																		// 終了
	void Update() override;																		// 更新
	void Draw() override;																		// 描画
	
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	void SetPos(D3DXVECTOR3 pos);													// 一括で位置変更
	void SetSize(D3DXVECTOR3 size);													// 一括でサイズ変更
	void AddScore(int add);															// スコア加算
	void SetScore(int add);															// スコア加算
	void ResetScore();																// スコア加算
	int GetScore() { return m_nScore; };
	int GetSendScore();
	void ReSetSendScore() { m_sendAddScore = 0; }
	void SetDigit();
	void SetDigitSize(const D3DXVECTOR3 size);										// 桁のサイズのセッター
private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	int m_nDigit;		//桁数
	int m_nScore;
	int m_sendAddScore;
	CNumber* m_pNumber;
};
#endif



