//=============================================================================
//
// loadingBar.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _LOADING_BAR_H
#define _LOADING_BAR_H


//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================



class CLoadingBar : public CPolygon2D
{
public:

	CLoadingBar();								//コンストラクタ
	~CLoadingBar() override;					//デストラクタ


	HRESULT Init() override;					//初期化
	void Uninit() override;						//終了
	void Update() override;						//更新
	void Draw() override;						//描画

	void SetPos(const D3DXVECTOR3 &pos) override;		// 位置のセッター
	void SetSize(const D3DXVECTOR3 &size) override;		// 大きさのセッター
	
	const bool GetEnd() { return m_bEnd; }		//終わったかどうかのフラグの取得


	void Reset();								//ロードのリセット

	static CLoadingBar* Create(D3DXVECTOR3 pos = DEFAULT_POS, D3DXVECTOR3 size = DEFAULT_SIZE, int nLoadTime = DEFAULT_LOAD_TIME);	//生成

private:

	static const int			DEFAULT_LOAD_TIME;		//ディフォルトのロードに必要なフレーム数
	static const D3DXVECTOR3	DEFAULT_POS;			//ディフォルトの位置
	static const D3DXVECTOR3	DEFAULT_SIZE;			//ディフォルトのサイズ
	static const D3DXCOLOR		DEFAULT_COLOR;			//外側のバーの色
	static const D3DXCOLOR		DEFAULT_INNER_COLOR;	//内側のバーの色


	D3DXVECTOR3		m_innerBarPos;				//内側のバーの相対位置
	D3DXVECTOR3		m_size;						//サイズ
	int				m_nLoadTime;				//ロードに必要なフレーム数
	int				m_nCntTime;					//ロード時間のカウンター
	bool			m_bEnd;						//終わったかどうかのフラグ

	CPolygon2D*		m_pInnerBar;				//内側
};

#endif