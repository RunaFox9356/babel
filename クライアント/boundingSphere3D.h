//=============================================================================
//
// マネージャークラス(manager.h)
// Author : 唐�ｱ結斗
// 概要 : マネージャーの派生を行う
//
//=============================================================================
#ifndef _BOUNDING_SPHERE3D_H_		// このマクロ定義がされてなかったら
#define _BOUNDING_SPHERE3D_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "manager.h"

//=============================================================================
// マネージャークラス
// Author : 唐�ｱ結斗
// 概要 : マネージャー生成を行うクラス
//=============================================================================
class CBoundingSphere3D
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CBoundingSphere3D(D3DXVECTOR3 Pos, float fRadius);
	~CBoundingSphere3D();

public:
	D3DXVECTOR3 m_Pos;
	float m_fRadius;
};

#endif
