//=============================================================================
//
// swipingImage.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "swipingImage.h"
#include "polygon2D.h"



//=============================================================================
// インクルード
//=============================================================================
const int			CSwipingImage::DEFAULT_IMAGE_IDX = -1;									//ディフォルトのテクスチャインデックス
const D3DXVECTOR3	CSwipingImage::DEFAULT_IMAGE_SPAWN_POS = { 640.0f, 830.0f, 0.0f };		//ディフォルトのマップの画像のスポーンの位置
const D3DXVECTOR3	CSwipingImage::DEFAULT_IMAGE_TARGET_POS = { 640.0f, 360.0f, 0.0f };		//ディフォルトのマップの画像の目的の位置
const D3DXVECTOR3	CSwipingImage::DEFAULT_IMAGE_END_POS = { 640.0f, -110.0f, 0.0f };		//ディフォルトのマップの画像の終点
const D3DXVECTOR3	CSwipingImage::DEFAULT_IMAGE_SIZE = { 200.0f, 200.0f, 0.0f };			//ディフォルトのマップの画像のサイズ
const D3DXCOLOR		CSwipingImage::DEFAULT_IMAGE_COLOR = { 1.0f, 1.0f, 1.0f, 1.0f };		//ディフォルトのマップの画像の色
const float			CSwipingImage::DEFAULT_SWIPE_SPEED_IN = 25.0f;							//ディフォルトの画像のスピード
const float			CSwipingImage::DEFAULT_SWIPE_SPEED_OUT = 25.0f;							//ディフォルトの出る時の画像のスピード



//コンストラクタ
CSwipingImage::CSwipingImage() : m_startingPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_imageSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_targetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_endPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_moveIn(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_moveOut(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_fSwipeSpeedIn(0.0f),
m_fSwipeSpeedOut(0.0f),
m_nIdxIn(0),
m_nIdxOut(0),
m_bUpdating(false)
{

}

//デストラクタ
CSwipingImage::~CSwipingImage()
{

}

// 初期化
HRESULT CSwipingImage::Init()
{
	m_startingPos = DEFAULT_IMAGE_SPAWN_POS;		//ディフォルトの始点の設定
	m_targetPos = DEFAULT_IMAGE_TARGET_POS;			//ディフォルトの目的の位置の設定
	m_endPos = DEFAULT_IMAGE_END_POS;				//ディフォルトの終点の設定
	m_imageSize = DEFAULT_IMAGE_SIZE;				//ディフォルトの画像のサイズの設定
	m_nIdxOut = -1;									
	m_fSwipeSpeedIn = DEFAULT_SWIPE_SPEED_IN;		//ディフォルトの入る時の速度の設定
	m_fSwipeSpeedOut = DEFAULT_SWIPE_SPEED_OUT;		//ディフォルトの出る時の速度の設定

	CalcSpeed();									//移動量を計算する

	return S_OK;
}

// 終了
void CSwipingImage::Uninit()
{
	//画像の破棄
	if ((int)m_vImages.size() > 0)
	{//画像がある時だけ

		for (int nCnt = 0; nCnt < (int)m_vImages.size(); nCnt++)
		{
			//終了処理
			m_vImages.data()[nCnt]->Uninit();
		}
	}

	//ベクトルをクリアし、サイズを0に戻す
	m_vImages.clear();
	m_vImages.shrink_to_fit();

	//メモリの解放
	Release();
}

// 更新
void CSwipingImage::Update()
{
	if (m_bUpdating && (int)m_vImages.size() > 1)
	{//更新中で、画像が複数ある時だけ

		//目的の位置に着いたかどうかのフラグを用意する
		bool bIn = false, bOut = false;

		//目的の位置に着いたかどうかを判定する
		HasArrived(bIn, bOut);

		//画面を出る画像がまだ終点に着いていない場合、位置を更新する
		if (!bOut)
			m_vImages.data()[m_nIdxOut]->SetPos(m_vImages.data()[m_nIdxOut]->GetPos() + m_moveOut);

		//画面に入る画像がまだ目的の位置に着いていない場合、位置を更新する
		if (!bIn)
			m_vImages.data()[m_nIdxIn]->SetPos(m_vImages.data()[m_nIdxIn]->GetPos() + m_moveIn);

		//両方の画像が目的の位置についた場合、更新していない状態に戻す
		if (bIn && bOut)
			m_bUpdating = false;
	}
}

// 描画
void CSwipingImage::Draw()
{

}

//画像の始点のセッター
void CSwipingImage::SetStartingPos(const D3DXVECTOR3 pos)
{
	m_startingPos = pos;

	//移動量を計算する
	CalcSpeed();
}

//画像の目的の位置のセッター
void CSwipingImage::SettargetPos(const D3DXVECTOR3 pos)
{
	m_targetPos = pos;

	//移動量を計算する
	CalcSpeed();
}

//画像の終点のセッター
void CSwipingImage::SetEndPos(const D3DXVECTOR3 pos)
{
	m_endPos = pos;

	//移動量を計算する
	CalcSpeed();
}

//画像のサイズのセッター
void CSwipingImage::SetImageSize(const D3DXVECTOR3 size)
{
	m_imageSize = size;

	//既に画像が存在する場合、全部の画像のサイズを変更する
	if ((int)m_vImages.size() > 0)
	{
		for (int nCnt = 0; nCnt < (int)m_vImages.size(); nCnt++)
		{
			m_vImages.data()[nCnt]->SetSize(m_imageSize);
		}
	}
}

//画像の速度のセッター
void CSwipingImage::SetImageSpeedIn(const float fSpeed)
{
	m_fSwipeSpeedIn = fSpeed;		//画像の速度の設定

	CalcSpeed();					//移動量を計算する
}

//出る時の画像のスピードのセッター
void CSwipingImage::SetImageSpeedOut(const float fSpeed)
{
	m_fSwipeSpeedOut = fSpeed;		//画像の速度の設定

	CalcSpeed();					//移動量を計算する
}

//画像を変更する(引数は新しい画像のインデックスです)
void CSwipingImage::ChangeImage(const int nNewIdx)
{
	if (!m_bUpdating && nNewIdx != m_nIdxIn && nNewIdx >= 0 && nNewIdx < (int)m_vImages.size())
	{//存在する画像のインデックスで、更新中ではなかったら

		m_nIdxOut = m_nIdxIn;									//画面を出る画像のインデックスの設定
		m_nIdxIn = nNewIdx;										//画面に入る画像のインデックスの設定
		m_bUpdating = true;										//更新中のフラグをtrueにする
																
		m_vImages.data()[m_nIdxIn]->SetPos(m_startingPos);		//画面に入る画像の位置を始点にする
	}
}

//新しい画像の生成
CPolygon2D * CSwipingImage::AddImage(int nTextureIdx, D3DXCOLOR col)
{
	//画像を生成する
	CPolygon2D* pObj = CPolygon2D::Create();

	if (pObj)
	{//nullチェック

		pObj->SetPos(m_startingPos);	//画像の位置の設定
		pObj->SetSize(m_imageSize);		//画像のサイズの設定
		pObj->SetColor(col);			//画像の色の設定
		std::vector<int> nNumTex;
		nNumTex.push_back(nTextureIdx);
		pObj->LoadTex(nNumTex);		//画像のテクスチャの設定

		m_vImages.push_back(pObj);		//画像を保存する

		if ((int)m_vImages.size() == 1)
		{//最初の画像だったら、目的の位置に配置する

			m_vImages.data()[0]->SetPos(m_targetPos);
		}
	}

	return pObj;						//生成したインスタンスを返す
}

//生成
CSwipingImage* CSwipingImage::Create(D3DXVECTOR3 startPos, D3DXVECTOR3 targetPos, D3DXVECTOR3 endPos)
{
	CSwipingImage* pObj = new CSwipingImage;			//インスタンスを生成する

	//初期化処理
	if (FAILED(pObj->Init()))
		return nullptr;
					
	pObj->SetStartingPos(startPos);		//画像の始点の設定
	pObj->SettargetPos(targetPos);		//画像の目的の位置の設定
	pObj->SetEndPos(endPos);			//画像の終点の設定

	return pObj;						//生成したインスタンスを返す
}

//スピードを計算する
void CSwipingImage::CalcSpeed()
{
	D3DXVECTOR3 dir = m_targetPos - m_startingPos;		//始点から目的の位置までのベクトルを計算する
	D3DXVec3Normalize(&m_moveIn, &dir);					//上のベクトルの長さを1にする
	m_moveIn.x *= m_fSwipeSpeedIn;						//上のベクトルのX座標にスピードを掛けます
	m_moveIn.y *= m_fSwipeSpeedIn;						//上のベクトルのZ座標にスピードを掛けます
														
	dir = m_endPos - m_targetPos;						//目的の位置から終点までのベクトルを計算する
	D3DXVec3Normalize(&m_moveOut, &dir);				//上のベクトルの長さを1にする
	m_moveOut.x *= m_fSwipeSpeedOut;					//上のベクトルのX座標にスピードを掛けます
	m_moveOut.y *= m_fSwipeSpeedOut;					//上のベクトルのZ座標にスピードを掛けます
}

//画像が着いたかどうかの判定
void CSwipingImage::HasArrived(bool& bIn, bool& bOut)
{
	D3DXVECTOR3 P = m_vImages.data()[m_nIdxIn]->GetPos();				//位置の取得

	D3DXVECTOR2 start = D3DXVECTOR2(m_startingPos.x, m_startingPos.y);	//始点
	D3DXVECTOR2 target = D3DXVECTOR2(m_targetPos.x, m_targetPos.y);		//目的の位置
	D3DXVECTOR2 pos = D3DXVECTOR2(P.x, P.y);							//2Dの位置

	D3DXVECTOR2 fromTarget = pos - target;								//目的の位置から画像の位置までのベクトル
	D3DXVECTOR2 vec = pos - start;										//始点から画像の位置までのベクトル
	D3DXVECTOR2 diff = pos - target;									//目的の位置までの距離

	if (D3DXVec2Length(&diff) < 0.5f)
	{//目的の位置までの距離が小さすぎる場合、着いた状態にし、現在の位置を目的の位置にする
		bIn = true;
		m_vImages.data()[m_nIdxIn]->SetPos(m_targetPos);
	}
	else
	{
		D3DXVec2Normalize(&fromTarget, &fromTarget);		//ベクトルを正規化する
		D3DXVec2Normalize(&vec, &vec);						//ベクトルを正規化する
		float fDot = D3DXVec2Dot(&fromTarget, &vec);		//内積を計算する

		if (fDot > 0.0f)
		{//内積の符号が+になると、目的の位置を超えたということなので、着いた状態にし、現在の位置を目的の位置にする
			bIn = true;
			m_vImages.data()[m_nIdxIn]->SetPos(m_targetPos);
		}
	}
	
	P = m_vImages.data()[m_nIdxOut]->GetPos();				//位置の取得

	start = D3DXVECTOR2(m_targetPos.x, m_targetPos.y);		//始点
	target = D3DXVECTOR2(m_endPos.x, m_endPos.y);			//目的の位置
	pos = D3DXVECTOR2(P.x, P.y);							//2Dの位置

	fromTarget = pos - target;								//目的の位置から画像の位置までのベクトル
	vec = pos - start;										//始点から画像の位置までのベクトル
	diff = pos - target;									//目的の位置までの距離

	if (D3DXVec2Length(&diff) < 0.5f)
	{//目的の位置までの距離が小さすぎる場合、着いた状態にし、現在の位置を目的の位置にする
		bOut = true;
		m_vImages.data()[m_nIdxOut]->SetPos(m_endPos);
	}
	else
	{
		D3DXVec2Normalize(&fromTarget, &fromTarget);		//ベクトルを正規化する
		D3DXVec2Normalize(&vec, &vec);						//ベクトルを正規化する
		float fDot = D3DXVec2Dot(&fromTarget, &vec);		//内積を計算する

		if (fDot > 0.0f)
		{//内積の符号が+になると、目的の位置を超えたということなので、着いた状態にし、現在の位置を目的の位置にする
			bOut = true;
			m_vImages.data()[m_nIdxOut]->SetPos(m_endPos);
		}
	}
}
