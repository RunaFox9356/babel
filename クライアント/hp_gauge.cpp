//=============================================================================
//
// HPQ[WNX(hp_gauge.cpp)
// Author : ú±l
// Tv : IuWFNg¶¬ðs¤
//
//=============================================================================

//*****************************************************************************
// CN[h
//*****************************************************************************
#include <assert.h>

#include "hp_gauge.h"
#include "polygon2D.h"
#include "polygon3D.h"
#include "gauge2D.h"
#include "gauge3D.h"
#include "renderer.h"
#include "calculation.h"
#include "application.h"

//=============================================================================
// CX^X¶¬
// Author : ú±l
// Tv : IuWFNgð¶¬·é
//=============================================================================
CHpGauge * CHpGauge::Create(DROW_TYPE EDrowType)
{
	// IuWFNgCX^X
	CHpGauge *pHpGauge = nullptr;

	// Ìðú
	pHpGauge = new CHpGauge();

	if (pHpGauge != nullptr)
	{// lÌú»
		pHpGauge->Init(EDrowType);
	}
	else
	{// ÌmÛªÅ«È©Á½
		assert(false);
	}

	// CX^XðÔ·
	return pHpGauge;
}

//=============================================================================
// RXgN^
// Author : ú±l
// Tv : CX^X¶¬És¤
//=============================================================================
CHpGauge::CHpGauge()
{
	m_pPolygon[0] = nullptr;
	m_pPolygon[1] = nullptr;							
	m_pPolygon[2] = nullptr;
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// Êu
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// ü«
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// å«³
	m_fRatio = 0.f;										// å«³Ìä¦
}

//=============================================================================
// fXgN^
// Author : ú±l
// Tv : CX^XI¹És¤
//=============================================================================
CHpGauge::~CHpGauge()
{

}

//=============================================================================
// |SÌú»
// Author : ú±l
// Tv : ¸_obt@ð¶¬µAoÏÌúlðÝè
//=============================================================================
HRESULT CHpGauge::Init()
{
	// |SîñÌÝè
	m_pPolygon[0] = nullptr;
	m_pPolygon[1] = nullptr;
	m_pPolygon[2] = nullptr;
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// Êu
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// ü«
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// å«³
	m_fRatio = 0.f;										// å«³Ìä¦

	return S_OK;
}

//=============================================================================
// |SÌú»
// Author : ú±l
// Tv : ¸_obt@ð¶¬µAoÏÌúlðÝè
//=============================================================================
HRESULT CHpGauge::Init(DROW_TYPE EDrowType)
{
	// |SîñÌÝè
	m_pPolygon[0] = nullptr;
	m_pPolygon[1] = nullptr;
	m_pPolygon[2] = nullptr;
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// Êu
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// ü«
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// å«³
	m_fRatio = 0.f;										// å«³Ìä¦

	SetDrowType(EDrowType);

	return S_OK;
}

//=============================================================================
// |SÌI¹
// Author : ú±l
// Tv : eNX`Ì|C^Æ¸_obt@Ìðú
//=============================================================================
void CHpGauge::Uninit()
{
	// IuWFNg2DÌðú
	Release();
}

//=============================================================================
// |SÌXV
// Author : ú±l
// Tv : 2D|SÌXVðs¤
//=============================================================================
void CHpGauge::Update()
{

}

//=============================================================================
// |SÌ`æ
// Author : ú±l
// Tv : 2D|SÌ`æðs¤
//=============================================================================
void CHpGauge::Draw()
{

}

//=============================================================================
// `æû@ÌÝè
// Author : ú±l
// Tv : `æû@ðÝèµAQ[Wp|SÌÝè
//=============================================================================
void CHpGauge::SetDrowType(DROW_TYPE EDrowType)
{
	m_EDrowType = EDrowType;

	for (int nCnt = 0; nCnt < MAX_POLYGON; nCnt++)
	{
		if (m_pPolygon[nCnt] != nullptr)
		{
			m_pPolygon[nCnt]->Uninit();
			m_pPolygon[nCnt] = nullptr;
		}
	}

	switch (m_EDrowType)
	{
	case CHpGauge::TYPE_2D:
		m_pPolygon[0] = CPolygon2D::Create();
		m_pPolygon[1] = CPolygon2D::Create(PRIORITY_LEVEL4);
		m_pPolygon[1]->SetColor(D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f));
		m_pPolygon[2] = CGauge2D::Create(PRIORITY_LEVEL4);
		m_pPolygon[2]->SetColor(D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f));
		break;

	case CHpGauge::TYPE_3D:
		assert(false);
		break;

	default:
		break;
	}
}

//=============================================================================
// ÊuÌZb^[
// Author : ú±l
// Tv : ÊuÌoÏÉøðãü
//=============================================================================
void CHpGauge::SetPos(const D3DXVECTOR3 &pos)
{
	// ÊuÌÝè
	m_pos = pos;
	Setting();
}

//=============================================================================
// ü«ÌZb^[
// Author : ú±l
// Tv : ü«ÌoÏÉøðãü
//=============================================================================
void CHpGauge::SetRot(const D3DXVECTOR3 &rot)
{
	// ü«ÌÝè
	m_rot = rot;
	Setting();
}

//=============================================================================
// å«³ÌZb^[
// Author : ú±l
// Tv : å«³ÌoÏÉøðãü
//=============================================================================
void CHpGauge::SetSize(const D3DXVECTOR3 & size)
{
	// å«³ÌÝè
	m_size = size;
	Setting();
}

//=============================================================================
// Q[WTCYÌä¦ÌÝè
// Author : ú±l
// Tv : Q[WÌÝèðs¤
//=============================================================================
void CHpGauge::SetRatio(const float fRatio)
{
	m_fRatio = fRatio;
	SetMaxSize(GetSize());
	Setting();
}

//=============================================================================
// Q[WÌÅåTCYÌÝè
// Author : ú±l
// Tv : Q[WÌÅålðÝè·éB
//=============================================================================
void CHpGauge::SetMaxSize(D3DXVECTOR3 size)
{
	D3DXVECTOR3 maxSize = size * m_fRatio;
	SetSize(size);

	switch (m_EDrowType)
	{
	case CHpGauge::TYPE_2D:
	{
		CGauge2D *pGauge = (CGauge2D*)m_pPolygon[2];
		pGauge->SetMaxSize(maxSize);
	}
	break;

	case CHpGauge::TYPE_3D:
		assert(false);
		break;

	default:
		break;
	}
}

//=============================================================================
// Q[WÌÅålÌÝè
// Author : ú±l
// Tv : Q[WÌÅålðÝè·éB
//=============================================================================
void CHpGauge::SetMaxNumber(const float fMaxNumber)
{
	switch (m_EDrowType)
	{
	case CHpGauge::TYPE_2D:
	{
		CGauge2D *pGauge = (CGauge2D*)m_pPolygon[2];
		pGauge->GaugeReset(fMaxNumber);
		pGauge->SetCoefficient(0.1f);
	}
	break;

	case CHpGauge::TYPE_3D:
		assert(false);
		break;

	default:
		break;
	}
}

//=============================================================================
// Q[WÌ¸lÌÝè
// Author : ú±l
// Tv : Q[WÌÅålðÝè·éB
//=============================================================================
void CHpGauge::SetNumber(const float fNumber)
{
	switch (m_EDrowType)
	{
	case CHpGauge::TYPE_2D:
	{
		CGauge2D *pGauge = (CGauge2D*)m_pPolygon[2];
		pGauge->SetNumber(fNumber);
	}
		break;

	case CHpGauge::TYPE_3D:
		assert(false);
		break;

	default:
		break;
	}
}

//=============================================================================
// Q[WÌZbeBO
// Author : ú±l
// Tv : Q[WÌÝèðs¤
//=============================================================================
void CHpGauge::Setting()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();
	D3DXVECTOR3 size = GetSize();

	// å«³ÌÝè
	D3DXVECTOR3 subSize = size * m_fRatio;
	m_pPolygon[0]->SetSize(size);
	m_pPolygon[1]->SetSize(subSize);
	m_pPolygon[2]->SetSize(subSize);

	// ü«ÌÝè
	m_pPolygon[0]->SetRot(rot);
	m_pPolygon[1]->SetRot(rot);
	m_pPolygon[2]->SetRot(rot);

	// Êu
	D3DXVECTOR3 gaugePos;
	float fRot = rot.z + D3DX_PI * 2.0f;
	float fLength = sqrtf((subSize.x * subSize.x) + (subSize.y * subSize.y)) / 2.0f;
	fRot = CCalculation::RotNormalization(fRot);
	gaugePos.x = pos.x + sinf(fRot) * fLength;
	gaugePos.y = pos.y + cosf(fRot) * fLength;
	gaugePos.z = pos.z;

	m_pPolygon[0]->SetPos(pos);
	m_pPolygon[1]->SetPos(pos);
	m_pPolygon[2]->SetPos(gaugePos);
}

