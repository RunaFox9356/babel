#ifndef _EscapePoint_H_		// このマクロ定義がされてなかったら
#define _EscapePoint_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "model_obj.h"
#include "gimmick.h"
#include "cylinder.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CModelObj;
class CGimmick;
class CCollision_Rectangle3D;
class CEmptyObj;
class CUiMessage;
class CPlayer;
class CEndAnimation;

class CEscapePoint :public CGimmick
{
public:
	static CEscapePoint *Create(D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f));							// モデルオブジェクトの生成


	CEscapePoint();
	~CEscapePoint()override;

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override {}			// 描画


	D3DXVECTOR3 GetPos() override { return m_model->GetPos(); }							// 位置のゲッター
	void SetPos(const D3DXVECTOR3 &pos) override { m_model->SetPos(pos); m_pCylinder->SetPos(pos); }				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3& /*posOld*/) override {}								// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &/*rot*/) override {};									// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};									// 大きさのセッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }								// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }								// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }								// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定

private:

	void LookForPlayer();		//プレイヤーまでの距離を判定する

	void CreateUiMessage(const char* pMessage, const int nTexIdx);				//UIメッセージの生成処理
	void DestroyUiMessage();			//UIメッセージの破棄処理

private:

	static const int			DEFAULT_CYLINDER_MODEL;					//ディフォルトのシリンダーのモデルインデックス
	static const int			DEFAULT_CYLINDER_TEXTURE;				//ディフォルトのシリンダーのテクスチャインデックス
	static const float			DEFAULT_CYLINDER_RADIUS;				//シリンダーの半径
	static const D3DXVECTOR3	DEFAULT_CYLINDER_SIZE;					//シリンダーのサイズ
	static const D3DXCOLOR		DEFAULT_CYLINDER_COLOR;					//シリンダーの色
	static const D3DXVECTOR2	DEFAULT_TEXTURE_ANIMATION_SPEED;		//ディフォルトのシリンダーのテクスチャアニメーションの速度
	static const D3DXVECTOR3	DEFAULT_AREA_HITBOX_POS;				//エスケープできる範囲の相対位置
	static const D3DXVECTOR3	DEFAULT_AREA_HITBOX_SIZE;				//エスケープできる範囲のサイズ


	CCollision_Rectangle3D *pCollisionEscape;
	CModelObj* m_hitModle;
	CEmptyObj* m_emptyObj;
	CModelObj* m_model;
	CCylinder* m_pCylinder;

	bool Goalflg;

	CUiMessage*			m_pUiMessage;				//メッセージのUI
	CPlayer*			m_pPlayer;					//プレイヤーへのポインタ
	CEndAnimation*		m_pEndAnim;					//演出アニメーションへのポインタ
};


#endif
