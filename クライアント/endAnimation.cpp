//=============================================================================
//
// endAnimation.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "endAnimation.h"




//コンストラクタ
CEndAnimation::CEndAnimation() : m_bEnd(false),
m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_rot(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
{

}

//デストラクタ
CEndAnimation::~CEndAnimation()
{

}

// 初期化
HRESULT CEndAnimation::Init()
{
	return S_OK;
}

// 終了
void CEndAnimation::Uninit()
{

}

// 更新
void CEndAnimation::Update()
{

}

// 描画
void CEndAnimation::Draw()
{

}