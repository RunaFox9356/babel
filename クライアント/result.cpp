//=============================================================================
//
// リザルトクラス(result.cpp)
// Author : 唐�ｱ結斗
// 概要 : リザルトクラスの管理を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "result.h"
#include "input.h"
#include "sound.h"
#include "debug_proc.h"
#include "camera.h"
#include "application.h"
#include "ranking.h"
#include "polygon2D.h"
#include "model_obj.h"
#include "polygon3D.h"
#include "model3D.h"
#include "utility.h"
#include "resultScoreUI.h"
#include "client.h"

//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXCOLOR		CResult::DEFAULT_NORMAL_COLOR = { 0.8f, 0.2f, 0.2f, 1.0f };				// ディフォルトの普通の色
const D3DXCOLOR		CResult::DEFAULT_ANIM_COLOR = { 0.8f, 0.8f, 0.2f, 1.0f };				// ディフォルトのアニメーション用の色

namespace
{
	float fCameraOffset = 0.0f;
};


//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CResult::CResult() : m_nRankedScoreIdx(0),
m_nCntFrame(0),
m_fScroll(0.0f),
m_pBg(nullptr),
m_pGround(nullptr),
m_pStreetlight(nullptr),
m_guardrailSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_roadSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f))
{
	for (int nCnt = 0; nCnt < 5; nCnt++)
	{
		m_pRank[nCnt] = nullptr;

		for (int nCnt2 = 0; nCnt2 < 5; nCnt2++)
		{
			m_pScore[nCnt][nCnt2] = nullptr;
		}

		m_pGuardrail[nCnt] = nullptr;
	}

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pRoad[nCnt] = nullptr;
	}
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CResult::~CResult()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CResult::Init()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM002);
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);

	fCameraOffset = 0.0f;
	pCamera->BlockCamera(true);
	pCamera->SetPosV(D3DXVECTOR3(-25.0f + fCameraOffset, 50.0f, -150.0f));
	pCamera->SetPosR(D3DXVECTOR3(-25.0f + fCameraOffset, 25.0f, 0.0f));
	m_bPressEnter = true;
	{
		m_pGround = CPolygon3D::Create();

		if (m_pGround)
		{
			m_pGround->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
			m_pGround->SetSize(D3DXVECTOR3(10000.0f, 10000.0f, 0.0f));
			m_pGround->SetRot(D3DXVECTOR3(D3DX_PI * 0.5f - 0.02f, 0.0f, 0.0f));
			m_pGround->LoadTex(91);
			m_pGround->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(1000.0f, 1000.0f));
		}
	}

	m_nextMode = CApplication::MODE_MAPSELECT;		//次のモードの設定

	//{//背景の生成
	//	CPolygon2D* pObj = CPolygon2D::Create();

	//	if (pObj)
	//	{//nullチェック
	//		pObj->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));		//位置の設定
	//		pObj->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));		//色の設定
	//		pObj->SetSize(D3DXVECTOR3(1280.0f, 720.0f, 0.0f));		//サイズの設定
	//	}
	//}

	{
		m_pBg = CPolygon3D::Create();

		if (m_pBg)
		{
			m_pBg->SetPos(D3DXVECTOR3(0.0f, 0.0f, 1000.0f));
			m_pBg->SetSize(D3DXVECTOR3(2560.0f * 5.0f, 480.0f * 5.0f, 0.0f));
			m_pBg->LoadTex(1);
			m_pBg->SetBillboard(true);
		}
	}

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pRoad[nCnt] = CModelObj::Create();

		if (m_pRoad[nCnt])
		{
			m_pRoad[nCnt]->SetType(17);
			m_pRoad[nCnt]->SetShadowDraw(false);

			CModel3D* pModel = m_pRoad[nCnt]->GetModel();

			if (pModel)
			{
				m_roadSize = pModel->GetMyMaterial().size;
			}

			m_pRoad[nCnt]->SetPos(D3DXVECTOR3(m_roadSize.x * nCnt, -40.0f, 0.0f));
		}
	}

	{
		m_pCar = CModelObj::Create();

		if (m_pCar)
		{
			m_pCar->SetType(213);
			m_pCar->SetPos(D3DXVECTOR3(0.0f, 0.0f, 150.0f));
		}
	}

	{
		CPolygon2D* pObj = CPolygon2D::Create(CSuper::PRIORITY_LEVEL2);

		if (pObj)
		{
			pObj->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));
			pObj->SetSize(D3DXVECTOR3(1280.0f, 720.0f, 0.0f));
			pObj->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.4f));
		}
	}

	//ランキングマネージャーの取得
	CRanking* pRanking = CApplication::GetInstance()->GetRanking();
	std::vector<int> vTask;
	bool bAgent = false;
	pRanking->GetCompletedTask(vTask, bAgent);

	m_pScoreResumeUI = CResultScoreUI::Create(bAgent, vTask);

	{
		for (int nCnt = 0; nCnt < 5; nCnt++)
		{
			m_pGuardrail[nCnt] = CModelObj::Create();

			if (m_pGuardrail[nCnt])
			{
				m_pGuardrail[nCnt]->SetType(230);

				CModel3D* pModel = m_pGuardrail[nCnt]->GetModel();

				if (pModel && nCnt == 0)
				{
					m_guardrailSize = pModel->GetMyMaterial().size;
				}

				m_pGuardrail[nCnt]->SetPos(D3DXVECTOR3(-2.0f * m_guardrailSize.x + nCnt * m_guardrailSize.x, 0.0f, 200.0f));
				m_pGuardrail[nCnt]->SetShadowDraw(false);
			}
		}

		m_pStreetlight = CModelObj::Create();

		if (m_pStreetlight)
		{
			m_pStreetlight->SetType(231);
			m_pStreetlight->SetPos(D3DXVECTOR3(200.0f, 0.0f, 250.0f));
		}
	}

	CInput *pInput = CInput::GetKey();
	pInput->SetCursorLock(false);
	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CResult::Uninit()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	//カメラを取得し、動ける用にする
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->BlockCamera(false);

	//前回のスコアとランキングをクリアする
	CRanking* pRanking = CApplication::GetInstance()->GetRanking();

	if (pRanking)
		pRanking->ClearLastScoreAndRank();

	//UIのポインタをnullに戻す
	for (int nCnt = 0; nCnt < 5; nCnt++)
	{
		if(m_pRank[nCnt])
			m_pRank[nCnt] = nullptr;

		for (int nCnt2 = 0; nCnt2 < 5; nCnt2++)
		{
			if (m_pScore[nCnt][nCnt2])
				m_pScore[nCnt][nCnt2] = nullptr;
		}

		if (m_pGuardrail[nCnt])
			m_pGuardrail[nCnt] = nullptr;
	}
	if (m_pStreetlight)
	{
		m_pStreetlight = nullptr;
	}

	//背景のポインタをnullにする
	if (m_pBg)
	{
		m_pBg = nullptr;
	}
	if (m_pGround)
		m_pGround = nullptr;

	//モデルのポインタをnullに戻す
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pRoad[nCnt] = nullptr;
	}

	//スコアの詳細の破棄
	if (m_pScoreResumeUI)
	{
		//m_pScoreResumeUI->Uninit();
		m_pScoreResumeUI = nullptr;
	}

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CResult::Update()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	if (m_pBg)
	{
		m_fScroll += 0.0001f;
		m_pBg->SetTex(0, D3DXVECTOR2(m_fScroll, 0.0f), D3DXVECTOR2(m_fScroll + 1.0f, 1.0f));

		CCamera *pCamera = CApplication::GetInstance()->GetCamera();

		if (pCamera)
		{
			fCameraOffset = 50.0f * sinf(m_fScroll * D3DX_PI * 20.0f);
			pCamera->SetPosV(D3DXVECTOR3(-25.0f + fCameraOffset, 50.0f, -150.0f));
			pCamera->SetPosR(D3DXVECTOR3(-25.0f + fCameraOffset, 25.0f, 0.0f));
		}
	}

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pRoad[nCnt])
		{
			D3DXVECTOR3 pos = m_pRoad[nCnt]->GetPos();

			pos.x -= 10.0f;

			if (pos.x < -m_roadSize.x)
			{
				pos.x += m_roadSize.x * 2.0f;
			}

			m_pRoad[nCnt]->SetPos(pos);
		}
	}

	for (int nCnt = 0; nCnt < 5; nCnt++)
	{
		if (m_pGuardrail[nCnt])
		{
			D3DXVECTOR3 pos = m_pGuardrail[nCnt]->GetPos();

			pos.x -= 10.0f;

			if (pos.x < -m_guardrailSize.x * 2.5f)
			{
				pos.x += m_guardrailSize.x * 5.0f;
			}

			m_pGuardrail[nCnt]->SetPos(pos);
		}
	}

	if (m_pStreetlight)
	{
		D3DXVECTOR3 pos = m_pStreetlight->GetPos();

		pos.x -= 10.0f;

		if (pos.x < -700.0f)
		{
			pos.x = 700.0f;
		}

		m_pStreetlight->SetPos(pos);
	}

	//AutoTransition();
	m_nCntFrame++;

	if (m_pScoreResumeUI)
	{
		if (m_pScoreResumeUI->GetEnd())
		{
			m_pScoreResumeUI->Uninit();
			m_pScoreResumeUI = nullptr;
			CreateRankingUI();
		}
	}
	else
		ScoresUpdate();

	if (!m_pScoreResumeUI
		&& m_bPressEnter
		&& pInput->Trigger(DIK_RETURN))
	{
		pSound->PlaySound(CSound::SOUND_LABEL_SE_DECIDE);
		m_bPressEnter = false;
		m_isConnect = SetMapRecv();
	}

	if (m_isConnect)
	{

		CApplication::GetInstance()->SetNextMode(CApplication::MODE_MAPSELECT);
	}

#ifdef _DEBUG
	// デバック表示
	CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル| F7 デバック表示削除\n");

	if (pInput->Trigger(DIK_F1))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_AGENT);
	}
	if (pInput->Trigger(DIK_F3))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_HACKER);
	}
	if (pInput->Trigger(DIK_F4))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TITLE);
	}

#endif // _DEBUG

}

//=============================================================================
// 自動遷移
// Author : 唐�ｱ結斗
// 概要 : 自動で画面遷移する
//=============================================================================
void CResult::AutoTransition()
{
	if (m_nCntFrame >= 600)
	{
		m_bPressEnter = false;
		m_nextMode = CApplication::MODE_MAPSELECT;
		m_nCntFrame = 0;
		CApplication::GetInstance()->SetNextMode(m_nextMode);
	}
}

//スコアのアニメーション
void CResult::ScoresUpdate()
{
	if (m_nRankedScoreIdx >= 0 && m_nRankedScoreIdx < 5)
	{
		//7フレームが経ったら、色を変える(赤→金色→赤...)
		if (m_nCntFrame % 14 == 0)
		{
			for (int nCnt = 0; nCnt < 5; nCnt++)
				m_pScore[m_nRankedScoreIdx][nCnt]->SetColor(DEFAULT_ANIM_COLOR);
		}
		else if (m_nCntFrame % 14 == 7)
		{
			for (int nCnt = 0; nCnt < 5; nCnt++)
				m_pScore[m_nRankedScoreIdx][nCnt]->SetColor(DEFAULT_NORMAL_COLOR);
		}
	}
}

//ランキングUIの生成処理
void CResult::CreateRankingUI()
{
	//ランキングマネージャーの取得
	CRanking* pRanking = CApplication::GetInstance()->GetRanking();
	std::vector<int> vScores;
	int nScore = 0;				//スコア
	m_nRankedScoreIdx = -1;		//ランク

	if (pRanking)
	{//nullチェック

		pRanking->GetLastScoreAndRank(nScore, m_nRankedScoreIdx);	//前回のスコアとランクの取得
		pRanking->GetAllScores(vScores);							//全部のスコアの取得
	}

	D3DXVECTOR3 pos = D3DXVECTOR3(540.0f, 250.0f, 0.0f);			//位置の設定
	D3DXVECTOR3 size = D3DXVECTOR3(50.0f, 50.0f, 0.0f);				//サイズの設定

	for (int nCntScore = 0; nCntScore < (int)vScores.size(); nCntScore++)
	{
		int nNum = 10000;							//計算用の変数
		nScore = vScores.data()[nCntScore];			//スコア

													//ランクのUIの生成
		m_pRank[nCntScore] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL4);

		if (m_pRank[nCntScore])
		{//nullチェック

			m_pRank[nCntScore]->SetPos(pos + D3DXVECTOR3(size.x * -2.0f, size.y * nCntScore, 0.0f));	//位置の設定
			m_pRank[nCntScore]->SetSize(size);															//サイズの設定
			m_pRank[nCntScore]->LoadTex(2);																//テクスチャの設定
			m_pRank[nCntScore]->SetColor(DEFAULT_NORMAL_COLOR);											//色の設定
			m_pRank[nCntScore]->SetTex(0, D3DXVECTOR2(0.1f + (0.1f * nCntScore), 0.0f), D3DXVECTOR2(0.2f + (0.1f * nCntScore), 1.0f));		//テクスチャ座標の設定
		}

		for (int nCnt = 0; nCnt < 5; nCnt++)
		{
			//スコアUIの生成
			m_pScore[nCntScore][nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL4);

			if (m_pScore[nCntScore][nCnt])
			{//nullチェック

				int nDigit = nScore / nNum;																		//スコアの桁を計算する
				m_pScore[nCntScore][nCnt]->SetPos(pos + D3DXVECTOR3(size.x * nCnt, size.y * nCntScore, 0.0f));	//位置の設定
				m_pScore[nCntScore][nCnt]->SetSize(size);														//サイズの設定
				m_pScore[nCntScore][nCnt]->LoadTex(2);															//テクスチャの設定
				m_pScore[nCntScore][nCnt]->SetColor(DEFAULT_NORMAL_COLOR);										//色の設定
				m_pScore[nCntScore][nCnt]->SetTex(0, D3DXVECTOR2(0.0f + (0.1f * nDigit), 0.0f), D3DXVECTOR2(0.1f + (0.1f * nDigit), 1.0f));		//テクスチャ座標の設定

			}

			nScore = nScore % nNum;
			nNum /= 10;
		}
	}
}

//=============================================================================
// オンラインでデータを取得する
// Author : 浜田琉雅
// 概要 : モードデータを出力する
//=============================================================================
bool CResult::SetMapRecv()
{
	bool comand = false;
	bool comand2 = true;
	CClient *pTcp = CApplication::GetInstance()->GetClient();	// 通信クラスの取得
	//pTcp->Send((char*)comand2, sizeof(bool), CClient::TYPE_TCP);
	char aRecvData[1024];	// 受信データ
	while (!comand)
	{
		pTcp->Recv(aRecvData, sizeof(bool), CClient::TYPE_TCP);
		memcpy(&comand, &aRecvData[0], sizeof(bool));
	}
	return comand;
}

