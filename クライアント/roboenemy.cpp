//=============================================================================
//
// walkingEnemy.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "roboenemy.h"
#include "game.h"
#include "agent.h"
#include "viewField.h"
#include "move.h"
#include "calculation.h"
#include "motion.h"
#include "collision_rectangle3D.h"

#include "debug_proc.h"
#include "input.h"

#include "hacker.h"

#include "application.h"

//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3 CRoboEnemy::EYE_RELATIVE_POS = D3DXVECTOR3(0.0f, 42.0f, 0.0f);			//目の相対位置
const float		  CRoboEnemy::TARGET_RADIUS = 10.0f;										//ターゲットの半径
const float		  CRoboEnemy::LOOK_AROUND_ANGLE = D3DX_PI * 0.25f;						//周りを見る時の加算される角度
const D3DXVECTOR3 CRoboEnemy::DEFAULT_COLLISION_POS = D3DXVECTOR3(0.0f, 30.0f, 0.0f);	//ディフォルトの当たり判定の相対位置
const D3DXVECTOR3 CRoboEnemy::DEFAULT_COLLISION_SIZE = D3DXVECTOR3(30.0f, 60.0f, 30.0f);	//ディフォルトの当たり判定のサイズ
const int		  CRoboEnemy::DEFAULT_HAND_INDEX = 3;									//ディフォルトの手のモデルのインデックス


//=============================================================================
//コンストラクタ
//=============================================================================
CRoboEnemy::CRoboEnemy() : m_nCurrentTarget(0),
m_newTarget(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nCntAnim(0),
m_nCntPhase(0),
m_bSeen(false),
m_pViewField(nullptr)
{
	for (int nCnt = 0; nCnt < MAX_TARGET_POS; nCnt++)
	{
		m_targetPos[nCnt] = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}
}

//=============================================================================
//デストラクタ
//=============================================================================
CRoboEnemy::~CRoboEnemy()
{

}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CRoboEnemy::Init()
{
	//親クラスの初期化処理
	CEnemy::Init();
	CApplication::GetInstance()->GetSceneMode()->SetEnemyUse(true, GetId());
	m_pViewField = CViewField::Create(this);		//視野の生成

	if (m_pViewField)
	{//nullチェック

		m_pViewField->SetPosV(EYE_RELATIVE_POS);					//視野の相対位置の設定
		m_pViewField->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));		//視野の相対回転の設定
	}

	//移動用の情報の設定
	CMove* pMove = GetMove();

	if (pMove)
	{
		pMove->SetMoving(0.2f, 1000.0f, 0.1f, 0.1f);
	}

	m_state = STATE_WALKING;			//現在の状態の設定	

	CCollision_Rectangle3D* pCollision = GetCollision();		//当たり判定の取得
	CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
	if (pCollision)
	{//当たり判定のサイズと相対位置の設定

		GetCollision()->SetSize(DEFAULT_COLLISION_SIZE);
		GetCollision()->SetPos(DEFAULT_COLLISION_POS);
	}


	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CRoboEnemy::Uninit()
{
	//視野の破棄
	if (m_pViewField)
	{
		m_pViewField->Uninit();
		m_pViewField = nullptr;
	}

	//親クラスの終了処理
	CEnemy::Uninit();
}

//=============================================================================
// 更新
//=============================================================================
void CRoboEnemy::Update()
{
	CreateGun(DEFAULT_HAND_INDEX);

	if (m_pViewField)
	{//視野のnullチェック	
		CPlayer* checkPlayer = CHacker::GetPlayer();		//プレイヤーの取得
		if (checkPlayer && CApplication::GetInstance()->GetEnemyConnect())
		{//ハッカーでは接続されてないと動かなくなるぞ！	
			SetPos(CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].Pos);
		}
		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDiscovery)
		{
			m_bSeen = true;
			m_state = STATE_ONLINEATTACK;
		}
		else
		{

			m_bSeen = false;
		}
		CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

		if (pPlayer && !pPlayer->GetHiddenState())
		{//プレイヤーが見える状態だったら

		 //プレイヤーが見えるかどうかを確認する
			D3DXVECTOR3 pos = pPlayer->GetPos();

			if (m_pViewField->IsPontInView(pos) ||
				m_pViewField->IsPontInView(pos + D3DXVECTOR3(0.0f, 25.0f, 0.0f)) ||
				m_pViewField->IsPontInView(pos + D3DXVECTOR3(0.0f, 50.0f, 0.0f)))
			{
				m_bSeen = true;
				CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(true, GetId());
				m_state = STATE_ATTACK;
			}
			else
			{
				CApplication::GetInstance()->GetSceneMode()->SetEnemyDiscovery(false, GetId());
				m_bSeen = false;
			}
		}
	}

	UpdateState();					//現在の状態によっての更新処理

	CEnemy::Update();				//親クラスの更新処理

	if (GetDeath())
	{//死亡フラグがtrueだったら

		Kill();			//死ぬように設定する

		return;
	}

	CApplication::GetInstance()->GetSceneMode()->SetEnemyPos(GetPos(), GetId());

#ifdef _DEBUG
	//デバッグ用の処理（殺す）
	CInput *pInput = CInput::GetKey();
	if (pInput->Trigger(DIK_5))
	{
		// これ通った時めっちゃメッシュが暴走します
		Kill();
	}
#endif // _DEBUG
}

//=============================================================================
//目の相対位置の設定
//=============================================================================
void CRoboEnemy::SetEyePos(const D3DXVECTOR3 eyePos)
{
	if (m_pViewField)
	{
		m_pViewField->SetPosV(eyePos);
	}
}





//生成
CRoboEnemy* CRoboEnemy::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 targetPos)
{
	CRoboEnemy* pEnemy = new CRoboEnemy;	//インスタンスを生成する

												//初期化処理
	if (FAILED(pEnemy->Init()))
		return nullptr;

	pEnemy->SetPos(pos);						//位置の設定
	pEnemy->m_targetPos[0] = pos;				//移動の始点の設定
	pEnemy->m_targetPos[1] = targetPos;			//移動の終点の設定
	pEnemy->m_nCurrentTarget = 1;				//現在の移動の目的の位置の設定

	return pEnemy;								//生成したインスタンスを返す
}

//状態によって更新
void CRoboEnemy::UpdateState()
{
	switch (m_state)
	{
	case CRoboEnemy::STATE_WALKING:

		//歩く
		UpdateWalk();

		break;

	case CRoboEnemy::STATE_LOOK_AROUND:

		//周りを見る
		LookAround();

		break;

	case CRoboEnemy::STATE_ATTACK:

		//攻撃
		Attack();

		break;
	case CRoboEnemy::STATE_ONLINEATTACK:


		//攻撃
		Attack(true);

		break;

	case CRoboEnemy::STATE_CHASE:

		//追いかける
		Chase();

		break;

	default:
		break;
	}
}

//移動状態の更新
void CRoboEnemy::UpdateWalk()
{
	Move();
}

//周りを見る
void CRoboEnemy::LookAround()
{
	D3DXVECTOR3 rotDest = GetRotDest();			//目的の回転角度の取得

	switch (m_nCntPhase)
	{
	case 0:

	{
		rotDest.y = m_fOriginalRot + LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);	//回転角度の設定
		m_nCntAnim++;			//フレーム数の更新

		if (m_nCntAnim >= 90)
		{//90フレームが経ったら

			m_nCntPhase++;		//次へ進む
			m_nCntAnim = 0;		//フレーム数を0に戻す
		}
	}

	break;

	case 1:

	{
		rotDest.y = m_fOriginalRot - LOOK_AROUND_ANGLE;

		// 目的の向きの補正
		if (rotDest.y - GetRot().y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - GetRot().y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);		//回転角度の設定
		m_nCntAnim++;				//フレーム数の更新

		if (m_nCntAnim >= 90)
		{//90フレームが経ったら
			m_nCntPhase++;			//次へ進む
			m_nCntAnim = 0;			//フレーム数を0に戻す
		}
	}

	break;

	case 2:

	{
		m_nCurrentTarget ^= 1;			//移動の目的の位置を切り替える

										//移動量を計算する
		D3DXVECTOR3 move = m_targetPos[m_nCurrentTarget] - GetPos();
		D3DXVec3Normalize(&move, &move);

		// 向きの取得
		D3DXVECTOR3 rot = GetRot();
		rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
		float fDot = D3DXVec3Dot(&move, &unit);

		//近似がないようにする
		if (fDot < -1.0f)
			fDot = -1.0f;
		else if (fDot > 1.0f)
			fDot = 1.0f;

		rotDest.y = acosf(fDot);		//目的の向きを計算する

										// 目的の向きの補正
		if (rotDest.y - rot.y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - rot.y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);			//目的の向きの設定

		m_state = STATE_WALKING;		//歩く状態にする
		m_nCntPhase = 0;				//フレーム数を0に戻す
		m_fOriginalRot = 0.0f;
	}

	break;

	default:
		break;
	}
}

//移動
void CRoboEnemy::Move()
{
	CMove* pMove = GetMove();			//移動の状態の取得

	if (pMove)
	{//取得できたら

		D3DXVECTOR3 pos = GetPos(), target = m_targetPos[m_nCurrentTarget];		//必要な位置の設定

																				//現在の位置と目的の位置の距離を計算する
		float dist = sqrtf(((target.x - pos.x) * (target.x - pos.x)) + ((target.y - pos.y) * (target.y - pos.y)) + ((target.z - pos.z) * (target.z - pos.z)));

		if (dist <= TARGET_RADIUS)
		{//前計算した距離が目的の位置の半径より小さかったら

			m_fOriginalRot = GetRotDest().y;	//現在の回転角度を保存する
			m_state = STATE_LOOK_AROUND;		//周りを見る状態にする
			return;
			//m_nCurrentTarget ^= 1;
		}

		//移動の向きを計算する
		D3DXVECTOR3 move = target - GetPos();
		D3DXVec3Normalize(&move, &move);
		pMove->Moving(move);

		//位置の更新
		D3DXVECTOR3 delta = pMove->GetMove();
		pos += delta;
		SetPos(pos);

		// 向きの取得
		D3DXVECTOR3 rot = GetRot(), rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
		float fDot = D3DXVec3Dot(&move, &unit);

		//近似がないようにする
		if (fDot < -1.0f)
			fDot = -1.0f;
		else if (fDot > 1.0f)
			fDot = 1.0f;

		rotDest.y = -acosf(fDot);		//目的の回転角度を計算する

		if (GetPos().x > target.x)
			rotDest.y = -rotDest.y;

		// 目的の向きの補正
		if (rotDest.y - rot.y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - rot.y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);			//目的の回転角度の設定
	}
}

void CRoboEnemy::Move(const D3DXVECTOR3 targetPos)
{
	CMove* pMove = GetMove();			//移動の状態の取得

	if (pMove)
	{//取得できたら

		D3DXVECTOR3 pos = GetPos(), target = targetPos;		//必要な位置の設定

															//現在の位置と目的の位置の距離を計算する
		float dist = sqrtf(((target.x - pos.x) * (target.x - pos.x)) + ((target.y - pos.y) * (target.y - pos.y)) + ((target.z - pos.z) * (target.z - pos.z)));

		if (dist <= TARGET_RADIUS)
		{//前計算した距離が目的の位置の半径より小さかったら

			m_fOriginalRot = GetRotDest().y;	//現在の回転角度を保存する
			m_state = STATE_LOOK_AROUND;		//周りを見る状態にする
			return;
		}

		//移動の向きを計算する
		D3DXVECTOR3 move = target - GetPos();
		D3DXVec3Normalize(&move, &move);
		pMove->Moving(move);

		//位置の更新
		D3DXVECTOR3 delta = pMove->GetMove();
		pos += delta;
		SetPos(pos);

		// 向きの取得
		D3DXVECTOR3 rot = GetRot(), rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
		float fDot = D3DXVec3Dot(&move, &unit);

		//近似がないようにする
		if (fDot < -1.0f)
			fDot = -1.0f;
		else if (fDot > 1.0f)
			fDot = 1.0f;

		rotDest.y = -acosf(fDot);		//目的の回転角度を計算する

		if (GetPos().x > target.x)
			rotDest.y = -rotDest.y;

		// 目的の向きの補正
		if (rotDest.y - rot.y >= D3DX_PI)
		{// 移動方向の正規化
			rotDest.y -= D3DX_PI * 2;
		}
		else if (rotDest.y - rot.y <= -D3DX_PI)
		{// 移動方向の正規化
			rotDest.y += D3DX_PI * 2;
		}

		SetRotDest(rotDest);			//目的の回転角度の設定
	}
}

//攻撃
void CRoboEnemy::Attack(const bool isOnline)
{
	//プレイヤーのほうに向かう
	LookAtPlayer(isOnline);

	if (Shoot() && !m_bSeen)
	{//攻撃が終わった時、プレイヤーが見えない場合、歩く状態に戻す
		if (!isOnline)
		{
			m_newTarget = CGame::GetPlayer()->GetPos();
		}
		else
		{
			m_newTarget = CHacker::GetPlayer()->GetPos();
		}

		m_state = STATE_CHASE;
		m_nCurrentTarget ^= 1;
	}
}

//プレイヤーのほうに向かう
void CRoboEnemy::LookAtPlayer(bool isOnline)
{
	CGun* pGun = GetGun();					//銃の取得
	CPlayer* pPlayer = nullptr;//プレイヤーの取得
	if (isOnline)
	{
		pPlayer = CHacker::GetPlayer();	//プレイヤーの取得
	}
	else
	{
		pPlayer = CGame::GetPlayer();	//プレイヤーの取得
	}

	if (!pGun || !pPlayer)
	{
		m_state = STATE_WALKING;
		return;
	}

	//必要なデータの宣言と初期化
	D3DXVECTOR3 unit = D3DXVECTOR3(0.0f, 0.0f, -1.0f), dir = GetPos() - pPlayer->GetPos();
	D3DXVECTOR3 rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = GetRot();
	float fDot = 0.0f;

	//向きの単位ベクトルを計算する
	D3DXVec3Normalize(&dir, &dir);
	fDot = D3DXVec3Dot(&unit, &dir);

	//近似がないようにする
	if (fDot < -1.0f)
		fDot = -1.0f;
	else if (fDot > 1.0f)
		fDot = 1.0f;

	rotDest.y = acosf(fDot) + D3DX_PI;		//目的の回転角度を計算する

	if (GetPos().x > pPlayer->GetPos().x)
		rotDest.y = -rotDest.y;

	// 目的の向きの補正
	if (rotDest.y - rot.y >= D3DX_PI)
	{// 移動方向の正規化
		rotDest.y -= D3DX_PI * 2;
	}
	else if (rotDest.y - rot.y <= -D3DX_PI)
	{// 移動方向の正規化
		rotDest.y += D3DX_PI * 2;
	}

	SetRotDest(rotDest);			//目的の回転角度の設定
}



//プレイヤーを追いかける
void CRoboEnemy::Chase()
{
	Move(m_newTarget);
}
