//=============================================================================
//
// enemydrone.h
// Author: 有田明玄
//
//=============================================================================
#ifndef _DRONE_ENEMY_H
#define _DRONE_ENEMY_H


//=============================================================================
// インクルード
//=============================================================================
#include "enemy.h"
#include "drone.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CViewField;


class CEnemyDrone : public CEnemy
{
public:
	~CEnemyDrone() override;			//コンストラクタ
	CEnemyDrone();					//デストラクタ

	HRESULT Init() override;			// 初期化
	void Uninit() override;				// 終了
	void Update() override;				// 更新
	void SetEyePos(const D3DXVECTOR3 eyePos);		//目の相対位置の設定
	void Hacking();						//　ハッキングされたときの処理
	void SetSide(int side) { m_side = (ESide)side; };	//敵か味方か

	static CEnemyDrone* Create(const D3DXVECTOR3 pos);	//生成

private:

	//void UpdateState();								//状態によって更新
	//void UpdateWalk();								//移動状態の更新
	//void LookAround();								//周りを見る
	//void Move();									//移動
	//void Move(const D3DXVECTOR3 targetPos);			//移動
	//void Attack(const bool isOnline = false);		//弾を撃つ
	//void LookAtPlayer(bool isOnline = false);		//プレイヤーのほうに向かう
	//void Chase();									//プレイヤーを追いかける
	void Control();	//移動
	void EnemyMode();
	void Shot(int nType);	//射撃

private:

	enum EState
	{
		STATE_WALKING = 0,
		STATE_LOOK_AROUND,
		STATE_CHASE,
		STATE_ATTACK,
		STATE_ONLINEATTACK,
		STATE_HACK,
		STATE_MAX
	};
	enum ESide	//敵側か味方側か
	{
		SIDE_ENEMY = 0,
		SIDE_NEUTRAL,
		SIDE_PLAYER,
		SIDE_MAX
	};
	static const int		 MAX_TARGET_POS = 2;	//目的の位置の数
	static const D3DXVECTOR3 EYE_RELATIVE_POS;		//目の相対位置
	static const float		 TARGET_RADIUS;			//ターゲットの半径
	static const float		 LOOK_AROUND_ANGLE;		//周りを見る時の加算される角度
	static const D3DXVECTOR3 DEFAULT_COLLISION_POS;	//ディフォルトの当たり判定の相対位置
	static const D3DXVECTOR3 DEFAULT_COLLISION_SIZE;//ディフォルトの当たり判定のサイズ

	D3DXVECTOR3 m_targetPos[MAX_TARGET_POS];		//目的の位置
	int			m_nCurrentTarget;					//現在の目的の位置
	D3DXVECTOR3 m_newTarget;						//新しい目的の位置
	int			m_nCntAnim;							//アニメーション用の変数
	int			m_nCntPhase;						//アニメーション用の変数
	float		m_fOriginalRot;						//元の回転角度
	bool		m_bSeen;							//プレイヤーを見たかどうか		
	EState		m_state;
	ESide		m_side;								//敵か味方か
	CViewField* m_pViewField;						//視野へのポインタ
	int				m_nLife;					// 体力
	int				m_nShockTimer;				// ショックタイマー
	int				m_nHealTimer;				// 回復タイマー
};


#endif
