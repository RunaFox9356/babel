//=============================================================================
//
// UIΗNX(HackerUI.h)
// Author : ϊ±l
// Tv : UIΗπs€
//
//=============================================================================
#ifndef _AGENT_UI_H_		// ±Μ}Nθ`ͺ³κΔΘ©Α½η
#define _AGENT_UI_H_		// ρdCN[hh~Μ}Nθ`

//*****************************************************************************
// CN[h
//*****************************************************************************
#include "manager.h"
#include "UImanager.h"
#include <vector>

class CHackingObj;
class CPolygon2D;
class CTimer;
class CNumber;
class CScore;
class CTaskUI;
class CHpGauge;
class CAgent;
class CWords;
class CWatch;

//=============================================================================
// }l[W[NX
// Author : ϊ±l
// Tv : }l[W[Ά¬πs€NX
//=============================================================================
class CAgentUI :public CUIManager
{

public:
	//--------------------------------------------------------------------
	// ΓIoΦ
	//--------------------------------------------------------------------
	static CAgentUI *Create();	// UIΜΆ¬

private:
	//--------------------------------------------------------------------
	// θ
	//--------------------------------------------------------------------
	static const int   MAX_ITEM = 3;			// g¦ιACeΜ
	static const int   MAX_CAMERA = 5;			// g¦ιJEh[Μ
	static const float ITEM_NORMAL_SIZE;		// ACeκΜUIΜIΞκΔ’Θ’ΜTCY
	static const float ITEM_WIDTH;				// ACeκΜUIzuΤu
	static const float ITEM_DISTANCE_FROM_EDGE;	// [©ηΜ£
	static const int HEAL_RECAST_TIME;			// ρΜLXgΤ
	static const int NUMBER_0;					// TEXTUREΜΜ0Μκ
	static const float TIMER_SIZE;				// ^C}[ΜTCY
	static const D3DXVECTOR3 CAgentUI::SCORE_SIZE;	// XRA

public:
	//--------------------------------------------------------------------
	// RXgN^ΖfXgN^
	//--------------------------------------------------------------------
	CAgentUI();
	~CAgentUI();
	HRESULT Init()override;
	void Uninit()override;
	void Update()override;
	void Draw()override;
	CTaskUI*GetTask() { return m_pTask; };
	CPolygon2D* GetMinimap() { return m_pMinimap; };
	CPolygon2D* GetWeapons() { return m_pCurrentWeapons; };
	CNumber* GetBullets() { return m_pBullets; };
	CHpGauge* GetHPGauge() { return m_pHPGauge; };
	CScore* GetScore() { return m_Score; };
	CTimer* GetTimer() { return m_pTimer; };
	void AddScore(const int nScore);							//XRAΜΑZ
	void SetScore(const int nScore);
	void SetAgent(CAgent* pAgent) { m_pAgent = pAgent; }
	void SetHpGauge(CHpGauge* pGauge);							//HpQ[WΜέθ

private:
	void LeftBulletUpdate();			// ceΜXV

private:
	//UIάνθ
	CTaskUI* m_pTask;							//	^XN
	CPolygon2D* m_pMinimap;						//	~j}bv
	CNumber* m_pBullets;						//	ce

	CHpGauge* m_pHPGauge;						//	HPo[
	CScore* m_Score;							//	XRA
	CTimer* m_pTimer;							//	^C}[
	CWatch*	m_pWatch;							//  rv

	CAgent* m_pAgent;
	CPolygon2D* m_pCurrentWeapons;				//	»έΜν
	std::vector<CPolygon2D*> m_pPolygonItem;	// ACeΜ|S
	std::vector<CWords*> m_pWord;				
	int m_nItemID;
};
#endif
