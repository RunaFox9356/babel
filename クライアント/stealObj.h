//=============================================================================
//
// stealObj.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _STEAL_OBJ_H
#define _STEAL_OBJ_H


//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CEmptyObj;
class CModelObj;
class CCollision_Rectangle3D;
class CUiMessage;


class CStealObj : public CGimmick
{
public:

	static const int			DEFAULT_BELL_IDX;					//ディフォルトのモデルインデックス
	static const int			DEFAULT_DOCUMENTS_IDX;				//ディフォルトのモデルインデックス

	CStealObj();
	~CStealObj() override;

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};			// 大きさのセッター
	D3DXVECTOR3 GetPos() override;								// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override;								// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定
	void SetIsOnMap(const bool bOnMap) override;				//マップに表示されているかどうかの設定処理

	static CStealObj* Create(const D3DXVECTOR3 pos, const int nModelIdx = DEFAULT_BELL_IDX);			//生成

private:

	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIメッセージの生成処理
	void DestroyUiMessage();	//UIメッセージの破棄処理

private:


	D3DXVECTOR3	m_pos;						//位置

	CModelObj*	m_pModel;					//モデルへのポインタ
	CUiMessage*	m_pUiMessage;				//UIメッセージへのポインタ
};


#endif