#include "minigame_ring.h"
#include "polygon2D.h"
#include "input.h"
#include "debug_proc.h"
#include "MiniGameMNG.h"
#include "application.h"
#include "scene_mode.h"
#include "minigame.h"
#include "sound.h"
#include "text.h"


CMinGame_Ring::CMinGame_Ring(int nPriority):CMiniGame(nPriority),
m_pCommandUI(nullptr)
{
}

CMinGame_Ring::~CMinGame_Ring()
{
}

HRESULT CMinGame_Ring::Init()
{
	CMiniGame::Init();

	{
		 m_PosX    = 0.0f;		// 描画座標X
		 m_PosY    = 0.0f;		// 描画座標Y
		 m_Radius  = 0.0f;		// 半径(描画用)
		 m_CenterX = 0.0f;		// 中心座標X
		 m_CenterY = 0.0f;		// 中心座標Y
		 m_Angle = 0.0f;		// 角度
		 m_Length = 100.0f;		// 半径の長さ

		 flg = false;
	}

	LoadTex(42);
	SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	SetSize(D3DXVECTOR3(50.0f, 50.0f, 0.0f));
	SetPos(D3DXVECTOR3(640,360,0));
	karapos = GetPos();
	m_backGround = CPolygon2D::Create(3);
	m_backGround->SetPos(D3DXVECTOR3(640, 360, -10));
	m_backGround->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_backGround->SetSize(D3DXVECTOR3(400.0f, 400.0f, 0.0f));
	m_backGround->LoadTex(43);

	m_pText = CText::Create(D3DXVECTOR3(420.0f, 80.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "壁に当たらずゴールを目指せ！！",
		D3DXVECTOR3(15.0f, 30.0f, 20.0f), D3DXCOLOR(1.0f, 0.1f, 0.1f, 1.0f), CFont::FONT_SOUEIKAKU);
	m_pText->TexChange("赤いエリアよくボタンを押せ！！");

	//操作のUIを生成する
	m_pCommandUI = CPolygon2D::Create(CSuper::PRIORITY_LEVEL2);

	if (m_pCommandUI)
	{//nullチェック

		m_pCommandUI->SetPos(D3DXVECTOR3(650.0f, 600.0f, 0.0f));
		m_pCommandUI->SetSize(D3DXVECTOR3(300.0f, 100.0f, 0.0f));
		m_pCommandUI->SetColor(D3DXCOLOR(0.75f, 0.75f, 0.75f, 1.0f));
		m_pCommandUI->LoadTex(106);
	}

	return S_OK;
}

void CMinGame_Ring::Uninit()
{
	CMiniGame::Uninit();
	if (m_backGround!=nullptr)
	{
		m_backGround->Uninit();
	}

	if (m_pCommandUI)
	{
		m_pCommandUI->Uninit();
		m_pCommandUI = nullptr;
	}
	if (m_pText != nullptr)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}


}

void CMinGame_Ring::Update()
{
	CMiniGame::Update();

	D3DXVECTOR3 pos = GetPos();

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(DIK_SPACE))
	{
		flg = !flg;
	}


	// 度数法の角度を弧度法に変換
	float radius = m_Angle * 3.14f / 180.0f;

	float a = cos(radius);

	if (!flg)
	{
		// 中心座標に角度と長さを使用した円の位置を加算する

		// 三角関数を使用し、円の位置を割り出す。
		add_x = cos(radius) * this->m_Length;
		add_y = sin(radius) * this->m_Length;


		// 結果ででた位置を中心位置に加算し、それを描画位置とする
		pos.x = this->m_CenterX + add_x + karapos.x;
		pos.y = this->m_CenterY + add_y + karapos.y;

		// 角度更新
		this->m_Angle += 1.0f;

	}

	// デバック表示
	CDebugProc::Print("%d\n", a); // ブレクポイント

	SetPos(pos);

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	if (a < 0.50 && a <= -0.95&&flg==true)
	{//判定中に収まり成功した

		pSound->SetVolume(CSound::SOUND_LABEL_SE_GAKO, 30.5f);

		pSound->PlaySound(CSound::SOUND_LABEL_SE_GAKO);

		CMiniGameMNG* pMng = CApplication::GetInstance()->GetSceneMode()->GetMiniMng();
		pMng->SetClear(true);
		return Uninit();
	}
}

void CMinGame_Ring::Draw()
{
	CMiniGame::Draw();
}

CMinGame_Ring *CMinGame_Ring::Create(int count)
{
	// オブジェクトインスタンス
	CMinGame_Ring *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CMinGame_Ring(count);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

