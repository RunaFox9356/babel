//=============================================================================
//
// viewField.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _VIEW_FIELD_H
#define _VIEW_FIELD_H


//=============================================================================
// インクルード
//=============================================================================
#include "object.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CLine;


class CViewField : public CObject
{
public:

	static const float DEFAULT_VIEW_DISTANCE;	//ディフォルトの見える距離
	static const float DEFAULT_VIEW_ANGLE;		//ディフォルトの視野角


	CViewField();
	~CViewField() override;				//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &/*pos*/) override { }								// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override { }							// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override { m_rot = rot; }						// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override { };								// 大きさのセッター
	D3DXVECTOR3 GetPos() override { return D3DXVECTOR3(); }								// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }							// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override { return D3DXVECTOR3(); }							// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }							// 大きさのゲッター

	void SetPosV(const D3DXVECTOR3 posV) { m_posV = posV; }								//視点の設定
																						
	void SetViewAngle(const float fAngle);												//視野角の設定
	void SetViewDistance(const float fDistance);										//見える距離の設定

	void SetParent(CObject* pParent) { m_pParent = pParent; }							//親へのポインタの設定

	const float GetViewDistance() { return m_fViewDistance; }							//見える距離の取得


	bool IsPontInView(D3DXVECTOR3 point);	//ポイントが見えるかどうかの判定
	bool IsPointInFront(D3DXVECTOR3 point);	//ポイントが前にあるかどうかの判定



	static CViewField* Create();						//生成
	static CViewField* Create(CObject* pParent);		//生成

private:

	void CalcPlaneEdgesLength();						//ロジェクション平面の辺の長さを計算する
	void CreateInverseMatrix(D3DXMATRIX* mtxInv);		//逆行列を作る

#ifdef _DEBUG
	void SetLine();
#endif // _DEBUG

private:

	static const float MIN_VIEW_ANGLE;			//最小の視野角
	static const float MAX_VIEW_ANGLE;			//最大の視野角
	static const float DEFAULT_SENSE_RADIUS;	//プレイヤーに気付くディフォルト範囲の半径

	D3DXVECTOR3 m_posV;							//視点(相対位置)
	D3DXVECTOR3 m_rot;							//回転角度(相対)

	float		m_fViewDistance;				//見える距離
	float		m_fViewAngle;					//視野角
	float		m_fProjectionPlaneEdge;			//プロジェクション平面の辺の長さ(正方形)


	CObject*	m_pParent;						//親へのポインタ

#ifdef _DEBUG
	static const int LINE_NUMBER = 8;				//ライン数
		
	CLine*		m_pLine[LINE_NUMBER];				//ラインへのポインタ
	D3DXCOLOR	m_lineCol;							//ラインの色
#endif // _DEBUG

};



#endif