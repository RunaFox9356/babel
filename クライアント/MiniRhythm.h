#ifndef _MINIRHYTHM_H_		// このマクロ定義がされてなかったら
#define _MINIRHYTHM_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include"polygon2D.h"
#include "minigame.h"

class CText;
class CMiniRhythm : public CMiniGame
{
private:
	const int CLEAR_SCORE = 2;

public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CMiniRhythm(int nPriority = PRIORITY_LEVEL3);
	~CMiniRhythm();

	HRESULT Init();		// 初期化
	void Uninit();		// 終了
	void Update();		// 更新
	void Draw();		// 描画
	void Judge();		// 接触判定
	static CMiniRhythm *Create(int count);

protected:

private:
	CPolygon2D* m_Notes;	//ノーツ
	CPolygon2D* m_Line;		//判定ライン
	CPolygon2D* m_Lane[2];	//レーン
	int nCount;
	int m_nScore;
	CText*m_pText;
	bool m_up;
	CPolygon2D*		m_pCommandUI;		//操作のUI
};
#endif
#pragma once
