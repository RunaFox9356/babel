//=============================================================================
//
// ハッカーモードクラス(hacker.cpp)
// Author : 有田明玄
// 概要 : ハッカーモードクラス管理を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "mapDataManager.h"
#include "hacker.h"
#include "game.h"
#include "calculation.h"
#include "input.h"
#include "application.h"
#include "camera.h"
#include "renderer.h"
#include "object.h"
#include "polygon2D.h"
#include "polygon3D.h"
#include "player.h"
#include "motion_model3D.h"
#include "model3D.h"
#include "model_obj.h"
#include "mesh.h"
#include "sphere.h"
#include "bg.h"
#include "model_obj.h"
#include "debug_proc.h"
#include "sound.h"
#include "line.h"
#include "collision_rectangle3D.h"
#include "drone.h"
#include "agent.h"
#include "enemy.h"
#include "GimmicDoor.h"
#include "model_data.h"
#include "tcp_client.h"
#include "application.h"
#include "map.h"
#include "agent.h"
#include "walkingEnemy.h"
#include "MiniGameMNG.h"
#include "EscapePoint.h"
#include "hostage.h"
#include "carry_player.h"
#include "gun.h"
#include "viewField.h"
#include "utility.h"
#include"boundingBox3D.h"
#include"HackerUI.h"
#include"UImanager.h"
#include "Timer.h"
#include "Score.h"
#include "securityCamera.h"
#include "task.h"
#include "enemydrone.h"
#include "HackingObj.h"
#include "UImessage.h"

#include "jammingTool.h"

#include "cardboard.h"

#include "model_skin.h"

#include "hackingdoor.h"

// ---------------------------------------------------------------------------- 
// 定数
//-----------------------------------------------------------------------------
const int CHacker::HEAL_RECAST_TIME = 30;				// 回復のリキャスト時間
const int CHacker::MINIGAME_CLEAR_SCORE = 100;			//ミニゲームクリア時のスコア

//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
CCameraList *CHacker::m_pCameraList[5];
CPlayer *CHacker::m_pPlayer = nullptr;					// プレイヤークラス
CDrone *CHacker::m_pDrone = nullptr;					// ドローンクラス
bool CHacker::m_bGame = false;							// ゲームの状況
CMesh3D* CHacker::m_Mesh = nullptr;
CTask* CHacker::m_pTask = nullptr;						// タスクマネージャー
CHackerUI*	CHacker::m_pHackerUI = nullptr;				//　ハッカーのUI

namespace
{
	// フォグの数値設定
	const D3DXCOLOR fogColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f);		// フォグカラー
	const float fogStartPos = 600.0f;								// フォグの開始点
	const float fogEndPos = 2000.0f;								// フォグの終了点
	const float fogDensity = 0.00001f;								// フォグの密度

	const float gravity = 0.2f;
}

//=============================================================================
// コンストラクタ
// Author : 有田明玄
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CHacker::CHacker() : m_pUiMessage(nullptr)
{
	m_nSelectHackObj = 0;
	m_bGame = true;
	m_nNowCamera = 0;
	m_pHackTg = nullptr;
	
}

//=============================================================================
// デストラクタ
// Author : 有田明玄
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CHacker::~CHacker()
{
}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CHacker::Init()
{
	//CHackDoor::Create()->SetDoorPos(D3DXVECTOR3(0.0f,0.0f,0.0f));

	//CCardboard::Create({ 0.0f,0.0f,0.0f });
	//MAPのデータを初期化する。
	InitPosh();

	CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->SetPlayerClear();
	CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->SetPlayerClear();

	CApplication::GetInstance()->SetConnect(true);
	CApplication::GetInstance()->SetEnemyConnect(true);
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	//タスクマネージャーの生成
	m_pTask = CTask::Create();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM001);
	m_Map = 0;
	m_Map = CApplication::GetInstance()->GetMap();

	if (m_Map < 0 || m_Map >= CApplication::GetInstance()->GetMapDataManager()->GetMaxMap())
	{
		m_Map = 0;
	}

	if(m_Map != 2)
	{
		m_pFloor = CPolygon3D::Create();

		if (m_pFloor)
		{
			m_pFloor->SetPos(D3DXVECTOR3(0.0f, 1.0f, 0.0f));
			m_pFloor->SetSize(D3DXVECTOR3(20000.0f, 20000.0f, 0.0f));
			m_pFloor->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(400.0f, 400.0f));
			m_pFloor->SetRot(D3DXVECTOR3(D3DX_PI * 0.5f, 0.0f, 0.0f));

			if (m_Map == 0)
			{
				m_pFloor->LoadTex(92);
			}
			else if (m_Map == 1)
			{
				m_pFloor->LoadTex(120);
			}
			else if (m_Map == 2)
			{
				m_pFloor->LoadTex(114);
			}
		}
	}

	CMap* pMap = CMap::Create(m_Map);

	SetMap(pMap);

	CApplication::GetInstance()->SetMap(m_Map);

	CApplication::SMapList MapData;
	MapData.enemyCount = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vEnemyData.size();
	MapData.gimmickConnect = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vGimmickData.size();
	CApplication::GetInstance()->SetList(MapData);
#ifdef _DEBUG

	//CApplication::GetInstance()->SetMap(2);					//デバッグ用

#endif // _DEBUG
	

	// 重力の値を設定
	CCalculation::SetGravity(gravity);

	if (m_Map != 1)
	{
		// スカイボックスの設定
		CSphere *pSphere = CSphere::Create();
		pSphere->SetRot(D3DXVECTOR3(D3DX_PI, 0.0f, 0.0f));
		pSphere->SetSize(D3DXVECTOR3(10.0f, 0, 10.0f));
		pSphere->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
		pSphere->SetRadius(10000.0f);
		pSphere->SetSphereRange(D3DXVECTOR2(D3DX_PI * 2.0f, D3DX_PI * -0.5f));
		pSphere->LoadTex(1);
	}

	// プレイヤーの設定
	m_pPlayer = CPlayer::Create();
	//m_pPlayer->SetMotion("data/MOTION/motion.txt");
	m_pPlayer->SetRotMove(false);
	m_pPlayer->SetSkin(CSkinMesh::Create("data/MODEL/skin/default.x"));
	m_pPlayer->SetMtxItemParent(m_pPlayer->GetSkin()->GetpBoneMatrix("NewJoint21_Joint"));
	CApplication::GetInstance()->SetMoveModel(m_pPlayer);

	CApplication::GetInstance()->SetModelSet(true);
	// ドローン設定
	m_pDrone = CDrone::Create();
	m_pDrone->SetMotion("data/MOTION/motion002_drone.txt");

	CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

	if (pManager)
	{
		CMapDataManager::MAP_DATA MapData = pManager->GetMapData(CApplication::GetInstance()->GetMap());

		if (m_pPlayer)
			m_pPlayer->SetPos(MapData.agentPos);
		if (m_pDrone)
			m_pDrone->SetPos(MapData.agentPos);
	}

	// カメラの追従設定(目標 : ドローン)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(m_pDrone, 1.0);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);
	
	// マウスカーソルを消す
	pInput->SetCursorErase(true);

	// フォグの設定
	SetFog(pDevice, fogStartPos, fogEndPos, fogDensity, fogColor);

	//m_pLine = CLine::Create();	//ハック対象とのライン

#ifdef _DEBUG

	for (int i = 0; i < 5; i++)
	{
		m_pCameraList[i] = new CCameraList;
		m_pCameraList[i]->MapPosV = D3DXVECTOR3(0.0f, 5000.0f, -1.0f);
		m_pCameraList[i]->MapPosR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}
	m_pCameraList[0]->PosV = D3DXVECTOR3(100.0f, 50.0f, -200.0f);
	m_pCameraList[0]->PosR = D3DXVECTOR3(100.0f, 0.0f, 100.0f);

	m_pCameraList[1]->PosV = D3DXVECTOR3(200.0f, 50.0f, -200.0f);
	m_pCameraList[1]->PosR = D3DXVECTOR3(200.0f, 0.0f, 100.0f);

	m_pCameraList[2]->PosV = D3DXVECTOR3(-200.0f, 50.0f, 400.0f);
	m_pCameraList[2]->PosR = D3DXVECTOR3(-200.0f, 0.0f, 200.0f);

	m_pCameraList[3]->PosV = D3DXVECTOR3(200.0f, 50.0f, 400.0f);
	m_pCameraList[3]->PosR = D3DXVECTOR3(200.0f, 0.0f, 200.0f);

	m_pCameraList[4]->PosV = D3DXVECTOR3(200.0f, 50.0f, -400.0f);
	m_pCameraList[4]->PosR = D3DXVECTOR3(200.0f, 0.0f, -200.0f);

#endif // _DEBUG

	//m_pSecurityCamera[0] = CSecurityCamera::Create(D3DXVECTOR3(-200.0f, 50.0f, -200.0f), D3DXVECTOR3(0.0f, +D3DX_PI * 0.25f, 0.0f));
	//m_pSecurityCamera[1] = CSecurityCamera::Create(D3DXVECTOR3(+200.0f, 50.0f, -200.0f), D3DXVECTOR3(0.0f, -D3DX_PI * 0.25f, 0.0f));

	//ChangeDrone();	//ドローン追従

	CGun *pGun = CGun::Create();
	pGun->SetType(56);
	CCollision_Rectangle3D *pCollision = pGun->GetCollision();
	D3DXVECTOR3 modelSize = pGun->GetModel()->GetMyMaterial().size;
	pCollision->SetPos(D3DXVECTOR3(0.f, modelSize.y / 2.0f, 0.f));
	pCollision->SetSize(modelSize);
	pGun->SetPos(D3DXVECTOR3(0.0f, 0.0f, -100.0f));
	pGun->SetAllReload(true);
	pGun->SetRapidFire(false);
	pGun->SetMaxBullet(15);
	pGun->SetMaxReload(60);

	/*pGun = CGun::Create();
	pGun->SetType(1);
	pCollision = pGun->GetCollision();
	modelSize = pGun->GetModel()->GetMyMaterial().size;
	pCollision->SetPos(D3DXVECTOR3(0.f, modelSize.y / 2.0f, 0.f));
	pCollision->SetSize(modelSize);
	pGun->SetPos(D3DXVECTOR3(0.0f, 0.0f, -200.0f));*/
	m_pHackerUI = CHackerUI::Create();	//UI生成
	//m_pHackerUI->SetCameraNumber(0, 0);
	//m_pHackerUI->SetCameraNumber(MAX_CAMERA, 1);

	if (m_pTask)
	{//nullチェック
		m_pTask->CreateUI();		//UIの生成
	}
	
	
	std::thread th(CApplication::Recv, 1);
	// スレッドを切り離す
	th.detach();

	return S_OK;
}

//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CHacker::Uninit()
{
	CApplication::GetInstance()->SetModelSet(false);
	CApplication::GetInstance()->SetConnect(false);
	CApplication::GetInstance()->SetEnemyConnect(false);
	CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->SetPlayerClear();
	CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->SetPlayerClear();
	for (int i = 0; i < 5; i++)
	{
		if (m_pCameraList[i] != nullptr)
		{
			delete m_pCameraList[i];
		}
	}

	//UIの破棄
	if (m_pHackerUI)
	{
		m_pHackerUI = nullptr;
	}
	if (m_pUiMessage)
	{
		m_pUiMessage = nullptr;
	}

	//タスクマネージャーの破棄
	if (m_pTask)
	{
		m_pTask = nullptr;
	}

	// スコアの解放
	Release();

}

//=============================================================================
// 更新
// Author : 有田明玄
// 概要 : 更新を行う
//=============================================================================
void CHacker::Update()
{
	CCamera *pCamera = nullptr;
	CInput *pInput = CInput::GetKey();	//キーボードよびだし

	CModelData* Model = CApplication::GetInstance()->GetPlayer(0);

	for (int i = 0; i < GetGimmickSize(); i++)
	{
		Model->GetPlayerData()->Player.m_isPopGimmick[i].isUse = GetGimmick(i)->GetAnimation();
		Model->GetPlayerData()->Player.m_isPopGimmick[i].isMap = GetGimmick(i)->GetIsOnMap();
	}

	for (int i = 0; i <GetEnemySize(); i++)
	{
		Model->GetPlayerData()->Player.m_isPopEnemy[i].Pos = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).Pos;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isDiscovery = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isDiscovery;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isUse = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isUse;
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isDes = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isDes;

		
		Model->GetPlayerData()->Player.m_isPopEnemy[i].isMap = CApplication::GetInstance()->GetSceneMode()->GetEnemy(i).isMap;
	}

	Model->GetPlayerData()->Player.m_popGimmick = GetGimmickSize();
	Model->GetPlayerData()->Player.m_popEnemy = GetEnemySize();

#ifdef _DEBUG

	// カメラの追従設定
	pCamera = CApplication::GetInstance()->GetCamera();

	if (pInput->Trigger(DIK_F10))
	{
		pCamera->Shake(60, 50.0f);
	}

	if (pInput->Press(DIK_LSHIFT))
	{
		pCamera->Zoom();
	}

	if (pInput->Trigger(DIK_RETURN))
	{
		m_bGame = false;
	}

	// デバック表示
	CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル| F7 デバック表示削除\n");


	if (pInput->Trigger(DIK_F1))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_AGENT);
	}
	if (pInput->Trigger(DIK_F3))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_HACKER);
	}
	if (pInput->Trigger(DIK_F4))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TITLE);
	}
	if (pInput->Trigger(DIK_V))
	{//スコアテスト
		m_pHackerUI->GetScore()->AddScore(100);
	}
#endif // _DEBUG
	if (m_pHackTg == nullptr || m_pHackTg->GetHackState() != CHackObj::STATE_NOW_HACKING)
	{//ハッキング中は操作できない
			// カメラ切り替え
		CSecurityCamera::ChangeCamera();
	}
	
	//ハッキング
	Hack();

	HWND activeWindowHandle;
	HWND hWnd = CApplication::GetInstance()->GetWnd();
	bool isActiveWindowThis = CApplication::GetInstance()->GetActiveWindowThis();
	activeWindowHandle = GetForegroundWindow();
	if (hWnd != activeWindowHandle && isActiveWindowThis)
	{ // 自分が最前にいないなら
		isActiveWindowThis = false;
		pInput->SetCursorErase(true);
	}
	if (hWnd == activeWindowHandle && !isActiveWindowThis)
	{ // 自分が最前なら
		isActiveWindowThis = true;
		pInput->SetCursorErase(false);
	}
	CApplication::GetInstance()->SetActiveWindowThis(isActiveWindowThis);


	pInput->SetCursorLock(isActiveWindowThis);
}

//=============================================================================
// カメラ変更
// Author : 有田明玄
// 概要 : カメラの切り替えシステム
//=============================================================================
void CHacker::ChangeCamera(int CameraNumber)
{
	if (m_pSecurityCamera[m_nNowCamera])
		m_pSecurityCamera[m_nNowCamera]->ActivateCamera();

	//CCameraList* pPos = GetCameraPos(CameraNumber);
	//if (pPos != nullptr)
	//{
	//	//m_pCameraNumber[0]->LoadTex((NUMBER_0+1) + CameraNumber);//UI

	//	// カメラの追従設定(目標 : 無)
	//	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	//	pCamera->SetFollowTarget(false);
	//	pCamera->SetPosVOffset(pPos->PosV);
	//	pCamera->SetPosROffset(pPos->PosR);
	//	pCamera = CApplication::GetInstance()->GetMapCamera();
	//	pCamera->SetPosVOffset(pPos->MapPosV);
	//	pCamera->SetPosROffset(pPos->MapPosR);
	//	pCamera->SetPos(pPos->MapPosR);
	//}
}

//=============================================================================
// ドローン変更
// Author : 有田明玄
// 概要 : カメラの切り替えシステム
//=============================================================================
void CHacker::ChangeDrone()
{
	if (m_pSecurityCamera[m_nNowCamera])
		m_pSecurityCamera[m_nNowCamera]->DeactivateCamera(m_pDrone);

	// カメラの追従設定(目標 : ドローン)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(m_pDrone, 1.0);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);

	// カメラの追従設定(目標 : ドローン)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(m_pDrone, 1.0);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));
}

//=============================================================================
// ハッキング
// Author : 有田明玄
// 概要 : ハッキングの処理
//=============================================================================
CHackObj* CHacker::Hacking()
{
	CHackObj* pHack = nullptr;
	float PlayerStroke = 0; //比較のオブジェクトとプレイヤーまでの距離
	D3DXVECTOR3 math = {};	//距離計算用
	CHackObj* pHackList[5] = { nullptr };
	float StrokeList[5] = { 10000,10000,10000,10000,10000 };
	int nCntHackobj = 0;

	//ハッキングしている場合は対象は固定
	if (m_pHackTg)
	{
		if (m_pHackTg->GetHackState() == CHackObj::STATE_HACKED||m_pDrone->GetView()->IsPontInView(m_pHackTg->GetPos())==false)
		{
			m_pHackTg = nullptr;	//ハッキングしていない場合はリセットして再計算
		}
	}
	////オブジェクト検索用
	//CGimmick* pObj = (CGimmick*)CObject::GetTop(CSuper::PRIORITY_LEVEL0);

	//while (pObj != nullptr)	//最後に行くまで
	//{

	if (!CApplication::GetInstance()->GetMapDataManager() || CApplication::GetInstance()->GetMap() < 0 || CApplication::GetInstance()->GetMap() >= CApplication::GetInstance()->GetMapDataManager()->GetMaxMap())
		return nullptr;

	std::vector<CMapDataManager::GIMMICK_DATA> vGimmick = CApplication::GetInstance()->GetMapDataManager()->GetMapData(CApplication::GetInstance()->GetMap()).vGimmickData;

	for(int nCnt = 0; nCnt < (int)vGimmick.size(); nCnt++)
	{
		CGimmick* pObj = vGimmick.data()[nCnt].pGimmick;

		if (pObj && pObj->GetObjType() == CObject::OBJETYPE_HACKOBJ)
		{
			pHack = (CHackObj*)pObj;
			math = D3DXVECTOR3(
				pow(m_pDrone->GetPos().x - pHack->GetPos().x, 2),
				pow(m_pDrone->GetPos().y - pHack->GetPos().y, 2),
				pow(m_pDrone->GetPos().z - pHack->GetPos().z, 2));
			PlayerStroke = sqrtf(pow(sqrtf(math.x + math.y), 2) + math.z);

			if (pHack->GetHackState() != CHackObj::STATE_HACKED&&		//ハッキング済かどうか
				m_pDrone->GetView()->IsPontInView(pHack->GetPos()))		//視界の判定
			{
				if (pHackList[0] == nullptr)
				{//初回は絶対入れる
					pHackList[0] = pHack;
					StrokeList[0] = PlayerStroke;	//距離保存
					nCntHackobj++;
				}
				else
				{//ターゲット入れ替え
					for (int i = 0; i < nCntHackobj + 1; i++)
					{//数字の小さい順から近いオブジェクト
						if (StrokeList[i] > PlayerStroke)//下から順番に比較
						{//入れ替え
							for (int j = 0; j < MAX_SAVE_HACJOBJ - i; j++)	//上から入れ替える
							{//上にずらす
								if (j > 0)//最初の一回のみ通さない
								{
									pHackList[MAX_SAVE_HACJOBJ - j] = pHackList[MAX_SAVE_HACJOBJ - j - 1];
									StrokeList[MAX_SAVE_HACJOBJ - j] = StrokeList[MAX_SAVE_HACJOBJ - j - 1];
								}
							}
							pHackList[i] = pHack;
							StrokeList[i] = PlayerStroke;
							nCntHackobj++;
							break;
						}
					}
				}
			}
		}
	}
	////ハッキング対象選択
	//CInput *pInput = CInput::GetKey();	//キーボードよびだし
	//if (pInput->Trigger(DIK_E))
	//{
	//	m_nSelectHackObj++;
	//	if (m_nSelectHackObj >= nCntHackobj)
	//	{
	//		m_nSelectHackObj = 0;
	//	}
	//	m_pHackTg = pHackList[m_nSelectHackObj];
	//}
	//else if (pInput->Trigger(DIK_Q))
	//{
	//	m_nSelectHackObj--;
	//	if (m_nSelectHackObj < 0 && nCntHackobj >= 1)
	//	{
	//		m_nSelectHackObj = nCntHackobj - 1;
	//	}
	//	else
	//	{
	//		m_nSelectHackObj = 0;
	//	}
	//	m_pHackTg = pHackList[m_nSelectHackObj];
	//}
	
	if (m_pHackTg == nullptr && pHackList[m_nSelectHackObj] != nullptr&&m_nSelectHackObj >= 0)
	{
		m_pHackTg = pHackList[m_nSelectHackObj];
	}

	CDebugProc::Print("選択オブジェクト番号 : %d |\n", m_nSelectHackObj);

	//ターゲットとカメラを線で繋げる
	if (m_pHackTg != nullptr)
	{//ターゲットがいる場合true
	/*	m_pLine->SetLine(
			D3DXVECTOR3(0.0f, 0, 0),
			D3DXVECTOR3(0, 0, 0),
			D3DXVECTOR3(m_pDrone->GetPos() + D3DXVECTOR3(0, m_pDrone->GetSize().y, 0)),
			m_pHackTg->GetPos() + D3DXVECTOR3(0, m_pHackTg->GetCollision()->GetSize().y / 2, 0),
			D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));*/

		m_pHackerUI->GetTargetUI()->SetPos(m_pHackTg->GetPos() + D3DXVECTOR3(0, m_pHackTg->GetCollision()->GetSize().y / 2, 0));
		m_pHackerUI->GetTargetUI()->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		if (!m_pUiMessage)
			CreateUiMessage("Hack", 44);

		// デバック表示
		CDebugProc::Print("プレイヤーとオブジェクトの距離 : %.3f |\n", StrokeList[m_nSelectHackObj]);
	}
	else
	{
		//m_pLine->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		m_pHackerUI->GetTargetUI()->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

		if (m_pUiMessage)
			DestroyUiMessage();
	}
	return m_pHackTg;
}

//=============================================================================
// カメラ切り替え
// Author : 有田明玄
// 概要 : カメラ切り替えの処理
//=============================================================================
void CHacker::CameraSwitching()
{
	CInput *pInput = CInput::GetKey();	//キーボードよびだし
	if (pInput->Trigger(DIK_LEFT))
	{
		if (m_pSecurityCamera[m_nNowCamera])
			m_pSecurityCamera[m_nNowCamera]->SetCameraHackedState(false);

		if (--m_nNowCamera < 0)
		{
			m_nNowCamera = SECURITY_CAMERA_NUM - 1;
		}
		ChangeCamera(m_nNowCamera);
		m_pHackerUI->SetCameraNumber(m_nNowCamera,0);
	}
	if (pInput->Trigger(DIK_RIGHT))
	{
		if (m_pSecurityCamera[m_nNowCamera])
			m_pSecurityCamera[m_nNowCamera]->SetCameraHackedState(false);

		if (++m_nNowCamera >= SECURITY_CAMERA_NUM)
		{
			m_nNowCamera = 0;
		}
		ChangeCamera(m_nNowCamera);
		m_pHackerUI->SetCameraNumber(m_nNowCamera, 0);
	}
	if (pInput->Press(DIK_0))
	{// 1キーが押された時
	 // カメラ切り替え
		ChangeDrone();	//ドローン追従
	}
}

//=============================================================================
// HACK
// Author : 有田明玄
// 概要 : ハッキング周りの管理
//=============================================================================
void CHacker::Hack()
{
	CInput *pInput = CInput::GetKey();	//キーボードよびだし

	if (Hacking())
	{
		if (m_pHackTg->GetHackState() == CHackObj::STATE_NOT_HACK)
		{
			//m_pHackerUI->GetHackUI()->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

			if (pInput->Trigger(DIK_E))
			{
				m_pHackTg->Hacking();
				m_pHackTg->SetHackState(CHackObj::STATE_NOW_HACKING);
				//m_pHackerUI->GetHackUI()->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.f));
				m_pHackerUI->AllClear();
				m_pDrone->SetControl(false);

				CreateUiMessage("Cancel", 46);
			}
		}
	}
	else 
	{//射程外なら消す
		//if (m_pHackerUI->GetHackUI() != nullptr)
		//m_pHackerUI->GetHackUI()->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.f));
	}

	//クリア判定
	if (GetMiniMng())
	{
		if (GetMiniMng()->GetClear() == true)
		{
			if (m_pHackTg != nullptr)
			{//nullでここ通るってことはカメラモードってこと
				m_pHackTg->SetHackState(CHackObj::STATE_HACKED);	//ハッキング完了
				m_pHackTg->SetRenderMode(CModel3D::ERenderMode::Render_Default);		//ハイライトエフェクトを消す
				m_pHackTg->HackObjTP();
				GetMiniMng()->SetClear(false);	//リセット
				m_pHackTg = nullptr;	//ハッキングのターゲット
				m_pDrone->SetControl(true);
			}
	
			m_pHackerUI->AllClearCancel();

			if (m_pTask)
			{//nullチェック

			 //タスクの更新
				m_pTask->SubtractHackerTask(CTask::HACKER_TASK_HACK);
			}

			DestroyUiMessage();
		}
		else if (pInput->Trigger(DIK_Q) && m_pHackTg != nullptr)
		{//ハッキングキャンセル
			if (m_pHackTg->GetHackState() == CHackObj::STATE_NOW_HACKING)
			{
				m_pHackerUI->AllClearCancel();
				m_pHackTg->SetHackState(CHackObj::STATE_NOT_HACK);	//ミニゲーム削除
				GetMiniMng()->Cancel();
				m_pDrone->SetControl(true);

				CreateUiMessage("Hack", 44);
			}
		}
	}
}


//UIメッセージの生成処理
void CHacker::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage();

	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CHacker::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}
