#ifndef _FILTER_H_
#define _FILTER_H_

class CFilter
{
public:
	CFilter();
	~CFilter();

	void SetUp(LPDIRECT3DDEVICE9 dev);
	void CleanUp();
	void Begin() { m_pDevice->SetRenderTarget(0, m_pDrowSurface); }
	void End() { m_pDevice->SetRenderTarget(0, m_pOrgSurface); }

private:
	LPDIRECT3DDEVICE9 m_pDevice;
	LPDIRECT3DTEXTURE9 m_pRenderTexture;
	IDirect3DSurface9* m_pOrgSurface;			// 元の描画用サーフェイスのポインタ
	IDirect3DSurface9* m_pDrowSurface;			// 描画用サーフェイスのポインタ
};

#endif