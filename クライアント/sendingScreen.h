//=============================================================================
//
// sendingScreen.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _SENDING_SCREEN_H
#define _SENDING_SCREEN_H


//=============================================================================
// インクルード
//=============================================================================
#include "polygon2D.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CLoadingBar;
class CText;
class CDrone;


class CSendingScreen : public CPolygon2D
{
public:

	CSendingScreen();							//コンストラクタ
	~CSendingScreen() override;					//デストラクタ

	HRESULT Init() override;					//初期化
	void Uninit() override;						//終了
	void Update() override;						//更新
	void Draw() override;						//描画

	static CSendingScreen* Create(int nMaxLoad = 1);			//生成
	static CSendingScreen* Create(std::vector<int> vData, CDrone* pDrone);		//生成

private:

	void ControlObjIdx(const int idx);			//送信したデータのインデックスの確認処理

private:

	static const D3DXVECTOR3	DEFAULT_POS;	//ディフォルトの位置
	static const D3DXVECTOR3	DEFAULT_SIZE;	//ディフォルトのサイズ
	static const D3DXCOLOR		DEFAULT_COLOR;	//ディフォルトの色

	int					m_nMaxLoad;				//ロード回数
	
	CLoadingBar*		m_pLoadingBar;			//ロードのUI
	CText*				m_pText;				//テキストへのポインタ
	CDrone*				m_pDrone;				//このオブジェクトを生成したドローンへのポインタ

	std::vector<int>	m_vSendDataIdx;			//送信するデータのインデックス
};


#endif