//=============================================================================
//
// SaveDataManager.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _SAVE_DATA_MANAGER_H_		// このマクロ定義がされてなかったら
#define _SAVE_DATA_MANAGER_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "mesh.h"
#include <fstream>
#include "model_obj.h"
#include "task.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CMesh3D;
class CModelObj;
class CEnemy;
class CGimmick;
class CWall;
class CSecurityCamera;

class CMapDataManager
{
public:

	//読み込めるデータの種類
	enum EDataType
	{
		DATA_TYPE_FIELD = 0,			//メッシュフィールド
		DATA_TYPE_MODEL,				//モデル
		DATA_TYPE_ENEMY,				//エネミー
		DATA_TYPE_GIMMICK,				//ギミック
		DATA_TYPE_WALL,					//壁

		DATA_TYPE_MAX
	};

	//エネミーの種類
	enum EEnemyType
	{
		ENEMY_TYPE_SLEEPING = 0,		//眠っているエネミー
		ENEMY_TYPE_STATIC,				//動かないエネミー
		ENEMY_TYPE_PATROLLING,			//動くエネミー

		ENEMY_TYPE_MAX
	};

	//メッシュフィールドのデータ
	struct MESHFIELD_DATA
	{
		D3DXVECTOR3				pos;				//位置
		CMesh3D::DOUBLE_INT		blockNum;			//ブロック数
		D3DXVECTOR3				fieldSize;			//フィールドのサイズ
		D3DXCOLOR				color;				//色
		int						nTextureIdx;		//テクスチャのインデックス
		bool					bSplitTexture;		//テクスチャを一枚貼るか、複数貼るかのフラグ
													
		int						nCreatedIdx;		//メッシュフィールドのベクトル内のインデックス
												
		std::vector<float>		fVtxHeight;			//全部の頂点のY座標を持っているベクトル
		CMesh3D*				pMesh;				//このデータから生成されたインスタンスへのポインタ
	};

	//モデルのデータ
	struct MODEL_DATA
	{
		D3DXVECTOR3				pos;				//位置
		D3DXVECTOR3				rot;				//向き
		int						nType;				//モデルの種類
		int						nEffectIdx;			//エフェクトのインデックス
		bool					bShadow;			//影を描画するかどうかのフラグ
		bool					bHitbox;			//当たり判定を使うかどうかのフラグ
		bool					bIgnoreDistance;	//描画の時、距離を無視するかどうか
		D3DXVECTOR3				hitboxPos;			//当たり判定の相対位置
		D3DXVECTOR3				hitboxSize;			//当たり判定のサイズ

		int						nCreatedIdx;		//モデルのベクトル内のインデックス
		CModelObj*				pModel;				//このデータから生成されたインスタンスへのポインタ
	};

	//敵のデータ
	struct ENEMY_DATA
	{
		D3DXVECTOR3					pos;					//位置
		D3DXVECTOR3					rot;					//向き
		int							nType;					//敵の種類
		float						fWakeRadius;			//プレイヤーが聞こえる範囲の半径
		std::vector <D3DXVECTOR3>	vPatrollingPoint;		//動く時の目的の位置
															
		int							nCreatedIdx;			//エネミーのベクトル内のインデックス
		CEnemy*						pEnemy;					//このデータから生成されたインスタンスへのポインタ
	};

	//ギミックのデータ
	struct GIMMICK_DATA
	{
		D3DXVECTOR3					pos;					//位置
		D3DXVECTOR3					rot;					//向き
		int							nRot;					//向きのスイッチ
		int							nType;					//ギミックの種類(CGimmickの中にある)
		int							nEffectIdx;				//エフェクトのインデックス
		int							nModelType;				//モデルの種類
		D3DXVECTOR3					extraPos;				//エクストラモデルの位置(ない可能性があります)
															
		int							nCreatedIdx;			//ギミックのベクトル内のインデックス
		CGimmick*					pGimmick;				//このデータから生成されたインスタンスへのポインタ
	};

	//壁のデータ
	struct WALL_DATA
	{
		D3DXVECTOR3					pos;					//壁の位置
		int							nRot;					//壁の向き
		int							nType;					//壁の種類
															
		bool						bCamera;				//監視カメラがあるかどうか
		D3DXVECTOR3					cameraPos;				//監視カメラの相対位置
		float						fCameraInclination;		//監視カメラのX座標の角度
															
		bool						bPc;					//ハッキングできるPCがあるかどうか
		int							nPcIdx;					//PCのインデックス

		int							nCreatedIdx;			//壁のベクトル内のインデックス
		CWall*						pWall;					//壁へのポインタ
		CSecurityCamera*			pCamera;				//監視カメラへのポインタ

	};

	struct OBJ_TO_FIND_DATA
	{
		CObject*				pObj;			//オブジェクトへのポインタ
		CTask::HACKER_TASK_TYPE	type;			//タスクの種類
		bool					bFound;			//ハッカーに見つかれたかどうか
		bool					bSent;			//データが送信されたかどうか
		bool					bGot;			//エージェントにタスクが達成したかどうか
	};

	//マップのデータ
	struct MAP_DATA
	{
		std::vector<MESHFIELD_DATA>		vMeshData;				//メッシュフィールドのベクトル
		std::vector<MODEL_DATA>			vModelData;				//モデルのベクトル
		std::vector<ENEMY_DATA>			vEnemyData;				//エネミーのベクトル
		std::vector<GIMMICK_DATA>		vGimmickData;			//ギミックのベクトル
		std::vector<WALL_DATA>			vWallData;				//壁のベクトル
		std::vector<OBJ_TO_FIND_DATA>	vToFindData;			//ハッカーが探すオブジェクトのデータ
		D3DXVECTOR3						agentPos;				//エージェントのスポーツの位置
	};

public:

	CMapDataManager();				//コンストラクタ
	~CMapDataManager();				//デストラクタ

	HRESULT Init();					//初期化
	void Uninit();					//終了

	void LoadData(void);							//データの入力

	const int GetMaxMap() { return m_nMaxMap; }		//マップの数の取得
	const MAP_DATA GetMapData(const int nIdx);		//マップデータの取得

	void SetPointerToField(const int nMapIdx, const int nCreatedIdx, CMesh3D* pObj);			//生成したメッシュフィールドへのポインタの保存処理
	void SetPointerToModel(const int nMapIdx, const int nCreatedIdx, CModelObj* pObj);			//生成したモデルへのポインタの保存処理
	void SetPointerToEnemy(const int nMapIdx, const int nCreatedIdx, CEnemy* pObj);				//生成したエネミーへのポインタの保存処理
	void SetPointerToGimmick(const int nMapIdx, const int nCreatedIdx, CGimmick* pObj);			//生成したギミックへのポインタの保存処理
	void SetPointerToWall(const int nMapIdx, const int nCreatedIdx, CWall* pObj);				//生成した壁へのポインタの保存処理
	void SetPointerToWall(const int nMapIdx, const int nCreatedIdx, CSecurityCamera* pObj);		//生成した壁へのポインタの保存処理
	void SetToFindPointer(const int nMapIdx, CTask::HACKER_TASK_TYPE type, CObject* pPointer);	//ハッカーが探すオブジェクトのポインタの保存処理
	void ClearAllPointers(const int nMapIdx);													//マップのポインタのクリア処理

	void DeleteObj(const int nMapIdx, const int nIdx);		//オブジェクトを破棄する
	void FoundObj(const int nMapIdx, const int nIdx);		//ハッカーが探すオブジェクトが見つかったフラグをtrueにする処理
	void SentObjData(const int nMapIdx, const int nIdx);	//ハッカーが探すオブジェクトのデータが送信されたにする処理
	void GotObj(const int nMapIdx, const int nIdx);			//ハッカーが探すオブジェクトのタスクがエージェントに達成したフラグをtrueにする処理

	static CMapDataManager* Create();				//生成処理

private:

	void LoadFilePaths(std::vector<std::string>& vPath);		//マップデータのファイルパスの読み込み
	void LoadMapData(std::string path);							//マップデータの読み込み

private:
	int m_nMaxMap;								//マップの数
	std::vector<MAP_DATA> m_mapData;			//マップデータを持っているベクトル
};




#endif