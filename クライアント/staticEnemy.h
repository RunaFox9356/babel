//=============================================================================
//
// staticEnemy.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _STATIC_ENEMY_H
#define _STATIC_ENEMY_H


//=============================================================================
// インクルード
//=============================================================================
#include "enemy.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CViewField;


class CStaticEnemy : public CEnemy
{
public:

	CStaticEnemy();
	~CStaticEnemy() override;


	HRESULT Init() override;			// 初期化
	void Uninit() override;				// 終了
	void Update() override;				// 更新

	void SetEyePos(const D3DXVECTOR3 eyePos);		//目の相対位置の設定
	void SetSecurityLevel(const int nSecurity) override;	//セキュリティーレベルのセッター


	static CStaticEnemy* Create(const D3DXVECTOR3 pos, D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f), const float fLookAroundAngle = LOOK_AROUND_ANGLE);	//生成

private:

	void UpdateState();								//状態によって更新
	void LookAround();								//周りを見る
	void LookAroundAPoint(const D3DXVECTOR3 point);	//位置の周りを見る
	void Attack(const bool isOnline = false);		//弾を撃つ
	void LookAtPlayer(bool isOnline = false);		//プレイヤーのほうに向かう
	bool LookAtPos(const D3DXVECTOR3 pos);			//ポイントの方に向かう

private:

	//挙動の状態
	enum EState
	{
		STATE_LOOK_AROUND = 0,

		STATE_ATTACK,

		STATE_ONLINEATTACK,

		STATE_HEARD_SOUND,

		STATE_MAX
	};


	static const D3DXVECTOR3		EYE_RELATIVE_POS;			//目の相対位置
	static const float				LOOK_AROUND_ANGLE;			//周りを見る時のディフォルトの最大角度
	static const D3DXVECTOR3		DEFAULT_COLLISION_POS;		//ディフォルトの当たり判定の相対位置
	static const D3DXVECTOR3		DEFAULT_COLLISION_SIZE;		//ディフォルトの当たり判定のサイズ
	static const int				DEFAULT_HAND_INDEX;			//ディフォルトの手のモデルのインデックス
	static const float				DEFAULT_ROTATION_FRICTION;	//ディフォルトの回転の摩擦係数
	static const float				ATTACK_ROTATION_FRICTION;	//攻撃の時の回転の摩擦係数
	static const int				DEFAULT_WAIT_FRAMES;		//ディフォルトの待つフレーム数


	int				m_nCntAnim;							//アニメーション用の変数
	int				m_nCntPhase;						//アニメーション用の変数
	float			m_fLookAroundAngle;					//周りを見る時の最大角度
	float			m_fOriginalRot;						//元の向き
	float			m_fTargetRot;						//目的の向き
	bool			m_bSeen;							//プレイヤーを見たかどうか	

	EState			m_state;							//現在の状態

	CViewField*		m_pViewField;						//視野へのポインタ
};


#endif