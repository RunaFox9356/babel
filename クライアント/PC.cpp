//=============================================================================
//
// PC.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "PC.h"
#include "model_obj.h"
#include "collision_rectangle3D.h"
#include "application.h"
#include "task.h"
#include "game.h"
#include "text.h"
#include "agent.h"
#include "UImessage.h"
#include "bomb.h"
#include "input.h"
#include "model_data.h"
#include "hacker.h"
#include "mapDataManager.h"

//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3 CGimmickPC::DEFAULT_HITBOX_POS = { 0.0f, 49.5f, 0.0f };				//ディフォルトのヒットボックスの相対位置
const D3DXVECTOR3 CGimmickPC::DEFAULT_SIZE = { 60.0f, 99.0f, 50.0f };					//ディフォルトのサイズ
const D3DXVECTOR3 CGimmickPC::DEFAULT_ACTIVATION_HITBOX_POS = { 0.0f, 49.5f, 0.0f };	//ディフォルトのヒットボックスの相対位置
const D3DXVECTOR3 CGimmickPC::DEFAULT_ACTIVATION_SIZE = { 110.0f, 99.0f, 100.0f };		//ディフォルト当たり判定のサイズ


namespace HPc
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
};


//コンストラクタ
CGimmickPC::CGimmickPC() : m_pModel(nullptr),
m_pActivationArea(nullptr),
m_pUiMessage(nullptr),
m_pBomb(nullptr)
{

}

//デストラクタ
CGimmickPC::~CGimmickPC()
{

}

// 初期化
HRESULT CGimmickPC::Init()
{
	m_pModel = CModelObj::Create();		//モデルの生成
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));
	if (m_pModel)
	{//生成出来たら

		m_pModel->SetType(53);			//モデルの種類の設定
		
		CCollision_Rectangle3D* pCollision = m_pModel->GetCollision();	//モデルの当たり判定の取得

		if (pCollision)
		{
			pCollision->SetPos(DEFAULT_HITBOX_POS);	//当たり判定の相対位置の設定
			pCollision->SetSize(DEFAULT_SIZE);		//当たり判定のサイズの設定
		}
	}

	m_pActivationArea = CCollision_Rectangle3D::Create();		

	if (m_pActivationArea)
	{
		m_pActivationArea->SetPos(DEFAULT_ACTIVATION_HITBOX_POS);
		m_pActivationArea->SetSize(DEFAULT_ACTIVATION_SIZE);
		m_pActivationArea->SetParent(this);
	}

	CTask* pTask = nullptr;

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
	{
		pTask = CGame::GetTask();			//タスクの取得
		SetRenderMode(0);
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pTask = CHacker::GetTask();			//タスクの取得
		SetRenderMode(1);
	}

	if (pTask)
	{//nullチェック

		//タスクの更新
		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			pTask->AddTask(CTask::AGENT_TASK_DESTROY);
		else if (mode == CApplication::MODE_HACKER)
			pTask->AddHackerTask(CTask::HACKER_TASK_FIND_DESTROYABLE);
	}

	SetAnimation(false);
	return S_OK;
}

// 終了
void CGimmickPC::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}
	//当たり判定の破棄
	if (m_pActivationArea)
	{
		m_pActivationArea->Uninit();
		m_pActivationArea = nullptr;
	}

	//UIメッセージの破棄
	DestroyUiMessage();

	Release();		//メモリの解放
}	

// 更新
void CGimmickPC::Update()
{
	if (m_pActivationArea->Collision(CObject::OBJETYPE_PLAYER, false))
	{//プレイヤーが爆弾を置くことができる範囲内だったら

		CInput* pInput = CApplication::GetInstance()->GetInput();		//インプットの取得

		if (pInput->Trigger(DIK_E) && !m_pBomb)
		{//Eボタンが押されて、爆弾はまだ設置していなかったら

			CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

			if (pPlayer)
			{//nullチェック
				
				SetAnimation(true);
				DestroyUiMessage();							//UIメッセージの破棄
				m_pBomb = CBomb::Create(pPlayer->GetPos());	//爆弾の生成
			}
		}
		else
		{
			//UIメッセージがまだ生成されていなくて、爆弾がまだ設置していなかったら
			if (!m_pUiMessage && !m_pBomb)
				CreateUiMessage();				//UIメッセージの生成
		}
	}
	else if (m_pUiMessage)						//プレイヤーが爆弾を設置せず離れる場合
		DestroyUiMessage();						//UIメッセージの破棄

	if (m_pBomb && m_pBomb->GetDeath())
	{//爆弾が爆発したら

		m_pBomb->Uninit();		//爆弾の終了処理
		//m_pBomb = nullptr;		//ポインタをnullにする
		Uninit();				//終了処理

		DeleteData();

		CTask* pTask = nullptr;

		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			pTask = CGame::GetTask();			//タスクの取得

		if (pTask)
		{//nullチェック

			//タスクの更新
			pTask->SubtractTask(CTask::AGENT_TASK_DESTROY);
		}
	}
	//オンラインでボムが置かれた時の処理
	if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse && !m_pBomb&&!GetFlagDeath())
	{
		CPlayer* pPlayer = CHacker::GetPlayer();
		if (pPlayer != nullptr)
		{
			m_pBomb = CBomb::Create(pPlayer->GetPos());
		}
		
	}

	if (!GetIsOnMap() && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isMap)
		SetIsOnMap(true);

	//オンラインで起爆ボタンが押された時の処理
	if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_PlayData.m_pushBomComands&&m_pBomb)
	{
		m_pBomb->SetDeath(true);
	}
	//ローカルの起爆ボタンが押された時の処理
	if (CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->Player.m_PlayData.m_pushBomComands&&m_pBomb)
	{
		m_pBomb->SetDeath(true);
	}
}

// 描画
void CGimmickPC::Draw()
{

}

// 位置のセッター
void CGimmickPC::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;

	//モデルの位置の設定
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPos(pos);
	}
}

// 過去位置のセッター
void CGimmickPC::SetPosOld(const D3DXVECTOR3 &/*posOld*/)
{

}

// 向きのセッター
void CGimmickPC::SetRot(const D3DXVECTOR3 &rot)
{
	m_rot = rot;

	//モデルの向きの設定
	if (m_pModel)
	{//nullチェック
		m_pModel->SetRot(rot);
	}
}

// 大きさのセッター
void CGimmickPC::SetSize(const D3DXVECTOR3 &/*size*/)
{

}

//エフェクトのインデックスの設定
void CGimmickPC::SetRenderMode(int mode)
{
	if (m_pModel)
		m_pModel->SetRenderMode(mode);
}

// モデルインデックスの設定処理
void CGimmickPC::ChangeModel(int idx)
{
	if (idx < 0 || idx > CModel3D::GetMaxModel() - 1)
		idx = 53;

	if (m_pModel)
	{
		m_pModel->SetType(idx);

		CModel3D* pModel = m_pModel->GetModel();
		CCollision_Rectangle3D* pCollision = m_pModel->GetCollision();

		if (pCollision && pModel)
		{
			D3DXVECTOR3 size = pModel->GetMyMaterial().size, pos = D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f);

			pCollision->SetSize(size);
			pCollision->SetPos(pos);

			if (m_pActivationArea)
			{
				m_pActivationArea->SetSize(D3DXVECTOR3(size.x + 50.0f, size.y, size.z + 50.0f));
				m_pActivationArea->SetPos(pos);
			}
		}
	}
}

//マップに表示されているかどうかの設定処理
void CGimmickPC::SetIsOnMap(const bool bOnMap)
{
	CGimmick::SetIsOnMap(bOnMap);

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (!bOnMap)
		return;

	if (mode == CApplication::MODE_HACKER)
		SetRenderMode(0);
	else if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
		SetRenderMode(1);
}



//=============================================================================
//
//								静的関数
//
//=============================================================================




//生成
CGimmickPC* CGimmickPC::Create(const D3DXVECTOR3 pos)
{
	CGimmickPC* pPC = new CGimmickPC;			//インスタンスを生成する

	if (FAILED(pPC->Init()))
	{//初期化
		return nullptr;
	}

	pPC->SetPos(pos);			//位置の設定

	return pPC;					//生成したインスタンスを返す
}

//生成
CGimmickPC* CGimmickPC::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot)
{
	CGimmickPC* pPC = new CGimmickPC;			//インスタンスを生成する

	if (FAILED(pPC->Init()))
	{//初期化
		return nullptr;
	}

	pPC->SetPos(pos);			//位置の設定
	pPC->SetRot(rot);			//回転角度の設定

	return pPC;					//生成したインスタンスを返す
}



//=============================================================================
//
//							プライベート関数
//
//=============================================================================



//UIメッセージの生成処理
void CGimmickPC::CreateUiMessage()
{
	DestroyUiMessage();

	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 150.0f, 0.0f), D3DXVECTOR3(85.0f, 85.0f, 0.0f), HPc::E_BUTTON_TEXTURE_IDX, "Set Bomb", D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CGimmickPC::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}

//このオブジェクトの保存したデータを消す
void CGimmickPC::DeleteData()
{
	CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
	int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

	if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
	{//マップデータマネージャーのnullチェックとマップインデックスの確認

	 //ハッカーのターゲットのデータを取得する
		std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

		for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
		{//全部確認する

			if (vData.data()[nCnt].pObj == this)
			{
				pDataManager->DeleteObj(nMapIdx, nCnt);
			}
		}

	}
}
