//=============================================================================
//
// エネミークラス(enemy.h)
// Author : 唐�ｱ結斗
// 概要 : エネミー生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "enemy.h"
#include "game.h"
#include "mesh.h"
#include "motion.h"
#include "renderer.h"
#include "application.h"
#include "calculation.h"
#include "move.h"
#include "collision_rectangle3D.h"
#include "debug_proc.h"
#include "parts.h"
#include "sound.h"
#include "scene_mode.h"
#include "model_data.h"
#include "task.h"
#include "gun.h"
#include "agent.h"
#include "utility.h"
#include "particle_emitter.h"
#include "hacker.h"
#include "mapDataManager.h"
#include "exclamationMark.h"

//--------------------------------------------------------------------
// 静的メンバ変数の定義
//--------------------------------------------------------------------
const int		CEnemy::DEFAULT_AIM_DELAY = 45;						//狙う時間
const int		CEnemy::DEFAULT_BURST_DELAY = 15;					//
const int		CEnemy::DEFAULT_RECHARGE_DELAY = 120;				//
const float		CEnemy::DEFAULT_BULLET_SPEED = 10.0f;				//ディフォルトの弾の速度
const float		CEnemy::DEFAULT_ROTATION_FRICTION = 0.05f;			//ディフォルトの回転の摩擦係数

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CEnemy * CEnemy::Create()
{
	// オブジェクトインスタンス
	CEnemy *pEnemy = nullptr;

	// メモリの解放
	pEnemy = new CEnemy;

	// メモリの確保ができなかった
	assert(pEnemy != nullptr);

	// 数値の初期化
	pEnemy->Init();

	// インスタンスを返す
	return pEnemy;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CEnemy::CEnemy() : m_pMove(nullptr),
m_pCollision(nullptr),
m_rotDest(D3DXVECTOR3(0.0f,0.0f,0.0f)),
m_nNumMotion(0),
m_nBulletShot(0),
m_nAttackPattern(0),
m_nNumHandPart(0),
m_nLife(0),
m_nCntShoot(0),
m_nBurstBullet(0),
m_nReloadTime(0),
m_nSecurityLevel(0),
m_fRotFriction(0.0f),
m_bDeath(false),
m_bFound(false),
m_bHeardSound(false),
m_bOnMap(false),
m_bDeathAnimation(false),
m_pGun(nullptr),
m_nStun(1),
m_pAllertUi(nullptr)
{
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CEnemy::~CEnemy()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CEnemy::Init()
{
	// 初期化
	CMotionModel3D::Init();

	//SetId(CApplication::GetInstance()->GetSceneMode()->PoshEnemy(false));
	// オブジェクトタイプの設定
	SetObjType(OBJETYPE_ENEMY);

	// 移動クラスのメモリ確保
	m_pMove = new CMove;
	assert(m_pMove != nullptr);

	// 3D矩形の当たり判定の設定
	m_pCollision = CCollision_Rectangle3D::Create();
	m_pCollision->SetParent(this);
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshEnemyUse(true));
	m_nBulletShot = 0;
	m_nAttackPattern = hmd::IntRandom(SHOOT_MODEL_MAX, 0);
	m_nReloadTime = 30;

	m_fRotFriction = DEFAULT_ROTATION_FRICTION;				//回転の摩擦係数の設定

	CTask* pTask = nullptr;

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
	{
		pTask = CGame::GetTask();			//タスクの取得
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pTask = CHacker::GetTask();			//タスクの取得
	}

	if (pTask)
	{//nullチェック

		//タスクの更新
		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			pTask->AddTask(CTask::AGENT_TASK_KILL);
		else if (mode == CApplication::MODE_HACKER)
			pTask->AddHackerTask(CTask::HACKER_TASK_FIND_ENEMY);
	}

	//UIの生成
	m_pAllertUi = CExclamationMark::Create(this);

	if (m_pAllertUi)
	{//nullチェック

		m_pAllertUi->SetDraw(false);
	}
	CApplication::GetInstance()->GetSceneMode()->SetEnemyDes(false, GetId());
	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CEnemy::Uninit()
{
	CApplication::GetInstance()->GetSceneMode()->SetEnemyUse(false,GetId());
	if (m_pMove != nullptr)
	{// メモリの解放
		delete m_pMove;
		m_pMove = nullptr;
	}

	if (m_pCollision != nullptr)
	{// 終了処理
		m_pCollision->Uninit();
		m_pCollision = nullptr;
	}

	if (m_pGun != nullptr)
	{
		m_pGun->Uninit();
		m_pGun = nullptr;
	}

	//UIの破棄
	if (m_pAllertUi)
	{
		m_pAllertUi->Uninit();
		m_pAllertUi = nullptr;
	}

	// 終了
	CMotionModel3D::Uninit();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CEnemy::Update()
{
	if (!m_bDeath && m_nStun > 0)
	{
		// 位置の取得
		D3DXVECTOR3 pos = GetPos();

		// 過去位置の更新
		SetPosOld(pos);

		// 回転
		Rotate();

		// 更新
		CMotionModel3D::Update();

		//弾と当たったかどうかの判定
		WasHitByBullet();

		if (!m_bOnMap && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isMap)
			m_bOnMap = true;

		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDes&&CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
		{
			if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_log >= 0 && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.mId >= 0)
			{
				m_bDeath = true;			
				//Uninit();
			}
		}
		else
		{
			D3DXVECTOR3 vec = GetRot();
			vec.x = 0.0f;
			m_bDeathAnimation = false;
			SetRot(vec);
		}
	}
	else
	{
		KillAnimation();

		if (!CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].isDes&&CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
		{
			D3DXVECTOR3 vec = GetRot();
			vec.x = 0.0f;
			m_bDeathAnimation = false;
			SetRot(vec);
			m_bDeath = false;
		}
	}

	if (CApplication::GetInstance()->GetEnemyConnect() &&CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
	{
		D3DXVECTOR3 pos = CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopEnemy[GetId()].Pos;
		SetPos(pos);
	}
	
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CEnemy::Draw()
{
	// 描画
	CMotionModel3D::Draw();
}

// 敵を殺す 
void CEnemy::Kill()
{
	CTask* pTask = nullptr;
	
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
		pTask = CGame::GetTask();			//タスクの取得

	if (pTask)
	{//nullチェック

		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			//タスクの更新
			pTask->SubtractTask(CTask::AGENT_TASK_KILL);
	}

	CMapDataManager* pDataManager = CApplication::GetInstance()->GetMapDataManager();		//データマネージャーの取得
	int nMapIdx = CApplication::GetInstance()->GetMap();									//マップの最大数を取得する

	if (pDataManager && nMapIdx >= 0 && nMapIdx < pDataManager->GetMaxMap())
	{//マップデータマネージャーのnullチェックとマップインデックスの確認

	 //ハッカーのターゲットのデータを取得する
		std::vector<CMapDataManager::OBJ_TO_FIND_DATA> vData = pDataManager->GetMapData(nMapIdx).vToFindData;

		for (int nCnt = 0; nCnt < (int)vData.size(); nCnt++)
		{//全部確認する

			if (vData.data()[nCnt].pObj == this)
			{
				pDataManager->DeleteObj(nMapIdx, nCnt);
			}
		}

		std::vector<CMapDataManager::ENEMY_DATA> vEnemyData = pDataManager->GetMapData(nMapIdx).vEnemyData;

		for (int nCnt = 0; nCnt < (int)vEnemyData.size(); nCnt++)
		{//全部確認する

			if (vEnemyData.data()[nCnt].pEnemy == this)
			{
				pDataManager->SetPointerToEnemy(nMapIdx, vEnemyData.data()[nCnt].nCreatedIdx, nullptr);
			}
		}

	}

	CApplication::GetInstance()->GetSceneMode()->SetEnemyOnMap(false, GetId());

	m_bDeath = true;
	SetRenderMode(CModel3D::Render_Default);

	CApplication::GetInstance()->GetSceneMode()->SetEnemyDes(m_bDeath, GetId());
	if (m_pAllertUi)
	{
		m_pAllertUi->Uninit();
		m_pAllertUi = nullptr;
	}
	if (mode == CApplication::MODE_GAME)
	{
		CApplication::GetInstance()->GetSceneMode()->SetEnemyAnime(GetMotion()->GetNumMotion(), GetId());
	}
	else
	{
		CApplication::GetInstance()->GetSceneMode()->SetEnemyAnime(0, GetId());
	}
	if (mode == CApplication::MODE_HACKER)
	{
		GetMotion()->SetNumMotion(CApplication::GetInstance()->GetSceneMode()->GetEnemy(GetId()).isMotion);
	}
	
	//Uninit();									//終了処理
}

//マップに表示されているかどうかの設定処理
void CEnemy::SetIsOnMap(const bool bOnMap)
{
	m_bOnMap = bOnMap;

	CApplication::GetInstance()->GetSceneMode()->SetEnemyOnMap(bOnMap, GetId());

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (!bOnMap)
		return;

	if (mode == CApplication::MODE_HACKER)
		SetRenderMode(CModel3D::Render_Default);
	else if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
		SetRenderMode(CModel3D::Render_Highlight);
}

// 手のモデルのインデックスの設定
void CEnemy::SetHandModelIdx(const int nHandIdx)
{
	// モーション情報の取得
	CMotion *pMotion = CMotionModel3D::GetMotion();

	//手のモデルインデックスの設定
	m_nNumHandPart = nHandIdx;
	
	// 手のパーツの取得
	CParts *pHand = pMotion->GetParts(m_nNumHandPart);

	//親の設定
	m_pGun->CItemObj::SetParent(pHand);					
}

// 聞いた音の位置の設定
void CEnemy::SetSoundPos(const D3DXVECTOR3 soundPos)
{
	if (!m_bHeardSound)
	{
		m_bHeardSound = true;
		m_soundPos = soundPos;
	}
}

// スタンしたときの設定
void CEnemy::Stun()
{
	m_nStun--;
	if (m_nStun<=0)
	{

		Kill();
		return;
		/*if (m_pGun)
		{
			m_pGun->CItemObj::SetParent();
			m_pGun->SetParent();
			m_pGun->SetHeldFlag(false);
			m_pGun = nullptr;
		}*/
		SetActionState(NEUTRAL_ACTION);

		if (m_pAllertUi)
		{
			m_pAllertUi->Uninit();
			m_pAllertUi = nullptr;
		}

		CTask* pTask = nullptr;

		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			pTask = CGame::GetTask();			//タスクの取得

		if (pTask)
		{//nullチェック

			if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
				//タスクの更新
				pTask->SubtractTask(CTask::AGENT_TASK_KILL);
		}
	}
}

void CEnemy::IncreaseSecurity()
{
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (mode != CApplication::MODE_GAME && mode != CApplication::MODE_AGENT)
		return;

	CGame* pGame = (CGame*)CApplication::GetInstance()->GetSceneMode();

	if (!pGame)
		return;

	pGame->IncreaseSecurity();
}


// 銃の生成
void CEnemy::CreateGun(const int nHandIdx)
{
	if (m_pGun)
		return;

	m_pGun = CGun::Create();		//銃の生成

	if (m_pGun)
	{//nullチェック

		m_pGun->SetPos(D3DXVECTOR3(0.f, 0.f, 500.0f));	//位置の設定
		m_pGun->SetType(56);							//モデルの種類の設定
		m_pGun->SetMaxBullet(6);						//弾数カウントの最大数
		m_pGun->SetMaxInterval(5);						//弾発射までのインターバルの最大数
		m_pGun->SetMaxReload(6);						//リロードカウントの最大数
		m_pGun->SetRapidFire(false);
		m_pGun->SetHeldFlag(true);

		//銃の当たり判定の生成と設定
		CCollision_Rectangle3D* pCollision = m_pGun->GetCollision();
		D3DXVECTOR3 modelSize = m_pGun->GetModel()->GetMyMaterial().size;

		if (pCollision)
		{
			pCollision->SetSize(modelSize);
			pCollision->SetPos(D3DXVECTOR3(0.0f, modelSize.y / 2.0f, 0.0f));
		}

		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		m_nNumHandPart = nHandIdx;		//手のパーツの番号の設定

		// 手のパーツの取得
		CParts *pHand = pMotion->GetParts(m_nNumHandPart);
		m_pGun->CItemObj::SetParent(pHand);

		m_pGun->SetReload(true);		//銃をリロードする
	}
}

// 弾を撃つ(ランダムなパターン)
const bool CEnemy::Shoot()
{
	//攻撃パターンの設定
	int nSwitch = m_nAttackPattern;
	bool bEnd = false;

	if (nSwitch < 0)
		nSwitch = 0;
	else if (nSwitch >= SHOOT_MODEL_MAX)
		nSwitch = SHOOT_MODEL_MAX - 1;

	//攻撃カウンターをデクリメントする
	m_nCntShoot--;

	if (m_nReloadTime <= 0)
	{
		switch (nSwitch)
		{
		case SHOOT_MODEL_SINGLE:

			bEnd = SingleShoot();

			break;

		case SHOOT_MODEL_DOUBLE:

			bEnd = DoubleShoot();

			break;

		case SHOOT_MODEL_TRIPLE:

			bEnd = TripleShoot();

			break;

		default:
			break;
		}
	}
	else
	{
		m_nReloadTime--;		//リロード時間の更新
	}

	if (m_nBulletShot > m_pGun->GetMaxBullet())
	{//弾がなくなったら、リロードする

	 //カウンターを0に戻す
		m_nBurstBullet = 0;
		m_nBulletShot = 0;

		m_nReloadTime = DEFAULT_RECHARGE_DELAY;			//リロード時間の設定
		m_pGun->SetReload(true);						//銃のリロード処理
	}

	return bEnd;
}

// 弾を撃つ
const bool CEnemy::Shoot(const int nShootModelIdx)
{
	if (!m_pGun)
		return true;

	//引数の値を確認する
	int nSwitch = nShootModelIdx;
	bool bEnd = false;

	if (nSwitch < 0)
		nSwitch = 0;
	else if (nSwitch >= SHOOT_MODEL_MAX)
		nSwitch = SHOOT_MODEL_MAX - 1;

	//攻撃カウンターをデクリメントする
	m_nCntShoot--;

	if (m_nReloadTime <= 0)
	{
		switch (nSwitch)
		{
		case SHOOT_MODEL_SINGLE:

			bEnd = SingleShoot();		

			break;

		case SHOOT_MODEL_DOUBLE:

			bEnd = DoubleShoot();

			break;

		case SHOOT_MODEL_TRIPLE:

			bEnd = TripleShoot();

			break;

		default:
			break;
		}
	}
	else
	{
		m_nReloadTime--;		//リロード時間の更新
	}

	if (m_nBulletShot > m_pGun->GetMaxBullet())
	{//弾がなくなったら、リロードする

		//カウンターを0に戻す
		m_nBurstBullet = 0;								
		m_nBulletShot = 0;	

		m_nReloadTime = DEFAULT_RECHARGE_DELAY;			//リロード時間の設定
		m_pGun->SetReload(true);						//銃のリロード処理
	}

	return bEnd;
}

// 音を聞いていない状態に戻す
void CEnemy::SoundPosReset()
{
	m_bHeardSound = false;
	m_soundPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}


void CEnemy::SetActionState(ACTION_STATE state)
{
	CMotion* pMotion = GetMotion();

	if (pMotion)
	{
		pMotion->SetNumMotion(state);
	}
}

//=============================================================================
// 回転
// Author : 唐�ｱ結斗
// 概要 : 目的の向きまで回転する
//=============================================================================
void CEnemy::Rotate()
{
	// 向きの取得
	D3DXVECTOR3 rot = GetRot();

	// 向きの更新
	rot.y += (m_rotDest.y - rot.y) * m_fRotFriction;

	// 向きの正規化
	rot.y = CCalculation::RotNormalization(rot.y);

	// 向きの設定
	SetRot(rot);
}

//UIの描画設定
void CEnemy::SetExclamationMarkDraw(const bool bDraw)
{
	if (m_pAllertUi)
	{//nullチェック

		m_pAllertUi->SetDraw(bDraw);
	}
}

// 弾に当たったかどうかの判定
bool CEnemy::WasHitByBullet()
{
	CCollision_Rectangle3D* pCollision = GetCollision();	//当たり判定の取得

	//取得できなかった場合、falseを返す
	if (!pCollision)
		return false;

	if (pCollision->Collision(CObject::OBJTYPE_PLAYER_BULLET, false))
	{
		// プレイヤーの弾であることが保障されているはず
		std::vector<CObject*> pObj = pCollision->GetCollidedObj();
		for (auto& p : pObj)
		{
			p->Uninit();
		}

		m_nLife--;					//ライフをデクリメントする
		CParticleEmitter* pEmitter = CParticleEmitter::Create("Bullet");
		pEmitter->SetPos(GetPos());
		if (m_nLife <= 0)
			SetDeath(true);			//死亡フラグをtrueにする

		return true;				//弾と当たったので、trueを返す
	}

	return false;					//弾と当たっていないので、falseを返す
}

// 弾の速度の設定
void CEnemy::SetBulletSpeed()
{
	CAgent* pPlayer = CGame::GetPlayer();		//プレイヤーの取得

	if (pPlayer && m_pGun)
	{//プレイヤーと銃のnullチェック

		D3DXVECTOR3 vec = D3DXVECTOR3(hmd::FloatRandam(7.0f, -7.0f), hmd::FloatRandam(7.0f, -7.0f), hmd::FloatRandam(7.0f, -7.0f));
		D3DXVECTOR3 dir = (pPlayer->GetPlayerBody() + vec) - m_pGun->GetBulletSpawn();		//この敵からプレイヤーまでのベクトル
		D3DXVec3Normalize(&dir, &dir);						//上のベクトルを正規化する

		//ベクトルの長さをディフォルトの弾の速度にする
		dir.x *= DEFAULT_BULLET_SPEED;						
		dir.y *= DEFAULT_BULLET_SPEED;						
		dir.z *= DEFAULT_BULLET_SPEED;						

		m_pGun->SetBulletMove(dir);							//弾の速度の設定
	}
}

// 攻撃
const bool CEnemy::SingleShoot()
{
	bool bEnd = false;

	if (m_nCntShoot <= 0)
	{//ディレイが経ったら

		m_nCntShoot = DEFAULT_AIM_DELAY;		//ディレイの設定

		SetBulletSpeed();						//弾の速度の設定
		m_pGun->SetShot(true);					//弾の発生

		m_nBulletShot++;						//撃った弾のカウンターをインクリメントする
		bEnd = true;							//攻撃が終わったことにする
	}
	else if (m_nCntShoot == DEFAULT_AIM_DELAY - 1)
	{//攻撃後リロードする
		m_pGun->SetReload(true);
	}

	return bEnd;
}

// 攻撃
const bool CEnemy::DoubleShoot()
{
	bool bEnd = false;

	if (m_nCntShoot <= 0)
	{//ディレイが経ったら

		m_nCntShoot = DEFAULT_BURST_DELAY;		//ディレイの設定

		SetBulletSpeed();						//弾の速度の設定
		m_pGun->SetShot(true);					//弾の発生

		m_nBurstBullet++;						//攻撃の弾の数をインクリメントする
		m_nBulletShot++;						//撃った弾のカウンターをインクリメントする

		if (m_nBurstBullet > 1)
		{//2つの弾を連続撃ったら
			m_nCntShoot = DEFAULT_AIM_DELAY;	//ディレイの設定
			m_nBurstBullet = 0;					//攻撃の弾の数を0に戻す
			bEnd = true;						//攻撃が終わったことにする
		}
	}
	else if (m_nCntShoot == DEFAULT_BURST_DELAY - 1)
	{//攻撃後リロードする
		m_pGun->SetReload(true);
	}

	return bEnd;
}

// 攻撃
const bool CEnemy::TripleShoot()
{
	bool bEnd = false;

	if (m_nCntShoot <= 0)
	{//ディレイが経ったら

		m_nCntShoot = DEFAULT_BURST_DELAY;			//ディレイの設定

		SetBulletSpeed();							//弾の速度の設定
		m_pGun->SetShot(true);						//弾の発生

		m_nBurstBullet++;							//攻撃の弾の数をインクリメントする
		m_nBulletShot++;							//撃った弾のカウンターをインクリメントする

		if (m_nBurstBullet > 2)
		{//3つの弾を連続撃ったら
			m_nCntShoot = DEFAULT_AIM_DELAY;		//ディレイの設定
			m_nBurstBullet = 0;						//攻撃の弾の数を0に戻す
			bEnd = true;							//攻撃が終わったことにする
		}
	}
	else if (m_nCntShoot == DEFAULT_BURST_DELAY - 1)
	{//攻撃後リロードする
		m_pGun->SetReload(true);
	}


	return bEnd;
}

//=============================================================================
// 死んだときの動き
// Author : 浜田
// 概要 : 目的の向きまで回転する
//=============================================================================
void CEnemy::KillAnimation()
{
	if (m_bDeathAnimation)
	{
		return;
	}
	float Move = 1.0f;

	D3DXVECTOR3 vec = GetRot();
	D3DXVECTOR3 pos = GetPos();
	vec.x += 0.1f;

	if (D3DX_PI*0.5f <= vec.x)
	{
		m_bDeathAnimation = true;
	}
	SetRot(vec);
	D3DXVec3Normalize(&vec, &vec);
	
	pos.y = 10;
	pos.z += Move*vec.z;

	SetPos(pos);
}