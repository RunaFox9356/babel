//=============================================================================
//
// �Q�[���N���X(game.cpp)
// Author : �������l
// �T�v : �Q�[���N���X�̊Ǘ����s��
//
//=============================================================================

//*****************************************************************************
// �C���N���[�h
//*****************************************************************************
#include <assert.h>

#include "debug_scene.h"
#include "calculation.h"
#include "input.h"
#include "application.h"
#include "camera.h"
#include "renderer.h"
#include "object.h"
#include "polygon3D.h"
#include "player.h"
#include "motion_model3D.h"
#include "model3D.h"
#include "model_obj.h"
#include "mesh.h"
#include "sphere.h"
#include "bg.h"
#include "model_obj.h"
#include "debug_proc.h"
#include "sound.h"
#include "line.h"
#include "collision_rectangle3D.h"
#include "drone.h"
#include "agent.h"
#include "enemy.h"
#include "model_data.h"
#include "motion.h"
#include "GimmicDoor.h"
#include "map.h"
//*****************************************************************************
// �ÓI�����o�ϐ��錾
//*****************************************************************************
CPlayer *CDebug_Scene::m_pPlayer = nullptr;					// �v���C���[�N���X
CDrone *CDebug_Scene::m_pDrone = nullptr;						// �h���[���N���X
D3DXCOLOR CDebug_Scene::fogColor;								// �t�H�O�J���[
float CDebug_Scene::fFogStartPos;								// �t�H�O�̊J�n�_
float CDebug_Scene::fFogEndPos;								// �t�H�O�̏I���_
float CDebug_Scene::fFogDensity;								// �t�H�O�̖��x
bool CDebug_Scene::m_bGame = false;							// �Q�[���̏�

//=============================================================================
// �R���X�g���N�^
// Author : �������l
// �T�v : �C���X�^���X�������ɍs������
//=============================================================================
CDebug_Scene::CDebug_Scene()
{

}

//=============================================================================
// �f�X�g���N�^
// Author : �������l
// �T�v : �C���X�^���X�I�����ɍs������
//=============================================================================
CDebug_Scene::~CDebug_Scene()
{

}

//=============================================================================
// ������
// Author : �������l
// �T�v : ���_�o�b�t�@�𐶐����A�����o�ϐ��̏����l��ݒ�
//=============================================================================
HRESULT CDebug_Scene::Init()
{// ���̓f�o�C�X�̎擾
	CInput *pInput = CInput::GetKey();

	// �f�o�C�X�̎擾
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// �T�E���h���̎擾
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM001);

	// �d�͂̒l��ݒ�
	CCalculation::SetGravity(0.2f);

	// �X�J�C�{�b�N�X�̐ݒ�
	CSphere *pSphere = CSphere::Create();
	pSphere->SetRot(D3DXVECTOR3(D3DX_PI, 0.0f, 0.0f));
	pSphere->SetSize(D3DXVECTOR3(100.0f, 0, 100.0f));
	pSphere->SetBlock(CMesh3D::DOUBLE_INT(100, 100));
	pSphere->SetRadius(10000.0f);
	pSphere->SetSphereRange(D3DXVECTOR2(D3DX_PI * 2.0f, D3DX_PI * -0.5f));
	pSphere->LoadTex(1);

	// �n�ʂ̐���
	CMesh3D *pField = CMesh3D::Create();
	pField->SetSize(D3DXVECTOR3(20000.0f, 0.0f, 20000.0f));
	pField->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
	pField->SetSplitTex(true);
	pField->SetUseCollison(true);

	// �v���C���[�̐ݒ�
	m_pPlayer = CPlayer::Create();
	m_pPlayer->SetMotion("data/MOTION/motion.txt");

	// �G�l�~�[�̐ݒ�
	CEnemy *pEnemy = CEnemy::Create();
	pEnemy->SetMotion("data/MOTION/motion.txt");
	pEnemy->SetPos(D3DXVECTOR3(0.0f, 0.0f, 30.0f));
	CCollision_Rectangle3D* pCollision = pEnemy->GetCollision();
	pCollision->SetSize(D3DXVECTOR3(30.0f, 60.0f, 30.0f));
	pCollision->SetPos(D3DXVECTOR3(0.0f, 30.0f, 0.0f));

	// ���f��
	CModelObj *pModel = CModelObj::Create();
	pModel->SetType(1);
	pCollision = pModel->GetCollision();
	D3DXVECTOR3 modelSize = pModel->GetModel()->GetMyMaterial().size;
	pCollision->SetSize(modelSize);
	pCollision->SetPos(D3DXVECTOR3(0.0f, modelSize.y / 2.0f, 0.0f));

	//m_EnemySocket = new CEnemy_Socket;
	//m_EnemySocket->Init();

	//----------
	CMap* pMap = CMap::Create(2);

	SetMap(pMap);

	//�J�M�~�b�N
	CGimmicDoor* pdoor = CGimmicDoor::Create();

	// �h���[���̐ݒ�
	m_pDrone = CDrone::Create();
	m_pDrone->SetMotion("data/MOTION/motion001.txt");
	//// �h���[���̐ݒ�
	//m_pDrone = CDrone::Create();
	//m_pDrone->SetMotion("data/MOTION/motion001.txt");

	// �J�����̒Ǐ]�ݒ�(�ڕW : �v���C���[)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(m_pPlayer, 1.0);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);

	// �J�����̒Ǐ]�ݒ�(�ڕW : �v���C���[)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(m_pPlayer, 1.0);
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));

	// �}�E�X�J�[�\��������
	pInput->SetCursorErase(true);

	// �t�H�O�̐��l�ݒ�
	fogColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);		// �t�H�O�J���[
	fFogStartPos = 1.0f;								// �t�H�O�̊J�n�_
	fFogEndPos = 100000.0f;								// �t�H�O�̏I���_
	fFogDensity = 0.00001f;								// �t�H�O�̖��x

	// �t�H�O�̗L���ݒ�
	pDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);

	// �t�H�O�J���[�̐ݒ�
	pDevice->SetRenderState(D3DRS_FOGCOLOR, fogColor);

	// �t�H�O�͈̔͐ݒ�
	pDevice->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);
	pDevice->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&fFogStartPos));
	pDevice->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&fFogEndPos));

	// �t�H�O�̖��x�̐ݒ�
	pDevice->SetRenderState(D3DRS_FOGDENSITY, *(DWORD*)(&fFogDensity));

	m_bGame = true;

	//m_Socket = new CSocket;
	//m_Socket->Init();
#ifdef _DEBUG


	//// ���C�����
	//m_pLine = new CLine*[12];

	//for (int nCntLine = 0; nCntLine < 12; nCntLine++)
	//{
	//	m_pLine[nCntLine] = CLine::Create();

	//	m_pLine[nCntLine]->SetLine(D3DXVECTOR3(724.0f, 0.0f, 2700.0f), D3DXVECTOR3(0.0f,0.0f,0.0f), D3DXVECTOR3(100.0f, 0.0f, 0.0f), D3DXVECTOR3(-100.0f, 0.0f, 0.0f), D3DXCOLOR(1.0f,0.0f,0.0f,1.0f));

	//}


#endif // _DEBUG
	CApplication::GetInstance()->SetMoveModel(m_pPlayer);
	//std::thread th(CApplication::GetInstance()->Recv, 1);
	// �X���b�h��؂藣��
	//th.detach();
	m_worldTimer = 0;
	return S_OK;
}

//=============================================================================
// �I��
// Author : �������l
// �T�v : �e�N�X�`���̃|�C���^�ƒ��_�o�b�t�@�̉��
//=============================================================================
void CDebug_Scene::Uninit()
{// ���̓f�o�C�X�̎擾
	CInput *pInput = CInput::GetKey();

	// �f�o�C�X�̎擾
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// �T�E���h���̎擾
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// �T�E���h�I��
	pSound->StopSound();

	// �t�H�O�̗L���ݒ�
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	// �}�E�X�J�[�\�����o��
	pInput->SetCursorErase(true);

	// �J�����̒Ǐ]�ݒ�
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// �J�����̒Ǐ]�ݒ�
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// �ÓI�ϐ��̏�����
	if (m_pPlayer != nullptr)
	{
		m_pPlayer = nullptr;
	}
	// �ÓI�ϐ��̏�����
	if (m_pPlayer != nullptr)
	{
		m_pDrone = nullptr;
	}

	//if (m_EnemySocket != nullptr)
	//{
	//	m_EnemySocket->Uninit();
	//	delete m_EnemySocket;
	//}
	
	// �X�R�A�̉��
	Release();
}

//=============================================================================
// �X�V
// Author : �������l
// �T�v : �X�V���s��
//=============================================================================
void CDebug_Scene::Update()
{
	//m_EnemySocket->Update();
	// �J�����̒Ǐ]�ݒ�
	CCamera *pCamera = nullptr;

	// ���̓f�o�C�X�̎擾
	CInput *pInput = CInput::GetKey();

#ifdef _DEBUG

	// �J�����̒Ǐ]�ݒ�
	pCamera = CApplication::GetInstance()->GetCamera();

	if (pInput->Trigger(DIK_F10))
	{
		pCamera->Shake(60, 50.0f);
	}

	if (pInput->Press(DIK_LSHIFT))
	{
		pCamera->Zoom();
	}

	if (pInput->Trigger(DIK_RETURN))
	{
		m_bGame = false;
	}

	//m_Socket->GetPlayerList("0", m_pPlayer);
	

#endif // _DEBUG

	if (!m_bGame)
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}

	/*CModelData * Player = CApplication::GetInstance()->GetPlayerLog(&m_worldTimer);
	m_worldTimer++;

	std::ofstream outputfile("data/test2.txt", std::ios_base::app);
	outputfile << "X" << Player->GetCommu()->Player.m_pos.x << " Z" << Player->GetCommu()->Player.m_pos.z << " ID" << Player->GetCommu()->Player.m_Log << "\n";
	outputfile.close();*/
	
	//m_pPlayer->SetPos(Player->GetCommu()->Player.m_pos);
	//m_pPlayer->SetRotDest(Player->GetCommu()->Player.m_rot);
	//if (Player->GetCommu()->Player.m_motion != 6)
	//{
	//	m_pPlayer->GetMotion()->SetNumMotion(Player->GetCommu()->Player.m_motion);
	//}
}