//=============================================================================
//
// UIマネージャークラス(UImanager.cpp)
// Author : 唐�ｱ結斗
// 概要 : UIの生成
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include"UImanager.h"
#include "utility.h"
#include"polygon2D.h"
#include"polygon3D.h"
#include"Score.h"
#include"HackerUI.h"
#include"Timer.h"
#include "TargetUI.h"
#include "watch.h"
#include "UImessage.h"
#include "application.h"
#include "hacker.h"
#include "drone.h"

// ---------------------------------------------------------------------------- 
// 定数
//-----------------------------------------------------------------------------
const float CHackerUI::ITEM_NORMAL_SIZE = 80.0f;			// アイテム一覧のUIの選ばれていない時のサイズ
const float CHackerUI::ITEM_WIDTH = 40.0f;					// アイテムの設置間隔
const float CHackerUI::ITEM_DISTANCE_FROM_EDGE = 60.0f;		// 最初のアイテムの端からの位置
const int CHackerUI::NUMBER_0 = 27;							// テクスチャの0の番号
const float CHackerUI::BULLET_TIMER_SIZE = 25.f;					// タイマーの大きさ
const D3DXVECTOR3 CHackerUI::SCORE_SIZE = D3DXVECTOR3(60, 60, 0);	// スコア
const D3DXVECTOR3 CHackerUI::TIMER_SIZE = D3DXVECTOR3(60, 60, 0);	// タイマー


//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : ミニゲームを生成する
//=============================================================================
CHackerUI * CHackerUI::Create()
{
	// オブジェクトインスタンス
	CHackerUI *pUI = nullptr;

	// メモリの解放
	pUI = new CHackerUI;

	// メモリの確保ができなかった
	assert(pUI != nullptr);

	pUI->Init();

	// インスタンスを返す
	return pUI;
}
//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CHackerUI::CHackerUI()
{
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pChangeCameraUI[nCnt] = nullptr;
		m_pDroneCommand[nCnt] = nullptr;
	}
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CHackerUI::~CHackerUI()
{

}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 初期化
//=============================================================================
HRESULT CHackerUI::Init()
{
	int nMap = CApplication::GetInstance()->GetMap(), nMinimapIdx = 80;

	if (nMap == 1)
		nMinimapIdx = 122;
	else if (nMap == 2)
		nMinimapIdx = 139;

	//ミニマップUIの生成
	m_pMinimap = LoadUI(
		UI_TYPE_MINIMAP,
		D3DXVECTOR3(1180.0f, 100.0f, 0.0f),
		D3DXVECTOR3(200.0f, 200.0f, 0.0f),
		nMinimapIdx);

	//ハッキング用UI生成
	//m_pHackUI = LoadUI(
	//	UI_TYPE_UI,
	//	D3DXVECTOR3(1280.0f * 0.5f, 1080.0f * 0.5f, 100.0f),
	//	D3DXVECTOR3(100.0f, 100.0f, 100.0f),
	//	-1);

	////弾用タイマー生成
	//m_pShockTimer = (CTimer*)LoadUI(
	//	UI_TYPE_TIMER,
	//	D3DXVECTOR3(ITEM_DISTANCE_FROM_EDGE + ITEM_NORMAL_SIZE * 2 + ITEM_WIDTH * 2 + BULLET_TIMER_SIZE, 620.0f, 0.0f),
	//	D3DXVECTOR3(BULLET_TIMER_SIZE, BULLET_TIMER_SIZE, BULLET_TIMER_SIZE),
	//	-1);
	//m_pShockTimer->SetMode(1);
	//m_pShockTimer->SetDraw(false);

	//m_pHealTimer = (CTimer*)LoadUI(
	//	UI_TYPE_TIMER,
	//	D3DXVECTOR3(ITEM_DISTANCE_FROM_EDGE + ITEM_NORMAL_SIZE * 1 + ITEM_WIDTH * 1 + BULLET_TIMER_SIZE, 620.0f, 0.0f),
	//	D3DXVECTOR3(BULLET_TIMER_SIZE, BULLET_TIMER_SIZE, BULLET_TIMER_SIZE),
	//	-1);
	//m_pHealTimer->SetMode(1);
	//m_pHealTimer->SetDraw(false);

	////アイコン
	//m_pCameraIcon = LoadUI(
	//	UI_TYPE_UI,
	//	D3DXVECTOR3(1130.0f, 580.0f, 100.0f),
	//	D3DXVECTOR3(80.0f, 80.0f, 80.0f),
	//	-1);	

	//{
	//	D3DXVECTOR3 pos[2] = { { 1160.0f, 680.0f, 0.0f }, { 1220.0f, 680.0f, 0.0f} };

	//	//カメラの番号
	//	for (int i = 0; i < 2; i++)
	//	{
	//		m_pCameraNumber[i] = (CNumber*)LoadUI(
	//			UI_TYPE_NUMBER,
	//			pos[i],
	//			D3DXVECTOR3(60.0f, 60.0f, 60.0f),
	//			NUMBER_0 + 1 + ((MAX_CAMERA - 1)*i));
	//		m_pCameraNumber[i]->SetDigit(1);
	//	}
	//}
	//
	m_pCamNumberSlash = LoadUI(
		UI_TYPE_UI, 
		D3DXVECTOR3(1100.0f + 30.0f, 680.0f, 100.0f),
		D3DXVECTOR3(60.0f, 60.0f, 0.0f),
		125);

	m_Score = (CScore*)LoadUI(
		UI_TYPE_SCORE,
		D3DXVECTOR3(1280, 60.0f, 0.0f),
		SCORE_SIZE,
		-1);

	m_pTimer=(CTimer*)LoadUI(
		UI_TYPE_TIMER,
		D3DXVECTOR3(740.0f, 60.0f, 0.0f),
		TIMER_SIZE,
		-1);
	m_pTimer->SetMode(0);
	m_pTimer->SetTimer((60 * 60 * 15) + (60 * 10));//←分　→秒

	m_pTarget = Load3DUI(
		UI_TYPE_BILLBOARD,
		D3DXVECTOR3(0.0, 0.0f, 0.0f),
		D3DXVECTOR3(0,0,0),
		71);
	m_pTarget->SetColor(D3DXCOLOR(0, 0, 0, 0));

	{
		D3DXVECTOR3 cameraPos[2] = { { 1210.0f, 680.0f, 0.0f }, { 1050.0f, 680.0f, 0.0f } };
		int aTexIdx[2] = { 101, 100 };

			for (int nCnt = 0; nCnt < 2; nCnt++)
			{
				m_pChangeCameraUI[nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

				if (m_pChangeCameraUI[nCnt])
				{
					m_pChangeCameraUI[nCnt]->LoadTex(aTexIdx[nCnt]);
					m_pChangeCameraUI[nCnt]->SetSize(D3DXVECTOR3(80.0f, 80.0f, 0.0f));
					m_pChangeCameraUI[nCnt]->SetPos(cameraPos[nCnt]);
				}
			}

		/*m_pChangeCameraUI[0]->SetPos(D3DXVECTOR3(1225.0f, 680.0f, 0.0f));
		m_pChangeCameraUI[1]->SetPos(D3DXVECTOR3(1035.0f, 680.0f, 0.0f));*/
	}

	CWatch* pWatch = CWatch::Create();

	if (pWatch)
	{
		pWatch->SetTextureType(1);

		if (m_Score)
			m_Score->SetDigitSize(D3DXVECTOR3(15.0f, 30.0f, 0.0f));
		pWatch->AddObj(m_Score, D3DXVECTOR3(70.0f, -20.0f, 0.0f));
		if (m_pTimer)
			m_pTimer->SetDigitSize(D3DXVECTOR3(15.0f, 30.0f, 0.0f));
		pWatch->AddObj(m_pTimer, D3DXVECTOR3(0.0f, 20.0f, 0.0f));
	}

	{
		int nTxt[2] = { 37, 46 };

		for (int nCnt = 0; nCnt < 2; nCnt++)
		{
			m_pDroneCommand[nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

			if (m_pDroneCommand[nCnt])
			{
				m_pDroneCommand[nCnt]->SetPos(D3DXVECTOR3(1130.0f - 85.0f * nCnt, 600.0f, 100.0f));
				m_pDroneCommand[nCnt]->SetSize(D3DXVECTOR3(80.0f, 80.0f, 0.0f));
				m_pDroneCommand[nCnt]->LoadTex(nTxt[nCnt]);
				m_pDroneCommand[nCnt]->SetDraw(false);
			}
		}
	}

	return S_OK;
}

//=============================================================================
// 終了
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CHackerUI::Uninit()
{
	//2D UIの破棄
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pChangeCameraUI[nCnt])
			m_pChangeCameraUI[nCnt] = nullptr;

		if (m_pDroneCommand[nCnt])
			m_pDroneCommand[nCnt] = nullptr;
	}
}
//=============================================================================
// 更新
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CHackerUI::Update()
{
	CDrone* pDrone = CHacker::GetDrone();

	if (pDrone)
	{

	}
}
//=============================================================================
// 描画
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CHackerUI::Draw()
{
}

//=============================================================================
// 全部透明にする
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CHackerUI::AllClear()
{
	return;
	//ハッキング用UI生成
	//m_pHackUI->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	//アイコン
	m_pCameraIcon->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	//カメラの番号
	for (int i = 0; i < 2; i++)
	{
		m_pCameraNumber[i]->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
	}

	m_pCameraNumber[2]->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	//弾用タイマー生成
	//m_pShockTimer->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	//m_pHealTimer->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	m_pTimer->SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
}


//=============================================================================
// 透明解除
// Author : 有田明玄
// 概要 : ミニゲームのハッキング
//=============================================================================
void CHackerUI::AllClearCancel()
{
	return;
	//ハッキング用UI生成
	//m_pHackUI->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//アイコン
	m_pCameraIcon->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//カメラの番号
	for (int i = 0; i < 2; i++)
	{
		m_pCameraNumber[i]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}

	m_pCameraNumber[2]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//弾用タイマー生成
	//m_pShockTimer->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	//m_pHealTimer->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_pTimer->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
}

//スコアの加算処理
void CHackerUI::AddScore(const int nScore)
{
	if (m_Score)
	{
		m_Score->AddScore(nScore);
	}
}
//スコアの加算処理
void CHackerUI::SetScore(const int nScore)
{
	if (m_Score)
	{
		m_Score->SetScore(nScore);
	}
}

//ドローン操作のUI描画フラグのセッター
void CHackerUI::SetDroneCommandUi(const bool bDraw)
{
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pDroneCommand[nCnt])
		{
			m_pDroneCommand[nCnt]->SetDraw(bDraw);
		}
	}
}
