//=============================================================================
//
// 2D�i���o�[(number.h)
// Author : �������l
// �T�v : 2D�i���o�[�������s��
//
//=============================================================================
#ifndef _NUMBER_H_		// ���̃}�N����`������ĂȂ�������
#define _NUMBER_H_		// ��d�C���N���[�h�h�~�̃}�N����`

//*****************************************************************************
// �C���N���[�h
//*****************************************************************************
#include "polygon2D.h"

//=============================================================================
// 2D�i���o�[�N���X
// Author : �������l
// �T�v : 2D�i���o�[�������s���N���X
//=============================================================================
class CNumber : public CPolygon2D
{
private:
	static const int NUMBER_MAX = 5;

public:
	//--------------------------------------------------------------------
	// �ÓI�����o�֐�
	//--------------------------------------------------------------------
	// 2D�i���o�[�̐���
	static CNumber *Create(int nPriority = CObject::PRIORITY_LEVEL4);

	//--------------------------------------------------------------------
	// �R���X�g���N�^�ƃf�X�g���N�^
	//--------------------------------------------------------------------
	explicit CNumber(int nPriority = CObject::PRIORITY_LEVEL4);
	~CNumber() override;

	//--------------------------------------------------------------------
	// �����o�֐�
	//--------------------------------------------------------------------
	HRESULT Init() override;				// ������
	void Uninit() override;					// �I��
	void Update() override;					// �X�V
	void Draw() override;					// �`��
	void SetNumber(int nNumber);			// �i���o�[�̐ݒ�
	int GetNumber() { return m_nNumber; };	// �i���o�[�̎擾
	void SetDigit(int nNumber);
	void SetCol(D3DXCOLOR col);				// �F�ݒ�
	void SetPos(const D3DXVECTOR3& pos) override;
	void SetSize(D3DXVECTOR3 siz);
	void SetDigitSize(const D3DXVECTOR3 size);						//���̃T�C�Y�̃Z�b�^�[
	const D3DXVECTOR3 GetDigitSize() { return m_digitSize; }		//���̃T�C�Y�̃Q�b�^�[
	CPolygon2D* GetPolygon(int number) {return  m_pNumber[number]; };
	void SetDraw(const bool bDraw) override;	// �`��t���O�̃Z�b�^�[

private:

	static const D3DXVECTOR3	DEFAULT_DIGIT_SIZE;		//�f�B�t�H���g�̌��̃T�C�Y

	//--------------------------------------------------------------------
	// �����o�ϐ�
	//--------------------------------------------------------------------
	int m_nNumber;		// ���l
	CPolygon2D* m_pNumber[NUMBER_MAX];	//5���܂Őݒ�\
	D3DXVECTOR3 m_digitSize;			//���̃T�C�Y
	int m_nDigit;	//����
	int m_nMaxDigit;
};

#endif
