//=============================================================================
//
// agentTutorial.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "agentTutorial.h"
#include "application.h"
#include "camera.h"
#include "input.h"
#include "renderer.h"
#include "sound.h"
#include "task.h"
#include "map.h"
#include "sphere.h"
#include "calculation.h"
#include "agent.h"
#include "drone.h"
#include "stencil_canvas.h"
#include "walkingEnemy.h"
#include "agentUi.h"
#include "debug_proc.h"


namespace
{
	// フォグの数値設定
	const D3DXCOLOR fogColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f);		// フォグカラー
	const float fogStartPos = 600.0f;									// フォグの開始点
	const float fogEndPos = 2000.0f;									// フォグの終了点
	const float fogDensity = 0.00001f;									// フォグの密度

	const int	nMapIdx = 0;											// チュートリアルマップのインデックス
}



//コンストラクタ
CAgentTutorial::CAgentTutorial()
{

}

//デストラクタ
CAgentTutorial::~CAgentTutorial()
{

}

//初期化
HRESULT CAgentTutorial::Init()
{
	CApplication::GetInstance()->SetConnect(true);
	CApplication::GetInstance()->SetEnemyConnect(true);
	{
		bOpenflg = false;
	}

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM001);

	m_pTask = CTask::Create();

	int Map = CApplication::GetInstance()->GetMap();

	CMap* pMap = CMap::Create(nMapIdx);

	SetMap(pMap);

	//SetMap(pMap);

	CApplication::GetInstance()->SetMap(nMapIdx);

	// 重力の値を設定
	CCalculation::SetGravity(0.2f);

	// スカイボックスの設定
	CSphere *pSphere = CSphere::Create();
	pSphere->SetRot(D3DXVECTOR3(D3DX_PI, 0.0f, 0.0f));
	pSphere->SetSize(D3DXVECTOR3(10.0f, 0, 10.0f));
	pSphere->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
	pSphere->SetRadius(2500.0f);
	pSphere->SetSphereRange(D3DXVECTOR2(D3DX_PI * 2.0f, D3DX_PI * -0.5f));
	pSphere->LoadTex(1);

	// プレイヤーの設定
	m_pPlayer = CAgent::Create();
	m_pPlayer->SetMotion("data/MOTION/motion.txt");
	//m_pPlayer->SetRenderMode(CModel3D::ERenderMode::Render_Highlight);

	// ドローン設定
	m_pDrone = CPlayer::Create();
	m_pDrone->SetMotion("data/MOTION/motion002_drone.txt");

	//m_EnemySocket = new CEnemy_Socket;
	//m_EnemySocket->Init();

	// カメラの追従設定(目標 : プレイヤー)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	// カメラの追従設定(目標 : プレイヤー)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	CStencilCanvas* pStencilCanvas = CStencilCanvas::Create();
	pStencilCanvas->SetCol(D3DXCOLOR(0.f, 0.f, 0.f, 1.f));

	SetStencilCanvas(pStencilCanvas);

	// マウスカーソルを消す
	pInput->SetCursorErase(true);

	// フォグの設定
	SetFog(pDevice, fogStartPos, fogEndPos, fogDensity, fogColor);

	m_bGame = true;

	m_pUiManager = CAgentUI::Create();

	if (m_pTask)
	{//nullチェック
		m_pTask->CreateUI();		//UIの生成
	}
	
	CApplication::GetInstance()->SetMoveModel(m_pDrone);
	std::thread th(CApplication::Recv, 1);
	// スレッドを切り離す
	th.detach();

	return S_OK;
}

//終了
void CAgentTutorial::Uninit()
{
	CGame::Uninit();
}

//更新
void CAgentTutorial::Update()
{
#ifdef _DEBUG
	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	// デバック表示
	CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル| F7 デバック表示削除\n");

	if (pInput->Trigger(DIK_F1))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_AGENT);
	}
	if (pInput->Trigger(DIK_F3))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_HACKER);
	}
	if (pInput->Trigger(DIK_F4))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TITLE);
	}
#endif // _DEBUG

}