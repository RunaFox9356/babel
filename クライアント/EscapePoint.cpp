
//=============================================================================
// インクルード
//=============================================================================
#include "EscapePoint.h"
#include <assert.h>
#include"collision_rectangle3D.h"
#include"model_obj.h"
#include"model3D.h"
#include "game.h"
#include "application.h"
#include "model_data.h"
#include "emptyObj.h"
#include "input.h"
#include "UImessage.h"
#include "cylinder.h"
#include "player.h"
#include "hacker.h"
#include "agent.h"
#include "carAnimation.h"
#include "doorAnimation.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			CEscapePoint::DEFAULT_CYLINDER_MODEL = 59;									//ディフォルトのシリンダーのモデルインデックス
const int			CEscapePoint::DEFAULT_CYLINDER_TEXTURE = 55;								//ディフォルトのシリンダーのテクスチャインデックス
const float			CEscapePoint::DEFAULT_CYLINDER_RADIUS = 300.0f;								//シリンダーの半径
const D3DXVECTOR3	CEscapePoint::DEFAULT_CYLINDER_SIZE = { 100.0f, 0.0f, 300.0f };				//シリンダーのサイズ
const D3DXCOLOR		CEscapePoint::DEFAULT_CYLINDER_COLOR = { 0.2f, 1.0f, 0.2f, 1.0f };			//シリンダーの色
const D3DXVECTOR2	CEscapePoint::DEFAULT_TEXTURE_ANIMATION_SPEED = { 0.005f, 0.0f};			//ディフォルトのシリンダーのテクスチャアニメーションの速度
const D3DXVECTOR3	CEscapePoint::DEFAULT_AREA_HITBOX_POS = { 0.0f, 125.0f, 0.0f };				//エスケープできる範囲の相対位置
const D3DXVECTOR3	CEscapePoint::DEFAULT_AREA_HITBOX_SIZE = { 300.0f, 250.0f, 300.0f };		//エスケープできる範囲のサイズ


namespace HEscapePoint
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
};



CEscapePoint * CEscapePoint::Create(D3DXVECTOR3 pos)
{
	// オブジェクトインスタンス
	CEscapePoint *pEscapeObj = nullptr;

	// メモリの解放
	pEscapeObj = new CEscapePoint;

	// メモリの確保ができなかった
	assert(pEscapeObj != nullptr);

	// 数値の初期化
	pEscapeObj->Init();

	//位置の設定
	pEscapeObj->SetPos(pos);

	// インスタンスを返す
	return pEscapeObj;
}

CEscapePoint::CEscapePoint() : m_pUiMessage(nullptr),
m_pPlayer(nullptr),
m_pEndAnim(nullptr)
{
}

CEscapePoint::~CEscapePoint()
{
}

HRESULT CEscapePoint::Init()
{
	{
		m_model = CModelObj::Create();
		assert(m_model != nullptr);
		m_model->SetType(DEFAULT_CYLINDER_MODEL);
		m_model->SetObjType(CObject::OBJETYPE_NON_SOLID_GIMMICK);

		// スカイボックスの設定
		m_pCylinder = CCylinder::Create(CSuper::PRIORITY_LEVEL3);

		m_pCylinder->SetSize(DEFAULT_CYLINDER_SIZE);
		m_pCylinder->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
		m_pCylinder->SetRadius(DEFAULT_CYLINDER_RADIUS);
		m_pCylinder->SetCol(DEFAULT_CYLINDER_COLOR);
		m_pCylinder->SetScrollTex(DEFAULT_TEXTURE_ANIMATION_SPEED, true);
		m_pCylinder->LoadTex(DEFAULT_CYLINDER_TEXTURE);

		SetAnimation(false);

		Goalflg = false;
		pCollisionEscape = CCollision_Rectangle3D::Create();
		pCollisionEscape->SetPos(DEFAULT_AREA_HITBOX_POS);
		pCollisionEscape->SetSize(DEFAULT_AREA_HITBOX_SIZE);
	
	}
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));
	SetAnimation(false);
	pCollisionEscape->SetParent(m_model);

	return S_OK;
}

void CEscapePoint::Update()
{
	if (!m_pEndAnim)
	{
		bool bHit = false;
		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

		if ((mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME) && pCollisionEscape->Collision(CObject::OBJETYPE_PLAYER, false))
		{//脱出処理

			bHit = true;
			LookForPlayer();

			CInput *pInput = CInput::GetKey();

			if (!m_pUiMessage)
				CreateUiMessage("Escape", HEscapePoint::E_BUTTON_TEXTURE_IDX);

			if (pInput->Trigger(DIK_E))
			{

				/*Sleep(100);
				SetAnimation(true);

				CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);*/

				SetAnimation(true);
				int nMap = CApplication::GetInstance()->GetMap();
				D3DXVECTOR3 pos = GetPos();

				DestroyUiMessage();					

				if (nMap == 0)
				{
					m_pEndAnim = CCarAnimation::Create(pos);
				}
				else if (nMap == 1)
				{
					m_pEndAnim = CDoorAnimation::Create(pos);

					m_pCylinder->SetDraw(false);
				}
				else
				{
					m_pEndAnim = CCarAnimation::Create(pos);
				}

				//ボタンを押したら脱出地点
				//TODO:マップ選択画面作る
				//if (CApplication::GetInstance()->GetMode() == CApplication::MODE_GAME || CApplication::GetInstance()->GetMode() == CApplication::MODE_AGENT)
				//	//CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
				//else if (CApplication::GetInstance()->GetMode() == CApplication::MODE_AGENT_TUTORIAL)
				//	//CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_TITLE);
			}
		}
		else if (m_pUiMessage)
			DestroyUiMessage();

		if (m_pPlayer && !bHit)
		{
			m_pPlayer->SetNearEscape(nullptr);
			m_pPlayer = nullptr;
		}
		if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_log >= 0 && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.mId >= 0 && CApplication::GetInstance()->GetMode() == CApplication::MODE_HACKER)
		{
			bool bUse = CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse;
			//オンラインのデータで起動したらタイトルに戻る
			if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
			{
				SetAnimation(true);
				//CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);

				int nMap = CApplication::GetInstance()->GetMap();
				D3DXVECTOR3 pos = GetPos();

				if (nMap == 0)
				{
					m_pEndAnim = CCarAnimation::Create(pos);
				}
				else if (nMap == 1)
				{
					m_pEndAnim = CDoorAnimation::Create(pos);

					m_pCylinder->SetDraw(false);
				}
				else
				{
					m_pEndAnim = CCarAnimation::Create(pos);
				}
			}
		}
	}
	else
	{
		if (m_pEndAnim->GetEnd())
		{
			m_pEndAnim->Uninit();
			m_pEndAnim = nullptr;

			SetAnimation(true);
			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
		}
	}
}

//エフェクトのインデックスの設定
void CEscapePoint::SetRenderMode(int mode)
{
	if (m_model)
		m_model->SetRenderMode(mode);
}

//プレイヤーまでの距離を判定する
void CEscapePoint::LookForPlayer()
{
	if (m_pPlayer)
		return;

	CPlayer* pPlayer = nullptr;			//プレイヤーへのポインタ

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT_TUTORIAL)
		pPlayer = CGame::GetPlayer();			//プレイヤーの位置の取得
	else if (mode == CApplication::MODE_HACKER)
		pPlayer = CHacker::GetPlayer();			//プレイヤーの位置の取得

	m_pPlayer = pPlayer;
	m_pPlayer->SetNearEscape(this);
}

//UIメッセージの生成処理
void CEscapePoint::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage();

	if (!m_pUiMessage)
	{
		m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 260.0f, 0.0f), D3DXVECTOR3(85.0f, 85.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
	}
}

//UIメッセージの破棄処理
void CEscapePoint::DestroyUiMessage()
{
	//UIメッセージの破棄
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}

void CEscapePoint::Uninit()
{
	if (pCollisionEscape)
	{
		pCollisionEscape->Uninit();
		pCollisionEscape = nullptr;
	}

	if (m_pCylinder)
	{
		m_pCylinder->Uninit();
		m_pCylinder = nullptr;
	}

	//UIメッセージの破棄
	DestroyUiMessage();
}

