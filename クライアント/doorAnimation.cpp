//=============================================================================
//
// carAnimation.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "doorAnimation.h"
#include "camera.h"
#include "game.h"
#include "hacker.h"
#include "application.h"
#include "agent.h"
#include "player.h"
#include "drone.h"
#include "motion.h"



//=============================================================================
// インクルード
//=============================================================================
const D3DXVECTOR3	CDoorAnimation::DEFAULT_SPEED = { -10.0f, 0.0f, 0.0f };		//ディフォルトの速度



//コンストラクタ
CDoorAnimation::CDoorAnimation() : m_speed(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nDelay(0),
m_animState((EAnimState)0)
{

}

//デストラクタ
CDoorAnimation::~CDoorAnimation()
{

}

//初期化
HRESULT CDoorAnimation::Init()
{
	//親クラスの初期化処理
	if (FAILED(CEndAnimation::Init()))
		return E_FAIL;

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
	CPlayer* pDrone = nullptr;

	if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT)
	{
		pDrone = CGame::GetDrone();

		if (pDrone)
		{
			CDrone* pDr = dynamic_cast<CDrone*>(pDrone);

			if (pDr)
				pDr->SetControl(false);
		}

		if (CGame::GetPlayer())
		{
			CGame::GetPlayer()->SetStopState(true);
			CGame::GetPlayer()->SetInvulnerability(1800);
		}

	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pDrone = CHacker::GetDrone();
		CHacker::GetDrone()->SetControl(false);
	}

	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (pDrone && pCamera)
	{
		pCamera->SetFollowTarget(false);
		pCamera->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 60.0f, -200.0f));
		pCamera->BlockCamera(true);

		pCamera->SetPosR(pDrone->GetPos());
		pCamera->SetPosV(pDrone->GetPos() + D3DXVECTOR3(0.0f, 60.0f, -200.0f));
	}

	m_animState = STATE_DRONE_OUT;


	return S_OK;
}

//終了
void CDoorAnimation::Uninit()
{
	CCamera* pCamera = CApplication::GetInstance()->GetCamera();

	if (pCamera)
	{
		pCamera->BlockCamera(false);
	}

	//親クラスの終了処理
	CEndAnimation::Uninit();
}

//更新
void CDoorAnimation::Update()
{
	UpdateState();
}




CDoorAnimation* CDoorAnimation::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	CDoorAnimation* pAnim = new CDoorAnimation;		//インスタンスを生成する

	if (FAILED(pAnim->Init()))
	{//初期化処理
		return nullptr;
	}

	pAnim->CalcSpeed();				//速度の計算処理

	return pAnim;					//生成したインスタンスを返
}





//速度の計算処理
void CDoorAnimation::CalcSpeed()
{
	D3DXVECTOR3 rot = GetRot();
	D3DXMATRIX mtxOut, mtxRot, mtxTrans;

	D3DXMatrixIdentity(&mtxOut);

	D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
	D3DXMatrixMultiply(&mtxOut, &mtxOut, &mtxRot);

	D3DXVec3TransformCoord(&m_speed, &DEFAULT_SPEED, &mtxRot);
}

//状態の更新処理
void CDoorAnimation::UpdateState()
{
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	CPlayer* pPlayer = nullptr;
	CPlayer* pDrone = nullptr;

	if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT)
	{
		pPlayer = CGame::GetPlayer();
		pDrone = CGame::GetDrone();
	}
	else if (mode == CApplication::MODE_HACKER)
	{
		pPlayer = CHacker::GetPlayer();
		pDrone = CHacker::GetDrone();
	}

	if (!pPlayer || !pDrone)
		return;

	switch (m_animState)
	{
	case CDoorAnimation::STATE_DRONE_OUT:

	{
		m_nDelay++;

		if (m_nDelay > 30)
		{
			D3DXVECTOR3 dronePos = pDrone->GetPos();

			dronePos.y += 5.0f;

			pDrone->SetPos(dronePos);

			if (dronePos.y >= 300.0f)
			{
				m_animState = STATE_DRONE_IN;
				m_nDelay = 0;

				CCamera* pCamera = CApplication::GetInstance()->GetCamera();

				if (pCamera)
				{
					D3DXVECTOR3 pos = pPlayer->GetPos();
					pCamera->SetTargetPosR(pPlayer);
					pCamera->BlockCamera(true);
					pCamera->SetPosV(pos + D3DXVECTOR3(400.0f, 100.0f, 20.0f));
					pCamera->SetPosR(pos);

					pDrone->SetPos(pos + D3DXVECTOR3(-m_speed.x * 5.0f, 1000.0f, -m_speed.z * 5.0f));
				}
			}
		}
	}

		break;

	case CDoorAnimation::STATE_DRONE_IN:

	{
		//ドローンの位置を取得し、更新する
		D3DXVECTOR3 dronePos = pDrone->GetPos(), pos = pPlayer->GetPos();
		dronePos.y += -10.0f;

		//ターゲットに着いたかどうかの判定
		D3DXVECTOR3 dir = -m_speed;
		D3DXVec3Normalize(&dir, &dir);
		D3DXVECTOR3 target = pos + D3DXVECTOR3(-m_speed.x * 5.0f, 100.0f, -m_speed.z * 5.0f), dist = target - dronePos;

		if (D3DXVec3Length(&dist) < 5.0f || D3DXVec3Dot(&dir, &dist) < 0.0f)
		{
			m_animState = STATE_ESCAPE;

			CAgent* pAgent = dynamic_cast<CAgent*>(pPlayer);

			if (pAgent)
			{
				pAgent->GetMotion()->SetNumMotion(CAgent::ACTION_STATE::DASH_ACTION);
			}
		}

		pDrone->SetPos(dronePos);
	}

		break;

	case CDoorAnimation::STATE_ESCAPE:

	{
		m_nDelay++;

		if (m_nDelay > 30)
		{
			pPlayer->SetPos(pPlayer->GetPos() + m_speed);
			pDrone->SetPos(pDrone->GetPos() + m_speed);
		}		

		if (m_nDelay >= 180)
		{
			SetEnd(true);
		}
	}

		break;

	default:
		break;
	}
}
