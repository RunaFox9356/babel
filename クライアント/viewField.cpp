//=============================================================================
//
// viewField.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "viewField.h"
#include "renderer.h"
#include "application.h"
#include "camera.h"
#include "line.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const float CViewField::DEFAULT_VIEW_DISTANCE = 500.0f;				//ディフォルトの見える距離
const float CViewField::DEFAULT_VIEW_ANGLE = D3DX_PI * 0.125f;		//ディフォルトの視野角
const float CViewField::MIN_VIEW_ANGLE = D3DX_PI * 0.05f;			//最小の視野角
const float CViewField::MAX_VIEW_ANGLE = D3DX_PI * 0.3f;			//最大の視野角
const float CViewField::DEFAULT_SENSE_RADIUS = 300.0f;				//プレイヤーに気付くディフォルト範囲の半径


namespace
{
	D3DXCOLOR NORMAL_LINE_COLOR   = D3DXCOLOR(0.0f, 1.0f, 1.0f, 1.0f);
	D3DXCOLOR ALLERTED_LINE_COLOR = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
};


//コンストラクタ
CViewField::CViewField() : m_posV(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_rot(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_fViewAngle(0.0f),
m_pParent(nullptr)
{
#ifdef _DEBUG

	for (int nCnt = 0; nCnt < LINE_NUMBER; nCnt++)
		m_pLine[nCnt] = nullptr;				//ラインへのポインタ

	m_lineCol = D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);							//ラインの色
#endif // _DEBUG
}

//デストラクタ
CViewField::~CViewField()
{

}

// 初期化
HRESULT CViewField::Init()
{
	m_fViewAngle = DEFAULT_VIEW_ANGLE;

#ifdef _DEBUG

	for (int nCnt = 0; nCnt < LINE_NUMBER; nCnt++)
	{
		m_pLine[nCnt] = CLine::Create();		//ラインへのポインタ
	}

	m_lineCol = NORMAL_LINE_COLOR;					//ラインの色
#endif // _DEBUG

	SetViewAngle(DEFAULT_VIEW_ANGLE);
	SetViewDistance(DEFAULT_VIEW_DISTANCE);

	return S_OK;
}

// 終了
void CViewField::Uninit()
{
	Release();

#ifdef _DEBUG
	for (int nCnt = 0; nCnt < LINE_NUMBER; nCnt++)
	{
		if (m_pLine[nCnt])
		{
			m_pLine[nCnt]->Uninit();
			m_pLine[nCnt] = nullptr;
		}
	}
#endif // _DEBUG
}

// 更新
void CViewField::Update()
{
#ifdef _DEBUG
	SetLine();
#endif
}

// 描画
void CViewField::Draw()
{

}

//視野角の設定
void CViewField::SetViewAngle(const float fAngle)
{
	m_fViewAngle = fAngle;

	CalcPlaneEdgesLength();
}

//見える距離の設定
void CViewField::SetViewDistance(const float fDistance)
{
	m_fViewDistance = fDistance;

	CalcPlaneEdgesLength();
}

//ポイントが見えるかどうかの判定
bool CViewField::IsPontInView(D3DXVECTOR3 point)
{
	bool bViewable = true;
	D3DXMATRIX mtxInverse;
	D3DXVECTOR3 unitX = D3DXVECTOR3(1.0f, 0.0f, 0.0f), unitY = D3DXVECTOR3(0.0f, 1.0f, 0.0f), unitZ = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	CreateInverseMatrix(&mtxInverse);		//逆行列を作る
	D3DXVec3TransformCoord(&point, &point, &mtxInverse);		//グローバル座標をカメラスペース座標に変換する

	D3DXVECTOR3 pos = point;

	float fDist = 0.0f/*sqrtf((pos.x * pos.x) + (pos.y * pos.y) + (pos.z * pos.z))*/;		//ポイントまでの距離を計算する

	////見えなくてもオブジェクトに気付く範囲内だったら、trueを返す
	//if (fDist < DEFAULT_SENSE_RADIUS)
	//	return true;

	fDist = D3DXVec3Dot(&pos, &unitZ);		//d'

	//ポイントが遠すぎる又は後ろだったら、falseを返す
	if (fDist > m_fViewDistance || fDist <= 0.0f)
	{
#ifdef _DEBUG
		m_lineCol = NORMAL_LINE_COLOR;
#endif
		return false;
	}

	pos.x *= (1 / fDist);
	pos.y *= (1 / fDist);
	pos.z *= (1 / fDist);

	D3DXVECTOR3 v = m_fViewDistance * (pos - unitZ);
	float fLimit = m_fProjectionPlaneEdge;

	float fPosX = D3DXVec3Dot(&v, &unitX);

	if (fPosX < -fLimit || fPosX > fLimit)
	{
#ifdef _DEBUG
		m_lineCol = NORMAL_LINE_COLOR;
#endif
		return false;
	}

	float fPosY = D3DXVec3Dot(&v, &unitY);

	if (fPosY < -fLimit || fPosY > fLimit)
	{
#ifdef _DEBUG
		m_lineCol = NORMAL_LINE_COLOR;
#endif
		return false;
	}

#ifdef _DEBUG
	for (int nCnt = 0; nCnt < LINE_NUMBER; nCnt++)
	{
		if (m_pLine[nCnt])
		{
			m_lineCol = ALLERTED_LINE_COLOR;
			m_pLine[nCnt]->SetCol(m_lineCol);
		}
	}
#endif

	return bViewable;
}

//ポイントが前にあるかどうかの判定
bool CViewField::IsPointInFront(D3DXVECTOR3 point)
{
	D3DXMATRIX mtxInverse;
	D3DXVECTOR3 unitZ = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	CreateInverseMatrix(&mtxInverse);		//逆行列を作る
	D3DXVec3TransformCoord(&point, &point, &mtxInverse);		//グローバル座標をカメラスペース座標に変換する

	D3DXVECTOR3 pos = point;

	float fDist = sqrtf((pos.x * pos.x) + (pos.y * pos.y) + (pos.z * pos.z));		//ポイントまでの距離を計算する

	float fDot = D3DXVec3Dot(&pos, &unitZ);		//d'

	//ポイントが遠すぎる又は後ろだったら、falseを返す
	if (fDot <= 0.0f || fDist > m_fViewDistance)
	{
#ifdef _DEBUG
		m_lineCol = NORMAL_LINE_COLOR;
#endif
		return false;
	}

	//m_lineCol = ALLERTED_LINE_COLOR;

	return true;
}

//生成
CViewField * CViewField::Create()
{
	// オブジェクトインスタンス
	CViewField *pObj = new CViewField;

	// メモリの確保ができなかった
	assert(pObj != nullptr);

	// 数値の初期化
	pObj->Init();

	// インスタンスを返す
	return pObj;
}

//生成
CViewField * CViewField::Create(CObject * pParent)
{
	// オブジェクトインスタンス
	CViewField *pObj = new CViewField;

	// メモリの確保ができなかった
	assert(pObj != nullptr);

	// 数値の初期化
	pObj->Init();

	pObj->SetParent(pParent);		//親の設定

	// インスタンスを返す
	return pObj;
}

//ロジェクション平面の辺の長さを計算する
void CViewField::CalcPlaneEdgesLength()
{
	if (m_fViewAngle < MIN_VIEW_ANGLE)
	{
		m_fViewAngle = MIN_VIEW_ANGLE;
	}
	else if (m_fViewAngle > MAX_VIEW_ANGLE)
	{
		m_fViewAngle = MAX_VIEW_ANGLE;
	}

	m_fProjectionPlaneEdge = m_fViewDistance * tanf(m_fViewAngle);

#ifdef _DEBUG
	SetLine();
#endif
}

//逆行列を作る
void CViewField::CreateInverseMatrix(D3DXMATRIX* mtxInv)
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXMATRIX mtxWorld, mtxRot, mtxTrans;

	if (m_pParent)
	{
		pos = m_pParent->GetPos() + m_posV;
		rot = m_pParent->GetRot() + m_rot;
	}

	D3DXMatrixIdentity(&mtxWorld);

	D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
	D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxRot);

	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxTrans);

	D3DXMatrixInverse(mtxInv, nullptr, &mtxWorld);
}

#ifdef _DEBUG
void CViewField::SetLine()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pParent)
	{
		pos = m_pParent->GetPos() + m_posV;
		rot = m_pParent->GetRot() + m_rot;
	}

	D3DXVECTOR3 start[LINE_NUMBER], end[LINE_NUMBER];

	for (int nCnt = 0; nCnt < LINE_NUMBER; nCnt++)
	{
		if (!m_pLine[nCnt])
			return;
	}

	m_pLine[0]->SetLine(pos, rot, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 
		D3DXVECTOR3(m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[1]->SetLine(pos, rot, D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(-m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[2]->SetLine(pos, rot, D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[3]->SetLine(pos, rot, D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(-m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);


	m_pLine[4]->SetLine(pos, rot, D3DXVECTOR3(m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance),
		D3DXVECTOR3(-m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[5]->SetLine(pos, rot, D3DXVECTOR3(-m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance),
		D3DXVECTOR3(-m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[6]->SetLine(pos, rot, D3DXVECTOR3(-m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance),
		D3DXVECTOR3(m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);

	m_pLine[7]->SetLine(pos, rot, D3DXVECTOR3(m_fProjectionPlaneEdge, -m_fProjectionPlaneEdge, m_fViewDistance),
		D3DXVECTOR3(m_fProjectionPlaneEdge, m_fProjectionPlaneEdge, m_fViewDistance), m_lineCol);
}
#endif // _DEBUG


