//=============================================================================
//
// 運んで開けるドア.cpp
// Author: hamada
//
//=============================================================================

//=============================================================================
// インクルードファイル
//=============================================================================
#include <assert.h>
#include "gimmick.h"
#include "move.h"
#include "agent.h"
#include"carry_player.h"
#include"collision_rectangle3D.h"
#include"model_obj.h"
#include"model3D.h"
#include "game.h"
#include "application.h"
#include "model_data.h"
#include "input.h"
#include "itemObj.h"

CGimmicCarry * CGimmicCarry::Create()
{
	// オブジェクトインスタンス
	CGimmicCarry *pDoorObj = nullptr;

	// メモリの解放
	pDoorObj = new CGimmicCarry;

	// メモリの確保ができなかった
	assert(pDoorObj != nullptr);

	// 数値の初期化
	pDoorObj->Init();

	// インスタンスを返す
	return pDoorObj;
}

CGimmicCarry::CGimmicCarry() : m_pMove(nullptr),
m_door(nullptr),
m_key(nullptr),
m_hitModle(nullptr),
pCollisionDoorGimmick(nullptr),
pCollisionDoor(nullptr),
pCollisionkey(nullptr)
{
}

CGimmicCarry::~CGimmicCarry()
{
}

//=============================================================================
// 初期化
// Author : 浜田琉雅
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CGimmicCarry::Init()
{
	m_Sin = 0;
	m_door = CModelObj::Create();
	assert(m_door != nullptr);
	m_door->SetType(49);
	m_door->SetPos(D3DXVECTOR3(300, 0.f, 100.0f));
	m_door->SetObjType(CObject::OBJETYPE_GIMMICK);
	SetPosDefault(D3DXVECTOR3(300, 0.f, 100.0f));


	D3DXVECTOR3 modelDoorGimmick = m_door->GetModel()->GetMyMaterial().size;
	pCollisionDoorGimmick = m_door->GetCollision();
	pCollisionDoorGimmick->SetSize(modelDoorGimmick);
	pCollisionDoorGimmick->SetPos(D3DXVECTOR3(0.0f, modelDoorGimmick.y / 2.0f, 0.0f));

	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_move.x = 5.0f;

	m_hitModle = CModelObj::Create();
	m_IsLeft = true;

	//ギミックにはIDを絶対つけてね
	SetId(CApplication::GetInstance()->GetSceneMode()->PoshGimmick(this));

	pCollisionDoor = m_hitModle->GetCollision();

	D3DXVECTOR3 pos = D3DXVECTOR3(0, 50.f, 0.0f);
	D3DXVECTOR3 size = D3DXVECTOR3(100, 100.f, 100.0f);
	if (m_hitModle)
	{
		m_hitModle->SetPos(m_door->GetPos());
		m_hitModle->SetObjType(CObject::EObjectType::OBJETYPE_NON_SOLID_GIMMICK);
	}

	if (pCollisionDoor)
	{//nullチェック
		pCollisionDoor->SetPos(pos);				//コリジョンのサイズの位置の設定
		pCollisionDoor->SetSize(size);				//コリジョンのサイズの設定
	}

	//鍵の設定
	m_key = CItemObj::Create();
	m_key->SetType(55);
	m_key->SetPos(D3DXVECTOR3(10, 0.f, 100.0f));
	m_key->SetObjType(CObject::OBJTYPE_GIMMICKKEY);

	pCollisionkey = m_key->GetCollision();

	if (pCollisionkey)
	{//nullチェック
		pCollisionkey->SetPos(pos);				//コリジョンのサイズの位置の設定
		pCollisionkey->SetSize(size);				//コリジョンのサイズの設定
	}
	m_flg = false;
	return S_OK;
}

// 終了
void CGimmicCarry::Uninit()
{
	if (m_pMove)
	{
		delete m_pMove;
		m_pMove = nullptr;
	}

	if (m_door)
	{
		m_door->Uninit();
		m_door = nullptr;
	}

	if (m_key)
	{
		m_key->Uninit();
		m_key = nullptr;
	}

	if (m_hitModle)
	{
		m_hitModle->Uninit();
		m_hitModle = nullptr;
	}

	if (pCollisionDoorGimmick)
	{
		pCollisionDoorGimmick->Uninit();
		pCollisionDoorGimmick = nullptr;
	}

	if (pCollisionDoor)
	{
		pCollisionDoor->Uninit();
		pCollisionDoor = nullptr;
	}

	if (pCollisionkey)
	{
		pCollisionkey->Uninit();
		pCollisionkey = nullptr;
	}

	Release();
}


//=============================================================================
// 更新
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
void CGimmicCarry::Update()
{
	//あたってたらデータを保存
	SetAnimation(pCollisionDoor->Collision(CObject::OBJTYPE_GIMMICKKEY, false));

	D3DXVECTOR3 pos = GetPos();
	if (CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_log >= 0 && CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.mId >= 0)
	{
		if (GetAnimation() || CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->Player.m_isPopGimmick[GetId()].isUse)
		{	//オンラインのデータかローカルのデータがtrueだったら動く
			if ((pos.x >= m_posDefault.x - 50.0f) && !m_IsLeft)
			{
				pos.x += -m_move.x;
				SetPos(pos);
			}
			else if ((pos.x <= m_posDefault.x + 50.0f) && m_IsLeft)
			{
				pos.x += m_move.x;
				SetPos(pos);
			}
		}
		else if (!GetAnimation())
		{
			if (pos.x <= m_posDefault.x && !m_IsLeft)
			{
				pos.x += m_move.x;
				SetPos(pos);
			}
			else if (pos.x >= m_posDefault.x && m_IsLeft)
			{
				pos.x += -m_move.x;
				SetPos(pos);
			}
		}
	}

	D3DXVECTOR3 move;
	m_Sin++;
	move.y = cosf((D3DX_PI*2.0f) * 0.01f * (m_Sin + (20)));

	D3DXVECTOR3 Pos = m_key->GetPos();
	Pos.y = 20.0f;
	Pos.y += move.y * 5;
	m_key->SetPos(D3DXVECTOR3(Pos.x, Pos.y, Pos.z));
}

// エフェクトのインデックスの設定
void CGimmicCarry::SetRenderMode(int mode)
{
	if (m_door)
		m_door->SetRenderMode(mode);

	if (m_key)
		m_key->SetRenderMode(mode);
}

// モデルの種類の設定
void CGimmicCarry::SetModelType(int nIdx)
{
	if (!m_door)
		return;

	if (nIdx < 0 || nIdx >= CModel3D::GetMaxModel())
		nIdx = 49;

	m_door->SetType(nIdx);

	CCollision_Rectangle3D* pCollision = m_door->GetCollision();
	CModel3D* pModel = m_door->GetModel();

	if (pCollision && pModel)
	{
		D3DXVECTOR3 size = pModel->GetMyMaterial().size, pos = D3DXVECTOR3(0.0f, size.y * 0.5f, 0.0f);

		pCollision->SetSize(size);
		pCollision->SetPos(pos);
	}
}

//=============================================================================
// 移動
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
D3DXVECTOR3 CGimmicCarry::Move()
{
	// 移動情報の計算
	m_pMove->Moving(m_move);
	m_move = D3DXVECTOR3(0.f, 0.f, 0.f);

	// 移動情報の取得
	D3DXVECTOR3 moveing = m_pMove->GetMove();

	return moveing;
}



