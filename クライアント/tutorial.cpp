//=============================================================================
//
// チュートリアルクラス(tutorial.cpp)
// Author : 唐�ｱ結斗
// 概要 : チュートリアルクラスの管理を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "tutorial.h"
#include "calculation.h"
#include "input.h"
#include "application.h"
#include "camera.h"
#include "renderer.h"
#include "object.h"
#include "polygon2D.h"
#include "polygon3D.h"
#include "player.h"
#include "motion_model3D.h"
#include "model3D.h"
#include "model_obj.h"
#include "mesh.h"
#include "sphere.h"
#include "bg.h"
#include "model_obj.h"
#include "debug_proc.h"
#include "sound.h"
#include "line.h"
#include "particle_emitter.h"
#include "collision_rectangle3D.h"
#include "drone.h"
#include "agent.h"
#include "enemy.h"
#include "GimmicDoor.h"
#include "model_data.h"
#include "tcp_client.h"
#include "application.h"
#include "map.h"
#include "agent.h"
#include "minigame_ring.h"
#include "task.h"
#include "gun.h"
#include "cylinder.h"
#include "mesh2D.h"
#include "ranking.h"
#include "Score.h"

#include "walkingEnemy.h"
#include "hostage.h"
#include "carry_player.h"
#include "radio.h"
#include "AgentUI.h"
#include "stone.h"

#include "shadow_volume.h"

#include "model_skin.h"
#include "stencil_canvas.h"

#include "HackingObj.h"
#include "securityTimer.h"
#include "carAnimation.h"

#include "cardboard.h"


//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
CAgent *CTutorial::m_pPlayer = nullptr;						// プレイヤークラス
CTask* CTutorial::m_pTask = nullptr;						// タスクマネージャー
CMesh3D* CTutorial::m_Mesh = nullptr;						// メッシュ
CAgentUI* CTutorial::m_pUiManager = nullptr;				// UIマネージャー

namespace
{
	const float gravity = 0.2f;

	// フォグの数値設定
	const D3DXCOLOR fogColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f);		// フォグカラー
	const float fogStartPos = 600.0f;								// フォグの開始点
	const float fogEndPos = 2000.0f;								// フォグの終了点
	const float fogDensity = 0.00001f;								// フォグの密度
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CTutorial::CTutorial()
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CTutorial::~CTutorial()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CTutorial::Init()
{//MAPのデータを初期化する。
	InitPosh();

	CApplication::GetInstance()->GetPlayer(0)->GetPlayerData()->SetPlayerClear();
	CApplication::GetInstance()->GetPlayer(1)->GetPlayerData()->SetPlayerClear();

	CApplication::GetInstance()->SetConnect(true);
	CApplication::GetInstance()->SetEnemyConnect(true);

	// 入力デバイスの取得
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	//pSound->SetVolume(CSound::SOUND_LABEL_BGM001, 0.01f);
	pSound->PlaySound(CSound::SOUND_LABEL_BGM001);

	m_pTask = CTask::Create();

	{
		CPolygon3D* pObj = CPolygon3D::Create();

		if (pObj)
		{
			pObj->SetPos(D3DXVECTOR3(0.0f, 1.0f, 0.0f));
			pObj->SetSize(D3DXVECTOR3(10000.0f, 10000.0f, 0.0f));
			pObj->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(200.0f, 200.0f));
			pObj->SetRot(D3DXVECTOR3(D3DX_PI * 0.5f, 0.0f, 0.0f));
			pObj->LoadTex(92);
		}
	}

	m_Map = CApplication::GetInstance()->GetMap();

	if (m_Map < 0 || m_Map >= CApplication::GetInstance()->GetMapDataManager()->GetMaxMap())
		m_Map = 0;

	CMap* pMap = CMap::Create(m_Map);

	SetMap(pMap);

	CApplication::GetInstance()->SetMap(m_Map);

	CApplication::SMapList MapData;
	MapData.enemyCount = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vEnemyData.size();
	MapData.gimmickConnect = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_Map).vGimmickData.size();
	CApplication::GetInstance()->SetList(MapData);

	// 重力の値を設定
	CCalculation::SetGravity(0.2f);

	// スカイボックスの設定
	CSphere *pSphere = CSphere::Create();
	pSphere->SetRot(D3DXVECTOR3(D3DX_PI, 0.0f, 0.0f));
	pSphere->SetSize(D3DXVECTOR3(10.0f, 0, 10.0f));
	pSphere->SetBlock(CMesh3D::DOUBLE_INT(10, 10));
	pSphere->SetRadius(10000.0f);
	pSphere->SetSphereRange(D3DXVECTOR2(D3DX_PI * 2.0f, D3DX_PI * -0.5f));
	pSphere->LoadTex(1);

	// プレイヤーの設定
	m_pPlayer = CAgent::Create();
	m_pPlayer->SetMotion("data/MOTION/motion.txt");

	CGun *pGun = CGun::Create();
	pGun->SetType(56);
	CCollision_Rectangle3D *pCollision = pGun->GetCollision();
	D3DXVECTOR3 modelSize = pGun->GetModel()->GetMyMaterial().size;
	pCollision->SetPos(D3DXVECTOR3(0.f, modelSize.y / 2.0f, 0.f));
	pCollision->SetSize(modelSize);

	pGun->SetPos(D3DXVECTOR3(0.0f, 0.0f, -100.0f));
	pGun->SetAllReload(true);
	pGun->SetRapidFire(false);
	pGun->SetMaxBullet(15);
	pGun->SetMaxReload(60);

	// カメラの追従設定(目標 : プレイヤー)
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, D3DX_PI, 0.0f));
	pCamera->SetUseRoll(true, true);
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	// カメラの追従設定(目標 : プレイヤー)
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 5000.0f, -1.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetViewSize(0, 0, 250, 250);
	pCamera->SetUseRoll(false, true);
	pCamera->SetAspect(D3DXVECTOR2(10000.0f, 10000.0f));
	pCamera->SetFollowTarget(m_pPlayer, 1.0);

	// フォグの設定
	SetFog(pDevice, fogStartPos, fogEndPos, fogDensity, fogColor);

	// マウスカーソルを消す
	pInput->SetCursorErase(false);
	pInput->SetCursorLock(true);

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CTutorial::Uninit()
{// 外部デバイスの取得
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	// フォグの有効設定
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	// マウスカーソルを出す
	pInput->SetCursorErase(true);

	// カメラの追従設定
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// カメラの追従設定
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CTutorial::Update()
{
	// カメラの追従設定
	CCamera *pCamera = nullptr;

	// 外部デバイスの取得
	CInput *pInput = CInput::GetKey();

#ifdef _DEBUG

	// カメラの追従設定
	pCamera = CApplication::GetInstance()->GetCamera();

	if (pInput->Trigger(DIK_F10))
	{
		pCamera->Shake(60, 50.0f);
	}

	if (pInput->Press(DIK_LSHIFT))
	{
		pCamera->Zoom();
	}

	// デバック表示
	CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル| F7 デバック表示削除\n");

	if (pInput->Trigger(DIK_F1))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_RESULT);
	}
	if (pInput->Trigger(DIK_F2))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_AGENT);
	}
	if (pInput->Trigger(DIK_F3))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_HACKER);
	}
	if (pInput->Trigger(DIK_F4))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TUTORIAL);
	}
	if (pInput->Trigger(DIK_F5))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TITLE);
	}

#endif // _DEBUG

	if (pInput->Trigger(DIK_F3))
	{
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_TITLE);
	}
}