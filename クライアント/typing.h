#ifndef _MINGAME_RING_H_		// このマクロ定義がされてなかったら
#define _MINGAME_RING_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include"polygon2D.h"
#include "minigame.h"

class CMinGame_Typing : public CMiniGame
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CMinGame_Typing(int nPriority = PRIORITY_LEVEL0);
	~CMinGame_Typing();

	HRESULT Init();														// 初期化
	void Uninit();															// 終了
	void Update();															// 更新
	void Draw();															// 描画

	static CMinGame_Typing *Create(int count);

protected:

private:

	bool flg;
	bool uninitflg;
	bool uninitflg2;
	bool uninitflg3;
	bool uninitflg4;

	enum Tex_Type
	{
		Tex_fst,

		Tex_Sec,
		Tex_thi,
		Tex_for,
	};

	enum Mission_TYPE
	{

		MISSION_NONE = 0,

		MISSION_UP,
		MISSION_DOWN,
		MISSION_RIGHT,
		MISSION_LEFT,

		MAX_MISSION				// 最大レベル
	};

	Mission_TYPE type;

	CPolygon2D*	m_backGround;

	CPolygon2D* pPolygonUp;
	CPolygon2D* pPolygonDown;
	CPolygon2D* pPolygonRight;
	CPolygon2D* pPolygonLeft;
	CPolygon2D* pyajirushi;


};
#endif
