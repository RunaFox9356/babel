//=============================================================================
//
// タイトルクラス(title.cpp)
// Author : 唐�ｱ結斗
// 概要 : タイトルクラスの管理を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "title.h"

#include "application.h"
#include "input.h"
#include "sound.h"
#include "client.h"
#include "debug_proc.h"
#include "camera.h"
#include "text.h"
#include "fade.h"
#include "loading.h"
#include "polygon3D.h"
#include "mesh.h"
#include "move.h"
#include "motion_model3D.h"
#include "shadow_volume.h"
#include "stencil_canvas.h"
#include "model_skin.h"
#include "map.h"

#include "optionMenu.h"

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CTitle::CTitle() : m_ETitleMode(MODE_TITLE),
m_ENextMode(MODE_GAME),
m_EPlayerType(TYPE_NONE),
m_pLoading(nullptr),
m_nCntFrame(0),
m_fFlash(0.0f),
m_fAlpha(0.0f),
m_fAddFlash(0.0f),
m_bKey(false),
m_bTransition(false),
m_pOptionMenu(nullptr)
{
	m_pPolygon.clear();
	m_pPolygon3D.clear();
	m_pText.clear();

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pPlayerSel[nCnt] = nullptr;
	}
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CTitle::~CTitle()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CTitle::Init()
{
	//CModel3D::LoadModelAllNo();
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetUseRoll(false, false);
	pCamera->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));
	pCamera->SetUseMove(false);
	CCamera *pCameraMap = CApplication::GetInstance()->GetCamera();

	m_list = CText::Create(D3DXVECTOR3(0.0f, 100.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "マスター版（バージョン1.00）",D3DXVECTOR3(10.0f, 20.0f, 20.0f) );

	// 移動クラスのメモリ確保
	m_pMove = new CMove;
	assert(m_pMove != nullptr);

	// 地面の生成
	CMesh3D* m_Mesh = CMesh3D::Create();
	m_Mesh->SetSize(D3DXVECTOR3(10000.0f, 0.0f, 10000.0f));
	m_Mesh->SetBlock(CMesh3D::DOUBLE_INT(30, 30));
	m_Mesh->SetSplitTex(true);
	m_Mesh->SetUseCollison(true);
	m_Mesh->LoadTex(9);

	//// プレイヤーの設定
	//CMotionModel3D *pPlayer = CMotionModel3D::Create();
	//pPlayer->SetMotion("data/MOTION/motion.txt");

	// ステンシルの描画
	CStencilCanvas *pStencilCanvas = CStencilCanvas::Create();
	pStencilCanvas->SetCol(D3DXCOLOR(0.f, 0.f, 0.f, 1.f));

	NextMode(MODE_TITLE);

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM000);
	{
		m_pBg = CPolygon3D::Create();

		if (m_pBg)
		{
			m_pBg->LoadTex(1);
			m_pBg->SetPos(D3DXVECTOR3(0.0f, 2000.0f, 1500.0f));
			m_pBg->SetSize(D3DXVECTOR3(20000.0f, 4000.0f, 0.0f));
			m_pBg->SetTex(0, D3DXVECTOR2(0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f));
		}
	}

	CMap::Create(0);

	//m_socket->Init();
	m_Connect.myConnect = false;
	m_Connect.enemyConnect = false;
	
	

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CTitle::Uninit()
{
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	//m_socket->Uninit();

	if (m_pMove != nullptr)
	{// メモリの解放
		delete m_pMove;
		m_pMove = nullptr;
	}

	// ポリゴンの解放
	int nPolygonSize = m_pPolygon.size();
	if (nPolygonSize < 0)
	{
		for (int nCnt = 0; nCnt < nPolygonSize; nCnt++)
		{
			m_pPolygon[nCnt]->Uninit();
			m_pPolygon[nCnt] = nullptr;
		}
	}

	int nTextSize = m_pText.size();
	if (nTextSize < 0)
	{
		for (int nCnt = 0; nCnt < nTextSize; nCnt++)
		{
			m_pText[nCnt]->Uninit();
			m_pText[nCnt] = nullptr;
		}
	}

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pPlayerSel[nCnt] = nullptr;
	}
	if (m_pBg)
	{
		m_pBg = nullptr;
	}

	// ポリゴンの解放
	int nPolygon3DSize = m_pPolygon3D.size();
	if (nPolygon3DSize < 0)
	{
		for (int nCnt = 0; nCnt < nPolygon3DSize; nCnt++)
		{
			m_pPolygon3D[nCnt]->Uninit();
			m_pPolygon3D[nCnt] = nullptr;
		}
	}

	if (m_pOptionMenu)
	{
		//m_pOptionMenu->Uninit();
		m_pOptionMenu = nullptr;
	}

	// スコアの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新を行う
//=============================================================================
void CTitle::Update()
{
	CDebugProc::Print("シャドウボリュームの使用数 : %d個使用中\n", CShadowVolume::nCntMyObject);

	bool bFade = CApplication::GetInstance()->GetFade()->GetFadeSituation();

	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	if (!m_pOptionMenu)
	{
		if (!bFade)
		{//遷移中ではない場合

			switch (m_ETitleMode)
			{
			case CTitle::MODE_TITLE:
				TitleMode();
				break;

			case CTitle::MODE_SELECT:
				CharaSelect();
				break;

			case CTitle::MODE_GAME_SELECT:
				if (m_Connect.myConnect&&m_Connect.enemyConnect)
				{
					CApplication::GetInstance()->SetNextMode(CApplication::MODE_MAPSELECT);
				}
				break;

			default:
				assert(false);
				break;
			}
		}

	
		if (pInput->Trigger(DIK_F2))
		{

			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_AGENT);
		}
		if (pInput->Trigger(DIK_F3))
		{

			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_HACKER);
		}
#ifdef _DEBUG
		// デバック表示
		CDebugProc::Print("F1 リザルト| F2 エージェント| F3 ハッカー| F4 チュートリアル| F5 タイトル | F6 ゲーム | F7 デバック表示削除 | F9 マップ選択\n");

		if (!m_bTransition && !bFade)
		{
			if (pInput->Trigger(DIK_F1))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_RESULT);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F2))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_AGENT);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F3))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_HACKER);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F4))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_TUTORIAL);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F5))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_TITLE);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F6))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_GAME);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F9))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_MAPSELECT);
				m_bTransition = true;
			}
			else if (pInput->Trigger(DIK_F10))
			{
				CApplication::GetInstance()->SetNextMode(CApplication::MODE_GAMEOVER);
				m_bTransition = true;
			}
		
			
		}
#endif // _DEBUG
	}
	else
	{
		if (pInput->Trigger(DIK_O))
		{
			m_option->TexChange("option");
			m_pOptionMenu->Uninit();
			m_pOptionMenu = nullptr;
		}
	
	}
	//m_socket->Update();
}

void CTitle::NextMode(TITLE_MODE nextMode)
{
	// カメラの設定
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetUseRoll(false, false);
	pCamera->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	pCamera->SetPosVOffset(D3DXVECTOR3(0.0f, 50.0f, -200.0f));
	pCamera->SetPosROffset(D3DXVECTOR3(0.0f, 0.0f, 100.0f));

	m_ETitleMode = nextMode;

	switch (m_ETitleMode)
	{
	case CTitle::MODE_TITLE:
	{
		// タイトルロゴ
		CPolygon2D* pPolygon = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);
		pPolygon->SetPos(D3DXVECTOR3(240.0f, 580.0f, 0.0f));
		pPolygon->SetSize(D3DXVECTOR3(1280.0f*0.5f, 720.0f*0.5f, 0.0f));
		pPolygon->LoadTex(75);
		m_pPolygon.push_back(pPolygon);

		CText* pText = CText::Create(D3DXVECTOR3(900.0f, 500.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 600, 3, "New Game",
			D3DXVECTOR3(20.0f, 40.0f, 20.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), CFont::FONT_SOUEIKAKU);
		m_pText.push_back(pText);
		pText = CText::Create(D3DXVECTOR3(900.0f, 580.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 600, 3, "Tutorial",
			D3DXVECTOR3(20.0f, 40.0f, 20.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), CFont::FONT_SOUEIKAKU);
		m_pText.push_back(pText);
		pText = CText::Create(D3DXVECTOR3(900.0f, 660.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 600, 3, "Exit",
			D3DXVECTOR3(20.0f, 40.0f, 20.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), CFont::FONT_SOUEIKAKU);
		m_pText.push_back(pText);
	}
		break;
	case CTitle::MODE_SELECT:
	{
		m_option = CText::Create(D3DXVECTOR3(1000.0f, 600.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), CText::MAX, 1000, 10, "option", D3DXVECTOR3(10.0f, 20.0f, 20.0f), D3DXCOLOR(0.3f, 1.0f, 0.3f, 1.0f));
		CPolygon3D *pPolygon3D = CPolygon3D::Create();
		pPolygon3D->SetPos(D3DXVECTOR3(100.0f, 25.0f, 0.0f));
		pPolygon3D->SetSize(D3DXVECTOR3(50.0f, 25.0f, 0.0f));
		pPolygon3D->SetColor(D3DXCOLOR(1.0f, 0.0f, 1.0f, 1.0f));
		m_pPolygon3D.push_back(pPolygon3D);
		pPolygon3D = CPolygon3D::Create();
		pPolygon3D->SetPos(D3DXVECTOR3(-100.0f, 25.0f, 0.0f));
		pPolygon3D->SetSize(D3DXVECTOR3(50.0f, 25.0f, 0.0f));
		pPolygon3D->SetColor(D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f));
		m_pPolygon3D.push_back(pPolygon3D);

		CSkinMesh* pSkin = CSkinMesh::Create("data/MODEL/skin/default.x");

		if (pSkin)
		{
			pSkin->SetPos(D3DXVECTOR3(100.0f, 0.0f, -5.0f));
			pSkin->SetRot(D3DXVECTOR3(0.0f, D3DX_PI * 0.125f, 0.0f));
		}

		{
			CModelObj* pObj = CModelObj::Create();

			if (pObj)
			{
				pObj->SetPos(D3DXVECTOR3(-100.0f, 25.0f, -5.0f));
				pObj->SetRot(D3DXVECTOR3(0.0f, -D3DX_PI * 0.125f, 0.0f));
				pObj->SetType(50);
				pObj->GetModel()->SetSize(D3DXVECTOR3(0.5f, 0.5f, 0.5f));
			}
		}

		for (int nCnt = 0; nCnt < 2; nCnt++)
		{
			m_pPlayerSel[nCnt] = CPolygon3D::Create();

			if (m_pPlayerSel[nCnt])
			{
				m_pPlayerSel[nCnt]->SetBillboard(true);
				m_pPlayerSel[nCnt]->SetPos(D3DXVECTOR3(100.0f - (200.0f * nCnt), 75.0f, -5.0f));
				m_pPlayerSel[nCnt]->SetSize(D3DXVECTOR3(100.0f, 66.7f, 0.0f));
				m_pPlayerSel[nCnt]->LoadTex(97);
				m_pPlayerSel[nCnt]->SetTex(0, D3DXVECTOR2(0.0f, 0.5f - (0.5f * nCnt)), D3DXVECTOR2(1.0f, 1.0f - (0.5f * nCnt)));
			}
		}
	}
		break;

	case CTitle::MODE_GAME_SELECT:
		break;

	default:
		assert(false);
		break;
	}
}

//=============================================================================
// タイトルモード
// Author : 唐�ｱ結斗
// 概要 : タイトルモード時の更新を行う
//=============================================================================
void CTitle::TitleMode()
{
	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	if (m_nCntFrame >= 40)
	{
		for (int nCnt = 0; nCnt < (int)m_pPolygon.size(); nCnt++)
		{
			m_pPolygon[nCnt]->Uninit();
		}

		m_pPolygon.clear();

		for (int nCnt = 0; nCnt < (int)m_pText.size(); nCnt++)
		{
			m_pText[nCnt]->Uninit();
		}

		m_pText.clear();

		if (m_ENextMode == MODE_END)
		{
			// ウィンドウを破棄
			DestroyWindow(CApplication::GetInstance()->GetWnd());
		}

		m_nCntFrame = 0;
		m_bKey = false;
		NextMode(MODE_SELECT);
	}
	else
	{
		if (!m_bKey)
		{
			if (pInput->Trigger(DIK_RETURN)
				&& m_ENextMode != MODE_TUTORIAL)
			{
				m_bKey = true;
			}

			int nType = (int)m_ENextMode;

			if (pInput->Trigger(DIK_W))
			{
				nType--;

				if (nType <= (int)MODE_NONE)
				{
					nType = (int)MODE_END;
				}
			}
			else if (pInput->Trigger(DIK_S))
			{
				nType++;

				if (nType >= (int)MAX_NEXT_MODE)
				{
					nType = (int)MODE_GAME;
				}
			}

			if (m_ENextMode != nType)
			{
				if (m_ENextMode > (int)MODE_NONE
					&& m_ENextMode < (int)MAX_NEXT_MODE)
				{
					D3DXCOLOR color = m_pText[(int)m_ENextMode]->GetColor();
					color.a = 1.0f;
					m_pText[(int)m_ENextMode]->SetColor(color);
					m_pText[(int)m_ENextMode]->SetTextCol(m_pText[(int)m_ENextMode]->GetColor());
				}

				m_ENextMode = (NEXT_MODE)nType;
			}

			m_fAddFlash = 0.07f;
		}
		else
		{
			D3DXCOLOR col = m_pPolygon[0]->GetColor();
			col.a -= 1.0f / 40;
			m_pPolygon[0]->SetColor(col);

			for (int nCnt = 0; nCnt < (int)m_pText.size(); nCnt++)
			{
				col = m_pText[nCnt]->GetColor();
				col.a -= 1.0f / 40;
				m_pText[nCnt]->SetColor(col);
				m_pText[nCnt]->SetTextCol(m_pText[nCnt]->GetColor());
			}

			m_fAddFlash = 0.5f;
			m_nCntFrame++;
		}

		m_fAlpha = 0.2f;
		FlashPolygon(m_pText[(int)m_ENextMode]);
		m_pText[(int)m_ENextMode]->SetTextCol(m_pText[(int)m_ENextMode]->GetColor());
	}
}

void CTitle::CharaSelect()
{
	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	CCamera *pCamera = CApplication::GetInstance()->GetCamera();

	if (m_nCntFrame >= 40)
	{
		for (int nCnt = 0; nCnt < (int)m_pPolygon3D.size(); nCnt++)
		{
			m_pPolygon3D[nCnt]->Uninit();
		}

		m_pPolygon3D.clear();

		m_nCntFrame = 0;
		m_bKey = false;
		NextMode(MODE_GAME_SELECT);



		m_bTransition = true;
	}
	else
	{
		int nType = (int)m_EPlayerType;

		if (pInput->Trigger(DIK_LEFT))
		{
			nType--;

			if (nType <= (int)TYPE_NONE)
			{
				nType = (int)TYPE_HACKER;
			}
		}
		else if (pInput->Trigger(DIK_RIGHT))
		{
			nType++;

			if (nType >= (int)MAX_TYPE)
			{
				nType = (int)TYPE_AGENT;
			}
		}

		if (m_EPlayerType != nType)
		{
			if (m_EPlayerType > (int)TYPE_NONE
				&& m_EPlayerType < (int)MAX_TYPE)
			{
				D3DXCOLOR color = m_pPolygon3D[(int)m_EPlayerType]->GetColor();
				color.a = 1.0f;
				m_pPolygon3D[(int)m_EPlayerType]->SetColor(color);
			}

			m_EPlayerType = (PLAYER_TYPE)nType;
		}

		if (m_EPlayerType > (int)TYPE_NONE
			&& m_EPlayerType < (int)MAX_TYPE)
		{
			D3DXVECTOR3 posR = pCamera->GetPosR();

			if (!m_bKey)
			{
				if (pInput->Trigger(DIK_RETURN))
				{	
					switch (m_ENextMode)
					{
					case CTitle::MODE_GAME:
						SetTransition(m_EPlayerType);
						break;

					case CTitle::MODE_TUTORIAL:
						CApplication::GetInstance()->SetNextMode(CApplication::MODE_TUTORIAL);
						m_bTransition = true;
						break;

					default:
						assert(false);
						break;
					}
					
					m_bKey = true;
					
				}

				m_fAddFlash = 0.07f;
				m_pMove->Moving(posR, m_pPolygon3D[(int)m_EPlayerType]->GetPos(), 30);
			}
			else
			{
				m_pMove->Moving(posR, D3DXVECTOR3(0.0f, 0.0f, 100.0f), 40);
				m_fAddFlash = 0.5f;
				m_nCntFrame++;
			}

			m_fAlpha = 0.2f;
			FlashPolygon(m_pPolygon3D[(int)m_EPlayerType]);

			pCamera->SetPosROffset(posR);

		}
	}
	if (pInput->Trigger(DIK_O) && m_pOptionMenu == nullptr)
	{
		m_option->TexChange("exit");
		m_pOptionMenu = COptionMenu::Create();
	}
}

void CTitle::FlashPolygon(CPolygon * pPolygon)
{
	m_fFlash += m_fAddFlash;
	float fAlpha = sinf(m_fFlash) + m_fAlpha;

	if (fAlpha < m_fAlpha)
	{
		fAlpha = m_fAlpha;
	}

	D3DXCOLOR color = pPolygon->GetColor();
	color.a = fAlpha;

	pPolygon->SetColor(color);
}

//=============================================================================
// マッチング
// Author : はまだ
// 概要 : タイトルモード時の更新を行う
//=============================================================================
void CTitle::SetTransition(PLAYER_TYPE mode)
{
	CSound* pSound = CApplication::GetInstance()->GetSound();

	pSound->PlaySound(CSound::SOUND_LABEL_SE_DECIDE);

	std::thread ConnectOn([&] {m_Connect = CApplication::GetInstance()->ConnectTh(mode); });

	// スレッドをきりはなす
	ConnectOn.detach();
	switch (mode)
	{
	case CTitle::TYPE_NONE:
		break;
	case CTitle::TYPE_AGENT:
		m_model = CApplication::MODE_GAME;
		CApplication::GetInstance()->SetSelectMode(CApplication::MODE_GAME);
		break;
	case CTitle::TYPE_HACKER:
		m_model = CApplication::MODE_HACKER;
		CApplication::GetInstance()->SetSelectMode(CApplication::MODE_HACKER);
		break;
	case CTitle::MAX_TYPE:
		break;
	default:
		break;
	}

	//CApplication::SetNextMode(mode);
	m_bTransition = true;
	m_pLoading = new CLoading;
	m_pLoading->Init();

}
