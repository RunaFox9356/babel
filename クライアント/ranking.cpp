//=============================================================================
//
// ranking.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "ranking.h"
#include "application.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const char*	CRanking::DEFAULT_RANKING_FILE_PATH = "data\\RANKING\\ranking.txt";		//ディフォルトのランキングファイルのパス



//コンストラクタ
CRanking::CRanking() : m_nLastRank(0),
m_bAgent(true)
{
	//変数をクリアする
	for (int nCnt2 = 0; nCnt2 < 2; nCnt2++)
	{
		for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
		{
			m_aScore[nCnt2][nCnt] = 0;
		}
	}

	m_vAgentTask.clear();
	m_vAgentTask.shrink_to_fit();
	m_vHackerTask.clear();
	m_vHackerTask.shrink_to_fit();
}

//デストラクタ
CRanking::~CRanking()
{

}

//初期化
HRESULT CRanking::Init()
{
	//プログラム終了まで消えないように設定する
	SetPermanent(true);

	//ランキングファイルを開く
	std::ifstream strm(DEFAULT_RANKING_FILE_PATH);

	if (strm.good())
	{//開けたら

		//ランキングを読み込む
		for (int nCnt2 = 0; nCnt2 < 2; nCnt2++)
		{
			for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
			{
				strm >> m_aScore[nCnt2][nCnt];
			}
		}
	}

	//ファイルを閉じる
	strm.close();

	return S_OK;
}

//終了
void CRanking::Uninit()
{
	//メモリの解放
	CSuper::Release();
}

//更新
void CRanking::Update()
{

}

//描画
void CRanking::Draw()
{

}

//スコアの設定処理
const int CRanking::GetRank(const int nScore)
{
	//ローカル変数を宣言し、値を代入する
	int nLastScore = nScore;				
	int aScore[DEFAULT_RANKING_NUM + 1] = {};
	m_nLastScore = nScore;
	int nMap = CApplication::GetInstance()->GetMap();

	if (nMap < 0 || nMap >= 2)
		nMap = 0;

	//スコアのローカル変数を代入する
	for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
	{
		aScore[nCnt] = m_aScore[nMap][nCnt];
	}
	aScore[DEFAULT_RANKING_NUM] = nLastScore;

	int nMemory = -1;

	//ランキングを並び替える
	for (int nCount2 = 0; nCount2 < DEFAULT_RANKING_NUM; nCount2++)
	{
		for (int nCount = nCount2 + 1; nCount < DEFAULT_RANKING_NUM + 1; nCount++)
		{
			if (aScore[nCount2] <= aScore[nCount])
			{
				nMemory = aScore[nCount];
				aScore[nCount] = aScore[nCount2];
				aScore[nCount2] = nMemory;
			}
		}
	}

	//今回のランク
	int nRank = -1;

	//メンバー変数を更新し、今回のランクを代入する
	for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
	{
		m_aScore[nMap][nCnt] = aScore[nCnt];

		if (m_aScore[nMap][nCnt] == nLastScore)
			nRank = nCnt;
	}

	//ランキングを書き出す
	SaveRanking();

	//今回のランクを保存する
	m_nLastRank = nRank;

	return nRank;			//今回のランクを返す
}

//前回のスコアとランクの取得
void CRanking::GetLastScoreAndRank(int & nScore, int & nRank)
{
	nScore = m_nLastScore;
	nRank = m_nLastRank;
}

void CRanking::GetAllScores(std::vector<int>& vScore)
{
	vScore.clear();
	vScore.shrink_to_fit();

	int nMap = CApplication::GetInstance()->GetMap();

	if (nMap < 0 || nMap >= 2)
		nMap = 0;

	for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
	{
		vScore.push_back(m_aScore[nMap][nCnt]);
	}
}

//達成したタスクの保存処理
void CRanking::SaveCompletedTask(std::vector<int> vTask, const bool bAgent)
{
	m_vAgentTask.clear();
	m_vAgentTask.shrink_to_fit();
	m_vHackerTask.clear();
	m_vHackerTask.shrink_to_fit();

	m_bAgent = bAgent;

	if (m_bAgent)
	{
		m_vAgentTask = vTask;
	}
	else
	{
		m_vHackerTask = vTask;
	}
}

//達成したタスクの取得処理
void CRanking::GetCompletedTask(std::vector<int>& vTask, bool & bAgent)
{
	if (m_bAgent)
		vTask = m_vAgentTask;
	else
		vTask = m_vHackerTask;

	bAgent = m_bAgent;
}

//前回のスコアとランクのクリア
void CRanking::ClearLastScoreAndRank()
{
	m_nLastScore = 0;
	m_nLastRank = 0;
}



//生成
CRanking * CRanking::Create()
{
	CRanking* pObj = new CRanking;		//インスタンスを生成する

	if (FAILED(pObj->Init()))			//初期化処理
		return nullptr;

	return pObj;						//生成したインスタンスを返す
}

//ランキングを並び替える
void CRanking::SaveRanking()
{
	//ランキングのファイルを開く
	std::ofstream strm(DEFAULT_RANKING_FILE_PATH);

	if (strm.good())
	{//開けたら

		for (int nCnt2 = 0; nCnt2 < 2; nCnt2++)
		{
			//ランキングを書き出す
			for (int nCnt = 0; nCnt < DEFAULT_RANKING_NUM; nCnt++)
			{
				strm << m_aScore[nCnt2][nCnt] << "\n";
			}

			strm << "\n\n\n";
		}
	}

	//ファイルを閉じる
	strm.close();
}
