//=============================================================================
//
// チュートリアルクラス(tutorial.h)
// Author : 唐�ｱ結斗
// 概要 : チュートリアルクラスの管理を行う
//
//=============================================================================
#ifndef _TUTORIAL_H_		// このマクロ定義がされてなかったら
#define _TUTORIAL_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "scene_mode.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CAgent;
class CDrone;
class CMesh3D;
class CLine;
class CModelObj;
class CCollision_Rectangle3D;
class CGimmicDoor;
class CPlayer;
class CTask;
class CMap;
class CRadio;
class CAgentUI;
class CStencilCanvas;
class CSecurityTimer;

//=============================================================================
// ゲームクラス
// Author : 唐�ｱ結斗
// 概要 : ゲーム生成を行うクラス
//=============================================================================
class CTutorial : public CSceneMode
{
public:
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CTutorial();
	~CTutorial() override;

	//--------------------------------------------------------------------
	// 静的メンバ変数
	//--------------------------------------------------------------------
	static CAgent *m_pPlayer;						// プレイヤークラス
	static CTask* m_pTask;							// タスクマネージャー
	static CAgentUI* m_pUiManager;					// UIマネージャー
	static CMesh3D* m_Mesh;

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;												// 初期化
	void Uninit() override;													// 終了
	void Update() override;													// 更新

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------		
	int m_Map;
};

#endif




