#include "application.h"
#include "renderer.h"
#include "particle.h"
#include "utility.h"
#include "camera.h"

namespace
{
	const int   startCollisionTime = 30;
	const float eraseDistance = 1000.0f;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルのコンストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CParticle::CParticle() :
	m_info({}),
	m_nTime(0)			// 時間
{
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルのデストラクタ
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CParticle::~CParticle()
{
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの生成
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
CParticle * CParticle::Create(SInfo& info, const std::string& url, CParticleEmitter* emitter)
{
	CParticle *pParticle = new CParticle();

	if (pParticle != nullptr)
	{
		pParticle->SetInfo(info);
		pParticle->SetPath(url);
		pParticle->m_pEmitter = emitter;
		pParticle->Init();
	}
	else
	{
		assert(false);
	}

	return pParticle;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの初期化
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
HRESULT CParticle::Init()
{
	// オブジェクトの初期化
	CPolygon3D::Init();

	CPolygon3D::SetPos(m_info.pos);
	CPolygon3D::SetColor(m_info.col);
	CPolygon3D::SetSize(m_info.scale);
	CPolygon3D::SetRot(D3DXVECTOR3(0.0f, 0.0f, m_info.rotateValue));

	m_move		  = m_info.move;
	m_DestroyTime = m_info.popTime;

	if (m_info.useRandom)
	{	// ランダムな値を使用する場合
		m_info.weight   = utility::Random<float>(m_info.random.randomWeight.x, m_info.random.randomWeight.y);
		m_info.velocity = D3DXVECTOR3(utility::Random<float>(m_info.random.randomVelocityMax.x, m_info.random.randomVelocityMin.x),
									  utility::Random<float>(m_info.random.randomVelocityMax.y, m_info.random.randomVelocityMin.y),
									  utility::Random<float>(m_info.random.randomVelocityMax.z, m_info.random.randomVelocityMin.z));
	}

	if (m_info.random.randomRotate != D3DXVECTOR2(0.0f, 0.0f))
	{	// ランダムな回転量を使用する場合
		m_info.rotateValue = utility::Random<float>(m_info.random.randomRotate.x, m_info.random.randomRotate.y);
	}

	SetBillboard(true);
	SetPlaneState(true);

	// テクスチャの読み込み
	LoadTex(m_info.textureIndex);

	return S_OK;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの破棄
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticle::Uninit()
{
	CPolygon3D::Uninit();
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの更新
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticle::Update()
{
	CCamera*	pCamera = CApplication::GetInstance()->GetCamera();
	D3DXVECTOR3 pos		= CPolygon3D::GetPos();

	if (pCamera->Distance(pos) > eraseDistance)
	{
		m_DestroyTime = 0;
	}

	CPolygon3D::Update();

	D3DXVECTOR3 scale = CPolygon3D::GetSize();
	D3DXCOLOR   col	  = CPolygon3D::GetColor();
	D3DXMATRIX  mtx	  = CPolygon3D::GetMtxWorld();

	scale += m_info.scalingValue;

	if (m_info.rotate.useRotate)
	{
		m_move = D3DXVECTOR3(m_info.move.x + (m_info.rotate.angle * m_info.rotate.radius) * sinf(D3DXToRadian(m_info.rotate.angle)),
							 m_move.y,
							 m_info.move.z + (m_info.rotate.angle * m_info.rotate.radius) * cosf(D3DXToRadian(m_info.rotate.angle)));

		m_info.rotate.angle += utility::Random(m_info.rotate.randomMax, m_info.rotate.randomMin);
	}

	// 落下
	if (m_nTime >= m_info.fallDelayTime)
	{
		m_move.y -= m_info.weight;
	}

	// 色関係
	if (m_info.destCol.r >= 0.0f && m_info.destCol.g >= 0.0f && m_info.destCol.b >= 0.0f)
	{
		col.r += (m_info.destCol.r - col.r) / (m_info.popTime * m_info.colAttenuation);
		col.g += (m_info.destCol.g - col.g) / (m_info.popTime * m_info.colAttenuation);
		col.b += (m_info.destCol.b - col.b) / (m_info.popTime * m_info.colAttenuation);
	}
	if (m_info.destCol.a >= 0.0f)
	{
		col.a += (m_info.destCol.a - col.a) / (m_info.popTime * m_info.colAttenuation);
	}

	pos += m_move * m_info.moveAttenuation;

	CPolygon3D::SetPos(pos);
	CPolygon3D::SetSize(scale);
	CPolygon3D::SetColor(col);

	m_DestroyTime--;
}

//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
// パーティクルの描画
//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
void CParticle::Draw()
{
	// デバイスのポインタを取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// ライトを無効にする
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// Zテスト
	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

	// アルファテスト
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_ALPHAREF, 0);
	pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	// 合成モードの設定
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	// サンプラーステートの設定
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

	CPolygon3D::Draw();

	// ライトを有効にする
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// Zバッファの設定を元に戻す
	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);

	// アルファテストを無効
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);

	// 設定を元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// サンプラーステートの設定を元に戻す
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
}