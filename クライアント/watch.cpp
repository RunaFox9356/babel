//=============================================================================
//
// watch.cpp
// Author : Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "watch.h"
#include "input.h"
#include "application.h"
#include "Score.h"
#include "Timer.h"
#include "hp_gauge.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const D3DXVECTOR3		CWatch::DEFAULT_INACTIVE_POS = { -150.0f, 400.0f, 0.0f };		//ディフォルトの画面外の位置
const D3DXVECTOR3		CWatch::DEFAULT_ACTIVE_POS = { 150.0f, 400.0f, 0.0 };			//ディフォルトの画面内の位置
const D3DXVECTOR3		CWatch::DEFAULT_SIZE = { 400.0f, 400.0f, 0.0f };				//ディフォルトのサイズ
const float				CWatch::DEFAULT_ANIMATION_SPEED = 10.0f;						//ディフォルトのアニメーションの速度
const int				CWatch::DEFAULT_TEXTURE_IDX[TEXTURE_TYPE_MAX] = { 98, 99 };		//テクスチャのインデックス
const D3DXVECTOR3		CWatch::DEFAULT_COMMAND_POS = { 0.0f, 0.0f, 0.0f };				//ディフォルトの操作UIの相対位置
const D3DXVECTOR3		CWatch::DEFAULT_COMMAND_SIZE = { 80.0f, 80.0f, 0.0f };			//ディフォルトの操作UIのサイズ


namespace
{
	const int DEFAULT_TAB_BUTTON_TEXTURE = 137;
};


//コンストラクタ
CWatch::CWatch() : m_nState(0),
m_fDir(0.0f),
m_scoreRelativePos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_timerRelativePos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_pScore(nullptr),
m_pTimer(nullptr),
m_pHpGauge(nullptr),
m_pCommand(nullptr)
{

}

//デストラクタ
CWatch::~CWatch()
{

}

// 初期化
HRESULT CWatch::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	m_nState = 0;					//現在の状態の設定
	m_fDir = -1.0f;					//移動速度の向きの設定
	SetPos(DEFAULT_INACTIVE_POS);	//位置の設定
	SetSize(DEFAULT_SIZE);			//サイズの設定
	LoadTex(DEFAULT_TEXTURE_IDX[0]);					//テクスチャの設定

	m_pCommand = CPolygon2D::Create();

	if (m_pCommand)
	{
		D3DXVECTOR3 size = D3DXVECTOR3(DEFAULT_SIZE.x * 0.5f, 0.0f, 0.0f);
		m_pCommand->SetPos(DEFAULT_INACTIVE_POS + size + DEFAULT_COMMAND_POS);
		m_pCommand->SetSize(DEFAULT_COMMAND_SIZE);
		m_pCommand->LoadTex(DEFAULT_TAB_BUTTON_TEXTURE);
	}

	return S_OK;
}

// 終了
void CWatch::Uninit()
{
	//ポインタをnullに戻す
	if (m_pScore)
	{
		m_pScore = nullptr;
	}
	if (m_pTimer)
	{
		m_pTimer = nullptr;
	}
	if (m_pHpGauge)
	{
		m_pHpGauge = nullptr;
	}
	if (m_pCommand)
	{
		m_pCommand = nullptr;
	}

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

// 更新
void CWatch::Update()
{
	switch (m_nState)
	{
	case -1:

		CheckKey();				//キーボードインプットの確認

		break;

	case 0:

		UpdateObjectPos();		//オブジェクトの位置の更新処理

		break;

	case 1:

		CheckKey();				//キーボードインプットの確認

		break;

	default:
		break;
	}

	//親クラスの更新処理
	CPolygon2D::Update();
}

//オブジェクトを追加する
void CWatch::AddObj(CScore* pScore, const D3DXVECTOR3 relativePos)
{
	m_scoreRelativePos = relativePos;
	m_pScore = pScore;
}

//オブジェクトを追加する
void CWatch::AddObj(CTimer* pTimer, const D3DXVECTOR3 relativePos)
{
	m_timerRelativePos = relativePos;
	m_pTimer = pTimer;
}

//オブジェクトを追加する
void CWatch::AddObj(CHpGauge * pGauge, const D3DXVECTOR3 relativePos)
{
	m_GaugeRelativePos = relativePos;
	m_pHpGauge = pGauge;
}


//=============================================================================
//
//									静的関数
//
//=============================================================================


//生成処理
CWatch* CWatch::Create()
{
	CWatch* pObj = new CWatch;		//インスタンスを生成する

	if (FAILED(pObj->Init()))		//初期化処理
		return nullptr;				

	return pObj;					//生成したインスタンスを返
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



//オブジェクトの位置の更新
void CWatch::UpdateObjectPos()
{
	//位置の取得
	D3DXVECTOR3 pos = GetPos();

	pos.x += m_fDir * DEFAULT_ANIMATION_SPEED;			//位置の更新

	if (m_fDir > 0.0f && pos.x >= DEFAULT_ACTIVE_POS.x)
	{
		pos.x = DEFAULT_ACTIVE_POS.x;
		m_nState = 1;
	}
	else if (m_fDir < 0.0f && pos.x <= DEFAULT_INACTIVE_POS.x)
	{
		pos.x = DEFAULT_INACTIVE_POS.x;
		m_nState = -1;
	}

	SetPos(pos);										//位置の設定

	if (m_pScore)
	{//nullチェック

		m_pScore->SetPos(pos + m_scoreRelativePos);		//スコアの位置の設定
	}

	if (m_pTimer)
	{//nullチェック

		m_pTimer->SetPos(pos + m_timerRelativePos);		//タイマーの位置の設定
	}

	if (m_pHpGauge)
	{//nullチェック

		m_pHpGauge->SetPos(pos + m_GaugeRelativePos);	//HPゲージの位置の設定
	}

	if (m_pCommand)
	{
		D3DXVECTOR3 size = D3DXVECTOR3(DEFAULT_SIZE.x * 0.5f, 0.0f, 0.0f);
		m_pCommand->SetPos(pos + size + DEFAULT_COMMAND_POS);
	}
}

//キーボード入力の確認
void CWatch::CheckKey()
{
	//インプットの取得
	CInput* pInput = CApplication::GetInstance()->GetInput();

	if (pInput && pInput->Trigger(DIK_TAB))
	{//Lキーを押したら

		m_fDir = (float)m_nState * -1.0f;		//アニメーションの速度の設定
		m_nState = 0;
	}
}

//テクスチャの種類の設定
void CWatch::SetTextureType(int nType)
{
	if (nType < 0 || nType >= TEXTURE_TYPE_MAX)
		nType = 0;

	LoadTex(DEFAULT_TEXTURE_IDX[nType]);
}