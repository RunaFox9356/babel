//=============================================================================
//
// stone.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "stone.h"
#include "model_obj.h"
#include "collision_rectangle3D.h"
#include "mapDataManager.h"
#include "application.h"
#include "input.h"
#include "game.h"
#include "agent.h"
#include "enemy.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			CStone::DEFAULT_MODEL_INDEX = 2;						//ディフォルトのモデルインデック
const float			CStone::DEFAULT_SOUND_RADIUS = 500.0f;					//音が聞こえる範囲の半径
const D3DXVECTOR3	CStone::DEFAULT_HITBOX_SIZE = { 50.0f, 50.0f, 50.0f };	//ディフォルトのヒットボックスサイズ
const int			CStone::DEFAULT_DELAY = 30;								//ディフォルトのディレイ
const float			CStone::DEFAULT_THROW_SPEED = 10.0f;					//ディフォルトの投げる速度


namespace nsStone
{
	const float		DEFAULT_GRAVITY_ACCELERATION = -0.5f;					//ディフォルトの重力加速度
};


//コンストラクタ
CStone::CStone() : m_move(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_nCntDelay(0),
m_bPickedUp(false),
m_state((STATE)0),
m_pModel(nullptr),
m_pHitbox(nullptr)
{

}

//デストラクタ
CStone::~CStone()
{

}

// 初期化
HRESULT CStone::Init()
{
	//オブジェクトの種類の設定
	SetObjType(CObject::OBJETYPE_NON_SOLID_GIMMICK);

	//モデルの生成
	m_pModel = CModelObj::Create();

	if (m_pModel)
	{//nullチェック
		
		m_pModel->SetType(2);			//モデルの種類の設定
	}

	//ヒットボックスの生成
	m_pHitbox = CCollision_Rectangle3D::Create();

	if (m_pHitbox)
	{//nullチェック
		m_pHitbox->SetSize(DEFAULT_HITBOX_SIZE);									//ヒットボックスのサイズの設定
		m_pHitbox->SetPos(D3DXVECTOR3(0.0f, DEFAULT_HITBOX_SIZE.y * 0.5f, 0.0f));	//ヒットボックスの位置の設定
		m_pHitbox->SetParent(this);													//ヒットボックスの親の設定
	}

	return S_OK;
}

// 終了
void CStone::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}

	//ヒットボックスの破棄
	if (m_pHitbox)
	{
		m_pHitbox->Uninit();
		m_pHitbox = nullptr;
	}

	//メモリの解放
	CSuper::Release();
}

// 更新
void CStone::Update()
{
	//状態によっての更新処理
	UpdateState();
}

// 描画
void CStone::Draw()
{

}

// 位置のセッター
void CStone::SetPos(const D3DXVECTOR3& pos)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPos(pos);			//モデルの位置を設定する
	}
}

// 過去位置のセッター
void CStone::SetPosOld(const D3DXVECTOR3& posOld)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetPosOld(posOld);			//モデルの過去の位置を設定する
	}
}

// 位置のゲッター
D3DXVECTOR3 CStone::GetPos()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//nullチェック
		pos = m_pModel->GetPos();		//モデルの位置を取得する
	}

	return pos;							//位置を返す
}

// 過去位置のゲッター
D3DXVECTOR3 CStone::GetPosOld()
{
	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (m_pModel)
	{//nullチェック
		pos = m_pModel->GetPosOld();		//モデルの過去の位置を取得する
	}

	return pos;							//位置を返す
}

// エフェクトのインデックスの設定
void CStone::SetRenderMode(int mode)
{
	if (m_pModel)
	{//nullチェック
		m_pModel->SetRenderMode(mode);			//モデルのエフェクトインデックスの設定
	}
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
CStone * CStone::Create(D3DXVECTOR3 pos)
{
	CStone* pObj = new CStone;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);				//位置の設定

	return pObj;					//生成したインスタンスを返
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



//状態によって更新する
void CStone::UpdateState()
{
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (mode != CApplication::MODE_AGENT && mode != CApplication::MODE_GAME)
		return;

	switch (m_state)
	{
	case CStone::STATE_NORMAL:
		NormalUpdate();
		break;

	case CStone::STATE_PICKED_UP:
		PickedUpUpdate();
		break;

	case CStone::STATE_THROWN:
		ThrownUpdate();
		break;

	default:
		break;
	}
}

//普通状態の更新
void CStone::NormalUpdate()
{
	if (m_pHitbox)
	{//nullチェック

		if (m_pHitbox->Collision(CObject::OBJETYPE_PLAYER, false))
		{//プレイヤーと重なっている場合

			//インプットデバイスの取得
			CInput* pInput = CApplication::GetInstance()->GetInput();

			if (pInput && pInput->Trigger(DIK_R))
			{//Rボタンを押したら

				//プレイヤーの取得
				CAgent* pPlayer = CGame::GetPlayer();

				if (!pPlayer)
					return;

				//拾う
				if (pPlayer->PickUpStone())
				{
					//拾われた状態にする
					m_state = STATE_PICKED_UP;
					m_nCntDelay = DEFAULT_DELAY;
				}
			}
		}
	}
}

//拾われた状態の更新
void CStone::PickedUpUpdate()
{
	//プレイヤーの取得
	CAgent* pPlayer = CGame::GetPlayer();

	if (!pPlayer)
		return;

	//位置の更新
	SetPos(pPlayer->GetPos() + D3DXVECTOR3(0.0f, 15.0f, 0.0f));
	
	if (m_nCntDelay <= 0)
	{
		//インプットデバイスの取得
		CInput* pInput = CApplication::GetInstance()->GetInput();

		if (pInput && pInput->Trigger(DIK_R))
		{//Rボタンを押したら

			if (!pPlayer->ThrowStone())
				return;

			//投げられた状態にする
			m_state = STATE_THROWN;

			//速度の向きを計算し、設定する
			D3DXMATRIX mtxRot;
			D3DXVECTOR3 dir = D3DXVECTOR3(0.0f, 0.0f, -1.0f), rot = pPlayer->GetRot();

			D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);
			D3DXVec3TransformCoord(&dir, &dir, &mtxRot);

			m_move = dir * DEFAULT_THROW_SPEED;
			m_move.y = DEFAULT_THROW_SPEED;

			if (m_pModel)
			{//ディレイの後、描画しないように設定する(ポケットに入れたら)
				m_pModel->SetDraw(true);
			}
		}
	}
	else
	{
		//ディレイの更新
		m_nCntDelay--;

		if (m_nCntDelay <= 0 && m_pModel)
		{//ディレイの後、描画しないように設定する(ポケットに入れたら)
			m_pModel->SetDraw(false);
		}
	}
}

//投げられた状態の更新
void CStone::ThrownUpdate()
{
	//音を出すかどうか
	bool bSound = false;

	//位置を取得する
	D3DXVECTOR3 pos = GetPos();

	SetPosOld(pos);

	//位置の更新
	pos += m_move;
	SetPos(pos);

	//重力をかける
	m_move.y += nsStone::DEFAULT_GRAVITY_ACCELERATION;

	if (CApplication::GetInstance()->GetMapDataManager()->GetMapData(0).vMeshData[0].pMesh->MeshtoSelfCollison(this))
	{//メッシュフィールドと当たった場合

		m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);		//移動を止める
		m_state = STATE_NORMAL;						//普通状態に戻す
		bSound = true;								//音を出す
	}
	else if (m_pHitbox && m_pHitbox->Collision(CObject::EObjectType::OBJTYPE_3DMODEL, false))
	{
		//移動量のXとY座標を逆にし、音を出す
		m_move.x *= -0.75f;
		m_move.z *= -0.75f;

		if ((GetPos().y - GetPosOld().y) * (GetPos().y - GetPosOld().y) <= 0.1f)
			m_state = STATE_NORMAL;

		bSound = true;
	}

	if (bSound)
	{//音を出すフラグがtrueだったら

		CMapDataManager* pManager = CApplication::GetInstance()->GetMapDataManager();

		if (!pManager)
			return;

		int nMapIdx = CApplication::GetInstance()->GetMap();

		if (nMapIdx < 0 || nMapIdx >= pManager->GetMaxMap())
			return;

			CMapDataManager::MAP_DATA allData = pManager->GetMapData(nMapIdx);

			for (int nCnt = 0; nCnt < (int)allData.vEnemyData.size(); nCnt++)
			{
				CEnemy* pEnemy = allData.vEnemyData.data()[nCnt].pEnemy;

				if (pEnemy)
				{
					//距離を計算する
					D3DXVECTOR3 dist = pEnemy->GetPos() - GetPos();		
					float fDist = D3DXVec3Length(&dist);

					if (fDist <= DEFAULT_SOUND_RADIUS)
					{
						pEnemy->SetSoundPos(GetPos());
					}
				}
			}
	}
}