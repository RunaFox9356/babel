//��������������������������
//ShaderManager.h
//��������������������������
#ifndef _SHADER_MANAGER_H_
#define _SHADER_MANAGER_H_

#include "file.h"

class CShaderManager
{
public:
	struct WVP
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	CShaderManager();
	~CShaderManager();

	void Load(nlohmann::json& list);
	void LoadAll();
	void ReleaseAll();
	void Apply(std::string label, unsigned int pass = 0);
	void End(std::string label);
	void AddTechniqueCache(const std::string& label, const std::string& handleName);
	void AddParameterCache(const std::string& label, const std::string& handleName);

	std::unordered_map<std::string, LPD3DXEFFECT> GetEffectData() { return m_effect; }
	D3DVERTEXELEMENT9* GetDeclaration(const std::string& label) { return m_decl[label]; }
	LPD3DXEFFECT GetEffect(std::string label) { return m_effect[label]; }
	D3DXHANDLE GetTechniqueCache(const std::string& label, const std::string& handleName);
	D3DXHANDLE GetParameterCache(const std::string& label, const std::string& handleName);
	LPDIRECT3DVERTEXDECLARATION9 GetVertexDeclaration(std::string path) { return m_vtxDecl[path]; }

private:
	std::unordered_map<std::string, LPD3DXEFFECT>				  m_effect;
	std::unordered_map<std::string, D3DVERTEXELEMENT9*>			  m_decl;
	std::unordered_map<std::string, LPDIRECT3DVERTEXDECLARATION9> m_vtxDecl;
	std::unordered_map<std::string, D3DXHANDLE>					  m_techniqueCache;
	std::unordered_map<std::string, D3DXHANDLE>					  m_parameterCache;
};

#endif
