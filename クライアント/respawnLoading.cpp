//=============================================================================
//
// respawnLoading.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "respawnLoading.h"
#include "loadingBar.h"
#include "text.h"



//=============================================================================
//								静的変数の初期化	
//=============================================================================
const int			CRespawnLoading::DEFAULT_RESPAWN_TIME = 900;					//ディフォルトのリスポーンフレーム数
const D3DXVECTOR3	CRespawnLoading::DEFAULT_POS = { 110.0f, 690.0f, 0.0f };		//ディフォルトの位置
const D3DXVECTOR3	CRespawnLoading::DEFAULT_SIZE = { 200.0f, 25.0f, 0.0f };		//ディフォルトのサイズ



//コンストラクタ
CRespawnLoading::CRespawnLoading() : m_nTime(0),
m_bEnd(false),
m_bChange(false),
m_pBar(nullptr),	
m_pText(nullptr),
m_pNewMessage(nullptr)
{

}

//デストラクタ
CRespawnLoading::~CRespawnLoading()
{

}

//初期化
HRESULT CRespawnLoading::Init()
{
	//親クラスの初期化処理
	if (FAILED(CEmptyObj::Init()))
		return E_FAIL;

	if (m_nTime <= 0)
		m_nTime = DEFAULT_RESPAWN_TIME;

	//ロードバーの生成
	m_pBar = CLoadingBar::Create(DEFAULT_POS, DEFAULT_SIZE, m_nTime);

	{//テキスト

	 //テキストの情報の設定
		D3DXVECTOR3 fontSize = D3DXVECTOR3(8.0f, 8.0f, 8.0f);					//フォントサイズ
		D3DXVECTOR3 size = D3DXVECTOR3(200.0f, 50.0f, 0.0f);					//サイズ
		D3DXVECTOR3 pos = D3DXVECTOR3(DEFAULT_POS.x - DEFAULT_SIZE.x * 0.55f, 660.0f, 0.0f);				//位置
		D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0, 0.2f, 1.0f);						//フォント色

		//テキストの生成
		m_pText = CText::Create(pos, size, CText::MAX, 1000, 5, "RESPAWNING...", fontSize, color);	//テキストの生成
	}

	return S_OK;
}

//終了
void CRespawnLoading::Uninit()
{
	//ロードバーの破棄
	if (m_pBar)
	{
		m_pBar->Uninit();
		m_pBar = nullptr;
	}

	//テキストの破棄
	if (m_pText)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	//親クラスの終了処理
	CEmptyObj::Uninit();
}

//更新
void CRespawnLoading::Update()
{
	//親クラスの更新処理
	CEmptyObj::Update();

	//ロードが終わったら
	if (!m_bEnd && m_pBar && m_pBar->GetEnd())
	{
		m_bEnd = true;

		//テキストを変更する
		if (m_bChange)
			ChangeText();
	}
}

//描画
void CRespawnLoading::Draw()
{
	//親クラスの描画処理
	CEmptyObj::Draw();
}

//新しいテキストの設定
void CRespawnLoading::SetNewMessage(char* pText)
{
	m_bChange = true;
	m_pNewMessage = pText;
}



//=============================================================================
//
//								 静的関数
//
//=============================================================================



//生成
CRespawnLoading* CRespawnLoading::Create(const int nRespawnTime)
{
	CRespawnLoading* pObj = new CRespawnLoading;		//インスタンスを生成する

	if (pObj)
	{//nullチェック

		//リスポーン時間の設定
		pObj->m_nTime = nRespawnTime;

		//初期化処理
		if (FAILED(pObj->Init()))
			return nullptr;
	}

	return pObj;			//生成したインスタンスを返
}




//=============================================================================
//
//								プライベート関数
//
//=============================================================================




//テキストの変更処理
void CRespawnLoading::ChangeText()
{
	m_bChange = false;

	//テキストを破棄する
	if (m_pText)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	{//テキスト

	 //テキストの情報の設定
		D3DXVECTOR3 fontSize = D3DXVECTOR3(8.0f, 8.0f, 8.0f);					//フォントサイズ
		D3DXVECTOR3 size = D3DXVECTOR3(200.0f, 50.0f, 0.0f);					//サイズ
		D3DXVECTOR3 pos = D3DXVECTOR3(DEFAULT_POS.x - DEFAULT_SIZE.x * 0.55f, 660.0f, 0.0f);				//位置
		D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0, 0.2f, 1.0f);						//フォント色

		//テキストの生成
		m_pText = CText::Create(pos, size, CText::MAX, 1000, 5, m_pNewMessage, fontSize, color);	//テキストの生成
	}
}
