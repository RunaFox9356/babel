//=============================================================================
//
// 2Dポリゴンクラス(polygon2D.h)
// Author : 唐�ｱ結斗
// 概要 : ポリゴン生成を行う
//
//=============================================================================
#ifndef _POLYGON2D_H_		// このマクロ定義がされてなかったら
#define _POLYGON2D_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"

//=============================================================================
// 2Dポリゴンクラス
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴン生成を行うクラス
//=============================================================================
class CPolygon2D : public CPolygon
{
public:
	// 頂点フォーマット
	static const unsigned int	FVF_VERTEX_2D = (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1);
	static const unsigned int	FVF_MULTI_TEX_VTX_2D = (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEX2);
	static const unsigned int	MAX_TEX = 2;

	//*****************************************************************************
	// 構造体定義
	//*****************************************************************************
	// 頂点データ
	struct VERTEX_2D
	{
		D3DXVECTOR3		pos;				// 位置
		float			rhw;				// rhw
		D3DCOLOR		col;				// カラー
		D3DXVECTOR2		tex[MAX_TEX];		// テクスチャ座標
	};

	enum ERenderMode
	{
		Render_Default,
		Render_Blur,
		Render_Monochrome,
		Render_MAX
	};

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CPolygon2D *Create(void);				// 2Dポリゴンの生成
	static CPolygon2D *Create(int nPriority);		// 2Dポリゴンの生成(オーバーロード)

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CPolygon2D(int nPriority = PRIORITY_LEVEL0);
	~CPolygon2D() override;

	//--------------------------------------------------------------------
	// オーバーライド関数
	//--------------------------------------------------------------------
	HRESULT Init() override;																	// 初期化
	void Uninit() override;																		// 終了
	void Update() override;																		// 更新
	void Draw() override;																		// 描画
	void DrawShader();

	void SetColor(const D3DXCOLOR &color) override;												// 色の設定
	void SetVtx() override;																		// 頂点座標などの設定
	void SetTex(int nTex, const D3DXVECTOR2 &minTex, const D3DXVECTOR2 &maxTex) override;		// テクスチャ座標の設定
	void SetShader(const bool is, const ERenderMode mode);
	virtual void SetDraw(const bool bDraw) { m_bDraw = bDraw; }								// 描画するかどうかの設定

protected:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	LPDIRECT3DVERTEXBUFFER9		m_pVtxBuff;			// 頂点バッファ

private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	float						m_fAngle;			// 対角線の角度
	float						m_fLength;			// 対角線の長さ
	bool						m_bDraw;			// 描画するかどうか

	//=========================================
	// シェーダー関係
	//=========================================
	void RenderDefault(LPD3DXEFFECT effect, const int pass);
	void RenderBlur(LPD3DXEFFECT effect);

	struct Handler
	{
		bool isEmpty = true;
		D3DXHANDLE Technique;	// テクニック
		D3DXHANDLE useTexture;
		D3DXHANDLE useSubTexture;
	};
	Handler m_handler;
	ERenderMode m_renderMode;
	bool m_useShader;
	//bool	m_bDraw;				//描画フラグ
};
#endif


