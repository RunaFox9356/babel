//=============================================================================
//
// securityTimer.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "securityTimer.h"
#include "renderer.h"
#include "number.h"
#include "text.h"



//=============================================================================
//								静的変数の初期化
//=============================================================================
const float			CSecurityTimer::DEFAULT_TIME = 30.0f;															//ディフォルトの最大時間
const D3DXVECTOR3	CSecurityTimer::DEFAULT_POS = { (int)CRenderer::SCREEN_WIDTH * 0.5f, 75.0f, 0.0f };				//ディフォルトの位置
const D3DXVECTOR3	CSecurityTimer::DEFAULT_SIZE = { 200.0f, 80.0f, 0.0f };											//ディフォルトのサイズ
const D3DXVECTOR3	CSecurityTimer::DEFAULT_DIGIT_SIZE = { 40.0f, 40.0f, 0.0f };									//ディフォルトの数値のサイズ
const char*			CSecurityTimer::DEFAULT_TEXT = "CAUTION!";														//ディフォルトのテキスト


namespace
{
	static const int		DEFAULT_BG_TEXTURE_IDX = 96;								//ディフォルトの背景のテクスチャインデックス
	static const int		DEFAULT_NUMBERS_TEXTURE_IDX = 73;							//数値のテクスチャインデックス
	static const D3DXCOLOR	DEFAULT_TEXT_COLOR = { 1.0f, 1.0f, 0.25f, 1.0f };			//ディフォルトのテキストの色
	static const D3DXCOLOR	DEFAULT_TEXT_ANIM_COLOR = { 1.0f, 0.5f, 0.25f, 1.0f };		//ディフォルトのテキストアニメーション用の色
};



//コンストラクタ
CSecurityTimer::CSecurityTimer() : CPolygon2D::CPolygon2D(CSuper::PRIORITY_LEVEL2),
m_nCntAnim(0),
m_nLastTime(0),
m_fMaxTime(0.0f),
m_fStartTime(0.0f),
m_pText(nullptr),
m_bEnd(false)
{
	for (int nCnt = 0; nCnt < DEFAULT_TIMER_DIGIT; nCnt++)
	{
		m_pTimer[nCnt] = nullptr;
	}
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pIcon[nCnt] = nullptr;
	}
}

//デストラクタ
CSecurityTimer::~CSecurityTimer()
{

}

//初期化
HRESULT CSecurityTimer::Init()
{
	//親クラスの初期化処理
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	LoadTex(DEFAULT_BG_TEXTURE_IDX);		//背景のテクスチャの設定
	SetPos(DEFAULT_POS);					//ディフォルトの位置の設定
	SetSize(DEFAULT_SIZE);					//ディフォルトのサイズの設定
	SetColor(D3DXCOLOR(0.5f, 0.5f, 0.5f, 0.75f));

	//タイマーの数値の生成
	for (int nCnt = 0; nCnt < DEFAULT_TIMER_DIGIT; nCnt++)
	{
		//生成
		m_pTimer[nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

		if (m_pTimer[nCnt])
		{//nullチェック

			D3DXVECTOR3 pos = DEFAULT_POS;
			pos.x += (0.5f * DEFAULT_DIGIT_SIZE.x) - (nCnt * DEFAULT_DIGIT_SIZE.x);
			pos.y += DEFAULT_DIGIT_SIZE.y * 0.5f - 5.0f;

			m_pTimer[nCnt]->LoadTex(DEFAULT_NUMBERS_TEXTURE_IDX);
			m_pTimer[nCnt]->SetSize(DEFAULT_DIGIT_SIZE);
			m_pTimer[nCnt]->SetPos(pos);
		}
	}

	m_fStartTime = (float)timeGetTime();			//はじめの時間の取得

	{
		D3DXVECTOR3 fontSize = D3DXVECTOR3(10.0f, 10.0f, 10.0f);							//フォントサイズ
		D3DXVECTOR3 P = DEFAULT_POS - D3DXVECTOR3(DEFAULT_SIZE.x * 0.5f - fontSize.x * 1.5f, 20.0f, 0.0f);	//絶対位置の設定
		m_pText = CText::Create(P, DEFAULT_SIZE, CText::MAX, 1000, 5, DEFAULT_TEXT, fontSize, DEFAULT_TEXT_ANIM_COLOR);		//テキストの生成
	}

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		m_pIcon[nCnt] = CPolygon2D::Create(CSuper::PRIORITY_LEVEL3);

		if (m_pIcon[nCnt])
		{
			D3DXVECTOR3 pos = DEFAULT_POS, size = D3DXVECTOR3(DEFAULT_SIZE.y, DEFAULT_SIZE.y, 0.0f);
			pos.x += ((-((DEFAULT_SIZE.x * 0.5f) + size.x * 0.75f))) * (1.0f + (-2.0f * nCnt));
			m_pIcon[nCnt]->SetPos(pos);
			m_pIcon[nCnt]->SetSize(size);
			m_pIcon[nCnt]->LoadTex(62);
		}
	}

	return S_OK;
}

//終了
void CSecurityTimer::Uninit()
{
	//数値の破棄
	for (int nCnt = 0; nCnt < DEFAULT_TIMER_DIGIT; nCnt++)
	{
		if (m_pTimer[nCnt])
		{
			m_pTimer[nCnt]->Uninit();
			m_pTimer[nCnt] = nullptr;
		}
	}

	//テキストの破棄
	if (m_pText)
	{
		m_pText->Uninit();
		m_pText = nullptr;
	}

	//アイコンの破棄
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pIcon[nCnt])
		{
			m_pIcon[nCnt]->Uninit();
			m_pIcon[nCnt] = nullptr;
		}
	}

	//親クラスの終了処理
	CPolygon2D::Uninit();
}

//更新
void CSecurityTimer::Update()
{
	//親クラスの更新処理
	CPolygon2D::Update();

	if (!m_bEnd)
	{//終わっていなかったら

		UpdateTimer();		//タイマーの更新
		IconUpdate();		//アイコンの更新
	}
}

//描画
void CSecurityTimer::Draw()
{
	//親クラスの描画処理
	CPolygon2D::Draw();
}

//位置の設定
void CSecurityTimer::SetPos(const D3DXVECTOR3& pos)
{
	D3DXVECTOR3 p = pos;

	//親の位置の設定処理
	CPolygon2D::SetPos(p);

	//数値の位置を計算し、設定する
	for (int nCnt = 0; nCnt < DEFAULT_TIMER_DIGIT; nCnt++)
	{
		if (m_pTimer[nCnt])
		{
			p = pos;
			p.x += (0.5f * DEFAULT_DIGIT_SIZE.x) - (nCnt * DEFAULT_DIGIT_SIZE.x);
			p.y += DEFAULT_DIGIT_SIZE.y * 0.5f - 5.0f;

			m_pTimer[nCnt]->SetPos(p);
		}
	}

	//アイコンの位置を計算し、設定する
	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pIcon[nCnt])
		{
			D3DXVECTOR3 p = pos, size = D3DXVECTOR3(DEFAULT_SIZE.y, DEFAULT_SIZE.y, 0.0f);
			p.x += ((-((DEFAULT_SIZE.x * 0.5f) + size.x * 0.75f))) * (1.0f + (-2.0f * nCnt));
			m_pIcon[nCnt]->SetPos(p);
		}
	}
}

//タイマーをリセットする
void CSecurityTimer::ResetTimer()
{
	m_fStartTime = (float)timeGetTime();		//はじめの時間の取得
	m_bEnd = false;
	m_nLastTime = 0;
}


//=============================================================================
//
//									静的関数
//
//=============================================================================


//生成
CSecurityTimer* CSecurityTimer::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, float fTime)
{
	CSecurityTimer* pObj = new CSecurityTimer;			//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);				//位置の設定
	pObj->SetSize(size);			//サイズの設定
	pObj->m_fMaxTime = fTime;		//最大時間の設定

	return pObj;					//生成したインスタンスを返
}



//=============================================================================
//
//							プライベート関数
//
//=============================================================================



//タイマーの更新処理
void CSecurityTimer::UpdateTimer()
{
	//時間を取得し、経過した時間を計算する
	float fTime = (float)timeGetTime(), pastTime = (fTime - m_fStartTime) * 0.001f;

	//表示する時間を計算する
	int nTime = (int)(m_fMaxTime - (pastTime)), nNum = 10;

	//表示されている時間が変わっていなかったら、中断する
	if (m_nLastTime == nTime)
		return;

	//このフレームの時間を保存する
	m_nLastTime = nTime;

	if (pastTime >= m_fMaxTime)
	{

		//アイコンを描画しないように設定する
		for (int nCnt = 0; nCnt < 2; nCnt++)
		{
			if (m_pIcon[nCnt])
			{
				m_pIcon[nCnt]->SetDraw(false);
			}
		}

		nTime = 0;			//残っている時間を0にする
		m_bEnd = true;		//終了したにする
	}

	//数値の桁のテクスチャを更新する
	for (int nCnt = 0; nCnt < DEFAULT_TIMER_DIGIT; nCnt++)
	{
		if (m_pTimer[nCnt])
		{
			int num = nTime % nNum;
			nTime /= 10;
			nNum *= 10;

			m_pTimer[nCnt]->SetTex(0, D3DXVECTOR2(0.1f * num, 0.0f), D3DXVECTOR2(0.1f + 0.1f * num, 1.0f));
		}
	}
}

//アイコンアニメーション
void CSecurityTimer::IconUpdate()
{
	m_nCntAnim++;		//アニメーションカウンターをインクリメントする

	for (int nCnt = 0; nCnt < 2; nCnt++)
	{
		if (m_pIcon[nCnt])
		{//nullチェック

			//30フレームごと描画フラグを切り替える
			if (m_nCntAnim % 60 == 30)
			{
				m_pIcon[nCnt]->SetDraw(false);
			}
			else if (m_nCntAnim % 60 == 0)
			{
				m_pIcon[nCnt]->SetDraw(true);
			}
		}
	}
}
