//============================
//
// テキスト表示するべんり
// Author:hamada ryuuga
//
//============================

#include "text.h"
#include "manager.h"
#include "renderer.h"
#include "utility.h"
#include "input.h"
#include "application.h"
#include <Shlwapi.h>


CFont::FONT	CText::m_font = (CFont::FONT)0;			//ディフォルトのフォント


//=============================================================================
// コンストラクタ関数
//=============================================================================
CText::CText(int list) : CPolygon2D(list)
{
}

//=============================================================================
// デストラクタ関数
//=============================================================================
CText::~CText()
{
}

//=============================================================================
// 初期化関数
//=============================================================================
HRESULT CText::Init()
{
	m_newlineCount = 0;
	m_wordsPopCount = 0;
	m_wordsPopCountX = 0;
	m_AddCount = 0;
	m_Text = "";
	CPolygon2D::Init();
	m_fontSize = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	CPolygon2D::SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	//SetTex(D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
	m_col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	m_isRelease = false;
	m_DesTimar = 0;
	m_func = nullptr;

	return S_OK;
}

//=============================================================================
// 破棄関数
//=============================================================================
void CText::Uninit()
{
	for (int wordsCount = 0; wordsCount < m_TextSize; wordsCount++)
	{
		if (m_words[wordsCount] != nullptr)
		{
			m_words[wordsCount]->Uninit();
		}
	}

	m_words.clear();

	CPolygon2D::Uninit();
}

//=============================================================================
// 更新関数
//=============================================================================
void CText::Update()
{
	CPolygon2D::Update();

	D3DXVECTOR3 Pos= GetPos();
	m_AddCount++;
	if (m_AddCount >= m_Addnumber)
	{
		WordList();
	}

	if (m_isRelease)
	{
		//消える設定
		m_DesTimar--;

		if (m_DesTimar <= 0)
		{
			if (m_func != nullptr)
			{
				m_func();
			}
			Uninit();
		}
	}
	else
	{//エンター感知したら削除
		//CInput *CInputpInput = CInput::GetKey();
		//if (CInputpInput->Trigger(CInput::KEY_SHOT))
		{
		//	Uninit();
		}
	}
}

//=============================================================================
// 描画関数
//=============================================================================
void CText::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_ALPHAREF, 0);
	pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);

	CPolygon2D::Draw();

	// 新規深度値 <= Zバッファ深度値 (初期設定)
	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);

	// αテストを無効に戻す
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

//=============================================================================
// 生成関数
//=============================================================================
CText *CText::Create(D3DXVECTOR3 SetPos, D3DXVECTOR3 SetSize, Type talkType, int DeleteTime, int SpeedText, const char * Text, D3DXVECTOR3 FontSize, D3DXCOLOR col,CFont::FONT Type,  int wordsPopCountX, bool Nottimerdelete)
{
	CText * pObject = nullptr;
	pObject = new CText(PRIORITY_LEVEL4);

	if (pObject != nullptr)
	{
		pObject->SetCountXfast(wordsPopCountX);
		pObject->Init();

		switch (talkType)
		{
		case CText::GON:
			//pObject->SetTexture(CTexture::TEXTURE_GONBOX);
			break;
		case CText::MASUO:
			//pObject->SetTexture(CTexture::TEXTURE_NONE);
			break;
		case CText::MAX:
			//pObject->SetTexture(CTexture::TEXTURE_NONE);
			pObject->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			break;
		default:
			break;
		}

		pObject->SetPos(SetPos);
		pObject->SetSize(SetSize);
		pObject->SetFontSize(FontSize);
		pObject->SetFont(m_font);
		pObject->Releasetimer(DeleteTime);
		pObject->TextLetter(Text, SpeedText);
		pObject->SetCol(col);
		pObject->SetRelease(Nottimerdelete);
	}

	return pObject;
}


//=============================================================================
// けすまでの時間設定関数
//=============================================================================
void CText::Releasetimer(int nTimar)
{
	m_DesTimar = nTimar;
	m_DesTimarMax = m_DesTimar;
	m_isRelease = true;
}

//=============================================================================
// けすまでの時間設定関数
//=============================================================================
void CText::TextLetter(const char * Text, int SpeedText)
{
	m_ALLText = Text;
	m_TextSize = m_ALLText.size();
	m_Addnumber = SpeedText;
	m_AddLetter = 0;
	m_words.resize(m_TextSize);
}

//=============================================================================
// テクスチャ変更
//=============================================================================
void CText::TexChange(const char * Text)
{
	for (int wordsCount = 0; wordsCount < m_TextSize; wordsCount++)
	{
		if (m_words[wordsCount] != nullptr)
		{
			m_words[wordsCount]->Uninit();
		}
	}

	m_words.clear();

	m_ALLText = Text;
	m_TextSize = m_ALLText.size();
	m_AddLetter = 0;
	m_wordsPopCount = 0;
	m_wordsPopCountX = 0;
	m_wordsPopCount = 0;
	m_AddCount++;
	m_words.resize(m_TextSize);

	for (int count = 0; count < m_TextSize; count++)
	{
		WordList();
	}

}

//=============================================================================
// 色変更
//=============================================================================
void CText::SetTextCol(D3DXCOLOR col)
{

	for (int count = 0; count < m_wordsPopCount; count++)
	{
		m_words[count]->SetColar(col);
	}

}

//=============================================================================
// 消滅する時のイベント設定
//=============================================================================
void CText::Setfunc(std::function<void(void)> func)
{
	m_func = func;
}

//=============================================================================
// 文字を１こ出す
//=============================================================================
void CText::WordList()
{
	D3DXVECTOR3 Pos = GetPos();
	if (m_AddLetter < m_TextSize)
	{
		m_Text += m_ALLText[m_AddLetter];
		std::string Txt = m_Text;
		if (Txt != "")
		{//空白チェック
			if (hmd::is_sjis_lead_byte(m_ALLText[m_AddLetter])
				&& m_AddLetter < m_TextSize)
			{
				//文字を一文字入れる
				m_AddLetter++;
				m_Text += m_ALLText[m_AddLetter];
				m_AddLetter++;
				m_words[m_wordsPopCount] = CWords::Create(m_Text.c_str(),
					D3DXVECTOR3(Pos.x + ((m_fontSize.x * 2) * (m_wordsPopCountX + 1)), Pos.y + m_newlineCount*(m_fontSize.y * 2), Pos.z),
					D3DXVECTOR3(m_fontSize),
					m_FontType);
				m_words[m_wordsPopCount]->SetColar(m_col);
				m_wordsPopCount++;
				m_wordsPopCountX++;

			}
			else
			{
				if (m_Text != "\n")
				{	//じゃなかったとき
					m_AddLetter++;
					m_words[m_wordsPopCount] = CWords::Create(m_Text.c_str(),
						D3DXVECTOR3(Pos.x + ((m_fontSize.x * 2) * (m_wordsPopCountX + 1)), Pos.y + m_newlineCount*(m_fontSize.y * 2), Pos.z),
						D3DXVECTOR3(m_fontSize),
						m_FontType);
					m_words[m_wordsPopCount]->SetColar(m_col);
					m_wordsPopCount++;
					m_wordsPopCountX++;
				}
				else
				{//改行コードだったとき
					m_wordsPopCountX = m_wordsPopCountXfast;
					m_AddLetter++;
					m_newlineCount++;
				}
			}

		}
	}
	m_Text = "";
	m_AddCount = 0;
}