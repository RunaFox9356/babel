//=============================================================================
//
// carAnimation.h
// Author : Ricci Alex
//
//=============================================================================
#ifndef _DOOR_ANIMATION_H_		// このマクロ定義がされてなかったら
#define _DOOR_ANIMATION_H_		// 二重インクルード防止のマクロ定義

//=============================================================================
// インクルード
//=============================================================================
#include "endAnimation.h"

//=============================================================================
// 前方宣言
//=============================================================================


class CDoorAnimation : public CEndAnimation
{
public:
	CDoorAnimation();						//コンストラクタ
	~CDoorAnimation() override;				//デストラクタ

	HRESULT Init() override;				//初期化
	void Uninit() override;					//終了
	void Update() override;					//更新

	static CDoorAnimation* Create(D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f));		//生成処理

private:

	void CalcSpeed();						//速度の計算処理
	void UpdateState();						//状態の更新処理

private:

	enum EAnimState
	{
		STATE_DRONE_OUT = 0,
		STATE_DRONE_IN,
		STATE_ESCAPE,

		STATE_MAX
	};

	static const D3DXVECTOR3	DEFAULT_SPEED;		//ディフォルトの速度


	D3DXVECTOR3					m_speed;			//速度
	int							m_nDelay;			//ディレイ

	EAnimState					m_animState;		//アニメーションの状態

};

#endif