//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "map.h"
#include "sound.h"
#include "debug_proc.h"
#include "super.h"
#include "mesh.h"
#include "model_obj.h"
#include "collision_rectangle3D.h"
#include "application.h"
#include "locker.h"
#include "PC.h"
#include "GimmicDoor.h"
#include "EscapePoint.h"
#include "hostage.h"
#include "carry_player.h"
#include "walkingEnemy.h"
#include "GimmicDoor.h"
#include "PC.h"
#include "locker.h"
#include "HackingObj.h"
#include "wall.h"
#include "securityCamera.h"
#include "staticEnemy.h"
#include "sleepingEnemy.h"
#include "hostage.h"
#include "EscapePoint.h"
#include "scene_mode.h"
#include "gun.h"
#include "model3D.h"
#include "jammingTool.h"
#include "hackingdoor.h"
#include "stealObj.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMap::CMap() : CManager(),
m_nMapIdx(0)
{

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMap::~CMap()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMap::Init()
{
	m_itemId = 0;

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMap::Uninit()
{
	//親クラスの終了処理
	CSuper::Release();

	//マップデータのポインタをクリアする
	CApplication::GetInstance()->GetMapDataManager()->ClearAllPointers(m_nMapIdx);
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMap::Update()
{
	
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMap::Draw()
{
}

////--------------------------------------------------
//// 生成
////--------------------------------------------------
//CMap* CMap::Create()
//{
//	CMap* pObject2D;
//	pObject2D = new CMap();
//
//	if (pObject2D != nullptr)
//	{
//		pObject2D->Init();
//
//	}
//	else
//	{
//		assert(false);
//	}
//
//	return pObject2D;
//}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMap * CMap::Create(int nMapIdx)
{
	//インスタンスの生成
	CMap* pMap;				
	pMap = new CMap();		

	if (pMap != nullptr)
	{//nullチェック
		pMap->Init();		//初期化処理

	}
	else
	{//生成できなかったら
		assert(false);
	}

	int nMax = CApplication::GetInstance()->GetMapDataManager()->GetMaxMap();

	//マップのインデックスを確認する
	if (nMapIdx < 0 || nMapIdx >= nMax)
		nMapIdx = 0;

	pMap->m_nMapIdx = nMapIdx;		//マップのインデックスの設定

	pMap->CreateMapObj();			//マップのオブジェクトの生成

	return pMap;					//生成したインスタンスを返す
}

//マップオブジェクトの生成
void CMap::CreateMapObj()
{
	CMapDataManager::MAP_DATA data = CApplication::GetInstance()->GetMapDataManager()->GetMapData(m_nMapIdx);

	//メッシュフィールドの生成
	for (int nCnt = 0; nCnt < (int)data.vMeshData.size(); nCnt++)
	{
		CreateMeshfieldFromData(data.vMeshData.data()[nCnt], m_nMapIdx);
	}
	//モデルの生成
	for (int nCnt = 0; nCnt < (int)data.vModelData.size(); nCnt++)
	{
		CreateModelFromData(data.vModelData.data()[nCnt], m_nMapIdx);
	}
	//エネミーの生成
	for (int nCnt = 0; nCnt < (int)data.vEnemyData.size(); nCnt++)
	{
		CreateEnemyFromData(data.vEnemyData.data()[nCnt], m_nMapIdx);
	}
	//ギミックの生成
	for (int nCnt = 0; nCnt < (int)data.vGimmickData.size(); nCnt++)
	{
		CreateGimmickFromData(data.vGimmickData.data()[nCnt], m_nMapIdx);
	}
	//壁の生成
	for (int nCnt = 0; nCnt < (int)data.vWallData.size(); nCnt++)
	{
		CreateWallFromData(data.vWallData.data()[nCnt], m_nMapIdx);
	}

	//一時的
	//CLocker* pLocker = CLocker::Create(D3DXVECTOR3(-100.0f, 0.0f, 100.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	//CGimmickPC* pPc = CGimmickPC::Create(D3DXVECTOR3(-300.0f, 0.0f, 100.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	//{
	//	CStaticEnemy* pEnemy = CStaticEnemy::Create(D3DXVECTOR3(500.0f, 0.0f, 1300.0f), D3DXVECTOR3(0.0f, D3DX_PI * 0.25f, 0.0f));

	//	if (pEnemy)
	//	{
	//		pEnemy->SetMotion("data/MOTION/motion.txt");
	//	}
	//}
	//{
	//	CSleepingEnemy* pEnemy = CSleepingEnemy::Create(D3DXVECTOR3(-300.0f, 0.0f, -500.0f));

	//	if (pEnemy)
	//	{
	//		pEnemy->SetMotion("data/MOTION/motion.txt");
	//	}
	//}
}

//読み込んだデータからメッシュフィールドを生成する
void CMap::CreateMeshfieldFromData(CMapDataManager::MESHFIELD_DATA data, const int nMapIdx)
{
	CMesh3D* pMesh = CMesh3D::Create();		//メッシュフィールドを生成する

	if (pMesh)
	{//生成出来たら

		pMesh->SetPos(data.pos);					//位置の設定
		pMesh->SetBlock(data.blockNum);				//ブロック数の設定
		pMesh->SetSize(data.fieldSize);				//フィールドサイズの設定
		pMesh->SetCol(data.color);					//メッシュフィールドの色の設定
		pMesh->LoadTex(data.nTextureIdx);			//テクスチャの設定
		pMesh->SetSplitTex(data.bSplitTexture);		//テクスチャを一枚貼るか、複数貼るかのフラグの設定
		pMesh->SetVertexHeight(data.fVtxHeight);	//頂点の高さの設定

		//生成したメッシュフィールドのポインタを保存する
		CApplication::GetInstance()->GetMapDataManager()->SetPointerToField(nMapIdx, data.nCreatedIdx, pMesh);
	}
}

//読み込んだデータからモデルを生成する
void CMap::CreateModelFromData(CMapDataManager::MODEL_DATA data, const int nMapIdx)
{
	//モデルインスタンスの生成
	CModelObj* pModel = nullptr;

	if (data.nEffectIdx == CModel3D::Render_Glass)
	{
		pModel = CModelObj::Create(CSuper::PRIORITY_LEVEL1);
	}
	else
	{
		pModel = CModelObj::Create();
	}

	if (pModel)
	{//nullチェック
		pModel->SetPos(data.pos);				//位置の設定
		pModel->SetRot(data.rot);				//回転角度の設定
		pModel->SetType(data.nType);			//モデル種類の設定
		pModel->SetRenderMode(data.nEffectIdx);	
		pModel->SetShadowDraw(data.bShadow);	//影を描画するかどうかのフラグの設定
		pModel->SetIgnoreDistance(data.bIgnoreDistance);		//描画の時、距離を無視するかどうかの設定
		
		//当たり判定がない場合、このままで終わる
		if (!data.bHitbox)
			return;

		//当たり判定の取得
		CCollision_Rectangle3D* pCollision = pModel->GetCollision();

		if (pCollision)
		{//nullチェック
			pCollision->SetPos(data.hitboxPos);			//当たり判定の相対位置の設定
			pCollision->SetSize(data.hitboxSize);		//当たり判定のサイズの設定
		}

		//生成したモデルのポインタを保存する
		CApplication::GetInstance()->GetMapDataManager()->SetPointerToModel(nMapIdx, data.nCreatedIdx, pModel);
	}
}

//読み込んだデータからエネミーを生成する
void CMap::CreateEnemyFromData(CMapDataManager::ENEMY_DATA data, const int nMapIdx)
{
	switch (data.nType)
	{
	case CEnemy::EEnemyType::ENEMY_TYPE_PATROLLING:

	{//動くエネミー
		//エネミーインスタンスの生成
		CWalkingEnemy* pEnemy = CWalkingEnemy::Create(data.pos, data.vPatrollingPoint.data()[0]);

		if (pEnemy)
		{//nullチェック
			pEnemy->SetRot(data.rot);							//回転角度の設定

			CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

			//エフェクトをつける
			if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Default);
			}
			else if (mode == CApplication::MODE_HACKER)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Highlight);
			}

			//生成したエネミーのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToEnemy(nMapIdx, data.nCreatedIdx, pEnemy);
			CApplication::GetInstance()->GetMapDataManager()->SetToFindPointer(nMapIdx, CTask::HACKER_TASK_FIND_ENEMY, pEnemy);

		}
	}

		break;

	case CEnemy::EEnemyType::ENEMY_TYPE_SLEEPING:

	{//眠っているエネミー
		//エネミーインスタンスの生成
		CSleepingEnemy* pEnemy = CSleepingEnemy::Create(data.pos, data.rot, data.fWakeRadius);

		if (pEnemy)
		{//nullチェック

			CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

																						//エフェクトをつける
			if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Default);
			}
			else if (mode == CApplication::MODE_HACKER)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Highlight);
			}

			//生成したエネミーのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToEnemy(nMapIdx, data.nCreatedIdx, pEnemy);
			CApplication::GetInstance()->GetMapDataManager()->SetToFindPointer(nMapIdx, CTask::HACKER_TASK_FIND_ENEMY, pEnemy);
		}
	}

		break;

	case CEnemy::EEnemyType::ENEMY_TYPE_STATIC:

	{//動かないエネミー
		//エネミーインスタンスの生成
		CStaticEnemy* pEnemy = CStaticEnemy::Create(data.pos, data.rot);

		if (pEnemy)
		{//nullチェック

			CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

																						//エフェクトをつける
			if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Default);
			}
			else if (mode == CApplication::MODE_HACKER)
			{
				pEnemy->SetRenderMode(CModel3D::Render_Highlight);
			}

			//生成したエネミーのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToEnemy(nMapIdx, data.nCreatedIdx, pEnemy);
			CApplication::GetInstance()->GetMapDataManager()->SetToFindPointer(nMapIdx, CTask::HACKER_TASK_FIND_ENEMY, pEnemy);
		}
	}

	break;


	default:
		break;
	}
}

//読み込んだデータからギミックを生成する
void CMap::CreateGimmickFromData(CMapDataManager::GIMMICK_DATA data, const int nMapIdx)
{
	switch (data.nType)
	{
	case CGimmick::GIMMICK_TYPE_DOOR:

	{
		bool bRot = false;

		if (data.nRot % 2 == 1)
			bRot = true;

		//ドアインスタンスの生成
		CGimmicDoor* pGimmick = CGimmicDoor::Create(bRot);

			if (pGimmick)
			{//nullチェック
				pGimmick->SetPos(data.pos);				//位置の設定
				pGimmick->SetPosDefault(data.pos);		//ディフォルトの位置の設定(アニメーションと当たり判定に必要)
				pGimmick->SetRenderMode(data.nEffectIdx);

				if (data.nModelType >= 0)
				{
					pGimmick->SetModelType(data.nModelType, bRot);
				}

				//生成したギミックのポインタを保存する
				CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
			}
	}

		break;

	case CGimmick::GIMMICK_TYPE_PC:

	{
		D3DXVECTOR3 fRot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		if (data.nRot % 4 == 1)
			fRot.y = D3DX_PI * 0.5f;
		else if (data.nRot % 4 == 2)
			fRot.y = D3DX_PI;
		else if (data.nRot % 4 == 3)
			fRot.y = D3DX_PI * 1.5f;

		//PCインスタンスの生成
		CGimmickPC* pGimmick = CGimmickPC::Create(data.pos);

		if (pGimmick)
		{//nullチェック
			pGimmick->SetRot(fRot);				//回転角度の設定
			//pGimmick->SetRenderMode(data.nEffectIdx);

			if (data.nModelType >= 0)
			{
				pGimmick->ChangeModel(data.nModelType);
			}

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
			CApplication::GetInstance()->GetMapDataManager()->SetToFindPointer(nMapIdx, CTask::HACKER_TASK_FIND_DESTROYABLE, pGimmick);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_LOCKER:

	{
		D3DXVECTOR3 fRot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

		if (data.nRot % 4 == 1)
			fRot.y = D3DX_PI * 0.5f;
		else if (data.nRot % 4 == 2)
			fRot.y = D3DX_PI;
		else if (data.nRot % 4 == 3)
			fRot.y = D3DX_PI * 1.5f;

		//ロッカーインスタンスの生成
		CLocker* pGimmick = CLocker::Create(data.pos, fRot);

		if (pGimmick)
		{//nullチェック

			pGimmick->SetRenderMode(data.nEffectIdx);

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_LOCKED_DOOR:

	{
		bool bRot = false;

		if (data.nRot % 2 == 1)
			bRot = true;

		//鍵がかかっているドアインスタンスの生成
		CGimmicCarry* pGimmick = CGimmicCarry::Create();

		if (pGimmick)
		{//nullチェック
			pGimmick->SetPos(data.pos);				//位置の設定
			//rot									//
			pGimmick->SetPosKey(data.extraPos);		//鍵の位置の設定
			pGimmick->SetRenderMode(data.nEffectIdx);

			if (data.nModelType >= 0)
			{
				pGimmick->SetModelType(data.nModelType);
			}

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_HACK_PC:

	{
		
		//ハッキング用のPCのインスタンスの生成
		CHackObj* pGimmick = CHackObj::Create();

		if (pGimmick)
		{//nullチェック
			pGimmick->SetPos(data.pos);			//位置の設定
			pGimmick->Rotate(data.nRot);		//向きの設定

			CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

			if (mode == CApplication::MODE_HACKER)
			{
				pGimmick->SetRenderMode(CModel3D::ERenderMode::Render_Highlight);
			}
			else
				pGimmick->SetRenderMode(data.nEffectIdx);

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_ESCAPE_POINT:

	{
		CEscapePoint* pEsc = CEscapePoint::Create(data.pos);

		if (pEsc)
		{
			pEsc->SetRenderMode(data.nEffectIdx);
		}
	}

	break;

	case CGimmick::GIMMICK_TYPE_HOSTAGE:

	{
		CHostage* pHostage = CHostage::Create(data.pos, data.rot);

		if (pHostage)
		{
			//pHostage->SetRenderMode(data.nEffectIdx);

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pHostage);
			CApplication::GetInstance()->GetMapDataManager()->SetToFindPointer(nMapIdx, CTask::HACKER_TASK_FIND_PRISONER, pHostage);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_GUN:

	{
		CGun* pGun = CGun::Create();			//銃の生成

		if (pGun)
		{//nullチェック

			pGun->SetPos(data.pos);					//位置の設定
			pGun->SetRot(data.rot);					//向きの設定
			pGun->SetRenderMode(data.nEffectIdx);	//シェーダーエフェクトの設定
		}
	}

	break;

	case CGimmick::GIMMICK_TYPE_JAMMING_TOOL:

	{
		CJammingTool* pGimmick = CJammingTool::Create(data.pos, data.rot);			//ジャミングツールの生成

		if (pGimmick)
		{//nullチェック

			pGimmick->SetRenderMode(data.nEffectIdx);	//シェーダーエフェクトの設定

			if (data.nModelType >= 0)
			{
				pGimmick->SetModelType(data.nModelType);
			}
		}
	}

	break;

	case CGimmick::GIMMICK_TYPE_HACKABLE_DOOR:
		
	{
		bool bRot = false;

		if (data.nRot % 2 == 1)
			bRot = true;

		//ドアインスタンスの生成
		CHackDoor* pGimmick = CHackDoor::Create(bRot);

		if (pGimmick)
		{//nullチェック
			pGimmick->SetPos(data.pos);				//位置の設定
			pGimmick->SetDoorPos(data.pos);			//ディフォルトの位置の設定(アニメーションと当たり判定に必要)
			pGimmick->SetRenderMode(data.nEffectIdx);

			if (data.nModelType >= 0)
			{
				pGimmick->SetModelType(data.nModelType, bRot);
			}

			//生成したギミックのポインタを保存する
			CApplication::GetInstance()->GetMapDataManager()->SetPointerToGimmick(nMapIdx, data.nCreatedIdx, pGimmick);
		}
	}

		break;

	case CGimmick::GIMMICK_TYPE_STEAL_OBJ:

	{
		int nIdx = CStealObj::DEFAULT_BELL_IDX;

		if (nMapIdx == 1)
			nIdx = CStealObj::DEFAULT_DOCUMENTS_IDX;

		CStealObj* pGimmick = CStealObj::Create(data.pos, nIdx);			//ジャミングツールの生成

		//if (pGimmick)
		//{//nullチェック

		//	pGimmick->SetRenderMode(data.nEffectIdx);	//シェーダーエフェクトの設定
		//}
	}

	break;

	default:
		break;
	}
}

//読み込んだデータから壁を生成する
void CMap::CreateWallFromData(CMapDataManager::WALL_DATA data, const int nMapIdx)
{
	CWall* pWall = CWall::Create(data.pos, (CWall::EWallType)data.nType, data.nRot);

	if (pWall)
	{
		if (data.bCamera)
		{
			D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			if (data.nRot == 1)
				rot.y = D3DX_PI * 0.5f;
			else if (data.nRot == 2)
				rot.y = D3DX_PI;
			else if (data.nRot == 3)
				rot.y = D3DX_PI * 1.5f;

			CSecurityCamera* pCamera = CSecurityCamera::Create(data.pos + data.cameraPos, rot);

			if (pCamera)
			{
				D3DXVECTOR3 R = pCamera->GetRot();
				R.x = data.fCameraInclination;

				pCamera->SetRot(R);
				data.pCamera = pCamera;

				if (data.bPc && data.nPcIdx > 0)
				{
					CGimmick* pGimmick = CApplication::GetInstance()->GetMapDataManager()->GetMapData(nMapIdx).vGimmickData.data()[data.nPcIdx].pGimmick;
					CHackObj* pPc = nullptr;

					if (pGimmick && pGimmick->GetObjType() == CObject::OBJETYPE_HACKOBJ)
					{
						pPc = dynamic_cast<CHackObj*>(pGimmick);
					}

					if (pPc)
						pCamera->SetHackablePc(pPc);
				}

				//生成した監視カメラのポインタを保存する
				CApplication::GetInstance()->GetMapDataManager()->SetPointerToWall(nMapIdx, data.nCreatedIdx, pCamera);
			}
		}

		//生成した壁のポインタを保存する
		CApplication::GetInstance()->GetMapDataManager()->SetPointerToWall(nMapIdx, data.nCreatedIdx, pWall);
	}
}


