//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "polygon3D.h"
#include "renderer.h"
#include "application.h"

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CPolygon3D * CPolygon3D::Create(void)
{
	// オブジェクトインスタンス
	CPolygon3D *pObject3D = nullptr;

	// メモリの解放
	pObject3D = new CPolygon3D;

	// メモリの確保ができなかった
	assert(pObject3D != nullptr);

	// 数値の初期化
	pObject3D->Init();

	// インスタンスを返す
	return pObject3D;
}

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CPolygon3D * CPolygon3D::Create(int nPriority)
{
	// オブジェクトインスタンス
	CPolygon3D *pObject3D = nullptr;

	// メモリの解放
	pObject3D = new CPolygon3D(nPriority);

	// メモリの確保ができなかった
	assert(pObject3D != nullptr);

	// 数値の初期化
	pObject3D->Init();

	// インスタンスを返す
	return pObject3D;
}


//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CPolygon3D::CPolygon3D(int nPriority) : CPolygon(nPriority),
m_bDraw(true)
{
	m_pVtxBuff = nullptr;								// 頂点バッファ
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CPolygon3D::~CPolygon3D()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CPolygon3D::Init()
{// レンダラーのゲット
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	//頂点バッファの生成
	pRenderer->GetDevice()->CreateVertexBuffer(sizeof(VERTEX_3D) * 4,		// 確保するバッファサイズ
		D3DUSAGE_WRITEONLY,
		FVF_MULTI_TEX_VTX_3D,														// 頂点ファーマット
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	// ポリゴン情報の設定
	m_zFunc = D3DCMP_LESS;							// Zテストの優先度
	m_nAlphaValue = 100;							// アルファテストの透過率
	m_bBillboard = false;							// ビルボードかどうか
	m_isPlaneState = false;

	// ポリゴンの初期化
	CPolygon::Init();

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CPolygon3D::Uninit()
{
	// 終了
	CPolygon::Uninit();

	//頂点バッファを破棄
	if (m_pVtxBuff != nullptr)
	{
		m_pVtxBuff->Release();

		m_pVtxBuff = nullptr;
	}

	// オブジェクト3Dの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CPolygon3D::Update()
{

}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CPolygon3D::Draw()
{// レンダラーのゲット

	if (!m_bDraw)
		return;

	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	// テクスチャポインタの取得
	//CTexture *pTexture = CApplication::GetTexture();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

	// 情報の取得
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();
	D3DXVECTOR3 size = GetSize();

	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxView;

	// ワールドマトリックスの初期化
	// 行列初期化関数(第一引数の[行列]を[単位行列]に初期化)
	D3DXMatrixIdentity(&m_mtxWorld);

	if (m_bBillboard)
	{// ビルボードである
		// ライトを無効
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		// ビューマトリックスの設定
		pDevice->GetTransform(D3DTS_VIEW, &mtxView);

		// カメラ逆行列を設定
		D3DXMatrixInverse(&m_mtxWorld, NULL, &mtxView);
		m_mtxWorld._41 = 0.0f;
		m_mtxWorld._42 = 0.0f;
		m_mtxWorld._43 = 0.0f;
	}
	else
	{// 向きの反映
		// 行列回転関数 (第一引数に[ヨー(y)ピッチ(x)ロール(z)]方向の回転行列を作成)
		D3DXMatrixRotationYawPitchRoll(&mtxRot, rot.y, rot.x, rot.z);

		// 行列掛け算関数 (第二引数 * 第三引数を第一引数に格納)
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);
	}

	// 位置を反映
	// 行列移動関数 (第一引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);						// 行列掛け算関数

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマット
	pDevice->SetFVF(FVF_MULTI_TEX_VTX_3D);

	if (!m_isPlaneState)
	{
		// Zテストを使用する
		pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
		pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);

		// Zテストの設定
		pDevice->SetRenderState(D3DRS_ZFUNC, m_zFunc);

		// αテストを使用する
		pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);

		// αテストの設定
		pDevice->SetRenderState(D3DRS_ALPHAREF, m_nAlphaValue);
		pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
	}

	// 描画
	CPolygon::Draw();

	// ライトを有効	
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// Zテストの終了
	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

	// αテストの終了
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 3D頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CPolygon3D::SetVtx()
{
	//頂点情報へのポインタを生成
	VERTEX_3D *pVtx;

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	D3DXVECTOR3 size = GetSize();
	size /= 2.0f;

	// 頂点座標
	pVtx[0].pos = D3DXVECTOR3(-size.x, size.y, 0.0f);
	pVtx[1].pos = D3DXVECTOR3(size.x, size.y, 0.0f);
	pVtx[2].pos = D3DXVECTOR3(-size.x, -size.y, 0.0f);
	pVtx[3].pos = D3DXVECTOR3(size.x, -size.y, 0.0f);

	// 各頂点の法線の設定(*ベクトルの大きさは1にする必要がある)
	pVtx[0].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[1].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[2].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	pVtx[3].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// 頂点カラーの設定
// Author : 唐�ｱ結斗
// 概要 : 頂点カラーを設定する
//=============================================================================
void CPolygon3D::SetColor(const D3DXCOLOR &color)
{
	CPolygon::SetColor(color);

	// 色の取得
	D3DXCOLOR col = GetColor();

	//頂点情報へのポインタを生成
	VERTEX_3D *pVtx;

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// 頂点カラーの設定
	pVtx[0].col = col;
	pVtx[1].col = col;
	pVtx[2].col = col;
	pVtx[3].col = col;

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// テクスチャ座標の設定
// Author : 唐�ｱ結斗
// 概要 : 3Dオブジェクトのテクスチャ座標を設定する
//=============================================================================
void CPolygon3D::SetTex(int nTex, const D3DXVECTOR2 &minTex, const D3DXVECTOR2 &maxTex)
{
	//頂点情報へのポインタを生成
	VERTEX_3D *pVtx;

	//頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// テクスチャ座標の設定
	pVtx[0].tex[nTex] = D3DXVECTOR2(minTex.x, minTex.y);
	pVtx[1].tex[nTex] = D3DXVECTOR2(maxTex.x, minTex.y);
	pVtx[2].tex[nTex] = D3DXVECTOR2(minTex.x, maxTex.y);
	pVtx[3].tex[nTex] = D3DXVECTOR2(maxTex.x, maxTex.y);

	//頂点バッファをアンロック
	m_pVtxBuff->Unlock();
}
