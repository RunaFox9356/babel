//=============================================================================
//
// stealObj.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "stealObj.h"
#include "model_obj.h"
#include "collision_rectangle3D.h"
#include "emptyObj.h"
#include "UImessage.h"
#include "application.h"
#include "input.h"
#include "task.h"
#include "game.h"
#include "agent.h"


//=============================================================================
//								静的変数の初期化
//=============================================================================
const int			CStealObj::DEFAULT_BELL_IDX = 232;									//ディフォルトのモデルインデックス
const int			CStealObj::DEFAULT_DOCUMENTS_IDX = 234;								//ディフォルトのモデルインデックス
namespace
{
	const int E_BUTTON_TEXTURE_IDX = 44;			//Eボタンのテクスチャのインデックス
};



//コンストラクタ
CStealObj::CStealObj() : m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_pModel(nullptr),
m_pUiMessage(nullptr)
{

}

//デストラクタ
CStealObj::~CStealObj()
{

}

// 初期化
HRESULT CStealObj::Init()
{
	{
		//モデルを生成する
		m_pModel = CModelObj::Create();

		if (m_pModel)
		{
			m_pModel->SetShadowDraw(false);
		}
	}

	CTask* pTask = nullptr;

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();		//モードの取得

	if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
	{
		pTask = CGame::GetTask();			//タスクの取得
		SetRenderMode(1);
	}
	else
	{
		SetRenderMode(0);
	}

	if (pTask)
	{//nullチェック

	 //タスクの更新
		if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
			pTask->AddTask(CTask::AGENT_TASK_STEAL);
	}

	return S_OK;
}

// 終了
void CStealObj::Uninit()
{
	//モデルの破棄
	if (m_pModel)
	{
		m_pModel->Uninit();
		m_pModel = nullptr;
	}

	//UIメッセージの破棄
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}

	//メモリの解放
	Release();
}

// 更新
void CStealObj::Update()
{
	//エージェントモードではなかったら、更新する必要がない
	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (mode != CApplication::MODE_AGENT && mode != CApplication::MODE_GAME)
		return;

	//インプットを取得する
	CInput* pInput = CApplication::GetInstance()->GetInput();
	D3DXVECTOR3 pos = GetPos(), playerPos = CGame::GetPlayer()->GetPos(), dist = playerPos - pos;

	if (D3DXVec3Length(&dist) <= 100.0f)
	{//プレイヤーと当たっている場合

		if (pInput->Trigger(DIK_E))
		{//Eボタンを押したら

			CTask* pTask = CGame::GetTask();			//タスクの取得

			if (pTask)
			{//nullチェック

				//タスクの更新
				pTask->SubtractTask(CTask::AGENT_TASK_STEAL);
			}

			Uninit();
		}
		else if(!m_pUiMessage)
		{
			CreateUiMessage("\"Borrow\"", E_BUTTON_TEXTURE_IDX);
		}
	}
	else if (m_pUiMessage)
	{//UIメッセージの破棄
		DestroyUiMessage();
	}
}

// 描画
void CStealObj::Draw()
{

}

// 位置のセッター
void CStealObj::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;

	//モデルとヒットボックス用のオブジェクトの位置の設定
	if (m_pModel)
	{
		m_pModel->SetPos(pos);
	}
}

// 向きのセッター
void CStealObj::SetRot(const D3DXVECTOR3 &rot)
{
	if (m_pModel)
	{
		m_pModel->SetRot(rot);
	}
}

// 位置のゲッター
D3DXVECTOR3 CStealObj::GetPos()
{
	return m_pos;
}

// 向きのゲッター
D3DXVECTOR3 CStealObj::GetRot()
{
	D3DXVECTOR3 rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	//モデルが存在したら、向きを取得する
	if (m_pModel)
	{
		rot = m_pModel->GetRot();
	}

	return rot;
}

//エフェクトのインデックスの設定
void CStealObj::SetRenderMode(int mode)
{
	if (m_pModel)
		m_pModel->SetRenderMode(mode);
}

//マップに表示されているかどうかの設定処理
void CStealObj::SetIsOnMap(const bool bOnMap)
{
	CGimmick::SetIsOnMap(bOnMap);

	CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();

	if (!bOnMap)
		return;

	if (mode == CApplication::MODE_HACKER)
		SetRenderMode(0);
	else if (mode == CApplication::MODE_AGENT || mode == CApplication::MODE_GAME)
		SetRenderMode(1);
}



//=============================================================================
//
//									静的関数
//
//=============================================================================



//生成
CStealObj* CStealObj::Create(const D3DXVECTOR3 pos, const int nModelIdx)
{
	CStealObj* pObj = new CStealObj;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理
		return nullptr;
	}

	pObj->SetPos(pos);			//位置の設定

	if (pObj->m_pModel)
	{//nullチェック
		pObj->m_pModel->SetType(nModelIdx);		//モデルの種類の設定
	}

	return pObj;
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



//UIメッセージの生成処理
void CStealObj::CreateUiMessage(const char* pMessage, const int nTexIdx)
{
	int nMax = CApplication::GetInstance()->GetTexture()->GetMaxTexture();
	int nNum = nTexIdx;

	if (nTexIdx < -1 || nTexIdx >= nMax)
		nNum = 0;

	DestroyUiMessage();

	m_pUiMessage = CUiMessage::Create(D3DXVECTOR3(800.0f, 380.0f, 0.0f), D3DXVECTOR3(40.0f, 40.0f, 0.0f), nNum, pMessage, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
}

//UIメッセージの破棄処理
void CStealObj::DestroyUiMessage()
{
	if (m_pUiMessage)
	{
		m_pUiMessage->Uninit();
		m_pUiMessage = nullptr;
	}
}