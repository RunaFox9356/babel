//=============================================================================
//
// ドローンクラス(drone.h)
// Author : 有田　明玄
// 概要 : ドローン生成を行う
//
//=============================================================================
#ifndef _DRONE_H_			// このマクロ定義がされてなかったら
#define _DRONE_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// ライブラリーリンク
//*****************************************************************************
#pragma comment(lib,"d3d9.lib")			// 描画処理に必要
#pragma comment(lib,"d3dx9.lib")		// [d3d9.lib]の拡張ライブラリ

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <Windows.h>
#include "d3dx9.h"							// 描画処理に必要
#include "player.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMove;
class CUdp_Socket;
class CPolygon2D;
class CViewField;
class CText;
class CRespawnLoading;
class CModelObj;
//=============================================================================
// プレイヤークラス
// Author : 唐�ｱ結斗
// 概要 : プレイヤー生成を行うクラス
//=============================================================================
class CDrone : public CPlayer
{
public:
	//--------------------------------------------------------------------
	// 定数定義
	//--------------------------------------------------------------------
	static const float fSPEED;	//加速度
	static const int   HACKING_STROKE;			// ハッキングできる間合い
	//--------------------------------------------------------------------
	// プレイヤーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_TYPE
	{
		// 通常
		NEUTRAL_ACTION = 0,		// ニュートラル
		MOVE_ACTION,			// 移動
		MAX_ACTION,				// 最大数
	};

	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CDrone *Create();			// プレイヤーの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	CDrone();
	~CDrone();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	virtual HRESULT Init();											// 初期化
	void Uninit() override;											// 終了
	void Update() override;											// 更新
	void Draw() override;											// 描画
	int GetShockRecastTime() { return m_nShockTimer; };
	int GetHealRecastTime() { return m_nHealTimer; };
	CViewField* GetView() { return m_View; };
	const int GetLife() { return m_nLife; }							//ライフの取得
	const bool GetHackingState() { return m_bHacking; }				//ハッキング状態のゲッター

	void SetSendingState(const bool bSend);							//データを送信済みであるかどうかのセッター
	void SetHackingState(const bool bHack) { m_bHacking = bHack; }	//ハッキング状態のセッター
	void SetControl(bool bCnt) { m_bControl = bCnt; };
private:
	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	D3DXVECTOR3 Flying();	//移動
	void Shot(int nType);			//射撃
	void LookForTargets();		//破壊できるPCを探す処理
	void SendData();			//データの送信処理
	void CollisionCheck();		//当たり判定
	void CreateMessage();		//UIメッセージの生成
	void DestroyMessage();		//UIメッセージの破棄
	bool Respawn();				//リスポーンの処理
	void UpdateStaticNoise();	//スノーノイズの更新

	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------

	static const int			DEFAULT_LIFE;					// ディフォルトのライフ
	static const int			DEFAULT_INVULNERABILITY;		//ディフォルトの無敵状態のフレーム数
	static const int			DEFAULT_RESPAWN_FRAMES;			//リスポーンに必要な時間
	static const float			DEFAULT_MAX_HEIGHT;				//最大のY座標
	static const D3DXVECTOR3	DEFAULT_PARTS_RELATiVE_POS[4];	//モデルパーツの相対位置


	ACTION_TYPE				m_EAction;					// アクションタイプ
	int						m_nNumMotion;				// 現在のモーション番号
	CUdp_Socket*				m_socket;
	int						m_nLife;					// 体力
	int						m_nInvulnerability;			// 無敵状態カウンター
	int						m_nShockTimer;				// ショックタイマー
	int						m_nHealTimer;				// 回復タイマー
	float					m_fSpeedY;					// Y座標の速度
	CPolygon2D*				m_pSite;					// サイト
	CViewField*				m_View;						// 視野
	bool					m_bSendingData;				// データを送信中であるかどうか
	bool					m_bHacking;					// 監視カメラをハッキングしているかどうか
	CText*					m_pMessage;					// UIメッセージ
	CRespawnLoading*		m_pRespawnUi;				// リスポーンUI
	bool					m_bControl;
	std::vector<int>		m_vDataToSend;				// まだデータを送信していない見つかったものデータインデックス
	CPolygon2D*				m_pStaticUi;				// スノーノイズのUI
	CModelObj*				m_pParts[4];				// モデルのパーツ
	CPolygon2D*				m_pCommandUI;				// 操作UIへのポインタ
};

#endif

#pragma once
