#ifndef _PARTICLE_MANAGER_H_
#define _PARTICLE_MANAGER_H_

#include "particle.h"
#include "particle_emitter.h"
#include <fstream>
#include <sstream>

class CParticle;
class CParticleEmitter;

class CParticleManager
{
public:
	struct SBundleData
	{
		CParticle::SInfo			   info;
		CParticleEmitter::SEmitterInfo emitterData;
	};

	CParticleManager();
	~CParticleManager();

	HRESULT Init();
	void ReleaseAll();
	void LoadJson(nlohmann::json& list);
	void LoadAll();
	void LoadText(std::string path, std::map<std::string, std::vector<SBundleData>, std::less<>>& data);

	void SetParticleData(std::string& str, std::vector<SBundleData>& info);

	std::vector<SBundleData> GetParticleData(std::string str);
	std::map<std::string, std::vector<SBundleData>, std::less<>> GetData() { return m_data; }

private:
	void LoadJsonBillboard(nlohmann::json& list, CParticleManager::SBundleData& data);

	std::map<std::string, std::vector<SBundleData>, std::less<>> m_data;
};

#endif