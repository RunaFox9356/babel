//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "polygon2D.h"
#include "object.h"
#include "renderer.h"
#include "application.h"
#include "Score.h"
#include "ranking.h"
#include "task.h"
#include "game.h"
#include "hacker.h"

//=============================================================================
//									静的変数の初期化
//=============================================================================

//エージェントのディフォルトタスクスコア
const int	CScore::DEFAULT_AGENT_TASK_SCORE[CTask::AGENT_TASK_MAX] =
{
	300,				//AGENT_TASK_RESCUEのスコア
	50,				//AGENT_TASK_DESTROYのスコア
	50,				//AGENT_TASK_KILLのスコア
	200				//AGENT_TASK_STEALのスコア
};

//ハッカーのディフォルトタスクスコア
const int	CScore::DEFAULT_HACKER_TASK_SCORE[CTask::HACKER_TASK_MAX] =
{
	300,				//HACKER_TASK_HACK
	150,				//HACKER_TASK_FIND_PRISONER
	25,				//HACKER_TASK_FIND_DESTROYABLE
	25					//HACKER_TASK_FIND_ENEMY
};
//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CScore * CScore::Create(int ndigit)
{
	// オブジェクトインスタンス
	CScore *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CScore;

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CScore * CScore::Create(int nPriority,int ndigit)
{
	// オブジェクトインスタンス
	CScore *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CScore(nPriority);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->m_nDigit = ndigit;
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CScore::CScore(int nPriority/* = PRIORITY_LEVEL0*/) : CPolygon2D()
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CScore::~CScore()
{

}

//=============================================================================
// ポリゴンの初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CScore::Init()
{
	if (FAILED(CPolygon2D::Init()))
		return E_FAIL;

	m_sendAddScore = 0;
	m_pNumber = CNumber::Create();
	m_pNumber->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	ResetScore();
	return S_OK;
}

//=============================================================================
// ポリゴンの終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CScore::Uninit()
{
	CRanking* pRank = CApplication::GetInstance()->GetRanking();

	if (pRank)
	{
		CTask* pTask = nullptr;
		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
		bool bAgent = false;
		std::vector<int> vTask;

		if (mode != CApplication::MODE_HACKER)
		{
			bAgent = true;
			pTask = CGame::GetTask();

			if (pTask)
			{
				int* pAllTask = pTask->GetAllTaskObjective();
				int* pMaxTask = pTask->GetAllTaskObjectiveMax();

				for (int nCnt = 0; nCnt < CTask::AGENT_TASK_MAX; nCnt++)
				{
					vTask.push_back(pMaxTask[nCnt] - pAllTask[nCnt]);
				}
			}
		}
		else
		{
			pTask = CHacker::GetTask();
			int* pMaxTask = pTask->GetAllHackerTaskObjectiveMax();

			if (pTask)
			{
				int* pAllTask = pTask->GetHackerAllTaskObjective();

				for (int nCnt = 0; nCnt < CTask::HACKER_TASK_MAX; nCnt++)
				{
					vTask.push_back(pMaxTask[nCnt] - pAllTask[nCnt]);
				}
			}
		}
		
		pRank->SaveCompletedTask(vTask, bAgent);
	}

	CPolygon2D::Uninit();
}

//=============================================================================
// ポリゴンの更新
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの更新を行う
//=============================================================================
void CScore::Update()
{
}

//=============================================================================
// ポリゴンの描画
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの描画を行う
//=============================================================================
void CScore::Draw()
{
}

//=============================================================================
// 座標の設定
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトの座標を設定する
//=============================================================================
void CScore::SetPos(D3DXVECTOR3 pos)
{
	if (m_pNumber)
		m_pNumber->SetPos(pos - D3DXVECTOR3((m_pNumber->GetDigitSize().x), 0.0f, 0.0f));
}

//=============================================================================
// 大きさの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトの座標を設定する
//=============================================================================
void CScore::SetSize(D3DXVECTOR3 size)
{
	if (m_pNumber)
		m_pNumber->SetSize(size);
}

//=============================================================================
// スコア
// Author : 有田明玄
// 概要 : 時間の処理とそれに伴うポリゴンの調整
//=============================================================================
void CScore::AddScore(int add)
{
	//スコア加算
	m_nDigit = 0;
	m_sendAddScore += add;
	m_nScore += add;
	if (m_pNumber != nullptr)
	{
		SetDigit();
		m_pNumber->SetNumber(m_nScore);//10以上の位
	}
}

void CScore::SetScore(int add)
{
	//スコア設定
	m_nDigit = 0;
	m_nScore = add;
	SetDigit();
	m_pNumber->SetNumber(m_nScore);//10以上の位
}

//=============================================================================
// リセット
// Author : 有田明玄
// 概要 : スコアのリセット
//=============================================================================
void CScore::ResetScore()
{
	//スコア加算
	m_nScore = 0;
	SetDigit();
	if (m_pNumber)
	{
		m_pNumber->SetNumber(0);//10以上の位
		m_pNumber->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
}

int CScore::GetSendScore()
{
	int sendScore = m_sendAddScore;
	ReSetSendScore();
	return sendScore;
}

void CScore::SetDigit()
{
	int i = 1;
	while (1)
	{
		int n = m_nScore / pow(10, i);
		if (n <= 0)
		{
			m_pNumber->SetDigit(i);
			break;
		}
		i++;
	}
}

// 桁のサイズのセッター
void CScore::SetDigitSize(const D3DXVECTOR3 size)
{
	if (m_pNumber)
	{
		m_pNumber->SetDigitSize(size);
	}
}
