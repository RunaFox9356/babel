//**************************************************
//
// 制作 ( loading )
// Author : hamada ryuuga
//
//**************************************************

#ifndef _LOADINGSCENE_H_
#define _LOADINGSCENE_H_

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "scene_mode.h"
#include"main.h"

class  CWords;
class CLoadingScene :public CSceneMode
{
public:
	CLoadingScene();
	~CLoadingScene();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

private:
	CWords * m_Words[10]; //NowLoading
	bool m_LoadingEnd;

	int m_timer;
	int m_timerCount = 30;
	int m_moveType;
	int m_Sin;
	

};

#endif
