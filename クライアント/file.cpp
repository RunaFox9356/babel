//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================
#include "file.h"

nl::json CFile::LoadJson(const wchar_t* fileName)
{
	std::ifstream ifs(fileName);
	if (ifs)
	{
		nlohmann::json list;

		ifs >> list;
		return list;
	}
	return nullptr;
}

//------------------------------------
//今あるオブジェクトの保存
//------------------------------------
void CFile::Savefile(const char * pFileName)
{
	auto jobj = m_savejson.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = pFileName;
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();
}

void CFile::SaveJson(nl::json Save, const char * pPassName)
{
	m_savejson = Save;

	Savefile(pPassName);
}
