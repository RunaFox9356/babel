//=============================================================================
//
// UIマネージャークラス(UImanager.cpp)
// Author : 唐�ｱ結斗
// 概要 : UIの生成
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include"UImanager.h"
#include "utility.h"
#include"polygon2D.h"
#include"Score.h"
#include"AgentUI.h"
#include"Timer.h"
#include "taskUI.h"
#include"number.h"
#include "agent.h"
#include "words.h"
#include "renderer.h"
#include "game.h"
#include "watch.h"
#include "hp_gauge.h"
#include "UImessage.h"
#include "application.h"

// ---------------------------------------------------------------------------- 
// 定数
//-----------------------------------------------------------------------------
const float CAgentUI::ITEM_NORMAL_SIZE = 80.0f;							// アイテム一覧のUIの選ばれていない時のサイズ
const float CAgentUI::ITEM_WIDTH = 40.0f;								// アイテムの設置間隔
const float CAgentUI::ITEM_DISTANCE_FROM_EDGE = 60.0f;					// 最初のアイテムの端からの位置
const int CAgentUI::NUMBER_0 = 27;										// テクスチャの0の番号
const float CAgentUI::TIMER_SIZE = 25.f;								// タイマーの大きさ
const D3DXVECTOR3 CAgentUI::SCORE_SIZE = D3DXVECTOR3(100.0f, 50.0f, 0.0f);		// スコア

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : ミニゲームを生成する
//=============================================================================
CAgentUI * CAgentUI::Create()
{
	// オブジェクトインスタンス
	CAgentUI *pUI = nullptr;

	// メモリの解放
	pUI = new CAgentUI;

	// メモリの確保ができなかった
	assert(pUI != nullptr);

	pUI->Init();

	// インスタンスを返す
	return pUI;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CAgentUI::CAgentUI() :m_pWatch(nullptr)
{
	m_pAgent = nullptr;
	m_pPolygonItem.clear();
	m_pWord.clear();
	m_nItemID = -1;
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CAgentUI::~CAgentUI()
{

}

//=============================================================================
// 初期化
// Author : 有田明玄
// 概要 : 初期化
//=============================================================================
HRESULT CAgentUI::Init()
{
	if (FAILED(CUIManager::Init()))
		return E_FAIL;

	int nMap = CApplication::GetInstance()->GetMap(), nMinimapIdx = 80;

	if (nMap == 1)
		nMinimapIdx = 122;
	else if (nMap == 2)
		nMinimapIdx = 139;

	//	ミニマップ
	m_pMinimap = LoadUI(
		UI_TYPE_MINIMAP,
		D3DXVECTOR3(1180.0f, 100.0f, 0.0f),
		D3DXVECTOR3(200.0f, 200.0f, 0.0f),
		nMinimapIdx);		

	//	残弾 (初期15弾)
	m_pBullets =(CNumber*) LoadUI(
		UI_TYPE_NUMBER,
		D3DXVECTOR3(1200.0f, 650.0f, 100.0f),
		D3DXVECTOR3(64.0f, 64.0f, 64.0f),
		78);	

	m_pBullets->SetDigit(2);
	m_pBullets->SetDigitSize(D3DXVECTOR3(30.0f, 60.0f, 0.0f));
	m_pBullets->SetNumber(0);
	m_pBullets->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_pBullets->SetCol(D3DXCOLOR(0.25f, 0.9f, 0.9f, 1.0f));


	////	HPバー
	//m_pHPGauge = (CHpGauge*)LoadUI(
	//	UI_TYPE_UI,
	//	D3DXVECTOR3(580 / 2, 600, 0.0f),
	//	SCORE_SIZE,
	//	27);				

	m_pTimer = (CTimer*)LoadUI(
		UI_TYPE_TIMER,
		D3DXVECTOR3(440, 60.0f, 0.0f),
		D3DXVECTOR3(60, 60, 0),
		-1);
	m_pTimer->SetMode(0);
	m_pTimer->SetTimer((60 * 60 * 15) + (60 * 10));//←分　→秒

	//スコア
	m_Score = (CScore*)LoadUI(
		UI_TYPE_SCORE,
		D3DXVECTOR3(1280.0f * 0.35f, (SCORE_SIZE.y * 0.5f) + 5.0f, 0.0f),
		SCORE_SIZE,
		-1);

	//	現在の武器
	m_pCurrentWeapons = LoadUI(
		UI_TYPE_UI,
		D3DXVECTOR3(1180.0f, 550.0f, 100.0f),
		D3DXVECTOR3(128.0f, 96.0f, 80.0f),
		79);
	m_pCurrentWeapons->SetColor(D3DXCOLOR(0.25f, 0.25f, 0.25f, 1.0f));

	//// アイテム関係のUI
	//for (int nCntPolygon = 0; nCntPolygon < CPlayer::nMaxItem; nCntPolygon++)
	//{
	//	CPolygon2D* pPolygonItem = CPolygon2D::Create();
	//	pPolygonItem->SetSize(D3DXVECTOR3(200.0f, 80.0f, 0.0f));
	//	D3DXVECTOR3 size = pPolygonItem->GetSize();
	//	pPolygonItem->SetPos(D3DXVECTOR3(CRenderer::SCREEN_WIDTH - size.x * 0.6f, CRenderer::SCREEN_HEIGHT - size.y * (CPlayer::nMaxItem - 1 - nCntPolygon) * 1.2f - size.y * 0.6f, 0.0f));
	//	m_pPolygonItem.push_back(pPolygonItem);
	//}

	m_pWatch = CWatch::Create();

	if (m_pWatch)
	{
		if (m_Score)
			m_Score->SetDigitSize(D3DXVECTOR3(15.0f, 30.0f, 0.0f));
		m_pWatch->AddObj(m_Score, D3DXVECTOR3(160.0f, -25.0f, 0.0f));
		if (m_pTimer)
			m_pTimer->SetDigitSize(D3DXVECTOR3(15.0f, 30.0f, 0.0f));
		m_pWatch->AddObj(m_pTimer, D3DXVECTOR3(15.0f, 0.0f, 0.0f));

		
	}

	return S_OK;
}

//=============================================================================
// 終了
// Author : 有田明玄
//=============================================================================
void CAgentUI::Uninit()
{
	
}

//=============================================================================
// 更新
// Author : 有田明玄
//=============================================================================
void CAgentUI::Update()
{
	if (m_pAgent == nullptr)
	{
		return;
	}

	/*int nItemID = m_pAgent->GetCurrentItem();

	if (m_nItemID != nItemID)
	{
		if (m_nItemID != -1)
		{
			m_pPolygonItem[m_nItemID]->SetSize(D3DXVECTOR3(200.0f, 80.0f, 0.0f));
		}
		
		m_pPolygonItem[nItemID]->SetSize(D3DXVECTOR3(200.0f * 1.2f, 80.0f * 1.2f, 0.0f));
		m_nItemID = nItemID;
	}*/
	LeftBulletUpdate();
}

//=============================================================================
// 描画
// Author : 有田明玄
//=============================================================================
void CAgentUI::Draw()
{
}

//スコアの加算処理
void CAgentUI::AddScore(const int nScore)
{
	if (m_Score)
	{
		m_Score->AddScore(nScore);
	}
}
//スコアの加算処理
void CAgentUI::SetScore(const int nScore)
{
	if (m_Score)
	{
		m_Score->SetScore(nScore);
	}
}

//Hpゲージの設定処理
void CAgentUI::SetHpGauge(CHpGauge * pGauge)
{
	if (m_pWatch && pGauge)
	{
		pGauge->SetRatio(0.8f);
		pGauge->SetMaxSize(D3DXVECTOR3(30.0f, 140.0f, 0.0f));
		pGauge->SetSize(D3DXVECTOR3(30.0f, 140.0f, 0.0f));
		m_pWatch->AddObj(pGauge, D3DXVECTOR3(0.0f, 35.0f, 0.0f));
	}
}



//=============================================================================
//
//								プライベート関数
//
//=============================================================================



// 残数弾の更新
void CAgentUI::LeftBulletUpdate()
{
	//プレイヤーの取得
	CAgent* pPlayer = CGame::GetPlayer();

	if (!pPlayer || !m_pBullets)
		return;

	m_pBullets->SetNumber(pPlayer->GetLeftBullet());
}


