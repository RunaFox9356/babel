//=============================================================================
//
// selectScreen.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "selectScreen.h"
#include "input.h"
#include "sound.h"
#include "renderer.h"
#include "camera.h"
#include "polygon2D.h"
#include "menuButton.h"
#include "menu.h"
#include "fade.h"
#include "loading.h"




//コンストラクタ
CSelectScreen::CSelectScreen() : m_bTransition(false),
m_pMenu(nullptr)
{

}

//デストラクタ
CSelectScreen::~CSelectScreen()
{

}

//初期化
HRESULT CSelectScreen::Init()
{
	m_Connect.myConnect = false;
	m_Connect.enemyConnect = false;
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();
	pSound->PlaySound(CSound::SOUND_LABEL_BGM000);

	{
		CPolygon2D* pBg = CPolygon2D::Create();

		if (pBg)
		{
			pBg->SetPos(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH * 0.5f, (float)CRenderer::SCREEN_HEIGHT * 0.5f, 0.0f));
			pBg->SetSize(D3DXVECTOR3((float)CRenderer::SCREEN_WIDTH, (float)CRenderer::SCREEN_HEIGHT, 0.0f));
			std::vector<int> nNumTex;
			nNumTex.push_back(51);
			pBg->LoadTex(nNumTex);
		}
	}

	CPolygon2D* pPolygon = CPolygon2D::Create();

	if (pPolygon)
	{
		pPolygon->SetPos(D3DXVECTOR3(640.0f, 100.0f, 0.0f));
		pPolygon->SetSize(D3DXVECTOR3(300.0f, 150.0f, 0.0f));
		std::vector<int> nNumTex;
		nNumTex.push_back(16);
		pPolygon->LoadTex(nNumTex);
	}

	m_pMenu = CMenu::Create();

	if (m_pMenu)
	{
		m_pMenu->SetBackgroundTexture(9);

		CMenuButton* pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, -75.0f, 0.0f));

		if (pButton)
		{
			pButton->SetTexture(52);
			m_pMenu->AddButton(pButton);
		}

		pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

		if (pButton)
		{
			pButton->SetTexture(53);
			m_pMenu->AddButton(pButton);
		}

		pButton = CMenuButton::Create(D3DXVECTOR3(0.0f, 75.0f, 0.0f));

		if (pButton)
		{
			pButton->SetTexture(18);
			m_pMenu->AddButton(pButton);
		}
	}

	//m_socket->Init();
	return S_OK;
}

//終了
void CSelectScreen::Uninit()
{
	CInput *pInput = CInput::GetKey();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// サウンド終了
	pSound->StopSound();

	// フォグの有効設定
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	// マウスカーソルを出す
	pInput->SetCursorErase(true);

	// カメラの追従設定
	CCamera *pCamera = CApplication::GetInstance()->GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// カメラの追従設定
	pCamera = CApplication::GetInstance()->GetMapCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	// スコアの解放
	Release();
}

// 更新
void CSelectScreen::Update()
{

	// サウンド情報の取得
	CSound *pSound = CApplication::GetInstance()->GetSound();

	// 入力情報の取得
	CInput *pInput = CInput::GetKey();

	
	if (m_Connect.myConnect&& m_Connect.enemyConnect)
	{//マッチングしたら遷移
		//CApplication::GetInstance()->SetNextMode(m_ConnectMode);
		CApplication::GetInstance()->SetSelectMode(m_ConnectMode);
		CApplication::GetInstance()->SetNextMode(CApplication::MODE_MAPSELECT);
	}
	else
	{

		if (pInput->Trigger(DIK_RETURN))
		{
			CApplication::GetInstance()->SetNextMode(m_ConnectMode);
		}
		
	}

	bool bFade = CApplication::GetInstance()->GetFade()->GetFadeSituation();

	if (!bFade && !m_bTransition && m_pMenu)
	{
		int nButton = m_pMenu->GetTriggeredButton();

		if (nButton >= 0 && nButton < BUTTON_MAX)
		{
			switch (nButton)
			{
			case BUTTON_GAME:
				SetTransition(CApplication::MODE_GAME);
				break;
			case BUTTON_HACKER:
				SetTransition(CApplication::MODE_HACKER);
				break;
			case BUTTON_TITLE:
				SetTransition(CApplication::MODE_TITLE);
				break;

			default:
				break;
			}
		}
	}
}

//遷移の設定
void CSelectScreen::SetTransition(CApplication::SCENE_MODE mode)
{
	CSound* pSound = CApplication::GetInstance()->GetSound();

	pSound->PlaySound(CSound::SOUND_LABEL_SE_DECIDE);
	
	m_ConnectMode = mode;
	
	std::thread ConnectOn([&] {m_Connect = CApplication::GetInstance()->ConnectTh(m_ConnectMode); });

	// スレッドをきりはなす
	ConnectOn.detach();
	CApplication::GetInstance()->SetSelectMode(mode);
	//CApplication::SetNextMode(mode);
	m_bTransition = true;
	CLoading*load = new CLoading;
	load->Init();

}
