#ifndef _MINGAME_RING_H_		// このマクロ定義がされてなかったら
#define _MINGAME_RING_H_		// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "polygon.h"
#include "texture.h"
#include"polygon2D.h"
#include "minigame.h"

class CText;
class CMinGame_Ring : public CMiniGame
{
public:
	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CMinGame_Ring(int nPriority = PRIORITY_LEVEL0);
	~CMinGame_Ring();

	HRESULT Init();														// 初期化
	void Uninit() ;															// 終了
	void Update() ;															// 更新
	void Draw() ;															// 描画

	static CMinGame_Ring *Create(int count);

protected:

private:

	D3DXVECTOR3 karapos;
	float add_x = 640;
	float add_y = 320;
	float m_PosX;		// 描画座標X
	float m_PosY;		// 描画座標Y
	float m_Radius;		// 半径(描画用)
	float m_CenterX;	// 中心座標X
	float m_CenterY;	// 中心座標Y
	float m_Angle;		// 角度
	float m_Length;		// 半径の長さ

	bool flg;

	CPolygon2D*	m_backGround;
	CPolygon2D*	m_pCommandUI;		//操作のUI
	CText*m_pText;
};
#endif
