//=============================================================================
//
// メッシュクラス(mesh2D.cpp)
// Author : 唐�ｱ結斗
// 概要 : メッシュ生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "calculation.h"
#include "mesh2D.h"
#include "renderer.h"
#include "application.h"
#include "debug_proc.h"
#include "boundingBox3D.h"
#include "utility.h"

//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dメッシュを生成する
//=============================================================================
CMesh2D * CMesh2D::Create(void)
{
	// メッシュインスタンス
	CMesh2D *pMesh3D = nullptr;

	// メモリの解放
	pMesh3D = new CMesh2D;

	// メモリの確保ができなかった
	assert(pMesh3D != nullptr);

	// 数値の初期化
	pMesh3D->Init();

	// インスタンスを返す
	return pMesh3D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CMesh2D::CMesh2D()
{
	m_pVtxBuff = nullptr;								// 頂点バッファ
	m_pIdxBuff = nullptr;								// インデックスバッファへのポインタ
	m_mtxWorld = {};									// ワールドマトリックス
	m_pos = D3DXVECTOR3(0.0f,0.0f,0.0f);				// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 向き
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 大きさ
	m_blockSize = D3DXVECTOR3(0.0f, 0.0f, 0.0f);		// ブロックサイズ
	m_tex = D3DXVECTOR2(1.0f, 1.0f);					// テクスチャ座標の基準値
	m_col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);			// カラー
	m_nNumTex = -1;										// テクスチャタイプ
	m_block = DOUBLE_INT(0, 0);							// ブロック数
	m_line = DOUBLE_INT(0, 0);							// 列数
	m_nLineVtxX = 0;									// 列頂点数(X)
	m_nVtx = 0;											// 頂点数
	m_nPolygon = 0;										// ポリゴン数
	m_nIndex = 0;										// インデックス数
	m_bSplitTex = false;								// 分割するかどうか
}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CMesh2D::~CMesh2D()
{

}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CMesh2D::Init()
{
	// メンバ変数の初期化
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 向き
	m_size = D3DXVECTOR3(100.0f, 0.0f, 100.0f);			// 大きさ
	m_block = DOUBLE_INT(2, 2);							// ブロック数

	// ブロック一つあたりの大きさの算出
	m_blockSize = D3DXVECTOR3(m_size.x / m_block.x, 0.0f, m_size.z / m_block.y);

	// 数値の計算
	SetMeshInfo();

	// 頂点座標などの設定
	SetVtx();

	// 色の設定
	SetCol(m_col);

	// テクスチャの設定
	SetTex();

	// インデックスの設定
	SetIndexBuff();

	// オブジェクトタイプの設定
	SetObjType(CObject::OBJETYPE_MESH);

	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CMesh2D::Uninit()
{
	//頂点バッファを破棄
	if (m_pVtxBuff != nullptr)
	{
		m_pVtxBuff->Release();

		m_pVtxBuff = nullptr;
	}

	// インデックスバッファの破棄	  
	if (m_pIdxBuff != NULL)
	{
		m_pIdxBuff->Release();

		m_pIdxBuff = nullptr;
	}

	// メッシュ3Dの解放
	Release();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CMesh2D::Update()
{
	
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 2D描画を行う
//=============================================================================
void CMesh2D::Draw()
{// レンダラーのゲット
	CRenderer *pRenderer = CApplication::GetInstance()->GetRenderer();

	// テクスチャポインタの取得
	CTexture *pTexture = CApplication::GetInstance()->GetTexture();

	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = pRenderer->GetDevice();

	//頂点バッファをデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_2D));

	// インデックスバッファをデータストリームに設定
	pDevice->SetIndices(m_pIdxBuff);

	//頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D);

	// テクスチャの設定
	pDevice->SetTexture(0, pTexture->GetTexture(m_nNumTex));

	// メッシュフィールド描画
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_nVtx, 0, m_nPolygon);

	// テクスチャの解除
	pDevice->SetTexture(0, nullptr);
}

//=============================================================================
// 位置のセッター
// Author : 唐�ｱ結斗
// 概要 : 位置のメンバ変数に引数を代入
//=============================================================================
void CMesh2D::SetPos(const D3DXVECTOR3 &pos)
{
	// 位置の設定
	m_pos = pos;

	// 頂点座標などの設定
	SetVtx();

	// 色の設定
	SetCol(m_col);

	// テクスチャの設定
	SetTex();

	// インデックスの設定
	SetIndexBuff();
}

//=============================================================================
// 向きのセッター
// Author : 唐�ｱ結斗
// 概要 : 向きのメンバ変数に引数を代入
//=============================================================================
void CMesh2D::SetRot(const D3DXVECTOR3 &rot)
{
	// 位置の設定
	m_rot = rot;

	// 頂点座標などの設定
	SetVtx();

	// 色の設定
	SetCol(m_col);

	// テクスチャの設定
	SetTex();

	// インデックスの設定
	SetIndexBuff();
}

//=============================================================================
// 大きさのセッター
// Author : 唐�ｱ結斗
// 概要 : 大きさのメンバ変数に引数を代入
//=============================================================================
void CMesh2D::SetSize(const D3DXVECTOR3 & size)
{
	// 大きさの設定
	m_size = size;

	// 数値の計算
	SetMeshInfo();

	// 頂点座標などの設定
	SetVtx();

	// 色の設定
	SetCol(m_col);

	// テクスチャの設定
	SetTex();

	// インデックスの設定
	SetIndexBuff();
}

//=============================================================================
// ブロック数の設定
// Author : 唐�ｱ結斗
// 概要 : ブロック数の設定
//=============================================================================
void CMesh2D::SetBlock(DOUBLE_INT block)
{
	m_block = block;

	// 数値の計算
	SetMeshInfo();

	// 頂点座標などの設定
	SetVtx();

	// 色の設定
	SetCol(m_col);

	// テクスチャの設定
	SetTex();

	// インデックスの設定
	SetIndexBuff();
}

//=============================================================================
// テクスチャ分割するかのセッター
// Author : 唐�ｱ結斗
// 概要 : テクスチャ分割するかの設定
//=============================================================================
void CMesh2D::SetSplitTex(bool bSplitTex)
{
	m_bSplitTex = bSplitTex;

	// テクスチャの設定
	SetTex();
}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 3D頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CMesh2D::SetVtx()
{
	// 頂点情報の取得
	VERTEX_2D *pVtx = NULL;

	// 頂点バッファをロック
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int nCntZ = 0; nCntZ < m_line.y; nCntZ++)
	{
		for (int nCntX = 0; nCntX < m_line.x; nCntX++)
		{// 変数宣言
			int nCntVtx = nCntX + (nCntZ *  m_line.x);

			// 頂点座標の設定
			pVtx[nCntVtx].pos.x = (nCntVtx % m_line.x * m_blockSize.x) - m_size.x / 2.0f;
			pVtx[nCntVtx].pos.y = (nCntVtx / m_line.x * m_blockSize.y) - m_size.y / 2.0f;
			pVtx[nCntVtx].pos.z = 0.0f;

			// ワールド座標にキャスト
			pVtx[nCntVtx].pos = CCalculation::WorldCastVtx(pVtx[nCntVtx].pos, m_pos, m_rot);

			// rhwの設定
			pVtx[nCntVtx].rhw = 1.0f;
		}
	}

	// 頂点バッファのアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// テクスチャ座標の設定
// Author : 唐�ｱ結斗
// 概要 : 3Dメッシュのテクスチャ座標を設定する
//=============================================================================
void CMesh2D::SetTex()
{
	// 頂点情報の取得
	VERTEX_2D *pVtx = NULL;

	// 頂点バッファをロック
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int nCntZ = 0; nCntZ < m_line.y; nCntZ++)
	{
		for (int nCntX = 0; nCntX < m_line.x; nCntX++)
		{// 変数宣言
			int nCntVtx = nCntX + (nCntZ *  m_line.x);

			if (m_bSplitTex)
			{// テクスチャ座標の設定
				pVtx[nCntVtx].tex = D3DXVECTOR2(1.0f * nCntX + m_tex.x, 1.0f * nCntZ + m_tex.y);
			}
			else
			{// テクスチャ座標の設定
				pVtx[nCntVtx].tex = D3DXVECTOR2(1.0f / m_block.x * nCntX + m_tex.x, 1.0f / m_block.y * nCntZ + m_tex.y);
			}	
		}
	}

	// 頂点バッファのアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// 色の設定
// Author : 唐�ｱ結斗
// 概要 : 3Dメッシュの色を設定する
//=============================================================================
void CMesh2D::SetCol(const D3DXCOLOR &col)
{
	m_col = col;

	// 頂点情報の取得
	VERTEX_2D *pVtx = NULL;

	// 頂点バッファをロック
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int nCntZ = 0; nCntZ < m_line.y; nCntZ++)
	{
		for (int nCntX = 0; nCntX < m_line.x; nCntX++)
		{// 変数宣言
			int nCntVtx = nCntX + (nCntZ *  m_line.x);

			// 頂点カラーの設定
			pVtx[nCntVtx].col = m_col;
		}
	}

	// 頂点バッファのアンロック
	m_pVtxBuff->Unlock();
}

//=============================================================================
// インデックスバッファの設定
// Author : 唐�ｱ結斗
// 概要 : インデックスバッファの数値を設定する
//=============================================================================
void CMesh2D::SetIndexBuff()
{
	// インデックスバッファをロック
	WORD *pIdx;
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	// インデックスの設定
	for (int nCntZ = 0; nCntZ < m_block.y; nCntZ++)
	{
		for (int nCntX = 0; nCntX < m_line.x; nCntX++)
		{// インデックス数の設定
			pIdx[0] = (WORD)(m_line.x + nCntX + nCntZ * m_line.x);
			pIdx[1] = (WORD)(m_line.x + nCntX + nCntZ * m_line.x - m_line.x);

			pIdx += 2;
		}

		if (nCntZ != m_block.y - 1)
		{// 最大数以下の時
		 // 縮退ポリゴンインデックス数の設定
			pIdx[0] = (WORD)(m_line.x * nCntZ + m_block.x);
			pIdx[1] = (WORD)(m_line.x * (nCntZ + 2));

			pIdx += 2;
		}
	}

	// インデックスバッファのアンロック
	m_pIdxBuff->Unlock();
}

//=============================================================================
// メッシュの数値の計算
// Author : 唐�ｱ結斗
// 概要 : ライン数,1列分の頂点数(X),頂点数,ポリゴン数,インデックス数の計算
//=============================================================================
void CMesh2D::SetMeshInfo()
{// デバイスのゲット
	LPDIRECT3DDEVICE9 pDevice = CApplication::GetInstance()->GetRenderer()->GetDevice();

	m_line = DOUBLE_INT(m_block.x + 1, m_block.y + 1);									// ライン数
	m_nLineVtxX = m_block.x * 2 + 2;													// 1列分の頂点数(X)
	m_nVtx = m_line.x * m_line.y;														// 頂点数
	m_nPolygon = (m_block.x * m_block.y * 2) + (((m_block.y - 1) * 2) * 2);				// ポリゴン数
	m_nIndex = (m_nLineVtxX * m_block.y) + ((m_block.y - 1) * 2);						// インデックス数

	// ブロック一つあたりの大きさの算出
	m_blockSize = D3DXVECTOR3(m_size.x / m_block.x, m_size.y / m_block.y, 0.0f);

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_2D) * m_nVtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_2D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	// インデックスバッファの生成
	pDevice->CreateIndexBuffer(sizeof(WORD) * m_nIndex,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&m_pIdxBuff,
		NULL);
}