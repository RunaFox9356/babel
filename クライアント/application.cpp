//=============================================================================
//
// アプリケーションクラス(application.h)
// Author : 唐�ｱ結斗
// 概要 : 各クラスの呼び出しを行うクラス
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "application.h"

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include "super.h"
#include "renderer.h"
#include "debug_proc.h"
#include "input.h"
#include "texture.h"
#include "camera.h"
#include "light.h"
#include "sound.h"
#include "object.h"
#include "polygon2D.h"
#include "polygon3D.h"
#include "model3D.h"
#include "scene_mode.h"
#include "title.h"
#include "game.h"
#include "hacker.h"
#include "result.h"
#include "fade.h"
#include "pause.h"
#include "tutorial.h"
#include "collision.h"
#include "debug_scene.h"
#include "tcp_client.h"
#include "model_data.h"
#include "mapDataManager.h"
#include "player.h"
#include "motion.h"
#include "matching.h"
#include "font.h"
#include "selectScreen.h"
#include "particle_manager.h"
#include "shader_manager.h"
#include "map_select.h"
#include "ranking.h"
#include "agentTutorial.h"
#include "gameover.h"
#include "client.h"
#include "tcp_client.h"
#include "udp_client.h"
#include "loadingScene.h"
#include "model_skin.h"
#include "HackerUI.h"
#include "Timer.h"
#include "AgentUI.h"

//*****************************************************************************
// 静的メンバ変数宣言
//*****************************************************************************
CApplication* CApplication::m_pApplication = nullptr;

CApplication* CApplication::GetInstance()
{
	if (m_pApplication == nullptr)
	{
		m_pApplication = new CApplication;
	}
	return m_pApplication;
}

CApplication::CApplication() : m_mode(MODE_NONE),
	m_nextMode(MODE_NONE),
	m_nPriority(0),
	m_bWireFrame(false),
	m_hWnd(nullptr),
	m_pSceneMode(nullptr),
	m_Client(nullptr),
	m_pMapCamera(nullptr),
	m_pCamera(nullptr),
	m_pDebugProc(nullptr),
	m_pRenderer(nullptr),
	m_pInput(nullptr),
	m_pTexture(nullptr),
	m_pFade(nullptr),
	m_pLight(nullptr),
	m_pSound(nullptr),
	m_pPause(nullptr),
	m_pParticleManager(nullptr),
	m_pShaderManager(nullptr),
	m_pMapDataManager(nullptr),
	m_pFont(nullptr),
	m_pRanking(nullptr)
{
	// 疑似乱数の初期化
	srand((unsigned int)time(NULL));
}

CApplication::~CApplication()
{
	assert(m_pRenderer == nullptr);
	assert(m_pInput == nullptr);
	assert(m_pTexture == nullptr);
	assert(m_pCamera == nullptr);
	assert(m_pSound == nullptr);
}

//=============================================================================
// スクリーン座標をワールド座標にキャストする
// Author : 唐�ｱ結斗
// 概要 : 
//=============================================================================
D3DXVECTOR3 CApplication::ScreenCastWorld(const D3DXVECTOR3 &pos)
{
	// 計算用マトリックス
	D3DXMATRIX mtx, mtxTrans, mtxView, mtxPrj, mtxViewPort;

	// 行列移動関数 (第一引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);

	// カメラのビューマトリックスの取得
	mtxView = m_pCamera->GetMtxView();

	// カメラのプロジェクションマトリックスの取得
	mtxPrj = m_pCamera->GetMtxProj();

	// マトリックスの乗算
	mtx = mtxTrans * mtxView * mtxPrj;

	// ビューポート行列（スクリーン行列）の作成
	float w = (float)CRenderer::SCREEN_WIDTH / 2.0f;
	float h = (float)CRenderer::SCREEN_HEIGHT / 2.0f;

	mtxViewPort = {
		w , 0 , 0 , 0 ,
		0 ,-h , 0 , 0 ,
		0 , 0 , 1 , 0 ,
		w , h , 0 , 1
	};

	// マトリックスのXYZ
	D3DXVECTOR3 vec = D3DXVECTOR3(mtx._41, mtx._42, mtx._43);

	D3DXVec3TransformCoord(&vec, &vec, &mtxViewPort);

	return vec;
}

//=============================================================================
// ワールド座標をスクリーン座標にキャストする
// Author : 唐�ｱ結斗
// 概要 : 
//=============================================================================
D3DXVECTOR3 CApplication::WorldCastScreen(const D3DXVECTOR3 &pos)
{
	// 計算用ベクトル
	D3DXVECTOR3 vec = pos;

	// 計算用マトリックス
	D3DXMATRIX mtx, mtxTrans, mtxView, mtxPrj, mtxViewPort;

	// 行列移動関数 (第一引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, pos.x, pos.y, pos.z);

	// カメラのビューマトリックスの取得
	mtxView = m_pCamera->GetMtxView();

	// カメラのプロジェクションマトリックスの取得
	mtxPrj = m_pCamera->GetMtxProj();
	
	// ビューポート行列（スクリーン行列）の作成
	D3DXMatrixIdentity(&mtxViewPort);
	float w = (float)CRenderer::SCREEN_WIDTH / 2.0f;
	float h = (float)CRenderer::SCREEN_HEIGHT / 2.0f;
	mtxViewPort = {
		w , 0 , 0 , 0 ,
		0 ,-h , 0 , 0 ,
		0 , 0 , 1 , 0 ,
		w , h , 0 , 1
	};

	// 逆行列の算出
	D3DXMatrixInverse(&mtxView, NULL, &mtxView);
	D3DXMatrixInverse(&mtxPrj, NULL, &mtxPrj);
	D3DXMatrixInverse(&mtxViewPort, NULL, &mtxViewPort);

	// 逆変換
	mtx = mtxViewPort * mtxPrj * mtxView;
	D3DXVec3TransformCoord(&vec, &D3DXVECTOR3(vec.x, vec.y, vec.z), &mtx);

	return vec;
}

//=============================================================================
// 角度の正規化処理
// Author : 唐�ｱ結斗
// 概要 : 角度が円周率の2倍を超えたときに範囲内に戻す
//=============================================================================
float CApplication::RotNormalization(float fRot)
{
	if (fRot >= D3DX_PI)
	{// 移動方向の正規化
		fRot -= D3DX_PI * 2;
	}
	else if (fRot <= -D3DX_PI)
	{// 移動方向の正規化
		fRot += D3DX_PI * 2;
	}

	return fRot;
}

//=============================================================================
// 角度の正規化処理
// Author : 唐�ｱ結斗
// 概要 : 角度が円周率の2倍を超えたときに範囲内に戻す
//=============================================================================
float CApplication::RotNormalization(float fRot, float fMin, float fMax)
{
	if (fRot >= fMax)
	{// 移動方向の正規化
		fRot -= D3DX_PI * 2;
	}
	else if (fRot <= fMin)
	{// 移動方向の正規化
		fRot += D3DX_PI * 2;
	}

	return fRot;
}

//=============================================================================
// シーンの設定
// Author : 唐�ｱ結斗
// 概要 : 現在のシーンを終了し、新しいシーンを設定する
//=============================================================================
void CApplication::SetMode(SCENE_MODE mode)
{
	// オブジェクトの解放
	CSuper::ReleaseAll(false);

	// 当たり判定の終了
	CCollision::ReleaseAll();

	CCamera *pCamera = CApplication::GetCamera();
	pCamera->SetFollowTarget(false);
	pCamera->SetTargetPosR(false);

	CCamera *pCameraMap = CApplication::GetMapCamera();
	pCameraMap->SetFollowTarget(false);
	pCameraMap->SetTargetPosR(false);
	if (m_pSceneMode != nullptr)
	{
		m_pSceneMode->Uninit();
		m_pSceneMode = nullptr;
	}

	m_mode = mode;

	switch (m_mode)
	{
	case CApplication::MODE_TITLE:
		m_pSceneMode = new CTitle;
		break;

	case CApplication::MODE_GAME:
		m_pSceneMode = new CGame;
		break;

	case CApplication::MODE_AGENT:
		m_pSceneMode = new CGame;
		break;

	case CApplication::MODE_HACKER:
		m_pSceneMode = new CHacker;
		break;

	case CApplication::MODE_RESULT:
		m_pSceneMode = new CResult;
		break;

	case CApplication::MODE_TUTORIAL:
		m_pSceneMode = new CTutorial;
		break;

	case CApplication::MODE_DEBUG:
		m_pSceneMode = new CDebug_Scene;
		break;

	case CApplication::MODE_CONNECT:
		m_pSceneMode = new CMatching;
		break;

	case CApplication::MODE_SELECT:
		m_pSceneMode = new CSelectScreen;
		break;
	case CApplication::MODE_MAPSELECT:
		m_pSceneMode = new CMapSelect;
		break;
	case CApplication::MODE_AGENT_TUTORIAL:
		m_pSceneMode = new CAgentTutorial;
		break;
	case CApplication::MODE_GAMEOVER:
		m_pSceneMode = new CGameover;
		break;
	case CApplication::MODE_LOADING:
		m_pSceneMode = new CLoadingScene;
		break;
	default:
		break;
	}

	if (m_pSceneMode != nullptr)
	{
		m_pSceneMode->Init();
		m_pSceneMode->SetPermanent(true);
	}
}

//=============================================================================
// SReceiveListからデータを受け取る
//=============================================================================
void CApplication::SetReceivePlayerLog(CReceiveData::SReceiveList player)
{
	for (int i = 0; i < MAX_PLAYER; i++)
	{
		CModelData * Data = m_Player[i];
		Data->IsUse = true;
		Data->GetPlayerData()->Player.m_IsGame = true;

		Data->m_SendData.Player.m_PlayData = player.m_PlayrData[i];
		for (int j = 0; j < MaxModel; j++)
		{
			
			Data->m_SendData.Player.m_isPopGimmick[j] = player.m_isPopGimmick[j];
			Data->m_SendData.Player.m_isPopEnemy[j] = player.m_isPopEnemy[j];
		}
	}

	SetPlayerLog(m_Player[1]);
}

//=============================================================================
//CModelDataからデータを受け取る
//=============================================================================
void CApplication::SetPlayerLog(CModelData * player)
{
	if (m_MoveModel != nullptr&&m_IsbConnect && m_isMap && player->IsUse && player->GetPlayerData()->Player.m_IsGame)
	{
		if (player->GetPlayerData()->Player.mId != 0 || m_MoveModel == nullptr)
		{
			return;
		}
		if (m_MoveModel != nullptr)
		{
			D3DXVECTOR3 pos = player->GetPlayerData()->Player.m_PlayData.m_pos;
			m_MoveModel->SetPos(pos);
			m_MoveModel->SetRotDest(player->GetPlayerData()->Player.m_PlayData.m_rot);
		}
	

		if (player->GetPlayerData()->Player.m_PlayData.m_haveItemLeftId != 0&& m_MoveModel != nullptr)
		{
			m_MoveModel->SetHaveItem(m_MoveModel->SetListItem(player->GetPlayerData()->Player.m_PlayData.m_haveItemLeftId));
		}

		if (player->GetPlayerData()->Player.m_PlayData.m_haveItemRightId != 0 && m_MoveModel != nullptr)
		{
			//m_MoveModel->SetHaveItemKey(player->GetCommu()->Player.m_haveItemRightId&& m_MoveModel != nullptr);
		}
		if (m_MoveModel->GetMotion() != nullptr)
		{
			if (player->GetPlayerData()->Player.m_PlayData.m_motion != m_MoveModel->GetMotion()->GetNumMotion() && m_MoveModel != nullptr)
			{
				m_MoveModel->GetMotion()->SetNumMotion(player->GetPlayerData()->Player.m_PlayData.m_motion);
			}
		}
		if (m_MoveModel->GetSkin() != nullptr)
		{
			m_MoveModel->GetSkin()->ChangeAnim(player->GetPlayerData()->Player.m_PlayData.m_motion);
		}
		if (player->GetPlayerData()->Player.m_addscore != 0 && m_MoveModel != nullptr)
		{
			m_MoveModel->SetScore(player->GetPlayerData()->Player.m_score);
		}
		if (GetMode() == MODE_AGENT)
		{
			CGame::GetUiManager()->GetTimer()->SetTimer(player->GetPlayerData()->Player.m_PlayData.m_Timer);
		}
		if (GetMode() == MODE_HACKER)
		{
			CHacker::GetHackerUI()->GetTimer()->SetTimer(player->GetPlayerData()->Player.m_PlayData.m_Timer);
		}
		if (player->GetPlayerData()->Player.m_PlayData.m_Life <= 0 && GetMode() == MODE_HACKER)
		{
			CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
		}
	}
	//m_PlayerLog.push_back(*player);
}

//=============================================================================
// 初期化
// Author : 唐�ｱ結斗
// 概要 : メンバ変数を解放し、他クラスのクリエイト関数を呼び出す
//=============================================================================
HRESULT CApplication::Init(HINSTANCE hInstance, HWND hWnd)
{
	m_isActiveWindowThis = false;
	// ウィンドウ
	m_isMap = false;
	m_hWnd = hWnd;
	m_isMainSocket = false;
	// メモリの確保	
	m_pRenderer = new CRenderer;
	m_pDebugProc = new CDebugProc;
	m_pTexture = new CTexture;
	m_pCamera = new CCamera;
	m_pMapCamera = new CCamera;
	m_pSound = new CSound;
	m_pMapDataManager = CMapDataManager::Create();
	m_pParticleManager = new CParticleManager();
	m_pFont = nullptr;
	m_pFont = new CFont;
	m_pRanking = CRanking::Create();

	m_Client = new CClient;

	WSADATA wasData;


	int nErr = WSAStartup(WINSOCK_VERSION, &wasData);

	if (nErr != 0)
	{
		printf("was　error");
	}

	CApplication::LoodIp();
	

	int poot = atoi(m_IPoot.c_str());
	if (!m_Client->Init(m_IPaddress.c_str(), 22233))
	{
		printf("error");
	}
	else
	{
	
	}

	m_IsbConnect = false;
	m_IsEnemybConnect = false;

	// 入力デバイスのメモリ確保
	m_pInput = CInput::Create();

	// 初期化処理
	assert(m_pInput != nullptr);
	if (FAILED(m_pInput->Init(hInstance, m_hWnd)))
	{
		return E_FAIL;
	}

	// 初期化処理
	assert(m_pRenderer != nullptr);
	if (FAILED(m_pRenderer->Init(m_hWnd, TRUE)) != 0)
	{//初期化処理が失敗した場合
		return-1;
	}

	// 初期化処理
	assert(m_pTexture != nullptr);
	//std::future<void> texAsync = m_pTexture->InitAsync();
	m_pTexture->Init();

	// 初期化処理
	assert(m_pCamera != nullptr);
	m_pCamera->Init();
	m_pCamera->SetViewType(CCamera::TYPE_CLAIRVOYANCE);

	// 初期化処理
	assert(m_pMapCamera != nullptr);
	m_pMapCamera->Init();
	m_pMapCamera->SetViewType(CCamera::TYPE_PARALLEL);

	// 初期化
	assert(m_pDebugProc != nullptr);
	m_pDebugProc->Init();

	if (m_pMapDataManager)
	{
		m_pMapDataManager->LoadData();
	}

	// パーティクルデータの読み込み
	m_pParticleManager->LoadAll();

	// シェーダーの読み込み
	m_pShaderManager = new CShaderManager();
	m_pShaderManager->LoadAll();

	// 初期化処理
	assert(m_pSound != nullptr);
	m_pSound->Init(m_hWnd);

	// モデル情報の初期化
	CModel3D::InitModel();

	// ライトの作成
	m_pLight = CLight::Create(D3DXVECTOR3(1.0f, -1.0f, 1.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	CLight::Create(D3DXVECTOR3(-1.0f, 1.0f, -1.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	// フェードの設定
	m_pFade = CFade::Create();

	// ポーズの生成
	m_pPause = CPause::Create();
	m_pPause->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));

	m_Player[0] = new CModelData;
	m_Player[1] = new CModelData;

	// 待機処理
	//texAsync.wait();

	SetMode(MODE_LOADING);
	return S_OK;
}

//=============================================================================
// 終了
// Author : 唐�ｱ結斗
// 概要 : メモリの解放とリリース関数を呼び出す
//=============================================================================
void CApplication::Uninit()
{
	// モデル情報の終了
	CModel3D::UninitModel();
	m_IsbConnect = false;
	m_IsEnemybConnect = false;
	// オブジェクトの解放
	CSuper::ReleaseAll(true);

	// 当たり判定の終了
	CCollision::ReleaseAll();

	if (m_pFont != nullptr)
	{// 終了処理

		m_pFont->ReleaseAll();
		delete m_pFont;
		m_pFont = nullptr;
	}

	if (m_pParticleManager != nullptr)
	{
		m_pParticleManager->ReleaseAll();
		delete m_pParticleManager;
		m_pParticleManager = nullptr;
	}

	for (int i = 0; i < MAX_PLAYER; i++)
	{
		if (m_Player[i] != nullptr)
		{// 終了処理
			// メモリの解放
			delete m_Player[i];
			m_Player[i] = nullptr;
		}
	}

	if (m_pRenderer != nullptr)
	{// 終了処理
		m_pRenderer->Uninit();

		// メモリの解放
		delete m_pRenderer;
		m_pRenderer = nullptr;
	}
	if (m_pDebugProc != nullptr)
	{// 終了処理
		m_pDebugProc->Uninit();

		// メモリの解放
		delete m_pDebugProc;
		m_pDebugProc = nullptr;
	}

	if (m_pInput != nullptr)
	{// 終了処理
		m_pInput->Uninit();
		m_pInput = nullptr;
	}

	if (m_pTexture != nullptr)
	{// 終了処理
		m_pTexture->Uninit();

		// メモリの解放
		delete m_pTexture;
		m_pTexture = nullptr;
	}

	if (m_pCamera != nullptr)
	{// 終了処理
		m_pCamera->Uninit();

		// メモリの解放
		delete m_pCamera;
		m_pCamera = nullptr;
	}

	if (m_pMapCamera != nullptr)
	{// 終了処理
		m_pMapCamera->Uninit();

		// メモリの解放
		delete m_pMapCamera;
		m_pMapCamera = nullptr;
	}

	if (m_pSound != nullptr)
	{// 終了処理
		m_pSound->Uninit();

		// メモリの解放
		delete m_pSound;
		m_pSound = nullptr;
	}
	if (m_Client != NULL)
	{
		m_Client->Uninit();
		delete m_Client;
		m_Client = NULL;
	}
	if (m_pMapDataManager)
	{
		m_pMapDataManager->Uninit();
		delete m_pMapDataManager;
		m_pMapDataManager = nullptr;
	}
	if (m_pShaderManager)
	{
		m_pShaderManager->ReleaseAll();
		delete m_pShaderManager;
		m_pShaderManager = nullptr;
	}

	//ランキングマネージャーの破棄
	if (m_pRanking)
	{
		m_pRanking = nullptr;
	}

	// ライトの解放
	CLight::ReleaseAll();
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 更新
//=============================================================================
void CApplication::Update()
{
	if (m_nextMode != MODE_NONE)
	{
		m_pFade->SetFade(m_nextMode);
		m_nextMode = MODE_NONE;
	}

	m_pInput->Update();

	if (!CSuper::GetPause())
	{
		m_pCamera->Update();
		m_pMapCamera->Update();
	}

	m_pRenderer->Update();

#ifdef _DEBUG
	CDebugProc::Print("更新時間 : %lf\n", m_pRenderer->GetUpdateTime());	// (秒)
	CDebugProc::Print("描画時間 : %lf\n", m_pRenderer->GetRenderTime());	// (秒)
	CDebugProc::Print("FPS : %d\n", GetFps());
	CDebugProc::Print("現在のシーン : %d\n", (int)m_mode);

	if (m_pInput->Trigger(DIK_F7))
	{
		bool bDrawFlag = CDebugProc::GetDrawFlag();
		bDrawFlag ^= 1;
		CDebugProc::SetDrawFlag(bDrawFlag);
	}

	if (m_pInput->Trigger(DIK_F8))
	{
		m_bWireFrame ^= 1;
	}

	if (m_bWireFrame)
	{
		m_pRenderer->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	}
	else
	{
		m_pRenderer->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	}

#endif // DEBUG
}

//=============================================================================
// 描画
// Author : 唐�ｱ結斗
// 概要 : 描画
//=============================================================================
void CApplication::Draw()
{
	m_pRenderer->Draw();
}

//=============================================================================
// 通信スレッド
//=============================================================================
CApplication::SConnectCheck CApplication::ConnectTh(int game)
{
	SConnectCheck ConnectCheck;
	ConnectCheck.myConnect = false;
	ConnectCheck.enemyConnect = false;
	CClient *Client = CApplication::GetClient();	// 通信クラスの取得
	char aRecvData[2048];	// 受信データ
	int Timer = 0;
	//自分のゲームモード保存
	SetMyPlayer(game);

	// つながるまでループ
	while (!m_IsbConnect)
	{
		m_IsbConnect = Client->GetTcp()->Connect();
		Client->GetUdp()->Connect();
		ConnectCheck.myConnect = m_IsbConnect;

		if (m_IsbConnect)
		{//gameモードを設定する
			Client->Send((const char*)&game,4, CClient::TYPE_TCP);
		}
		Timer++;
		if (Timer >= 2)
		{
			break;
		}
	}
	// 敵がつながるまでループ
	while (!m_IsEnemybConnect)
	{
		int isRecv = Client->Recv(&aRecvData[0], sizeof(bool), CClient::TYPE_TCP);
		if (isRecv != -1)
		{
			memcpy(&m_IsEnemybConnect, &aRecvData[0], sizeof(bool)); 
			ConnectCheck.enemyConnect = m_IsEnemybConnect;
		}
		
	}
	return ConnectCheck;
}

//=============================================================================
// レシーブスレッド
//=============================================================================
void CApplication::Recv(int Num)
{
	CModelData * Player = CApplication::GetInstance()->GetPlayer(Num);
	// 繋がっている間はループ

	while (CApplication::GetInstance()->GetIsMap())
	{
		CClient *pTcp = CApplication::GetInstance()->GetClient();	// 通信クラスの取得
		char aRecv[2048];	// 受信データ
							// 受信
		int nRecvSize = pTcp->Recv(&aRecv[0], sizeof(CModelData::SSendPack), CClient::TYPE_UDP);

		// 受信データが無かったら
		if (nRecvSize < 0)
		{
			if (pTcp != NULL)
			{
				//pTcp->GetTcp()->Uninit();

				//break;
			}
		}
		if (nRecvSize <= 8)
		{
			int data = 0;
		}

		if (nRecvSize == (int)sizeof(CModelData::SSendPack))
		{

			CModelData::SSendPack Data;
			memcpy(&Data, &aRecv[0], (int)sizeof(CModelData::SSendPack));

			if (CApplication::GetInstance()->GetConnect() && CApplication::GetInstance()->GetIsMap()&&Player->GetPlayerData()->Player.m_MyId == Data.m_MyId)
			{
				Player->GetPlayerData()->SetPlayer(Data);
				Player->SetUse(CApplication::GetInstance()->GetModelSet());
				CApplication::GetInstance()->SetPlayerLog(Player);			
			}
		}
	}
}

std::string CApplication::GetConfigString(const std::string& filePath, const char* pSectionName, const char* pKeyName)
{
	if (filePath.empty())
	{
		return "";
	}
	std::array<char, MAX_PATH> buf = {};
	GetPrivateProfileStringA(pSectionName, pKeyName, "", &buf.front(), static_cast<DWORD>(buf.size()), filePath.c_str());
	return &buf.front();
}


void CApplication::LoodIp()
{
	std::string filePath = "data\\system.ini";
	auto WindowText = GetConfigString(filePath, "System", "IPADDRESS");
	auto WindowWidth = GetConfigString(filePath, "System", "PORT_NUM");
	auto WindowHeight = GetConfigString(filePath, "System", "NAME");

	m_IPaddress = WindowText;
	m_IPoot = WindowWidth;
}