//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 有田明玄
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "polygon2D.h"
#include "object.h"
#include "renderer.h"
#include "application.h"
#include "Timer.h"

//=============================================================================
// インスタンス生成
// Author : 有田明玄
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CTimer * CTimer::Create(int nPriority)
{
	// オブジェクトインスタンス
	CTimer *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CTimer(nPriority);

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す 
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 有田明玄
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CTimer::CTimer(int nPriority/* = PRIORITY_LEVEL0*/) : CPolygon2D(nPriority)
{

}

//=============================================================================
// デストラクタ
// Author : 有田明玄
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CTimer::~CTimer()
{

}

//=============================================================================
// ポリゴンの初期化
// Author : 有田明玄
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CTimer::Init()
{
	CPolygon2D::Init();
	CPolygon2D::SetColor(D3DXCOLOR(1, 1, 1, 0));
	CPolygon2D::LoadTex(72);
	for (int i = 0; i < TIMERMAX; i++)
	{
		m_pNumber[i] = CNumber::Create();
		m_pNumber[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
	return S_OK;
}

//=============================================================================
// ポリゴンの終了
// Author : 有田明玄
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CTimer::Uninit()
{
}

//=============================================================================
// ポリゴンの更新
// Author : 有田明玄
// 概要 : 2Dポリゴンの更新を行う
//=============================================================================
void CTimer::Update()
{
	switch (m_Mode)
	{
	case CTimer::TIME_60:
		TimeCount60();
		break;
	case CTimer::TIME_10:
		TimeCount();
		break;
	}
}

//=============================================================================
// ポリゴンの描画
// Author : 有田明玄
// 概要 : 2Dポリゴンの描画を行う
//=============================================================================
void CTimer::Draw()
{
	CPolygon2D::Draw();
}

//位置のセッター
void CTimer::SetPos(const D3DXVECTOR3& pos)
{
	CPolygon2D::SetPos(pos);

	SetMode(m_Mode);
}

//=============================================================================
// タイマー
// Author : 有田明玄
// 概要 : 時間の処理とそれに伴うポリゴンの調整
//=============================================================================
void CTimer::TimeCount()
{
	//リキャスト
	if (m_nTimer == 0)
	{
		SetDraw(false);
	}
	if (m_nTimer > 0)
	{
		m_pNumber[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_pNumber[0]->SetNumber((m_nTimer / 60));//10以上の位
		m_nTimer--;
	}
	/*else
	{
		m_pNumber[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}*/
}

//=============================================================================
// タイマー
// Author : 有田明玄
// 概要 : 60進数で分と秒と小数点以下で刻む
//=============================================================================
void CTimer::TimeCount60()
{
	m_nMinutes = m_nTimer /3600 ;		//分
	m_nSecond = m_nTimer % 3600 / 60;	//秒

	if (m_nTimer > 0)
	{
		m_pNumber[0]->SetNumber((m_nSecond ) );//秒
		m_pNumber[1]->SetNumber(m_nMinutes);//分
		m_nTimer--;
	}
	else if (m_nTimer <= 0)
	{
		CApplication::GetInstance()->SetNextMode(CApplication::GetInstance()->MODE_RESULT);
	}
}

//=============================================================================
// 桁設定
// Author : 有田明玄
// 概要 : タイマーのモード切り替え
//=============================================================================
void CTimer::SetMode(int mode)
{
	m_Mode = (ETime)mode;

	D3DXVECTOR3 pos = GetPos(), size = GetSize();

	switch (m_Mode)
	{
	case CTimer::TIME_60:
		//分と秒で別のタイマーを使う
		for (int i = 0; i < TIMERMAX; i++)
		{
			m_pNumber[i]->SetDigit(2);
			m_pNumber[i]->SetSize(size);
			//m_pNumber[i]->SetPos(GetPos() - D3DXVECTOR3(m_pNumber[i]->GetDigitSize().x * 2.0f * i, 0.0f, 0.0f));
		}
		m_pNumber[0]->SetPos(pos + D3DXVECTOR3(m_pNumber[0]->GetDigitSize().x * 2.0f, 0.0f, 0.0f));
		m_pNumber[1]->SetPos(pos - D3DXVECTOR3(m_pNumber[1]->GetDigitSize().x * 1.0f, 0.0f, 0.0f));
		CPolygon2D::SetPos(pos/* - D3DXVECTOR3(GetSize().x * 1.5f, 0.0f, 0.0f)*/);
		CPolygon2D::SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		break;
	case CTimer::TIME_10:
		m_pNumber[0]->SetDigit(3);
		m_pNumber[0]->SetSize(size);
		m_pNumber[0]->SetPos(pos);
		break;

	default:

		m_Mode = TIME_60;

		//分と秒で別のタイマーを使う
		for (int i = 0; i < TIMERMAX; i++)
		{
			m_pNumber[i]->SetDigit(2);
			m_pNumber[i]->SetSize(size);
			//m_pNumber[i]->SetPos(pos - D3DXVECTOR3(m_pNumber[i]->GetSize().x * (i * 2.0f), 0.0f, 0.0f));
		}
		m_pNumber[0]->SetPos(pos + D3DXVECTOR3(m_pNumber[0]->GetDigitSize().x * 2.0f, 0.0f, 0.0f));
		m_pNumber[1]->SetPos(pos - D3DXVECTOR3(m_pNumber[1]->GetDigitSize().x * 1.0f, 0.0f, 0.0f));
		CPolygon2D::SetPos(pos/* - D3DXVECTOR3(GetSize().x * 1.5f, 0.0f, 0.0f)*/);
		CPolygon2D::SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		break;
	}
}

//　桁のサイズのセッター
void CTimer::SetDigitSize(const D3DXVECTOR3 digitSize)
{
	SetSize(digitSize);

	for (int nCnt = 0; nCnt < TIMERMAX; nCnt++)
	{
		if (m_pNumber[nCnt])
		{
			m_pNumber[nCnt]->SetDigitSize(digitSize);
		}
	}
}

//描画フラグのセッター
void CTimer::SetDraw(const bool bDraw)
{
	CPolygon2D::SetDraw(bDraw);

	for (int nCnt = 0; nCnt < TIMERMAX; nCnt++)
	{
		if (m_pNumber[nCnt])
			m_pNumber[nCnt]->SetDraw(bDraw);
	}
}
