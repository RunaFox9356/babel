//=============================================================================
//
// soundSource.cpp
// Author: Ricci Alex
//
//=============================================================================

//=============================================================================
// インクルード
//=============================================================================
#include "soundSource.h"
#include "player.h"
#include "application.h"
#include "agent.h"
#include "hacker.h"
#include "game.h"
#include "drone.h"





//コンストラクタ
CSoundSource::CSoundSource() : m_nDelay(0),
m_pos(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
m_label((CSound::SOUND_LABEL)0),
m_fSlope(0.0f),
m_fMaxDist(0.0f),
m_fIntercept(0.0f),
m_fSoundMax(0.0f),
m_nCntDelay(0),
m_pParent(nullptr),
m_pSound(nullptr),
m_bUpdate(true)
{

}

//デストラクタ
CSoundSource::~CSoundSource()
{

}

//初期化
HRESULT CSoundSource::Init()
{
	m_pSound = CApplication::GetInstance()->GetSound();

	if (!m_pSound)
		return E_FAIL;

	//m_pSound->StopSound();

	return S_OK;
}

//終了
void CSoundSource::Uninit()
{
	//ポインタをnullに戻す
	if (m_pParent)
		m_pParent = nullptr;
	if (m_pSound)
		m_pSound = nullptr;

	//メモリの解放
	CSuper::Release();
}

//更新
void CSoundSource::Update()
{
	if (m_pParent)
	{
		SetPos(m_pParent->GetPos());		//位置を更新する
	}

	if (!m_pSound || !m_bUpdate)
		return;

	if (m_nCntDelay > 0)
	{
		m_nCntDelay--;
	}
	else
	{
		m_nCntDelay = m_nDelay;

		//現在のゲームモードを取得する
		CApplication::SCENE_MODE mode = CApplication::GetInstance()->GetMode();
		CPlayer* pPlayer = nullptr;

		//プレイヤー情報を取得する
		if (mode == CApplication::MODE_GAME || mode == CApplication::MODE_AGENT || mode == CApplication::MODE_AGENT_TUTORIAL)
			pPlayer = CGame::GetPlayer();
		else if (mode == CApplication::MODE_HACKER)
			pPlayer = CHacker::GetDrone();

		//nullチェック
		if (!pPlayer)
			return;

		D3DXVECTOR3 playerPos = pPlayer->GetPos(), distance = playerPos - m_pos;
		float fDist = D3DXVec3Length(&distance);

		if (fDist < m_fMaxDist)
		{
			float fVolume = m_fSlope * fDist + m_fIntercept;

			m_pSound->SetVolume(m_label, fVolume);
			m_pSound->PlaySoundA(m_label);
		}
	}
}

//描画
void CSoundSource::Draw()
{

}




//サウンドラベルの設定
void CSoundSource::SetSoudLabel(CSound::SOUND_LABEL label)
{
	if (m_pSound)
		m_pSound->StopSound(m_label);

	m_nCntDelay = 0; 
	m_label = label;
}

//生成
CSoundSource* CSoundSource::Create(D3DXVECTOR3 pos, CSound::SOUND_LABEL label, const int nPlayDelay, const float fMaxSound, const float fMaxDist)
{
	CSoundSource* pObj = new CSoundSource;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理ができなかったら
		return nullptr;
	}

	pObj->m_pos = pos;				//位置の設定
	pObj->m_label = label;			//サウンドラベルの設定
	pObj->m_nDelay = nPlayDelay;	//再生ディレイの設定
	pObj->m_fMaxDist = fMaxDist;	//音波減衰係数の設定
	pObj->m_fSoundMax = fMaxSound;	//最大音量の設定
	pObj->CalcParameters();			//更新処理に必要なパラメーターの計算処理

	return pObj;					//生成したインスタンスを返す
}

//生成
CSoundSource* CSoundSource::Create(CObject* pParent, CSound::SOUND_LABEL label, const int nPlayDelay, const float fMaxSound, const float fMaxDist)
{
	CSoundSource* pObj = new CSoundSource;		//インスタンスを生成する

	if (FAILED(pObj->Init()))
	{//初期化処理ができなかったら
		return nullptr;
	}

	D3DXVECTOR3 pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	if (pParent)
	{//nullチェック
		pObj->m_pParent = pParent;	//親の設定
		pos = pParent->GetPos();	//親の位置を取得する
	}

	pObj->m_pos = pos;				//位置の設定
	pObj->m_label = label;			//サウンドラベルの設定
	pObj->m_nDelay = nPlayDelay;	//再生ディレイの設定
	pObj->m_fMaxDist = fMaxDist;	//音波減衰係数の設定
	pObj->m_fSoundMax = fMaxSound;	//最大音量の設定
	pObj->CalcParameters();			//更新処理に必要なパラメーターの計算処理

	return pObj;					//生成したインスタンスを返す
}

//更新処理に必要なパラメーターの計算処理
void CSoundSource::CalcParameters()
{
	float fSoundMin = 0.0f, fSoundMax = m_fSoundMax, fDistMin = 0.0f, fDistMax = m_fMaxDist;

	if (fSoundMin == fSoundMax)
	{
		m_fSlope = 0.0f;
		m_fIntercept = 0.0f;
		return;
	}

	m_fSlope = (fSoundMin - fSoundMax) / (fDistMax - fDistMin);
	m_fIntercept = fSoundMax - (m_fSlope * fDistMin);
}
