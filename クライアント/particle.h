#ifndef _PARTICLE_H_
#define _PARTICLE_H_

#include "polygon3D.h"

class CParticleEmitter;

class CParticle : public CPolygon3D
{
public:
	//--------------------------------------
	// パーティクル情報の列挙
	//--------------------------------------
	enum ERandomBehavior
	{
		RandomBehavior_Default,
		RandomBehavior_Circle,
		RandomBehavior_MAX
	};

	enum ELockVector
	{
		Lock_None,
		Lock_X,
		Lock_Y,
		Lock_Z,
		Lock_XY,
		Lock_XZ,
		Lock_YZ,
		Lock_MAX
	};

	//--------------------------------------
	// パーティクル情報の構造体
	//--------------------------------------
	struct SRotate
	{
		SRotate()
		{
			angle	  = 0.0f;
			radius	  = 360.0f;
			randomMin = 0.0f;
			randomMax = 0.0f;
			useRotate = false;
		}

		float angle;
		float radius;
		float randomMin;
		float randomMax;
		bool  useRotate;
	};

	struct SRandom
	{
		SRandom()
		{
			randomVelocityMin = { 0.0f,0.0f,0.0f };
			randomVelocityMax = { 0.0f,0.0f,0.0f };
			randomWeight	  = { 0.0f,0.0f };
			randomRotate	  = { 0.0f,0.0f };
			randomBehavior	  = RandomBehavior_Default;
			randomLockVector  = Lock_None;
			distortion		  = { 1.0f,1.0f };
		}

		D3DXVECTOR3		randomVelocityMin;
		D3DXVECTOR3		randomVelocityMax;
		D3DXVECTOR2		randomWeight;
		D3DXVECTOR2		randomRotate;
		ERandomBehavior randomBehavior;
		ELockVector		randomLockVector;
		D3DXVECTOR2		distortion;
	};

	struct SColor
	{
		SColor()
		{
			color		= { 1.0f,1.0f,1.0f,1.0f };
			colorPtTime = 0;
		}

		D3DXCOLOR color;
		int		  colorPtTime;
	};

	struct SInfo
	{
		SInfo()
		{
			pos				= D3DXVECTOR3( 0.0f, 10.0f, 0.0f );
			destPos			= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			posOffset		= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			move			= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			velocity		= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			scale			= D3DXVECTOR3( 10.0f, 10.0f, 0.0f );
			scalingValue	= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			col				= D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
			destCol			= D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );
			rotate			= SRotate();
			random			= SRandom();
			beginDelay		= 0;
			fallDelayTime	= 0;
			popTime			= 75;
			moveAttenuation = 1.0f;
			colAttenuation  = 0.5f;
			rotateValue		= 0.0f;
			weight			= 0.0f;
			circleDistance  = 0.0f;
			collision		= false;
			useRandom		= false;
		}

		D3DXVECTOR3			pos;				// 位置
		D3DXVECTOR3			destPos;			// 目的の位置
		D3DXVECTOR3			posOffset;			// 位置のオフセット
		D3DXVECTOR3			move;				// 移動量
		D3DXVECTOR3			velocity;			// ベクトル
		D3DXVECTOR3			scale;				// 大きさ
		D3DXVECTOR3			scalingValue;		// 拡縮量
		std::vector<SColor> colorProp;			// 色のプロパティ
		D3DXCOLOR			col;				// 色
		D3DXCOLOR			destCol;			// 目的の色
		SRotate				rotate;				// 回転構造体
		SRandom				random;				// ランダム構造体
		int					textureIndex;		// テクスチャのインデックス
		int					beginDelay;			// 開始の遅延
		int					fallDelayTime;		// 落ちるまでの遅延
		int					popTime;			// 生存時間
		float				moveAttenuation;	// 移動の減衰量
		float				colAttenuation;		// 色の減衰量
		float				rotateValue;		// 回転量
		float				weight;				// 重さ
		float				circleDistance;		// 円の範囲
		bool				collision;			// 判定
		bool				useRandom;			// ランダムを適用するかどうか
	};

	CParticle();
	~CParticle() override;

	static CParticle* Create(SInfo& info, const std::string& url, CParticleEmitter* emitter = nullptr);	// 生成処理

	HRESULT Init() override;							// 初期化処理
	void Uninit() override;
	void Update() override;								// 更新処理
	void Draw() override;								// 描画処理
	bool isExpired() { return m_DestroyTime <= 0; }		// 期限切れかどうか

	//セッター
	void SetInfo(SInfo& info) { m_info = info; }
	void SetPath(const std::string& path) { m_path = path; }

	SInfo GetInfo() { return m_info; }

private:
	CParticleEmitter *m_pEmitter;			// エミッタのポインタ
	SInfo			 m_info;				// パーティクルの情報
	std::string		 m_path;				// テクスチャのパス
	D3DXVECTOR3		 m_move;
	int				 m_nTime;				// 時間
	int				 m_DestroyTime;
};
#endif