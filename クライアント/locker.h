//=============================================================================
//
// locker.h
// Author: Ricci Alex
//
//=============================================================================
#ifndef _LOCKER_H
#define _LOCKER_H


//=============================================================================
// インクルード
//=============================================================================
#include "gimmick.h"

//=============================================================================
// 前方宣言
//=============================================================================
class CEmptyObj;
class CModelObj;
class CCollision_Rectangle3D;
class CAgent;
class CUiMessage;



class CLocker : public CGimmick
{
public:
	CLocker();						//コンストラクタ
	~CLocker() override;			//デストラクタ

	HRESULT Init() override;		// 初期化
	void Uninit() override;			// 終了
	void Update() override;			// 更新
	void Draw() override;			// 描画

	void SetPos(const D3DXVECTOR3 &pos) override;				// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &/*posOld*/) override {}		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot) override;				// 向きのセッター
	void SetSize(const D3DXVECTOR3 &/*size*/) override {};			// 大きさのセッター
	D3DXVECTOR3 GetPos() override;								// 位置のゲッター
	D3DXVECTOR3 GetPosOld()  override { return D3DXVECTOR3(); }	// 過去位置のゲッター
	D3DXVECTOR3 GetRot()  override;								// 向きのゲッター
	D3DXVECTOR3 GetSize()  override { return D3DXVECTOR3(); }	// 大きさのゲッター

	void SetRenderMode(int mode) override;						// エフェクトのインデックスの設定


	static CLocker* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 rot);		//生成

private:

	void CalcExitPos();			//出る位置を計算する
	void OpenDoor();			//ドアを開けるアニメーション
	void CloseDoor();			//ドアを閉めるアニメーション

	void CreateUiMessage(const char* pMessage, const int nTexIdx);		//UIメッセージの生成処理
	void DestroyUiMessage();	//UIメッセージの破棄処理

private:

	static const D3DXVECTOR3 DEFAULT_DOOR_POS;					//ディフォルトドアの相対位置
	static const D3DXVECTOR3 DEFAULT_COLLISION_POS;				//ディフォルトコリジョンの相対位置
	static const D3DXVECTOR3 DEFAULT_COLLISION_SIZE;			//ディフォルトコリジョンのサイズ
	static const D3DXVECTOR3 DEFAULT_INTERACT_HITBOX_POS;		//ディフォルトのヒットボックスの相対位置
	static const D3DXVECTOR3 DEFAULT_INTERACT_HITBOX_SIZE;		//ディフォルトのヒットボックスのサイズ
	static const int		 DEFAULT_DELAY_FRAMES;				//ディフォルトのディレイフレーム数

	D3DXVECTOR3		 m_pos;										//位置
	D3DXVECTOR3		 m_rot;										//回転角度
	D3DXVECTOR3		 m_exitPos;									

	int				 m_nDelayFrames;							//ディレイフレーム数（アニメーション）
	bool			 m_bOpen;									
	bool			 m_bClose;

	CModelObj*		 m_pModel;									//ロッカーモデルへのポインタ
	CEmptyObj*		 m_pHitboxObj;								//ヒットボックス用のオブジェクト
	CModelObj*		 m_pDoor;									//ドアモデルへのポインタ
	CCollision_Rectangle3D*	 m_pInteractionHitbox;				//ヒットボックスのサイズへのポインタ	
	CAgent*			 m_pPlayer;									//プレイヤーへのポインタ
	CUiMessage*		 m_pUiMessage;								//メッセージのUI

};


#endif